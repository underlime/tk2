package
{
    import application.models.common.Advices;
    import application.views.load.LoadView;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.events.TimerEvent;
    import flash.utils.Timer;
    import flash.utils.getDefinitionByName;

    import framework.core.utils.Singleton;

    /**
     * Preloader
     * @author antondenikin
     */

    public class Preloader extends MovieClip
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Preloader()
        {
            if (stage) {
                stage.scaleMode = StageScaleMode.NO_SCALE;
                stage.align = StageAlign.TOP_LEFT;
            }

            _createLoadingView();
            _startTimer();
            _bind();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _loadView:LoadView;
        private var _adviceModel:Advices;

        private var _timer:Timer;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _createLoadingView():void
        {
            _adviceModel = Singleton.getClass(Advices);
            _adviceModel.shuffleAdvices();
            _loadView = new LoadView();
            addChild(_loadView);
            _loadView.status = _adviceModel.getRandomAdvice();
        }

        private function _bind():void
        {
            addEventListener(Event.ENTER_FRAME, checkFrame);
            loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
            loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);
        }

        private function _startTimer():void
        {
            _timer = new Timer(10000);
            _timer.addEventListener(TimerEvent.TIMER, _updateStatus);
            _timer.start();
        }

        private function loadingFinished():void
        {
            _timer.stop();
            _timer.removeEventListener(TimerEvent.TIMER, _updateStatus);
            _timer = null;

            removeEventListener(Event.ENTER_FRAME, checkFrame);
            loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
            loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
            _loadView.destroy();
            _startup();
        }

        private function _startup():void
        {
            var mainClass:Class = getDefinitionByName("Main") as Class;
            addChild(new mainClass() as DisplayObject);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function ioError(e:IOErrorEvent):void
        {
            trace(e.text);
        }

        private function progress(e:ProgressEvent):void
        {
            var percent:Number = (e.bytesLoaded / e.bytesTotal) * 200;
            _loadView.percent = percent;
        }

        private function checkFrame(e:Event):void
        {
            if (currentFrame == totalFrames) {
                stop();
                loadingFinished();
            }
        }

        private function _updateStatus(event:TimerEvent):void
        {
            _loadView.status = _adviceModel.getRandomAdvice();
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }

}