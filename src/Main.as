package
{
    import application.controllers.ApplicationController;

    import com.flashdynamix.utils.SWFProfiler;

    import flash.display.Sprite;
    import flash.events.Event;

    /**
     * Class holder
     * @author Aleksey Tsvetkov
     */
    [Frame(factoryClass="Preloader")]
    [SWF(width='750', height='510', backgroundColor='#000000', frameRate='30')]
    public class Main extends Sprite
    {
        public function Main():void
        {
            if (stage)
                init();
            else
                addEventListener(Event.ADDED_TO_STAGE, init);
        }

        private function init(e:Event = null):void
        {
            CONFIG::DEBUG {
                SWFProfiler.init(stage, this);
            }

            new ApplicationController(this);
        }
    }
}