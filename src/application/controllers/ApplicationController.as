/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 15:56
 */
package application.controllers
{
    import application.config.AppConfig;
    import application.config.Config;
    import application.config.DataApi;
    import application.controllers.application.ApiExporter;
    import application.controllers.bank.BankExpressController;
    import application.controllers.bonus.StartBonusController;
    import application.controllers.events.EventsController;
    import application.events.ApplicationEvent;
    import application.events.LoadEvent;
    import application.models.GameDataBase;
    import application.models.ModelData;
    import application.models.user.User;
    import application.server_api.ApiMethods;
    import application.server_api.ServerApi;
    import application.service.energy.EnergySync;
    import application.service.settings.SettingsSevice;
    import application.views.PopupManager;
    import application.views.error.ErrorView;
    import application.views.load.LoadingView;

    import flash.display.DisplayObjectContainer;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.UncaughtErrorEvent;

    import framework.core.error.ApplicationError;
    import framework.core.events.GameEvent;
    import framework.core.socnet.SocNet;
    import framework.core.struct.controller.BaseController;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;
    import framework.core.tools.wall_post.WallPostTool;
    import framework.core.utils.Singleton;

    import mx.utils.ObjectUtil;

    public class ApplicationController extends BaseController
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ApplicationController(root:DisplayObjectContainer)
        {
            super(root);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            super.execute();
            super.root.addChild(_eventsView);

            _setupSettings();
            _setupLoadingView();
            _setupApplication();
            _setupLoading();
        }

        override public function appError(error:ApplicationError):void
        {
            _loadingView.hideLoader();

            if (error.fatal) {
                super.clear();
            }

            super.root.addChild(new ErrorView(error));
        }

        override public function destroy():void
        {
            if (_eventBrokerController && !_eventBrokerController.destroyed) {
                _eventBrokerController.destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function onApplicationAlbumCreated(e:GameEvent):void
        {
            serverApi.makeRequest(ApiMethods.USER_SET_ALBUM_ID, {'album_id': e.data['photo_album_id']});
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _loadController:LoadController;
        private var _mapController:MapController;
        private var _eventBrokerController:EventBrokerController;

        private var _loadingView:LoadingView;
        private var _config:Config = new Config();
        private var _apiExporter:ApiExporter;

        private var _settingsService:SettingsSevice;
        private var _eventsView:View = new View();

        private var _eventsController:EventsController;
        private var _userModel:User;

        private var _registerController:RegisterController;
        private var _energySync:EnergySync;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupSettings():void
        {
            _settingsService = new SettingsSevice(super.root.stage);
            _settingsService.execute();

            var popupManager:PopupManager = Singleton.getClass(PopupManager);
            popupManager.registerRoot(container);
        }

        private function _setupLoadingView():void
        {
            _loadingView = new LoadingView();
            super.root.addChild(_loadingView);
        }

        private function _setupApplication():void
        {
            _dataBase = new GameDataBase();
            ModelsRegistry.dataBase = _dataBase;
            _userModel = dataBase.getChildByName(ModelData.USER) as User;

            _socNet = SocNet.instance(this);

            var dataApiConfig:DataApi = this.config.getChildByName(Config.DATA_API_CONFIG) as DataApi;
            var mock:Boolean = (dataApiConfig.mock.value && AppConfig.DEBUG);
            _serverApi = new ServerApi(this, mock);

            _eventsController = new EventsController(this);
            _eventsController.execute();

            super.root.loaderInfo.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, _uncaughtErrorHandler);
        }

        private function _setupLoading():void
        {
            _loadController = new LoadController(this);
            _loadController.addEventListener(LoadEvent.COMPLETE, _loadCompleteHandler);
            _loadController.execute();
        }

        private function _addMapController():void
        {
            _energySync = new EnergySync(this);
            _energySync.execute();

            _mapController = new MapController(this);
            _mapController.execute();
        }

        private function _showStartBonus():void
        {
            (new StartBonusController(this)).execute();
        }

        private function _exportApi():void
        {
            _apiExporter = new ApiExporter(this);
            _apiExporter.addEventListener(ApplicationEvent.CALL_BANK, _onCallBank);
            _apiExporter.addEventListener(ApplicationEvent.CALL_GUEST_SCREEN, _onCallGuestScreen);
            _apiExporter.exportApi();
            _apiExporter.dispatchApiInit();
        }

        private function _setMap():void
        {
            _addMapController();
            _exportApi();
            _eventsController.buffered = false;
            if (!_userModel.userInfo.was_friends_prizes_given.value && !_userModel.firstTime) {
                _showStartBonus();
            }
            _setupWallPosts();
            _eventBrokerController = new EventBrokerController(this);
            _eventBrokerController.execute();
        }

        private function _setRegister():void
        {
            _registerController = new RegisterController(this);
            _registerController.addEventListener(Event.COMPLETE, _registerCompleteHandler);
            _registerController.execute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _loadCompleteHandler(event:LoadEvent):void
        {
            _loadController.destroy();
            if (event.registerNeeded) {
                _setRegister();
            }
            else {
                _setMap();
            }
        }

        private function _registerCompleteHandler(event:Event):void
        {
            _registerController.destroy();
            _userModel.showTutorial = true;
            _userModel.firstTime = true;
            _setMap();
        }

        private function _setupWallPosts():void
        {
            WallPostTool.applicationLink = AppConfig.applicationLink;
            WallPostTool.gameName = AppConfig.APP_LABEL;
            WallPostTool.albumId = _userModel.userInfo.album_id.value;
        }

        private function _onCallBank(e:ApplicationEvent):void
        {
            (new BankExpressController(this)).execute();
        }

        private function _onCallGuestScreen(e:ApplicationEvent):void
        {
            if (_mapController) {
                _mapController.callGuestScreen(e.data['soc_net_id']);
            }
        }

        private function _uncaughtErrorHandler(event:UncaughtErrorEvent):void
        {
            var time:String = (new Date()).toUTCString();
            var userMessage:String = "Runtime error at {time}!".replace("{time}", time);

            var errorInfo:String;
            if (event.error is Error) {
                var error:Error = Error(event.error);
                errorInfo = "Exception at {time}\n".replace("{time}", time);
                errorInfo += error.name + ": " + error.message + "\n";
                errorInfo += error.getStackTrace() + "\n";
            }
            else if (event.error is ErrorEvent) {
                var errorEvent:ErrorEvent = ErrorEvent(event.error);
                errorInfo = "Error event at {time}\n".replace("{time}", time);
                errorInfo += errorEvent.text;
                errorInfo += errorEvent.toString() + "\n";
            }
            else {
                errorInfo = "Unclassified error at {time}\n".replace("{time}", time);
                errorInfo += ObjectUtil.toString(event.error) + "\n";
            }

            _serverApi.makeRequest(ApiMethods.SYSTEM_LOG, {"error_info": errorInfo});

            var appError:ApplicationError = new ApplicationError();
            appError.errorName = ApplicationError.UNKNOWN_ERROR;
            appError.message = userMessage;
            this.appError(appError);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get loadingView():LoadingView
        {
            return _loadingView;
        }

        public function get config():Config
        {
            return _config;
        }

        public function get eventsView():View
        {
            return _eventsView;
        }

        public function get eventsController():EventsController
        {
            return _eventsController;
        }
    }
}
