/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.09.13
 * Time: 19:59
 */
package application.controllers.events
{
    import application.controllers.ApplicationController;
    import application.controllers.bonus.EveryDayBonusController;
    import application.models.ModelData;
    import application.models.events.EventsModel;
    import application.views.events.EventsView;
    import application.views.popup.events.AchievementEventView;
    import application.views.popup.events.SpecialEventView;

    import flash.display.DisplayObjectContainer;
    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;

    public class EventsController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EventsController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
            _container = _appController.eventsView;
            _model = ModelsRegistry.getModel(ModelData.EVENTS) as EventsModel;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _model.addEventListener(Event.CHANGE, _changeHandler);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:EventsModel;
        private var _buffered:Boolean = true;
        private var _eventsInBuffer:Boolean = false;

        private var _appController:ApplicationController;
        private var _container:DisplayObjectContainer;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _showEvents():void
        {
            if (_model.achievements.length > 0)
                _showAchievements();
            if (_model.dayBonus != null)
                _showDayBonus();
            if (_model.new_specials.length > 0)
                _showNewSpecials();
        }

        private function _showAchievements():void
        {
            var events:Array = [];
            for (var i:int = 0; i < _model.achievements.length; i++) {
                events.push(new AchievementEventView(_model.achievements[i], ApplicationController(baseController)));
            }

            var eventsView:EventsView = new EventsView(events);
            _container.addChild(eventsView);
            _model.clearAchievements();
        }

        private function _showNewSpecials():void
        {
            var events:Array = [];
            for (var i:int = 0; i < _model.new_specials.length; i++) {
                events.push(new SpecialEventView(_model.new_specials[i], ApplicationController(baseController)));
            }

            var eventsView:EventsView = new EventsView(events);
            _container.addChild(eventsView);
            _model.clearSpecials();
        }

        private function _showDayBonus():void
        {
            var dayBonusController:EveryDayBonusController = new EveryDayBonusController(_appController);
            dayBonusController.execute();
            _model.clearDayBonus();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            if (_buffered)
                _eventsInBuffer = true;
            else
                _showEvents();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get buffered():Boolean
        {
            return _buffered;
        }

        public function set buffered(value:Boolean):void
        {
            _buffered = value;
            if (!value && _eventsInBuffer) {
                _showEvents();
                _eventsInBuffer = false;
            }
        }
    }
}
