/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.09.13
 * Time: 19:52
 */
package application.controllers.academy
{
    import application.controllers.ApplicationController;
    import application.controllers.popup.LowMoneyController;
    import application.events.ApplicationEvent;
    import application.events.BuyEvent;
    import application.helpers.PostBgHelper;
    import application.models.ModelData;
    import application.models.chars.CharList;
    import application.models.user.User;
    import application.models.user.Vegetable;
    import application.server_api.ApiMethods;
    import application.views.TextFactory;
    import application.views.popup.ChangeVegetableSuccess;
    import application.views.screen.change_vegetable.ChangeVegetableView;

    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.core.error.ApplicationError;
    import framework.core.events.GameEvent;
    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.tools.wall_post.WallPostTool;
    import framework.core.underquery.ApiRequestEvent;

    public class ChangeVegetableController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ChangeVegetableController(baseController:IBaseController)
        {
            super(baseController);
            _model = ModelsRegistry.getModel(ModelData.CHAR_LIST) as CharList;
            _appController = super.baseController as ApplicationController;
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            _currentVegetable = Vegetable.getEnumByKey(_user.userInfo.vegetable.value);

            _price = _user.configPrices.changeVegetablePrice;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_model.isEmpty)
                _loadItems();
            else
                _renderView();
        }

        override public function destroy():void
        {
            _view.removeEventListener(BuyEvent.BUY, _buyHandler);
            _view.destroy();
            if (_successView && !_successView.destroyed)
                _successView.destroy();
            _destroyWallPostTool();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:CharList;
        private var _appController:ApplicationController;

        private var _view:ChangeVegetableView;
        private var _user:User;
        private var _currentVegetable:Vegetable;

        private var _price:int;
        private var _successView:ChangeVegetableSuccess;
        private var _wallPostTool:WallPostTool;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderView():void
        {
            _view = new ChangeVegetableView();
            _view.addEventListener(BuyEvent.BUY, _buyHandler);
            super.container.addChild(_view);
        }

        private function _loadItems():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.GET_CHARS, null, _onServerSuccess);
        }

        private function _checkBuy():Boolean
        {
            var haveMoney:int = _user.userFightData.ferros.value;
            return haveMoney >= _price;
        }

        private function _buy():void
        {
            var needVegetable:Vegetable = _view.vegetable;
            if (needVegetable != _currentVegetable) {
                var params:Object = {
                    "vegetable": _view.vegetable.toString()
                };
                _appController.loadingView.showLoader();
                _appController
                        .serverApi
                        .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onChangeComplete);
                _appController
                        .serverApi
                        .makeRequest(ApiMethods.CHANGE_VEGETABLE, params, _onServerSuccess);
            }
        }

        private function _destroyWallPostTool():void
        {
            if (_wallPostTool && !_wallPostTool.destroyed)
                _wallPostTool.destroy();
            _wallPostTool = null;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            trace("all items loaded");
            _renderView();
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        private function _buyHandler(event:BuyEvent):void
        {
            if (_checkBuy())
                _buy();
            else
                (new LowMoneyController(super.baseController)).execute();
        }

        private function _onChangeComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onChangeComplete);

            _currentVegetable = Vegetable.getEnumByKey(_user.userInfo.vegetable.value);

            _successView = new ChangeVegetableSuccess();
            _successView.addEventListener(ApplicationEvent.SHARE, _onShare);
            super.container.addChild(_successView);
        }

        private function _onShare(e:ApplicationEvent):void
        {
            _appController.loadingView.showLoader();
            _destroyWallPostTool();

            _wallPostTool = new WallPostTool();
            _wallPostTool.postText = TextFactory.instance.getLabel('new_vegetable_label');
            _wallPostTool.bitmapImage = PostBgHelper.buildPicture(_successView.unitContainer.getAsBitmap());
            _wallPostTool.addEventListener(Event.COMPLETE, _onPostComplete);
            _wallPostTool.addEventListener(ErrorEvent.ERROR, _onPostError);
            _wallPostTool.addEventListener(GameEvent.ALBUM_CREATED, _appController.onApplicationAlbumCreated);
            _wallPostTool.addPost();
        }

        private function _onPostError(event:ErrorEvent):void
        {
            var appError:ApplicationError = new ApplicationError();
            appError.fatal = false;
            appError.message = event.text;
            _appController.appError(appError);
        }

        private function _onPostComplete(event:Event):void
        {
            _appController.loadingView.hideLoader();
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
