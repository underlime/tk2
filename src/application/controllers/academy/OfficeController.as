/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 03.09.13
 * Time: 15:52
 */
package application.controllers.academy
{
    import application.controllers.ApplicationController;
    import application.events.OfficeEvent;
    import application.models.user.LoginModel;
    import application.server_api.ApiMethods;
    import application.service.model.CheckLogin;
    import application.sound.SoundManager;
    import application.sound.lib.UISound;
    import application.views.popup.ChangeLoginSuccess;
    import application.views.screen.office.OfficeScreen;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.task.TaskEvent;
    import framework.core.underquery.ApiRequestEvent;
    import framework.core.utils.Singleton;

    public class OfficeController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OfficeController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new OfficeScreen();
            _view.addEventListener(OfficeEvent.CHECK_LOGIN, _checkLoginHandler);
            _view.addEventListener(OfficeEvent.CHANGE_LOGIN, _changeLoginHandler);
            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            if (_checkLoginService && !_checkLoginService.destroyed)
                _checkLoginService.destroy();
            _view.removeEventListener(OfficeEvent.CHECK_LOGIN, _checkLoginHandler);
            _view.removeEventListener(OfficeEvent.CHANGE_LOGIN, _changeLoginHandler);
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _view:OfficeScreen;

        private var _checkLoginService:CheckLogin;

        private var _loginModel:LoginModel = new LoginModel();
        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _checkLoginHandler(event:OfficeEvent):void
        {
            var login:String = _view.login;
            _checkLoginService = new CheckLogin(_appController, login);
            _checkLoginService.addEventListener(TaskEvent.COMPLETE, _checkCompleteHandler);
            _checkLoginService.execute();
        }

        private function _changeLoginHandler(event:OfficeEvent):void
        {
            var params:Object = {
                "login": _view.login
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.CHANGE_LOGIN, params, _onLoginChangeSuccess);
        }

        private function _updateData():void
        {
            if (_loginModel.correct.value == 0)
                _view.loginDeny();
            else
                if (_loginModel.exists.value > 0)
                    _view.loginDeny();
                else
                    _view.loginAllow();
            _sound.playUISound(UISound.OK);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _checkCompleteHandler(event:TaskEvent):void
        {
            _loginModel = _checkLoginService.loginModel;
            _updateData();
            _checkLoginService.destroy();
        }

        private function _onLoginChangeSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();
            var data:Object = e.data;
            if (data['login_allowable'])
                _loginModel.importData(data['login_allowable']);
            _updateData();

            _view.update();
            super.container.addChild(new ChangeLoginSuccess(_view.login));
            _sound.playUISound(UISound.SUCCESS);

            return true;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
