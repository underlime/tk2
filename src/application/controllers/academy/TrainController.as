/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 05.09.13
 * Time: 19:26
 */
package application.controllers.academy
{
    import application.controllers.ApplicationController;
    import application.events.TrainEvent;
    import application.server_api.ApiMethods;
    import application.sound.SoundManager;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.popup.AlertWindow;
    import application.views.screen.train.TrainView;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.underquery.ApiRequestEvent;
    import framework.core.utils.Singleton;

    public class TrainController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrainController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new TrainView();
            _view.addEventListener(TrainEvent.TRAIN, _trainHandler);
            _view.addEventListener(TrainEvent.RESET_SKILLS, _resetHandler);
            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _view:TrainView;
        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _trainHandler(event:TrainEvent):void
        {
            var params:Object = {
                "strength": event.strength,
                "agility": event.agility,
                "intellect": event.intellect
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onTrainComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.SAVE_POINTS, params, _onServerSuccess);
        }

        private function _resetHandler(event:TrainEvent):void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onResetComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.RESET_SKILLS, {}, _onServerSuccess);
        }

        private function _onTrainComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();

            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onTrainComplete);

            var header:String = TextFactory.instance.getLabel("success_label");
            var message:String = TextFactory.instance.getLabel("points_save_label");
            super.container.addChild(new AlertWindow(header, message));
            _sound.playUISound(UISound.OK);
            _view.update();
        }

        private function _onResetComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onResetComplete);
            _view.update();

            var header:String = TextFactory.instance.getLabel("success_label");
            var message:String = TextFactory.instance.getLabel("points_reset_label");
            _sound.playUISound(UISound.OK);
            super.container.addChild(new AlertWindow(header, message));
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
