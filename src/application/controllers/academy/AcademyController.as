/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.09.13
 * Time: 17:20
 */
package application.controllers.academy
{
    import application.events.ApplicationEvent;
    import application.views.TextFactory;
    import application.views.screen.academy.AcademyScreen;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;

    public class AcademyController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AcademyController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupHash();
            _setupView();
        }

        override public function destroy():void
        {
            _academyScreen.destroy();
            _destroyOperateController();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _academyScreen:AcademyScreen;
        private var _hash:Object;

        private var _operateController:Controller;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupHash():void
        {
            _hash = {
                "0": {
                    "controller": TrainController,
                    "label": TextFactory.instance.getLabel('train_label')
                },
                "1": {
                    "controller": LaboratoryController,
                    "label": TextFactory.instance.getLabel('laboratory_label')
                },
                "2": {
                    "controller": OfficeController,
                    "label": TextFactory.instance.getLabel('office_label')
                },
                "3": {
                    "controller": ChangeVegetableController,
                    "label": TextFactory.instance.getLabel('change_vegetable_label')
                }
            };
        }

        private function _setupView():void
        {
            _academyScreen = new AcademyScreen();
            _academyScreen.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _academyScreen.addEventListener(Event.CHANGE, _tabsChangeHandler);

            super.container.addChild(_academyScreen);
        }

        private function _destroyOperateController():void
        {
            if (_operateController && !_operateController.destroyed)
                _operateController.destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _tabsChangeHandler(event:Event):void
        {
            var tabID:int = _academyScreen.tabs.selectedTab;
            if (_hash[tabID] && _hash[tabID]) {
                _destroyOperateController();

                var ControllerClass:Class = _hash[tabID]['controller'];
                var label:String = _hash[tabID]['label'];
                if (_academyScreen)
                    _academyScreen.label = label;
                _operateController = new ControllerClass(super.baseController);
                _operateController.execute();
            }
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
