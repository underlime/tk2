/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 10:38
 */
package application.controllers.academy
{
    import application.common.Currency;
    import application.controllers.*;
    import application.controllers.popup.LowMoneyController;
    import application.events.ApplicationEvent;
    import application.events.BuyEvent;
    import application.helpers.CharsRequestHelper;
    import application.models.ModelData;
    import application.models.chars.CharDetail;
    import application.models.chars.CharList;
    import application.models.user.User;
    import application.models.user.UserInfo;
    import application.server_api.ApiMethods;
    import application.service.model.GetAllChars;
    import application.views.popup.BuyingItem;
    import application.views.popup.GreatBuy;
    import application.views.screen.laboratory.LaboratoryScreen;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;
    import framework.core.underquery.ApiRequestEvent;

    public class LaboratoryController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LaboratoryController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
            _charsList = ModelsRegistry.getModel(ModelData.CHAR_LIST) as CharList;

            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            _userInfo = _user.userInfo;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _getChars();
        }

        private function _getChars():void
        {
            _charsService = new GetAllChars(baseController as ApplicationController);
            _charsService.addEventListener(TaskEvent.COMPLETE, _charsLoadedHandler);
            _charsService.execute();
        }

        private function _charsLoadedHandler(event:Event):void
        {
            _charsService.destroy();
            _renderView();
        }

        override public function destroy():void
        {
            _labView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _charsService:GetAllChars;

        private var _charsList:CharList;
        private var _labView:LaboratoryScreen;
        private var _appController:ApplicationController;
        private var _user:User;

        private var _userInfo:UserInfo;
        private var _char:CharDetail;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderView():void
        {
            _labView = new LaboratoryScreen();
            _labView.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _labView.addEventListener(BuyEvent.BUY, _buyHandler);
            super.container.addChild(_labView);
        }

        private function _canBuy(char:CharDetail, currency:Currency):Boolean
        {
            var haveMoney:int;
            var price:int;

            if (currency == Currency.FERROS) {
                haveMoney = _user.userFightData.ferros.value;
                price = char.price_ferros.value
            } else {
                haveMoney = _user.userFightData.tomatos.value;
                price = char.price_tomatos.value
            }

            return haveMoney >= price;
        }

        private function _buy(char:CharDetail, currency:Currency):void
        {
            if (char) {
                var params:Object = CharsRequestHelper.getCharsData(char);
                params['currency'] = currency.toString();
                super.baseController
                        .serverApi
                        .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _buyCompleteHandler);

                super.baseController
                        .serverApi
                        .makeRequest(ApiMethods.CHANGE_DETAILS, params, _onBuySuccess);

                _appController.loadingView.showLoader();
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        private function _buyHandler(event:BuyEvent):void
        {
            _char = event.char;
            var currency:Currency = event.currency;

            if (_canBuy(_char, currency))
                _buy(_char, currency);
            else
                (new LowMoneyController(super.baseController)).execute();
        }

        private function _buyCompleteHandler(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            super.baseController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _buyCompleteHandler);
            _labView.updateUnit();

            var item:BuyingItem = new BuyingItem();
            item.importFromChar(_char);
            super.container.addChild(new GreatBuy(item));
        }

        private function _onBuySuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
