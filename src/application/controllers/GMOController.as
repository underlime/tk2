/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.11.13
 * Time: 16:02
 */
package application.controllers
{
    import application.controllers.popup.LowMoneyController;
    import application.events.ApplicationEvent;
    import application.events.GmoEvent;
    import application.models.ModelData;
import application.models.gmo.GmoGains;
import application.models.gmo.GmoPrices;
    import application.models.user.User;
    import application.server_api.ApiMethods;
    import application.service.model.GetGmoPrices;
    import application.sound.SoundManager;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.popup.AlertWindow;
    import application.views.screen.gmo.GMOScreen;
    import application.views.screen.gmo.GmoType;

    import framework.core.helpers.MathHelper;
    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;
    import framework.core.underquery.ApiRequestEvent;
    import framework.core.utils.Singleton;

    public class GMOController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GMOController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            _pricesModel = ModelsRegistry.getModel(ModelData.GMO_PRICES) as GmoPrices;
            _gmoGains = ModelsRegistry.getModel(ModelData.GMO_GAINS) as GmoGains;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _priceTask = new GetGmoPrices(_appController);
            _priceTask.addEventListener(TaskEvent.COMPLETE, _priceLoadHandler);
            _priceTask.execute();
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;

        private var _view:GMOScreen;
        private var _priceTask:GetGmoPrices;

        private var _pricesModel:GmoPrices;
        private var _user:User;
        private var _gmoGains:GmoGains;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderView():void
        {
            _view = new GMOScreen();
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.addEventListener(GmoEvent.BUY, _buyHandler);
            super.container.addChild(_view);
        }

        private function _gmoEnabled(gmoType:GmoType):Boolean
        {
            return _gmoGains.isEnabled(gmoType);
        }

        private function _buyGmo(gmoType:GmoType, hours:int):void
        {
            var params:Object = {
                "hours": hours,
                "gain_object": gmoType.toString()
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.SET_GAIN, params, _onServerSuccess);
        }

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);

            _showSuccess();
        }

        private function _showSuccess():void
        {
            var sound:SoundManager = Singleton.getClass(SoundManager);
            sound.playUISound(UISound.SUCCESS);
            var header:String = TextFactory.instance.getLabel("success_label");
            var message:String = TextFactory.instance.getLabel("gmo_buy_msg");
            super.container.addChild(new AlertWindow(header, message));
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _priceLoadHandler(event:TaskEvent):void
        {
            _priceTask.removeEventListener(TaskEvent.COMPLETE, _priceLoadHandler);
            _priceTask.destroy();
            _renderView();
        }

        private function _buyHandler(event:GmoEvent):void
        {
            var period:int = MathHelper.clamp(_view.period, 0, 3);
            var hours:int = GmoPrices.PERIODS[period];
            var gmoType:GmoType = event.gmoType;
            var price:int = _pricesModel.getGmoPrice(gmoType, period);

            if (_user.userFightData.ferros.value >= price) {
                if (!_gmoEnabled(gmoType)) {
                    _buyGmo(gmoType, hours);
                } else {
                    var header:String = TextFactory.instance.getLabel("gmo_refuse_header");
                    var msg:String = TextFactory.instance.getLabel("gmo_refuse_msg");
                    super.container.addChild(new AlertWindow(header, msg));
                }
            } else {
                (new LowMoneyController(baseController)).execute();
            }

        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
