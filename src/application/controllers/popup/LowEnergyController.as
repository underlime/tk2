/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.09.13
 * Time: 20:23
 */
package application.controllers.popup
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.user.User;
    import application.server_api.ApiMethods;
    import application.views.popup.LowEnergyView;

    import flash.events.MouseEvent;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;

    public class LowEnergyController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LowEnergyController(baseController:IBaseController)
        {
            super(baseController);
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new LowEnergyView(_user.userOptInfo.energy_recover_ferros_price.value);

            super.container.addChild(_view);
            _view.okButton.addEventListener(MouseEvent.CLICK, _okClickHandler);
            _view.restoreButton.addEventListener(MouseEvent.CLICK, _restoreClickHandler);
        }

        override public function destroy():void
        {
            _view.okButton.removeEventListener(MouseEvent.CLICK, _okClickHandler);
            _view.restoreButton.removeEventListener(MouseEvent.CLICK, _restoreClickHandler);
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:LowEnergyView;
        private var _user:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _okClickHandler(event:MouseEvent):void
        {
            this.destroy();
        }

        private function _restoreClickHandler(event:MouseEvent):void
        {
            if (_user.userOptInfo.energy_recover_ferros_price.value > _user.userFightData.ferros.value) {
                (new LowMoneyController(super.baseController)).execute();
            }
            else {
                ApplicationController(baseController).loadingView.showLoader();
                baseController.serverApi.makeRequest(ApiMethods.USER_RECOVER_ENERGY, null, _onRecoverEnergy);
            }
        }

        private function _onRecoverEnergy(...args):Boolean
        {
            ApplicationController(baseController).loadingView.hideLoader();
            this.destroy();
            return true;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
