/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.09.13
 * Time: 14:26
 */
package application.controllers.popup
{
    import application.events.ApplicationEvent;
    import application.views.popup.LowMoneyDialog;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;

    public class LowMoneyController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LowMoneyController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new LowMoneyDialog();
            _view.addEventListener(Event.CLOSE, _closeHandler);
            _view.addEventListener(ApplicationEvent.CALL_EXPRESS_BANK, _callExpressBank);

            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:LowMoneyDialog;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(event:Event):void
        {
            this.destroy();
        }

        private function _callExpressBank(event:ApplicationEvent):void
        {
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
