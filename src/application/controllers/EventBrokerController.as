package application.controllers
{
    import application.events.ServerEvent;
    import application.helpers.ServerBroadcaster;
    import application.models.ModelData;
    import application.models.eventBroker.EventBrokerParams;
    import application.models.user.User;

    import flash.events.Event;

    import framework.core.socnet.SocNet;
    import framework.core.struct.controller.Controller;

    import io.socket.flash.ISocketIOTransport;
    import io.socket.flash.ISocketIOTransportFactory;
    import io.socket.flash.SocketIOErrorEvent;
    import io.socket.flash.SocketIOEvent;
    import io.socket.flash.SocketIOTransportFactory;
    import io.socket.flash.WebsocketTransport;

    public class EventBrokerController extends Controller
    {
        private var _params:EventBrokerParams;
        private var _userModel:User;
        private var _socketIOTransportFactory:ISocketIOTransportFactory = new SocketIOTransportFactory();
        private var _ioSocket:ISocketIOTransport;

        public function EventBrokerController(baseController:ApplicationController)
        {
            super(baseController);
            _params = baseController.dataBase.getChildByName(ModelData.EVENT_BROKER_PARAMS) as EventBrokerParams;
            _userModel = baseController.dataBase.getChildByName(ModelData.USER) as User;
        }

        override public function execute():void
        {
            super.execute();
            if (_params.host.value && _params.port.value && _params.channel.value && _params.channel.value !== "null") {
                _connect();
            } else {
                _params.addEventListener(Event.CHANGE, _onParamsChange);
            }
        }

        private function _onParamsChange(event:Event):void
        {
            if (_params.host.value && _params.port.value && _params.channel.value && _params.channel.value !== "null") {
                _params.removeEventListener(Event.CHANGE, _onParamsChange);
                _connect();
            }
        }

        private function _connect():void
        {
            var uri:String = _params.host.value + ":" + _params.port.value + "/socket.io";
            _ioSocket = _socketIOTransportFactory.createSocketIOTransport(WebsocketTransport.TRANSPORT_TYPE, uri, baseController.container);
            _ioSocket.addEventListener(SocketIOEvent.CONNECT, _onSocketConnected);
            _ioSocket.addEventListener(SocketIOEvent.DISCONNECT, _onSocketDisconnected);
            _ioSocket.addEventListener(SocketIOEvent.MESSAGE, _onSocketMessage);
            _ioSocket.addEventListener(SocketIOErrorEvent.CONNECTION_FAULT, _onSocketConnectionFault);
            _ioSocket.addEventListener(SocketIOErrorEvent.SECURITY_FAULT, _onSocketSecurityFault);
            _ioSocket.connect();
        }

        private function _onSocketMessage(event:SocketIOEvent):void
        {
            try {
                var data:Object = JSON.parse(String(event.message));
                switch (data.type) {
                    case "log_please":
                        _login();
                        break;
                    default:
                        var serverEvent:ServerEvent = new ServerEvent(data.type);
                        serverEvent.data = data.data;
                        ServerBroadcaster.instance().dispatchEvent(serverEvent);
                }
            } catch (e:SyntaxError) {
                trace("Invalid syntax:", String(event.message));
            }
        }

        private function _login():void
        {
            try {
                var message:String = JSON.stringify(_getLoginData());
                _ioSocket.send(message);
            }
            catch (e:Error) {
                _ioSocket.disconnect();
            }
        }

        private function _getLoginData():Object
        {
            var data:Object = {
                type: "login",
                data: {
                    channel: _params.channel.value,
                    userId: _userModel.userInfo.soc_net_id.value
                }
            };

            switch (SocNet.socNetType) {
                case SocNet.VK_COM:
                case SocNet.MOCK:
                    data.data["authKey"] = baseController.socNet.serverCallParams["auth_key"];
                    break;
                case SocNet.ODNOKLASSNIKI_RU:
                    data.data["authSig"] = baseController.socNet.queryParams["auth_sig"];
                    data.data["sessionKey"] = baseController.socNet.queryParams["session_key"];
                    break;
                default:
                    throw new Error("Wrong social network: " + SocNet.socNetType);
            }

            return data;
        }

        private function _onSocketConnected(event:SocketIOEvent):void
        {
            trace("Connected " + event.target);
        }

        private function _onSocketDisconnected(event:SocketIOEvent):void
        {
            trace("Disconnected " + event.target);
        }

        private function _onSocketConnectionFault(event:SocketIOErrorEvent):void
        {
            trace(event.type + ": " + event.text);
        }

        private function _onSocketSecurityFault(event:SocketIOErrorEvent):void
        {
            trace(event.type + ": " + event.text);
        }
    }
}
