package application.controllers.application
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.events.JsScrollEvent;
    import application.events.ServerEvent;
    import application.helpers.ScrollBroadcaster;
    import application.helpers.ServerBroadcaster;
    import application.models.ModelData;
    import application.models.ranks.RankData;
    import application.models.ranks.RanksModel;
    import application.models.user.User;
    import application.models.user.config.ConfigPrices;
    import application.views.screen.common.CommonData;

    import flash.external.ExternalInterface;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.service.Service;
    import framework.core.tools.SourceManager;
    import framework.core.underquery.ApiRequestEvent;
    import framework.core.utils.Singleton;

    public class ApiExporter extends Service
    {
        private static var _lockCalls:Boolean = false;

        private var _appController:ApplicationController;
        private var _rankModel:RanksModel = ModelsRegistry.getModel(ModelData.RANKS) as RanksModel;
        private var _userModel:User = ModelsRegistry.getModel(ModelData.USER) as User;

        private var _scrollBroadCaster:ScrollBroadcaster = Singleton.getClass(ScrollBroadcaster);

        public function ApiExporter(appController:ApplicationController)
        {
            _appController = appController;
        }

        public function exportApi():void
        {
            if (ExternalInterface.available) {
                //Вызовы API
                ExternalInterface.addCallback('callServerApi', _onCallServerApi);
                ExternalInterface.addCallback('callBank', _onCallBank);
                ExternalInterface.addCallback('callGuestScreen', _onCallGuestScreen);

                //Геттеры
                ExternalInterface.addCallback('getRankByGlory', _getRankByGlory);
                ExternalInterface.addCallback('getConfigPrices', _getConfigPrices);
                ExternalInterface.addCallback('getUsersMoney', _getUsersMoney);

                //События
                ExternalInterface.addCallback('onJsScroll', _onJsScroll);

                //Обратные вызовы
                ServerBroadcaster.instance().addEventListener(ServerEvent.RIVAL_ADDED, _onRivalAdded);
            }
            else {
                trace('API is not exported');
            }
        }

        private function _onRivalAdded(event:ServerEvent):void
        {
            var func:String = "function (data) { Common && Common.onRivalAdded && Common.onRivalAdded(data); }";
            ExternalInterface.call(func, event.data);
        }

        private function _onCallServerApi(method:String, params:Object = null, successCallbackName:String = null, errorCallbackName:String = null):void
        {
            if (_lockCalls)
                return;

            _appController.serverApi.makeRequest(method, params, _onServerApiSuccess, _onServerApiError);

            function _onServerApiSuccess(e:ApiRequestEvent):Boolean
            {
                if (successCallbackName)
                    return ExternalInterface.call(successCallbackName, e.data) as Boolean;
                else
                    return true;
            }

            function _onServerApiError(e:ApiRequestEvent):Boolean
            {
                if (errorCallbackName)
                    return ExternalInterface.call(errorCallbackName, e.data) as Boolean;
                else
                    return true;
            }
        }

        private function _onCallBank():void
        {
            if (!_lockCalls) {
                var appEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_BANK);
                this.dispatchEvent(appEvent);
            }
        }

        private function _onCallGuestScreen(socNetId:String):void
        {
            if (!_lockCalls) {
                var appEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_GUEST_SCREEN);
                appEvent.data = {'soc_net_id': socNetId};
                this.dispatchEvent(appEvent);
            }
        }

        private function _getRankByGlory(glory:int):Object
        {
            var rankData:RankData = _rankModel.getRankData(glory);
            var picture:String = SourceManager.instance.getBase64String(CommonData.ICON_PREFIX + rankData.picture);

            var result:Object = {
                'name': rankData.name,
                'picture_name': rankData.picture,
                'picture_data': picture
            };
            return result;
        }

        private function _getConfigPrices():Object
        {
            var configPrices:ConfigPrices = _userModel.configPrices;
            return {
                //В случае нужны, можно добавить сюда другие цены
                //для доступа из JS
                'rival_price': configPrices.rival_price.value
            };
        }

        private function _getUsersMoney():Object
        {
            return {
                'ferros': _userModel.userFightData.ferros.value,
                'tomatos': _userModel.userFightData.tomatos.value
            };
        }

        private function _onJsScroll(dir:int):void
        {
            var event:JsScrollEvent = new JsScrollEvent(JsScrollEvent.SCROLL);
            event.dir = dir;
            _scrollBroadCaster.dispatchEvent(event);
        }

        public function dispatchApiInit():void
        {
            if (ExternalInterface.available)
                ExternalInterface.call('onApplicationApiInit');
        }

        public static function get lockCalls():Boolean
        {
            return _lockCalls;
        }

        public static function set lockCalls(value:Boolean):void
        {
            if (ExternalInterface.available) {
                if (value)
                    ExternalInterface.call('Common.onLockGameApi');
                else
                    ExternalInterface.call('Common.onUnlockGameApi');
            }
            _lockCalls = value;
        }
    }
}
