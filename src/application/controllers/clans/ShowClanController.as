/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.01.14
 * Time: 13:24
 */
package application.controllers.clans
{
    import application.controllers.ApplicationController;
    import application.events.ClanEvent;
    import application.events.InfoEvent;
    import application.models.ModelData;
    import application.models.clans.ClanInfo;
    import application.models.clans.ClansRegistry;
    import application.models.opponents.OpponentsList;
    import application.models.user.User;
    import application.service.model.clans.GetFullClanInfo;
    import application.service.model.clans.LeaveClanAction;
    import application.service.model.clans.SendInviteRequest;
    import application.service.model.clans.SendItemAction;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.popup.AlertWindow;
    import application.views.popup.ConfirmWindow;
    import application.views.screen.clans.ClanUserRole;
    import application.views.screen.clans.show.ShowClanView;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;

    public class ShowClanController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShowClanController(baseController:IBaseController, clanID:int = 0)
        {
            super(baseController);
            _clanID = clanID;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (!_clanID) {
                _clanID = _user.clanInfo.id.value;
                if (!_clanID)
                    throw new Error("Clan not found!")
            }

            if (!_user.clanInfo.id.value && !_user.clanInfo.requestSent)
                _canRequest = true;

            _sendQuery();
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _clanID:int;
        private var _view:ShowClanView;
        private var _canRequest:Boolean = false;
        private var _clanInfo:ClanInfo;
        private var _user:User = ModelsRegistry.getModel(ModelData.USER) as User;
        private var _task:GetFullClanInfo;
        private var _sendRequest:SendInviteRequest;
        private var _deleteUserId:int = 0;
        private var _leaveClanAction:LeaveClanAction;

        private var _item_id:int = 0;
        private var _to_user_id:int = 0;
        private var _sendItemAction:SendItemAction;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            _task = new GetFullClanInfo(baseController as ApplicationController, _clanID);
            _task.addEventListener(TaskEvent.COMPLETE, _completeHandler);
            _task.execute();
        }

        private function _renderView():void
        {
            var localRole:ClanUserRole = ClanUserRole.GUEST;
            if (_clanID == _user.clanInfo.id.value) {
                localRole = ClanUserRole.MEMBER;
                if (_user.isClanLeader)
                    localRole = ClanUserRole.ADMIN;
            }

            _view = new ShowClanView(_clanInfo, _canRequest, localRole);
            _view.addEventListener(ClanEvent.REQUEST, _requestHandler);
            _view.addEventListener(InfoEvent.GET_INFO, _redispatchEvent);
            _view.addEventListener(ClanEvent.DELETE, _deleteUserHandler);
            _view.addEventListener(ClanEvent.SEND_ITEM, _sendItemHandler);
            super.container.addChild(_view);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeHandler(event:TaskEvent):void
        {
            _task.removeEventListener(TaskEvent.COMPLETE, _completeHandler);
            _task.destroy();

            _clanInfo = ClansRegistry.getClan(_clanID);
            if (!_clanInfo)
                throw new Error("Clan was load, but not found");
            else
                _renderView();
        }

        private function _requestHandler(event:ClanEvent):void
        {
            _user.clanInfo.requestSent = true;
            _sendRequest = new SendInviteRequest(baseController as ApplicationController, _clanID);
            _sendRequest.addEventListener(TaskEvent.COMPLETE, _sendCompleteHandler);
            _sendRequest.execute();
        }

        private function _sendCompleteHandler(event:TaskEvent):void
        {
            _sendRequest.destroy();
            var alert:AlertWindow = new AlertWindow(TextFactory.instance.getLabel("success_label"), TextFactory.instance.getLabel("send_clan_success"));
            super.container.addChild(alert);
        }

        private function _deleteUserHandler(event:ClanEvent):void
        {
            _deleteUserId = event.user_id;
            var confirm:ConfirmWindow = new ConfirmWindow(TextFactory.instance.getLabel('delete_member_confirm'), _sendDeleteQuery);
            super.container.addChild(confirm);
        }

        private function _sendDeleteQuery():void
        {
            _leaveClanAction = new LeaveClanAction(baseController as ApplicationController, _deleteUserId);
            _leaveClanAction.addEventListener(TaskEvent.COMPLETE, _deleteCompleteHandler);
            _leaveClanAction.execute();
        }

        private function _deleteCompleteHandler(event:TaskEvent):void
        {
            _leaveClanAction.destroy();
            _user.clanInfo.clan_members.deleteMember(_deleteUserId);

            var opponents:OpponentsList = ModelsRegistry.getModel(ModelData.OPPONENTS_LIST) as OpponentsList;
            opponents.deleteOpponent(_deleteUserId);

            _view.rebuild();
            _view.sound.playUISound(UISound.CANCEL);

            var alert:AlertWindow = new AlertWindow(TextFactory.instance.getLabel("success_label"), TextFactory.instance.getLabel("user_deleted"));
            super.container.addChild(alert);
        }

        private function _sendItemHandler(event:ClanEvent):void
        {
            _item_id = event.item_id;
            _to_user_id = event.user_id;
            var confirm:ConfirmWindow = new ConfirmWindow(TextFactory.instance.getLabel('send_item_confirm'), _sendItem);
            super.container.addChild(confirm);
        }

        private function _sendItem():void
        {
            _sendItemAction = new SendItemAction(baseController as ApplicationController, _item_id, _to_user_id);
            _sendItemAction.addEventListener(TaskEvent.COMPLETE, _sendItemCompleteHandler);
            _sendItemAction.execute();
        }

        private function _sendItemCompleteHandler(event:TaskEvent):void
        {
            _sendItemAction.destroy();
            var alert:AlertWindow = new AlertWindow(TextFactory.instance.getLabel("success_label"), TextFactory.instance.getLabel("item_sent_text"));
            super.container.addChild(alert);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
