/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.01.14
 * Time: 13:29
 */
package application.controllers.clans
{
    import application.controllers.ApplicationController;
    import application.controllers.popup.LowMoneyController;
    import application.events.ApplicationEvent;
    import application.events.OfficeEvent;
    import application.models.ModelData;
    import application.models.clans.ClanNameModel;
    import application.models.user.User;
    import application.server_api.ApiMethods;
    import application.service.model.clans.CheckClanName;
    import application.sound.SoundManager;
    import application.sound.lib.UISound;
    import application.views.popup.CreateClanSuccess;
    import application.views.screen.clans.create.CreateClanView;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;
    import framework.core.underquery.ApiRequestEvent;
    import framework.core.utils.Singleton;

    public class CreateClanController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CreateClanController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new CreateClanView(_user.configPrices.clan_creation_price.value);
            _view.addEventListener(OfficeEvent.CHANGE_LOGIN, _changeHandler);
            _view.addEventListener(OfficeEvent.CHECK_LOGIN, _checkHandler);
            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            _view.destroy();
            if (_checkService && !_checkService.destroyed)
                _checkService.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:CreateClanView;
        private var _checkService:CheckClanName;

        private var _loginModel:ClanNameModel;
        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        private var _appController:ApplicationController;
        private var _user:User = ModelsRegistry.getModel(ModelData.USER) as User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateData():void
        {
            if (_loginModel.correct.value == 0)
                _view.loginDeny();
            else
                if (_loginModel.exists.value > 0)
                    _view.loginDeny();
                else
                    _view.loginAllow();
            _sound.playUISound(UISound.OK);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:OfficeEvent):void
        {
            if (_user.userFightData.ferros.value >= _user.configPrices.clan_creation_price.value)
                _sendQuery(event.login);
            else
                (new LowMoneyController(baseController)).execute();
        }

        private function _sendQuery(login:String):void
        {
            var params:Object = {
                "name": login
            };

            _appController.loadingView.showLoader();
            _appController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _registerHandler);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.REGISTER_CLAN, params, _onClanRegistered);
        }

        private function _registerHandler(event:ApiRequestEvent):void
        {
            _appController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _registerHandler);
            _appController.loadingView.hideLoader();

            var createSuccess:CreateClanSuccess = new CreateClanSuccess();
            createSuccess.addEventListener(Event.CLOSE, _closeHandler);
            super.container.addChild(createSuccess);
            _sound.playUISound(UISound.SUCCESS);
        }

        private function _closeHandler(event:Event):void
        {
            dispatchEvent(new ApplicationEvent(ApplicationEvent.UPDATE));
        }

        private function _onClanRegistered(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        private function _checkHandler(event:OfficeEvent):void
        {
            _checkService = new CheckClanName(baseController as ApplicationController, event.login);
            _checkService.addEventListener(TaskEvent.COMPLETE, _checkCompleteHandler);
            _checkService.execute();
        }

        private function _checkCompleteHandler(event:TaskEvent):void
        {
            _loginModel = _checkService.clanNameModel;
            _updateData();

            _checkService.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
