/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.01.14
 * Time: 13:26
 */
package application.controllers.clans
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.events.ClanEvent;
    import application.models.ModelData;
    import application.models.user.User;
    import application.service.model.clans.DeleteClan;
    import application.service.model.clans.LeaveClan;
    import application.views.TextFactory;
    import application.views.popup.ConfirmWindow;
    import application.views.screen.clans.ClanUserRole;
    import application.views.screen.clans.edit.EditClanView;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;

    public class EditClanController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EditClanController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new EditClanView(_user.clanInfo, _user.clanRole);
            _view.addEventListener(ClanEvent.DELETE, _clanDeleteHandler);
            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:EditClanView;
        private var _user:User = ModelsRegistry.getModel(ModelData.USER) as User;

        private var _deleteClanTask:DeleteClan;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _deleteClan():void
        {
            if (_user.clanRole == ClanUserRole.ADMIN)
                _deleteClanTask = new DeleteClan(baseController as ApplicationController, _user.clanInfo);
            else
                _deleteClanTask = new LeaveClan(baseController as ApplicationController, _user.clanInfo);

            _deleteClanTask.addEventListener(TaskEvent.COMPLETE, _deleteCompleteHandler);
            _deleteClanTask.execute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clanDeleteHandler(event:ClanEvent):void
        {
            var label:String = _user.clanRole == ClanUserRole.ADMIN ? TextFactory.instance.getLabel('delete_clan_confirm') : TextFactory.instance.getLabel('leave_clan_confirm');
            var confirm:ConfirmWindow = new ConfirmWindow(label, _deleteClan);
            super.container.addChild(confirm);
        }

        private function _deleteCompleteHandler(event:TaskEvent):void
        {
            _deleteClanTask.destroy();
            dispatchEvent(new ApplicationEvent(ApplicationEvent.UPDATE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
