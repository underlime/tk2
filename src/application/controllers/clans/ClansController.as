/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.01.14
 * Time: 20:37
 */
package application.controllers.clans
{
    import application.controllers.clans.common.ClanControllers;
    import application.controllers.clans.common.ClanControllersHelper;
    import application.events.ApplicationEvent;
    import application.events.InfoEvent;
    import application.models.ModelData;
    import application.models.user.User;
    import application.views.TextFactory;
    import application.views.screen.clans.ClanUserRole;
    import application.views.screen.clans.ClansView;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.interfaces.IController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;

    public class ClansController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        /**
         *
         * @param baseController
         * @param clanID id другого клана (переход их топа)
         */
        public function ClansController(baseController:IBaseController, clanID:int = 0)
        {
            super(baseController);
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            _role = _user.clanRole;
            _clanID = clanID;
            if (_clanID == _user.clanInfo.id.value)
                _clanID = 0;
            _getControllers();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new ClansView(_role, _clanID > 0);
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.addEventListener(Event.CHANGE, _changeHandler);

            if (_clanID > 0) {
                _view.showLastTab();
            }

            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            _destroyOperateController();
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:User;
        private var _view:ClansView;
        private var _clanID:int;
        private var _role:ClanUserRole;
        private var _controllers:Array;

        private var _operateController:IController;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getControllers():void
        {
            _controllers = ClanControllersHelper.getControllers(_role);
        }

        private function _destroyOperateController():void
        {
            if (_operateController && !_operateController.destroyed)
                _operateController.destroy();
        }

        private function _showOtherClanInfo():void
        {
            _operateController = new ShowClanController(baseController, _clanID);
            _operateController.addEventListener(InfoEvent.GET_INFO, _infoHandler);
            _view.label = TextFactory.instance.getLabel("clan_hall_label");
            _operateController.execute();
        }

        private function _showMyClan(tab:int):void
        {
            if (_controllers[tab]) {
                var control:ClanControllers = _controllers[tab];
                _view.label = control.title;

                _operateController = new control.controller(baseController);
                _operateController.addEventListener(ApplicationEvent.UPDATE, _updateHandler);
                _operateController.addEventListener(InfoEvent.GET_INFO, _infoHandler);
                _operateController.execute();
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        /**
         * Пользователь создал собственный клан
         * Обновляем интерфейс
         * @param event
         */
        private function _updateHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _view.destroy();

            _role = _user.clanRole;
            _clanID = 0;
            _getControllers();
            execute();
        }

        private function _changeHandler(event:Event):void
        {
            _destroyOperateController();

            if (_view.tabID >= _controllers.length && _clanID > 0) {
                _showOtherClanInfo();
            }

            if (_view.tabID < _controllers.length && _view.tabID >= 0) {
                _showMyClan(_view.tabID);
            }
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            destroy();
        }

        private function _infoHandler(event:InfoEvent):void
        {
            var e:InfoEvent = new InfoEvent(InfoEvent.GET_INFO);
            e.soc_net_id = event.soc_net_id;
            dispatchEvent(e);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
