/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.01.14
 * Time: 13:27
 */
package application.controllers.clans
{
    import application.controllers.ApplicationController;
    import application.events.ClanEvent;
    import application.models.ModelData;
    import application.models.clans.ClanRequests;
    import application.server_api.ApiMethods;
    import application.service.model.clans.ClanRequestResolution;
    import application.service.model.clans.GetClanRequests;
    import application.service.model.clans.GetRequestsSocialInfo;
    import application.views.TextFactory;
    import application.views.popup.ConfirmWindow;
    import application.views.screen.clans.log.CLanLogView;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;

    import mx.utils.ObjectUtil;

    public class ClanLogController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanLogController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _getRequests = new GetClanRequests(baseController as ApplicationController);
            _getRequests.addEventListener(TaskEvent.COMPLETE, _onRequests);
            _getRequests.execute();
        }

        override public function destroy():void
        {
            if (_view && !_view.destroyed) {
                _view.destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:CLanLogView;
        private var _getRequests:GetClanRequests;
        private var _getRequestsSocialInfo:GetRequestsSocialInfo;
        private var _requests:ClanRequests = ModelsRegistry.getModel(ModelData.CLAN_REQUESTS) as ClanRequests;

        private var _user_id:int = 0;
        private var _resolution:Boolean = false;

        private var _requestSender:ClanRequestResolution;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderView():void
        {
            _view = new CLanLogView(_requests);
            _view.addEventListener(ClanEvent.RESOLUTION, _resolutionHandler);
            super.container.addChild(_view);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onRequests(event:TaskEvent):void
        {
            if (_getRequests && !_getRequests.destroyed) {
                _getRequests.destroy();
            }

            _getRequestsSocialInfo = new GetRequestsSocialInfo(baseController as ApplicationController);
            _getRequestsSocialInfo.addEventListener(TaskEvent.COMPLETE, _onSocialInfo);
            _getRequestsSocialInfo.execute();
        }

        private function _onSocialInfo(event:TaskEvent):void
        {
            if (_getRequestsSocialInfo && _getRequestsSocialInfo.destroyed) {
                _getRequestsSocialInfo.destroy();
            }
            _renderView();
        }

        private function _resolutionHandler(event:ClanEvent):void
        {
            _user_id = event.user_id;
            _resolution = event.resolution;

            var confirmMsg:String = event.resolution ? TextFactory.instance.getLabel('confirm_add_req') : TextFactory.instance.getLabel('confirm_reject_req');
            var confirm:ConfirmWindow = new ConfirmWindow(confirmMsg, _sendQuery);
            super.container.addChild(confirm);
        }

        private function _sendQuery():void
        {
            var action:String = _resolution ? ApiMethods.CLAN_ACCEPT_REQUEST : ApiMethods.CLAN_REJECT_REQUEST;
            _requestSender = new ClanRequestResolution(baseController as ApplicationController, action, _user_id);
            _requestSender.addEventListener(TaskEvent.COMPLETE, _requestComplete);
            _requestSender.execute();
        }

        private function _requestComplete(event:TaskEvent):void
        {
            if (_requestSender && !_requestSender.destroyed) {
                _requestSender.destroy();
            }
            _view.addResolution(_user_id, _resolution);
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
