/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.01.14
 * Time: 13:30
 */
package application.controllers.clans.common
{
    import application.controllers.clans.ClanLogController;
    import application.controllers.clans.CreateClanController;
    import application.controllers.clans.EditClanController;
    import application.controllers.clans.ShowClanController;
    import application.views.TextFactory;
    import application.views.screen.clans.ClanUserRole;

    public class ClanControllersHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static var l10n:TextFactory = TextFactory.instance;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        public static function getControllers(role:ClanUserRole):Array
        {
            switch (role) {
                case ClanUserRole.ADMIN:
                    return _getAdminControllers();
                case ClanUserRole.MEMBER:
                    return _getMemberControllers();
                case ClanUserRole.GUEST:
                    return _getGuestControllers();

            }

            return _getGuestControllers();
        }

        private static function _getAdminControllers():Array
        {
            return [
                new ClanControllers(ShowClanController, l10n.getLabel("clan_hall_label")),
                new ClanControllers(EditClanController, l10n.getLabel("edit_clan_label")),
                new ClanControllers(ClanLogController, l10n.getLabel("log_clan_label"))
            ]
        }

        private static function _getMemberControllers():Array
        {
            return [
                new ClanControllers(ShowClanController, l10n.getLabel("clan_hall_label")),
                new ClanControllers(EditClanController, l10n.getLabel("edit_clan_label"))
            ]
        }

        private static function _getGuestControllers():Array
        {
            return [
                new ClanControllers(CreateClanController, l10n.getLabel("create_clan_label"))
            ]

        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
