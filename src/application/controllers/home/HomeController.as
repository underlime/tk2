/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.06.13
 * Time: 10:32
 */
package application.controllers.home
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.helpers.PostBgHelper;
    import application.helpers.WallPicturesRegistry;
    import application.models.ModelData;
    import application.models.user.User;
    import application.views.TextFactory;
    import application.views.screen.home.HomeScreen;

    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.core.error.ApplicationError;
    import framework.core.events.GameEvent;
    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;
    import framework.core.tools.wall_post.WallPostTool;

    public class HomeController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HomeController(baseController:IBaseController)
        {
            super(baseController);
            _appController = ApplicationController(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupHash();
            _setupView();
        }

        override public function destroy():void
        {
            if (_homeScreen && !_homeScreen.destroyed)
                _homeScreen.destroy();

            _destroyOperateController();
            _destroyWallPostTool();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _homeScreen:HomeScreen;
        private var _hash:Object;

        private var _operateController:Controller;
        private var _appController:ApplicationController;
        private var _wallPostTool:WallPostTool;

        private var _userModel:User = ModelsRegistry.getModel(ModelData.USER) as User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupHash():void
        {
            _hash = {
                "0": {
                    "controller": InventoryController,
                    "label": TextFactory.instance.getLabel('inventory_label')
                },
                "1": {
                    "controller": UserInfoController,
                    "label": TextFactory.instance.getLabel('user_info_label')
                },
                "2": {
                    "controller": AchievementsController,
                    "label": TextFactory.instance.getLabel('achievements_label')
                },
                "3": {
                    "controller": SpecialsController,
                    "label": TextFactory.instance.getLabel('skills_label')
                }
            };
        }

        private function _setupView():void
        {
            _homeScreen = new HomeScreen();
            _homeScreen.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _homeScreen.addEventListener(Event.CHANGE, _tabsChangeHandler);

            super.container.addChild(_homeScreen);
        }

        private function _destroyOperateController():void
        {
            if (_operateController && !_operateController.destroyed)
                _operateController.destroy();
        }

        private function _destroyWallPostTool():void
        {
            if (_wallPostTool && !_wallPostTool.destroyed) {
                _wallPostTool.destroy();
                _wallPostTool = null;
            }
        }

        private function _shareUserInfo(event:ApplicationEvent):void
        {
            _appController.loadingView.showLoader();

            _destroyWallPostTool();
            _wallPostTool = new WallPostTool();

            switch (_homeScreen.tabs.selectedTab) {
                case 1:
                    _setStatisticsPostParams();
                    break;
                case 2:
                    _setAchievementsPostParams();
                    break;
                case 3:
                    _setSpecialsPostParams();
                    break;
                default:
                    throw new Error('Wrong tab');
            }

            _wallPostTool.addEventListener(Event.COMPLETE, _onPostComplete);
            _wallPostTool.addEventListener(ErrorEvent.ERROR, _onPostError);
            _wallPostTool.addEventListener(GameEvent.ALBUM_CREATED, _appController.onApplicationAlbumCreated);
            _wallPostTool.addPost();
        }

        private function _onPostComplete(e:Event):void
        {
            _appController.loadingView.hideLoader();
        }

        private function _onPostError(e:ErrorEvent):void
        {
            _appController.loadingView.hideLoader();
            var appError:ApplicationError = new ApplicationError();
            appError.fatal = false;
            appError.message = e.text;
            _appController.appError(appError);
        }

        private function _setStatisticsPostParams():void
        {
            _wallPostTool.postText = TextFactory.instance.getLabel("my_statistics_label");
            _wallPostTool.bitmapImage = PostBgHelper.buildPicture(_getInfoAreaBitmap());
        }

        private function _setAchievementsPostParams():void
        {
            _wallPostTool.postText = TextFactory.instance.getLabel("my_achievements_label")
                .replace('{count}', _userModel.userAchievements.length)
                .replace('{max_count}', _userModel.userOptInfo.achievements_count.value);
            _wallPostTool.pictureSocNetId = WallPicturesRegistry.achievement;
        }

        private function _setSpecialsPostParams():void
        {
            _wallPostTool.postText = TextFactory.instance.getLabel("my_specials_label")
                .replace('{count}', _userModel.userSpecials.length)
                .replace('{max_count}', _userModel.userOptInfo.specials_count.value);
            _wallPostTool.pictureSocNetId = WallPicturesRegistry.special;
        }

        private function _getInfoAreaBitmap():Bitmap
        {
            return View(super.container).getAsBitmap(45, 215, 386, 256);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _tabsChangeHandler(event:Event):void
        {
            var tabID:int = _homeScreen.tabs.selectedTab;
            if (_hash[tabID] && _hash[tabID]) {
                _destroyOperateController();

                var ControllerClass:Class = _hash[tabID]['controller'];
                var label:String = _hash[tabID]['label'];
                if (_homeScreen)
                    _homeScreen.label = label;
                _operateController = new ControllerClass(super.baseController);
                _operateController.addEventListener(ApplicationEvent.SHARE, _shareUserInfo);
                _operateController.execute();
            }
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
