/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.08.13
 * Time: 10:52
 */
package application.controllers.home
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.inventory.Inventory;
    import application.models.user.User;
    import application.views.screen.user_info.UserInfoView;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;

    public class UserInfoController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserInfoController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            var inventory:Inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;

            _userInfoScreen = new UserInfoView(_user, inventory);
            _userInfoScreen.addEventListener(ApplicationEvent.SHARE, _redispatchEvent);
            super.container.addChild(_userInfoScreen);
        }

        override public function destroy():void
        {
            _userInfoScreen.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _userInfoScreen:UserInfoView;
        private var _user:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
