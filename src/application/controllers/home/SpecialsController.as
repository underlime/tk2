/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.08.13
 * Time: 10:34
 */
package application.controllers.home
{
    import application.controllers.*;
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.inventory.Inventory;
    import application.models.user.User;
    import application.models.user.specials.Specials;
    import application.server_api.ApiMethods;
    import application.views.screen.specials.SpecialsScreen;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.underquery.ApiRequestEvent;

    public class SpecialsController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialsController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            _model = _user.userSpecials;
            _inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;

            if (_model.isEmpty)
                _loadData();
            else
                _renderView();
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:SpecialsScreen;

        private var _appController:ApplicationController;
        private var _user:User;
        private var _model:Specials;

        private var _inventory:Inventory;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _loadData():void
        {
            var params:Object = {
                "user_id": _user.userInfo.user_id.value
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onImport);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.GET_SPECIALS, params, _onServerSuccess);
        }

        private function _renderView():void
        {
            _view = new SpecialsScreen(_user, _inventory);
            _view.addEventListener(ApplicationEvent.SHARE, _redispatchEvent);
            super.container.addChild(_view);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onImport(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onImport);
            _renderView();
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            trace("success");
            return true;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
