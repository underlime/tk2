/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.06.13
 * Time: 10:16
 */
package application.controllers.home
{
    import application.controllers.*;
    import application.controllers.popup.LowMoneyController;
    import application.events.ApplicationEvent;
    import application.events.InventoryEvent;
    import application.events.SlotEvent;
    import application.models.ModelData;
    import application.models.inventory.InventoryHelper;
    import application.models.market.Item;
    import application.models.user.User;
    import application.server_api.ApiMethods;
    import application.sound.SoundManager;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.popup.BuyAlert;
    import application.views.screen.inventory.InventoryScreen;
    import application.views.screen.inventory.slots.user.UserConsumeSlot;
    import application.views.screen.inventory.slots.user.UserRingSlot;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.underquery.ApiRequestEvent;
    import framework.core.utils.Singleton;

    public class InventoryController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InventoryController(baseController:IBaseController)
        {
            super(baseController);
            _appController = super.baseController as ApplicationController;
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupView();
        }

        override public function destroy():void
        {
            if (_inventoryScreen && !_inventoryScreen.destroyed)
                _inventoryScreen.destroy();
            _destroyListeners();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _inventoryScreen:InventoryScreen;
        private var _appController:ApplicationController;
        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        private var _user:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupView():void
        {
            _inventoryScreen = new InventoryScreen();
            _inventoryScreen.addEventListener(ApplicationEvent.GENERATE_AVATAR, _avatarHandler);
            _inventoryScreen.addEventListener(InventoryEvent.SELL_ITEM, _sellHandler);
            _inventoryScreen.addEventListener(InventoryEvent.APPLY_ITEM, _applyHandler);
            _inventoryScreen.addEventListener(InventoryEvent.PUT_ITEMS, _putHandler);

            _inventoryScreen.addEventListener(SlotEvent.UNLOCK, _unlockHandler);

            super.container.addChild(_inventoryScreen);
        }

        private function _destroyListeners():void
        {
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onSellComplete);

            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApplyComplete);

            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onPutComplete);
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onUnlockComplete);
        }

        private function _unlockHandler(event:SlotEvent):void
        {
            var factory:TextFactory = TextFactory.instance;
            var buyDialog:BuyAlert;

            if (event.target is UserRingSlot) {
                buyDialog = new BuyAlert(
                        factory.getLabel('unlock_ring_header'),
                        factory.getLabel('unlock_ring_message'),
                        _user.configPrices.ring_slot_ferros_price.value,
                        _unlockRing
                );
                super.container.addChild(buyDialog);
            }

            if (event.target is UserConsumeSlot) {
                buyDialog = new BuyAlert(
                        factory.getLabel('unlock_item_slot'),
                        factory.getLabel('unlock_item_message'),
                        _user.configPrices.item_slot_ferros_price.value,
                        _unlockConsume
                );
                super.container.addChild(buyDialog);
            }

        }

        private function _unlockRing():void
        {
            if (_user.userFightData.ferros.value < _user.configPrices.ring_slot_ferros_price.value) {
                (new LowMoneyController(baseController)).execute();
                return;
            }

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onUnlockComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.BUY_RING_SLOT, {}, _onServerSuccess);
        }

        private function _unlockConsume():void
        {
            if (_user.userFightData.ferros.value < _user.configPrices.item_slot_ferros_price.value) {
                (new LowMoneyController(baseController)).execute();
                return;
            }

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onUnlockComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.BUY_CONSUME_SLOT, {}, _onServerSuccess);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _sellHandler(event:InventoryEvent):void
        {
            var item:Item = event.item;
            var params:Object = {
                "item_id": item.item_id.value,
                "count": 1
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onSellComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.SELL_ITEM, params, _onServerSuccess);
        }

        private function _applyHandler(event:InventoryEvent):void
        {
            var item:Item = event.item;
            var params:Object = {
                "item_id": item.item_id.value
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApplyComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.APPLY_ITEM, params, _onServerSuccess);
        }

        private function _putHandler(event:InventoryEvent):void
        {
            var put_list:Array = InventoryHelper.getItemIdCount(event.data['put_list'] ? event.data['put_list'] : []);
            var social_put_list:Array = InventoryHelper.getItemIdCount(event.data['social_put_list'] ? event.data['social_put_list'] : []);

            var params:Object = {

            };

            for (var key:String in put_list) {
                params["put_list[" + key + "]"] = put_list[key];
            }

            for (key in social_put_list) {
                params["social_put_list[" + key + "]"] = social_put_list[key];
            }

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onPutComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.PUT_ITEMS, params, _onServerSuccess);
        }

        private function _onSellComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _destroyListeners();
            _inventoryScreen.update();
            _sound.playUISound(UISound.ITEM_SALE);
        }

        private function _onApplyComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _destroyListeners();
            _inventoryScreen.update();
            _sound.playUISound(UISound.ITEM_DRINK);
        }

        private function _onPutComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _destroyListeners();
            _sound.playUISound(UISound.EQUIP_ITEMS);
            trace("состояние сохранено");
        }

        private function _avatarHandler(event:ApplicationEvent):void
        {
            trace("сделать аватар");
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            trace("success");
            return true;
        }

        private function _onUnlockComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _destroyListeners();
            _inventoryScreen.destroy();
            _setupView();
            _sound.playUISound(UISound.CASH);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
