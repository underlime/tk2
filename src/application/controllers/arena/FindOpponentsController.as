/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.07.13
 * Time: 15:46
 */
package application.controllers.arena
{
    import application.controllers.*;
    import application.events.ArenaEvent;
    import application.events.BattleEvent;
    import application.events.InfoEvent;
    import application.models.ModelData;
    import application.models.user.User;
    import application.models.user.UserLevelParams;
    import application.server_api.ApiMethods;
    import application.views.screen.arena.find.FindOpponentsScreen;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.underquery.ApiRequestEvent;

    public class FindOpponentsController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FindOpponentsController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
            var model:User = ModelsRegistry.getModel(ModelData.USER) as User;
            _userLevelParams = model.getChildByName(ModelData.USER_LEVEL_PARAMS) as UserLevelParams;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _params = {
                "level": _userLevelParams.max_level.value
            };

            _renderView();
            _getOpponents();
        }

        override public function destroy():void
        {
            if (_view && !_view.destroyed)
                _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:FindOpponentsScreen;
        private var _appController:ApplicationController;
        private var _userLevelParams:UserLevelParams;

        private var _apiMethod:String = ApiMethods.OPPONENTS_BY_LEVEL;
        private var _params:Object = {};

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getOpponents():void
        {
            _view.blockSearch = true;
            _appController.loadingView.showLoader();
            _appController.serverApi.makeRequest(_apiMethod, _params, _onServerSuccess);
        }

        private function _renderView():void
        {
            _view = new FindOpponentsScreen();
            _view.addEventListener(BattleEvent.CALL_BATTLE, _callBattleHandler);
            _view.addEventListener(InfoEvent.GET_INFO, _getInfoHandler);
            _view.addEventListener(ArenaEvent.GET_BY_LEVEL, _refreshByLevel);
            _view.addEventListener(ArenaEvent.GET_BY_IDENTITY, _refreshByIdentity);
            super.container.addChild(_view);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onImport(event:ApiRequestEvent):void
        {
            _appController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onImport);
            _view.update();
            _view.blockSearch = false;
            _appController.loadingView.hideLoader();
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onImport);
            return true;
        }

        private function _callBattleHandler(e:BattleEvent):void
        {

            var event:BattleEvent = e.clone() as BattleEvent;
            if (e.enemyRecord.user_info.user_id.value > 0) {
                _appController.loadingView.showLoader();
                _appController.socNet.getUserInfo(e.enemyRecord.user_info.soc_net_id.value, _onEnemyInfo);
            }
            else {
                var robotData:Object = {
                    "first_name": "Alan",
                    "last_name": "Smithee",
                    "picture": "http://tomato-kombat.webkraken.ru/assets/ROBOT_PIC_50x50.png",
                    "photo_medium": "http://tomato-kombat.webkraken.ru/assets/ROBOT_PIC_128x128.png",
                    "photo_big": "http://tomato-kombat.webkraken.ru/assets/ROBOT_PIC_128x128.png"
                };
                event.enemyRecord.soc_net_user_info.importData(robotData);
                dispatchEvent(event);
            }

            function _onEnemyInfo(data:Object):void
            {
                event.enemyRecord.soc_net_user_info.importData(data["user_info"]);
                _appController.loadingView.hideLoader();
                dispatchEvent(event);
            }
        }

        private function _getInfoHandler(event:InfoEvent):void
        {
            dispatchEvent(event);
        }

        private function _refreshByLevel(event:ArenaEvent):void
        {
            _apiMethod = ApiMethods.OPPONENTS_BY_LEVEL;
            _params = {
                "level": event.level
            };

            _getOpponents();
        }

        private function _refreshByIdentity(event:ArenaEvent):void
        {
            _apiMethod = ApiMethods.OPPONENTS_BY_IDENTITY;
            _params = {
                "identity": event.identity
            };

            _getOpponents();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
