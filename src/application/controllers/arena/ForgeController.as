/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.12.13
 * Time: 20:32
 */
package application.controllers.arena
{
    import application.common.Currency;
    import application.controllers.ApplicationController;
    import application.controllers.popup.LowMoneyController;
    import application.events.BuyEvent;
    import application.models.ModelData;
    import application.models.forge.ForgeParams;
    import application.models.inventory.InventoryItem;
    import application.models.user.User;
    import application.service.model.GetUpgradeParams;
    import application.service.model.SharpenWeapon;
    import application.views.screen.arena.forge.ForgeResultScreen;
    import application.views.screen.arena.forge.ForgeScreen;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;

    public class ForgeController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ForgeController(baseController:IBaseController)
        {
            super(baseController);
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new ForgeScreen();
            _view.addEventListener(Event.SELECT, _itemSelectHandler);
            _view.addEventListener(BuyEvent.BUY, _buyHandler);
            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            _view.removeEventListener(Event.SELECT, _itemSelectHandler);
            _view.removeEventListener(BuyEvent.BUY, _buyHandler);

            if (_paramsTask && !_paramsTask.destroyed)
                _paramsTask.destroy();

            if (_sharpenTask && !_sharpenTask.destroyed)
                _sharpenTask.destroy();

            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:ForgeScreen;
        private var _paramsTask:GetUpgradeParams;

        private var _sharpenTask:SharpenWeapon;
        private var _currentForgeParams:ForgeParams;

        private var _currentItem:InventoryItem;
        private var _user:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _itemSelectHandler(event:Event):void
        {
            if (_paramsTask && !_paramsTask.destroyed)
                _paramsTask.destroy();
            _paramsTask = new GetUpgradeParams(baseController as ApplicationController, _view.item);
            _paramsTask.addEventListener(TaskEvent.COMPLETE, _paramsReadyHandler);
            _paramsTask.execute();
        }

        private function _paramsReadyHandler(event:TaskEvent):void
        {
            _paramsTask.removeEventListener(TaskEvent.COMPLETE, _paramsReadyHandler);
            _currentForgeParams = _paramsTask.forgeParams;
            _view.addBuyScreen(_currentForgeParams);
        }

        private function _buyHandler(event:BuyEvent):void
        {
            _currentItem = event.item as InventoryItem;
            if (_canSharpen(event.currency))
                _sharpenWeapon(event.currency);
        }

        private function _canSharpen(currency:Currency):Boolean
        {
            if (currency == Currency.FERROS) {
                if (_user.userFightData.ferros.value < _currentForgeParams.price_ferros.value) {
                    (new LowMoneyController(baseController).execute());
                    return false;
                }
            }

            if (currency == Currency.TOMATOS) {
                if (_user.userFightData.tomatos.value < _currentForgeParams.price_tomatos.value) {
                    (new LowMoneyController(baseController).execute());
                    return false;
                }
            }

            return true;
        }

        private function _sharpenWeapon(currency:Currency):void
        {
            if (_sharpenTask && !_sharpenTask.destroyed)
                _sharpenTask.destroy();

            _sharpenTask = new SharpenWeapon(
                    baseController as ApplicationController,
                    _currentItem.item_id.value,
                    currency
            );

            _sharpenTask.addEventListener(TaskEvent.COMPLETE, _sharpenCompleteHandler);
            _sharpenTask.execute();
        }

        private function _sharpenCompleteHandler(event:TaskEvent):void
        {
            _sharpenTask.removeEventListener(TaskEvent.COMPLETE, _sharpenCompleteHandler);
            super.container.addChild(new ForgeResultScreen(_currentItem, _sharpenTask.success));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
