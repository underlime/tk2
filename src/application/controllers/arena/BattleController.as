/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.07.13
 * Time: 11:18
 */
package application.controllers.arena
{
    import application.controllers.*;
    import application.controllers.application.ApiExporter;
    import application.controllers.popup.LowMoneyController;
    import application.events.ApplicationEvent;
    import application.events.BattleEvent;
    import application.helpers.WallPicturesRegistry;
    import application.models.ModelData;
    import application.models.arena.EnemyRecord;
    import application.models.battle.Battle;
    import application.models.inventory.Inventory;
    import application.models.inventory.InventoryHelper;
    import application.models.market.Item;
    import application.models.user.SocNetUserInfo;
    import application.models.user.User;
    import application.models.user.UserCommonData;
    import application.models.user.UserFightData;
    import application.models.user.UserInfo;
    import application.models.user.UserLevelParams;
    import application.service.model.GetBattle;
    import application.views.TextFactory;
    import application.views.battle.BattleView;
    import application.views.constructor.builders.BattleUnit;
    import application.views.versus.VersusScreen;
    import application.views.versus.profit.ProfitScreen;

    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.core.error.ApplicationError;
    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;
    import framework.core.tools.SourceManager;
    import framework.core.tools.wall_post.WallPostTool;

    public class BattleController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const DEFAULT_ARENA:String = "ColosseumDay";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleController(baseController:IBaseController, enemyRecord:EnemyRecord)
        {
            super(baseController);
            _enemyRecord = enemyRecord;
            _enemy_id = enemyRecord.user_info.user_id.value;
            _appController = baseController as ApplicationController;
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            _inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;
            _battleModel = ModelsRegistry.getModel(ModelData.BATTLE_MODEL) as Battle;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_enemy_id == _user.userInfo.user_id.value) {
                _showOpponentError(); // TODO исправить костыль
            }

            ApiExporter.lockCalls = true;

            _appController.eventsController.buffered = true;
            _battleGetter = new GetBattle(super.baseController as ApplicationController, _enemy_id);

            _setupData();
            _setupUnits();
            _addVersusScreen();
        }

        override public function destroy():void
        {
            _unbindProfitReactions();
            _destroyScreens();
            _battleGetter.destroy();
            _unit.destroy();
            _enemy.destroy();
            super.destroy();
            ApiExporter.lockCalls = false;
            _appController.eventsController.buffered = false;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _battleView:BattleView;
        private var _enemy_id:int;
        private var _appController:ApplicationController;
        private var _user:User;
        private var _battleModel:Battle;
        private var _enemyRecord:EnemyRecord;

        private var _unit:BattleUnit;
        private var _enemy:BattleUnit;

        private var _userCommonData:UserCommonData;
        private var _enemyCommonData:UserCommonData;
        private var _versusScreen:VersusScreen;

        private var _arena:Bitmap;
        private var _inventory:Inventory;

        private var _profitScreen:ProfitScreen;

        private var _isQuickBattle:Boolean = false;

        private var _battleGetter:GetBattle;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _showOpponentError():void
        {
            var error:ApplicationError = new ApplicationError();
            error.code = 403;
            _appController.appError(error);
        }

        private function _setupData():void
        {
            var userInfo:UserInfo = _user.getChildByName(ModelData.USER_INFO) as UserInfo;
            var socNetUserInfo:SocNetUserInfo = _user.socNetUserInfo;
            var userLevelParams:UserLevelParams = _user.getChildByName(ModelData.USER_LEVEL_PARAMS) as UserLevelParams;
            var userFightData:UserFightData = _user.getChildByName(ModelData.USER_FIGHT_DATA) as UserFightData;
            _userCommonData = new UserCommonData();
            _userCommonData.importData(userInfo, socNetUserInfo, userFightData, userLevelParams);
            _userCommonData.bonus_hp = _user.userBonus.scale.hp.value;

            _enemyCommonData = new UserCommonData();
            _enemyCommonData.importData(
                    _enemyRecord.user_info,
                    _enemyRecord.soc_net_user_info,
                    _enemyRecord.user_fight_data,
                    _enemyRecord.user_level_params
            );
            _enemyCommonData.bonus_hp = _enemyRecord.bonus_hp.value;
        }

        private function _addVersusScreen():void
        {
            _unit.toDefaultState();
            _enemy.toDefaultState();

            _versusScreen = new VersusScreen(_unit, _enemy, _userCommonData, _enemyCommonData);
            _versusScreen.addEventListener(BattleEvent.CALL_BATTLE, _battleHandler);
            _versusScreen.addEventListener(BattleEvent.EASE, _easeBattleHandler);
            _versusScreen.addEventListener(BattleEvent.QUICK_BATTLE, _quickBattleHandler);
            super.container.addChild(_versusScreen);

            if (_user.userOptInfo.energy_for_fight.value > _user.userFightData.energy.value) {
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.LOW_ENERGY));
            }
        }

        private function _loadData(ease:Boolean = false):void
        {
            _battleGetter.ease = ease;
            _battleGetter.addEventListener(TaskEvent.COMPLETE, _onBattleReady);
            _battleGetter.execute();
        }

        private function _initBattle():void
        {
            _setupArena();
            _startBattle();
        }

        private function _setupUnits():void
        {
            var userInfo:UserInfo = _user.userInfo;
            var userInventory:Inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;
            var items:Vector.<Item> = InventoryHelper.ItemsArray2Vector(userInventory.getBuildingItems());

            _unit = new BattleUnit(userInfo, items);

            var enemyInfo:UserInfo = _enemyRecord.user_info;
            var enemyInventory:Inventory = _enemyRecord.user_inventory;
            var enemyItems:Vector.<Item> = InventoryHelper.ItemsArray2Vector(enemyInventory.getBuildingItems());

            _enemy = new BattleUnit(enemyInfo, enemyItems);
        }

        private function _setupArena():void
        {
            if (SourceManager.instance.has(_battleModel.arena_data.image.value))
                _arena = SourceManager.instance.getBitmap(_battleModel.arena_data.image.value);
            else
                _arena = SourceManager.instance.getBitmap(DEFAULT_ARENA);
        }

        private function _startBattle():void
        {
            var userInfo:UserInfo = _user.getChildByName(ModelData.USER_INFO) as UserInfo;
            var userWins:Boolean = userInfo.user_id.value == _battleModel.wins.value;

            _userCommonData = new UserCommonData();
            _userCommonData.importData(_user.userInfo, _user.socNetUserInfo, _user.userFightData,
                    _user.userLevelParams);

            // добавляем бонусное здоровье для пользователя
            _userCommonData.bonus_hp = _user.userBonus.scale.hp.value;

            _enemyCommonData = new UserCommonData();
            _enemyCommonData.importData(_battleModel.enemy_info, _enemyRecord.soc_net_user_info,
                    _battleModel.enemy_fight_data, _battleModel.enemy_level_params);

            // для соперника не нужно добавлять. На сервере все учтено

            if (_unit)
                _unit.destroy();
            if (_enemy)
                _enemy.destroy();

            _unit = new BattleUnit(_user.userInfo, InventoryHelper.ItemsArray2Vector(_inventory.getBuildingItems()));
            _enemy = new BattleUnit(_battleModel.enemy_info,
                    InventoryHelper.ItemsArray2Vector(_battleModel.enemy_inventory.getBuildingItems()));

            _unit.defaultHealth = _user.userFightData.hp.value;
            _enemy.defaultHealth = _battleModel.enemy_fight_data.hp.value;

            _battleView =
                    new BattleView(_battleModel.battle_steps, _userCommonData, _enemyCommonData, _battleModel.profit, _unit,
                            _enemy, _arena, userWins);
            _battleView.addEventListener(Event.COMPLETE, _battleCompleteHandler);

            super.container.addChild(_battleView);
        }

        private function _bindProfitReactions():void
        {
            _profitScreen.addEventListener(BattleEvent.BATTLE_AGAIN, _againHandler);
            _profitScreen.addEventListener(BattleEvent.CALL_MAP, _mapHandler);
            _profitScreen.addEventListener(BattleEvent.SHARE, _shareHandler);
            _profitScreen.addEventListener(ApplicationEvent.CALL_ARENA, _callArenaHandler);
        }

        private function _unbindProfitReactions():void
        {
            if (_profitScreen) {
                _profitScreen.removeEventListener(BattleEvent.BATTLE_AGAIN, _againHandler);
                _profitScreen.removeEventListener(BattleEvent.CALL_MAP, _mapHandler);
                _profitScreen.removeEventListener(BattleEvent.SHARE, _shareHandler);
                _profitScreen.removeEventListener(ApplicationEvent.CALL_ARENA, _callArenaHandler);
            }
        }

        private function _unbindVersusScreen():void
        {
            _versusScreen.removeEventListener(BattleEvent.CALL_BATTLE, _battleHandler);
            _versusScreen.removeEventListener(BattleEvent.QUICK_BATTLE, _quickBattleHandler);
            _versusScreen.removeEventListener(BattleEvent.EASE, _easeBattleHandler);
        }

        private function _addProfitScreen():void
        {
            var userInfo:UserInfo = _user.getChildByName(ModelData.USER_INFO) as UserInfo;
            var userWins:Boolean = userInfo.user_id.value == _battleModel.wins.value;

            _profitScreen = new ProfitScreen(_battleModel.profit, userWins);
            _bindProfitReactions();
            super.container.addChild(_profitScreen);
        }

        private function _addBattle():void
        {
            _unbindVersusScreen();
            _versusScreen.destroy();
            _initBattle();
        }

        private function _addQuickBattle():void
        {
            var userInfo:UserInfo = _user.getChildByName(ModelData.USER_INFO) as UserInfo;
            var userWins:Boolean = userInfo.user_id.value == _battleModel.wins.value;

            if (userWins) {
                _unit.win();
                _enemy.ko();
            }
            else {
                _unit.ko();
                _enemy.win();
            }
            _addProfitScreen();
        }

        private function _destroyScreens():void
        {
            if (_battleView && !_battleView.destroyed)
                _battleView.destroy();
            if (_profitScreen && !_profitScreen.destroyed)
                _profitScreen.destroy();
            if (_versusScreen && !_versusScreen.destroyed)
                _versusScreen.destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _battleHandler(event:BattleEvent):void
        {
            _isQuickBattle = false;
            _unbindVersusScreen();
            _loadData();
        }

        private function _easeBattleHandler(event:BattleEvent):void
        {
            if (_user.configPrices.ease_enemy_ferros_price.value > _user.userFightData.ferros.value) {
                (new LowMoneyController(super.baseController)).execute();
            }
            else {
                _isQuickBattle = false;
                _unbindVersusScreen();
                _loadData(true);
            }
        }

        private function _onBattleReady(event:TaskEvent):void
        {
            _battleGetter.removeEventListener(TaskEvent.COMPLETE, _onBattleReady);

            if (_isQuickBattle)
                _addQuickBattle();
            else
                _addBattle();
        }

        private function _quickBattleHandler(event:BattleEvent):void
        {
            _isQuickBattle = true;
            _unbindVersusScreen();
            _loadData();
        }

        private function _battleCompleteHandler(event:Event):void
        {
            _battleView.removeEventListener(Event.COMPLETE, _battleCompleteHandler);
            _addProfitScreen();
        }

        private function _mapHandler(event:BattleEvent):void
        {
            super.dispatchEvent(new BattleEvent(BattleEvent.CALL_MAP));
        }

        private function _callArenaHandler(event:ApplicationEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_ARENA));
        }

        private function _shareHandler(event:BattleEvent):void
        {
            _appController.loadingView.showLoader();
            var wallPostTool:WallPostTool = new WallPostTool();
            wallPostTool.postText = TextFactory.instance.getLabel('share_win_label')
                    .replace('{login}', _enemyRecord.user_info.login.value);
            wallPostTool.pictureSocNetId = WallPicturesRegistry.victory;
            wallPostTool.addEventListener(Event.COMPLETE, _onWallPostSuccess);
            wallPostTool.addEventListener(ErrorEvent.ERROR, _onWallPostError);
            wallPostTool.addPost();
        }

        private function _onWallPostSuccess(e:Event):void
        {
            _appController.loadingView.hideLoader();
            e.target.destroy();
        }

        private function _onWallPostError(e:ErrorEvent):void
        {
            _appController.loadingView.hideLoader();
            e.target.destroy();

            var appError:ApplicationError = new ApplicationError();
            appError.fatal = false;
            appError.message = e.text;
            _appController.appError(appError);
        }

        private function _againHandler(event:BattleEvent):void
        {
            _destroyScreens();
            _addVersusScreen();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
