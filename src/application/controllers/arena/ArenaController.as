/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.12.13
 * Time: 19:59
 */
package application.controllers.arena
{
    import application.events.ApplicationEvent;
    import application.events.BattleEvent;
    import application.events.InfoEvent;
    import application.views.screen.arena.ArenaScreen;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;

    public class ArenaController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArenaController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupNav();
            _setupView();
        }

        override public function destroy():void
        {
            _destroyOperateController();
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:ArenaScreen;
        private var _operateController:Controller;
        private var _navigation:Object = {};

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupNav():void
        {
            _navigation = {
                "0": _setFindOpponents,
                "1": _setForge
            };
        }

        private function _setupView():void
        {
            _view = new ArenaScreen();
            _view.addEventListener(Event.CHANGE, _changeHandler);
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            super.container.addChild(_view);
        }

        private function _setFindOpponents():void
        {
            _destroyOperateController();
            _operateController = new FindOpponentsController(baseController);
            _operateController.execute();

            _operateController.addEventListener(BattleEvent.CALL_BATTLE, _opponentsHandler);
            _operateController.addEventListener(InfoEvent.GET_INFO, _opponentsHandler);
        }

        private function _setForge():void
        {
            _destroyOperateController();
            _operateController = new ForgeController(baseController);
            _operateController.execute();
        }

        private function _destroyOperateController():void
        {
            if (_operateController && !_operateController.destroyed)
                _operateController.destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _opponentsHandler(event:ApplicationEvent):void
        {
            dispatchEvent(event);
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            destroy();
        }

        private function _changeHandler(event:Event):void
        {
            if (_navigation[_view.tabID]) {
                var method:Function = _navigation[_view.tabID];
                method.call();
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
