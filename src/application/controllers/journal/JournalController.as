/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 03.10.13
 * Time: 21:04
 */
package application.controllers.journal
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.events.InfoEvent;
    import application.service.model.GetMarketItems;
    import application.views.screen.journal.JournalView;

    import flash.events.Event;

    import framework.core.helpers.MathHelper;
    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.task.TaskEvent;

    public class JournalController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function JournalController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupDependencies();
            _loadMarketItems();
        }

        override public function destroy():void
        {
            _destroyOperateController();
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:JournalView;
        private var _controls:Array = [];

        private var _operateController:Controller;

        private var _loadItemsService:GetMarketItems;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupDependencies():void
        {
            _controls.push(
                    StatsController,
                    NewsController
            )
        }

        private function _loadMarketItems():void
        {
            _loadItemsService = new GetMarketItems(baseController as ApplicationController);
            _loadItemsService.addEventListener(TaskEvent.COMPLETE, _itemsLoadHandler);
            _loadItemsService.execute();
        }

        private function _renderView():void
        {
            _view = new JournalView();
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            super.container.addChild(_view);
            _view.tabs.addEventListener(Event.CHANGE, _tabChangeHandler);
            _view.tabs.select(0);
        }

        private function _destroyOperateController():void
        {
            if (_operateController && !_operateController.destroyed)
                _operateController.destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _itemsLoadHandler(event:TaskEvent):void
        {
            _loadItemsService.removeEventListener(TaskEvent.COMPLETE, _itemsLoadHandler);
            _loadItemsService.destroy();
            _renderView();
        }

        private function _tabChangeHandler(event:Event):void
        {
            _destroyOperateController();

            var selectedTab:int = MathHelper.clamp(_view.tabs.selectedTab, 0, 1);
            _operateController = new _controls[selectedTab](super.baseController);
            _operateController.addEventListener(InfoEvent.GET_INFO, _redispatchEvent);
            _operateController.execute();
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
