/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.10.13
 * Time: 21:08
 */
package application.controllers.journal
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.events.InfoEvent;
    import application.helpers.WallPicturesRegistry;
    import application.models.ModelData;
    import application.models.user.journal.Journal;
    import application.server_api.ApiMethods;
    import application.views.TextFactory;
    import application.views.screen.stats.StatsView;

    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.core.error.ApplicationError;
    import framework.core.events.GameEvent;
    import framework.core.helpers.StringHelper;
    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.tools.wall_post.WallPostTool;
    import framework.core.underquery.ApiRequestEvent;

    public class StatsController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StatsController(baseController:IBaseController)
        {
            super(baseController);
            _appController = ApplicationController(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_journalModel.length) {
                _renderView(null);
                _journalModel.addEventListener(Event.CHANGE, _renderView);
            }
            else {
                _appController.loadingView.showLoader();
                _appController.serverApi.makeRequest(ApiMethods.USER_GET_JOURNAL, null, _onJournalData);
            }
        }

        override public function destroy():void
        {
            _destroyView();
            _journalModel.removeEventListener(Event.CHANGE, _renderView);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:StatsView;
        private var _appController:ApplicationController;
        private var _journalModel:Journal = ModelsRegistry.getModel(ModelData.USER_JOURNAL) as Journal;
        private var _wallPostTool:WallPostTool;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderView(event:Event = null):void
        {
            _destroyView();
            _view = new StatsView();
            _view.addEventListener(InfoEvent.GET_INFO, _redispatchEvent);
            _view.addEventListener(ApplicationEvent.SHARE, _onShare);
            super.container.addChild(_view);
        }

        private function _onShare(event:ApplicationEvent):void
        {
            _appController.loadingView.showLoader();
            var message:String = TextFactory.instance.getLabel('share_win_label')
                .replace('{login}', event.data['login']);

            _destroyWallPostTool();
            _wallPostTool = new WallPostTool();
            _wallPostTool.postText = StringHelper.stripTags(message);
            _wallPostTool.pictureSocNetId = WallPicturesRegistry.victory;
            _wallPostTool.addEventListener(Event.COMPLETE, _onWallPost);
            _wallPostTool.addEventListener(ErrorEvent.ERROR, _onPostError);
            _wallPostTool.addEventListener(GameEvent.ALBUM_CREATED, _appController.onApplicationAlbumCreated);
            _wallPostTool.addPost();
        }

        private function _onWallPost(e:Event):void
        {
            _appController.loadingView.hideLoader();
        }

        private function _onPostError(e:ErrorEvent):void
        {
            var appError:ApplicationError = new ApplicationError();
            appError.message = e.text;
            appError.fatal = false;
            _appController.appError(appError);
        }

        private function _destroyView():void
        {
            if (_view && !_view.destroyed)
                _view.destroy();
            _view = null;
        }

        private function _onJournalData(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();
            _journalModel.addEventListener(Event.CHANGE, _renderView);
            return true;
        }

        private function _destroyWallPostTool():void
        {
            if (_wallPostTool && !_wallPostTool.destroyed)
                _wallPostTool.destroy();
            _wallPostTool = null;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
