/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.10.13
 * Time: 21:09
 */
package application.controllers.journal
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.events.InfoEvent;
    import application.helpers.WallPicturesRegistry;
    import application.models.ModelData;
    import application.models.user.journal.News;
    import application.server_api.ApiMethods;
    import application.views.screen.stats.NewsView;

    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.core.error.ApplicationError;
    import framework.core.events.GameEvent;
    import framework.core.helpers.StringHelper;
    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.tools.wall_post.WallPostTool;
    import framework.core.underquery.ApiRequestEvent;

    public class NewsController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function NewsController(baseController:IBaseController)
        {
            super(baseController);
            _appController = ApplicationController(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_newsModel.length) {
                _renderView(null);
                _newsModel.addEventListener(Event.CHANGE, _renderView);
            }
            else {
                _appController.loadingView.showLoader();
                _appController.serverApi.makeRequest(ApiMethods.NEWS_GET_NEWS, null, _onNewsData);
            }
        }

        override public function destroy():void
        {
            _newsModel.removeEventListener(Event.CHANGE, _renderView);
            _newsModel = null;
            _destroyView();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _viewPosition:Number = 0;
        private var _view:NewsView;
        private var _wallPostTool:WallPostTool;
        private var _newsModel:News = ModelsRegistry.getModel(ModelData.NEWS) as News;
        private var _likeComplete:Boolean;
        private var _shareComplete:Boolean;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _onNewsData(e:ApiRequestEvent):Boolean
        {
            _appController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onNewsDataImported);
            return true;
        }

        private function _onNewsDataImported(event:ApiRequestEvent):void
        {
            _appController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onNewsDataImported);
            _newsModel.addEventListener(Event.CHANGE, _renderView);
            _renderView();
            _appController.loadingView.hideLoader();
        }

        private function _renderView(event:Event = null):void
        {
            _destroyView();
            _view = new NewsView(baseController as ApplicationController);
            _view.addEventListener(InfoEvent.GET_INFO, _redispatchEvent);
            _view.addEventListener(ApplicationEvent.LIKE, _onNewsLike);
            super.container.addChild(_view);
            _view.scrollPosition = _viewPosition;
        }

        private function _onNewsLike(e:ApplicationEvent):void
        {
            _appController.loadingView.showLoader();
            _likeComplete = false;

            if (e.data['share']) {
                _shareComplete = false;
                _destroyWallPostTool();
                _wallPostTool = new WallPostTool();
                _wallPostTool.postText = StringHelper.stripTags(e.data['message']);
                _wallPostTool.pictureSocNetId = WallPicturesRegistry.news;
                _wallPostTool.addEventListener(Event.COMPLETE, _onWallPost);
                _wallPostTool.addEventListener(ErrorEvent.ERROR, _onPostError);
                _wallPostTool.addEventListener(GameEvent.ALBUM_CREATED, _appController.onApplicationAlbumCreated);
                _wallPostTool.addPost();
            }
            else {
                _shareComplete = true;
            }

            var params:Object = {
                'news_id': e.data['news_id'],
                'is_shared': e.data['share'] ? '1' : '0'
            };
            _appController.serverApi.makeRequest(ApiMethods.NEWS_SET_NEWS_READ, params, _onSetRead);
        }

        private function _onSetRead(e:ApiRequestEvent):Boolean
        {
            _likeComplete = true;
            _checkLikeComplete();
            return true;
        }

        private function _onWallPost(e:Event):void
        {
            _shareComplete = true;
            _checkLikeComplete();
        }

        private function _checkLikeComplete():void
        {
            if (_likeComplete && _shareComplete)
                _appController.loadingView.hideLoader();
        }

        private function _onPostError(e:ErrorEvent):void
        {
            var appError:ApplicationError = new ApplicationError();
            appError.message = e.text;
            appError.fatal = false;
            _appController.appError(appError);
        }

        private function _destroyView():void
        {
            if (_view && !_view.destroyed) {
                _viewPosition = _view.scrollPosition;
                _view.destroy();
            }
            _view = null;
        }

        private function _destroyWallPostTool():void
        {
            if (_wallPostTool && !_wallPostTool.destroyed)
                _wallPostTool.destroy();
            _wallPostTool = null;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
