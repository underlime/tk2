/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 18:21
 */
package application.controllers
{
    import application.controllers.load.AssetsLoader;
    import application.error.ErrorCode;
    import application.events.LoadEvent;
    import application.models.ModelData;
    import application.models.ranks.RanksModel;
    import application.server_api.ApiMethods;
    import application.sound.SoundManager;

    import flash.display.Loader;
    import flash.events.Event;
    import flash.system.LoaderContext;
    import flash.utils.ByteArray;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.tools.SourceManager;
    import framework.core.underquery.ApiRequestEvent;
    import framework.core.utils.Singleton;

    public class LoadController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LoadController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _appController = super.baseController as ApplicationController;
            _appController.loadingView.showLoader();

            _setLoaderContext();
            _loadBinaryLibs();
            _loadAdditionalData();
            _loadInfo();
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../lib/templates/templates.swf", mimeType="application/octet-stream")]
        private var TemplateClass:Class;

        [Embed(source="../../../lib/common_graphic/TK2_GUI_KIT.swf", mimeType="application/octet-stream")]
        private var BitmapAssetsClass:Class;

        [Embed(source="../../../lib/common_graphic/TEMPLATES_SpecialAtackNames.swf", mimeType="application/octet-stream")]
        private var SpecialNamesClass:Class;

        [Embed(source="../../../lib/common_graphic/VERSUS_AND_FIGHT.swf", mimeType="application/octet-stream")]
        private var VersusScreenClass:Class;

        [Embed(source="../../../lib/templates/bank_units.swf", mimeType="application/octet-stream")]
        private var BankUnitsClass:Class;

        [Embed(source="../../../lib/templates/embrions.swf", mimeType="application/octet-stream")]
        private var EmbryoClass:Class;

        [Embed(source="../../../lib/common_graphic/MAP.swf", mimeType="application/octet-stream")]
        private var MapClass:Class;

        [Embed(source="../../../lib/config/ranks.json", mimeType="application/octet-stream")]
        private var RanksConfig:Class;

        [Embed(source="../../../lib/sounds/sounds.swf", mimeType="application/octet-stream")]
        private var SoundClass:Class;

        [Embed(source="../../../lib/templates/templates_Items_OK.swf", mimeType="application/octet-stream")]
        private var ItemsClass:Class;

        [Embed(source="../../../lib/templates/arena.swf", mimeType="application/octet-stream")]
        private var ArenaClass:Class;

        [Embed(source="../../../lib/templates/templates_body_parts_OK.swf", mimeType="application/octet-stream")]
        private var BodyPartsClass:Class;

        [Embed(source="../../../lib/templates/templates_faces_OK.swf", mimeType="application/octet-stream")]
        private var FacesClass:Class;

        [Embed(source="../../../lib/templates/MAD_CUCUMBER_CHAR.swf", mimeType="application/octet-stream")]
        private var AddChars:Class;

        [Embed(source="../../../lib/common_graphic/TutorToFlash.swf", mimeType="application/octet-stream")]
        private var TutorialClass:Class;

        private var _loaderContext:LoaderContext;
        private var _framesDelay:int = 0;

        private var _libsLoaded:Boolean = false;
        private var _infoReady:Boolean = false;
        private var _socNetInfoReady:Boolean = false;

        private var _appController:ApplicationController;

        private var _registerNeeded:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setLoaderContext():void
        {
            _loaderContext = new LoaderContext(false, SourceManager.instance.appDomain);
        }

        private function _loadBinaryLibs():void
        {
            _loadByteArray(new TemplateClass());
            _loadByteArray(new BitmapAssetsClass());
            _loadByteArray(new SpecialNamesClass());
            _loadByteArray(new VersusScreenClass());
            _loadByteArray(new BankUnitsClass());
            _loadByteArray(new EmbryoClass());
            _loadByteArray(new MapClass());
            _loadSounds(new SoundClass());
            _loadByteArray(new ItemsClass());
            _loadByteArray(new ArenaClass());
            _loadByteArray(new BodyPartsClass());
            _loadByteArray(new FacesClass());
            _loadByteArray(new AddChars());
            _loadByteArray(new TutorialClass());
            (new AssetsLoader()).execute();
            _waitFrame();
        }

        private function _loadInfo():void
        {
            _appController.serverApi.makeRequest(ApiMethods.USER_GET_INFO, null, _onServerSuccess, _onServerError);
            _appController.socNet.getCurrentUserFullInfo(_onSocNetInfo);
        }

        private function _waitFrame():void
        {
            super.container.addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            trace("графические библиотеки загружены");
        }

        private function _enterFrameHandler(event:Event):void
        {
            _framesDelay++;
            if (_framesDelay > 9) {
                super.container.removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
                _libsLoaded = true;
                _checkComplete();
            }
        }

        private function _loadAdditionalData():void
        {
            _loadRanksConfig();
        }

        private function _loadRanksConfig():void
        {
            var bytes:ByteArray = new RanksConfig();
            var json:String = bytes.readUTFBytes(bytes.length);
            var data:Object = JSON.parse(json);

            var model:RanksModel = super.dataBase.getChildByName(ModelData.RANKS) as RanksModel;

            if (data.ranks_collection)
                model.importData(data.ranks_collection);

            trace("Звания установлены");
        }

        private function _checkComplete():void
        {
            if (_libsLoaded && _infoReady && _socNetInfoReady)
                _completeLoading();
        }

        private function _completeLoading():void
        {
            _appController.loadingView.hideLoader();
            trace("Загрузка завершена");
            var event:LoadEvent = new LoadEvent(LoadEvent.COMPLETE);
            event.registerNeeded = _registerNeeded;

            super.dispatchEvent(event);

        }

        private function _onSocNetInfo(...args):Boolean
        {
            _socNetInfoReady = true;
            trace("Данные соц. сети загружены");
            _checkComplete();
            return true;
        }

        private function _loadByteArray(byteArray:ByteArray):void
        {
            var bytesLoader:Loader = new Loader();
            bytesLoader.loadBytes(byteArray, _loaderContext);
        }

        private function _loadSounds(_bytes:ByteArray):void
        {
            var bytesLoader:Loader = new Loader();
            var soundManager:SoundManager = Singleton.getClass(SoundManager);
            bytesLoader.loadBytes(_bytes, new LoaderContext(false, soundManager.appDomain));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            return true;
        }

        private function _onServerError(e:ApiRequestEvent):Boolean
        {
            var errorData:Object = e.data;
            if (errorData && parseInt(errorData['code']) == ErrorCode.NEED_REGISTER_CODE) {
                _infoReady = true;
                _registerNeeded = true;
                _checkComplete();
                return false;
            }
            return true;
        }

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);

            trace('данные пользователя с сервера загружены');
            _infoReady = true;
            _checkComplete();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
