package application.controllers.load
{
    import framework.core.tools.SourceManager;

    public class AssetsLoader
    {
        [Embed(source="../../../../lib/assets/ICON_TOMATOS.png")]
        private var IconTomatos:Class;

        [Embed(source="../../../../lib/assets/ICON_FERROS.png")]
        private var IconFerros:Class;

        [Embed(source="../../../../lib/assets/ICON_MEDICINE.png")]
        private var IconMedicine:Class;

        [Embed(source="../../../../lib/assets/ICON_GRENADE.png")]
        private var IconGrenade:Class;

        [Embed(source="../../../../lib/assets/ICON_BLACK_BOX.png")]
        private var IconBlackBox:Class;

        [Embed(source="../../../../lib/assets/WALL_POST_BG.png")]
        private var WallPostBg:Class;

        [Embed(source="../../../../lib/assets/WALL_POST_BG_S.png")]
        private var WallPostBgS:Class;

        [Embed(source="../../../../lib/assets/REFORGE_BG.png")]
        private var ForgeBack:Class;

        [Embed(source="../../../../lib/assets/REFORGE_LEVEL.png")]
        private var ForgeIcons:Class;

        [Embed(source="../../../../lib/assets/REFORGE_LABEL.png")]
        private var ForgeLabel:Class;

        [Embed(source="../../../../lib/assets/CLAN_SIGNS-01.png")]
        private var NoClanIcon:Class;

        [Embed(source="../../../../lib/assets/ia_tutor_complete.png")]
        private var TutorialCompleteIcon:Class;

        private var _sourceManager:SourceManager = SourceManager.instance;

        public function AssetsLoader()
        {
        }

        public function execute():void
        {
            _sourceManager.addBitmap(new IconTomatos(), 'ICON_TOMATOS');
            _sourceManager.addBitmap(new IconFerros(), 'ICON_FERROS');
            _sourceManager.addBitmap(new IconMedicine(), 'ICON_MEDICINE');
            _sourceManager.addBitmap(new IconGrenade(), 'ICON_GRENADE');
            _sourceManager.addBitmap(new IconBlackBox(), 'ICON_BLACK_BOX');
            _sourceManager.addBitmap(new WallPostBg(), 'WALL_POST_BG');
            _sourceManager.addBitmap(new WallPostBgS(), 'WALL_POST_BG_S');

            _sourceManager.addBitmap(new ForgeBack(), 'Forge_Back');
            _sourceManager.addBitmap(new ForgeIcons(), 'ForgeIcons');
            _sourceManager.addBitmap(new ForgeLabel(), 'ForgeLabel');
            _sourceManager.addBitmap(new NoClanIcon(), 'NoClanIcon');
            _sourceManager.addBitmap(new TutorialCompleteIcon(), 'itutorial_complete');
        }
    }
}
