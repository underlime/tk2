/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.09.13
 * Time: 19:31
 */
package application.controllers.bank
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.events.BankEvent;
    import application.models.ModelData;
    import application.models.bank.Bank;
    import application.models.bank.BankRecord;
    import application.server_api.ApiMethods;
    import application.views.TextFactory;
    import application.views.screen.bank.BankScreen;

    import framework.core.interfaces.IBaseController;
    import framework.core.socnet.struct.BuyMoneyById;
    import framework.core.struct.controller.Controller;
    import framework.core.underquery.ApiRequestEvent;

    public class BankController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankController(baseController:IBaseController)
        {
            super(baseController);
            _appController = ApplicationController(baseController);
            _bankModel = baseController.dataBase.getChildByName(ModelData.BANK_MODEL) as Bank;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_bankModel.length) {
                _renderView();
            }
            else {
                _appController.loadingView.showLoader();
                _appController.serverApi.makeRequest(ApiMethods.BANK_GET_CURRENCY_RATES, null, _onCurrencyRates);
            }

        }

        private function _onCurrencyRates(...args):Boolean
        {
            _appController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onBankDataImported);
            return true;
        }

        private function _onBankDataImported(event:ApiRequestEvent):void
        {
            _appController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onBankDataImported);
            _appController.loadingView.hideLoader();
            _renderView();
        }

        override public function destroy():void
        {
            _view.removeEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.removeEventListener(BankEvent.BUY, _buyHandler);
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:BankScreen;
        private var _bankModel:Bank;
        private var _appController:ApplicationController;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderView():void
        {
            _view = new BankScreen();
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.addEventListener(BankEvent.BUY, _buyHandler);
            super.container.addChild(_view);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        private function _buyHandler(event:BankEvent):void
        {
            _appController.loadingView.showLoader();
            var record:BankRecord = event.data['record'];

            var params:BuyMoneyById = new BuyMoneyById();
            params.item_id = record.id.value;
            params.count = record.count.value;
            params.price = record.price.value;
            params.currency = "F";
            _appController.socNet.buyMoneyById(params, _onBuyMoney, null, _onBuyCancel);
        }

        private function _onBuyMoney(...args):void
        {
            _appController.serverApi.makeRequest(ApiMethods.USER_GET_MONEY, null, _onGetMoney);
        }

        private function _onGetMoney(...args):Boolean
        {
            _appController.loadingView.hideLoader();
            return true;
        }

        private function _onBuyCancel(...args):void
        {
            _appController.loadingView.hideLoader();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
