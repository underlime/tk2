/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.09.13
 * Time: 14:29
 */
package application.controllers.bank
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.events.BankEvent;
    import application.models.ModelData;
    import application.models.bank.Bank;
    import application.models.bank.BankRecord;
    import application.server_api.ApiMethods;
    import application.views.popup.BankExpress;

    import framework.core.interfaces.IBaseController;
    import framework.core.socnet.struct.BuyMoneyById;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.underquery.ApiRequestEvent;

    public class BankExpressController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankExpressController(baseController:IBaseController)
        {
            super(baseController);
            _appController = ApplicationController(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_bankModel.length) {
                _renderView();
            }
            else {
                _appController.loadingView.showLoader();
                _appController.serverApi.makeRequest(ApiMethods.BANK_GET_CURRENCY_RATES, null, _onCurrencyRates);
            }
        }

        private function _onCurrencyRates(...args):Boolean
        {
            _appController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onBankDataImported);
            return true;
        }

        private function _onBankDataImported(event:ApiRequestEvent):void
        {
            _appController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onBankDataImported);
            _appController.loadingView.hideLoader();
            _renderView();
        }

        private function _renderView():void
        {
            _view = new BankExpress();
            super.container.addChild(_view);
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.addEventListener(BankEvent.BUY, _buyHandler);
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bankModel:Bank = ModelsRegistry.getModel(ModelData.BANK_MODEL) as Bank;
        private var _appController:ApplicationController;
        private var _view:BankExpress;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _buyHandler(event:BankEvent):void
        {
            _appController.loadingView.showLoader();
            var record:BankRecord = event.data['record'];

            var params:BuyMoneyById = new BuyMoneyById();
            params.item_id = record.id.value;
            params.count = record.count.value;
            params.price = record.price.value;
            params.currency = "F";
            _appController.socNet.buyMoneyById(params, _onBuyMoney, null, _onBuyCancel);
        }

        private function _onBuyMoney(...args):void
        {
            _appController.serverApi.makeRequest(ApiMethods.USER_GET_MONEY, null, _onGetMoney);
        }

        private function _onGetMoney(...args):Boolean
        {
            _appController.loadingView.hideLoader();
            return true;
        }

        private function _onBuyCancel(...args):void
        {
            _appController.loadingView.hideLoader();
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
