/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 12:41
 */
package application.controllers.top
{
    import application.controllers.*;
    import application.events.ApplicationEvent;
    import application.events.ClanEvent;
    import application.events.InfoEvent;
    import application.models.ModelData;
    import application.models.top.TopHelper;
    import application.models.top.TopInterval;
    import application.models.top.TopModel;
    import application.models.top.TopType;
    import application.models.user.User;
    import application.models.user.UserInfo;
    import application.service.model.clans.GetClansTopAction;
    import application.service.model.top.TopAction;
    import application.views.screen.top.TopScreen;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ListData;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;

    public class TopController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopController(baseController:IBaseController, selectedTab:int = 0)
        {
            super(baseController);
            _selectedTab = selectedTab;
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            _userInfo = _user.userInfo;

            _model = ModelsRegistry.getModel(ModelData.TOP_MODEL) as TopModel;
            _appController = baseController as ApplicationController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _renderView();
        }

        override public function destroy():void
        {
            _destroyTopAction();
            if (_clanTopAction && !_clanTopAction.destroyed)
                _clanTopAction.destroy();
            if (_view && !_view.destroyed)
                _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _selectedTab:int;
        private var _view:TopScreen;
        private var _model:TopModel;
        private var _userInfo:UserInfo;
        private var _topSuite:ListData;
        private var _user:User;
        private var _type:TopType = TopType.GLOBAL;
        private var _interval:TopInterval = TopInterval.GLOBAL;

        private var _topAction:TopAction;
        private var _clanTopAction:GetClansTopAction;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderView():void
        {
            _view = new TopScreen();
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.addEventListener(InfoEvent.GET_INFO, _getInfoHandler);
            _view.addEventListener(ClanEvent.SHOW, _redispatchEvent);
            super.container.addChild(_view);

            _view.intervalTabs.select(_selectedTab);

            _view.intervalTabs.addEventListener(Event.CHANGE, _tabsChangeHandler);
            _view.typeTabs.addEventListener(Event.CHANGE, _tabsChangeHandler);

            _view.typeTabs.select(0);
        }

        private function _loadData():void
        {
            _destroyTopAction();
            _topAction = new TopAction(_appController, _type, _interval);
            _topAction.addEventListener(TaskEvent.COMPLETE, _loadCompleteHandler);
            _topAction.execute();
        }

        private function _destroyTopAction():void
        {
            if (_topAction && !_topAction.destroyed)
                _topAction.destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _tabsChangeHandler(event:Event):void
        {
            if (!TopHelper.isClanTab(_view.intervalTabs.selectedTab)) {
                _showUsersTop();
            } else {
                _showClanTop();
            }
        }

        private function _showUsersTop():void
        {
            _type = TopHelper.getType(_view.typeTabs.selectedTab);
            _interval = TopHelper.getInterval(_view.intervalTabs.selectedTab);
            _loadData();
            _view.showUsersTop();
        }

        private function _showClanTop():void
        {
            _clanTopAction = new GetClansTopAction(baseController as ApplicationController);
            _clanTopAction.addEventListener(TaskEvent.COMPLETE, _clansActionCompleteHandler);
            _clanTopAction.execute();
        }

        private function _clansActionCompleteHandler(event:TaskEvent):void
        {
            _clanTopAction.destroy();
            _view.showClansTop();
        }

        private function _loadCompleteHandler(event:TaskEvent):void
        {
            _destroyTopAction();
            _topSuite = _model.getTop(_type, _interval);
            _view.topList = _topSuite;
            _view.userPosition = TopHelper.getUserPosition(_topSuite, _userInfo.user_id.value);
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        private function _getInfoHandler(event:InfoEvent):void
        {
            if (event.soc_net_id != _userInfo.soc_net_id.value)
                dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
