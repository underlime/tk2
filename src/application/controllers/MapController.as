/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:10
 */
package application.controllers
{
    import application.controllers.academy.AcademyController;
    import application.controllers.arena.ArenaController;
    import application.controllers.arena.BattleController;
    import application.controllers.bank.BankController;
    import application.controllers.bank.BankExpressController;
    import application.controllers.clans.ClansController;
    import application.controllers.home.HomeController;
    import application.controllers.journal.JournalController;
    import application.controllers.opponent.OpponentController;
    import application.controllers.popup.LowEnergyController;
    import application.controllers.top.TopController;
    import application.events.ApplicationEvent;
    import application.events.BattleEvent;
    import application.events.ClanEvent;
    import application.events.InfoEvent;
    import application.models.ModelData;
    import application.models.user.User;
    import application.server_api.ApiMethods;
    import application.views.map.MapView;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.interfaces.IController;
    import framework.core.struct.controller.Controller;

    public class MapController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MapController(baseController:IBaseController)
        {
            super(baseController);
            _userModel = baseController.dataBase.getChildByName(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupMap();
            _addDefaultBindings();
        }

        override public function destroy():void
        {
            _mapView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function callGuestScreen(socNetId:String):void
        {
            if (socNetId != _userModel.userInfo.soc_net_id.value)
                _executeOpponentController(socNetId);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _mapView:MapView;
        private var _operateController:IController;
        private var _userModel:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addDefaultBindings():void
        {
            super.container.addEventListener(ApplicationEvent.CALL_EXPRESS_BANK, _callExpressBank);
        }

        private function _setupMap():void
        {
            if (!_mapView || _mapView.destroyed) {
                _mapView = new MapView();
                _mapView.addEventListener(ApplicationEvent.TUTORIAL_COMPLETE, _onTutorialComplete);
                super.container.addChild(_mapView);
                _bindMap();
            }
        }

        private function _bindMap():void
        {
            _mapView.addEventListener(ApplicationEvent.CALL_MARKET, _callMarketHandler);
            _mapView.addEventListener(ApplicationEvent.CALL_ACADEMY, _callLaboratoryHandler);
            _mapView.addEventListener(ApplicationEvent.CALL_HOME, _callHomeHandler);
            _mapView.addEventListener(ApplicationEvent.CALL_TOP, _callTopHandler);
            _mapView.addEventListener(ApplicationEvent.CALL_SETTINGS, _callSettingsHandler);
            _mapView.addEventListener(ApplicationEvent.CALL_ARENA, _callArenaHandler);
            _mapView.addEventListener(ApplicationEvent.CALL_BANK, _callBankHandler);
            _mapView.addEventListener(ApplicationEvent.CALL_JOURNAL, _callJournalHandler);
            _mapView.addEventListener(ApplicationEvent.CALL_GMO, _callGmoHandler);
            _mapView.addEventListener(ApplicationEvent.CALL_CLANS, _callClansHandler);
        }

        private function _unbindMap():void
        {
            _mapView.removeEventListener(ApplicationEvent.CALL_MARKET, _callMarketHandler);
            _mapView.removeEventListener(ApplicationEvent.CALL_ACADEMY, _callLaboratoryHandler);
            _mapView.removeEventListener(ApplicationEvent.CALL_HOME, _callHomeHandler);
            _mapView.removeEventListener(ApplicationEvent.CALL_TOP, _callTopHandler);
            _mapView.removeEventListener(ApplicationEvent.CALL_SETTINGS, _callSettingsHandler);
            _mapView.removeEventListener(ApplicationEvent.CALL_ARENA, _callArenaHandler);
            _mapView.removeEventListener(ApplicationEvent.CALL_BANK, _callBankHandler);
            _mapView.removeEventListener(ApplicationEvent.CALL_JOURNAL, _callJournalHandler);
        }

        private function _destroyOperateController():void
        {
            if (_operateController && !_operateController.destroyed)
                _operateController.destroy();
        }

        private function _executeOpponentController(socNetId:String):void
        {
            _destroyOperateController();
            _operateController = new OpponentController(baseController, socNetId);
            _operateController.addEventListener(BattleEvent.CALL_BATTLE, _callBattleHandler);
            _operateController.addEventListener(ClanEvent.SHOW, _showClanHandler);
            _operateController.execute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _callExpressBank(event:ApplicationEvent):void
        {
            (new BankExpressController(super.baseController)).execute();
        }

        private function _callMarketHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _operateController = new MarketController(super.baseController);
            _operateController.execute();
        }

        private function _callLaboratoryHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _operateController = new AcademyController(super.baseController);
            _operateController.execute();
        }

        private function _callHomeHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _operateController = new HomeController(super.baseController);
            _operateController.execute();
        }

        private function _callTopHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _operateController = new TopController(super.baseController);
            _operateController.addEventListener(InfoEvent.GET_INFO, _getInfoHandler);
            _operateController.addEventListener(ClanEvent.SHOW, _showClanHandler);
            _operateController.execute();
        }

        private function _callSettingsHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _operateController = new SettingsController(super.baseController);
            _operateController.execute();
        }

        private function _callArenaHandler(event:ApplicationEvent = null):void
        {
            _destroyOperateController();
            _operateController = new ArenaController(super.baseController);
            _operateController.addEventListener(BattleEvent.CALL_BATTLE, _callBattleHandler);
            _operateController.addEventListener(InfoEvent.GET_INFO, _getInfoHandler);
            _operateController.execute();
        }

        private function _callJournalHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _operateController = new JournalController(baseController);
            _operateController.addEventListener(InfoEvent.GET_INFO, _getInfoHandler);
            _operateController.execute();
        }

        private function _callGmoHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _operateController = new GMOController(baseController);
            _operateController.addEventListener(InfoEvent.GET_INFO, _getInfoHandler);
            _operateController.execute();
        }

        private function _callClansHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _operateController = new ClansController(baseController);
            _operateController.addEventListener(InfoEvent.GET_INFO, _getInfoHandler);
            _operateController.execute();
        }

        private function _showClanHandler(event:ClanEvent):void
        {
            _destroyOperateController();
            _operateController = new ClansController(baseController, event.clan_id);
            _operateController.addEventListener(InfoEvent.GET_INFO, _getInfoHandler);
            _operateController.execute();
        }

        // Battle

        private function _callBattleHandler(e:BattleEvent):void
        {
            _destroyOperateController();
            _unbindMap();
            _mapView.destroy();
            _operateController = new BattleController(super.baseController, e.enemyRecord);
            _operateController.addEventListener(Event.COMPLETE, _battleCompleteHandler);
            _operateController.addEventListener(BattleEvent.CALL_MAP, _showMapHandler);
            _operateController.addEventListener(ApplicationEvent.CALL_ARENA, _showArenaHandler);
            _operateController.addEventListener(ApplicationEvent.LOW_ENERGY, _lowEnergyHandler);
            _operateController.execute();
        }

        private function _lowEnergyHandler(event:ApplicationEvent):void
        {
            var battleController:BattleController = event.currentTarget as BattleController;
            battleController.removeEventListener(Event.COMPLETE, _battleCompleteHandler);
            battleController.destroy();

            _setupMap();
            (new LowEnergyController(super.baseController)).execute();
        }

        private function _battleCompleteHandler(event:Event):void
        {
            var battleController:BattleController = event.currentTarget as BattleController;
            battleController.removeEventListener(Event.COMPLETE, _battleCompleteHandler);
            battleController.destroy();
        }

        private function _showMapHandler(event:BattleEvent):void
        {
            var battleController:BattleController = event.currentTarget as BattleController;
            battleController.removeEventListener(BattleEvent.CALL_MAP, _showMapHandler);
            battleController.destroy();

            _setupMap();
        }

        private function _showArenaHandler(event:ApplicationEvent):void
        {
            var battleController:BattleController = event.currentTarget as BattleController;
            battleController.removeEventListener(ApplicationEvent.CALL_ARENA, _showArenaHandler);
            battleController.destroy();

            _setupMap();
            _callArenaHandler();
        }

        // Battle

        private function _getInfoHandler(event:InfoEvent):void
        {
            _executeOpponentController(event.soc_net_id);
        }

        private function _callBankHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();
            _operateController = new BankController(super.baseController);
            _operateController.execute();
        }

        private function _onTutorialComplete(event:ApplicationEvent):void
        {
            baseController.serverApi.makeRequest(ApiMethods.TRIGGERS_ON_TUTORIAL_COMPLETE);
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
