/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 22:06
 */
package application.controllers
{
    import application.common.Currency;
    import application.controllers.popup.LowMoneyController;
    import application.events.ApplicationEvent;
    import application.events.BuyEvent;
    import application.helpers.CurrencyHelper;
    import application.models.ModelData;
    import application.models.market.Item;
    import application.models.market.MarketItems;
    import application.models.user.User;
    import application.server_api.ApiMethods;
    import application.service.model.GetMarketItems;
    import application.views.popup.BuyingItem;
    import application.views.popup.GreatBuy;
    import application.views.screen.market.MarketScreen;

    import framework.core.error.ApplicationError;
    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;
    import framework.core.underquery.ApiRequestEvent;

    public class MarketController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketController(baseController:IBaseController)
        {
            super(baseController);
            _model = ModelsRegistry.getModel(ModelData.MARKET_ITEMS) as MarketItems;
            _appController = super.baseController as ApplicationController;
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _loadItemsService = new GetMarketItems(baseController as ApplicationController);
            _loadItemsService.addEventListener(TaskEvent.COMPLETE, _itemsLoadHandler);
            _loadItemsService.execute();
        }

        override public function destroy():void
        {
            if (_marketScreen && !_marketScreen.destroyed)
                _marketScreen.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:MarketItems;
        private var _appController:ApplicationController;
        private var _marketScreen:MarketScreen;

        private var _user:User;

        private var _item:Item;
        private var _loadItemsService:GetMarketItems;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderMarket():void
        {
            _marketScreen = new MarketScreen();
            _marketScreen.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _marketScreen.addEventListener(BuyEvent.BUY, _buyEvent);
            super.container.addChild(_marketScreen);
        }

        private function _canBuy(item:Item, currency:Currency, count:int):Boolean
        {
            var price:int;
            var haveMoney:int;

            if (currency == Currency.FERROS) {
                price = item.price_ferros.value * count;
                haveMoney = _user.userFightData.ferros.value;
            } else {
                price = item.price_tomatos.value * count;
                haveMoney = _user.userFightData.tomatos.value;
            }

            return haveMoney >= price;
        }

        private function _buy(item:Item, currency:Currency, count:int):void
        {
            var params:Object = {
                "count": count,
                "currency": CurrencyHelper.getShortCurrency(currency),
                "item_id": item.item_id.value
            };

            super.baseController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _buyCompleteHandler);

            super.baseController
                    .serverApi
                    .makeRequest(ApiMethods.ITEMS_BUY, params, _onBuySuccess, _onBuyError);

            (super.baseController as ApplicationController).loadingView.showLoader();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _itemsLoadHandler(event:TaskEvent):void
        {
            _loadItemsService.removeEventListener(TaskEvent.COMPLETE, _itemsLoadHandler);
            _loadItemsService.destroy();
            _renderMarket();
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            _marketScreen.removeEventListener(ApplicationEvent.CLOSE, _closeHandler);
            this.destroy();
        }

        private function _buyEvent(event:BuyEvent):void
        {
            _item = event.item;
            var currency:Currency = event.currency;
            var count:int = event.count;

            if (_canBuy(_item, currency, count))
                _buy(_item, currency, count);
            else
                (new LowMoneyController(super.baseController)).execute();
        }

        private function _buyCompleteHandler(event:ApiRequestEvent):void
        {
            super.baseController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _buyCompleteHandler);

            (super.baseController as ApplicationController).loadingView.hideLoader();
            var item:BuyingItem = new BuyingItem();
            item.importFromItem(_item);
            super.container.addChild(new GreatBuy(item));
        }

        private function _onBuySuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        private function _onBuyError(e:ApiRequestEvent):void
        {
            (super.baseController as ApplicationController).loadingView.hideLoader();
            var error:ApplicationError = new ApplicationError();
            error.message = e.errorType;
            super.baseController.appError(error);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
