/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.09.13
 * Time: 16:55
 */
package application.controllers
{
    import application.events.OfficeEvent;
    import application.events.RegisterEvent;
    import application.models.ModelData;
    import application.models.user.LoginModel;
    import application.models.user.User;
    import application.server_api.ApiMethods;
    import application.service.model.CheckLogin;
    import application.service.model.GetAllChars;
    import application.sound.SoundManager;
    import application.sound.lib.UISound;
    import application.views.register.RegisterView;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.TaskEvent;
    import framework.core.underquery.ApiRequestEvent;
    import framework.core.utils.Singleton;

    public class RegisterController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RegisterController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _getChars();
        }

        override public function destroy():void
        {
            _view.destroy();

            if (_checkLoginService && !_checkLoginService.destroyed)
                _checkLoginService.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _charsService:GetAllChars;
        private var _view:RegisterView;

        private var _checkLoginService:CheckLogin;
        private var _loginModel:LoginModel = new LoginModel();

        private var _sound:SoundManager = Singleton.getClass(SoundManager);
        private var _appController:ApplicationController;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getChars():void
        {
            _charsService = new GetAllChars(baseController as ApplicationController);
            _charsService.addEventListener(TaskEvent.COMPLETE, _charsLoadedHandler);
            _charsService.execute();
        }

        private function _renderView():void
        {
            _view = new RegisterView();
            _view.addEventListener(OfficeEvent.CHECK_LOGIN, _checkLoginHandler);
            _view.addEventListener(RegisterEvent.REGISTER, _registerHandler);
            super.container.addChild(_view);
        }

        private function _sendLoginResult():void
        {
            if (_loginModel.correct.value == 0)
                _view.loginDeny();
            else
                if (_loginModel.exists.value > 0)
                    _view.loginDeny();
                else
                    _view.loginAllow();
            _sound.playUISound(UISound.OK);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _charsLoadedHandler(event:TaskEvent):void
        {
            _charsService.destroy();
            _renderView();
        }

        private function _checkLoginHandler(event:OfficeEvent):void
        {
            _checkLoginService = new CheckLogin(baseController as ApplicationController, event.login);
            _checkLoginService.addEventListener(TaskEvent.COMPLETE, _checkCompleteHandler);
            _checkLoginService.execute();
        }

        private function _checkCompleteHandler(event:TaskEvent):void
        {
            _loginModel = _checkLoginService.loginModel;
            if (_checkLoginService && !_checkLoginService.destroyed)
                _checkLoginService.destroy();
            _sendLoginResult();
        }

        private function _registerHandler(event:RegisterEvent):void
        {
            _appController.loadingView.showLoader();
            var userModel:User = ModelsRegistry.getModel(ModelData.USER) as User;

            _appController.socNet.getReferrerInfo(function (data:Object):void
            {
                var params:Object = {
                    "login": event.registerData.login,
                    "vegetable": event.registerData.vegetable,
                    "body": event.registerData.body,
                    "hands": event.registerData.hands,
                    "eyes": event.registerData.eyes,
                    "mouth": event.registerData.mouth,
                    "hair": event.registerData.hair,
                    "referrer": data["referrer_data"]["user_id"],
                    "referrer_type": data["referrer_data"]["type"],
                    "city": userModel.socNetUserInfo.city.value
                };
                _appController.serverApi.makeRequest(ApiMethods.REGISTER, params, _onServerSuccess);
            });
        }

        private function _onComplete(event:ApiRequestEvent):void
        {
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onComplete);
            _appController.loadingView.hideLoader();

            dispatchEvent(new Event(Event.COMPLETE));
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onComplete);
            return true;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
