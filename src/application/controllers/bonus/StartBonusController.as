package application.controllers.bonus
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.market.MarketItems;
    import application.models.user.User;
    import application.server_api.ApiMethods;
    import application.views.popup.events.bonus.StartBonus;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.underquery.ApiRequestEvent;

    public class StartBonusController extends Controller
    {
        private var _view:StartBonus;
        private var _applicationController:ApplicationController;

        private var _userModel:User = ModelsRegistry.getModel(ModelData.USER) as User;
        private var _itemsModel:MarketItems = ModelsRegistry.getModel(ModelData.MARKET_ITEMS) as MarketItems;
        private var _friendsCount:int;

        private var _friendsInfoReceived:Boolean = false;
        private var _itemsReceived:Boolean = false;

        public function StartBonusController(baseController:IBaseController)
        {
            super(baseController);
            _applicationController = ApplicationController(baseController);
        }

        override public function execute():void
        {
            super.execute();
            _applicationController.loadingView.showLoader();
            if (_itemsModel.length)
                _itemsReceived = true;
            else
                _applicationController.serverApi.makeRequest(ApiMethods.ITEMS_GET_MARKET_LIST, null, _onMarketList);
            _applicationController.socNet.getFriendsInAppList(_onFriendsList);
        }

        private function _onMarketList(...args):Boolean
        {
            _applicationController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onMarketListLoaded);
            return true;
        }

        private function _onMarketListLoaded(e:ApiRequestEvent):void
        {
            _applicationController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onMarketListLoaded);
            _itemsReceived = true;
            _checkLoadingComplete();
        }

        private function _onFriendsList(data:Object):void
        {
            _friendsCount = _getFriendsCount(data);
            _friendsInfoReceived = true;
            _checkLoadingComplete();
        }

        private function _getFriendsCount(data:Object):int
        {
            var count:int = 0;
            if (data['friends_list'] is Array) {
                count = data['friends_list'].length;
            }
            else
                if (data['friends_list'] && (data['friends_list']['_add'] || data['friends_list']['_init'])) {
                    var locData:Object = (data['friends_list']['_add']) ? data['friends_list']['_add'] : data['friends_list']['_init'];
                    for each (var locRecord:Object in locData)
                        ++count;
                }
            return count;
        }

        private function _checkLoadingComplete():void
        {
            if (_friendsInfoReceived && _itemsReceived) {
                _applicationController.loadingView.hideLoader();
                if (_friendsCount > 0)
                    _showView();
                else
                    this.destroy();
            }
        }

        private function _showView():void
        {
            _view = new StartBonus(_friendsCount);
            _view.addEventListener(ApplicationEvent.TAB_CHOOSE, _onChoose);
            _applicationController.container.addChild(_view);
        }

        private function _onChoose(e:ApplicationEvent):void
        {
            _applicationController.loadingView.showLoader();
            var config:Array = _userModel.userOptInfo.start_bonuses_config.value as Array;
            var params:Object = {
                'bonus_type': config[e.data.position]['type']
            };
            baseController.serverApi.makeRequest(ApiMethods.USER_GIVE_FRIENDS_PRIZES, params, _onBonusSuccess, _onBonusError);
        }

        private function _onBonusSuccess(...args):Boolean
        {
            _applicationController.loadingView.hideLoader();
            this.destroy();
            return true;
        }

        private function _onBonusError(data:ApiRequestEvent):Boolean
        {
            _applicationController.loadingView.hideLoader();
            trace("ошибка");
            this.destroy();
            return false;
        }

        override public function destroy():void
        {
            if (_view && !_view.destroyed)
                _view.destroy();
            super.destroy();
        }
    }
}
