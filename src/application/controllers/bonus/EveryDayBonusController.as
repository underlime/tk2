/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.09.13
 * Time: 12:51
 */
package application.controllers.bonus
{
    import application.events.ApplicationEvent;
    import application.views.popup.events.bonus.EveryDayBonus;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;

    public class EveryDayBonusController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EveryDayBonusController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new EveryDayBonus();
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:EveryDayBonus;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
