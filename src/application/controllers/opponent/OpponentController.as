/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 11:33
 */
package application.controllers.opponent
{
    import application.events.ApplicationEvent;
    import application.events.BattleEvent;
    import application.events.ClanEvent;
    import application.models.ModelData;
    import application.models.arena.EnemyRecord;
    import application.models.opponents.Opponent;
    import application.models.opponents.OpponentsList;
    import application.views.TextFactory;
    import application.views.screen.opponents.OpponentScreen;

    import flash.events.Event;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;

    public class OpponentController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OpponentController(baseController:IBaseController, soc_net_id:String)
        {
            super(baseController);
            _soc_net_id = soc_net_id;
            _opponents = ModelsRegistry.getModel(ModelData.OPPONENTS_LIST) as OpponentsList;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupHash();

            _view = new OpponentScreen();
            _view.addEventListener(Event.CHANGE, _tabsChangeHandler);
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);

            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            _destroyOperateController();
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _soc_net_id:String;
        private var _view:OpponentScreen;
        private var _opponents:OpponentsList;
        private var _hash:Object;

        private var _operateController:Controller;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupHash():void
        {
            _hash = {
                "0": {
                    "controller": OpponentInfoController,
                    "label": TextFactory.instance.getLabel('user_info_label')
                },
                "1": {
                    "controller": OpponentAchievementsController,
                    "label": TextFactory.instance.getLabel('achievements_label')
                },
                "2": {
                    "controller": OpponentSpecialsController,
                    "label": TextFactory.instance.getLabel('skills_label')
                }
            };
        }

        private function _destroyOperateController():void
        {
            if (_operateController && !_operateController.destroyed)
                _operateController.destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _tabsChangeHandler(event:Event):void
        {
            var tabID:int = _view.tabs.selectedTab;
            if (_hash[tabID] && _hash[tabID]) {
                _destroyOperateController();

                var ControllerClass:Class = _hash[tabID]['controller'];
                var label:String = _hash[tabID]['label'];
                if (_view)
                    _view.label = label;
                _operateController = new ControllerClass(super.baseController, _soc_net_id);
                _operateController.addEventListener(BattleEvent.CALL_BATTLE, _battleHandler);
                _operateController.addEventListener(ClanEvent.SHOW, _showClanHandler);
                _operateController.execute();
            }
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        private function _battleHandler(event:BattleEvent):void
        {
            if (!_opponents.hasOpponent(_soc_net_id))
                throw new Error("Opponent not found!");
            var opponent:Opponent = _opponents.getOpponent(_soc_net_id);

            var enemyRecord:EnemyRecord = new EnemyRecord();
            enemyRecord.user_fight_data = opponent.userFightData;
            enemyRecord.user_info = opponent.userInfo;
            enemyRecord.user_level_params = opponent.userLevelParams;
            enemyRecord.user_inventory = opponent.user_inventory;
            enemyRecord.bonus_hp.value = opponent.userBonus.scale.hp.value;

            var battleEvent:BattleEvent = new BattleEvent(BattleEvent.CALL_BATTLE);
            battleEvent.enemy_id = opponent.userInfo.user_id.value;
            battleEvent.enemyRecord = enemyRecord;

            dispatchEvent(battleEvent);

        }

        private function _showClanHandler(event:ClanEvent):void
        {
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
