/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 13:21
 */
package application.controllers.opponent
{
    import application.controllers.ApplicationController;
    import application.events.BattleEvent;
    import application.models.ModelData;
    import application.models.inventory.Inventory;
    import application.models.opponents.Opponent;
    import application.models.opponents.OpponentsList;
    import application.models.user.achievements.Achievements;
    import application.server_api.ApiMethods;
    import application.views.screen.opponents.OpponentAchievementScreen;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.underquery.ApiRequestEvent;

    public class OpponentAchievementsController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OpponentAchievementsController(baseController:IBaseController, soc_net_id:String)
        {
            super(baseController);
            _soc_net_id = soc_net_id;
            _opponents = ModelsRegistry.getModel(ModelData.OPPONENTS_LIST) as OpponentsList;
            _appController = baseController as ApplicationController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            CONFIG::DEBUG {
                if (!_opponents.hasOpponent(_soc_net_id))
                    throw new Error("Не найден оппонент")
            }

            _opponent = _opponents.getOpponent(_soc_net_id);
            _model = _opponent.userAchievements;
            _inventory = _opponent.user_inventory;

            if (_model.isEmpty)
                _loadData();
            else
                _renderView();
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _soc_net_id:String;
        private var _opponents:OpponentsList;
        private var _opponent:Opponent;
        private var _appController:ApplicationController;

        private var _model:Achievements;
        private var _view:OpponentAchievementScreen;
        private var _inventory:Inventory;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _loadData():void
        {
            var params:Object = {
                "user_id": _opponent.userInfo.user_id.value
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.GET_ACHIEVEMENTS, params, _onServerSuccess);
        }

        private function _renderView():void
        {
            _view = new OpponentAchievementScreen(_opponent, _inventory);
            _view.addEventListener(BattleEvent.CALL_BATTLE, _battleHandler);
            super.container.addChild(_view);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();

            var data:Object = e.data;
            _opponent.invokeDataImport(data);

            _renderView();

            return false;
        }

        private function _battleHandler(event:BattleEvent):void
        {
            dispatchEvent(new BattleEvent(BattleEvent.CALL_BATTLE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
