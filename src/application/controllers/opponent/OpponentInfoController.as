/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 12:29
 */
package application.controllers.opponent
{
    import application.controllers.ApplicationController;
    import application.events.BattleEvent;
    import application.events.ClanEvent;
    import application.models.ModelData;
    import application.models.opponents.Opponent;
    import application.models.opponents.OpponentsList;
    import application.server_api.ApiMethods;
    import application.views.screen.opponents.OpponentInfoScreen;

    import framework.core.interfaces.IBaseController;
    import framework.core.struct.controller.Controller;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.underquery.ApiRequestEvent;

    public class OpponentInfoController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OpponentInfoController(baseController:IBaseController, soc_net_id:String)
        {
            super(baseController);
            _soc_net_id = soc_net_id;
            _opponents = ModelsRegistry.getModel(ModelData.OPPONENTS_LIST) as OpponentsList;
            _appController = baseController as ApplicationController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_opponents.hasOpponent(_soc_net_id)) {
                _opponent = _opponents.getOpponent(_soc_net_id);
                _renderView();
            }
            else {
                _loadData();
            }
        }

        override public function destroy():void
        {
            if (_opponentInfoScreen)
                _opponentInfoScreen.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _soc_net_id:String;
        private var _opponents:OpponentsList;

        private var _opponentInfoScreen:OpponentInfoScreen;
        private var _opponent:Opponent;

        private var _appController:ApplicationController;
        private var _serverInfoLoaded:Boolean = false;
        private var _socNetInfoLoaded:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderView():void
        {
            _appController.loadingView.hideLoader();
            _opponentInfoScreen = new OpponentInfoScreen(_opponent, _opponent.user_inventory);
            _opponentInfoScreen.addEventListener(BattleEvent.CALL_BATTLE, _battleHandler);
            _opponentInfoScreen.addEventListener(ClanEvent.SHOW, _showClanHandler);
            super.container.addChild(_opponentInfoScreen);
        }

        private function _loadData():void
        {
            _appController.loadingView.showLoader();
            _opponent = new Opponent();
            _opponents.addOpponent(_opponent);

            _serverInfoLoaded = false;
            _socNetInfoLoaded = false;

            var params:Object = {
                "soc_net_id": _soc_net_id
            };
            _appController.serverApi.makeRequest(ApiMethods.USER_GET_INFO, params, _onServerSuccess);
            _appController.socNet.getUserInfo(_soc_net_id, _onSocNetInfo);
        }

        private function _onSocNetInfo(data:Object):void
        {
            _opponent.socNetUserInfo.importData(data['user_info']);
            _socNetInfoLoaded = true;
            _checkInfoLoaded();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            var data:Object = e.data;
            _opponent.invokeDataImport(data);
            _opponent.importData(data);

            _serverInfoLoaded = true;
            _checkInfoLoaded();
            return false;
        }

        private function _checkInfoLoaded():void
        {
            if (_serverInfoLoaded && _socNetInfoLoaded)
                _renderView();
        }

        private function _battleHandler(event:BattleEvent):void
        {
            dispatchEvent(new BattleEvent(BattleEvent.CALL_BATTLE));
        }

        private function _showClanHandler(event:ClanEvent):void
        {
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
