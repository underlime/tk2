/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 17:19
 */
package application.views.error
{
    import application.error.ErrorCode;
    import application.views.TextFactory;
    import application.views.popup.BasePopup;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.external.ExternalInterface;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.error.ApplicationError;

    import mx.utils.ObjectUtil;

    public class ErrorView extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const EVENT_PIC:String = "EVENT_PICS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ErrorView(error:ApplicationError)
        {
            super();

            _error = error;
            _popupWidth = 309;
            _popupHeight = 302;

            var unknownError:String = TextFactory.instance.getLabel('error_header_label');
            var error503:String = TextFactory.instance.getLabel('error_header_503');

            if (error.code == ErrorCode.TEMPORARILY_UNAVAILABLE) {
                _header = error503;
                _error.message = TextFactory.instance.getLabel("error503message");
            } else {
                _header = unknownError;
            }

        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addPicture();
            _addDescription();
            _addOkButton();
        }

        override public function destroy():void
        {
            _txtMessage.destroy();
            _okButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _error:ApplicationError;
        private var _txtMessage:AppTextField;
        private var _okButton:AcceptButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addPicture():void
        {
            var eventPic:Bitmap = super.source.getBitmap(EVENT_PIC);
            eventPic.scrollRect = new Rectangle(0, 200, 280, 100);

            addChild(eventPic);
            eventPic.x = _backShape.x + _popupWidth / 2 - eventPic.width / 2;
            eventPic.y = _backShape.y + 56;
        }

        private function _addDescription():void
        {
            _txtMessage = new AppTextField();
            _txtMessage.color = 0x5e3113;
            _txtMessage.size = 12;
            _txtMessage.width = 280;
            _txtMessage.multiline = true;
            _txtMessage.wordWrap = true;
            _txtMessage.align = TextFormatAlign.CENTER;
            _txtMessage.autoSize = TextFieldAutoSize.LEFT;
            addChild(_txtMessage);
            _txtMessage.text = _error.message;
            _txtMessage.x = _backShape.x + _popupWidth / 2 - _txtMessage.width / 2;
            _txtMessage.y = _backShape.y + 178;
        }

        private function _addOkButton():void
        {
            var label:String = TextFactory.instance.getLabel('refresh_page_label');

            _okButton = new AcceptButton(label);
            _okButton.w = 120;
            addChild(_okButton);

            _okButton.x = int(_backShape.x + _popupWidth / 2 - _okButton.w / 2);
            _okButton.y = int(_backShape.y + _popupHeight - _okButton.h - 20);

            _okButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            if (ExternalInterface.available) {
                ExternalInterface.call("window.location.reload");
            } else {
                destroy();
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
