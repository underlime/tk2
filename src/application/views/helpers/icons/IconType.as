/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 14:13
 */
package application.views.helpers.icons
{

    import framework.core.enum.BaseEnum;

    public class IconType extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const HP_ICO:IconType = new IconType("hp");
        public static const ENERGY_ICO:IconType = new IconType("energy");
        public static const EXPERIENCE_ICO:IconType = new IconType("experience");
        public static const GLORY_ICO:IconType = new IconType("glory");
        public static const STRENGTH_ICO:IconType = new IconType("strength");
        public static const AGILITY_ICO:IconType = new IconType("agility");
        public static const INTELLECT_ICO:IconType = new IconType("intellect");
        public static const DAMAGE_ICO:IconType = new IconType("damage");
        public static const ARMOR_ICO:IconType = new IconType("armor");
        public static const SPEED_ICO:IconType = new IconType("speed");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function IconType(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.views.helpers.icons.IconType;

IconType.lockUp();