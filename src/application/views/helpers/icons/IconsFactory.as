/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 14:11
 */
package application.views.helpers.icons
{
    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.tools.SourceManager;

    public class IconsFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ICONS:String = "STATS_ICONS";
        public static const ICON_WIDTH:int = 40;
        public static const ICON_HEIGHT:int = 40;

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getIcon(iconType:IconType):Bitmap
        {
            var hash:Object = _getHash();
            var iconsMap:Bitmap = SourceManager.instance.getBitmap(ICONS);
            var offset:int = 0;

            if (hash[iconType.toString()])
                offset = hash[iconType.toString()];

            iconsMap.scrollRect = new Rectangle(0, offset, ICON_WIDTH, ICON_HEIGHT);

            return iconsMap;
        }

        private static function _getHash():Object
        {
            var hash:Object = {};
            hash[IconType.HP_ICO.toString()] = 0;
            hash[IconType.ENERGY_ICO.toString()] = 40;
            hash[IconType.EXPERIENCE_ICO.toString()] = 80;
            hash[IconType.GLORY_ICO.toString()] = 120;
            hash[IconType.STRENGTH_ICO.toString()] = 160;
            hash[IconType.AGILITY_ICO.toString()] = 200;
            hash[IconType.INTELLECT_ICO.toString()] = 240;
            hash[IconType.DAMAGE_ICO.toString()] = 280;
            hash[IconType.ARMOR_ICO.toString()] = 320;
            hash[IconType.SPEED_ICO.toString()] = 360;

            return hash;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
