/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.06.13
 * Time: 10:57
 */
package application.views.helpers
{
    import application.models.ModelData;
    import application.models.bonus.Bonus;
    import application.models.market.Item;
    import application.models.user.User;
    import application.models.user.UserLevelParams;
    import application.views.TextFactory;
    import application.views.text.AppTextField;

    import framework.core.struct.data.ModelsRegistry;

    public class ItemDescriptionHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static var _factory:TextFactory = TextFactory.instance;
        private static var _model:UserLevelParams;
        private static var _item:Item;
        private static var _html:String = "";

        private static var _brown:String = "#3c2415";
        private static var _light_brown:String = "#603913";
        private static var _green:String = "#006838";
        private static var _red:String = "#be1e2d";

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getDescription(item:Item):String
        {
            _item = item;
            _html = "";

            _setupModel();
            _getName();
            _getDescription();
            _generateDescription();

            return _html;
        }

        private static function _getName():void
        {
            _html += "<font color='" + _brown + "'><b>" + _item.name_ru.value + "</b></font><br>";
        }

        private static function _getDescription():void
        {
            _html += "<font size='10' color='" + _light_brown + "'>" + _item.description_ru.value + "</font><br><br>";
        }

        private static function _setupModel():void
        {
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;
            _model = user.getChildByName(ModelData.USER_LEVEL_PARAMS) as UserLevelParams;
        }

        private static function _generateDescription():void
        {
            _html += "<font face='" + AppTextField.COMIC_SANS + "' color='" + _brown + "'><b>" + _factory.getLabel('requirements_label') + ": </b></font>";
            _html += _getRequirementsBlock();
            _html += _getBonusBlock();
        }

        private static function _getRequirementsBlock():String
        {
            var color:String = _green;
            if (_item.required_level.value > _model.level.value)
                color = _red;

            var reqLevel:int = _item.required_level.value > 0 ? _item.required_level.value : 0;

            var html:String = "<p><font color='" + color + "'><b>" + reqLevel + " " + _factory.getLabel('level_label').toUpperCase() + "</b></font></p><br>";

            return html;
        }

        private static function _getBonusBlock():String
        {
            var bonus:Bonus = new Bonus(new XML(_item.bonuses.value));
            if (bonus.damage == 0 && bonus.armor == 0 && bonus.speed == 0 && bonus.hp == 0)
                return "";


            var html:String = "<p>";
            html += "<font color='" + _brown + "'><b>" + _factory.getLabel('bonus_label') + ":</b></font><br>";

            if (bonus.damage > 0 || _item.sharpen_damage.value > 0) {
                var dmg:int = _item.sharpen_damage.value > 0 ? _item.sharpen_damage.value : bonus.damage;
                html += "<font color='" + _red + "'><b>" + _factory.getLabel('short_dmg_label') + ":</b> <font color='" + _brown + "'><b>+" + dmg + "</b></font></font>&nbsp;&nbsp;&nbsp;";
            }

            if (bonus.armor > 0 || _item.sharpen_armor.value > 0) {
                var armor:int = _item.sharpen_armor.value > 0 ? _item.sharpen_armor.value : bonus.armor;
                html += "<font color='" + _red + "'><b>" + _factory.getLabel('short_armor_label') + ":</b> <font color='" + _brown + "'><b>+" + armor + "</b></font></font>";
            }

            if (bonus.damage > 0 || bonus.armor > 0)
                html += "<br>";

            if (bonus.speed > 0)
                html += "<font color='" + _red + "'><b>" + _factory.getLabel('short_speed_label') + ":</b> <font color='" + _brown + "'><b>+" + bonus.speed + "</b></font></font>&nbsp;&nbsp;&nbsp;";

            if (bonus.hp > 0)
                html += "<font color='" + _red + "'><b>" + _factory.getLabel('short_hp_label') + ":</b> <font color='" + _brown + "'><b>+" + bonus.hp + "</b></font></font>";

            html += "</p>";
            return html;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
