/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.09.13
 * Time: 14:26
 */
package application.views.popup
{
    import application.events.ApplicationEvent;
    import application.views.screen.bank.BankTiles;

    public class BankExpress extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankExpress()
        {
            super();
            _popupWidth = 480;
            _popupHeight = 365;
            _header = "";
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            super._addCloseButton();
            _addBankVariants();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bankTiles:BankTiles;

        private function _addBankVariants():void
        {
            _bankTiles = new BankTiles();
            addChild(_bankTiles);
            _bankTiles.x = int(20 + _backShape.x);
            _bankTiles.y = int(20 + _backShape.y);
        }

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _closeHandler(event:ApplicationEvent):void
        {
            super.dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
