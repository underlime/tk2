/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.09.13
 * Time: 15:49
 */
package application.views.popup
{
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.cell.Cell;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    public class GreatBuy extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GreatBuy(item:BuyingItem)
        {
            super();
            _item = item;
            _header = TextFactory.instance.getLabel('great_buy_label');
            _popupWidth = 237;
            _popupHeight = 234;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addIcon();
            _addOkButton();
            super.sound.playUISound(UISound.CASH);
        }

        override public function destroy():void
        {
            _okButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item:BuyingItem;
        private var _frame:Bitmap;

        private var _okButton:AcceptButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addIcon():void
        {
            _frame = super.source.getBitmap(Cell.SOURCE);
            _frame.scrollRect = new Rectangle(0, 0, Cell.ITEM_WIDTH, Cell.ITEM_HEIGHT);
            addChild(_frame);

            _frame.x = _popupWidth / 2 - _frame.width / 2 + _backShape.x;
            _frame.y = _backShape.y + 71;

            addChild(_item.picture);
            _item.picture.x = _frame.x + Cell.ITEM_WIDTH / 2 - _item.picture.width / 2;
            _item.picture.y = _frame.y + Cell.ITEM_HEIGHT / 2 - _item.picture.height / 2;
        }

        private function _addOkButton():void
        {
            _okButton = new AcceptButton("OK");
            _okButton.w = 77;
            _okButton.fontSize = 14;

            addChild(_okButton);
            _okButton.x = int(_backShape.x + 80);
            _okButton.y = int(_backShape.y + 175);

            _okButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
