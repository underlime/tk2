/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.08.13
 * Time: 11:51
 */
package application.views.popup
{
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.views.AppView;
    import application.views.screen.common.buttons.CloseButton;
    import application.views.text.AppTextField;

    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.text.TextFormatAlign;

    public class BasePopup extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const DARK_COLOR:uint = 0x754c29;
        public static const MIDDLE_COLOR:uint = 0xddc078;
        public static const LIGHT_COLOR:uint = 0xefdf9a;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BasePopup()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addShape();
            _addTitle();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _popupWidth:int = 309;
        protected var _popupHeight:int = 302;
        protected var _headerSize:int = 16;

        protected var _backShape:Shape;

        protected var _header:String = "";
        protected var _closeButton:CloseButton;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _addCloseButton():void
        {
            if (!super.stage)
                return;

            _closeButton = new CloseButton();
            addChild(_closeButton);
            _closeButton.x = _backShape.x + _popupWidth - _closeButton.w / 2;
            _closeButton.y = (-1) * _closeButton.h / 2 + _backShape.y;

            _closeButton.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtLabel:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        protected function _addBackground():void
        {
            graphics.beginFill(0x000000, .8);
            graphics.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            graphics.endFill();
        }

        protected function _addShape():void
        {
            _backShape = new Shape();
            _backShape.x = AppConfig.APP_WIDTH / 2 - _popupWidth / 2;
            _backShape.y = AppConfig.APP_HEIGHT / 2 - _popupHeight / 2;
            addChild(_backShape);

            _backShape.graphics.beginFill(DARK_COLOR);
            _backShape.graphics.drawRect(0, 0, _popupWidth, _popupHeight);
            _backShape.graphics.endFill();

            var frameShape:Shape = new Shape();
            addChild(frameShape);
            frameShape.x = _backShape.x + 2;
            frameShape.y = _backShape.y + 2;

            frameShape.graphics.beginFill(MIDDLE_COLOR);
            frameShape.graphics.drawRect(0, 0, _popupWidth - 4, _popupHeight - 4);
            frameShape.graphics.endFill();

            var centerShape:Shape = new Shape();
            addChild(centerShape);
            centerShape.x = frameShape.x + 4;
            centerShape.y = frameShape.y + 4;

            centerShape.graphics.beginFill(LIGHT_COLOR);
            centerShape.graphics.drawRect(0, 0, _popupWidth - 12, _popupHeight - 12);
            centerShape.graphics.endFill();
        }

        private function _addTitle():void
        {
            _txtLabel = new AppTextField();
            _txtLabel.size = _headerSize;
            _txtLabel.bold = true;
            _txtLabel.color = 0x3c2415;

            _txtLabel.width = _popupWidth;
            _txtLabel.align = TextFormatAlign.CENTER;
            addChild(_txtLabel);

            _txtLabel.text = _header.toUpperCase();
            _txtLabel.y = _backShape.y + 20;
            _txtLabel.x = _backShape.x;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _closeHandler(event:ApplicationEvent):void
        {
            event.stopImmediatePropagation();
            if (_closeButton && !_closeButton.destroyed) {
                _closeButton.removeEventListener(MouseEvent.CLICK, _closeHandler);
                _closeButton.destroy();
            }
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
