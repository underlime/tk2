/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.09.13
 * Time: 19:50
 */
package application.views.popup
{
    import application.views.TextFactory;
    import application.views.popup.common.RestoreButton;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    public class LowEnergyView extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const EVENT_PIC:String = "EVENT_PICS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LowEnergyView(price:int)
        {
            super();
            _price = price;
            _popupWidth = 309;
            _popupHeight = 302;
            _header = TextFactory.instance.getLabel('low_energy_label');
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addEventPic();
            _addMessage();
            _addOkButton();
            _addRestoreButton();
        }

        override public function destroy():void
        {
            _txtMessage.destroy();
            _okButton.destroy();
            _restoreButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtMessage:AppTextField;

        private var _okButton:AcceptButton;
        private var _restoreButton:RestoreButton;
        private var _price:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addEventPic():void
        {
            var eventPic:Bitmap = super.source.getBitmap(EVENT_PIC);
            eventPic.scrollRect = new Rectangle(0, 100, 280, 100);
            addChild(eventPic);
            eventPic.x = _backShape.x + _popupWidth / 2 - eventPic.width / 2;
            eventPic.y = _backShape.y + 56;
        }

        private function _addMessage():void
        {
            _txtMessage = new AppTextField();
            _txtMessage.color = 0x5e3113;
            _txtMessage.size = 12;
            _txtMessage.width = 280;
            _txtMessage.multiline = true;
            _txtMessage.wordWrap = true;
            _txtMessage.align = TextFormatAlign.CENTER;
            _txtMessage.autoSize = TextFieldAutoSize.LEFT;
            addChild(_txtMessage);
            _txtMessage.text = TextFactory.instance.getLabel('low_energy_message');
            _txtMessage.x = _backShape.x + _popupWidth / 2 - _txtMessage.width / 2;
            _txtMessage.y = _backShape.y + 178;
        }

        private function _addOkButton():void
        {
            _okButton = new AcceptButton("OK");
            _okButton.w = 73;
            addChild(_okButton);
            _okButton.x = _backShape.x + 22;
            _okButton.y = _backShape.y + 244;
        }

        private function _addRestoreButton():void
        {
            _restoreButton = new RestoreButton(_price);
            addChild(_restoreButton);
            _restoreButton.x = _backShape.x + _popupWidth - _restoreButton.w - 22;
            _restoreButton.y = _backShape.y + 244;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get okButton():AcceptButton
        {
            return _okButton;
        }

        public function get restoreButton():RestoreButton
        {
            return _restoreButton;
        }
    }
}

