/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.09.13
 * Time: 16:58
 */
package application.views.popup
{
    import application.views.TextFactory;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    public class ChangeLoginSuccess extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ChangeLoginSuccess(login:String)
        {
            super();
            _login = login;
            _popupWidth = 309;
            _popupHeight = 302;
            _header = TextFactory.instance.getLabel('congratulations_label');
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addEventPic();
            _addMessage();
            _addOkButton();
        }

        override public function destroy():void
        {
            _okButton.destroy();
            _txtMessage.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _getText():String
        {
            var tempMessage:String = TextFactory.instance.getLabel('change_name_success_label');
            return tempMessage.replace("{{NAME}}", _login);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _login:String;

        private var _txtMessage:AppTextField;

        private var _okButton:AcceptButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addEventPic():void
        {
            var eventPic:Bitmap = super.source.getBitmap(LowEnergyView.EVENT_PIC);
            eventPic.scrollRect = new Rectangle(0, 0, 280, 100);
            addChild(eventPic);
            eventPic.x = _backShape.x + _popupWidth / 2 - eventPic.width / 2;
            eventPic.y = _backShape.y + 56;
        }

        private function _addMessage():void
        {
            _txtMessage = new AppTextField();
            _txtMessage.color = 0x5e3113;
            _txtMessage.size = 14;
            _txtMessage.width = 280;
            _txtMessage.multiline = true;
            _txtMessage.wordWrap = true;
            _txtMessage.align = TextFormatAlign.CENTER;
            _txtMessage.autoSize = TextFieldAutoSize.LEFT;
            addChild(_txtMessage);

            _txtMessage.htmlText = _getText();
            _txtMessage.x = _backShape.x + _popupWidth / 2 - _txtMessage.width / 2;
            _txtMessage.y = _backShape.y + 178;
        }

        private function _addOkButton():void
        {
            _okButton = new AcceptButton(TextFactory.instance.getLabel('yes_its_me_label'));
            _okButton.w = 177;
            _okButton.fontSize = 14;

            addChild(_okButton);
            _okButton.x = int(_backShape.x + _popupWidth / 2 - _okButton.w / 2);
            _okButton.y = int(_backShape.y + 240);

            _okButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _clickHandler(event:MouseEvent):void
        {
            dispatchEvent(new Event(Event.CLOSE));
            this.destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
