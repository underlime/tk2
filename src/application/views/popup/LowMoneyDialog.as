/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.09.13
 * Time: 21:03
 */
package application.views.popup
{
    import application.events.ApplicationEvent;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.SpecialButton;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    public class LowMoneyDialog extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const EVENT_PIC:String = "EVENT_PICS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LowMoneyDialog()
        {
            super();
            _popupWidth = 309;
            _popupHeight = 302;
            _header = TextFactory.instance.getLabel('low_money_label');
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addEventPic();
            _addMessage();
            _addBankButton();
            _addCloseButton();
        }

        override public function destroy():void
        {
            _txtMessage.destroy();
            _bankButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtMessage:AppTextField;
        private var _bankButton:SpecialButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addEventPic():void
        {
            var eventPic:Bitmap = super.source.getBitmap(EVENT_PIC);
            eventPic.scrollRect = new Rectangle(0, 300, 280, 100);
            addChild(eventPic);
            eventPic.x = _backShape.x + _popupWidth / 2 - eventPic.width / 2;
            eventPic.y = _backShape.y + 56;
        }

        private function _addMessage():void
        {
            _txtMessage = new AppTextField();
            _txtMessage.color = 0x5e3113;
            _txtMessage.size = 12;
            _txtMessage.width = 280;
            _txtMessage.multiline = true;
            _txtMessage.wordWrap = true;
            _txtMessage.align = TextFormatAlign.CENTER;
            _txtMessage.autoSize = TextFieldAutoSize.LEFT;
            addChild(_txtMessage);
            _txtMessage.text = TextFactory.instance.getLabel('low_money_message');
            _txtMessage.x = _backShape.x + _popupWidth / 2 - _txtMessage.width / 2;
            _txtMessage.y = _backShape.y + 178;
        }

        private function _addBankButton():void
        {
            _bankButton = new SpecialButton(TextFactory.instance.getLabel('in_bank_label'));
            _bankButton.w = 167;
            _bankButton.fontSize = 14;
            addChild(_bankButton);
            _bankButton.x = _backShape.x + _popupWidth / 2 - _bankButton.w / 2;
            _bankButton.y = _backShape.y + 244;
            _bankButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _closeHandler(event:ApplicationEvent):void
        {
            super.dispatchEvent(new Event(Event.CLOSE));
        }

        private function _clickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_EXPRESS_BANK, true));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
