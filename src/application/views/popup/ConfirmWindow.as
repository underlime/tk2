/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.02.14
 * Time: 11:23
 */
package application.views.popup
{
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.buttons.RejectButton;

    import flash.events.MouseEvent;

    public class ConfirmWindow extends AlertWindow
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ConfirmWindow(text:String, okFunction:Function, cancelFunction:Function = null)
        {
            super("", text);
            _okFunction = okFunction;
            _cancelFunction = cancelFunction;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addButtons();
        }

        override public function destroy():void
        {
            _okButton.destroy();
            _cancelButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addOkButton():void
        {

        }


        override protected function _addText():void
        {
            super._addText();
            _txtMessage.y = _backShape.y + 50;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _okFunction:Function;
        private var _cancelFunction:Function;

        private var _okButton:AcceptButton;
        private var _cancelButton:RejectButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addButtons():void
        {
            _okButton = new AcceptButton("OK");
            _cancelButton = new RejectButton(TextFactory.instance.getLabel('cancel_label'));
            _okButton.w = 100;
            _cancelButton.w = 100;
            _okButton.x = _backShape.x + 30;
            _okButton.y = int(_backShape.y + 170);

            _cancelButton.x = int(_backShape.x + _popupWidth - _cancelButton.w - 30);
            _cancelButton.y = int(_backShape.y + 170);
            addChild(_cancelButton);
            addChild(_okButton);

            _cancelButton.addEventListener(MouseEvent.CLICK, _cancelHandler);
            _okButton.addEventListener(MouseEvent.CLICK, _okHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _cancelHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            if (_cancelFunction != null) {
                _cancelFunction.call(null);
            }
            destroy();
        }

        private function _okHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            _okFunction.call(null);
            destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
