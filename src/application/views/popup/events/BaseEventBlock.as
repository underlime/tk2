/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.09.13
 * Time: 20:35
 */
package application.views.popup.events
{
    import application.views.AppView;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.text.AppTextField;

    import flash.display.Shape;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    public class BaseEventBlock extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const DARK_COLOR:uint = 0x754c29;
        public static const MIDDLE_COLOR:uint = 0xddc078;
        public static const LIGHT_COLOR:uint = 0xefdf9a;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseEventBlock(label:String)
        {
            super();
            _label = label;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addHeader();
        }

        private function _addBackground():void
        {
            var backShape:Shape = new Shape();
            addChild(backShape);

            backShape.graphics.beginFill(DARK_COLOR);
            backShape.graphics.drawRect(0, 0, _eventWidth, _eventHeight);
            backShape.graphics.endFill();

            var frameShape:Shape = new Shape();
            addChild(frameShape);
            frameShape.x = backShape.x + 2;
            frameShape.y = backShape.y + 2;

            frameShape.graphics.beginFill(MIDDLE_COLOR);
            frameShape.graphics.drawRect(0, 0, _eventWidth - 4, _eventHeight - 4);
            frameShape.graphics.endFill();

            var centerShape:Shape = new Shape();
            addChild(centerShape);
            centerShape.x = frameShape.x + 4;
            centerShape.y = frameShape.y + 4;

            centerShape.graphics.beginFill(LIGHT_COLOR);
            centerShape.graphics.drawRect(0, 0, _eventWidth - 12, _eventHeight - 12);
            centerShape.graphics.endFill();
        }

        private function _addHeader():void
        {
            var txtLabel:ShadowAppText = new ShadowAppText(AppTextField.DARK_BROWN);
            txtLabel.autoSize = TextFieldAutoSize.NONE;
            txtLabel.width = _eventWidth;
            txtLabel.align = TextFormatAlign.CENTER;
            txtLabel.size = 16;
            addChild(txtLabel);
            txtLabel.text = _label;
            txtLabel.y = (-1) * txtLabel.textHeight * 2 / 3;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _eventWidth:int = 100;
        protected var _eventHeight:int = 100;

        private var _label:String = "";

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get eventWidth():int
        {
            return _eventWidth;
        }

        public function get eventHeight():int
        {
            return _eventHeight;
        }
    }
}
