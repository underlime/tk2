/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.09.13
 * Time: 11:52
 */
package application.views.popup.events.bonus
{
    import application.views.screen.common.cell.Cell;
    import application.views.screen.common.text.CellTextCounter;
    import application.views.text.AppTextField;

    public class BonusCell extends Cell
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BonusCell(iconSource:String, count:int)
        {
            super();
            _iconSource = iconSource;
            _count = count;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _setupCounter();

            buttonMode = true;
            useHandCursor = true;
            mouseEnabled = true;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _getIconSource():void
        {
            _source = _iconSource;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _count:int;
        private var _iconSource:String;
        private var _txtCount:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupCounter():void
        {
            var countValue:int = _count;
            if (countValue > 1) {
                _txtCount = new CellTextCounter();
                _txtCount.y = 43;
                _txtCount.x = _w - 9;
                addChild(_txtCount);

                var count:int = countValue;
                _txtCount.text = "x" + count.toString();
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
