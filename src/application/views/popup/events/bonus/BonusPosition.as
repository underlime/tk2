/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.09.13
 * Time: 11:39
 */
package application.views.popup.events.bonus
{
    import application.events.ApplicationEvent;
    import application.views.screen.common.buttons.AppSpriteButton;
    import application.views.text.AppTextField;

    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    public class BonusPosition extends AppSpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "DAILY_PRIZE";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BonusPosition(icon:String, count:int, note:String, isActive:Boolean = true)
        {
            super(SOURCE);
            _icon = icon;
            _count = count;
            _note = note;
            _isActive = isActive;

            _w = 94;
            _h = 126;

            _overPosition = 126;
            _downPosition = 126;
            _selectedPosition = 126;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addItemCell();
            _addDescription();

            if (!_isActive) {
                this.alpha = .5;
            }

            buttonMode = true;
            useHandCursor = true;
            this.scrollRect = new Rectangle(0, 0, _w, _h);
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        override public function select():void
        {
            super.select();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TAB_CHOOSE, true));
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            _cell.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _cell:BonusCell;

        private var _icon:String;
        private var _count:int;
        private var _note:String;

        private var _isActive:Boolean;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addItemCell():void
        {
            _cell = new BonusCell(_icon, _count);
            addChild(_cell);
            _cell.x = 11;
            _cell.y = 10;
        }

        private function _addDescription():void
        {
            var txtDay:AppTextField = new AppTextField();
            txtDay.size = 12;
            txtDay.color = AppTextField.DARK_BROWN;
            txtDay.bold = true;
            txtDay.width = _w;
            txtDay.align = TextFormatAlign.CENTER;
            addChild(txtDay);
            txtDay.y = 92;
            txtDay.text = _note.toString()
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _onOverHandler(e:MouseEvent):void
        {
            super._onOverHandler(e);
            _cell.select();
        }

        override protected function _onOutHandler(e:MouseEvent):void
        {
            super._onOutHandler(e);
            _cell.unselect();
        }

        private function _clickHandler(event:MouseEvent):void
        {
            select();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
