/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.09.13
 * Time: 10:46
 */
package application.views.popup.events.bonus
{
    import application.common.Colors;
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.popup.BasePopup;
    import application.views.screen.base.ScreenTitle;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.text.AppTextField;

    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.text.TextFormatAlign;

    public class EveryDayBonus extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EveryDayBonus()
        {
            super();
            _popupWidth = 722;
            _popupHeight = 349;
            _header = "";
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHeader();
            _addNote();
            _addBonusVariants();
            _addButton();
        }

        override public function destroy():void
        {
            _bonusVariants.destroy();
            _button.destroy();
            _screenTitle.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addBackground():void
        {
            graphics.beginFill(0x000000, 0);
            graphics.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            graphics.endFill();
        }

        override protected function _addShape():void
        {
            _backShape = new Shape();
            addChild(_backShape);
            _backShape.graphics.beginFill(0x000000, 0.8);
            _backShape.graphics.drawRoundRect(0, 0, _popupWidth, _popupHeight, 30, 30);
            _backShape.graphics.endFill();

            _backShape.x = AppConfig.APP_WIDTH / 2 - _popupWidth / 2;
            _backShape.y = AppConfig.APP_HEIGHT / 2 - _popupHeight / 2;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bonusVariants:BonusVariants;
        private var _button:AcceptButton;
        private var _screenTitle:ScreenTitle;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHeader():void
        {
            _screenTitle = new ScreenTitle(TextFactory.instance.getLabel("everyday_bonus"));
            addChild(_screenTitle);
            _screenTitle.x = AppConfig.APP_WIDTH / 2 - _screenTitle.width / 2;
            _screenTitle.y = _backShape.y - _screenTitle.height / 2 + 10;
        }

        private function _addNote():void
        {
            var note:AppTextField = new AppTextField();
            note.size = 18;
            note.color = Colors.WHITE;
            note.bold = true;
            note.width = _popupWidth;
            note.align = TextFormatAlign.CENTER;
            addChild(note);
            note.text = TextFactory.instance.getLabel("everyday_bonus_note");
            note.x = _backShape.x;
            note.y = _backShape.y + 60;
        }

        private function _addBonusVariants():void
        {
            _bonusVariants = new BonusVariants();
            addChild(_bonusVariants);
            _bonusVariants.x = 17 + _backShape.x;
            _bonusVariants.y = 133 + _backShape.y;
        }

        private function _addButton():void
        {
            _button = new AcceptButton(TextFactory.instance.getLabel("take_present_label"));
            _button.w = 176;
            addChild(_button);
            _button.x = int(_popupWidth / 2 - _button.w / 2 + _backShape.x);
            _button.y = int(_backShape.y + 285);

            _button.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _clickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            dispatchEvent(new ApplicationEvent(ApplicationEvent.CLOSE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
