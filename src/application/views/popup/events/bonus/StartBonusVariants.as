/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 27.09.13
 * Time: 15:56
 */
package application.views.popup.events.bonus
{
    import application.models.ModelData;
    import application.models.market.Item;
    import application.models.market.MarketItems;
    import application.models.user.User;
    import application.views.TextFactory;
    import application.views.screen.common.tabs.AppTabs;

    import framework.core.display.tabs.ITab;
    import framework.core.struct.data.ModelsRegistry;

    import mx.utils.ObjectUtil;

    public class StartBonusVariants extends AppTabs
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StartBonusVariants()
        {
            var tabs:Vector.<ITab> = new <ITab>[];
            var item:Item;
            for each (var data:Object in _userModel.userOptInfo.start_bonuses_config.value) {
                if (data['item_id']) {
                    item = _itemsModel.getItemById(parseInt(data['item_id']));
                    if (item)
                        tabs.push(new BonusPosition(
                                'i'+item.picture.value,
                                data['count'], data['name_ru']
                        ));
                }
                else if (data['type'] == 'tomatos') {
                    tabs.push(new BonusPosition(
                            'ICON_TOMATOS',
                            data['count'],
                            data['name_ru']
                    ));
                }
                else if (data['type'] == 'ferros') {
                    tabs.push(new BonusPosition(
                            'ICON_FERROS',
                            data['count'],
                            data['name_ru']
                    ));
                }
            }
            super(tabs);
            _dx = 5;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _userModel:User = ModelsRegistry.getModel(ModelData.USER) as User;
        private var _itemsModel:MarketItems = ModelsRegistry.getModel(ModelData.MARKET_ITEMS) as MarketItems;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
