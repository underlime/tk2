/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.09.13
 * Time: 12:17
 */
package application.views.popup.events.bonus
{
    import application.models.ModelData;
    import application.models.events.EventsModel;
    import application.models.market.Item;
    import application.models.market.MarketItems;
    import application.models.user.User;
    import application.models.user.UserOptInfo;
    import application.views.TextFactory;
    import application.views.screen.common.tabs.AppTabs;

    import framework.core.display.tabs.ITab;
    import framework.core.struct.data.ModelsRegistry;

    public class BonusVariants extends AppTabs
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BonusVariants()
        {
            var userModel:User = ModelsRegistry.getModel(ModelData.USER) as User;
            var optInfo:UserOptInfo = userModel.userOptInfo;
            var eventsModel:EventsModel = ModelsRegistry.getModel(ModelData.EVENTS) as EventsModel;
            var dayBonusesConfig:Object = optInfo.day_bonuses_config.value;

            var dayLabel:String = TextFactory.instance.getLabel("day_label");
            var tabs:Vector.<ITab> = new <ITab>[];

            var day:int, count:int, icon:String, isActive:Boolean, item:Item;
            for (var i:int=0; i<7; ++i) {
                day = i + 1;
                if (day == 3 || day == 7) {
                    count = dayBonusesConfig['item_days'][day]['count'];
                    if (eventsModel.dayBonus.day.value == day) {
                        item = _itemsModel.getItemById(eventsModel.dayBonus.item_id.value);
                        if (item)
                            icon = 'i'+item.picture.value;
                        else
                            icon = 'ICON_BLACK_BOX';
                    }
                    else {
                        icon = (day == 3) ? "ICON_MEDICINE" : "ICON_GRENADE";
                    }
                }
                else {
                    count = dayBonusesConfig['day_tomatos'] * day;
                    icon = "ICON_TOMATOS";
                }
                isActive = (day <= eventsModel.dayBonus.day.value);
                tabs.push(new BonusPosition(icon, count, dayLabel + " " + day, isActive));
            }

            super(tabs);
            _dx = 5;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _tabs[4].select();

            this.mouseEnabled = false;
            this.mouseChildren = false;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _itemsModel:MarketItems = ModelsRegistry.getModel(ModelData.MARKET_ITEMS) as MarketItems;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
