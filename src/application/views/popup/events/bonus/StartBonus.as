/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.09.13
 * Time: 13:49
 */
package application.views.popup.events.bonus
{
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.views.TextFactory;
    import application.views.popup.BasePopup;
    import application.views.screen.base.ScreenTitle;
    import application.views.text.AppTextField;

    import flash.display.Shape;
    import flash.events.Event;
    import flash.text.TextFormatAlign;

    import framework.core.tools.Declension;

    public class StartBonus extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StartBonus(friendsCount:int)
        {
            super();
            _friendsCount = friendsCount;
            _popupWidth = 540;
            _popupHeight = 290;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHeader();
            _addDescription();
            _addBonusVariants();
        }

        override public function destroy():void
        {
            _screenTitle.destroy();
            _bonusVariants.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addBackground():void
        {
            graphics.beginFill(0x000000, 0);
            graphics.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            graphics.endFill();
        }

        override protected function _addShape():void
        {
            _backShape = new Shape();
            addChild(_backShape);
            _backShape.graphics.beginFill(0x000000, 0.8);
            _backShape.graphics.drawRoundRect(0, 0, _popupWidth, _popupHeight, 30, 30);
            _backShape.graphics.endFill();

            _backShape.x = AppConfig.APP_WIDTH / 2 - _popupWidth / 2;
            _backShape.y = AppConfig.APP_HEIGHT / 2 - _popupHeight / 2;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _friendsCount:int;
        private var _screenTitle:ScreenTitle;
        private var _bonusVariants:StartBonusVariants;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHeader():void
        {
            _screenTitle = new ScreenTitle(TextFactory.instance.getLabel("gifts_time_label"));
            addChild(_screenTitle);
            _screenTitle.x = AppConfig.APP_WIDTH / 2 - _screenTitle.width / 2;
            _screenTitle.y = _backShape.y - _screenTitle.height / 2 + 10;
        }

        private function _addDescription():void
        {
            var count:int = _friendsCount;
            var friends:String = Declension.getVariant(count, ["друг", "друга", "друзей"]);
            var theirs:String = Declension.getVariant(count, ["него", "них", "них"]);

            var friendsInGame:String = TextFactory.instance.getLabel("count_friends_in_game")
                    .replace("{{count}}", "<font color='#ffffff'>" + count.toString() + "</font>")
                    .replace("{{friends}}", friends);

            var chooseGift:String = TextFactory.instance.getLabel("choose_gift_friend")
                    .replace("{{friends}}", theirs);

            var txtLabel:AppTextField = new AppTextField();
            txtLabel.size = 16;
            txtLabel.bold = true;
            txtLabel.width = _popupWidth;
            txtLabel.align = TextFormatAlign.CENTER;
            txtLabel.wordWrap = true;
            txtLabel.multiline = true;
            addChild(txtLabel);
            txtLabel.htmlText = "<font color='#ffffff'>" + friendsInGame + "</font><br>" + "<font color='#ffffff'>" + chooseGift + "</font>";
            txtLabel.x = _backShape.x;
            txtLabel.y = _backShape.y + 54;
        }

        private function _addBonusVariants():void
        {
            _bonusVariants = new StartBonusVariants();
            addChild(_bonusVariants);
            _bonusVariants.x = 25 + _backShape.x;
            _bonusVariants.y = 133 + _backShape.y;

            _bonusVariants.addEventListener(Event.CHANGE, _changeHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            var bonusEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.TAB_CHOOSE);
            bonusEvent.data['position'] = _bonusVariants.selectedTab;
            dispatchEvent(bonusEvent);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
