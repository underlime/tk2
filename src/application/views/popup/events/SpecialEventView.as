/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 11.10.13
 * Time: 21:00
 */
package application.views.popup.events
{
    import application.controllers.ApplicationController;
    import application.helpers.WallPicturesRegistry;
    import application.models.events.SpecialEventRecord;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.SpecialButton;
    import application.views.screen.specials.SpecialLock;
    import application.views.text.AppTextField;

    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;

    import framework.core.error.ApplicationError;
    import framework.core.tools.wall_post.WallPostTool;

    public class SpecialEventView extends BaseEventBlock
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialEventView(model:SpecialEventRecord, appController:ApplicationController)
        {
            super(TextFactory.instance.getLabel('new_special_label'));
            _model = model;
            _appController = appController;
            _eventWidth = 722;
            _eventHeight = 100;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addIcon();
            _addName();
            _addDescription();
            _addTellButton();
        }

        override public function destroy():void
        {
            if (_tellButton && !_tellButton.destroyed)
                _tellButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _model:SpecialEventRecord;
        private var _tellButton:SpecialButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addIcon():void
        {
            var button:SpecialLock = new SpecialLock(_model.picture.value);
            addChild(button);
            button.x = 10;
            button.y = 10;
        }

        private function _addName():void
        {
            var txtName:AppTextField = new AppTextField();
            txtName.size = 16;
            txtName.color = AppTextField.DARK_BROWN;
            txtName.bold = true;
            txtName.autoSize = TextFieldAutoSize.LEFT;
            addChild(txtName);
            txtName.text = _model.name_ru.value;
            txtName.x = 107;
            txtName.y = 29;
        }

        private function _addDescription():void
        {
            var txtDescription:AppTextField = new AppTextField();
            txtDescription.size = 12;
            txtDescription.color = AppTextField.LIGHT_BROWN;
            txtDescription.autoSize = TextFieldAutoSize.LEFT;
            txtDescription.wordWrap = true;
            txtDescription.multiline = true;
            txtDescription.width = 400;
            addChild(txtDescription);
            txtDescription.text = _model.text_ru.value;
            txtDescription.x = 107;
            txtDescription.y = 56;
        }

        private function _addTellButton():void
        {
            _tellButton = new SpecialButton(TextFactory.instance.getLabel("share_label"));
            _tellButton.w = 161;
            addChild(_tellButton);
            _tellButton.x = 535;
            _tellButton.y = 30;

            _tellButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            _appController.loadingView.showLoader();
            var postTool:WallPostTool = new WallPostTool();
            postTool.pictureSocNetId = WallPicturesRegistry.special;
            postTool.postText = TextFactory.instance.getLabel("special_share_label")
                    .replace("{name}", _model.name_ru.value);
            postTool.addEventListener(Event.COMPLETE, _onWallPostSuccess);
            postTool.addEventListener(ErrorEvent.ERROR, _onWallPostError);
            postTool.addPost();
        }

        private function _onWallPostSuccess(e:Event):void
        {
            _appController.loadingView.hideLoader();
            e.target.destroy();
        }

        private function _onWallPostError(e:ErrorEvent):void
        {
            _appController.loadingView.hideLoader();
            e.target.destroy();

            var appError:ApplicationError = new ApplicationError();
            appError.message = e.text;
            _appController.appError(appError);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
