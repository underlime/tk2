/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.08.13
 * Time: 16:34
 */
package application.views.popup
{
    import application.views.screen.common.navigation.Counter;

    import flash.display.Bitmap;
    import flash.events.Event;

    public class MultiBuyDialog extends BuyDialog
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const CROSS:String = "CROSS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MultiBuyDialog(item:BuyingItem)
        {
            super(item);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _updateFrame();
            _addCounter();
            _addCross();
        }

        override public function destroy():void
        {
            _counter.removeEventListener(Event.CHANGE, _changeHandler);
            _counter.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _getCount():int
        {
            var count:int = 1;
            if (_counter)
                count = _counter.count;

            return count;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _counter:Counter;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addCounter():void
        {
            _counter = new Counter();
            _counter.arrowMargin = 5;
            _counter.max = 30;

            addChild(_counter);
            _counter.x = 152 + _backShape.x;
            _counter.y = 81 + _backShape.y;

            _counter.addEventListener(Event.CHANGE, _changeHandler);
        }

        private function _updateFrame():void
        {
            _frame.x = _backShape.x + 42;
            item.picture.x = _frame.x + 6;
        }

        private function _addCross():void
        {
            var cross:Bitmap = super.source.getBitmap(CROSS);
            addChild(cross);
            cross.x = 130 + _backShape.x;
            cross.y = _backShape.y + 92;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _ferrosPrice.price = _counter.count * item.price_ferros;
            if (_tomatosPrice)
                _tomatosPrice.price = _counter.count * item.price_tomatos;

            _updatePositions();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
