/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.09.13
 * Time: 19:14
 */
package application.views.popup
{
    import application.views.TextFactory;
    import application.views.popup.common.TomatosPrice;
    import application.views.screen.common.buttons.TomatosButton;
    import application.views.screen.common.cell.Cell;

    import flash.display.Bitmap;
    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    public class SellDialog extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SellDialog(item:BuyingItem, buyCallBack:Function)
        {
            super();

            _item = item;
            _buyCallBack = buyCallBack;
            _popupWidth = 312;
            _popupHeight = 262;
            _header = item.item_name;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addDelimiter();
            _addItemIcon();
            _addTomatosButton();
            _addPrice();
            super._addCloseButton();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        override public function destroy():void
        {
            _tomatosButton.removeEventListener(MouseEvent.CLICK, _tomatosClickHandler);
            _tomatosButton.destroy();
            _tomatosPrice.destroy();
            super.destroy();
        }

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _buyCallBack:Function;

        private var _item:BuyingItem;
        private var _frame:Bitmap;

        private var _tomatosPrice:TomatosPrice;
        private var _tomatosButton:TomatosButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addDelimiter():void
        {
            var shape:Shape = new Shape();
            addChild(shape);
            shape.x = _backShape.x;
            shape.y = _backShape.y + 158;

            shape.graphics.beginFill(BasePopup.MIDDLE_COLOR);
            shape.graphics.drawRect(2, 0, _popupWidth - 4, 4);
            shape.graphics.endFill();
        }

        private function _addItemIcon():void
        {
            _frame = super.source.getBitmap(Cell.SOURCE);
            _frame.scrollRect = new Rectangle(0, 0, Cell.ITEM_WIDTH, Cell.ITEM_HEIGHT);
            addChild(_frame);

            _frame.x = _popupWidth / 2 - _frame.width / 2 + _backShape.x;
            _frame.y = _backShape.y + 65;

            addChild(_item.picture);
            _item.picture.x = _frame.x + 6;
            _item.picture.y = _frame.y + 6;
        }

        protected function _addTomatosButton():void
        {
            _tomatosButton = new TomatosButton();
            _tomatosButton.label = TextFactory.instance.getLabel('sell_label');
            addChild(_tomatosButton);
            _tomatosButton.y = _backShape.y + 205;
            _tomatosButton.x = _popupWidth / 2 - _tomatosButton.w / 2 + _backShape.x;

            _tomatosButton.addEventListener(MouseEvent.CLICK, _tomatosClickHandler);
        }

        private function _addPrice():void
        {
            _tomatosPrice = new TomatosPrice(_item.sell_price);
            addChild(_tomatosPrice);
            _tomatosPrice.x = _tomatosButton.x + _tomatosButton.w / 2 - _tomatosPrice.priceWidth / 2;
            _tomatosPrice.y = _tomatosButton.y - 32;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _tomatosClickHandler(event:MouseEvent):void
        {
            if (_buyCallBack is Function)
                _buyCallBack.call();

            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
