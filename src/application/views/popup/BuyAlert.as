/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.09.13
 * Time: 15:17
 */
package application.views.popup
{
    import application.views.TextFactory;
    import application.views.screen.common.buttons.FerrosPriceButton;

    import flash.events.MouseEvent;

    public class BuyAlert extends AlertWindow
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BuyAlert(header:String, text:String, price:int, buyCallBack:Function)
        {
            super(header, text);
            _price = price;
            _buyCallBack = buyCallBack;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addCloseButton();
        }

        override public function destroy():void
        {
            _buyButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        override protected function _addOkButton():void
        {
            _buyButton = new FerrosPriceButton(TextFactory.instance.getLabel("buy_label"), _price);
            _buyButton.w = 140;
            _buyButton.fontSize = 13;
            addChild(_buyButton);
            _buyButton.x = int(_popupWidth / 2 - _buyButton.w / 2 + _backShape.x);
            _buyButton.y = int(_backShape.y + 170);

            _buyButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _buyButton:FerrosPriceButton;
        private var _price:int;

        private var _buyCallBack:Function;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            _buyCallBack.call(null);
            this.destroy();
        }

    }
}
