/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.06.13
 * Time: 16:58
 */
package application.views.popup
{
    import application.models.chars.CharDetail;
    import application.models.chars.CharGroup;
    import application.models.market.Item;
    import application.views.screen.common.CommonData;

    import flash.display.DisplayObject;

    import framework.core.tools.SourceManager;

    public class BuyingItem
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BuyingItem()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var item_id:int = 0;
        public var item_name:String = "";
        public var picture:DisplayObject;
        public var price_ferros:int = 0;
        public var price_tomatos:int = 0;
        public var sell_price:int = 0;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function importFromItem(item:Item):void
        {
            item_id = item.item_id.value;
            item_name = item.name_ru.value;
            picture = SourceManager.instance.getBitmap(CommonData.ICON_PREFIX + item.picture.value);
            price_ferros = item.price_ferros.value;
            price_tomatos = item.price_tomatos.value;
            sell_price = item.sell_price_tomatos.value;
        }

        public function importFromChar(char:CharDetail):void
        {
            item_id = char.char_detail_id.value;
            if (char.group.value == CharGroup.BODIES.toString())
                picture = SourceManager.instance.getMovieClip(char.mc_2_name.value);
            else
                picture = SourceManager.instance.getBitmap(CommonData.ICON_PREFIX + char.mc_1_name.value);
            price_ferros = char.price_ferros.value;
            price_tomatos = char.price_tomatos.value;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
