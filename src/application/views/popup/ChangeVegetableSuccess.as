/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 05.09.13
 * Time: 20:49
 */
package application.views.popup
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.inventory.Inventory;
    import application.models.inventory.InventoryHelper;
    import application.models.market.Item;
    import application.models.user.User;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.constructor.builders.BattleUnit;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.buttons.SpecialButton;

    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.core.socnet.SocNet;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;

    public class ChangeVegetableSuccess extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ChangeVegetableSuccess()
        {
            super();
            _popupWidth = 384;
            _popupHeight = 299;
            _header = TextFactory.instance.getLabel('change_vegetable_success');
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addUnit();
            if (SocNet.instance().photosUploadAllowed) {
                _addTellButton();
            }
            _addOkButton();

            _addCloseButton();
            super.sound.playUISound(UISound.SUCCESS);
        }

        override public function destroy():void
        {
            if (_unit && !_unit.destroyed) {
                _unit.destroy();
            }
            if (_unitContainer && !_unitContainer.destroyed) {
                _unitContainer.destroy();
            }
            if (_tellButton && !_tellButton.destroyed) {
                _tellButton.destroy();
            }
            if (_okButton && !_okButton.destroyed) {
                _okButton.destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _unitContainer:View;
        private var _unit:BattleUnit;

        private var _tellButton:SpecialButton;
        private var _okButton:AcceptButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addUnit():void
        {
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;
            var inventory:Inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;
            var items:Vector.<Item> = InventoryHelper.ItemsArray2Vector(inventory.getBuildingItems());

            _unitContainer = new View();
            _unitContainer.x = 147 + _backShape.x;
            _unitContainer.y = 125 + _backShape.y;
            addChild(_unitContainer);

            _unit = new BattleUnit(user.userInfo, items);
            _unit.x = 0;
            _unit.y = 0;
            _unit.scaleX = .8;
            _unit.scaleY = .8;
            _unit.win();

            _unitContainer.addChild(_unit);

            var bounds:Rectangle = _unit.getBounds(_unitContainer);
            _unit.x -= bounds.x;
            _unit.y -= bounds.y;
            _unitContainer.x += bounds.x;
            _unitContainer.y += bounds.y;
        }

        private function _addTellButton():void
        {
            _tellButton = new SpecialButton(TextFactory.instance.getLabel('share_label'));
            _tellButton.w = 125;
            _tellButton.x = int(31 + _backShape.x);
            _tellButton.y = int(244 + _backShape.y);

            addChild(_tellButton);

            _tellButton.addEventListener(MouseEvent.CLICK, _tellHandler);
        }

        private function _addOkButton():void
        {
            _okButton = new AcceptButton(TextFactory.instance.getLabel('return_to_game'));
            _okButton.w = 175;
            _okButton.fontSize = 14;
            addChild(_okButton);

            _okButton.x = int(177 + _backShape.x);
            _okButton.y = int(244 + _backShape.y);
            _okButton.addEventListener(MouseEvent.CLICK, _okHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _tellHandler(event:MouseEvent):void
        {
            var shareEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.SHARE, true);
            dispatchEvent(shareEvent);
        }

        private function _okHandler(event:MouseEvent):void
        {
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get unitContainer():View
        {
            return _unitContainer;
        }
    }
}
