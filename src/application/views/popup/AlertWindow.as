/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 09.09.13
 * Time: 12:36
 */
package application.views.popup
{
    import application.sound.lib.UISound;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.text.AppTextField;

    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    public class AlertWindow extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AlertWindow(header:String, text:String)
        {
            super();
            _header = header;
            _text = text;
            _popupWidth = 309;
            _popupHeight = 231;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addText();
            _addOkButton();
        }

        override public function destroy():void
        {
            if (_okButton) {
                _okButton.removeEventListener(MouseEvent.CLICK, _clickHandler);
                _okButton.destroy();
            }

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _text:String;
        protected var _txtMessage:AppTextField;
        private var _okButton:AcceptButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        protected function _addText():void
        {
            _txtMessage = new AppTextField();
            _txtMessage.color = AppTextField.LIGHTER_BROWN;
            _txtMessage.size = 14;
            _txtMessage.width = 240;
            _txtMessage.multiline = true;
            _txtMessage.wordWrap = true;
            _txtMessage.align = TextFormatAlign.CENTER;
            _txtMessage.autoSize = TextFieldAutoSize.LEFT;
            addChild(_txtMessage);

            _txtMessage.text = _text;
            _txtMessage.x = _backShape.x + _popupWidth / 2 - _txtMessage.width / 2;
            _txtMessage.y = _backShape.y + 83;
        }

        protected function _addOkButton():void
        {
            _okButton = new AcceptButton("OK");
            _okButton.w = 120;
            addChild(_okButton);
            _okButton.x = int(_popupWidth / 2 - _okButton.w / 2 + _backShape.x);
            _okButton.y = int(_backShape.y + 170);

            _okButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
