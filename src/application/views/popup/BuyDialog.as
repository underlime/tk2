/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.08.13
 * Time: 14:05
 */
package application.views.popup
{
    import application.common.Currency;
    import application.events.BuyEvent;
    import application.sound.lib.UISound;
    import application.views.popup.common.FerrosPrice;
    import application.views.popup.common.TomatosPrice;
    import application.views.screen.common.buttons.FerrosButton;
    import application.views.screen.common.buttons.TomatosButton;
    import application.views.screen.common.cell.Cell;

    import flash.display.Bitmap;
    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    public class BuyDialog extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BuyDialog(item:BuyingItem)
        {
            super();
            _item = item;
            _popupWidth = 312;
            _popupHeight = 262;
            _header = item.item_name;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addDelimiter();
            _addItemIcon();
            _addFerrosButton();
            _addFerrosPrice();

            if (_item.price_tomatos > 0) {
                _addTomatosButton();
                _addTomatosPrice();
            }

            _updatePositions();
            super._addCloseButton();
        }

        override public function destroy():void
        {
            _ferrosButton.destroy();
            if (_tomatoButton && !_tomatoButton.destroyed)
                _tomatoButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _ferrosPrice:FerrosPrice;
        protected var _tomatosPrice:TomatosPrice;

        protected var _frame:Bitmap;

        protected var _item:BuyingItem;
        protected var _ferrosButton:FerrosButton;
        protected var _tomatoButton:TomatosButton;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _updatePositions():void
        {
            _ferrosPrice.x = _ferrosButton.x + _ferrosButton.w / 2 - _ferrosPrice.priceWidth / 2;
            if (_tomatosPrice && _tomatoButton)
                _tomatosPrice.x = _tomatoButton.x + _tomatoButton.w / 2 - _tomatosPrice.priceWidth / 2;
        }

        protected function _getCount():int
        {
            return 1;
        }

        protected function _addDelimiter():void
        {
            var shape:Shape = new Shape();
            addChild(shape);
            shape.x = _backShape.x;
            shape.y = _backShape.y + 158;

            shape.graphics.beginFill(BasePopup.MIDDLE_COLOR);
            shape.graphics.drawRect(2, 0, _popupWidth - 4, 4);
            shape.graphics.endFill();
        }

        protected function _addItemIcon():void
        {
            _frame = super.source.getBitmap(Cell.SOURCE);
            _frame.scrollRect = new Rectangle(0, 0, Cell.ITEM_WIDTH, Cell.ITEM_HEIGHT);
            addChild(_frame);

            _frame.x = _popupWidth / 2 - _frame.width / 2 + _backShape.x;
            _frame.y = _backShape.y + 65;

            addChild(_item.picture);
            _item.picture.x = _frame.x + Cell.ITEM_WIDTH / 2 - _item.picture.width / 2;
            _item.picture.y = _frame.y + Cell.ITEM_HEIGHT / 2 - _item.picture.height / 2;
        }

        protected function _addFerrosButton():void
        {
            _ferrosButton = new FerrosButton();
            addChild(_ferrosButton);
            _ferrosButton.y = _backShape.y + 205;
            _ferrosButton.x = _popupWidth / 2 - _ferrosButton.w / 2 + _backShape.x;

            _ferrosButton.addEventListener(MouseEvent.CLICK, _ferrosClickHandler);
        }

        protected function _addTomatosButton():void
        {
            _tomatoButton = new TomatosButton();
            addChild(_tomatoButton);

            _tomatoButton.y = _backShape.y + 205;

            _ferrosButton.x = 166 + _backShape.x;
            _tomatoButton.x = 19 + _backShape.x;

            _tomatoButton.addEventListener(MouseEvent.CLICK, _tomatosClickHandler);
        }

        protected function _addFerrosPrice():void
        {
            _ferrosPrice = new FerrosPrice(_item.price_ferros);
            addChild(_ferrosPrice);

            _ferrosPrice.y = _ferrosButton.y - 32;
        }

        protected function _addTomatosPrice():void
        {
            _tomatosPrice = new TomatosPrice(_item.price_tomatos);
            addChild(_tomatosPrice);

            _tomatosPrice.y = _tomatoButton.y - 32;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _ferrosClickHandler(e:MouseEvent):void
        {
            var event:BuyEvent = new BuyEvent(BuyEvent.BUY);
            event.currency = Currency.FERROS;
            event.count = _getCount();
            super.dispatchEvent(event);
            super.sound.playUISound(UISound.CLICK);
        }

        protected function _tomatosClickHandler(e:MouseEvent):void
        {
            var event:BuyEvent = new BuyEvent(BuyEvent.BUY);
            event.currency = Currency.TOMATOS;
            event.count = _getCount();
            super.dispatchEvent(event);
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get item():BuyingItem
        {
            return _item;
        }
    }
}
