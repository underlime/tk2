/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.08.13
 * Time: 15:43
 */
package application.views.popup.common
{
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;

    import framework.core.struct.view.View;

    import org.casalib.util.NumberUtil;

    public class PriceView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PriceView(icon:Bitmap, price:int)
        {
            super();
            _icon = icon;
            _price = price;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addPrice();
            _addIcon();
            _updatePositions();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;
        private var _price:int;

        private var _txtPrice:ShadowAppText;

        private var _prefix:String = "";

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addPrice():void
        {
            _txtPrice = new ShadowAppText(0x653b28);

            _txtPrice.size = 14;
            _txtPrice.color = 0xffffff;
            _txtPrice.autoSize = TextFieldAutoSize.LEFT;
            _txtPrice.bold = true;

            addChild(_txtPrice);
            _txtPrice.text = _prefix + NumberUtil.format(_price, " ");
        }

        private function _addIcon():void
        {
            addChild(_icon);
            _txtPrice.y = _icon.height / 2 - _txtPrice.textHeight / 2 - 2;
        }

        private function _updatePositions():void
        {
            _icon.x = _txtPrice.x + _txtPrice.textWidth + 7;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set price(value:int):void
        {
            _price = value;
            if (_txtPrice) {
                _txtPrice.text = NumberUtil.format(_price, " ");
                _updatePositions();
            }

            if (_price == 0)
                this.visible = false;
            else
                this.visible = true;
        }

        public function get priceWidth():Number
        {
            var width:Number = 0;
            if (_txtPrice && _icon)
                width = _txtPrice.textWidth + _icon.width + 7;
            return width;
        }

        public function get prefix():String
        {
            return _prefix;
        }

        public function set prefix(value:String):void
        {
            _prefix = value;
        }
    }
}
