/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.08.13
 * Time: 15:50
 */
package application.views.popup.common
{
    import application.views.screen.common.CommonData;

    import flash.display.Bitmap;

    import framework.core.tools.SourceManager;

    public class FerrosPrice extends PriceView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FerrosPrice(price:int)
        {
            var icon:Bitmap = SourceManager.instance.getBitmap(CommonData.FERROS_SMALL_ICON_BUTTON);
            super(icon, price);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
