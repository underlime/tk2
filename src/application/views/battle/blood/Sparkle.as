/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.08.13
 * Time: 20:06
 */
package application.views.battle.blood
{
    import flash.filters.GlowFilter;

    public class Sparkle extends Blood
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SPARKLE:String = "SparkleBlock";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Sparkle()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _defineBlood():void
        {
            _blood = super.source.getMovieClip(SPARKLE);
        }

        override protected function _addFilter():void
        {
            var filter1:GlowFilter = new GlowFilter(0xccff00, 1, 10, 10, 1.5, 1);
            var filter2:GlowFilter = new GlowFilter(0xff0000, 1, 5, 5, 1, 1);
            this.filters = [filter1, filter2];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
