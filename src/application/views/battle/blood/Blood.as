/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.08.13
 * Time: 16:11
 */
package application.views.battle.blood
{
    import application.config.AppConfig;
    import application.config.AppSettings;
    import application.views.constructor.helpers.AnimationHelper;

    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.filters.BevelFilter;
    import flash.geom.ColorTransform;

    import framework.core.helpers.MathHelper;
    import framework.core.storage.LocalStorage;
    import framework.core.struct.view.View;
    import framework.core.utils.DestroyUtils;

    public class Blood extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BLOOD:String = "BloodSmallDmg";
        public static const BLOOD_COUNT:int = 5;
        public static const EXECUTION:String = "BloodSmallDmg7_execution";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Blood()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            var bloodEnabled:Boolean = true;
            if (_storage.hasItem(AppSettings.BLOOD_ENABLED))
                bloodEnabled = Boolean(parseInt(_storage.getItem(AppSettings.BLOOD_ENABLED)));


            if (bloodEnabled)
                _addBlood();
            else
                destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var execution:Boolean = false;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _blood:MovieClip;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _defineBlood():void
        {
            var bloodSource:String;
            if (execution)
                bloodSource = EXECUTION;
            else
                bloodSource = BLOOD + MathHelper.random(1, BLOOD_COUNT);
            _blood = super.source.getMovieClip(bloodSource, false);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _helper:AnimationHelper;
        private var _storage:LocalStorage = LocalStorage.getInstance(AppConfig.APP_NAME);

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBlood():void
        {
            _defineBlood();
            addChild(_blood);

            _helper = new AnimationHelper(_blood);
            _helper.addEventListener(Event.COMPLETE, _completeHandler);
            _helper.execute();

            _addFilter();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeHandler(event:Event):void
        {
            _helper.removeEventListener(Event.COMPLETE, _completeHandler);
            _helper.destroy();
            DestroyUtils.destroy(_blood);
            destroy();
        }

        protected function _addFilter():void
        {
            var colorTransform:ColorTransform = _blood.transform.colorTransform;
            var filter:BevelFilter;

            var bloodColor:uint;
            if (_storage.hasItem(AppSettings.BLOOD_COLOR))
                bloodColor = uint(_storage.getItem(AppSettings.BLOOD_COLOR));
            else
                bloodColor = BloodColor.RED;

            switch (bloodColor) {
                case BloodColor.RED:
                    filter = new BevelFilter(2, 90, 0xffffff, 1, 0xff0000, 1, 10, 10, 2, 1);
                    this.filters = [filter];
                    break;
                case BloodColor.GREEN:
                    colorTransform.color = BloodColor.GREEN;
                    _blood.transform.colorTransform = colorTransform;
                    filter = new BevelFilter(2, 90, 0xffffff, 1, 0x66FF00, 1, 10, 10, 2, 1);
                    this.filters = [filter];
                    break;
                case BloodColor.PURPLE:
                    colorTransform.color = BloodColor.PURPLE;
                    _blood.transform.colorTransform = colorTransform;
                    filter = new BevelFilter(2, 90, 0xffffff, 1, 0xFF99CC, 1, 10, 10, 2, 1);
                    this.filters = [filter];
                    break;
                case BloodColor.BLUE:
                    colorTransform.color = BloodColor.BLUE;
                    _blood.transform.colorTransform = colorTransform;
                    filter = new BevelFilter(2, 90, 0xffffff, 1, 0x00FFFF, 1, 10, 10, 2, 1);
                    this.filters = [filter];
                    break;
                case BloodColor.YELLOW:
                    colorTransform.color = BloodColor.YELLOW;
                    _blood.transform.colorTransform = colorTransform;
                    filter = new BevelFilter(2, 90, 0xffffff, 1, 0xFFFF00, 1, 10, 10, 2, 1);
                    this.filters = [filter];
                    break;
                default :
                    filter = new BevelFilter(2, 90, 0xffffff, 1, 0xff0000, 1, 10, 10, 2, 1);
                    this.filters = [filter];
                    break;
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
