/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.07.13
 * Time: 11:23
 */
package application.views.battle
{
    import application.config.AppConfig;
    import application.models.battle.BattleProfit;
    import application.models.battle.BattleSteps;
    import application.models.user.UserCommonData;
    import application.service.battle.BattleFlow;
    import application.service.battle.common.BattleObserver;
    import application.views.battle.bar.LifeBar;
    import application.views.battle.common.UserHealth;
    import application.views.battle.common.text.UserLogin;
    import application.views.constructor.builders.BattleUnit;
    import application.views.screen.common.frame.UserFrame;
    import application.views.versus.profit.ProfitScreen;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.text.TextFieldAutoSize;

    import framework.core.struct.view.View;
    import framework.core.task.TaskEvent;

    public class BattleView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleView(battleSteps:BattleSteps, userData:UserCommonData, enemyData:UserCommonData, profitModel:BattleProfit, unit:BattleUnit, enemy:BattleUnit, arena:Bitmap, userWin:Boolean = true)
        {
            super();
            _battleSteps = battleSteps;
            _userData = userData;
            _enemyData = enemyData;
            _profitModel = profitModel;
            _user = unit;
            _enemy = enemy;
            _arena = arena;
            _userWin = userWin;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupBackground();
            _setUnits();
            _addUserFrames();
            _addLogin();
            _addLifeBars();
            _showBattle();
        }

        override public function destroy():void
        {
            _battleFlow.destroy();
            _userFrame.destroy();
            _enemyFrame.destroy();
            _userLifeBar.destroy();
            _enemyLifeBar.destroy();
            _userHealth.destroy();
            _enemyHealth.destroy();
            if (_profitScreen && !_profitScreen.destroyed)
                _profitScreen.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _battleSteps:BattleSteps;
        private var _user:BattleUnit;
        private var _enemy:BattleUnit;
        private var _arena:Bitmap;
        private var _observer:BattleObserver = BattleObserver.instance;

        private var _battleFlow:BattleFlow;
        private var _userWin:Boolean;

        private var _userFrame:UserFrame;
        private var _enemyFrame:UserFrame;

        private var _userData:UserCommonData;
        private var _enemyData:UserCommonData;

        private var _userLifeBar:LifeBar;
        private var _enemyLifeBar:LifeBar;

        private var _userHealth:UserHealth;
        private var _enemyHealth:UserHealth;

        private var _profitScreen:ProfitScreen;
        private var _profitModel:BattleProfit;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            addChild(_arena);
            _arena.y = -140;
        }

        private function _addUserFrames():void
        {
            _userFrame = new UserFrame(
                    _userData.soc_net_id,
                    _userData.picture,
                    _userData.maxLevel
            );
            addChild(_userFrame);
            _userFrame.x = 20;
            _userFrame.y = 20;

            _enemyFrame = new UserFrame(
                    _enemyData.soc_net_id,
                    _enemyData.picture,
                    _enemyData.maxLevel
            );
            addChild(_enemyFrame);
            _enemyFrame.x = 660;
            _enemyFrame.y = 20;
        }

        private function _addLogin():void
        {
            var userLogin:UserLogin = new UserLogin();
            addChild(userLogin);
            userLogin.x = 90;
            userLogin.y = 20;
            userLogin.text = _userData.login;

            var enemyLogin:UserLogin = new UserLogin();
            enemyLogin.autoSize = TextFieldAutoSize.RIGHT;
            addChild(enemyLogin);
            enemyLogin.x = 651;
            enemyLogin.y = 20;
            enemyLogin.text = _enemyData.login;
        }

        private function _addLifeBars():void
        {
            var userHp:int = _userData.hp + _userData.bonus_hp;
            var userMaxHp:int = _userData.maxHp + _userData.bonus_hp;

            var enemyHp:int = _enemyData.hp + _enemyData.bonus_hp;
            var enemyMaxHp:int = _enemyData.maxHp + _enemyData.bonus_hp;

            _userLifeBar = new LifeBar(userHp, userMaxHp);
            addChild(_userLifeBar);

            _userLifeBar.x = 90;
            _userLifeBar.y = 55;

            _enemyLifeBar = new LifeBar(enemyHp, enemyMaxHp);
            addChild(_enemyLifeBar);

            _enemyLifeBar.scaleX = -1;
            _enemyLifeBar.x = 656;
            _enemyLifeBar.y = 55;

            _userHealth = new UserHealth(userHp, userMaxHp);
            _enemyHealth = new UserHealth(enemyHp, enemyMaxHp);

            addChild(_userHealth);
            addChild(_enemyHealth);

            _userHealth.x = -45;
            _userHealth.y = 91;

            _enemyHealth.x = 595;
            _enemyHealth.y = 91;
        }

        private function _setUnits():void
        {
            addChild(_user);
            addChild(_enemy);
            _enemy.isEnemy = true;

            _user.scaleX = -1;
            _user.x = -50;
            _user.sector = 0;

            _enemy.x = AppConfig.APP_WIDTH + 50;
            _enemy.sector = 7;

            _user.y = 390;
            _enemy.y = 390;
        }

        private function _showBattle():void
        {
            _battleFlow = new BattleFlow(_user, _enemy, _battleSteps, _userWin);

            _observer.container = this;
            _observer.userBar = _userLifeBar;
            _observer.enemyBar = _enemyLifeBar;
            _observer.userHealth = _userHealth;
            _observer.enemyHealth = _enemyHealth;

            _battleFlow.addEventListener(TaskEvent.COMPLETE, _completeBattle);
            _battleFlow.execute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeBattle(event:TaskEvent):void
        {
            _battleFlow.removeEventListener(TaskEvent.COMPLETE, _completeBattle);
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
