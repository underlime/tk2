/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 09.08.13
 * Time: 10:44
 */
package application.views.battle.common.hint
{
    import application.config.AppConfig;
    import application.service.battle.special.SpecialType;
    import application.views.constructor.helpers.AnimationHelper;

    import flash.display.Bitmap;
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.enum.Language;
    import framework.core.struct.view.View;
    import framework.core.utils.DisplayObjectUtils;

    public class SpecialHint extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TEMPLATE:String = "cSpecialAtack";

        private const RU_PREFIX:String = "_RU";
        private const EN_PREFIX:String = "_En";

        private const TEMPLATE_CONTAINER:String = "SpecialName";

        private const BONE_BREAKER:String = "BoneBreaker";
        private const DOUBLE_STRIKE:String = "DoubleStrike";
        private const FORCE_BLOW:String = "ForceBlow";
        private const HEAVY_ARGUMENT:String = "HeavyArgument";
        private const RAGE_STRIKES:String = "RageStrikes";
        private const STALAGMITE:String = "Stalagmite";
        private const TELEKINESIS:String = "Telekinez";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialHint(specialType:SpecialType)
        {
            super();
            _specialType = specialType;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupHash();
            _addHint();
        }

        override public function destroy():void
        {

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _specialType:SpecialType;
        private var _template:MovieClip;

        private var _hash:Object = {};

        private var _helper:AnimationHelper;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupHash():void
        {
            _hash[SpecialType.BONE_BREAKER.toString()] = BONE_BREAKER;
            _hash[SpecialType.DOUBLE.toString()] = DOUBLE_STRIKE;
            _hash[SpecialType.FORCE_BLOW.toString()] = FORCE_BLOW;
            _hash[SpecialType.HEAVY_HELPER.toString()] = HEAVY_ARGUMENT;
            _hash[SpecialType.RAGE.toString()] = RAGE_STRIKES;
            _hash[SpecialType.STALAGMITE.toString()] = STALAGMITE;
            _hash[SpecialType.TELEKINESIS.toString()] = TELEKINESIS;
        }

        private function _addHint():void
        {
            _template = super.source.getMovieClip(TEMPLATE);
            addChild(_template);

            _helper = new AnimationHelper(_template);
            var container:DisplayObjectContainer = _template.getChildByName(TEMPLATE_CONTAINER) as DisplayObjectContainer;
            DisplayObjectUtils.clearContainer(container);

            if (_hash[_specialType.toString()]) {
                var prefix:String = AppConfig.LANGUAGE == Language.RU ? RU_PREFIX : EN_PREFIX;
                var source:String = _hash[_specialType.toString()];
                var child:Bitmap = super.source.getBitmap(source + prefix, true);
                container.addChild(child);
            }

            _helper.addEventListener(Event.COMPLETE, _completeHandler);
            _helper.execute();
        }

        private function _completeHandler(event:Event):void
        {
            _helper.removeEventListener(Event.COMPLETE, _completeHandler);
            _helper.destroy();
            destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
