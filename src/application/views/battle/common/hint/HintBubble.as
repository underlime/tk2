/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.08.13
 * Time: 13:37
 */
package application.views.battle.common.hint
{
    import application.views.TextFactory;
    import application.views.text.AppTextField;

    import com.greensock.TweenLite;

    import flash.filters.DropShadowFilter;
    import flash.filters.GlowFilter;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class HintBubble extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HintBubble(value:int)
        {
            super();
            _value = value;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupTextField();
            _addFilters();
            _defineText();
            _startAnimate();
        }

        override public function destroy():void
        {
            TweenLite.killTweensOf(this);
            this.alpha = 1;
            _txtHint.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtHint:AppTextField;

        private var _fontSize:int = 45;
        private var _noteSize:int = 35;

        private var _defaultColor:uint = 0xCC0000;

        private var _damageColor:uint = 0xCC0000;
        private var _critColor:uint = 0xFF3300;
        private var _blockColor:uint = 0x996600;
        private var _missColor:uint = 0x0066CC;
        private var _medicalColor:uint = 0x009933;

        private var _crit:Boolean = false;
        private var _block:Boolean = false;
        private var _miss:Boolean = false;
        private var _medical:Boolean = false;
        private var _damage:Boolean = false;

        private var _value:int = 0;
        private var _prefix:String = "-";

        private var _addText:String = "";

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTextField():void
        {
            _txtHint = new AppTextField();
            _txtHint.color = 0xffffff;
            _txtHint.bold = false;
            _txtHint.size = _fontSize;
            _txtHint.bold = true;
            _txtHint.align = TextFormatAlign.CENTER;
            _txtHint.autoSize = TextFieldAutoSize.CENTER
            _txtHint.multiline = true;
            _txtHint.wordWrap = true;
            _txtHint.width = 250;
            addChild(_txtHint);
        }

        private function _addFilters():void
        {
            var filters:Array = [
                new DropShadowFilter(1, 45, _defaultColor, 1, 7, 7, 10),
                new GlowFilter(0xffffff, 1, 5, 5, 10)
            ];

            this.filters = filters;
        }

        private function _defineText():void
        {
            var mainVal:String = _value.toString();
            if (_value == 0) {
                _prefix = "";
                mainVal = _addText;
                _addText = "";
                if (mainVal == "")
                    mainVal = TextFactory.instance.getLabel("miss_label");
            }

            if (_medical)
                _prefix = "+";

            _txtHint.htmlText = "<font>" + _prefix + mainVal + "</font><br><font size='" + _noteSize + "'>" + _addText + "</font>";
        }

        private function _startAnimate():void
        {
            this.alpha = 0;
            var params:Object = {
                "alpha": 1,
                "y": this.y - this.height - 20,
                "onComplete": _showMessage
            };
            TweenLite.to(this, .3, params);
        }

        private function _showMessage():void
        {
            TweenLite.killTweensOf(this);

            var params:Object = {
                "y": this.y - 7,
                "onComplete": _hideMessage
            };

            TweenLite.to(this, .7, params);
        }

        private function _hideMessage():void
        {
            TweenLite.killTweensOf(this);

            var params:Object = {
                "alpha": 0,
                "y": this.y - 20,
                "onComplete": destroy
            };

            TweenLite.to(this, .3, params);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set crit(value:Boolean):void
        {
            _crit = value;
            _defaultColor = _critColor;
        }

        public function set block(value:Boolean):void
        {
            _block = value;
            _defaultColor = _blockColor;
            _addText = TextFactory.instance.getLabel("block_label");
        }

        public function set miss(value:Boolean):void
        {
            _miss = value;
            _defaultColor = _missColor;
            _addText = TextFactory.instance.getLabel("miss_label");
        }

        public function set medical(value:Boolean):void
        {
            _medical = value;
            _defaultColor = _medicalColor;
        }

        public function set damage(value:Boolean):void
        {
            _damage = value;
            _defaultColor = _damageColor;
        }
    }
}
