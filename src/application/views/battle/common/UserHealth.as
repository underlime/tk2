/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.08.13
 * Time: 21:20
 */
package application.views.battle.common
{
    import application.views.screen.common.text.ShadowAppText;

    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class UserHealth extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserHealth(health:int, total:int = 0)
        {
            super();
            _health = health;
            if (total > _health)
                _current = total;
            else
                _current = _health;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addText();
        }

        override public function destroy():void
        {
            _txtData.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _health:int = 0;
        private var _current:int = 0;
        private var _txtData:ShadowAppText;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addText():void
        {
            _txtData = new ShadowAppText(0x58595b);
            _txtData.size = 14;
            _txtData.autoSize = TextFieldAutoSize.NONE;
            _txtData.align = TextFormatAlign.CENTER;
            _txtData.width = 200;
            _txtData.height = 30;
            addChild(_txtData);

            _update();
        }

        private function _update():void
        {
            if (_txtData)
                _txtData.text = _current.toString() + "/" + _health.toString();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set damage(value:int):void
        {
            _current -= value;
            if (_current < 0)
                _current = 0;
            _update();
        }

        public function set heal(value:int):void
        {
            _current += value;
            if (_current > _health)
                _current = _health;
            _update();
        }
    }
}
