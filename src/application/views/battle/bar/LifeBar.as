/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 09.08.13
 * Time: 20:00
 */
package application.views.battle.bar
{

    import com.greensock.TweenLite;

    import flash.display.Bitmap;
    import flash.display.MovieClip;
    import flash.geom.Rectangle;

    import framework.core.helpers.MathHelper;
    import framework.core.helpers.TimeHelper;
    import framework.core.struct.view.View;

    public class LifeBar extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "LIFEBAR_BG";
        public static const FRAME:String = "LIFEBAR_FRAME";
        public static const FILL:String = "LIFEBAR_FILL";
        public static const FIRE:String = "FIRE_LIFEBAR";
        private var _maxHp:int;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LifeBar(health:int, maxHp:int)
        {
            super();
            _health = health;
            _maxHp = MathHelper.equalOrGreater(maxHp, _health);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _renderBar();
            _initBar();
        }

        override public function destroy():void
        {
            TweenLite.killTweensOf(this);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var temp:int = 0;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _health:int;
        private var _fill:Bitmap;
        private var _fire:MovieClip;

        private var _scrollRect:Rectangle;
        private var _total:int = 200;

        private var _queue:Array = [];
        private var _blocked:Boolean = false;

        private var _initFrames:int = 54;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderBar():void
        {
            var background:Bitmap = super.source.getBitmap(BACKGROUND);
            addChild(background);

            _fill = super.source.getBitmap(FILL);
            addChild(_fill);

            _fill.x = 8;
            _fill.y = 7;

            _scrollRect = new Rectangle(0, 0, _total, 20);
            _fill.scrollRect = _scrollRect;

            _fire = super.source.getMovieClip(FIRE, false);
            addChild(_fire);
            _fire.x = 8;
            _fire.y = 7;
            _fire.scrollRect = _scrollRect;

            var frame:Bitmap = super.source.getBitmap(FRAME);
            addChild(frame);
        }

        private function _initBar():void
        {
            var seconds:Number = TimeHelper.getFramesTime(_initFrames);
            var target:Number = _total - (_total * (_maxHp - _health) / _maxHp);

            _scrollRect.width = 0;
            _fill.scrollRect = _scrollRect;
            _fire.scrollRect = _scrollRect;

            var params:Object = {
                "width": target,
                "onUpdate": _initUpdate,
                "onComplete": _initComplete
            };

            TweenLite.to(_scrollRect, seconds, params);
        }

        private function _initUpdate():void
        {
            _fill.scrollRect = _scrollRect;
            _fire.scrollRect = _scrollRect;
        }

        private function _initComplete():void
        {
            TweenLite.killTweensOf(this);
        }

        private function _moveBar(dx:Number):void
        {
            _blocked = true;
            this.temp = _scrollRect.width;

            var params:Object = {
                "temp": temp - dx,
                "onUpdate": _tweenUpdateHandler,
                "onComplete": _tweenCompleteHandler
            };

            TweenLite.to(this, .5, params);
        }

        private function _tweenUpdateHandler():void
        {
            _scrollRect.width -= _scrollRect.width - temp;
            _fill.scrollRect = _scrollRect;
            _fire.scrollRect = _scrollRect;
        }

        private function _tweenCompleteHandler():void
        {
            TweenLite.killTweensOf(this);

            _blocked = false;
            _queue.shift();
            if (_queue.length > 0)
                _moveBar(_queue[0]);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set damage(value:int):void
        {
            var dx:Number = _total * value / _maxHp;
            _queue.push(dx);
            if (!_blocked)
                _moveBar(_queue[0]);
        }

        public function set heal(value:int):void
        {
            var dx:Number = (-1) * _total * value / _maxHp;
            _queue.push(dx);
            if (!_blocked)
                _moveBar(_queue[0]);
        }
    }
}
