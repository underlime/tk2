/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.08.13
 * Time: 13:57
 */
package application.views.versus.buttons
{
    import application.views.TextFactory;
    import application.views.screen.common.buttons.FerrosPriceButton;

    public class ReduceButton extends FerrosPriceButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ReduceButton(price:int)
        {
            var label:String = TextFactory.instance.getLabel("weaken_label");
            super(label, price);
            _iconMargin = 22;
            _w = 232;
            _fontSize = 14;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
