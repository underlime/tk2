/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.08.13
 * Time: 16:48
 */
package application.views.versus.buttons
{
    import application.config.AppConfig;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.AppSpriteButton;
    import application.views.text.AppTextField;

    import flash.filters.GlowFilter;
    import flash.text.TextFormatAlign;

    import framework.core.enum.Language;

    public class FightButton extends AppSpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "VERSUS_BTN_FIGHT";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FightButton()
        {
            super(SOURCE);
            _w = 160;
            _h = 72;
            _overPosition = 72;
            _downPosition = 144;
            _blockPosition = 216;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addLabel();
        }

        override public function destroy():void
        {
            _txtLabel.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtLabel:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addLabel():void
        {
            _txtLabel = new AppTextField();
            _txtLabel.width = _w;
            _txtLabel.align = TextFormatAlign.CENTER;
            _txtLabel.color = 0xffffff;
            _txtLabel.size = 18;
            _txtLabel.bold = true;
            _txtLabel.filters = [new GlowFilter(0x77291f, 1, 5, 5, 5)];
            addChild(_txtLabel);

            _txtLabel.text = TextFactory.instance.getLabel("in_battle_label");
            _txtLabel.y = _h / 2 - _txtLabel.textHeight / 2;
            if (AppConfig.LANGUAGE == Language.RU)
                _txtLabel.y -= 4;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
