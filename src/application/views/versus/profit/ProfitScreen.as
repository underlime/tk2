/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 11.08.13
 * Time: 11:28
 */
package application.views.versus.profit
{
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.events.BattleEvent;
    import application.models.battle.BattleProfit;
    import application.models.common.Advices;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.text.AppTextField;
    import application.views.versus.profit.buttons.AgainButton;
    import application.views.versus.profit.buttons.ArenaButton;
    import application.views.versus.profit.buttons.MainScreenButton;
    import application.views.versus.profit.buttons.TellButton;

    import flash.display.Bitmap;
    import flash.display.MovieClip;
    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.utils.Singleton;

    public class ProfitScreen extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "ProfitScreenBackground";
        public static const LOSE_HEADER:String = "GBattleLose";
        public static const WIN_HEADER:String = "GBattleWin";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ProfitScreen(profitModel:BattleProfit, win:Boolean = true)
        {
            super();
            _profitModel = profitModel;
            _win = win;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addHeader();
            _addTellButton();
            _addAgainButton();
            _addArenaButton();
            _addMainScreenButton();
            _addIcons();
            _addLabels();
            _fillData();
            _bindReactions();
            _addAdvice();
        }

        override public function destroy():void
        {
            _unbindReactions();
            _tellButton.destroy();
            _againButton.destroy();
            _arenaButton.destroy();
            _mainScreenButton.destroy();
            _expLabel.destroy();
            _gloryLabel.destroy();
            _tomatosLabel.destroy();
            _ferrosLabel.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _profitModel:BattleProfit;
        private var _win:Boolean;

        private var _header:MovieClip;
        private var _background:Bitmap;

        private var _tellButton:TellButton;
        private var _againButton:AgainButton;
        private var _arenaButton:ArenaButton;
        private var _mainScreenButton:MainScreenButton;

        private var _expLabel:ShadowAppText;
        private var _gloryLabel:ShadowAppText;
        private var _tomatosLabel:ShadowAppText;
        private var _ferrosLabel:ShadowAppText;

        private var _txtAdvice:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            graphics.beginFill(0x000000, .9);
            graphics.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            graphics.endFill();

            _background = super.source.getBitmap(BACKGROUND);
            _background.x = AppConfig.APP_WIDTH / 2 - _background.width / 2;
            _background.y = 168;
            addChild(_background);
        }

        private function _addHeader():void
        {
            var headerSrc:String = _win ? WIN_HEADER : LOSE_HEADER;
            _header = super.source.getMovieClip(headerSrc);
            addChild(_header);
            _header.y = 140;
            var headerWidth:int = _win ? 210 : 295;
            _header.x = AppConfig.APP_WIDTH / 2 - headerWidth / 2;
        }

        private function _addTellButton():void
        {
            _tellButton = new TellButton();
            addChild(_tellButton);
            _tellButton.x = AppConfig.APP_WIDTH / 2 - _tellButton.width / 2;
            _tellButton.y = 362;
        }

        private function _addAgainButton():void
        {
            _againButton = new AgainButton();
            addChild(_againButton);
            _againButton.x = AppConfig.APP_WIDTH / 2 - _againButton.width / 2;
            _againButton.y = _background.y + 280;
        }

        private function _addArenaButton():void
        {
            _arenaButton = new ArenaButton();
            addChild(_arenaButton);
            _arenaButton.x = _background.x;
            _arenaButton.y = _background.y + 280;
        }

        private function _addMainScreenButton():void
        {
            _mainScreenButton = new MainScreenButton();
            addChild(_mainScreenButton);
            _mainScreenButton.x = _background.x + _background.width - _mainScreenButton.width;
            _mainScreenButton.y = _background.y + 280;
        }

        private function _addIcons():void
        {
            var expIco:Bitmap = super.source.getBitmap(CommonData.EXP_ICON);
            var gloryIcon:Bitmap = super.source.getBitmap(CommonData.GLORY_ICON);
            var tomatosIco:Bitmap = super.source.getBitmap(CommonData.TOMATOS_RES_ICON);
            var ferrosIco:Bitmap = super.source.getBitmap(CommonData.FERROS_RES_ICON);

            addChild(expIco);
            addChild(gloryIcon);
            addChild(tomatosIco);
            addChild(ferrosIco);

            expIco.y = _background.y + 42;
            gloryIcon.y = _background.y + 42;
            tomatosIco.y = _background.y + 42;
            ferrosIco.y = _background.y + 42;

            expIco.x = _background.x + 39;
            gloryIcon.x = _background.x + 260;
            tomatosIco.x = _background.x + 150;
            ferrosIco.x = _background.x + 370;
        }

        private function _addLabels():void
        {
            _expLabel = new ShadowAppText(0x336633);
            addChild(_expLabel);
            _expLabel.x = _background.x + 73;
            _expLabel.y = _background.y + 42;

            _expLabel.text = "0";


            _tomatosLabel = new ShadowAppText(0x996600);
            addChild(_tomatosLabel);
            _tomatosLabel.x = _background.x + 182;
            _tomatosLabel.y = _background.y + 42;

            _tomatosLabel.text = "0";


            _gloryLabel = new ShadowAppText(0x990033);
            addChild(_gloryLabel);
            _gloryLabel.x = _background.x + 293;
            _gloryLabel.y = _background.y + 42;

            _gloryLabel.text = "0";

            _ferrosLabel = new ShadowAppText(0x996600);
            addChild(_ferrosLabel);
            _ferrosLabel.x = _background.x + 404;
            _ferrosLabel.y = _background.y + 42;

            _ferrosLabel.text = "0";
        }

        private function _fillData():void
        {
            _expLabel.text = "+" + _profitModel.experience.value.toString();
            _tomatosLabel.text = "+" + _profitModel.tomatos.value.toString();
            _gloryLabel.text = "+" + _profitModel.glory.value.toString();
        }

        private function _bindReactions():void
        {
            _mainScreenButton.addEventListener(MouseEvent.CLICK, _mapHandler);
            _againButton.addEventListener(MouseEvent.CLICK, _againHandler);
            _tellButton.addEventListener(MouseEvent.CLICK, _tellClickHandler);
            _arenaButton.addEventListener(MouseEvent.CLICK, _arenaHandler);
        }

        private function _unbindReactions():void
        {
            _mainScreenButton.removeEventListener(MouseEvent.CLICK, _mapHandler);
            _againButton.removeEventListener(MouseEvent.CLICK, _againHandler);
            _tellButton.removeEventListener(MouseEvent.CLICK, _tellClickHandler);
            _arenaButton.removeEventListener(MouseEvent.CLICK, _arenaHandler);
        }

        private function _addAdvice():void
        {
            var advices:Advices = Singleton.getClass(Advices);
            advices.shuffleAdvices();

            _txtAdvice = new AppTextField();
            _txtAdvice.width = 365;
            _txtAdvice.align = TextFormatAlign.CENTER;
            _txtAdvice.autoSize = TextFieldAutoSize.LEFT;
            _txtAdvice.multiline = true;
            _txtAdvice.wordWrap = true;
            _txtAdvice.size = 12;
            _txtAdvice.color = 0x5e3113;
            addChild(_txtAdvice);
            _txtAdvice.text = advices.getRandomAdvice();

            _txtAdvice.x = _background.x + 63;
            _txtAdvice.y = _background.y + 118;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _mapHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new BattleEvent(BattleEvent.CALL_MAP, true));
            super.sound.playUISound(UISound.CLICK);
        }

        private function _againHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new BattleEvent(BattleEvent.BATTLE_AGAIN, true));
            super.sound.playUISound(UISound.CLICK);
        }

        private function _tellClickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new BattleEvent(BattleEvent.SHARE, true));
            super.sound.playUISound(UISound.CLICK);
        }

        private function _arenaHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_ARENA));
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
