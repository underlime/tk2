/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.08.13
 * Time: 14:39
 */
package application.views.versus.compare
{
    import application.config.AppConfig;
    import application.models.user.UserCommonData;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.UserLevel;

    import framework.core.struct.view.View;

    public class CompareView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CompareView(userCommonData:UserCommonData, enemyCommonData:UserCommonData)
        {
            super();
            _userCommonData = userCommonData;
            _enemyCommonData = enemyCommonData;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addGloryCompare();
            _addStrengthCompare();
            _addAgilityCompare();
            _addIntellectCompare();
            _addLevels();
            _addHpCompare();
        }

        override public function destroy():void
        {
            _gloryCompare.destroy();
            _strengthCompare.destroy();
            _agilityCompare.destroy();
            _intellectCompare.destroy();
            _userLevel.destroy();
            _enemyLevel.destroy();
            _hpCompare.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _userCommonData:UserCommonData;
        private var _enemyCommonData:UserCommonData;

        private var _gloryCompare:ComparePosition;
        private var _strengthCompare:ComparePosition;
        private var _agilityCompare:ComparePosition;
        private var _intellectCompare:ComparePosition;

        private var _userLevel:UserLevel;
        private var _enemyLevel:UserLevel;

        private var _hpCompare:ComparePosition;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addGloryCompare():void
        {
            _gloryCompare = new ComparePosition(CommonData.GLORY_BIG_ICON, _userCommonData.glory, _enemyCommonData.glory);
            _gloryCompare.y = 22;
            _gloryCompare.shadowColor = 0x000000;
            addChild(_gloryCompare);
        }

        private function _addStrengthCompare():void
        {
            _strengthCompare = new ComparePosition(CommonData.STRENGTH_BIG_ICON, _userCommonData.strength, _enemyCommonData.strength);
            _strengthCompare.y = 188;
            _strengthCompare.needCompare = true;
            addChild(_strengthCompare);
        }

        private function _addAgilityCompare():void
        {
            _agilityCompare = new ComparePosition(CommonData.AGILITY_BIG_ICON, _userCommonData.agility, _enemyCommonData.agility);
            _agilityCompare.y = 237;
            _agilityCompare.needCompare = true;
            addChild(_agilityCompare);
        }

        private function _addIntellectCompare():void
        {
            _intellectCompare = new ComparePosition(CommonData.INTELLECT_BIG_ICON, _userCommonData.intellect, _enemyCommonData.intellect);
            _intellectCompare.y = 286;
            _intellectCompare.needCompare = true;
            addChild(_intellectCompare);
        }

        private function _addLevels():void
        {
            _userLevel = new UserLevel(_userCommonData.maxLevel);
            addChild(_userLevel);

            _enemyLevel = new UserLevel(_enemyCommonData.maxLevel);
            addChild(_enemyLevel);

            _userLevel.x = 7;
            _userLevel.y = 111;

            _enemyLevel.x = AppConfig.APP_WIDTH - _enemyLevel.width - 7;
            _enemyLevel.y = 111;
        }

        private function _addHpCompare():void
        {
            _hpCompare = new ComparePosition(CommonData.HEART_BIG_ICON, _userCommonData.maxHp + _userCommonData.bonus_hp, _enemyCommonData.maxHp + _enemyCommonData.bonus_hp);
            _hpCompare.y = 125;
            _hpCompare.margin = 48;
            _hpCompare.needCompare = true;
            _hpCompare.textMargin = -5;

            addChild(_hpCompare);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
