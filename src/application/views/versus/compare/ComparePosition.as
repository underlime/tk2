/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.08.13
 * Time: 12:42
 */
package application.views.versus.compare
{
    import application.config.AppConfig;

    import flash.display.Bitmap;

    import framework.core.struct.view.View;

    public class ComparePosition extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ComparePosition(iconSource:String, userValue:int, enemyValue:int)
        {
            super();
            _iconSource = iconSource;
            _userValue = userValue;
            _enemyValue = enemyValue;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addUserCompare();
            _addEnemyCompare();
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _iconSource:String;
        private var _userValue:int;
        private var _enemyValue:int;

        private var _userCompare:CompareIconText;
        private var _enemyCompare:CompareIconText;

        private var _needCompare:Boolean = false;
        private var _shadowColor:uint = 0x6a220a;

        private var _margin:int = 0;
        private var _textMargin:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addUserCompare():void
        {
            var icon:Bitmap = super.source.getBitmap(_iconSource);
            _userCompare = new CompareIconText(icon, _userValue.toString());
            _userCompare.less = _userValue < _enemyValue;
            _userCompare.x = 8 + _margin;
            _userCompare.shadowColor = _shadowColor;
            _userCompare.compare = _needCompare;
            _userCompare.textMargin = _textMargin;
            addChild(_userCompare);
        }

        private function _addEnemyCompare():void
        {
            var icon:Bitmap = super.source.getBitmap(_iconSource);
            _enemyCompare = new CompareIconText(icon, _enemyValue.toString());
            _enemyCompare.less = _enemyValue < _userValue;
            _enemyCompare.x = AppConfig.APP_WIDTH - 8 - icon.width - _margin;
            _enemyCompare.inverse = true;
            _enemyCompare.shadowColor = _shadowColor;
            _enemyCompare.compare = _needCompare;
            _enemyCompare.textMargin = _textMargin;
            addChild(_enemyCompare);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set needCompare(value:Boolean):void
        {
            _needCompare = value;
        }

        public function set shadowColor(value:uint):void
        {
            _shadowColor = value;
        }

        public function set margin(value:int):void
        {
            _margin = value;
        }

        public function set textMargin(value:int):void
        {
            _textMargin = value;
        }
    }
}
