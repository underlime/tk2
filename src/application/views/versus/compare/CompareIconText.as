/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.08.13
 * Time: 10:26
 */
package application.views.versus.compare
{
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;

    import framework.core.struct.view.View;

    public class CompareIconText extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const RED_INDICATOR:String = "RED_INDICATOR";
        public static const GREEN_INDICATOR:String = "GREEN_INDICATOR";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CompareIconText(icon:Bitmap, value:String)
        {
            super();
            _icon = icon;
            _value = value;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupIcon();
            _addText();
            _updatePositions();
            if (_compare)
                _addCompare();
        }

        override public function destroy():void
        {
            _txtLabel.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;
        private var _value:String;

        private var _txtLabel:ShadowAppText;
        private var _shadowColor:uint = 0x6a220a;

        private var _inverse:Boolean = false;
        private var _margin:int = 0;

        private var _indicator:Bitmap;
        private var _less:Boolean = false;

        private var _compare:Boolean = false;

        private var _lessColor:uint = 0xff3333;
        private var _usualColor:uint = 0xffffff;

        private var _inverseMargin:int = 5;

        private var _textMargin:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupIcon():void
        {
            addChild(_icon);
        }

        private function _addText():void
        {
            _txtLabel = new ShadowAppText(_shadowColor);
            _txtLabel.color = _less && _compare ? _lessColor : _usualColor;
            _txtLabel.size = 14;
            _txtLabel.autoSize = _inverse ? TextFieldAutoSize.RIGHT : TextFieldAutoSize.LEFT;

            addChild(_txtLabel);
            _txtLabel.text = _value;
        }

        private function _updatePositions():void
        {
            if (!_inverse) {
                _txtLabel.x = _icon.x + _icon.width + _margin;
            } else {
                _txtLabel.x = _icon.x - _margin - _txtLabel.textWidth - _inverseMargin;
            }

            _txtLabel.y = _icon.height / 2 - _txtLabel.textHeight / 2 + _textMargin;
        }

        private function _addCompare():void
        {
            if (_less)
                _indicator = super.source.getBitmap(RED_INDICATOR);
            else
                _indicator = super.source.getBitmap(GREEN_INDICATOR);

            addChild(_indicator);
            _indicator.y = _txtLabel.y + _txtLabel.textHeight / 2 - _indicator.height / 2 + 3;

            if (_inverse)
                _indicator.x = _txtLabel.x - _margin - _indicator.width;
            else
                _indicator.x = _txtLabel.x + _txtLabel.width + _margin;

        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set shadowColor(value:uint):void
        {
            _shadowColor = value;
        }

        public function set inverse(value:Boolean):void
        {
            _inverse = value;
        }

        public function set margin(value:int):void
        {
            _margin = value;
        }

        public function set less(value:Boolean):void
        {
            _less = value;
        }

        public function set compare(value:Boolean):void
        {
            _compare = value;
        }

        public function set textMargin(value:int):void
        {
            _textMargin = value;
        }
    }
}
