/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 11.08.13
 * Time: 11:27
 */
package application.views.versus
{
    import application.config.AppConfig;
    import application.events.BattleEvent;
    import application.models.user.UserCommonData;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.constructor.builders.BattleUnit;
    import application.views.versus.buttons.FightButton;
    import application.views.versus.buttons.QuickBattleButton;
    import application.views.versus.buttons.ReduceButton;
    import application.views.versus.compare.CompareView;
    import application.views.versus.text.UserName;

    import com.greensock.TweenLite;
    import com.greensock.easing.Cubic;

    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    import framework.core.helpers.TimeHelper;

    public class VersusScreen extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "VersusScreen";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function VersusScreen(userUnit:BattleUnit, enemyUnit:BattleUnit, userCommonData:UserCommonData, enemyCommonData:UserCommonData)
        {
            super();
            _userUnit = userUnit;
            _enemyUnit = enemyUnit;
            _userCommonData = userCommonData;
            _enemyCommonData = enemyCommonData;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addUnits();
            _addNames();
            _addCompare();
            _addButtons();
        }

        override public function destroy():void
        {
            _background.stop();
            removeChild(_background);
            _userName.destroy();
            _enemyName.destroy();
            _reduceButton.destroy();
            _quickButton.destroy();
            _fightButton.destroy();
            _compareView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _quickButton:QuickBattleButton;
        private var _fightButton:FightButton;
        private var _reduceButton:ReduceButton;

        private var _userUnit:BattleUnit;
        private var _enemyUnit:BattleUnit;
        private var _userCommonData:UserCommonData;
        private var _enemyCommonData:UserCommonData;

        private var _userName:UserName;
        private var _enemyName:UserName;

        private var _background:MovieClip;

        private var _compareView:CompareView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            _background = super.source.getMovieClip(BACKGROUND);
            addChild(_background);
            _background.gotoAndPlay(1);
        }

        private function _addUnits():void
        {
            addChild(_userUnit);
            addChild(_enemyUnit);

            _userUnit.stay();
            _enemyUnit.stay();

            _userUnit.scaleX = -1;
            _userUnit.x = -22;
            _userUnit.y = 248;

            _enemyUnit.x = 778;
            _enemyUnit.y = 248;

            var userParams:Object = {
                "x": 279,
                ease: Cubic.easeIn
            };

            var enemyParams:Object = {
                "x": 474,
                ease: Cubic.easeIn
            };

            TweenLite.to(_userUnit, TimeHelper.getFramesTime(18), userParams);
            TweenLite.to(_enemyUnit, TimeHelper.getFramesTime(18), enemyParams);
        }

        private function _addNames():void
        {
            _userName = new UserName();
            addChild(_userName);
            _userName.text = _userCommonData.login;
            _userName.x = -236;
            _userName.y = 357;

            _enemyName = new UserName();
            addChild(_enemyName);
            _enemyName.text = _enemyCommonData.login;
            _enemyName.x = AppConfig.APP_WIDTH + 236;
            _enemyName.y = 357;

            var userParams:Object = {
                "x": 36,
                ease: Cubic.easeIn
            };

            var enemyParams:Object = {
                "x": AppConfig.APP_WIDTH - 36 - _enemyName.textWidth,
                ease: Cubic.easeIn,
                "onComplete": _tweenComplete
            };

            TweenLite.to(_userName, TimeHelper.getFramesTime(18), userParams);
            TweenLite.to(_enemyName, TimeHelper.getFramesTime(18), enemyParams);
        }

        private function _tweenComplete():void
        {
            TweenLite.killTweensOf(_userUnit);
            TweenLite.killTweensOf(_enemyName);
            TweenLite.killTweensOf(_userName);
            TweenLite.killTweensOf(_enemyName);
            _showButtons();
        }

        private function _addButtons():void
        {
            _addQuickBattleButton();
            _addFightButton();
            _addReduceButton();

            _quickButton.visible = false;
            _fightButton.visible = false;
            _reduceButton.visible = false;
        }

        private function _showButtons():void
        {
            _quickButton.visible = true;
            _fightButton.visible = true;
            _reduceButton.visible = true;
        }

        private function _addQuickBattleButton():void
        {
            _quickButton = new QuickBattleButton();
            addChild(_quickButton);
            _quickButton.x = AppConfig.APP_WIDTH - _quickButton.w - 13;
            _quickButton.y = 446;
            _quickButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _addFightButton():void
        {
            _fightButton = new FightButton();
            addChild(_fightButton);
            _fightButton.x = AppConfig.APP_WIDTH / 2 - _fightButton.w / 2;
            _fightButton.y = 430;
            _fightButton.addEventListener(MouseEvent.CLICK, _fightClickHandler);
        }

        private function _addReduceButton():void
        {
            _reduceButton = new ReduceButton(10);
            addChild(_reduceButton);
            _reduceButton.x = 13;
            _reduceButton.y = 446;
            _reduceButton.addEventListener(MouseEvent.CLICK, _reduceClickHandler);
        }

        private function _addCompare():void
        {
            _compareView = new CompareView(_userCommonData, _enemyCommonData);
            addChild(_compareView);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _fightClickHandler(event:MouseEvent):void
        {
            dispatchEvent(new BattleEvent(BattleEvent.CALL_BATTLE));
            super.sound.playUISound(UISound.CLICK);
        }

        private function _clickHandler(event:MouseEvent):void
        {
            dispatchEvent(new BattleEvent(BattleEvent.QUICK_BATTLE));
            super.sound.playUISound(UISound.CLICK);
        }

        private function _reduceClickHandler(event:MouseEvent):void
        {
            dispatchEvent(new BattleEvent(BattleEvent.EASE));
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
