/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 17:41
 */
package application.views.text
{
    import framework.core.display.text.SimpleTextField;

    public class AppTextField extends SimpleTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const COMIC_SANS:String = "ComicSans";

        public static const DARK_BROWN:uint = 0x3c2415;
        public static const LIGHT_BROWN:uint = 0x754c29;
        public static const LIGHTER_BROWN:uint = 0x5e3113;
        public static const RED:uint = 0xbe1e2d;
        public static const GREEN:uint = 0x006938;
        public static const BLUE:uint = 0x1c75bc;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AppTextField(settings:Object = null)
        {
            super(settings);
            super.fontFamily = COMIC_SANS;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/


        [Embed(source="../../../../lib/fonts/comic.ttf", fontFamily='ComicSans', fontStyle='normal', fontWeight='normal', mimeType="application/x-font", embedAsCFF='false')]
        private var ComicSansRegular:Class;

        [Embed(source="../../../../lib/fonts/comicbd.ttf", fontFamily='ComicSans', fontStyle='normal', fontWeight='bold', mimeType="application/x-font", embedAsCFF='false')]
        private var ComicSansBold:Class;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
