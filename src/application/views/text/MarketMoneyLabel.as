/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.06.13
 * Time: 10:18
 */
package application.views.text
{
    import flash.filters.DropShadowFilter;
    import flash.text.TextFieldAutoSize;

    public class MarketMoneyLabel extends AppTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketMoneyLabel()
        {
            super();
            _setup();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void
        {
            var filter:DropShadowFilter = new DropShadowFilter(1, 45, 0x000000, 1, 3, 3, 10, 1);
            this.color = 0xffffff;
            this.size = 18;
            this.height = 26;
            this.autoSize = TextFieldAutoSize.LEFT;
            this.bold = true;
            this.filters = [filter];
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
