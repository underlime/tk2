/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.06.13
 * Time: 14:56
 */
package application.views.text
{
    import application.views.AppView;
    import application.views.screen.common.CommonData;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;

    public class DialogPrice extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DialogPrice(ferros:int, tomatos:int = 0)
        {
            _ferros = ferros;
            _tomatos = tomatos;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addFerrosData();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _ferros:int;
        private var _tomatos:int;

        private var _txtFerros:AppTextField;
        private var _txtTomatos:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addFerrosData():void
        {
            _txtFerros = new AppTextField();
            _txtFerros.bold = true;
            _txtFerros.color = 0x4F2D0C;
            _txtFerros.size = 16;
            _txtFerros.autoSize = TextFieldAutoSize.LEFT;
            var ico:Bitmap = super.source.getBitmap(CommonData.FERROS_ICO);
            addChild(ico);
            addChild(_txtFerros);

            _txtFerros.text = _ferros.toString();
            _txtFerros.x = ico.width + 5;

            _txtTomatos = new AppTextField();
            _txtTomatos.bold = true;
            _txtTomatos.color = 0x4F2D0C;
            _txtTomatos.size = 16;
            _txtTomatos.autoSize = TextFieldAutoSize.LEFT;
            var icon:Bitmap = super.source.getBitmap(CommonData.TOMATOS_ICO);
            addChild(icon);
            addChild(_txtTomatos);
            icon.x = _txtFerros.x + _txtFerros.width + 10;

            _txtTomatos.text = _tomatos.toString();
            _txtTomatos.x = icon.x + icon.width + 5;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
