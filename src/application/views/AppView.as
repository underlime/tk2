/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 17:18
 */
package application.views
{
    import application.models.common.hint.Hints;
    import application.sound.SoundManager;
    import application.views.popup.AlertWindow;

    import framework.core.struct.view.View;
    import framework.core.utils.Singleton;

    public class AppView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AppView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function alert(header:String, message:String):void
        {
            var alertView:AlertWindow = new AlertWindow(header, message);
            addChild(alertView);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get sound():SoundManager
        {
            return Singleton.getClass(SoundManager);
        }

        public function get popupManager():PopupManager
        {
            return Singleton.getClass(PopupManager);
        }

        public function get hints():Hints
        {
            return Singleton.getClass(Hints);
        }
    }
}
