/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.09.13
 * Time: 19:48
 */
package application.views.register
{
    import application.views.AppView;
    import application.views.register.navigation.RegisterNavigation;
    import application.views.screen.base.ScreenTitle;

    import flash.display.Bitmap;

    public class RegisterControlView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const WIDTH:int = 400;
        public static const HEIGHT:int = 270;

        public const BACK:String = "INVENTORY_STALL";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RegisterControlView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBack();
            _addScreenTitle();
            _addNavigation();
        }

        override public function destroy():void
        {
            _navigation.destroy();
            super.destroy();
        }

        private function _addNavigation():void
        {
            _navigation = new RegisterNavigation();
            _navigation.x = 55;
            _navigation.y = 202;
            addChild(_navigation);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _title:String = "заголовок";

        private var _screenTitle:ScreenTitle;
        private var _navigation:RegisterNavigation;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            var back:Bitmap = super.source.getBitmap(BACK);
            addChild(back);
        }

        private function _addScreenTitle():void
        {
            _screenTitle = new ScreenTitle(_title);
            addChild(_screenTitle);
            _screenTitle.x = WIDTH / 2 - _screenTitle.width / 2;
            _screenTitle.y = (-1) * _screenTitle.height / 2 + 15;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get page():int
        {
            return _navigation.current;
        }

        public function set title(value:String):void
        {
            _title = value;
            if (_screenTitle)
                _screenTitle.title = _title;
        }
    }
}
