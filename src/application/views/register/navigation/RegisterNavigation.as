/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.10.13
 * Time: 12:30
 */
package application.views.register.navigation
{
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.screen.common.NotificationLabel;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.buttons.SpecialButton;

    import flash.events.Event;
    import flash.events.MouseEvent;

    public class RegisterNavigation extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RegisterNavigation()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupButtons();
            _setupTextField();

            _prevButton.block();
        }

        override public function destroy():void
        {
            _nextButton.destroy();
            _prevButton.destroy();
            _readyButton.destroy();
            _stepField.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _prevButton:AcceptButton;
        private var _nextButton:AcceptButton;
        private var _readyButton:SpecialButton;

        private var _stepField:NotificationLabel;

        private var _stepName:String = TextFactory.instance.getLabel("register_step_label");
        private var _total:int = 3;
        private var _current:int = 1;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupButtons():void
        {
            _prevButton = new AcceptButton(TextFactory.instance.getLabel("register_back_link"));
            _nextButton = new AcceptButton(TextFactory.instance.getLabel("register_next_link"));

            _prevButton.w = 90;
            _nextButton.w = 90;

            _prevButton.fontSize = 12;
            _nextButton.fontSize = 12;

            addChild(_prevButton);
            addChild(_nextButton);

            _nextButton.x = 197;

            _readyButton = new SpecialButton(TextFactory.instance.getLabel("ready_label"));
            _readyButton.w = 90;
            _readyButton.fontSize = 12;
            addChild(_readyButton);

            _readyButton.x = 197;

            _prevButton.addEventListener(MouseEvent.CLICK, _prevClickHandler);
            _nextButton.addEventListener(MouseEvent.CLICK, _nextClickHandler);
            _readyButton.addEventListener(MouseEvent.CLICK, _readyClickHandler);

            _readyButton.visible = false;
        }

        private function _updateLabel():void
        {
            _stepField.label = _getStepLabel();
        }

        private function _setupTextField():void
        {
            _stepField = new NotificationLabel(_getStepLabel());
            _stepField.notificationWidth = 100;
            addChild(_stepField);
            _stepField.x = 93;
        }

        private function _getStepLabel():String
        {
            return _stepName + " " + _current.toString() + "/" + _total;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _nextClickHandler(event:MouseEvent):void
        {
            if (_current < _total) {
                _current++;
                _prevButton.enable();
                _updateLabel();

                if (_current == _total) {
                    _nextButton.block();
                    _readyButton.visible = true;
                }

                dispatchEvent(new Event(Event.CHANGE, true));
                sound.playUISound(UISound.CLICK);
            }
        }

        private function _prevClickHandler(event:MouseEvent):void
        {
            if (_current > 1) {
                _current--;
                _nextButton.enable();
                _updateLabel();

                if (_current == 1)
                    _prevButton.block();

                dispatchEvent(new Event(Event.CHANGE, true));
                sound.playUISound(UISound.CLICK);
                _readyButton.visible = false;
            }
        }

        private function _readyClickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            dispatchEvent(new Event(Event.COMPLETE, true));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get current():int
        {
            return _current;
        }
    }
}
