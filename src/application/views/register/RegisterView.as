/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.09.13
 * Time: 20:01
 */
package application.views.register
{
    import application.events.RegisterEvent;
    import application.models.user.Vegetable;
    import application.service.register.RegisterConfirm;
    import application.service.register.RegisterControl;
    import application.service.register.RegisterData;
    import application.service.register.details.DetailsControl;
    import application.service.register.login.LoginControl;
    import application.service.register.vegetable.VegetableControl;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.popup.AlertWindow;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.core.helpers.MathHelper;

    public class RegisterView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "INKUBATOR_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RegisterView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setControls();
            _addBackground();
            _addControlView();
            _update();

            addEventListener(Event.COMPLETE, _completeHandler);
        }

        override public function destroy():void
        {
            _controlView.destroy();
            _vegetableControl.destroy();
            _detailsControl.destroy();
            _loginControl.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function loginDeny():void
        {
            _loginControl.loginDeny();
        }

        public function loginAllow():void
        {
            _loginControl.loginAllow();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _controlView:RegisterControlView;
        private var _page:int = 1;

        private var _pageControls:Vector.<RegisterControl>;
        private var _currentControl:RegisterControl;

        private var _vegetableControl:VegetableControl;
        private var _detailsControl:DetailsControl;
        private var _loginControl:LoginControl;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setControls():void
        {
            _vegetableControl = new VegetableControl(this);
            _detailsControl = new DetailsControl(this);
            _loginControl = new LoginControl(this);

            _pageControls = new <RegisterControl>[
                _vegetableControl,
                _detailsControl,
                _loginControl
            ];
        }

        private function _addBackground():void
        {
            var back:Bitmap = super.source.getBitmap(BACK);
            addChild(back);
        }

        private function _addControlView():void
        {
            _controlView = new RegisterControlView();
            _controlView.addEventListener(Event.CHANGE, _changeHandler);
            addChild(_controlView);
            _controlView.x = 319;
            _controlView.y = 138;
        }

        private function _update():void
        {
            if (_currentControl)
                _currentControl.done();
            _currentControl = _pageControls[_page - 1];
            _currentControl.open();
            _controlView.title = _currentControl.caption.toUpperCase();
        }

        private function _sendRegisterQuery():void
        {
            var data:RegisterData = new RegisterData();
            data.login = _loginControl.login;
            data.vegetable = _vegetableControl.vegetable.toString();
            _detailsControl.importDetails(data);

            var event:RegisterEvent = new RegisterEvent(RegisterEvent.REGISTER);
            event.registerData = data;
            dispatchEvent(event);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _page = MathHelper.clamp(_controlView.page, 1, 3);
            _update();
        }

        private function _completeHandler(event:Event):void
        {
            event.stopImmediatePropagation();
            if (_loginControl.ready) {
                addChild(new RegisterConfirm(_sendRegisterQuery));
            } else {
                addChild(new AlertWindow(
                        TextFactory.instance.getLabel("register_login_accept_header"),
                        TextFactory.instance.getLabel("register_login_accept")
                ));
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/


        public function get vegetable():Vegetable
        {
            return _vegetableControl.vegetable;
        }

        public function get details():Array
        {
            return _detailsControl.details;
        }
    }
}
