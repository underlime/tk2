/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 03.10.13
 * Time: 21:13
 */
package application.views.screen.journal
{
    import application.views.TextFactory;
    import application.views.screen.base.BaseScreen;
    import application.views.screen.journal.tabs.JournalTabs;
    import application.views.screen.office.OfficeScreen;

    import flash.display.Bitmap;

    public class JournalView extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function JournalView()
        {
            super(TextFactory.instance.getLabel('journal_label'));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addBack();
            _addTabs();
        }

        override public function destroy():void
        {
            _tabs.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _tabs:JournalTabs;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            var back:Bitmap = super.source.getBitmap(OfficeScreen.BACK);
            addChild(back);

            back.x = 38;
            back.y = 101;
        }

        private function _addTabs():void
        {
            _tabs = new JournalTabs();
            addChild(_tabs);
            _tabs.x = 152;
            _tabs.y = 45;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get tabs():JournalTabs
        {
            return _tabs;
        }
    }
}
