/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.10.13
 * Time: 20:33
 */
package application.views.screen.journal.tabs
{
    import application.views.screen.common.tabs.AppTabs;

    import framework.core.display.tabs.ITab;

    public class JournalTabs extends AppTabs
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function JournalTabs()
        {
            var tabs:Vector.<ITab> = new <ITab>[new StatsTab(), new NewsTab()];
            super(tabs);
            _dx = 10;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
