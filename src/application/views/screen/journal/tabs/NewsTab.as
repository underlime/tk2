/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.10.13
 * Time: 20:32
 */
package application.views.screen.journal.tabs
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.user.User;
    import application.models.user.UserOptInfo;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.CounterButton;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.struct.data.ModelsRegistry;

    public class NewsTab extends CounterButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function NewsTab()
        {
            _userModel = ModelsRegistry.getModel(ModelData.USER) as User;
            _optInfo = _userModel.userOptInfo;
            super(TextFactory.instance.getLabel('news_label'), _optInfo.unread_news_count.value);
            _w = 216;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            addEventListener(MouseEvent.CLICK, _clickHandler);
            _optInfo.addEventListener(Event.CHANGE, _onOptData);
        }

        private function _onOptData(e:Event):void
        {
            this.count = _optInfo.unread_news_count.value;
        }

        override public function select():void
        {
            super.select();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TAB_CHOOSE, true));
        }

        private function _clickHandler(event:MouseEvent):void
        {
            select();
        }

        override public function destroy():void
        {
            _optInfo.removeEventListener(Event.CHANGE, _onOptData);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _userModel:User;
        private var _optInfo:UserOptInfo;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
