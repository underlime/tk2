/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 10:39
 */
package application.views.screen.laboratory
{
    import application.events.BuyEvent;
    import application.models.ModelData;
    import application.models.chars.CharDetail;
    import application.models.chars.CharGroup;
    import application.models.chars.CharList;
    import application.models.inventory.Inventory;
    import application.models.inventory.InventoryHelper;
    import application.models.market.Item;
    import application.models.user.User;
    import application.models.user.UserInfo;
    import application.models.user.Vegetable;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.constructor.builders.CharsChanger;
    import application.views.popup.BuyDialog;
    import application.views.popup.BuyingItem;
    import application.views.screen.common.cell.Cell;
    import application.views.screen.common.cell.LaboratoryCell;
    import application.views.screen.laboratory.data.LaboratoryState;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.struct.data.ModelsRegistry;

    public class LaboratoryScreen extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LaboratoryScreen()
        {
            super();
            _charsList = ModelsRegistry.getModel(ModelData.CHAR_LIST) as CharList;
            var model:User = ModelsRegistry.getModel(ModelData.USER) as User;
            _userInfo = model.getChildByName(ModelData.USER_INFO) as UserInfo;
            _inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupLayout();
            _updateParams();
            _bindNavigation();
            _createUnit();
        }

        override public function destroy():void
        {
            _layout.destroy();
            if (!_unit.destroyed)
                _unit.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function updateUnit():void
        {
            if (_unit)
                _unit.update();
            super.sound.playUISound(UISound.CASH);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _layout:LaboratoryLayout;
        private var _charsList:CharList;
        private var _chars:Array = [];
        private var _state:LaboratoryState = new LaboratoryState();
        private var _userInfo:UserInfo;
        private var _inventory:Inventory;
        private var _unit:CharsChanger;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupLayout():void
        {
            _layout = new LaboratoryLayout();
            addChild(_layout);

            _layout.tabs.addEventListener(Event.CHANGE, _tabChangeHandler);
            _layout.tabs.select(0);

            _layout.mesh.addEventListener(Event.SELECT, _selectHandler);
        }

        private function _updateParams():void
        {
            _state.start = 0;

            _state.currentPage = 1;
            _state.totalPages = int(Math.ceil(_getTotalItems().length / _state.limit));

            _updateChars();
            _updateMesh();
            _updatePageData();
        }

        private function _updateChars():void
        {
            var vegetable:Vegetable;
            if (_layout.tabs.selectedTab == 0) {
                vegetable = Vegetable.getEnumByKey(_userInfo.vegetable.value);
                _chars = _charsList.getAllAvailableChars(vegetable, _state.start);
            } else {
                vegetable = Vegetable.getEnumByKey(_state.getVegetableCriteriaByRubric(_layout.tabs.selectedTab, _userInfo.vegetable.value));
                var charGroup:CharGroup = _state.getGroupByTab(_layout.tabs.selectedTab);

                _chars = _charsList.getChars(vegetable, charGroup, _state.start);
            }
        }

        private function _updateMesh():void
        {
            var cells:Vector.<Cell> = new Vector.<Cell>();
            var count:int = _chars.length;

            for (var i:int = 0; i < count; i++) {
                var cell:LaboratoryCell = new LaboratoryCell(_chars[i]);
                cells.push(cell);
            }

            _layout.mesh.cells = cells;
            _layout.mesh.draw();
        }

        private function _getTotalItems():Array
        {
            var vegetable:Vegetable;
            if (_layout.tabs.selectedTab == 0) {
                vegetable = Vegetable.getEnumByKey(_userInfo.vegetable.value);
                return _charsList.getAllAvailableChars(vegetable);
            }

            vegetable = Vegetable.getEnumByKey(_state.getVegetableCriteriaByRubric(_layout.tabs.selectedTab, _userInfo.vegetable.value));
            var charGroup:CharGroup = _state.getGroupByTab(_layout.tabs.selectedTab);

            return _charsList.getChars(vegetable, charGroup);
        }

        private function _updatePageData():void
        {
            _layout.navigation.currentPage = _state.currentPage;
            _layout.navigation.totalPages = _state.totalPages;
        }

        private function _bindNavigation():void
        {
            _layout.changeButton.addEventListener(MouseEvent.CLICK, _buyClickHandler);
            _layout.navigation.addEventListener(Event.CHANGE, _pageChangeHandler);
        }

        private function _updatePage():void
        {
            _state.start = (_state.currentPage - 1) * _state.limit;
            _updateChars();
            _updateMesh();
            _updatePageData();
        }

        private function _createUnit():void
        {
            var items:Vector.<Item> = InventoryHelper.ItemsArray2Vector(_inventory.getBuildingItems());
            _unit = new CharsChanger(_userInfo, items);
            _unit.scaleX = _unit.scaleY = .8;

            addChild(_unit);
            _unit.x = 570;
            _unit.y = 278;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _pageChangeHandler(event:Event):void
        {
            _state.currentPage = _layout.navigation.currentPage;
            _updatePage();
        }

        private function _tabChangeHandler(event:Event):void
        {
            _state.tabID = _layout.tabs.selectedTab;
            _updateParams();

            if (_unit)
                _unit.update();
        }

        private function _selectHandler(event:Event):void
        {
            if (_unit)
                _unit.update();

            var char:CharDetail = _layout.mesh.char;
            if (char) {
                _layout.priceFerros.price = char.price_ferros.value;
                if (char.price_tomatos.value > 0) {
                    _layout.priceTomatos.price = char.price_tomatos.value;
                } else {
                    _layout.priceTomatos.price = 0;
                }

                _unit.change(_layout.mesh.char);
                var charIndex:int = _charsList.getCharIndex(_unit.vegetable, char);
                _layout.charName = LabHelper.getCharName(char, charIndex);
            }
        }

        private function _buyClickHandler(event:MouseEvent):void
        {
            var char:CharDetail = _layout.mesh.char;
            if (char) {
                var dialogItem:BuyingItem = new BuyingItem();
                dialogItem.importFromChar(char);
                var dialog:BuyDialog = new BuyDialog(dialogItem);
                dialog.addEventListener(BuyEvent.BUY, _buyHandler);
                addChild(dialog);
                super.sound.playUISound(UISound.CLICK);
            }
        }

        private function _buyHandler(event:BuyEvent):void
        {
            (event.currentTarget as BuyDialog).destroy();
            event.char = _layout.mesh.char;
            super.dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
