/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.06.13
 * Time: 10:27
 */
package application.views.screen.laboratory.data
{
    import application.models.chars.CharGroup;
    import application.models.user.Vegetable;
    import application.views.screen.common.state.ScreenState;

    public class LaboratoryState extends ScreenState
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LaboratoryState()
        {
            super();
            super.limit = 10;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getGroupByTab(tabID:int):CharGroup
        {

            var group:CharGroup;

            switch (tabID) {
                case 0:
                    group = CharGroup.ALL;
                    break;
                case 1:
                    group = CharGroup.EYES;
                    break;
                case 2:
                    group = CharGroup.MOUTHS;
                    break;
                case 3:
                    group = CharGroup.HAIRS;
                    break;
                case 4:
                    group = CharGroup.BODIES;
                    break;
                default:
                    group = CharGroup.EYES;
                    break;
            }

            return group;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        public function getVegetableCriteriaByRubric(tabID:int, vegetable:String):String
        {
            if (tabID == 3 || tabID == 4)
                return vegetable;
            else
                return Vegetable.ALL.toString();
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
