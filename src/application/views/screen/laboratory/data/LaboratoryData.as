/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 10:52
 */
package application.views.screen.laboratory.data
{
    public class LaboratoryData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ACADEMY_BACKGROUND:String = "AcademyBackPic";
        public static const TRAINING_BUTTON:String = "ACADEMY_TrainingButton";
        public static const LABORATORY_BUTTON:String = "ACADEMY_LaboratoryButton";
        public static const OFFICE_BUTTON:String = "ACADEMY_OfficeButton";

        public static const LABORATORY_BACKGROUND:String = "ACADEMY_LaboratoryBackground";
        public static const EYES_TAB:String = "ACADEMY_EyesTab";
        public static const MOUTH_TAB:String = "ACADEMY_MouthTab";
        public static const HAIR_TAB:String = "ACADEMY_HairTab";
        public static const BODY_TAB:String = "ACADEMY_BodyTab";
        public static const VEGETABLE_TAB:String = "ACADEMY_VegetableTab";
        public static const CHANGE_BUTTON:String = "ACADEMY_ChangeButton";

        public static const TRAINING_BACKGROUND:String = "ACADEMY_TrainingBack";
        public static const TRAINING_RESET:String = "ACADEMY_ResetButton";
        public static const TRAINING_APPROVE:String = "ACADEMY_TrainingApprove";
        public static const TRAINING_CANCEL:String = "ACADEMY_TrainingCancel";
        public static const SKILL_PLUS:String = "ACADEMY_SkillPlus";
        public static const SKILL_MINUS:String = "ACADEMY_SkillMinus";

        public static const RADIO_BUTTON:String = "ACADEMY_RadioButton";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
