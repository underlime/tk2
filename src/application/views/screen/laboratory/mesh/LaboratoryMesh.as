/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 15:37
 */
package application.views.screen.laboratory.mesh
{
    import application.models.chars.CharDetail;
    import application.views.screen.common.cell.Cell;
    import application.views.screen.common.cell.LaboratoryCell;
    import application.views.screen.common.mesh.Mesh;

    public class LaboratoryMesh extends Mesh
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LaboratoryMesh(cells:Vector.<Cell> = null)
        {
            super(cells);

            _lines = 2;
            _cols = 5;
            _limit = 10;
            _y0 = 131;
            _x0 = 70;
            _dy = 87;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        public function get char():CharDetail
        {
            if (_selectedCell is LaboratoryCell)
                return (_selectedCell as LaboratoryCell).char;
            else
                return null;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
