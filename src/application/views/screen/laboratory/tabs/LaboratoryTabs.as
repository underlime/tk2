/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 02.09.13
 * Time: 20:07
 */
package application.views.screen.laboratory.tabs
{
    import application.views.screen.common.filter.FilterTabsPanel;
    import application.views.screen.common.filter.tabs.AllTab;
    import application.views.screen.common.filter.tabs.BodyTab;
    import application.views.screen.common.filter.tabs.EyesTab;
    import application.views.screen.common.filter.tabs.HairTab;
    import application.views.screen.common.filter.tabs.MouthTab;

    import framework.core.display.tabs.ITab;

    public class LaboratoryTabs extends FilterTabsPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LaboratoryTabs()
        {
            var tabs:Vector.<ITab> = new Vector.<ITab>();
            tabs.push(
                    new AllTab(),
                    new EyesTab(),
                    new MouthTab(),
                    new HairTab(),
                    new BodyTab()
            );
            super(tabs);
            _dy = 1;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
