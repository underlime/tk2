/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 31.10.13
 * Time: 15:15
 */
package application.views.screen.laboratory
{
    import application.models.chars.CharDetail;
    import application.models.chars.CharGroup;
    import application.views.TextFactory;

    public class LabHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getCharName(char:CharDetail, index:int):String
        {
            var factory:TextFactory = TextFactory.instance;
            var group:CharGroup = CharGroup.getEnumByKey(char.group.value);
            var basis:String = '';

            switch (group) {
                case CharGroup.BODIES:
                    basis = factory.getLabel('color_label');
                    break;
                case CharGroup.EYES:
                    basis = factory.getLabel('eyes_label');
                    break;
                case CharGroup.HAIRS:
                    basis = factory.getLabel('hair_label');
                    break;
                case CharGroup.MOUTHS:
                    basis = factory.getLabel('mouth_label');
                    break;
            }

            return basis + " " + (index + 1).toString();

        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
