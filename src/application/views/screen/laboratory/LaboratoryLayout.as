/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 10:46
 */
package application.views.screen.laboratory
{
    import application.common.Colors;
    import application.views.AppView;
    import application.views.popup.common.FerrosPrice;
    import application.views.popup.common.TomatosPrice;
    import application.views.screen.common.navigation.Navigation;
    import application.views.screen.home.HomeScreen;
    import application.views.screen.laboratory.mesh.LaboratoryMesh;
    import application.views.screen.laboratory.tabs.LaboratoryTabs;
    import application.views.screen.market.MarketLayout;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.text.TextFormatAlign;

    public class LaboratoryLayout extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const LABORATORY_FRAME:String = "ACADEMY_CHANGE_OUTFIT_CHAR_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LaboratoryLayout()
        {
            super();
            this.y = 107;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addCharName();
            _setupTabs();
            _setupMesh();
            _setChangeButton();
            _addNavigation();
            _addPrices();
        }

        override public function destroy():void
        {
            _tabs.destroy();
            _mesh.destroy();
            _priceFerros.destroy();
            _priceTomatos.destroy();
            _changeButton.destroy();
            _navigation.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _tabs:LaboratoryTabs;

        private var _mesh:LaboratoryMesh;

        private var _changeButton:ChangeButton;

        private var _navigation:Navigation;

        private var _priceTomatos:TomatosPrice;
        private var _priceFerros:FerrosPrice;

        private var _txtCharName:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var marketStall:Bitmap = super.source.getBitmap(MarketLayout.STALL);
            addChild(marketStall);
            marketStall.x = 38;
            marketStall.y = 101;

            var frame:Bitmap = super.source.getBitmap(LABORATORY_FRAME);
            addChild(frame);
            frame.x = 530;
            frame.y = 40;

            var shade:Bitmap = super.source.getBitmap(HomeScreen.SHADOW);
            shade.x = 565;
            shade.y = 246;
            addChild(shade);
        }

        private function _addCharName():void
        {
            _txtCharName = new AppTextField();
            _txtCharName.color = Colors.DARK_BROWN;
            _txtCharName.size = 16;
            _txtCharName.width = 178;
            _txtCharName.align = TextFormatAlign.CENTER;
            _txtCharName.height = 30;
            _txtCharName.bold = true;
            addChild(_txtCharName);
            _txtCharName.text = "";
            _txtCharName.x = 530;
            _txtCharName.y = 288;
        }

        private function _setupTabs():void
        {
            _tabs = new LaboratoryTabs();
            addChild(_tabs);

            _tabs.x = 6;
            _tabs.y = 110;
        }

        private function _setupMesh():void
        {
            _mesh = new LaboratoryMesh();
            addChild(_mesh);
        }

        private function _setChangeButton():void
        {
            _changeButton = new ChangeButton();
            _changeButton.x = 285;
            _changeButton.y = 305;
            addChild(_changeButton);
        }

        private function _addNavigation():void
        {
            _navigation = new Navigation();
            _navigation.arrowMargin = 5;
            addChild(_navigation);

            _navigation.y = 305;
            _navigation.x = 70;
        }

        private function _addPrices():void
        {
            _priceTomatos = new TomatosPrice(0);
            addChild(_priceTomatos);
            _priceTomatos.price = 0;
            _priceTomatos.x = 550;
            _priceTomatos.y = 335;

            _priceFerros = new FerrosPrice(0);
            addChild(_priceFerros);
            _priceFerros.price = 0;
            _priceFerros.y = 335;
            _priceFerros.x = 647;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get mesh():LaboratoryMesh
        {
            return _mesh;
        }

        public function get tabs():LaboratoryTabs
        {
            return _tabs;
        }

        public function get changeButton():ChangeButton
        {
            return _changeButton;
        }

        public function get navigation():Navigation
        {
            return _navigation;
        }

        public function get priceTomatos():TomatosPrice
        {
            return _priceTomatos;
        }

        public function get priceFerros():FerrosPrice
        {
            return _priceFerros;
        }

        public function set charName(value:String):void
        {
            if (_txtCharName)
                _txtCharName.text = value;
        }
    }
}
