/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.12.13
 * Time: 15:50
 */
package application.views.screen.arena.forge
{
    import application.models.inventory.InventoryItem;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.popup.BasePopup;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.cell.ForgeCell;
    import application.views.text.AppTextField;

    import flash.events.MouseEvent;
    import flash.text.TextFormatAlign;

    public class ForgeResultScreen extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ForgeResultScreen(item:InventoryItem, success:Boolean = true)
        {
            super();

            _item = item;
            _success = success;

            _popupWidth = 238;
            _popupHeight = 234;

            _header = success ? TextFactory.instance.getLabel("forge_success") : TextFactory.instance.getLabel("forge_fail");
            _headerSize = 14;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addForgeCell();
            _addDescription();
            _addOkButton();

            if (_success)
                super.sound.playUISound(UISound.SUCCESS);
        }

        override public function destroy():void
        {
            _okButton.destroy();
            _cell.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item:InventoryItem;
        private var _success:Boolean;

        private var _cell:ForgeCell;
        private var _okButton:AcceptButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addForgeCell():void
        {
            _cell = new ForgeCell(_item);
            addChild(_cell);
            _cell.x = _backShape.x + 83;
            _cell.y = _backShape.y + 60;
        }

        private function _addDescription():void
        {
            var txtItem:AppTextField = new AppTextField({
                "width": 300,
                "height": 40,
                "align": TextFormatAlign.CENTER
            });
            addChild(txtItem);
            txtItem.htmlText = ForgeHelper.getItemDesc(_item);
            txtItem.x = _cell.x - 114;
            txtItem.y = _cell.y + 86;
        }

        private function _addOkButton():void
        {
            _okButton = new AcceptButton("OK");
            _okButton.w = 76;
            addChild(_okButton);
            _okButton.x = _backShape.x + 81;
            _okButton.y = _backShape.y + 175;

            _okButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
