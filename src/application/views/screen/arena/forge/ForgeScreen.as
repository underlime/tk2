/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.12.13
 * Time: 20:33
 */
package application.views.screen.arena.forge
{
    import application.common.Colors;
    import application.models.ModelData;
    import application.models.forge.ForgeParams;
    import application.models.inventory.Inventory;
    import application.models.inventory.InventoryHelper;
    import application.models.inventory.InventoryItem;
    import application.service.items.ItemsSelector;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.screen.arena.find.FindOpponentsScreen;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.data.ModelsRegistry;

    public class ForgeScreen extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const FORGE_BACK:String = "Forge_Back";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ForgeScreen()
        {
            super();
            _inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addLabel();
            _addHeader();
            _addDescription();
            _addChooseButton();
            _bindReactions();
        }

        override public function destroy():void
        {
            _chooseButton.destroy();
            if (_itemSelector && !_itemSelector.destroyed)
                _itemSelector.destroy();
            if (_buyScreen && !_buyScreen.destroyed)
                _buyScreen.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function addBuyScreen(forgeParams:ForgeParams):void
        {
            _addBuyScreen(this.item, forgeParams);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _chooseButton:AcceptButton;
        private var _itemSelector:ItemsSelector;
        private var _buyScreen:ForgeBuyScreen;

        private var _inventory:Inventory;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var background:Bitmap = super.source.getBitmap(FindOpponentsScreen.STALL);
            background.x = 145;
            background.y = 130;
            addChild(background);

            var back:Bitmap = super.source.getBitmap(FORGE_BACK);
            back.x = background.x + 13;
            back.y = background.y + 73;
            addChild(back);
        }

        private function _addLabel():void
        {
            var label:ForgeLabel = new ForgeLabel();
            label.x = 292;
            label.y = 148;
            addChild(label);
        }

        private function _addHeader():void
        {
            var txtHeader:AppTextField = new AppTextField({
                "color": Colors.DARK_BROWN,
                "size": 16,
                "align": TextFormatAlign.CENTER,
                "width": 438,
                "height": 20,
                "x": 158,
                "y": 228,
                "bold": true

            });
            addChild(txtHeader);
            txtHeader.text = TextFactory.instance.getLabel("forge_header");
        }

        private function _addDescription():void
        {
            var txtDescription:AppTextField = new AppTextField();

            txtDescription.width = 418;
            txtDescription.multiline = true;
            txtDescription.wordWrap = true;
            txtDescription.autoSize = TextFieldAutoSize.LEFT;
            txtDescription.x = 168;
            txtDescription.y = 258;
            txtDescription.color = Colors.BROWN;
            addChild(txtDescription);

            txtDescription.htmlText = TextFactory.instance.getLabel("forge_description");
        }

        private function _addChooseButton():void
        {
            _chooseButton = new AcceptButton(TextFactory.instance.getLabel("forge_chose_label"));
            _chooseButton.w = 260;
            _chooseButton.x = 245;
            _chooseButton.y = 382;
            _chooseButton.fontSize = 12;
            addChild(_chooseButton);
        }

        private function _bindReactions():void
        {
            _chooseButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _setupItemSelector():void
        {
            _destroyItemSelector();
            _itemSelector = new ItemsSelector(_inventory.getArmorAndWeapon(0));
            _itemSelector.addEventListener(Event.SELECT, _selectItemHandler);
            addChild(_itemSelector);
        }

        private function _destroyItemSelector():void
        {
            if (_itemSelector && !_itemSelector.destroyed)
                _itemSelector.destroy();
        }

        private function _addBuyScreen(item:InventoryItem, forgeParams:ForgeParams):void
        {
            _destroyBuyScreen();
            _buyScreen = new ForgeBuyScreen(item, forgeParams);
            addChild(_buyScreen);
        }

        private function _destroyBuyScreen():void
        {
            if (_buyScreen && !_buyScreen.destroyed)
                _buyScreen.destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            if (_inventory.getArmorAndWeapon(0).length > 0)
                _setupItemSelector();
            else
                super.alert(
                        TextFactory.instance.getLabel("forge_refuse"),
                        TextFactory.instance.getLabel("forge_no_items")
                );
        }

        private function _selectItemHandler(event:Event):void
        {
            var item:InventoryItem = _itemSelector.item;
            if (!InventoryHelper.canUpgrade(item)) {
                super.alert(
                        TextFactory.instance.getLabel("forge_refuse"),
                        TextFactory.instance.getLabel("forge_refuse_msg")
                );
                event.stopImmediatePropagation();
            }

            _destroyItemSelector();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get item():InventoryItem
        {
            return _itemSelector.item;
        }

    }
}
