/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.12.13
 * Time: 18:26
 */
package application.views.screen.arena.forge
{
    import application.config.AppConfig;
    import application.views.AppView;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.enum.Language;

    public class ForgeLabel extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "ForgeLabel";
        public static const W:int = 180;
        public static const H:int = 60;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ForgeLabel()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            var offset:int = AppConfig.LANGUAGE == Language.RU ? 0 : H;
            var rect:Rectangle = new Rectangle(0, offset, W, H);
            var label:Bitmap = super.source.getBitmap(SOURCE);
            label.scrollRect = rect;
            addChild(label);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
