/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.12.13
 * Time: 13:03
 */
package application.views.screen.arena.forge
{
    import application.common.Currency;
    import application.events.BuyEvent;
    import application.models.forge.ForgeParams;
    import application.models.inventory.InventoryItem;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.popup.BasePopup;
    import application.views.popup.common.FerrosPrice;
    import application.views.popup.common.TomatosPrice;
    import application.views.screen.common.GreenArrows;
    import application.views.screen.common.buttons.FerrosButton;
    import application.views.screen.common.buttons.TomatosButton;
    import application.views.screen.common.cell.ForgeCell;
    import application.views.text.AppTextField;

    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.text.TextFormatAlign;

    import framework.core.enum.Side;

    public class ForgeBuyScreen extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ForgeBuyScreen(item:InventoryItem, forgeParams:ForgeParams)
        {
            super();
            _forgeParams = forgeParams;
            _popupWidth = 402;
            _popupHeight = 282;
            _header = item.name_ru.value;
            _headerSize = 14;

            _item = item;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addCloseButton();
            _addBeforeCell();
            _addAfterCell();
            _addArrows();
            _addItemDescription();
            _addItemBonusDescription();
            _addDelimiter();
            _addTomatosButton();
            _addFerrosButton();
            _addTomatosPrice();
            _addFerrosPrice();
        }

        override public function destroy():void
        {
            _afterCell.destroy();
            _beforeCell.destroy();
            _ferrosButton.destroy();
            _tomatosButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item:InventoryItem;
        private var _beforeCell:ForgeCell;

        private var _afterCell:ForgeCell;
        private var _arrows:GreenArrows;
        private var _forgeParams:ForgeParams;

        private var _tomatosButton:TomatosButton;
        private var _ferrosButton:FerrosButton;
        private var _tomatosPrice:TomatosPrice;
        private var _ferrosPrice:FerrosPrice;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBeforeCell():void
        {
            _beforeCell = new ForgeCell(_item);
            _beforeCell.x = _backShape.x + 68;
            _beforeCell.y = _backShape.y + 65;
            addChild(_beforeCell);
        }

        private function _addAfterCell():void
        {
            _afterCell = new ForgeCell(_item);
            _afterCell.x = _backShape.x + 265;
            _afterCell.y = _backShape.y + 65;
            _afterCell.level = _item.sharpening_level.value + 1;
            addChild(_afterCell);
        }

        private function _addArrows():void
        {
            _arrows = new GreenArrows(Side.RIGHT, 5);
            _arrows.x = _backShape.x + 147;
            _arrows.y = _backShape.y + 87;
            addChild(_arrows);
        }

        private function _addItemDescription():void
        {
            var txtItem:AppTextField = new AppTextField({
                "width": 300,
                "align": TextFormatAlign.CENTER
            });
            addChild(txtItem);
            txtItem.htmlText = ForgeHelper.getItemDesc(_item);
            txtItem.x = _beforeCell.x - 114;
            txtItem.y = _backShape.y + 150;
        }

        private function _addDelimiter():void
        {
            var shape:Shape = new Shape();
            addChild(shape);
            shape.x = _backShape.x;
            shape.y = _backShape.y + 178;

            shape.graphics.beginFill(BasePopup.MIDDLE_COLOR);
            shape.graphics.drawRect(4, 0, _popupWidth - 8, 4);
            shape.graphics.endFill();
        }

        private function _addItemBonusDescription():void
        {
            var txtItem:AppTextField = new AppTextField({
                "width": 300,
                "align": TextFormatAlign.CENTER
            });
            addChild(txtItem);
            txtItem.htmlText = ForgeHelper.getBonusDesc(_item, _forgeParams);
            txtItem.x = _afterCell.x - 114;
            txtItem.y = _backShape.y + 150;
        }

        private function _addTomatosButton():void
        {
            _tomatosButton = new TomatosButton();
            _tomatosButton.label = TextFactory.instance.getLabel("reforge_label") + " (" + _forgeParams.success_probability.value.toString() + "%)";
            _tomatosButton.w = 186;
            addChild(_tomatosButton);
            _tomatosButton.x = _backShape.x + 11;
            _tomatosButton.y = _backShape.y + 223;

            _tomatosButton.addEventListener(MouseEvent.CLICK, _tomatosClickHandler);
        }

        private function _addFerrosButton():void
        {
            _ferrosButton = new FerrosButton();
            _ferrosButton.label = TextFactory.instance.getLabel("reforge_label") + " (100%)";
            _ferrosButton.w = 186;
            addChild(_ferrosButton);
            _ferrosButton.x = _backShape.x + 205;
            _ferrosButton.y = _backShape.y + 223;


            _ferrosButton.addEventListener(MouseEvent.CLICK, _ferrosClickHandler);
        }

        private function _addTomatosPrice():void
        {
            _tomatosPrice = new TomatosPrice(_forgeParams.price_tomatos.value);
            addChild(_tomatosPrice);
            _tomatosPrice.x = _tomatosButton.x + _tomatosButton.w / 2 - _tomatosPrice.priceWidth / 2;
            _tomatosPrice.y = _tomatosButton.y - 32;
        }

        private function _addFerrosPrice():void
        {
            _ferrosPrice = new FerrosPrice(_forgeParams.price_ferros.value);
            addChild(_ferrosPrice);
            _ferrosPrice.y = _ferrosButton.y - 32;
            _ferrosPrice.x = _ferrosButton.x + _ferrosButton.w / 2 - _ferrosPrice.priceWidth / 2;
        }

        private function _sendBuyRequest(currency:Currency):void
        {
            super.sound.playUISound(UISound.CLICK);
            var event:BuyEvent = new BuyEvent(BuyEvent.BUY, true);
            event.item = _item;
            event.currency = currency;
            dispatchEvent(event);
            destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _ferrosClickHandler(event:MouseEvent):void
        {
            _sendBuyRequest(Currency.FERROS);
        }

        private function _tomatosClickHandler(event:MouseEvent):void
        {
            _sendBuyRequest(Currency.TOMATOS);
        }

        // ACCESSORS ---------------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/


    }
}
