/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.12.13
 * Time: 12:27
 */
package application.views.screen.arena.forge
{
    import application.common.ArrangementVariant;
    import application.models.bonus.Bonus;
    import application.models.forge.ForgeParams;
    import application.models.inventory.InventoryItem;
    import application.views.TextFactory;

    public class ForgeHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static var _factory:TextFactory = TextFactory.instance;
        private static var _brown:String = "#3c2415";
        private static var _green:String = "#006838";
        private static var _red:String = "#be1e2d";

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getItemDesc(item:InventoryItem):String
        {
            var av:ArrangementVariant = ArrangementVariant.getEnumByKey(item.arrangement_variant.value);
            switch (av) {
                case ArrangementVariant.GLOVES:
                case ArrangementVariant.ONE_HANDED:
                case ArrangementVariant.ROD_WEAPON:
                case ArrangementVariant.TWO_HANDED:
                    return _getDamageDesc(item);
                case ArrangementVariant.ARMOR:
                case ArrangementVariant.SHIELD:
                    return _getArmorDesc(item);
            }
            return "";
        }

        private static function _getDamageDesc(item:InventoryItem):String
        {
            var bonus:Bonus = new Bonus(new XML(item.bonuses.value));
            var dmg:int = item.sharpen_damage.value > 0 ? item.sharpen_damage.value : bonus.damage;
            var html:String = "";
            html += "<font color='" + _red + "'><b>" + _factory.getLabel('short_dmg_label') + ":</b> <font color='" + _brown + "'><b>" + dmg + "</b></font></font>";
            return html;
        }

        private static function _getArmorDesc(item:InventoryItem):String
        {
            var bonus:Bonus = new Bonus(new XML(item.bonuses.value));
            var armor:int = item.sharpen_armor.value > 0 ? item.sharpen_armor.value : bonus.armor;
            var html:String = "";
            html += "<font color='" + _red + "'><b>" + _factory.getLabel('short_armor_label') + ":</b> <font color='" + _brown + "'><b>" + armor + "</b></font></font>";
            return html;
        }


        public static function getBonusDesc(item:InventoryItem, forgeParams:ForgeParams):String
        {
            var av:ArrangementVariant = ArrangementVariant.getEnumByKey(item.arrangement_variant.value);
            switch (av) {
                case ArrangementVariant.GLOVES:
                case ArrangementVariant.ONE_HANDED:
                case ArrangementVariant.ROD_WEAPON:
                case ArrangementVariant.TWO_HANDED:
                    return _getBonusDamage(item, forgeParams);
                case ArrangementVariant.ARMOR:
                case ArrangementVariant.SHIELD:
                    return _getBonusArmor(item, forgeParams);
            }
            return "";
        }

        private static function _getBonusDamage(item:InventoryItem, forgeParams:ForgeParams):String
        {
            var bonus:Bonus = new Bonus(new XML(item.bonuses.value));
            var html:String = "";
            var compare:int = Math.max(item.sharpen_damage.value, bonus.damage);
            html += "<font color='" + _red + "'><b>" + _factory.getLabel('short_dmg_label') + ":</b> <font color='" + _brown + "'><b>" + forgeParams.damage.value + "</b></font></font> <font color='" + _green + "'><b>(+" + (forgeParams.damage.value - compare).toString() + ")</b></font>";
            return html;
        }

        private static function _getBonusArmor(item:InventoryItem, forgeParams:ForgeParams):String
        {
            var bonus:Bonus = new Bonus(new XML(item.bonuses.value));
            var html:String = "";
            var compare:int = Math.max(item.sharpen_armor.value, bonus.armor);
            html += "<font color='" + _red + "'><b>" + _factory.getLabel('short_armor_label') + ":</b> <font color='" + _brown + "'><b>" + forgeParams.armor.value + "</b></font></font> <font color='" + _green + "'><b>(+" + (forgeParams.armor.value - compare).toString() + ")</b></font>";
            return html;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
