/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.12.13
 * Time: 20:01
 */
package application.views.screen.arena
{
    import application.config.AppConfig;
    import application.views.AppView;
    import application.views.screen.arena.common.ArenaTabs;
    import application.views.screen.common.buttons.CloseButton;
    import application.views.screen.common.tabs.AppTabs;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.core.display.animation.Animation;

    public class ArenaScreen extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "ARENA_BACKGROUND";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArenaScreen()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBack();
            _addFire();
            _setupCloseButton();
            _addTabs();
        }

        override public function destroy():void
        {
            _leftFire.destroy();
            _rightFire.destroy();
            _closeButton.destroy();
            _tabs.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _leftFire:Animation;
        private var _rightFire:Animation;
        private var _closeButton:CloseButton;
        private var _tabs:AppTabs;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            this.graphics.beginFill(0x000000, .5);
            this.graphics.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            this.graphics.endFill();

            var background:Bitmap = super.source.getBitmap(BACKGROUND);
            addChild(background);
        }

        private function _addFire():void
        {
            _leftFire = new ArenaFire();
            _rightFire = new ArenaFire();

            addChild(_leftFire);
            addChild(_rightFire);

            _leftFire.x = 24;
            _leftFire.y = 155;

            _rightFire.x = 596;
            _rightFire.y = 155;
        }

        private function _setupCloseButton():void
        {
            _closeButton = new CloseButton();
            _closeButton.x = 677;
            _closeButton.y = 13;

            addChild(_closeButton);
        }

        private function _addTabs():void
        {
            _tabs = new ArenaTabs();
            _tabs.addEventListener(Event.CHANGE, _changeHandler);
            _tabs.y = 72;
            _tabs.x = 210;
            addChild(_tabs);
            _tabs.select(0);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get tabID():int
        {
            if (_tabs)
                return _tabs.selectedTab;
            return 0;
        }
    }
}
