/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.12.13
 * Time: 20:19
 */
package application.views.screen.arena.common
{
    import application.views.screen.common.tabs.AppTabs;
    import application.views.screen.common.tabs.TabFactory;
    import application.views.screen.common.tabs.TabType;

    import framework.core.display.tabs.ITab;

    public class ArenaTabs extends AppTabs
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArenaTabs()
        {
            var tabs:Vector.<ITab> = new Vector.<ITab>();
            tabs.push(
                    TabFactory.getTab(TabType.FIND_OPPONENTS),
                    TabFactory.getTab(TabType.FORGE)
            );

            super(tabs);
            _dx = 3;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
