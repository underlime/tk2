/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 14.09.13
 * Time: 18:53
 */
package application.views.screen.arena.common
{
    import application.views.screen.common.navigation.Counter;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldType;
    import flash.ui.Keyboard;

    import framework.core.display.rubber.RubberView;
    import framework.core.struct.view.View;
    import framework.core.tools.SourceManager;

    public class ArenaField extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArenaField()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBack();
            _addText();
            addEventListener(KeyboardEvent.KEY_DOWN, _keyHandler);
        }

        override public function destroy():void
        {
            removeEventListener(KeyboardEvent.KEY_DOWN, _keyHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _input:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            var left:Bitmap = SourceManager.instance.getBitmap(Counter.BOX_LEFT);
            var center:Bitmap = SourceManager.instance.getBitmap(Counter.BOX_CENTER);
            var right:Bitmap = SourceManager.instance.getBitmap(Counter.BOX_RIGHT);

            var rubber:RubberView = new RubberView(left, center, right);
            rubber.w = 142; // 250
            rubber.scrollRect = new Rectangle(0, 40, 142, 40);
            addChild(rubber);
        }

        private function _addText():void
        {
            _input = new AppTextField();
            _input.width = 110;
            _input.height = 30;
            _input.type = TextFieldType.INPUT;
            _input.color = AppTextField.DARK_BROWN;
            _input.size = 16;
            _input.bold = true;
            addChild(_input);
            _input.x = 16;
            _input.y = 5;
            _input.mouseEnabled = true;
            _input.selectable = true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _keyHandler(event:KeyboardEvent):void
        {
            if (event.keyCode == Keyboard.ENTER) {
                dispatchEvent(new Event(Event.SELECT))
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get input():AppTextField
        {
            return _input;
        }
    }
}
