/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.09.13
 * Time: 14:54
 */
package application.views.screen.arena.find
{
    import application.models.arena.EnemyRecord;
    import application.models.user.UserLevelParams;
    import application.views.TextFactory;
    import application.views.screen.arena.common.ArenaKey;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class PlayerStatistics extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TABLE:String = "ARENA_BARS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PlayerStatistics(enemyRecord:EnemyRecord)
        {
            super();
            _enemyRecord = enemyRecord;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addLogin();
            _addMaxLevel();
            _addParams();
            _addTable();
            _addSkills();
            _addBars();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _enemyRecord:EnemyRecord;

        private var _txtLogin:AppTextField;
        private var _txtLevel:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addLogin():void
        {
            _txtLogin = new AppTextField();
            _txtLogin.size = 14;
            _txtLogin.color = 0x3c2415;
            _txtLogin.bold = true;
            _txtLogin.autoSize = TextFieldAutoSize.LEFT;

            addChild(_txtLogin);
            _txtLogin.text = _enemyRecord.user_info.login.value;
            _txtLogin.x = 6;
            _txtLogin.y = 1;
        }

        private function _addMaxLevel():void
        {
            _txtLevel = new AppTextField();
            _txtLevel.size = 12;
            _txtLevel.color = 0xbe1e2d;
            _txtLevel.bold = true;
            _txtLevel.autoSize = TextFieldAutoSize.LEFT;

            addChild(_txtLevel);
            _txtLevel.text = TextFactory.instance.getLabel('top_level_label') + "." + _enemyRecord.user_level_params.max_level.value.toString();
            _txtLevel.x = _txtLogin.x + _txtLogin.textWidth + 5;
            _txtLevel.y = 1;
        }

        private function _addParams():void
        {
            var txtHealth:ArenaKey = new ArenaKey();
            addChild(txtHealth);
            txtHealth.text = TextFactory.instance.getLabel('arena_health_label');

            txtHealth.x = 3;
            txtHealth.y = 23;

            var txtValue:ArenaValue = new ArenaValue();
            txtValue.color = AppTextField.RED;
            addChild(txtValue);
            var hp:int = _enemyRecord.user_level_params.max_hp.value + _enemyRecord.bonus_hp.value;

            txtValue.text = hp.toString();
            txtValue.x = txtHealth.textWidth + txtHealth.x;
            txtValue.y = 23;

            var txtRate:ArenaKey = new ArenaKey();
            addChild(txtRate);
            txtRate.text = TextFactory.instance.getLabel('arena_rate_label');

            txtRate.x = txtValue.x + txtValue.textWidth + 10;
            txtRate.y = 23;

            var txtRateValue:ArenaValue = new ArenaValue();
            txtRateValue.color = AppTextField.RED;
            addChild(txtRateValue);
            txtRateValue.text = _enemyRecord.user_level_params.level.value.toString();
            txtRateValue.x = txtRate.textWidth + txtRate.x;
            txtRateValue.y = 23;
        }

        private function _addSkills():void
        {
            var txtStrength:ArenaKey = new ArenaKey();
            addChild(txtStrength);
            txtStrength.text = TextFactory.instance.getLabel("arena_strength_label");
            txtStrength.x = 3;
            txtStrength.y = 41;

            var txtAgility:ArenaKey = new ArenaKey();
            addChild(txtAgility);
            txtAgility.text = TextFactory.instance.getLabel("arena_agility_label");
            txtAgility.x = 3;
            txtAgility.y = 58;

            var txtIntellect:ArenaKey = new ArenaKey();
            addChild(txtIntellect);
            txtIntellect.text = TextFactory.instance.getLabel("arena_intellect_label");
            txtIntellect.x = 3;
            txtIntellect.y = 75;

            var strengthVal:ArenaValue = new ArenaValue();
            strengthVal.color = AppTextField.RED;
            strengthVal.autoSize = TextFieldAutoSize.NONE;
            strengthVal.align = TextFormatAlign.CENTER;
            strengthVal.width = 34;
            addChild(strengthVal);
            strengthVal.text = _enemyRecord.user_level_params.strength.value.toString();
            strengthVal.x = 117;
            strengthVal.y = 41;

            var agilityVal:ArenaValue = new ArenaValue();
            agilityVal.color = AppTextField.GREEN;
            agilityVal.autoSize = TextFieldAutoSize.NONE;
            agilityVal.align = TextFormatAlign.CENTER;
            agilityVal.width = 34;
            addChild(agilityVal);
            agilityVal.text = _enemyRecord.user_level_params.agility.value.toString();
            agilityVal.x = 117;
            agilityVal.y = 58;

            var intellectVal:ArenaValue = new ArenaValue();
            intellectVal.color = AppTextField.BLUE;
            intellectVal.autoSize = TextFieldAutoSize.NONE;
            intellectVal.align = TextFormatAlign.CENTER;
            intellectVal.width = 34;
            addChild(intellectVal);
            intellectVal.text = _enemyRecord.user_level_params.intellect.value.toString();
            intellectVal.x = 117;
            intellectVal.y = 75;
        }

        private function _addTable():void
        {
            var table:Bitmap = super.source.getBitmap(TABLE);
            addChild(table);
            table.x = 32;
            table.y = 43;
        }

        private function _addBars():void
        {
            var lvlParams:UserLevelParams = _enemyRecord.user_level_params;
            var total:int = lvlParams.strength.value + lvlParams.agility.value + lvlParams.intellect.value;

            var strengthBar:BarShape = BarsHelper.getShapeBar(lvlParams.strength.value, total);
            strengthBar.scrollRect = new Rectangle(0, 0, 80, 12);
            addChild(strengthBar);
            strengthBar.x = 34;
            strengthBar.y = 45;

            var agilityShape:BarShape = BarsHelper.getShapeBar(lvlParams.agility.value, total);
            agilityShape.offset = 12;
            addChild(agilityShape);
            agilityShape.x = 34;
            agilityShape.y = 62;

            var intellectShape:BarShape = BarsHelper.getShapeBar(lvlParams.intellect.value, total);
            intellectShape.offset = 24;
            addChild(intellectShape);
            intellectShape.x = 34;
            intellectShape.y = 79;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
