/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.07.13
 * Time: 15:50
 */
package application.views.screen.arena.find
{
    import application.events.ArenaEvent;
    import application.events.BattleEvent;
    import application.events.InfoEvent;
    import application.helpers.LoginHelper;
    import application.models.ModelData;
    import application.models.arena.EnemiesList;
    import application.models.arena.EnemyRecord;
    import application.models.user.User;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.screen.arena.common.ArenaField;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.context.CommonContext;
    import application.views.screen.common.navigation.Counter;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;
    import framework.core.utils.DisplayObjectUtils;

    import mx.utils.StringUtil;

    public class FindOpponentsScreen extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const STALL:String = "ARENA_STALL";
        public static const FOOTER:String = "ARENA_FOOTER";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FindOpponentsScreen()
        {
            super();
            _enemies = ModelsRegistry.getModel(ModelData.ENEMIES_LIST) as EnemiesList;
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addCont();
            _addRefreshButton();
            _addCounter();
            _addInput();
            _addBottomLabels();
            _addContext();
        }

        override public function destroy():void
        {
            _refreshButton.destroy();
            _context.destroy();
            _arenaField.destroy();
            _clear();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            _clear();
            _drawPositions();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _enemies:EnemiesList;
        private var _cards:Vector.<ArenaPosition> = new Vector.<ArenaPosition>();
        private var _refreshButton:AcceptButton;
        private var _counter:Counter;
        private var _arenaField:ArenaField;

        private var _user:User;
        private var _positionsCont:View = new View();
        private var _context:CommonContext;
        private var _currentPosition:ArenaPosition;
        private var _blockSearch:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var back:Bitmap = super.source.getBitmap(STALL);
            back.x = 145;
            back.y = 130;
            addChild(back);

            var footer:Bitmap = super.source.getBitmap(FOOTER);
            addChild(footer);
            footer.x = 144;
            footer.y = 457;
        }

        private function _addCont():void
        {
            addChild(_positionsCont);
        }

        private function _clear():void
        {
            var count:int = _cards.length;
            for (var i:int = 0; i < count; i++) {
                _cards[i].removeEventListener(MouseEvent.CLICK, _positionClickHandler);
                _cards[i].destroy();
            }
            _cards = new Vector.<ArenaPosition>();
        }

        private function _addRefreshButton():void
        {
            _refreshButton = new AcceptButton(TextFactory.instance.getLabel("refresh_label"));
            _refreshButton.w = 126;
            _refreshButton.fontSize = 14;
            addChild(_refreshButton);
            _refreshButton.x = 313;
            _refreshButton.y = 463;

            _refreshButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _addCounter():void
        {
            _counter = new Counter();
            _counter.min = 0;
            _counter.max = 999;
            _counter.arrowMargin = 6;
            addChild(_counter);

            _counter.x = 160;
            _counter.y = 463;
            _counter.count = _user.userLevelParams.max_level.value;
        }

        private function _addInput():void
        {
            _arenaField = new ArenaField();
            addChild(_arenaField);
            _arenaField.x = 456;
            _arenaField.y = 463;
            _arenaField.addEventListener(Event.SELECT, _selectHandler);
        }

        private function _addBottomLabels():void
        {
            var txtSearchLevel:ShadowAppText = new ShadowAppText(AppTextField.DARK_BROWN);
            txtSearchLevel.autoSize = TextFieldAutoSize.NONE;
            txtSearchLevel.width = 158;
            txtSearchLevel.align = TextFormatAlign.CENTER;
            txtSearchLevel.size = 12;
            addChild(txtSearchLevel);
            txtSearchLevel.text = TextFactory.instance.getLabel('arena_level_search');

            txtSearchLevel.x = 144;
            txtSearchLevel.y = 447;

            var txtSearchID:ShadowAppText = new ShadowAppText(AppTextField.DARK_BROWN);
            txtSearchID.autoSize = TextFieldAutoSize.NONE;
            txtSearchID.width = 158;
            txtSearchID.align = TextFormatAlign.CENTER;
            txtSearchID.size = 12;
            addChild(txtSearchID);
            txtSearchID.text = TextFactory.instance.getLabel('arena_id_search');

            txtSearchID.x = 447;
            txtSearchID.y = 447;
        }

        private function _drawPositions():void
        {
            var x0:Number = 156;
            var y0:Number = 140;

            var cx:Number = x0;
            var cy:Number = y0;

            var dx:Number = 219;
            var dy:Number = 102;

            var even:Boolean = false;

            var list:Object = _enemies.list.value;

            for (var key:String in list) {
                var position:ArenaPosition = new ArenaPosition(list[key] as EnemyRecord);
                _positionsCont.addChild(position);

                position.x = cx;
                position.y = cy;

                _cards.push(position);

                if (!even) {
                    cx += dx;
                } else {
                    cx = x0;
                    cy += dy;
                }

                even = !even;

                position.addEventListener(MouseEvent.CLICK, _positionClickHandler);
            }
        }

        private function _addContext():void
        {
            _context = new CommonContext(_fight, _callInfo);
            addChild(_context);
            _context.hide();
        }

        private function _fight():void
        {
            var model:EnemyRecord = _currentPosition.enemyRecord;
            var event:BattleEvent = new BattleEvent(BattleEvent.CALL_BATTLE, true);
            event.enemy_id = model.user_info.user_id.value;
            event.enemyRecord = model;
            super.dispatchEvent(event);
        }

        private function _callInfo():void
        {
            var model:EnemyRecord = _currentPosition.enemyRecord;
            var event:InfoEvent = new InfoEvent(InfoEvent.GET_INFO);
            event.soc_net_id = model.user_info.soc_net_id.value;
            super.dispatchEvent(event);
        }

        private function _searchEnemies():void
        {
            var identity:String = StringUtil.trim(_arenaField.input.text);
            var event:ArenaEvent;

            if (LoginHelper.LOGIN_PATTERN.test(identity)) {
                event = new ArenaEvent(ArenaEvent.GET_BY_IDENTITY);
                event.identity = identity;
            } else {
                var level:int = _counter.count;
                event = new ArenaEvent(ArenaEvent.GET_BY_LEVEL);
                event.level = level;
            }

            dispatchEvent(event);
            super.sound.playUISound(UISound.CLICK);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _positionClickHandler(event:MouseEvent):void
        {
            _currentPosition = event.currentTarget as ArenaPosition;

            _context.show();
            _context.x = mouseX;
            _context.y = mouseY;

            DisplayObjectUtils.moveIntoParent(_context, this);
            super.sound.playUISound(UISound.CLICK);
        }

        private function _clickHandler(e:MouseEvent):void
        {
            if (!_blockSearch)
                _searchEnemies();
        }

        private function _selectHandler(event:Event):void
        {
            if (!_blockSearch)
                _searchEnemies();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get blockSearch():Boolean
        {
            return _blockSearch;
        }

        public function set blockSearch(value:Boolean):void
        {
            _blockSearch = value;
        }
    }
}
