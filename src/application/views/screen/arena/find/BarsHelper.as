/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.09.13
 * Time: 20:51
 */
package application.views.screen.arena.find
{
    import flash.display.BitmapData;
    import flash.display.Shape;

    import framework.core.tools.SourceManager;

    public class BarsHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOTAL_WIDTH:int = 80;

        public static const BAR:String = "STATS_BARS";

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getShapeBar(strength:int, total:int):BarShape
        {
            var bitmapData:BitmapData = SourceManager.instance.getBitmapData(BAR);
            var needWidth:int = Math.round((TOTAL_WIDTH * strength) / total);
            var shape:Shape = new Shape();

            shape.graphics.beginBitmapFill(bitmapData);
            shape.graphics.drawRect(0, 0, needWidth, 36);
            shape.graphics.endFill();

            return new BarShape(shape);
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
