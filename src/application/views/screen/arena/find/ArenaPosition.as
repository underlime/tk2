/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 21.07.13
 * Time: 20:25
 */
package application.views.screen.arena.find
{
    import application.models.arena.EnemyRecord;
    import application.models.inventory.Inventory;
    import application.models.inventory.InventoryHelper;
    import application.models.market.Item;
    import application.models.user.UserInfo;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.constructor.builders.ClothesBuilder;
    import application.views.constructor.common.TemplateType;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    public class ArenaPosition extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "ARENA_ENEMY_BLOCK";


        public static const WIDTH:int = 220;
        public static const HEIGHT:int = 100;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArenaPosition(model:EnemyRecord)
        {
            super();
            _enemyRecord = model;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _drawBackground();
            _addInfo();
            _addUnit();
            _bindReactions();

            this.scrollRect = new Rectangle(0, 0, WIDTH, HEIGHT);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        override public function destroy():void
        {
            _unit.destroy();
            removeEventListener(MouseEvent.ROLL_OVER, _rollOver);
            removeEventListener(MouseEvent.ROLL_OUT, _rollOut);
            super.destroy();
        }

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _enemyRecord:EnemyRecord;
        private var _unit:ClothesBuilder;

        private var _back:Bitmap;
        private var _rect:Rectangle = new Rectangle(0, 0, WIDTH, HEIGHT);

        private var _playerStatistics:PlayerStatistics;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _drawBackground():void
        {
            _back = super.source.getBitmap(BACK);
            _back.scrollRect = _rect;
            addChild(_back);
        }

        private function _addInfo():void
        {
            _playerStatistics = new PlayerStatistics(_enemyRecord);
            addChild(_playerStatistics);
        }

        private function _addUnit():void
        {
            var inventory:Inventory = _enemyRecord.user_inventory;
            var arrItems:Array = inventory.getBuildingItems();

            var userInfo:UserInfo = _enemyRecord.user_info;
            var items:Vector.<Item> = InventoryHelper.ItemsArray2Vector(arrItems);

            _unit = new ClothesBuilder(userInfo, items);
            addChild(_unit);
            _unit.scaleX = .55;
            _unit.scaleY = .55;

            _unit.y = 47;
            _unit.x = _unit.templateType == TemplateType.SHORT ? 162 : 176;

            _unit.dress();
        }

        private function _bindReactions():void
        {
            buttonMode = true;
            useHandCursor = true;

            addEventListener(MouseEvent.ROLL_OVER, _rollOver);
            addEventListener(MouseEvent.ROLL_OUT, _rollOut);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _rollOver(event:MouseEvent):void
        {
            _rect.y = HEIGHT;
            _back.scrollRect = _rect;
            super.sound.playUISound(UISound.MOUSE_OVER);
        }

        private function _rollOut(event:MouseEvent):void
        {
            _rect.y = 0;
            _back.scrollRect = _rect;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get enemyRecord():EnemyRecord
        {
            return _enemyRecord;
        }
    }
}
