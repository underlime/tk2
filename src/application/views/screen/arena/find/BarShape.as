/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 14.09.13
 * Time: 18:23
 */
package application.views.screen.arena.find
{
    import com.greensock.TweenLite;

    import flash.display.Shape;
    import flash.geom.Rectangle;

    import framework.core.struct.view.View;

    public class BarShape extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BarShape(shape:Shape)
        {
            super();
            _shape = shape;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            addChild(_shape);
            _targetWidth = _shape.width;
            _rect = new Rectangle(0, _offset, 0, 12);

            this.scrollRect = _rect;

            var params:Object = {
                "width": _targetWidth,
                "onUpdate": _updateRect,
                "onComplete": _onComplete
            };

            TweenLite.to(_rect, .8, params);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _shape:Shape;
        private var _targetWidth:int = 0;
        private var _rect:Rectangle;

        private var _offset:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateRect():void
        {
            this.scrollRect = _rect;
        }

        private function _onComplete():void
        {
            TweenLite.killTweensOf(_rect);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set offset(value:int):void
        {
            _offset = value;
        }
    }
}
