/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.08.13
 * Time: 14:59
 */
package application.views.screen.common.navigation
{
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.display.buttons.SpriteButton;
    import framework.core.display.rubber.RubberView;

    public class Counter extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "NAV_ARROWS";

        public static const BOX_LEFT:String = "INPUT_BOX_01";
        public static const BOX_CENTER:String = "INPUT_BOX_02";
        public static const BOX_RIGHT:String = "INPUT_BOX_03";

        public static const RUBBER_HEIGHT:int = 40;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Counter()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addButtons();
            _addFrame();
            _addCountText();
            _updatePositions();
            _bindButtons();
            _checkCount();
        }

        override public function destroy():void
        {
            _unbindButtons();
            _leftButton.destroy();
            _rightButton.destroy();
            _txtLabel.destroy();
            _rubber.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _arrowMargin:int = 9;

        protected var _rubber:RubberView;
        protected var _txtLabel:ShadowAppText;

        protected var _leftButton:SpriteButton;
        protected var _rightButton:SpriteButton;

        protected var _fontSize:int = 16;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _updatePositions():void
        {
            _rubber.x = _arrowMargin + _leftButton.w;
            _rightButton.x = _rubber.x + _rubber.w + _arrowMargin;

            _leftButton.y = RUBBER_HEIGHT / 2 - _leftButton.h / 2;
            _rightButton.y = RUBBER_HEIGHT / 2 - _rightButton.h / 2;

            _txtLabel.x = _rubber.x;
        }

        protected function _updateText():void
        {
            if (_txtLabel)
                _txtLabel.text = _count.toString();
        }

        protected function _checkCount():void
        {
            if (_count <= _min) {
                _leftButton.block();
                _count = _min;
            }

            if (_count >= _max) {
                _rightButton.block();
                _count = _max;
            }

            _updateText();
        }

        protected function _bindButtons():void
        {
            _leftButton.addEventListener(MouseEvent.CLICK, _leftClickHandler);
            _rightButton.addEventListener(MouseEvent.CLICK, _rightClickHandler);
        }

        protected function _unbindButtons():void
        {
            _leftButton.removeEventListener(MouseEvent.CLICK, _leftClickHandler);
            _rightButton.removeEventListener(MouseEvent.CLICK, _rightClickHandler);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _min:int = 1;
        private var _max:int = 99;

        private var _count:int = 1;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addFrame():void
        {
            var left:Bitmap = super.source.getBitmap(BOX_LEFT);
            var center:Bitmap = super.source.getBitmap(BOX_CENTER);
            var right:Bitmap = super.source.getBitmap(BOX_RIGHT);

            _rubber = new RubberView(left, center, right);
            _rubber.w = 56;
            _rubber.scrollRect = new Rectangle(0, 0, _rubber.w, 40);
            addChild(_rubber);
        }

        private function _addCountText():void
        {
            _txtLabel = new ShadowAppText(0x5a291c);
            _txtLabel.size = _fontSize;
            _txtLabel.autoSize = TextFieldAutoSize.NONE;
            _txtLabel.align = TextFormatAlign.CENTER;
            _txtLabel.width = _rubber.w;
            addChild(_txtLabel);
            _txtLabel.text = _count.toString();
            _txtLabel.y = RUBBER_HEIGHT / 2 - _txtLabel.textHeight / 2 - 3;
        }

        protected function _addButtons():void
        {
            _leftButton = new LeftArrow();
            _rightButton = new RightArrow();
            addChild(_leftButton);
            addChild(_rightButton);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _leftClickHandler(event:MouseEvent):void
        {
            if (_count > _min) {
                _count--;
                _updateText();

                if (_rightButton.blocked)
                    _rightButton.enable();

                if (_count == _min)
                    _leftButton.block();
                dispatchEvent(new Event(Event.CHANGE));
                super.sound.playUISound(UISound.CLICK);
            }
        }

        protected function _rightClickHandler(event:MouseEvent):void
        {
            if (_count < _max) {
                _count++;
                _updateText();

                if (_leftButton.blocked)
                    _leftButton.enable();

                if (_count == _max)
                    _rightButton.block();
                dispatchEvent(new Event(Event.CHANGE));
                super.sound.playUISound(UISound.CLICK);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get count():int
        {
            return _count;
        }

        public function set count(value:int):void
        {
            _count = value;
            _updateText();
            _checkCount();
        }

        public function get arrowMargin():int
        {
            return _arrowMargin;
        }

        public function set arrowMargin(value:int):void
        {
            _arrowMargin = value;
        }

        public function get min():int
        {
            return _min;
        }

        public function set min(value:int):void
        {
            _min = value;
        }

        public function get max():int
        {
            return _max;
        }

        public function set max(value:int):void
        {
            _max = value;
        }
    }
}
