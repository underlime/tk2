/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.08.13
 * Time: 16:52
 */
package application.views.screen.common.navigation
{
    import application.sound.lib.UISound;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.helpers.MathHelper;

    public class Navigation extends Counter
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Navigation()
        {
            super();
            _fontSize = 14;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addArrows();
            super.render();
            _updateText();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _updatePositions():void
        {
            _leftButton.x = _arrowMargin + _leftEdgeArrow.w;

            _rubber.x = _arrowMargin + _leftButton.w + _leftButton.x;
            _rightButton.x = _rubber.x + _rubber.w + _arrowMargin;

            _rightEdgeArrow.x = _rightButton.x + _rightButton.w + _arrowMargin;

            _leftButton.y = Counter.RUBBER_HEIGHT / 2 - _leftButton.h / 2;
            _rightButton.y = Counter.RUBBER_HEIGHT / 2 - _rightButton.h / 2;
            _leftEdgeArrow.y = Counter.RUBBER_HEIGHT / 2 - _leftEdgeArrow.h / 2;
            _rightEdgeArrow.y = Counter.RUBBER_HEIGHT / 2 - _rightEdgeArrow.h / 2;

            _txtLabel.x = _rubber.x;
        }

        override protected function _updateText():void
        {
            if (_txtLabel)
                _txtLabel.text = _currentPage.toString() + "/" + _totalPages.toString();
        }

        override protected function _checkCount():void
        {
            if (_currentPage == _minPage) {
                _leftButton.block();
                _leftEdgeArrow.block();
            } else {
                if (_leftButton.blocked)
                    _leftButton.enable();
                if (_leftEdgeArrow.blocked)
                    _leftEdgeArrow.enable();
            }

            if (_currentPage == _totalPages) {
                _rightButton.block();
                _rightEdgeArrow.block();
            } else {
                if (_rightButton.blocked)
                    _rightButton.enable();
                if (_rightEdgeArrow.blocked)
                    _rightEdgeArrow.enable();
            }

        }

        override protected function _bindButtons():void
        {
            super._bindButtons();
            _leftEdgeArrow.addEventListener(MouseEvent.CLICK, _leftEdgeClickHandler);
            _rightEdgeArrow.addEventListener(MouseEvent.CLICK, _rightEdgeClickHandler);
        }

        override protected function _unbindButtons():void
        {
            super._unbindButtons();
            _leftEdgeArrow.removeEventListener(MouseEvent.CLICK, _leftEdgeClickHandler);
            _rightEdgeArrow.removeEventListener(MouseEvent.CLICK, _rightEdgeClickHandler);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _minPage:int = 1;

        private var _currentPage:int = 1;
        private var _totalPages:int = 10;

        protected var _leftEdgeArrow:LeftEdgeArrow;
        protected var _rightEdgeArrow:RightEdgeArrow;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addArrows():void
        {
            _leftEdgeArrow = new LeftEdgeArrow();
            _rightEdgeArrow = new RightEdgeArrow();
            addChild(_leftEdgeArrow);
            addChild(_rightEdgeArrow);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _leftClickHandler(event:MouseEvent):void
        {
            if (_currentPage > _minPage) {

                _currentPage--;
                _updateText();

                if (_rightButton.blocked)
                    _rightButton.enable();
                if (_rightEdgeArrow.blocked)
                    _rightEdgeArrow.enable();

                if (_currentPage == _minPage) {
                    _leftEdgeArrow.block();
                    _leftButton.block();
                }

                super.dispatchEvent(new Event(Event.CHANGE));
                super.sound.playUISound(UISound.CLICK);
            }
        }

        override protected function _rightClickHandler(event:MouseEvent):void
        {
            if (_currentPage < _totalPages) {

                _currentPage++;
                _updateText();

                if (_leftButton.blocked)
                    _leftButton.enable();
                if (_leftEdgeArrow.blocked)
                    _leftEdgeArrow.enable();

                if (_currentPage == _totalPages) {
                    _rightEdgeArrow.block();
                    _rightButton.block();
                }

                super.dispatchEvent(new Event(Event.CHANGE));
                super.sound.playUISound(UISound.CLICK);
            }
        }

        private function _rightEdgeClickHandler(event:MouseEvent):void
        {
            if (_currentPage < _totalPages) {

                _currentPage = _totalPages;
                _updateText();
                _rightEdgeArrow.block();
                _rightButton.block();

                if (_leftButton.blocked)
                    _leftButton.enable();
                if (_leftEdgeArrow.blocked)
                    _leftEdgeArrow.enable();

                super.dispatchEvent(new Event(Event.CHANGE));
                super.sound.playUISound(UISound.CLICK);
            }
        }

        private function _leftEdgeClickHandler(event:MouseEvent):void
        {
            if (_currentPage > _minPage) {

                _currentPage = _minPage;
                _updateText();
                _leftEdgeArrow.block();
                _leftButton.block();

                if (_rightButton.blocked)
                    _rightButton.enable();
                if (_rightEdgeArrow.blocked)
                    _rightEdgeArrow.enable();

                super.dispatchEvent(new Event(Event.CHANGE));
                super.sound.playUISound(UISound.CLICK);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get currentPage():int
        {
            return _currentPage;
        }

        public function get totalPages():int
        {
            return _totalPages;
        }

        public function set totalPages(value:int):void
        {
            _totalPages = MathHelper.clamp(value, 1, int.MAX_VALUE);
            _updateText();
            _checkCount();
        }

        public function set currentPage(value:int):void
        {
            _currentPage = value;
            _updateText();
            _checkCount();
        }

        override public function set count(value:int):void
        {
            throw new Error("Method deprecated!");
        }
    }
}
