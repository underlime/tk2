/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 11.03.14
 * Time: 11:47
 */
package application.views.screen.common.navigation
{
    import application.views.TextFactory;
    import application.views.screen.common.buttons.AcceptButton;

    public class TutorialCounter extends Navigation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TutorialCounter()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _addButtons():void
        {
            _leftButton = new AcceptButton(TextFactory.instance.getLabel('register_back_link'));
            _rightButton = new AcceptButton(TextFactory.instance.getLabel('register_next_link'));
            addChild(_leftButton);
            addChild(_rightButton);
        }

        override protected function render():void
        {
            super.render();
            _leftEdgeArrow.visible = false;
            _rightEdgeArrow.visible = false;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
