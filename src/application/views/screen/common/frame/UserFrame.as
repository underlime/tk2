/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 16:24
 */
package application.views.screen.common.frame
{
    import application.events.SocUserEvent;
    import application.models.ModelData;
    import application.models.ranks.RankData;
    import application.models.ranks.RanksModel;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.display.DisplayObject;
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.MouseEvent;
    import flash.events.SecurityErrorEvent;
    import flash.net.URLRequest;
    import flash.system.ApplicationDomain;
    import flash.system.LoaderContext;
    import flash.text.TextFieldAutoSize;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;

    public class UserFrame extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const FRAME:String = "USER_FRAME";
        public static const DEFAULT_ICON:String = "USER_DEFAULTH";
        private var _picture:String;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserFrame(soc_net_id:String, picture:String, level:int = 0, glory:int = 0)
        {
            super();
            _soc_net_id = soc_net_id;
            _picture = picture;
            _level = level;
            _glory = glory;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupFrame();
            _setupIcon();
            _setupLevel();

            if (_glory > 0)
                _addRank();

            _loadAvatar();
            _bindReactions();
        }


        override public function destroy():void
        {
            if (_avatarLoader) {
                _avatarLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, _onAvatarLoaded);
                _avatarLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
                _avatarLoader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);

                if (contains(_avatarLoader))
                    removeChild(_avatarLoader);

                _avatarLoader = null;
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _avatarLoader:Loader;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _frame:Bitmap;
        private var _icon:DisplayObject;
        private var _soc_net_id:String;
        private var _level:int = 0;
        private var _glory:int = 0;

        private var _txtLevel:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupFrame():void
        {
            _frame = super.source.getBitmap(FRAME);
            addChild(_frame);
        }

        private function _setupIcon():void
        {
            _icon = super.source.getBitmap(DEFAULT_ICON);
            _showIcon();
        }

        private function _showIcon():void
        {
            addChildAt(_icon, 0);

            _icon.x = _frame.width / 2 - _icon.width / 2;
            _icon.y = _frame.height / 2 - _icon.height / 2;
        }

        private function _setupLevel():void
        {
            _txtLevel = new ShadowAppText(0x000000);
            _txtLevel.autoSize = TextFieldAutoSize.RIGHT;
            _txtLevel.x = _frame.width - 10;
            _txtLevel.y = 43;
            addChild(_txtLevel);
            _txtLevel.text = _level.toString();
        }

        private function _addRank():void
        {
            var rankModel:RanksModel = ModelsRegistry.getModel(ModelData.RANKS) as RanksModel;
            var rankData:RankData = rankModel.getRankData(_glory);
            var icon:Bitmap = super.source.getBitmap(CommonData.ICON_PREFIX + rankData.picture);
            addChild(icon);
            icon.x = _frame.width - icon.width;
        }

        private function _loadAvatar():void
        {
            _avatarLoader = new Loader();
            _avatarLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, _onAvatarLoaded);
            _avatarLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
            _avatarLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);

            var request:URLRequest = new URLRequest(_picture);
            var context:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);
            _avatarLoader.load(request, context);
        }

        private function _onAvatarError(event:Event):void
        {
        }

        private function _onAvatarLoaded(event:Event):void
        {
            removeChild(_icon);
            _icon = _avatarLoader;
            _showIcon();
        }

        private function _bindReactions():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;

            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(e:MouseEvent):void
        {
            var event:SocUserEvent = new SocUserEvent(SocUserEvent.USER_INFO, true);
            event.soc_net_id = _soc_net_id;
            super.dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get soc_net_id():String
        {
            return _soc_net_id;
        }

        public function get level():int
        {
            return _level;
        }

        public function set level(value:int):void
        {
            _level = value;
            if (_txtLevel)
                _txtLevel.text = _level.toString();
        }
    }
}
