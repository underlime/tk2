/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.06.13
 * Time: 15:19
 */
package application.views.screen.common.state
{
    import application.common.ArrangementVariant;

    public class ScreenState
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ScreenState()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _currentPage:int = 1;
        private var _start:int = 0;
        private var _limit:int = 15;
        private var _tabID:int = 0;
        private var _totalPages:int = 1;
        private var _filter:ArrangementVariant;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set currentPage(value:int):void
        {
            _currentPage = value;
            _start = (_currentPage - 1) * _limit;
        }

        public function get currentPage():int
        {
            return _currentPage;
        }

        public function get start():int
        {
            return _start;
        }

        public function set start(value:int):void
        {
            _start = value;
        }

        public function get limit():int
        {
            return _limit;
        }

        public function set limit(value:int):void
        {
            _limit = value;
        }

        public function get tabID():int
        {
            return _tabID;
        }

        public function set tabID(value:int):void
        {
            _tabID = value;
        }

        public function get totalPages():int
        {
            return _totalPages;
        }

        public function set totalPages(value:int):void
        {
            _totalPages = value;
        }

        public function get filter():ArrangementVariant
        {
            return _filter;
        }

        public function set filter(value:ArrangementVariant):void
        {
            _filter = value;
        }
    }
}
