/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.08.13
 * Time: 16:40
 */
package application.views.screen.common
{
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class UserLevel extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserLevel(level:int)
        {
            super();
            _level = level;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupBackground();
            _addLabel();
        }

        override public function destroy():void
        {
            _txtLabel.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _level:int;
        private var _txtLabel:ShadowAppText;

        private var _background:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _background = super.source.getBitmap(CommonData.LEVEL_BIG_ICON);
            addChild(_background);
        }

        private function _addLabel():void
        {
            _txtLabel = new ShadowAppText(0x5f1511);
            _txtLabel.size = 18;
            _txtLabel.width = _background.width;
            _txtLabel.autoSize = TextFieldAutoSize.CENTER;
            _txtLabel.align = TextFormatAlign.CENTER;

            addChild(_txtLabel);
            _txtLabel.text = _level.toString();

            _txtLabel.y = _background.height / 2 - _txtLabel.textHeight / 2 - 5;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
