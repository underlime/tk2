/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.08.13
 * Time: 14:14
 */
package application.views.screen.common
{
    import application.models.ModelData;
    import application.models.ranks.RankData;
    import application.models.ranks.RanksModel;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;

    public class RankView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RankView(glory:int)
        {
            super();
            _glory = glory;
            super.$disableInteractive();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            var rankModel:RanksModel = ModelsRegistry.getModel(ModelData.RANKS) as RanksModel;
            var rankData:RankData = rankModel.getRankData(_glory);

            if (super.source.has(CommonData.ICON_PREFIX + rankData.picture)) {
                var icon:Bitmap = super.source.getBitmap(CommonData.ICON_PREFIX + rankData.picture);
                addChild(icon);
            }

            _txtRank = new AppTextField();
            _txtRank.color = AppTextField.DARK_BROWN;
            _txtRank.size = 10;
            _txtRank.bold = true;
            _txtRank.autoSize = TextFieldAutoSize.LEFT;
            _txtRank.x = 25;
            _txtRank.y = 2;

            addChild(_txtRank);
            _txtRank.text = rankData.name;
        }

        override public function destroy():void
        {
            _txtRank.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _glory:int;
        private var _txtRank:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
