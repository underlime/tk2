/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.10.13
 * Time: 16:27
 */
package application.views.screen.common
{
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.display.Shape;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class NotificationLabel extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BOX_LEFT:String = "INPUT_BOX_01";
        public static const BOX_CENTER:String = "INPUT_BOX_02";
        public static const BOX_RIGHT:String = "INPUT_BOX_03";

        public static const RUBBER_HEIGHT:int = 40;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function NotificationLabel(label:String)
        {
            super();
            _label = label;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _rectangle = new Rectangle(0, 0, _notificationWidth, RUBBER_HEIGHT);
            _drawBackground();
            _addLabel();

            this.scrollRect = _rectangle;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _fontSize:int = 16;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _label:String;
        private var _notificationWidth:int = 100;

        private var _txtLabel:ShadowAppText;

        private var _leftCorner:Bitmap;
        private var _centerShape:Shape;
        private var _rightCorner:Bitmap;

        private var _centerFill:Bitmap;

        private var _container:View;
        private var _rectangle:Rectangle;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _drawBackground():void
        {
            _container = new View();
            addChild(_container);

            _leftCorner = super.source.getBitmap(BOX_LEFT);
            _container.addChild(_leftCorner);

            _rightCorner = super.source.getBitmap(BOX_RIGHT);
            _container.addChild(_rightCorner);

            _centerFill = super.source.getBitmap(BOX_CENTER);

            _centerShape = new Shape();
            _container.addChild(_centerShape);

            var currentWidth:Number = _notificationWidth - _leftCorner.width - _rightCorner.width;
            _centerShape.x = _leftCorner.width;

            _centerShape.graphics.beginBitmapFill(_centerFill.bitmapData);
            _centerShape.graphics.drawRect(0, 0, currentWidth, _centerFill.height);
            _centerShape.graphics.endFill();

            _rightCorner.x = _leftCorner.width + currentWidth;
        }

        private function _addLabel():void
        {
            _txtLabel = new ShadowAppText(0x5a291c);
            _txtLabel.size = _fontSize;
            _txtLabel.autoSize = TextFieldAutoSize.NONE;
            _txtLabel.align = TextFormatAlign.CENTER;
            _txtLabel.width = _notificationWidth;

            addChild(_txtLabel);
            _txtLabel.text = _label;

            _txtLabel.x = 0;
            _txtLabel.y = RUBBER_HEIGHT / 2 - _txtLabel.textHeight / 2 - 3;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get notificationWidth():int
        {
            return _notificationWidth;
        }

        public function set notificationWidth(value:int):void
        {
            _notificationWidth = value;
        }

        public function get label():String
        {
            return _label;
        }

        public function set label(value:String):void
        {
            _label = value;
            if (_txtLabel) {
                _txtLabel.text = _label;
            }
        }


    }
}
