/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 11.09.13
 * Time: 11:25
 */
package application.views.screen.common.checkbox
{
    import application.views.text.AppTextField;

    import flash.text.TextFieldAutoSize;

    import framework.core.display.components.CheckBox;

    public class AppCheckBox extends CheckBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "CHECK_BOXES";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AppCheckBox(label:String)
        {
            super(SOURCE, label);
            _outPosition = 0;
            _overPosition = 28;

            _selectedPosition = 56;
            _selectedOverPosition = 84;

            _w = 28;
            _h = 28;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _addLabelField():void
        {
            _txtLabel = new AppTextField();
            _txtLabel.size = 12;
            _txtLabel.color = AppTextField.DARK_BROWN;
            _txtLabel.autoSize = TextFieldAutoSize.LEFT;
            _txtLabel.bold = true;

            addChild(_txtLabel);
            _txtLabel.text = _label;

            _txtLabel.x = _w + 2;
            _txtLabel.y = _h/2 - _txtLabel.textHeight/2 - 2;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
