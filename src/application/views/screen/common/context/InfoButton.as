/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 15:48
 */
package application.views.screen.common.context
{
    import application.views.TextFactory;
    import application.views.screen.inventory.context.buttons.ContextButton;

    public class InfoButton extends ContextButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InfoButton()
        {
            super(TextFactory.instance.getLabel("info_context_label"), 160);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
