/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 15:49
 */
package application.views.screen.common.context
{
    import application.views.screen.inventory.context.ContextMenu;

    public class CommonContext extends ContextMenu
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CommonContext(fightMethod:Function, infoMethod:Function)
        {
            super();

            register(new FightButton(), fightMethod);
            register(new InfoButton(), infoMethod);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
