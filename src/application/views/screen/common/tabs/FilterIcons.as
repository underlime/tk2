/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 24.08.13
 * Time: 20:39
 */
package application.views.screen.common.tabs
{
    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.tools.SourceManager;

    public class FilterIcons
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const SOURCE:String = "FILTER_ICONS";
        public static const ICON_WIDTH:int = 32;
        public static const ICON_HEIGHT:int = 32;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static function getIcon(filterType:FilterType):Bitmap
        {
            var icon:Bitmap = SourceManager.instance.getBitmap(SOURCE);
            var offset:Number = _getOffset(filterType);
            icon.scrollRect = new Rectangle(0, offset, ICON_WIDTH, ICON_HEIGHT);

            return icon;
        }

        private static function _getOffset(filterType:FilterType):Number
        {
            var offset:Number = 0;
            var hash:Object = {};

            hash[FilterType.ALL.toString()] = 0;
            hash[FilterType.GLOVES.toString()] = 32;
            hash[FilterType.ONE_HANDED.toString()] = 64;
            hash[FilterType.ROD_WEAPON.toString()] = 96;
            hash[FilterType.TWO_HANDED.toString()] = 128;
            hash[FilterType.ARMOR.toString()] = 160;
            hash[FilterType.SHIELD.toString()] = 192;
            hash[FilterType.GLASSES.toString()] = 224;
            hash[FilterType.MASK.toString()] = 256;
            hash[FilterType.RINGS.toString()] = 288;
            hash[FilterType.AMULET.toString()] = 320;
            hash[FilterType.MED.toString()] = 352;
            hash[FilterType.ENERGY.toString()] = 384;
            hash[FilterType.GRENADE.toString()] = 416;
            hash[FilterType.HAT.toString()] = 448;
            hash[FilterType.COAT.toString()] = 480;
            hash[FilterType.GUN.toString()] = 512;
            hash[FilterType.EYES.toString()] = 544;
            hash[FilterType.MOUTH.toString()] = 576;
            hash[FilterType.HAIR.toString()] = 608;
            hash[FilterType.BODY.toString()] = 640;

            if (hash[filterType.toString()])
                offset = hash[filterType.toString()];

            return offset;
        }

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
