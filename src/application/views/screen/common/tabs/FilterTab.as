/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 02.09.13
 * Time: 19:20
 */
package application.views.screen.common.tabs
{
    import application.common.ArrangementVariant;
    import application.events.ApplicationEvent;
    import application.sound.SoundManager;
    import application.sound.lib.UISound;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    import framework.core.display.buttons.SpriteButton;
    import framework.core.utils.Singleton;

    public class FilterTab extends SpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "FILTER_SMALL_TAB";

        public static const FILTER_WIDTH:int = 32;
        public static const FILTER_HEIGHT:int = 38;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FilterTab(filterType:FilterType)
        {
            super(SOURCE);
            _filterType = filterType;
            _w = 32;
            _h = 38;

            _outPosition = 0;
            _downPosition = 76;
            _selectedPosition = 76;
            _overPosition = 38;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addIcon();
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            super.destroy();
        }

        override public function select():void
        {
            super.select();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TAB_CHOOSE, true));
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _arrangement_variant:ArrangementVariant;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _filterType:FilterType;
        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addIcon():void
        {
            var icon:Bitmap = FilterIcons.getIcon(_filterType);
            addChild(icon);
            icon.x = 3;
            icon.y = FILTER_HEIGHT / 2 - FilterIcons.ICON_HEIGHT / 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            select();
            _sound.playUISound(UISound.TAB_CHANGE);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get arrangement_variant():ArrangementVariant
        {
            return _arrangement_variant;
        }
    }
}
