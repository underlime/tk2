/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 24.08.13
 * Time: 20:42
 */
package application.views.screen.common.tabs
{
    import framework.core.enum.BaseEnum;

    public class FilterType extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ALL:FilterType = new FilterType("all");
        public static const GLOVES:FilterType = new FilterType("gloves");
        public static const ONE_HANDED:FilterType = new FilterType("one_hand");
        public static const ROD_WEAPON:FilterType = new FilterType("rod_weapon");
        public static const TWO_HANDED:FilterType = new FilterType("two_handed");
        public static const ARMOR:FilterType = new FilterType("armor");
        public static const SHIELD:FilterType = new FilterType("shield");
        public static const GLASSES:FilterType = new FilterType("glasses");
        public static const MASK:FilterType = new FilterType("mask");
        public static const RINGS:FilterType = new FilterType("rings");
        public static const AMULET:FilterType = new FilterType("amulet");
        public static const MED:FilterType = new FilterType("med");
        public static const ENERGY:FilterType = new FilterType("energy");
        public static const GRENADE:FilterType = new FilterType("grenade");
        public static const HAT:FilterType = new FilterType("hat");
        public static const COAT:FilterType = new FilterType("coat");

        public static const GUN:FilterType = new FilterType("gun");
        public static const EYES:FilterType = new FilterType("eyes");
        public static const MOUTH:FilterType = new FilterType("mouth");
        public static const HAIR:FilterType = new FilterType("hair");
        public static const BODY:FilterType = new FilterType("body");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FilterType(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.views.screen.common.tabs.FilterType;

FilterType.lockUp();