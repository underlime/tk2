/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.08.13
 * Time: 15:28
 */
package application.views.screen.common.tabs
{
    import application.models.common.TabHintsModel;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.display.tabs.ITab;
    import framework.core.tools.SourceManager;

    public class TabFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TABS:String = "TABS_ICO";
        public static const ICO_WIDTH:int = 50;
        public static const ICO_HEIGHT:int = 50;
        public static const EXCEPTIONS:Array = [TabType.FIND_OPPONENTS, TabType.FORGE];

        private static var _hints:TabHintsModel = new TabHintsModel();

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getTab(tabType:TabType):ITab
        {
            var offset:int = _getTabOffset(tabType);
            var icon:Bitmap = SourceManager.instance.getBitmap(TABS);
            icon.scrollRect = new Rectangle(0, offset, ICO_WIDTH, ICO_HEIGHT);

            var spriteTab:SpriteTab = new SpriteTab(icon);
            var hint:Object = _hints.getHint(tabType.toString());
            if (hint['title'] && hint['description'] && EXCEPTIONS.indexOf(tabType) == -1) {
                spriteTab.hint = new TabsHint(hint['title'], hint['description']);
            }
            return spriteTab;
        }

        private static function _getTabOffset(tabType:TabType):int
        {
            var offset:int = 0;

            var _hash:Object = _getHash();
            if (_hash[tabType.toString()])
                offset = _hash[tabType.toString()];

            return offset;
        }

        private static function _getHash():Object
        {
            var hash:Object = {};

            hash[TabType.WEAPON_TAB.toString()] = 0;
            hash[TabType.HAT_TAB.toString()] = 50;
            hash[TabType.ARMOR_TAB.toString()] = 100;
            hash[TabType.COAT_TAB.toString()] = 150;
            hash[TabType.MASK_TAB.toString()] = 200;
            hash[TabType.RING_TAB.toString()] = 250;
            hash[TabType.DRINK_TAB.toString()] = 300;

            hash[TabType.INVENTORY_TAB.toString()] = 350;
            hash[TabType.USER_INFO_TAB.toString()] = 400;
            hash[TabType.ACHIEVEMENTS_TAB.toString()] = 450;
            hash[TabType.SPECIALS_TAB.toString()] = 500;
            hash[TabType.REPAIR_TAB.toString()] = 550;

            hash[TabType.TRAIN_TAB.toString()] = 600;
            hash[TabType.LABORATORY_TAB.toString()] = 650;
            hash[TabType.CHANGE_NAME_TAB.toString()] = 700;
            hash[TabType.CHANGE_VEGETABLE_TAB.toString()] = 750;

            hash[TabType.FIND_OPPONENTS.toString()] = 800;
            hash[TabType.TOURNAMENT.toString()] = 850;
            hash[TabType.FORGE.toString()] = 900;
            hash[TabType.MEDICINE.toString()] = 950;
            hash[TabType.SWORDS.toString()] = 1000;

            hash[TabType.MY_CLAN.toString()] = 1050;
            hash[TabType.EDIT_CLAN.toString()] = 1100;
            hash[TabType.CLAN_LOG.toString()] = 1150;
            hash[TabType.SHOW_CLAN.toString()] = 1200;

            return hash;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
