/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.08.13
 * Time: 15:25
 */
package application.views.screen.common.tabs
{

    import framework.core.enum.BaseEnum;

    public class TabType extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const WEAPON_TAB:TabType = new TabType("weapon");
        public static const HAT_TAB:TabType = new TabType("hat");
        public static const ARMOR_TAB:TabType = new TabType("armor");
        public static const COAT_TAB:TabType = new TabType("coat");
        public static const MASK_TAB:TabType = new TabType("mask");
        public static const RING_TAB:TabType = new TabType("ring");
        public static const DRINK_TAB:TabType = new TabType("drink");

        public static const INVENTORY_TAB:TabType = new TabType("inventory");
        public static const USER_INFO_TAB:TabType = new TabType("user_info");
        public static const SPECIALS_TAB:TabType = new TabType("specials");
        public static const ACHIEVEMENTS_TAB:TabType = new TabType("achievements");
        public static const REPAIR_TAB:TabType = new TabType("repair");

        public static const TRAIN_TAB:TabType = new TabType("train");
        public static const LABORATORY_TAB:TabType = new TabType("laboratory");
        public static const CHANGE_NAME_TAB:TabType = new TabType("change_name");
        public static const CHANGE_VEGETABLE_TAB:TabType = new TabType("change_vegetable");

        public static const FIND_OPPONENTS:TabType = new TabType("find_opponents");
        public static const FORGE:TabType = new TabType("forge");
        public static const MEDICINE:TabType = new TabType("medicine");
        public static const TOURNAMENT:TabType = new TabType("tournament");
        public static const SWORDS:TabType = new TabType("swords");

        public static const MY_CLAN:TabType = new TabType("my_clan");
        public static const EDIT_CLAN:TabType = new TabType("edit_clan");
        public static const CLAN_LOG:TabType = new TabType("log_clan");
        public static const SHOW_CLAN:TabType = new TabType("show_clan");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TabType(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.views.screen.common.tabs.TabType;

TabType.lockUp();