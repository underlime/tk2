/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.09.13
 * Time: 11:33
 */
package application.views.screen.common.tabs
{
    import framework.core.display.tabs.ITab;
    import framework.core.display.tabs.RelativeTabPanel;

    public class AppTabs extends RelativeTabPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AppTabs(tabs:Vector.<ITab>)
        {
            super(tabs);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
