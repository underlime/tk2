/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.08.13
 * Time: 15:36
 */
package application.views.screen.common.tabs
{
    import application.events.ApplicationEvent;
    import application.sound.SoundManager;
    import application.sound.lib.UISound;
    import application.views.PopupManager;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Point;

    import framework.core.display.buttons.SpriteButton;
    import framework.core.utils.Singleton;

    public class SpriteTab extends SpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static var SOURCE:String = "TAB_BIG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpriteTab(icon:Bitmap)
        {
            super(SOURCE);

            _icon = icon;

            _w = 62;
            _h = 58;

            _overPosition = 58;
            _downPosition = 116;
            _blockPosition = 116;
            _selectedPosition = 116;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addIcon();
            addEventListener(MouseEvent.CLICK, _clickHandler);
            if (_hint) {
                _addHintView();
            }
        }

        override public function select():void
        {
            super.select();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TAB_CHOOSE, true));
        }

        private function _clickHandler(event:MouseEvent):void
        {
            select();
            _sound.playUISound(UISound.TAB_CHANGE);
            if (_hint) {
                popupManager.remove();
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;
        private var _sound:SoundManager = Singleton.getClass(SoundManager);
        private var _hint:TabsHint;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addIcon():void
        {
            addChild(_icon);
            _icon.x = 6;
            _icon.y = 6;
        }

        private function _addHintView():void
        {
            addChild(_hint);
            var cords:Point = localToGlobal(new Point(-50, -1 * _hint.h - 10));
            removeChild(_hint);
            _hint.x = cords.x;
            _hint.y = int(cords.y);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _onOverHandler(e:MouseEvent):void
        {
            super._onOverHandler(e);
            if (_hint) {
                popupManager.add(_hint);
            }
        }

        override protected function _onOutHandler(e:MouseEvent):void
        {
            super._onOutHandler(e);
            if (_hint) {
                popupManager.remove();
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get hint():TabsHint
        {
            return _hint;
        }

        public function set hint(value:TabsHint):void
        {
            _hint = value;
        }

        public function get popupManager():PopupManager
        {
            return Singleton.getClass(PopupManager);
        }
    }
}
