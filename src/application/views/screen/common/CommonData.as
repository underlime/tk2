/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.06.13
 * Time: 16:16
 */
package application.views.screen.common
{
    public class CommonData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ICON_PREFIX:String = "i";
        public static const FERROS_ICO:String = "MAP_iFerrosIco";
        public static const TOMATOS_ICO:String = "MAP_iTomatosIco";

        public static const EXP_ICON:String = "battleResult_EXP_ico";
        public static const GLORY_ICON:String = "battleResult_glory_ico";
        public static const TOMATOS_RES_ICON:String = "battleResult_tomato_ico";
        public static const FERROS_RES_ICON:String = "battleResult_feros_ico";

        public static const AGILITY_BIG_ICON:String = "AGILITY_ICO_BIG";
        public static const GLORY_BIG_ICON:String = "GLORY_ICO";
        public static const HEART_BIG_ICON:String = "HEART_ICO_BIG";
        public static const LEVEL_BIG_ICON:String = "LEVEL_COUNT";

        public static const STRENGTH_BIG_ICON:String = "STRENGTH_ICO_BIG";
        public static const INTELLECT_BIG_ICON:String = "WISDOM_ICO_BIG";
        public static const FERROS_SMALL_ICON_BAR:String = "FERROS_ICO_SMALL_BAR";
        public static const FERROS_SMALL_ICON_BUTTON:String = "FERROS_ICO_SMALL_BUTTON";
        public static const TOMATOS_SMALL_ICON_BUTTON:String = "TOMATOS_ICO_SMALL";

        public static const TOMATOS_SMALL_ICON_BAR:String = "TOMATOS_ICO_SMALL_BAR";
        public static const LEARNING_POINTS_BG:String = "LEARNING_POINTS_COUNT";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
