/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 10:46
 */
package application.views.screen.common.window
{
    import application.config.AppConfig;
    import application.views.screen.common.buttons.CloseButton;
    import application.views.screen.common.window.text.PublicWindowCaption;

    import flash.display.Bitmap;
    import flash.display.Graphics;

    import framework.core.struct.view.View;

    public class PublicWindow extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static const BACKGROUND:String = "COMMON_WINDOW_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PublicWindow(caption:String)
        {
            super();
            _caption = caption;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _drawBackground();
            _addLabel();
            _addCloseButton();
        }

        override public function destroy():void
        {
            _txtCaption.destroy();
            graphics.clear();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _closeButton:CloseButton;
        private var _caption:String;

        private var _txtCaption:PublicWindowCaption;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _drawBackground():void
        {
            var gr:Graphics = this.graphics;
            gr.beginFill(0x000000, .8);
            gr.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            gr.endFill();

            _background = super.source.getBitmap(BACKGROUND);
            _background.x = AppConfig.APP_WIDTH / 2 - _background.width / 2;
            _background.y = AppConfig.APP_HEIGHT / 2 - _background.height / 2;

            addChild(_background);
        }

        private function _addCloseButton():void
        {
            _closeButton = new CloseButton();
            addChild(_closeButton);
            _closeButton.x = _background.width - _closeButton.width;
        }

        private function _addLabel():void
        {
            _txtCaption = new PublicWindowCaption();
            _txtCaption.x = 252;
            _txtCaption.y = 26;

            addChild(_txtCaption);
            _txtCaption.text = _caption;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
