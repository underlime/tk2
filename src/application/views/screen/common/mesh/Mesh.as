/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 03.06.13
 * Time: 15:55
 */
package application.views.screen.common.mesh
{
    import application.views.AppView;
    import application.views.screen.common.cell.Cell;

    import flash.events.Event;

    public class Mesh extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Mesh(cells:Vector.<Cell> = null)
        {
            _cells = cells;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            var count:int = _cells.length;
            for (var i:int = 0; i < count; i++)
                if (_cells[i] && !_cells[i].destroyed)
                    _cells[i].destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function draw():void
        {
            if (_cells)
                _drawMesh();
        }

        public function clear():void
        {
            if (!_cells)
                return;
            var count:int = _cells.length;
            for (var i:int = 0; i < count; i++)
                if (_cells[i] && this.contains(_cells[i]))
                    removeChild(_cells[i]);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _x0:Number = 65;
        protected var _y0:Number = 180;

        protected var _dx:Number = 87;
        protected var _dy:Number = 77;

        protected var _cols:int = 5;
        protected var _lines:int = 3;

        protected var _limit:int = 15;
        protected var _selectedCell:Cell;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _drawMesh():void
        {
            var cx:Number = _x0;
            var cy:Number = _y0;
            var colsCount:int = 0;
            var linesCount:int = 1;
            var rendered:int = 0;

            var count:int = _cells.length;
            var target:int = _limit <= count ? _limit : count;

            for (var i:int = 0; i < target; i++) {
                _cells[i].addEventListener(Event.SELECT, _onSelectCell);
                _cells[i].x = cx;
                _cells[i].y = cy;
                addChild(_cells[i]);

                colsCount++;
                rendered++;
                cx += _dx;

                if (colsCount >= _cols) {
                    colsCount = 0;
                    cx = _x0;
                    cy += _dy;
                    linesCount++;
                }
            }
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _cells:Vector.<Cell>;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _destroyCells():void
        {
            if (!_cells)
                return;
            var count:int = _cells.length;
            for (var i:int = 0; i < count; i++)
                if (_cells[i] && !_cells[i].destroyed)
                    _cells[i].destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onSelectCell(event:Event):void
        {
            var cell:Cell = event.currentTarget as Cell;
            if (_selectedCell != cell) {
                if (_selectedCell)
                    _selectedCell.clearSelection();

                _selectedCell = cell;
                _selectedCell.select();

                super.dispatchEvent(new Event(Event.SELECT));
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set cells(value:Vector.<Cell>):void
        {
            clear();
            _cells = value;
        }

        public function get cells():Vector.<Cell>
        {
            return _cells;
        }

        public function get selectedCell():Cell
        {
            return _selectedCell;
        }
    }
}
