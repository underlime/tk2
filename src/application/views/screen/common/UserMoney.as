/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.06.13
 * Time: 10:08
 */
package application.views.screen.common
{
    import application.models.ModelData;
    import application.models.user.User;
    import application.models.user.UserFightData;
    import application.views.text.MarketMoneyLabel;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;

    public class UserMoney extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserMoney()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupIcons();
            _setupText();
            _setupModel();
        }

        override public function destroy():void
        {
            _model.removeEventListener(Event.CHANGE, _changeHandler);
            _txtFerros.destroy();
            _txtTomatos.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtFerros:MarketMoneyLabel;
        private var _txtTomatos:MarketMoneyLabel;

        private var _model:UserFightData;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupIcons():void
        {
            var ferrosIco:Bitmap = super.source.getBitmap(CommonData.FERROS_ICO);
            var tomatosIco:Bitmap = super.source.getBitmap(CommonData.TOMATOS_ICO);

            addChild(ferrosIco);
            addChild(tomatosIco);

            ferrosIco.x = 132;
        }

        private function _setupText():void
        {
            _txtFerros = new MarketMoneyLabel();
            _txtTomatos = new MarketMoneyLabel();

            _txtTomatos.x = 31;
            _txtTomatos.y = 1;

            _txtFerros.x = 161;
            _txtFerros.y = 1;

            addChild(_txtFerros);
            addChild(_txtTomatos);
        }

        private function _setupModel():void
        {
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;
            _model = user.getChildByName(ModelData.USER_FIGHT_DATA) as UserFightData;
            _model.addEventListener(Event.CHANGE, _changeHandler);

            _txtFerros.text = _model.ferros.value.toString();
            _txtTomatos.text = _model.tomatos.value.toString();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _txtFerros.text = _model.ferros.value.toString();
            _txtTomatos.text = _model.tomatos.value.toString();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
