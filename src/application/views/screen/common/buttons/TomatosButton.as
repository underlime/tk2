/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.08.13
 * Time: 15:29
 */
package application.views.screen.common.buttons
{
    import application.views.TextFactory;
    import application.views.screen.common.CommonData;

    import flash.display.Bitmap;

    public class TomatosButton extends AcceptButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TomatosButton()
        {
            super(TextFactory.instance.getLabel('buy_label'));
            _w = 125;
            _fontSize = 14;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addFerrosIcon();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addFerrosIcon():void
        {
            var icon:Bitmap = super.source.getBitmap(CommonData.TOMATOS_SMALL_ICON_BUTTON);
            addChild(icon);
            icon.x = 10;
            icon.y = _h / 2 - icon.height / 2;
            _txtLabel.x += icon.x;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
