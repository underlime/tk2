/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 11.08.13
 * Time: 12:46
 */
package application.views.screen.common.buttons
{
    public class ButtonsData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SPECIAL_BUTTON_LEFT:String = "SPECIAL_BTN_01";
        public static const SPECIAL_BUTTON_CENTER:String = "SPECIAL_BTN_02";
        public static const SPECIAL_BUTTON_RIGHT:String = "SPECIAL_BTN_03";

        public static const REJECT_BUTTON_LEFT:String = "REJECT_BTN_01";
        public static const REJECT_BUTTON_CENTER:String = "REJECT_BTN_02";
        public static const REJECT_BUTTON_RIGHT:String = "REJECT_BTN_03";

        public static const ACCEPT_BUTTON_LEFT:String = "ACCEPT_BTN_01";
        public static const ACCEPT_BUTTON_CENTER:String = "ACCEPT_BTN_02";
        public static const ACCEPT_BUTTON_RIGHT:String = "ACCEPT_BTN_03";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
