/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.09.13
 * Time: 21:01
 */
package application.views.screen.common.buttons
{
    import application.sound.SoundManager;
    import application.sound.lib.UISound;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    import framework.core.display.buttons.RubberButton;
    import framework.core.utils.Singleton;

    public class AppRubberButton extends RubberButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AppRubberButton(label:String, left:Bitmap, right:Bitmap, center:Bitmap)
        {
            super(label, left, right, center);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _onOverHandler(e:MouseEvent):void
        {
            super._onOverHandler(e);
            _sound.playUISound(UISound.MOUSE_OVER);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
