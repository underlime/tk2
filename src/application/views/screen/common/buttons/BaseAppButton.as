/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.10.13
 * Time: 14:47
 */
package application.views.screen.common.buttons
{
    import flash.display.Bitmap;

    public class BaseAppButton extends AppRubberButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseAppButton(label:String, left:Bitmap, center:Bitmap, right:Bitmap)
        {
            super(label, left, center, right);

            _h = 40;
            _overPosition = 40;
            _outPosition = 0;
            _selectedPosition = 80;
            _downPosition = 80;
            _blockPosition = 120;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _fontSize:int = 14;

        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _updateLabelPosition():void
        {
            _txtLabel.x = _w / 2 - _txtLabel.textWidth / 2 - 4;
            _txtLabel.y = _h / 2 - _txtLabel.height / 2 - 2;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get fontSize():int
        {
            return _fontSize;
        }

        public function set fontSize(value:int):void
        {
            _fontSize = value;
        }
    }
}
