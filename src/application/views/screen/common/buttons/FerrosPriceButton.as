/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.08.13
 * Time: 15:43
 */
package application.views.screen.common.buttons
{
    import application.views.screen.common.CommonData;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.filters.GlowFilter;
    import flash.text.TextFieldAutoSize;

    public class FerrosPriceButton extends SpecialButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FerrosPriceButton(label:String, price:int)
        {
            super(label);
            _price = price;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            super.setup();
            _icon = super.source.getBitmap(CommonData.FERROS_SMALL_ICON_BUTTON);
        }

        override protected function render():void
        {
            super.render();
            _addPrice();
            _updatePositions();
        }

        override public function destroy():void
        {
            _txtPrice.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _iconMargin:int = 8;
        protected var _txtPrice:AppTextField;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;

        private var _price:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addPrice():void
        {
            _addPriceField();
            _addFerrosIcon();
        }

        private function _addPriceField():void
        {
            _txtPrice = new AppTextField();
            _txtPrice.autoSize = TextFieldAutoSize.LEFT;
            _txtPrice.color = 0xffffff;
            _txtPrice.size = _fontSize;
            _txtPrice.bold = true;
            addChild(_txtPrice);
            _txtPrice.filters = [new GlowFilter(0x1f5676, 1, 5, 5, 5)];

            _txtPrice.y = _txtLabel.y;
            _txtPrice.x = _txtLabel.x + _txtLabel.textWidth + _iconMargin;

            _txtPrice.text = _price.toString();
        }

        private function _addFerrosIcon():void
        {
            addChild(_icon);
            _icon.x = _txtPrice.x + _txtPrice.textWidth + 5;
            _icon.y = _h / 2 - _icon.height / 2;
        }

        private function _updatePositions():void
        {
            var total:Number = _txtLabel.textWidth + _iconMargin + _txtPrice.textWidth + _icon.width + 3;
            _txtLabel.x = _w / 2 - total / 2;
            _txtPrice.x = _txtLabel.x + _txtLabel.textWidth + _iconMargin;
            _icon.x = _txtPrice.x + _txtPrice.width + 3;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
