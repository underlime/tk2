/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 14.10.13
 * Time: 21:01
 */
package application.views.screen.common.buttons
{
    import application.views.map.layers.common.NotificationCount;

    public class CounterButton extends SpecialButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CounterButton(label:String, count:int = 0)
        {
            super(label);
            _count = count;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addCounter();
        }

        override public function destroy():void
        {
            _destroyCounter();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _counter:NotificationCount;
        private var _count:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addCounter():void
        {
            _destroyCounter();
            _counter = new NotificationCount(_count.toString());
            _counter.notificationWidth = 41;
            if (_count > 0) {
                addChild(_counter);
                _counter.enable();
                _counter.x = super.w - _counter.notificationWidth - 7;
                _counter.y = 7;
                _updateLabel();
            }
        }

        private function _updateLabel():void
        {
            _txtLabel.x -= _counter.notificationWidth / 3;
        }

        private function _destroyCounter():void
        {
            if (_counter && !_counter.destroyed)
                _counter.destroy();
            _counter = null;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get count():int
        {
            return _count;
        }

        public function set count(value:int):void
        {
            _count = value;
            _addCounter();
        }
    }
}
