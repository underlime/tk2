/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 11.08.13
 * Time: 13:10
 */
package application.views.screen.common.buttons
{
    import flash.display.Bitmap;
    import flash.filters.GlowFilter;

    import framework.core.tools.SourceManager;

    public class RejectButton extends BaseAppButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RejectButton(label:String)
        {
            var leftCorner:Bitmap = SourceManager.instance.getBitmap(ButtonsData.REJECT_BUTTON_LEFT);
            var rightCorner:Bitmap = SourceManager.instance.getBitmap(ButtonsData.REJECT_BUTTON_RIGHT);
            var centerPart:Bitmap = SourceManager.instance.getBitmap(ButtonsData.REJECT_BUTTON_CENTER);

            super(label, leftCorner, rightCorner, centerPart);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addStyle():void
        {
            _txtLabel.color = 0xffffff;
            _txtLabel.size = _fontSize;
            _txtLabel.bold = true;
            super.textFilters = [new GlowFilter(0x77291f, 1, 5, 5, 5)];
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
