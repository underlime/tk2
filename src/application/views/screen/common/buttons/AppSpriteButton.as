/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.09.13
 * Time: 21:03
 */
package application.views.screen.common.buttons
{
    import application.sound.SoundManager;
    import application.sound.lib.UISound;

    import flash.events.MouseEvent;

    import framework.core.display.buttons.SpriteButton;
    import framework.core.utils.Singleton;

    public class AppSpriteButton extends SpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AppSpriteButton(source:String)
        {
            super(source);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _onOverHandler(e:MouseEvent):void
        {
            super._onOverHandler(e);
            _sound.playUISound(UISound.MOUSE_OVER);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get sound():SoundManager
        {
            return _sound;
        }
    }
}
