/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.07.13
 * Time: 14:25
 */
package application.views.screen.common.buttons
{
    import application.events.ApplicationEvent;
    import application.sound.SoundManager;
    import application.sound.lib.UISound;

    import flash.events.MouseEvent;

    import framework.core.utils.Singleton;

    public class CloseButton extends AppSpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const CLOSE_BUTTON:String = "CLOSE_BTN";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CloseButton()
        {
            super(CLOSE_BUTTON);
            _w = 50;
            _h = 50;
            _overPosition = 50;
            _downPosition = 100;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _unbindReactions():void
        {
            super._unbindReactions();
            removeEventListener(MouseEvent.CLICK, _clickHandler);
        }

        override protected function _bindReactions():void
        {
            super._bindReactions();
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CLOSE, true));
            _sound.playUISound(UISound.CLOSE);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
