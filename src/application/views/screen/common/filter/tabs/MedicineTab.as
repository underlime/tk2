/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 02.09.13
 * Time: 20:40
 */
package application.views.screen.common.filter.tabs
{
    import application.common.ArrangementVariant;
    import application.views.screen.common.tabs.FilterTab;
    import application.views.screen.common.tabs.FilterType;

    public class MedicineTab extends FilterTab
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MedicineTab()
        {
            super(FilterType.MED);
            _arrangement_variant = ArrangementVariant.DRINK;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
