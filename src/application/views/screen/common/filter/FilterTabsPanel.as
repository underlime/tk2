/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 02.09.13
 * Time: 20:22
 */
package application.views.screen.common.filter
{
    import application.common.ArrangementVariant;
    import application.events.ApplicationEvent;
    import application.views.screen.common.tabs.AppTabs;
    import application.views.screen.common.tabs.FilterTab;

    import flash.display.DisplayObject;

    import framework.core.display.tabs.ITab;

    public class FilterTabsPanel extends AppTabs
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FilterTabsPanel(filters:Vector.<ITab>)
        {
            super(filters);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _setTabs():void
        {
            var count:int = _tabs.length;
            var vMargin:int = 0;
            var hMargin:int = 0;

            for (var i:int = 0; i < count; i++) {
                var tab:DisplayObject = _tabs[i] as DisplayObject;
                addChild(tab);

                tab.x = hMargin;
                tab.y = vMargin;

                if (_dx > 0)
                    hMargin += FilterTab.FILTER_WIDTH + _dx;

                if (_dy > 0)
                    vMargin += FilterTab.FILTER_HEIGHT + _dy;

                _tabs[i].addEventListener(ApplicationEvent.TAB_CHOOSE, _tabChooseHandler);
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get arrangement_variant():ArrangementVariant
        {
            if (_tabs[selectedTab] is FilterTab) {
                return (_tabs[selectedTab] as FilterTab).arrangement_variant;
            }
            return null;
        }

    }
}
