/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 17:30
 */
package application.views.screen.common.text
{
    import application.views.text.AppTextField;

    import flash.filters.DropShadowFilter;
    import flash.text.TextFieldAutoSize;

    public class ShadowAppText extends AppTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShadowAppText(shadowColor:uint, settings:Object = null)
        {
            super(settings);
            _shadowColor = shadowColor;
            _setup();
            _setupShadow();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _shadowColor:uint;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void
        {
            this.color = 0xffffff;
            this.size = 16;
            this.bold = true;
            this.autoSize = TextFieldAutoSize.LEFT;
        }

        private function _setupShadow():void
        {
            this.filters = [new DropShadowFilter(1, 45, _shadowColor, 1, 4, 4, 5)];
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
