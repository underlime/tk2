/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.03.14
 * Time: 11:19
 */
package application.views.screen.common.scroll
{
    import application.events.JsScrollEvent;
    import application.helpers.ScrollBroadcaster;

    import flash.geom.Rectangle;

    import framework.core.display.scroll.Scroll;
    import framework.core.display.scroll.ScrollContentObserver;
    import framework.core.struct.view.View;
    import framework.core.utils.Singleton;

    public class ScrollObserver extends ScrollContentObserver
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ScrollObserver(scrollContent:View, scroll:Scroll, scrollRect:Rectangle, customHeight:int = 0)
        {
            super(scrollContent, scroll, scrollRect, customHeight);
            _scrollBroadcaster.addEventListener(JsScrollEvent.SCROLL, _jsScrollHandler);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            _scrollBroadcaster.removeEventListener(JsScrollEvent.SCROLL, _jsScrollHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _scrollBroadcaster:ScrollBroadcaster = Singleton.getClass(ScrollBroadcaster);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _jsScrollHandler(event:JsScrollEvent):void
        {
            super._changeByWheel(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
