/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.07.13
 * Time: 15:45
 */
package application.views.screen.common.scroll
{
    import framework.core.display.buttons.SpriteButton;

    public class ScrollMarker extends SpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const MARKER:String = "SCROLLER";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ScrollMarker()
        {
            super(MARKER);
            _w = 32;
            _h = 32;
            _overPosition = 32;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
