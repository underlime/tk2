/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.07.13
 * Time: 16:09
 */
package application.views.screen.common.scroll
{
    import application.views.screen.top.scroll.ScrollTrack;

    import flash.display.Sprite;
    import flash.geom.Rectangle;

    import framework.core.display.scroll.Scroll;

    public class Scrollbar extends Scroll
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Scrollbar(height:int)
        {
            var marker:Sprite = new ScrollMarker();
            var track:ScrollTrack = new ScrollTrack(height);
            var rectangle:Rectangle = new Rectangle(0, 0, 0, 218);

            super(marker, track, rectangle);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _track.x = 9;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
