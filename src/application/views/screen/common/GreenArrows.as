/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.12.13
 * Time: 13:31
 */
package application.views.screen.common
{
    import application.views.AppView;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.enum.Side;
    import framework.core.helpers.ImagesHelper;

    public class GreenArrows extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "NAV_ARROWS";
        public static const LEFT_OFFSET:int = 196;
        public static const RIGHT_OFFSET:int = 28;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GreenArrows(side:Side, count:int)
        {
            super();
            _side = _checkSide(side);
            _count = count;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setOffset();
            _addArrows();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _side:Side;
        private var _offset:int = 0;
        private var _count:int = 3;
        private var _margin:int = 20;
        private var _source:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _checkSide(side:Side):Side
        {
            if (side != Side.LEFT || side != Side.RIGHT)
                side = Side.LEFT;
            return side;
        }

        private function _setOffset():void
        {
            _offset = LEFT_OFFSET;
            if (_side == Side.RIGHT)
                _offset = RIGHT_OFFSET;
        }

        private function _addArrows():void
        {
            _source = super.source.getBitmap(SOURCE);
            var cx:int = 0;

            for (var i:int = 0; i < _count; i++) {
                var arrow:Bitmap = ImagesHelper.getParticular(_source, new Rectangle(0, _offset, 30, 28));
                addChild(arrow);
                arrow.x = cx;
                cx += _margin;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
