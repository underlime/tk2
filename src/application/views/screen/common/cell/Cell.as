/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 02.06.13
 * Time: 18:13
 */
package application.views.screen.common.cell
{
    import application.sound.lib.UISound;
    import application.views.PopupManager;
    import application.views.screen.common.buttons.AppSpriteButton;

    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.utils.Singleton;

    public class Cell extends AppSpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "ITEM_BG";
        public static const ITEM_WIDTH:int = 72;
        public static const ITEM_HEIGHT:int = 72;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Cell()
        {
            super(SOURCE);

            _w = ITEM_WIDTH;
            _h = ITEM_HEIGHT;

            _overPosition = 72;
            _selectedPosition = 144;
            _downPosition = 144;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addIcon();
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        override public function select():void
        {
            super.select();
            _selected = true;
            super.dispatchEvent(new Event(Event.SELECT));
        }

        public function clearSelection():void
        {
            super.unselect();
            _selected = false;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _icon:DisplayObject;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _getIconSource():void
        {

        }

        protected function _getIcon():void
        {
            if (super.source.has(_source))
                _icon = super.source.getBitmap(_source);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _selected:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        protected function _addIcon():void
        {
            _getIconSource();
            _getIcon();

            if (_icon) {
                addChild(_icon);
                _icon.x = ITEM_WIDTH / 2 - _icon.width / 2;
                _icon.y = ITEM_HEIGHT / 2 - _icon.height / 2;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            this.select();
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get selected():Boolean
        {
            return _selected;
        }

        public function get popupManager():PopupManager
        {
            return Singleton.getClass(PopupManager);
        }
    }
}
