/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.11.13
 * Time: 19:41
 */
package application.views.screen.common.cell
{
    import application.models.market.Item;
    import application.views.helpers.ItemDescriptionHelper;
    import application.views.screen.common.hint.BaseHeaderHint;
    import application.views.screen.common.hint.HintCorner;
    import application.views.text.AppTextField;

    import flash.text.TextFieldAutoSize;

    import framework.core.enum.Side;

    public class CellHintView extends BaseHeaderHint
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const WIDTH:int = 190;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CellHintView(item:Item)
        {
            super("");
            _item = item;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addText();
            _addCorner();
        }

        override public function destroy():void
        {
            super.destroy();
            _txtData.destroy();
        }

        override protected function _updateDimensions():void
        {

        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtData:AppTextField;
        private var _item:Item;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        protected function _addCorner():void
        {
            var corner:HintCorner = new HintCorner(Side.BOTTOM);
            addChild(corner);

            corner.x = int(super.w / 2 - corner.w / 2);
            corner.y = int(super.h - 6);
        }

        private function _addText():void
        {
            _txtData = new AppTextField();
            _txtData.width = WIDTH;
            _txtData.multiline = true;
            _txtData.wordWrap = true;
            _txtData.autoSize = TextFieldAutoSize.LEFT;
            _txtData.x = 10;
            _txtData.y = 10;
            addChild(_txtData);
            _txtData.htmlText = ItemDescriptionHelper.getDescription(_item);

            super.w = WIDTH;
            super.h = _txtData.y + _txtData.textHeight + 13;
            _updateBackground();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
