package application.views.screen.common.cell
{
    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.helpers.ImagesHelper;
    import framework.core.helpers.MathHelper;
    import framework.core.tools.SourceManager;

    public class ForgeIcon
    {
        public static const SOURCE:String = "ForgeIcons";
        public static const W:int = 23;
        public static const H:int = 23;

        public static function getIcon(level:int):Bitmap
        {
            var realLevel:int = MathHelper.clamp(level, 0, 17);
            var offset:int = (realLevel - 1) * H;

            var bitmap:Bitmap = SourceManager.instance.getBitmap(SOURCE);
            return ImagesHelper.getParticular(
                    bitmap,
                    new Rectangle(0, offset, W, H)
            );
        }

    }
}
