/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 16:40
 */
package application.views.screen.common.cell
{
    import application.common.Colors;
    import application.models.market.Item;
    import application.views.TextFactory;
    import application.views.screen.common.CommonData;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    import framework.core.helpers.ImagesHelper;

    public class MarketCell extends Cell
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketCell(item:Item, canEquip:Boolean = false)
        {
            _item = item;
            super();
            _canEquip = canEquip;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            if (!_canEquip) {
                _addLevelInfo();
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _getIconSource():void
        {
            if (_item)
                _source = CommonData.ICON_PREFIX + _item.picture.value;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item:Item;
        private var _canEquip:Boolean;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addLevelInfo():void
        {
            _addBack();
            _addInfo();
        }

        private function _addBack():void
        {
            var back:Bitmap = ImagesHelper.getParticular(
                    super.source.getBitmap(Cell.SOURCE),
                    new Rectangle(0, Cell.ITEM_HEIGHT * 3, Cell.ITEM_WIDTH, Cell.ITEM_HEIGHT)
            );
            addChild(back);
        }

        private function _addInfo():void
        {
            var txt:AppTextField = new AppTextField();
            txt.align = TextFormatAlign.CENTER;
            txt.color = Colors.WHITE;
            txt.size = 12;
            txt.width = Cell.ITEM_WIDTH;
            txt.height = 15;
            addChild(txt);
            txt.text = TextFactory.instance.getLabel('top_level_label') + "." + item.required_level.value.toString();
            txt.y = Cell.ITEM_HEIGHT - 26;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get item():Item
        {
            return _item;
        }
    }
}
