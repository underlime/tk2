/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.06.13
 * Time: 14:10
 */
package application.views.screen.common.cell
{
    import application.models.inventory.InventoryItem;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.text.CellTextCounter;
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Point;

    public class InventoryCell extends Cell
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InventoryCell(item:InventoryItem)
        {
            _item = item;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _setupCounter();
            _addLevel();
            _addHint();

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);

        }

        override public function destroy():void
        {
            if (_txtCount && !_txtCount.destroyed)
                _txtCount.destroy();

            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            if (_hint && !_hint.destroyed)
                _hint.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _getIconSource():void
        {
            if (_item)
                _source = CommonData.ICON_PREFIX + _item.picture.value;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item:InventoryItem;
        private var _txtCount:ShadowAppText;

        private var _hint:CellHintView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHint():void
        {
            _hint = new CellHintView(_item);
            addChild(_hint);
            var cords:Point = localToGlobal(new Point(-59, -1 * _hint.h - 10));
            removeChild(_hint);
            _hint.x = cords.x;
            _hint.y = int(cords.y);
        }

        private function _setupCounter():void
        {
            var inventoryCount:int = _item.count.value - _item.temp_put.value;
            if (_item && inventoryCount > 1) {
                _txtCount = new CellTextCounter();
                _txtCount.y = 43;
                _txtCount.x = _w - 9;
                addChild(_txtCount);

                var count:int = _item.count.value - _item.temp_put.value;
                _txtCount.text = "x" + count.toString();
            }
        }

        private function _addLevel():void
        {
            if (_item.sharpening_level.value > 0) {
                var icon:Bitmap = ForgeIcon.getIcon(_item.sharpening_level.value);
                icon.x = -7;
                icon.y = -7;
                addChild(icon);
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            if (_hint)
                super.popupManager.add(_hint);
        }

        private function _outHandler(event:MouseEvent):void
        {
            super.popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get item():InventoryItem
        {
            return _item;
        }
    }
}
