/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.12.13
 * Time: 13:22
 */
package application.views.screen.common.cell
{
    import application.models.inventory.InventoryItem;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    public class ForgeCell extends MarketCell
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ForgeCell(item:InventoryItem)
        {
            super(item);
            _item = item;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addLevel();
        }

        override public function select():void
        {

        }

        override protected function _onOverHandler(e:MouseEvent):void
        {

        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item:InventoryItem;
        private var _level:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addLevel():void
        {
            if (_level > 0)
                _addItemLevel(_level);
            else
                if (_item.sharpening_level.value > 0)
                    _addItemLevel(_item.sharpening_level.value);
        }

        private function _addItemLevel(level:int):void
        {
            var icon:Bitmap = ForgeIcon.getIcon(level);
            icon.x = -7;
            icon.y = -7;
            addChild(icon);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get level():int
        {
            return _level;
        }

        public function set level(value:int):void
        {
            _level = value;
        }
    }
}
