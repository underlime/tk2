/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 16:48
 */
package application.views.screen.common.cell
{
    import application.models.chars.CharDetail;
    import application.models.chars.CharGroup;
    import application.views.screen.common.CommonData;

    public class LaboratoryCell extends Cell
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LaboratoryCell(char:CharDetail)
        {
            super();
            _char = char;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _getIconSource():void
        {
            if (_char) {
                if (_char.group.value == CharGroup.BODIES.toString())
                    _source = _char.mc_2_name.value;
                else
                    _source = CommonData.ICON_PREFIX + _char.mc_1_name.value;
            }
        }

        override protected function _getIcon():void
        {
            if (_char.group.value == CharGroup.BODIES.toString())
                _icon = super.source.getMovieClip(_source, false);
            else
                super._getIcon();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _char:CharDetail;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get char():CharDetail
        {
            return _char;
        }
    }
}
