/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.10.13
 * Time: 14:48
 */
package application.views.screen.common.hint
{
    import application.common.Colors;
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Shape;

    import framework.core.enum.Side;
    import framework.core.struct.view.View;

    public class BaseHeaderHint extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseHeaderHint(header:String)
        {
            super();
            _header = header;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addHeader();
            _updateBackground();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _txtHeader:ShadowAppText;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _updateDimensions():void
        {
            super.w = int(_margin * 2 + _padding * 2 + _txtHeader.textWidth);
            super.h = int(_margin * 2 + _padding * 2 + _txtHeader.textHeight);

            _txtHeader.x = _margin;
            _txtHeader.y = _margin;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _header:String;

        private var _backShape:Shape;
        private var _centerShape:Shape;
        private var _frontShape:Shape;

        private var _padding:int = 3;
        private var _margin:int = 6;

        private var _side:Side = Side.TOP;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            _backShape = new Shape();
            addChild(_backShape);

            _centerShape = new Shape();
            addChild(_centerShape);

            _frontShape = new Shape();
            addChild(_frontShape);
        }

        private function _addHeader():void
        {
            _txtHeader = new ShadowAppText(Colors.BROWN);
            _txtHeader.size = 14;
            _txtHeader.color = Colors.WHITE;
            _addHeaderStyles();
            addChild(_txtHeader);
            _txtHeader.text = _header;
        }

        protected function _addHeaderStyles():void
        {

        }

        protected function _updateBackground():void
        {
            _updateDimensions();

            _backShape.graphics.clear();
            _centerShape.graphics.clear();
            _frontShape.graphics.clear();

            _backShape.graphics.beginFill(Colors.BROWN);
            _backShape.graphics.drawRect(0, 0, super.w, super.h);
            _backShape.graphics.endFill();

            _centerShape.x = _backShape.x + 2;
            _centerShape.y = _backShape.y + 2;

            _centerShape.graphics.beginFill(Colors.DARK_MILK);
            _centerShape.graphics.drawRect(0, 0, super.w - 4, super.h - 4);
            _centerShape.graphics.endFill();

            _frontShape.x = _centerShape.x + 4;
            _frontShape.y = _centerShape.y + 4;

            _frontShape.graphics.beginFill(Colors.MILK);
            _frontShape.graphics.drawRect(0, 0, super.w - 12, super.h - 12);
            _frontShape.graphics.endFill();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get header():String
        {
            return _header;
        }

        public function set header(value:String):void
        {
            _header = value;
            if (_txtHeader) {
                _txtHeader.text = _header;
                _updateBackground();
            }
        }

        public function get padding():int
        {
            return _padding;
        }

        public function set padding(value:int):void
        {
            _padding = value;
        }

        public function get side():Side
        {
            return _side;
        }

        public function set side(value:Side):void
        {
            _side = value;
        }
    }
}
