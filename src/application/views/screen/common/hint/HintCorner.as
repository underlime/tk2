/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.10.13
 * Time: 15:13
 */
package application.views.screen.common.hint
{
    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.enum.Side;
    import framework.core.struct.view.View;

    public class HintCorner extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const CORNER_OFFSET:int = 10;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "CONTEXT_MENU_ARROW";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HintCorner(side:Side)
        {
            super();
            super.w = 16;
            super.h = 16;
            _side = side;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            var bitmap:Bitmap = super.source.getBitmap(SOURCE);
            bitmap.scrollRect = new Rectangle(0, _getOffset(), super.w, super.h);
            addChild(bitmap);
            this.scrollRect = new Rectangle(0, 0, super.w, super.h);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _side:Side;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getOffset():Number
        {
            switch (_side) {
                case Side.TOP:
                    return 0;
                case Side.RIGHT:
                    return 16;
                case Side.BOTTOM:
                    return 32;
                case Side.LEFT:
                    return 48;
            }
            return 0;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
