/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.08.13
 * Time: 18:13
 */
package application.views.screen.common.hint
{
    import application.views.text.AppTextField;

    import flash.display.Shape;
    import flash.text.TextFieldAutoSize;

    import framework.core.struct.view.View;

    public class DetailHintView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DetailHintView(header:String, description:String)
        {
            super();
            _header = header;
            _description = description;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            _w = 197;
            _h = 174;

            addChild(_backShape);
            addChild(_frontShape);
            _setupTextField();
            _updateBackground();
            _updateHeight();
        }

        override protected function render():void
        {
            _txtHeader.text = _header;
            _txtDescription.text = _description;

        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _radius:Number = 10;
        private var _backColor:uint = 0xfbedd4;
        private var _backAlpha:Number = 0.9;
        private var _frontBorderSize:Number = 2;
        private var _frontBorderColor:uint = 0xc49a6c;
        private var _frontMargin:Number = 4;

        private var _description:String = "";
        private var _header:String = "";

        private var _backShape:Shape = new Shape();
        private var _frontShape:Shape = new Shape();

        private var _txtHeader:AppTextField;
        private var _txtDescription:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateBackground():void
        {
            _backShape.graphics.clear();
            _backShape.graphics.beginFill(_backColor, _backAlpha);
            _backShape.graphics.drawRoundRect(0, 0, _w, _h, _radius + 6);
            _backShape.graphics.endFill();

            _frontShape.graphics.clear();
            _frontShape.graphics.beginFill(0x000000, 0);
            _frontShape.graphics.lineStyle(_frontBorderSize, _frontBorderColor);

            _frontShape.graphics.drawRoundRect(_frontMargin, _frontMargin, _w - (_frontMargin * 2), _h - (_frontMargin * 2), _radius);
            _frontShape.graphics.endFill();

        }

        private function _setupTextField():void
        {
            _setupHeader();
            _setupDescription();
        }

        private function _setupHeader():void
        {
            _txtHeader = new AppTextField();
            _txtHeader.color = 0xcc0000;
            _txtHeader.size = 12;
            _txtHeader.bold = true;

            _txtHeader.x = _txtHeader.y = _frontMargin * 2;
            _txtHeader.width = _w - (_frontMargin * 2) * 2;
            _txtHeader.wordWrap = true;
            _txtHeader.multiline = true;

            _txtHeader.autoSize = TextFieldAutoSize.LEFT;
            addChild(_txtHeader);
        }

        private function _setupDescription():void
        {
            _txtDescription = new AppTextField();
            _txtDescription.size = 12;
            _txtDescription.bold = true;
            _txtDescription.color = AppTextField.LIGHT_BROWN;

            _txtDescription.x = _frontMargin * 2;
            _txtDescription.y = _txtHeader.y + _txtHeader.height + 2;

            _txtDescription.width = _w - (_frontMargin * 2) * 2;
            _txtDescription.wordWrap = true;
            _txtDescription.multiline = true;
            _txtDescription.autoSize = TextFieldAutoSize.LEFT;

            addChild(_txtDescription);
        }

        private function _updateHeight():void
        {
            _h = _txtDescription.height + _txtHeader.height + 20;
            _updateBackground();
        }

        private function _update():void
        {
            _txtDescription.y = _txtHeader.y + _txtHeader.height + 2;
            _updateHeight();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set description(value:String):void
        {
            _description = value;
            _txtDescription.text = _description;
            _update();
        }

        public function set header(value:String):void
        {
            _header = value;
            _txtHeader.text = _header;
            _update();
        }
    }
}
