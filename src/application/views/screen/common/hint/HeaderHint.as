/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.10.13
 * Time: 15:24
 */
package application.views.screen.common.hint
{
    import application.models.common.hint.HintModel;

    import framework.core.enum.Side;

    public class HeaderHint extends BaseHeaderHint
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HeaderHint(model:HintModel = null)
        {
            if (model)
                _model = model;
            super("");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        private function _addCorner():void
        {
            if (_corner && !_corner.destroyed)
                _corner.destroy();
            _corner = new HintCorner(_model.side);
            addChild(_corner);
        }

        override protected function render():void
        {
            super.render();
            _update();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:HintModel;
        private var _corner:HintCorner;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateCornerPosition():void
        {
            switch (_model.side) {
                case Side.TOP:
                    _setTop();
                    break;
                case Side.RIGHT:
                    _setRight();
                    break;
                case Side.BOTTOM:
                    _setBottom();
                    break;
                case Side.LEFT:
                    _setLeft();
            }
        }

        private function _setLeft():void
        {
            _corner.x = -1 * HintCorner.CORNER_OFFSET;
            _corner.y = int(super.h / 2 - _corner.h / 2);
        }

        private function _setBottom():void
        {
            _corner.x = int(super.w / 2 - _corner.w / 2);
            _corner.y = super.h - HintCorner.CORNER_OFFSET + 4;
        }

        private function _setRight():void
        {
            _corner.x = super.w - HintCorner.CORNER_OFFSET + 4;
            _corner.y = int(super.h / 2 - _corner.h / 2);
        }

        private function _setTop():void
        {
            _corner.x = int(super.w / 2 - _corner.w / 2);
            _corner.y = -1 * HintCorner.CORNER_OFFSET;
        }

        private function _update():void
        {
            super.header = _model.header;
            _addCorner();
            _updateCornerPosition();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get model():HintModel
        {
            return _model;
        }

        public function set model(value:HintModel):void
        {
            _model = value;
            _update();
        }
    }
}
