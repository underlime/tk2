/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.09.13
 * Time: 10:38
 */
package application.views.screen.common.radio
{
    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.display.components.RadioButton;
    import framework.core.helpers.ImagesHelper;

    public class ColoredRadioButton extends RadioButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "COLOR_SELECT";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ColoredRadioButton()
        {
            super(SOURCE, "");

            _setupBird();

            _w = 28;
            _h = 28;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function select():void
        {
            super.select();
            _addBird();
        }

        override public function unselect():void
        {
            super.unselect();
            _removeBird();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _color:uint = 0x000000;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bird:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBird():void
        {
            var source:Bitmap = super.source.getBitmap(SOURCE);
            _bird = ImagesHelper.getParticular(source, new Rectangle(0, 0, 28, 28));
        }

        private function _addBird():void
        {
            addChild(_bird);
        }

        private function _removeBird():void
        {
            if (_bird && this.contains(_bird))
                removeChild(_bird);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get color():uint
        {
            return _color;
        }
    }
}
