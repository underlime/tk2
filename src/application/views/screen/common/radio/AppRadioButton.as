/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.09.13
 * Time: 14:09
 */
package application.views.screen.common.radio
{
    import application.views.text.AppTextField;

    import flash.text.TextFieldAutoSize;

    import framework.core.display.components.RadioButton;

    public class AppRadioButton extends RadioButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "CHECK_BOXES";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AppRadioButton(label:String)
        {
            super(SOURCE, label);

            _outPosition = 112;
            _overPosition = 140;

            _selectedPosition = 168;
            _selectedOverPosition = 196;

            _w = 28;
            _h = 28;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _addLabelField():void
        {
            _txtLabel = new AppTextField();
            _txtLabel.size = 12;
            _txtLabel.color = AppTextField.DARK_BROWN;
            _txtLabel.autoSize = TextFieldAutoSize.LEFT;
            _txtLabel.bold = true;

            addChild(_txtLabel);
            _txtLabel.text = _label;

            _txtLabel.x = _w;
            _txtLabel.y = _h / 2 - _txtLabel.textHeight / 2 - 2;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
