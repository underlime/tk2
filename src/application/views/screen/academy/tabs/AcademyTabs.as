/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.09.13
 * Time: 17:37
 */
package application.views.screen.academy.tabs
{
    import application.views.screen.common.tabs.AppTabs;
    import application.views.screen.common.tabs.TabFactory;
    import application.views.screen.common.tabs.TabType;

    import framework.core.display.tabs.ITab;

    public class AcademyTabs extends AppTabs
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AcademyTabs()
        {
            var tabs:Vector.<ITab> = new Vector.<ITab>();
            tabs.push(
                    TabFactory.getTab(TabType.TRAIN_TAB),
                    TabFactory.getTab(TabType.LABORATORY_TAB),
                    TabFactory.getTab(TabType.CHANGE_NAME_TAB),
                    TabFactory.getTab(TabType.CHANGE_VEGETABLE_TAB)
            );

            super(tabs);
            _dx = 3;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
