/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.09.13
 * Time: 17:26
 */
package application.views.screen.academy
{
    import application.views.TextFactory;
    import application.views.screen.academy.tabs.AcademyTabs;
    import application.views.screen.base.BaseScreen;

    import flash.events.Event;

    import framework.core.display.tabs.TabPanel;

    public class AcademyScreen extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AcademyScreen()
        {
            super(TextFactory.instance.getLabel('laboratory_label'));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _setupTabs();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _tabs:TabPanel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTabs():void
        {
            _tabs = new AcademyTabs();
            _tabs.addEventListener(Event.CHANGE, _changeHandler);
            _tabs.y = 43;
            _tabs.x = 50;
            addChild(_tabs);
            _tabs.select(0);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            event.stopImmediatePropagation();
            super.dispatchEvent(new Event(Event.CHANGE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get tabs():TabPanel
        {
            return _tabs;
        }
    }
}
