/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 14.11.13
 * Time: 15:45
 */
package application.views.screen.achievements
{
    import application.common.Colors;
    import application.views.screen.common.hint.BaseHeaderHint;
    import application.views.screen.common.hint.HintCorner;
    import application.views.text.AppTextField;

    import flash.text.TextFieldAutoSize;

    import framework.core.enum.Side;

    public class LockHintView extends BaseHeaderHint
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const WIDTH:int = 200;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LockHintView(header:String, message:String)
        {
            super(header);
            _message = message;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();

            _txtHeader.x = 10;
            _txtHeader.y = 10;

            _addText();
            _addCorner();
        }

        override public function destroy():void
        {
            super.destroy();
            _txtData.destroy();
        }

        override protected function _updateDimensions():void
        {

        }


        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _textHintWidth:int = WIDTH - 20;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _addCorner():void
        {
            var corner:HintCorner = new HintCorner(Side.BOTTOM);
            addChild(corner);

            corner.x = int(super.w / 2 - corner.w / 2);
            corner.y = int(super.h) - 6;
        }

        override protected function _addHeaderStyles():void
        {
            _txtHeader.multiline = true;
            _txtHeader.wordWrap = true;
            _txtHeader.width = _textHintWidth;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _message:String = "";
        private var _txtData:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addText():void
        {
            _txtData = new AppTextField();
            _txtData.width = _textHintWidth;
            _txtData.multiline = true;
            _txtData.wordWrap = true;
            _txtData.autoSize = TextFieldAutoSize.LEFT;
            _txtData.color = Colors.DARK_BROWN;
            _txtData.x = 10;
            _txtData.y = _txtHeader.y + _txtHeader.textHeight + 10;
            addChild(_txtData);
            _txtData.text = _message;

            super.w = _textHintWidth + 20;
            super.h = int(_txtData.textHeight + _txtData.y + 25);
            _updateBackground();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
