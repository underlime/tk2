/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.03.14
 * Time: 21:23
 */
package application.views.screen.achievements
{

    import flash.display.Bitmap;

    import framework.core.display.buttons.SpriteButton;

    public class LockBlock extends SpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LockBlock(bitmap:Bitmap)
        {
            super(LockButton.SOURCE);
            _bitmap = bitmap;
            _w = 80;
            _h = 80;

            _outPosition = 80;
            _overPosition = 80;
            _downPosition = 80;
            _blockPosition = 80;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _bitmap.x = 7;
            _bitmap.y = 7;
            addChild(_bitmap);
            block();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bitmap:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
