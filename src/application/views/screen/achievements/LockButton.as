/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.08.13
 * Time: 14:05
 */
package application.views.screen.achievements
{
    import application.views.PopupManager;
    import application.views.screen.common.CommonData;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Point;

    import framework.core.display.buttons.SpriteButton;
    import framework.core.utils.Singleton;

    public class LockButton extends SpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "ACHIEVE_AND_SKILL_FRAME";
        public static const DEFAULT_ICON:String = "iachieve001";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LockButton(icon:String, header:String = "", message:String = "")
        {
            super(SOURCE);
            _header = header;
            _message = message;
            _w = 80;
            _h = 80;

            _outPosition = 80;
            _overPosition = 160;
            _downPosition = 160;

            _icon = icon;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHint();
            _addIcon();
        }

        override public function destroy():void
        {
            _hintView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:String;
        private var _header:String;
        private var _message:String;
        private var _hintView:LockHintView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHint():void
        {
            _hintView = new LockHintView(_header, _message);
            addChild(_hintView);
            var cords:Point = localToGlobal(new Point(-60, -1 * _hintView.h - 10));
            removeChild(_hintView);
            _hintView.x = cords.x;
            _hintView.y = int(cords.y);
        }

        private function _addIcon():void
        {
            var icon:Bitmap;
            if (super.source.has(CommonData.ICON_PREFIX + _icon))
                icon = super.source.getBitmap(CommonData.ICON_PREFIX + _icon);
            else
                icon = super.source.getBitmap(DEFAULT_ICON);
            icon.x = 7;
            icon.y = 7;
            addChild(icon);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _onOverHandler(e:MouseEvent):void
        {
            super._onOverHandler(e);
            popupManager.add(_hintView);
        }

        override protected function _onOutHandler(e:MouseEvent):void
        {
            super._onOutHandler(e);
            popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get popupManager():PopupManager
        {
            return Singleton.getClass(PopupManager);
        }

    }
}
