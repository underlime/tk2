/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.08.13
 * Time: 12:34
 */
package application.views.screen.achievements
{
    import application.models.inventory.Inventory;
    import application.models.user.IUserData;
    import application.models.user.achievements.AchievementRecord;
    import application.models.user.achievements.Achievements;
    import application.views.TextFactory;
    import application.views.screen.common.navigation.Navigation;
    import application.views.screen.common.state.ScreenState;
    import application.views.screen.home.HomeCounter;
    import application.views.screen.home.UserInfoBlock;

    import flash.display.DisplayObject;
    import flash.events.Event;

    import framework.core.struct.view.View;

    public class AchievementsScreen extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AchievementsScreen(user:IUserData, inventory:Inventory)
        {
            _user = user;
            _inventory = inventory;
            _model = _user.userAchievements;
            super();
            this.y = 107;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            var items:Array = _model.search({});
            _total = items.length;
            _state.limit = 8;

            _totalAchievements = _user.userOptInfo.achievements_count.value;
        }

        override protected function render():void
        {
            _addNavigation();
            _addCounter();
            _updateParams();
            _bindButtons();
            _addTileList();
            _updateTileList();
            _updateTiles();
            _addUserInfoBlock();
        }

        override public function destroy():void
        {
            _tileList.destroy();
            _navigation.destroy();

            if (_userInfoBlock)
                _userInfoBlock.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _tileList:AchievementsTileList;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _addNavigation():void
        {
            _navigation = new Navigation();
            _navigation.arrowMargin = 3;
            addChild(_navigation);
            _navigation.y = 310;
            _navigation.x = 70;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:Achievements;
        private var _state:ScreenState = new ScreenState();
        private var _tiles:Array = [];
        private var _total:int = 0;

        private var _navigation:Navigation;
        private var _userInfoBlock:UserInfoBlock;

        private var _counter:HomeCounter;

        protected var _user:IUserData;
        protected var _inventory:Inventory;

        protected var _totalAchievements:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addCounter():void
        {
            var header:String = TextFactory.instance.getLabel("achievements_label");
            var current:int = _total;

            _counter = new HomeCounter(header, current, _totalAchievements);
            addChild(_counter);
            _counter.x = 278;
            _counter.y = 310;
        }

        private function _addTileList():void
        {
            _tileList = new AchievementsTileList();
            addChild(_tileList);
        }

        private function _updateTiles():void
        {
            var tiles:Vector.<DisplayObject> = new Vector.<DisplayObject>();

            var count:int = _tiles.length;

            for (var i:int = 0; i < count; i++) {

                var record:AchievementRecord = _tiles[i] as AchievementRecord;
                var button:LockButton = new LockButton(
                        record.pic.value,
                        record.name_ru.value,
                        record.text_ru.value
                );

                tiles.push(button);
            }

            _tileList.tiles = tiles;
            _tileList.draw();
        }

        private function _updateParams():void
        {
            _state.start = 0;
            _state.currentPage = 1;
            _state.totalPages = int(Math.ceil(_total / _state.limit));
            _updatePageData();
        }

        private function _updatePageData():void
        {
            _navigation.currentPage = _state.currentPage;
            _navigation.totalPages = _state.totalPages;
        }

        private function _bindButtons():void
        {
            _navigation.addEventListener(Event.CHANGE, _navigationChangeHandler);
        }

        private function _updatePage():void
        {
            _state.start = (_state.currentPage - 1) * _state.limit;
            _updatePageData();
        }

        private function _updateTileList():void
        {
            _tiles = [];
            _tiles = _model.search({}, _state.start, _state.limit);
        }

        protected function _addUserInfoBlock():void
        {
            _userInfoBlock = new UserInfoBlock(_user, _inventory);
            _userInfoBlock.x = 438;
            _userInfoBlock.y = 37;
            addChild(_userInfoBlock);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _navigationChangeHandler(event:Event):void
        {
            _state.currentPage = _navigation.currentPage;
            _updatePage();
            _updateTileList();
            _updateTiles();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
