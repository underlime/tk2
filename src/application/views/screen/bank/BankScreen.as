/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.09.13
 * Time: 19:34
 */
package application.views.screen.bank
{
    import application.views.TextFactory;
    import application.views.screen.base.BaseScreen;
    import application.views.screen.home.HomeScreen;

    import flash.display.Bitmap;
    import flash.display.MovieClip;

    public class BankScreen extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOMATO:String = "BankTomato";
        public static const CUCUMBER:String = "BankCucumber";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankScreen()
        {
            super(TextFactory.instance.getLabel("bank_label"));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addPositions();
            _addPersonages();
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bankTiles:BankTiles;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addPersonages():void
        {
            var tomato:MovieClip = super.source.getMovieClip(TOMATO);
            var cucumber:MovieClip = super.source.getMovieClip(CUCUMBER);

            var tomatoShade:Bitmap = super.source.getBitmap(HomeScreen.SHADOW);
            var cucumberShade:Bitmap = super.source.getBitmap(HomeScreen.SHADOW)

            addChild(tomatoShade);
            addChild(cucumberShade);

            addChild(tomato);
            addChild(cucumber);

            tomato.x = 613;
            tomato.y = 275;
            tomatoShade.x = tomato.x;
            tomatoShade.y = tomato.y + 75;

            cucumber.x = 45;
            cucumber.y = 237;

            cucumberShade.x = cucumber.x - 10;
            cucumberShade.y = cucumber.y + 110;
        }

        private function _addPositions():void
        {
            _bankTiles = new BankTiles();
            addChild(_bankTiles);
            _bankTiles.x = 149;
            _bankTiles.y = 42;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
