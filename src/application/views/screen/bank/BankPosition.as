/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.09.13
 * Time: 20:04
 */
package application.views.screen.bank
{
    import application.config.AppConfig;
    import application.events.BankEvent;
    import application.models.bank.BankRecord;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.buttons.SpecialButton;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    import framework.core.enum.Language;
    import framework.core.helpers.StringHelper;
    import framework.core.socnet.SocNet;

    import org.casalib.util.NumberUtil;

    public class BankPosition extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "MONEY_BG";
        public static const POSITION_WIDTH:int = 140;
        public static const POSITION_HEIGHT:int = 160;
        public static const BEST_PRICE:String = "BEST_PRICE";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankPosition(icon:Bitmap, record:BankRecord)
        {

            super();
            _icon = icon;
            _ferros = record.count.value;
            _label = _getLabel(record);
            _bestPrice = record.best_price.value;
            _record = record;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addIcon();
            _addNote();
            _addBuyButton();
            _addPrice();

            if (_bestPrice)
                _addBestPrice();

            _bindReactions();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            _buyButton.destroy();
            _txtNote.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _ferros:int;
        private var _label:String;
        private var _icon:Bitmap;
        private var _bestPrice:Boolean;

        private var _background:Bitmap;
        private var _scrollRect:Rectangle = new Rectangle(0, 0, POSITION_WIDTH, POSITION_HEIGHT);

        private var _txtNote:AppTextField;
        private var _buyButton:SpecialButton;
        private var _record:BankRecord;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getLabel(record:BankRecord):String
        {
            var variants:Array;
            switch (SocNet.socNetType) {
                case SocNet.VK_COM:
                case SocNet.MOCK:
                    variants = ['голос', 'голоса', 'голосов'];
                    break;
                case SocNet.ODNOKLASSNIKI_RU:
                    variants = ['OK', 'OK', 'OK'];
                    break;
                default:
                    variants = ['', '', ''];
            }
            var label:String = NumberUtil.format(record.price.value, ' ')
                    + ' '
                    + StringHelper.chooseRuPlural(record.price.value, variants);
            return label;
        }

        private function _addBackground():void
        {
            _background = super.source.getBitmap(BACK);
            _background.scrollRect = _scrollRect;

            addChild(_background);
        }

        private function _addIcon():void
        {
            addChild(_icon);
            _icon.x = POSITION_WIDTH / 2 - _icon.width / 2;
            _icon.y = _icon.height / 2 - 5;
        }

        private function _addNote():void
        {
            _txtNote = new AppTextField();
            _txtNote.color = AppTextField.RED;
            _txtNote.size = 12;
            _txtNote.align = TextFormatAlign.CENTER;
            _txtNote.width = POSITION_WIDTH;
            _txtNote.bold = true;
            _txtNote.height = 20;
            addChild(_txtNote);
            _txtNote.text = _label;
            _txtNote.y = 94;
        }

        private function _addBuyButton():void
        {
            _buyButton = new SpecialButton(TextFactory.instance.getLabel("buy_label"));
            _buyButton.w = 126;
            _buyButton.fontSize = 12;
            addChild(_buyButton);
            _buyButton.x = 7;
            _buyButton.y = 113;

            _buyButton.addEventListener(MouseEvent.CLICK, _buyClickHandler);
        }

        private function _addPrice():void
        {
            var margin:int = 8;

            var icon:Bitmap = super.source.getBitmap(CommonData.FERROS_SMALL_ICON_BUTTON);
            addChild(icon);

            var priceText:ShadowAppText = new ShadowAppText(AppTextField.DARK_BROWN);
            priceText.size = 18;
            priceText.color = 0xffffff;
            priceText.bold = true;
            addChild(priceText);
            priceText.text = _ferros.toString();

            priceText.x = POSITION_WIDTH / 2 - (priceText.textWidth + margin + icon.width) / 2;
            priceText.y = 6;

            icon.x = priceText.x + margin + priceText.textWidth;
            icon.y = 8;
        }

        private function _addBestPrice():void
        {
            var icon:Bitmap = super.source.getBitmap(BEST_PRICE);
            var margin:int = 30;
            var rect:Rectangle = new Rectangle(0, 0, 46, 46);
            if (AppConfig.LANGUAGE == Language.EN)
                rect.y = 46;
            icon.scrollRect = rect;

            addChild(icon);
            icon.x = POSITION_WIDTH - margin;
            icon.y = -margin + 10;
        }

        private function _bindReactions():void
        {
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            _scrollRect.y = POSITION_HEIGHT;
            _background.scrollRect = _scrollRect;
        }

        private function _outHandler(event:MouseEvent):void
        {
            _scrollRect.y = 0;
            _background.scrollRect = _scrollRect;
        }

        private function _buyClickHandler(e:MouseEvent):void
        {
            var event:BankEvent = new BankEvent(BankEvent.BUY, true);
            event.data['record'] = _record;
            super.dispatchEvent(event);
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
