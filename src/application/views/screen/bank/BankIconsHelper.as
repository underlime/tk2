/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.09.13
 * Time: 20:12
 */
package application.views.screen.bank
{
    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.helpers.ImagesHelper;
    import framework.core.tools.SourceManager;
    import framework.core.utils.Assert;

    public class BankIconsHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ICON_SOURCE:String = "BANK_MONEY_ICONS";
        public static const ICON_WIDTH:int = 80;
        public static const ICON_HEIGHT:int = 70;

        // CLASS METHODS -----------------------------------------------------------------------/

        /**
         * Возвращает иконку
         * @param type - номер позиции
         * @return
         */
        public static function getIcon(type:int):Bitmap
        {
            CONFIG::DEBUG {
                Assert.indexInRange(type, 0, 5);
            }

            var offset:int = type * ICON_HEIGHT;
            var iconSprite:Bitmap = SourceManager.instance.getBitmap(ICON_SOURCE);

            return ImagesHelper.getParticular(iconSprite, new Rectangle(0, offset, ICON_WIDTH, ICON_HEIGHT));
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
