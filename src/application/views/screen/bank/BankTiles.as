/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.09.13
 * Time: 20:10
 */
package application.views.screen.bank
{
    import application.models.ModelData;
    import application.models.bank.Bank;
    import application.models.bank.BankRecord;

    import flash.display.DisplayObject;

    import framework.core.display.components.TileList;
    import framework.core.helpers.StringHelper;
    import framework.core.socnet.SocNet;
    import framework.core.struct.data.ModelsRegistry;

    import org.casalib.util.NumberUtil;

    public class BankTiles extends TileList
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankTiles()
        {
            var positions:Vector.<DisplayObject> = new <DisplayObject>[];
            var i:int = 0;
            for each (var record:BankRecord in _bankModel.list.value) {
                if (i > 5)
                    break;
                positions.push(new BankPosition(BankIconsHelper.getIcon(i), record));
                ++i;
            }

            super(positions);

            _dx = 150;
            _dy = 165;

            _x0 = 0;
            _y0 = 0;

            _cols = 3;
            _lines = 3;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            super.draw();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bankModel:Bank = ModelsRegistry.getModel(ModelData.BANK_MODEL) as Bank;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
