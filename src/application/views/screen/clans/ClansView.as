/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.01.14
 * Time: 20:39
 */
package application.views.screen.clans
{
    import application.views.TextFactory;
    import application.views.screen.base.BaseScreen;
    import application.views.screen.clans.tabs.ClansTabs;
    import application.views.screen.office.OfficeScreen;

    import flash.display.Bitmap;
    import flash.events.Event;

    public class ClansView extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClansView(role:ClanUserRole, otherClan:Boolean = false)
        {
            super(TextFactory.instance.getLabel('clan_hall_label'));
            _role = role;
            _otherClan = otherClan;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addBack();
            _addTabs();
        }

        override public function destroy():void
        {
            _tabs.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _role:ClanUserRole;
        private var _tabs:ClansTabs;
        private var _otherClan:Boolean;

        private var _showLast:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            var back:Bitmap = super.source.getBitmap(OfficeScreen.BACK);
            addChild(back);

            back.x = 38;
            back.y = 101;
        }

        private function _addTabs():void
        {
            _tabs = new ClansTabs(_role, _otherClan);
            _tabs.addEventListener(Event.CHANGE, _changeHandler);
            _tabs.y = 43;
            _tabs.x = 50;
            addChild(_tabs);

            if (_showLast)
                _tabs.select(_tabs.tabs.length - 1);
            else
                _tabs.select(0);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get tabID():int
        {
            return _tabs.selectedTab;
        }

        public function showLastTab():void
        {
            _showLast = true;
        }
    }
}
