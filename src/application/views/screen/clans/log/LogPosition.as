/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.01.14
 * Time: 12:42
 */
package application.views.screen.clans.log
{
    import application.events.ClanEvent;
    import application.models.clans.ClanRequestRecord;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.helpers.icons.IconType;
    import application.views.helpers.icons.IconsFactory;
    import application.views.screen.common.RankView;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.buttons.RejectButton;
    import application.views.screen.common.frame.UserFrame;
    import application.views.screen.stats.StatsPosition;
    import application.views.screen.user_info.IconPosition;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.MouseEvent;
    import flash.events.SecurityErrorEvent;
    import flash.geom.Rectangle;
    import flash.net.URLRequest;
    import flash.system.LoaderContext;
    import flash.text.TextFieldAutoSize;

    import framework.core.helpers.DateHelper;
    import framework.core.helpers.ImagesHelper;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.view.View;

    public class LogPosition extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LogPosition(clanRequestRecord:ClanRequestRecord)
        {
            super();
            _clanRequestRecord = clanRequestRecord;
            super.w = 620;
            super.h = 120;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addHeader();
            _addAvatar();
            _addLogin();
            _addRank();
            _addGlory();
            _addButtons();
        }

        override public function destroy():void
        {
            if (_avatarContainer && !_avatarContainer.destroyed) {
                _avatarContainer.destroy();
            }
            super.destroy();
        }

// PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function addResolution(resolution:Boolean):void
        {
            _joinButton.destroy();
            _cancelButton.destroy();
            var clanResolution:ClanResolution = new ClanResolution(resolution);
            addChild(clanResolution);
            clanResolution.x = 432;
            clanResolution.y = 40;

            if (resolution) {
                super.sound.playUISound(UISound.OK);
            }
            else {
                super.sound.playUISound(UISound.CANCEL);
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _back:Bitmap;

        private var _scrollRect:Rectangle;
        private var _header:String = TextFactory.instance.getLabel("clan_request");
        private var _avatar:Bitmap;
        private var _txtLogin:AppTextField;
        private var _rankView:RankView;

        private var _joinButton:AcceptButton;
        private var _cancelButton:RejectButton;

        private var _clanRequestRecord:ClanRequestRecord;
        private var _avatarLoader:Loader;
        private var _avatarContainer:View;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            _back = super.source.getBitmap(StatsPosition.BACK);
            addChild(_back);

            _scrollRect = new Rectangle(0, 0, super.w, super.h);
            _back.scrollRect = _scrollRect;

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _addHeader():void
        {
            var dateFormat:String = "%d.%m.%Y %H:%i";
            var date:DateTimeField = _clanRequestRecord.request_time;

            var result:String = "<font size='18' color='#be1e2d'><b>{0}</b></font>".replace('{0}', _header);
            var dateMsg:String = "  <font color='#3c2415'><b>{0}</b></font>".replace('{0}', DateHelper.formatDate(date.value, dateFormat));

            var upText:AppTextField = new AppTextField();
            upText.autoSize = TextFieldAutoSize.LEFT;
            addChild(upText);
            upText.htmlText = result + dateMsg;
            upText.x = 15;
            upText.y = 10;
        }

        private function _addAvatar():void
        {
            var icon:Bitmap = super.source.getBitmap(UserFrame.DEFAULT_ICON, true);
            var data:BitmapData = ImagesHelper.matrixResize(icon, 40, 40, true);
            _avatar = new Bitmap(data);
            addChild(_avatar);

            _avatar.x = 10;
            _avatar.y = 41;

            _avatarLoader = new Loader();
            _avatarLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, _onAvatarLoaded);
            _avatarLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
            _avatarLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);

            var request:URLRequest = new URLRequest(_clanRequestRecord.picture.value);
            var context:LoaderContext = new LoaderContext(true);
            _avatarLoader.load(request, context);
        }

        private function _onAvatarLoaded(e:Event):void
        {
            _avatarContainer = new View();
            addChild(_avatarContainer);

            _avatarContainer.addChild(_avatarLoader);
            _avatarContainer.width = 34;
            _avatarContainer.height = 34;
            _avatarContainer.x = 14;
            _avatarContainer.y = 41;

            _clearLoader();
        }

        private function _onAvatarError(e:Event):void
        {
            _clearLoader();
        }

        private function _clearLoader():void
        {
            _avatarLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, _onAvatarLoaded);
            _avatarLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
            _avatarLoader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);
        }

        private function _addLogin():void
        {
            var login:String = _clanRequestRecord.login.value;
            var level:String = " [" + TextFactory.instance.getLabel("top_level_label") + "." + _clanRequestRecord.max_level.value + "]";

            _txtLogin = new AppTextField();
            _txtLogin.color = 0x3c2415;
            _txtLogin.size = 14;
            _txtLogin.autoSize = TextFieldAutoSize.LEFT;
            _txtLogin.bold = true;
            addChild(_txtLogin);
            _txtLogin.htmlText = login + level;
            _txtLogin.x = 50;
            _txtLogin.y = 48;
        }

        private function _addRank():void
        {
            _rankView = new RankView(_clanRequestRecord.glory.value);
            _rankView.x = _txtLogin.x + _txtLogin.textWidth + 20;
            _rankView.y = 50;
            addChild(_rankView);
        }

        private function _addGlory():void
        {
            var gloryPosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.GLORY_ICO), _clanRequestRecord.glory.value);
            gloryPosition.x = 10;
            gloryPosition.y = 75;
            gloryPosition.verticalMargin = -2;
            addChild(gloryPosition);
        }

        private function _addButtons():void
        {
            _joinButton = new AcceptButton(TextFactory.instance.getLabel("join_clan_label"));
            _cancelButton = new RejectButton(TextFactory.instance.getLabel("cancel_clan_label"));

            _joinButton.w = 106;
            _cancelButton.w = 106;

            _joinButton.fontSize = 12;
            _cancelButton.fontSize = 12;

            addChild(_joinButton);
            addChild(_cancelButton);

            _joinButton.x = 482;
            _joinButton.y = 16;

            _cancelButton.x = 482;
            _cancelButton.y = 62;

            _joinButton.addEventListener(MouseEvent.CLICK, _joinCLickHandler);
            _cancelButton.addEventListener(MouseEvent.CLICK, _cancelClickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            _scrollRect.y = super.h;
            _back.scrollRect = _scrollRect;
        }

        private function _outHandler(event:MouseEvent):void
        {
            _scrollRect.y = 0;
            _back.scrollRect = _scrollRect;
        }

        private function _joinCLickHandler(event:MouseEvent):void
        {
            var e:ClanEvent = new ClanEvent(ClanEvent.RESOLUTION, true);
            e.resolution = true;
            e.user_id = _clanRequestRecord.user_id.value;
            dispatchEvent(e);
        }

        private function _cancelClickHandler(event:MouseEvent):void
        {
            var e:ClanEvent = new ClanEvent(ClanEvent.RESOLUTION, true);
            e.resolution = false;
            e.user_id = _clanRequestRecord.user_id.value;
            dispatchEvent(e);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get user_id():int
        {
            return _clanRequestRecord.user_id.value;
        }

    }
}
