/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.01.14
 * Time: 12:40
 */
package application.views.screen.clans.log
{
    import application.models.clans.ClanRequestRecord;
    import application.models.clans.ClanRequests;
    import application.views.screen.stats.StatsView;

    public class CLanLogView extends StatsView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CLanLogView(clanRequests:ClanRequests)
        {
            super();
            _clanRequests = clanRequests;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function addResolution(user_id:int, resolution:Boolean):void
        {
            var count:int = _positions.length;
            for (var i:int = 0; i < count; i++) {
                var position:LogPosition = _positions[i];
                if (position.user_id == user_id)
                    position.addResolution(resolution);
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addPositions():void
        {
            var dy:int = 125;
            var cy:int = 0;
            var height:int = 0;

            var position:LogPosition;

            if (!_clanRequests.isEmpty) {
                var list:Object = _clanRequests.list.value;
                for (var key:String in list) {

                    position = new LogPosition(list[key] as ClanRequestRecord);
                    _scrollContainer.addChild(position);
                    position.y = cy;
                    cy += dy;
                    height += 125;
                    _positions.push(position);
                }
                _scrollContainer.$drawBackground(0);
            } else {
                _addEmptyMessage();
            }
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _clanRequests:ClanRequests;
        private var _positions:Array = [];

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
