/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.01.14
 * Time: 18:08
 */
package application.views.screen.clans.show
{
    import application.models.clans.ClanInfo;
    import application.views.AppView;

    public class ShowClanLayout extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShowClanLayout(clanInfo:ClanInfo)
        {
            super();
            _clanInfo = clanInfo;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addClanInfo();
            _showClanMember();
        }

        override public function destroy():void
        {
            _clanInfoView.destroy();
            _clanMembers.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _clanInfo:ClanInfo;
        private var _clanInfoView:ClanInfoView;
        private var _clanMembers:ClanMembersView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addClanInfo():void
        {
            _clanInfoView = new ClanInfoView(_clanInfo);
            addChild(_clanInfoView);
        }

        private function _showClanMember():void
        {
            _clanMembers = new ClanMembersView(_clanInfo.clan_members);
            _clanMembers.x = 249;
            _clanMembers.y = 223;
            addChild(_clanMembers);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
