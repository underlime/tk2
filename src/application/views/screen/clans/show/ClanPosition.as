/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.01.14
 * Time: 18:26
 */
package application.views.screen.clans.show
{
    import application.events.ClanEvent;
    import application.models.clans.ClanMember;
    import application.views.AppView;
    import application.views.screen.common.frame.UserFrame;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.MouseEvent;
    import flash.events.SecurityErrorEvent;
    import flash.geom.Rectangle;
    import flash.net.URLRequest;
    import flash.system.LoaderContext;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.helpers.ImagesHelper;
    import framework.core.struct.view.View;

    public class ClanPosition extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKS:String = "CLAN_MEMEBER_FIELD";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanPosition(rank:int, member:ClanMember)
        {
            super();
            _rank = rank;
            _member = member;
            this.w = 480;
            this.h = 40;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addRank();
            _addAvatar();
            _bindReactions();
            _addLogin();
            _addItemsCount();
            _addGlory();
            addEventListener(MouseEvent.CLICK, _clickHandler);

            buttonMode = true;
            useHandCursor = true;
        }

        override public function destroy():void
        {
            if (_avatarLoader) {
                _avatarLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, _onAvatarLoaded);
                _avatarLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
                _avatarLoader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);
            }

            if (_avatarContainer && !_avatarContainer.destroyed) {
                _avatarContainer.destroy();
            }

            super.destroy();
        }

        private function _clickHandler(event:MouseEvent):void
        {
            var e:ClanEvent = new ClanEvent(ClanEvent.SELECT, true);
            e.user_id = _member.user_id.value;
            e.soc_net_id = _member.soc_net_id.value;

            dispatchEvent(e);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _back:Bitmap;
        private var _rect:Rectangle;
        private var _rank:int;
        private var _avatar:Bitmap;
        private var _member:ClanMember;
        private var _avatarLoader:Loader;
        private var _avatarContainer:View;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            _back = super.source.getBitmap(BACKS);
            _rect = new Rectangle(0, 0, this.w, this.h);
            _back.scrollRect = _rect;
            addChild(_back);
        }

        private function _addRank():void
        {
            var txtPosition:ShadowAppText = new ShadowAppText(0x5f3933);
            txtPosition.autoSize = TextFieldAutoSize.NONE;
            txtPosition.width = 50;
            txtPosition.height = 30;
            txtPosition.align = TextFormatAlign.CENTER;
            txtPosition.size = 14;
            txtPosition.bold = true;
            addChild(txtPosition);
            txtPosition.text = _rank.toString();
            txtPosition.x = 2;
            txtPosition.y = 7;
        }

        private function _addAvatar():void
        {
            var icon:Bitmap = super.source.getBitmap(UserFrame.DEFAULT_ICON, true);
            var data:BitmapData = ImagesHelper.matrixResize(icon, 40, 40, true);
            _avatar = new Bitmap(data);
            addChild(_avatar);

            _avatar.x = 45;
            _avatar.y = 0;

            _avatarLoader = new Loader();
            _avatarLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, _onAvatarLoaded);
            _avatarLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
            _avatarLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);

            var request:URLRequest = new URLRequest(_member.picture.value);
            var context:LoaderContext = new LoaderContext(true);
            _avatarLoader.load(request, context);
        }

        private function _onAvatarLoaded(e:Event):void
        {
            _avatarContainer = new View();
            addChild(_avatarContainer);

            _avatarContainer.addChild(_avatarLoader);
            _avatarContainer.width = 34;
            _avatarContainer.height = 34;
            _avatarContainer.x = 45;
            _avatarContainer.y = 3;

            _clearLoader();
        }

        private function _onAvatarError(e:Event):void
        {
            _clearLoader();
        }

        private function _clearLoader():void
        {
            _avatarLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, _onAvatarLoaded);
            _avatarLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
            _avatarLoader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);
        }

        private function _addLogin():void
        {
            var login:ClanMemberLogin = new ClanMemberLogin(_member.login.value, _member.is_leader.value);
            addChild(login);
            login.x = 85;
            login.y = 6;
        }

        private function _addItemsCount():void
        {
            var txtItemsCount:AppTextField = new AppTextField();
            txtItemsCount.color = 0x3c2415;
            txtItemsCount.size = 14;
            txtItemsCount.width = 60;
            txtItemsCount.align = TextFormatAlign.CENTER;
            txtItemsCount.bold = true;
            addChild(txtItemsCount);
            txtItemsCount.text = _member.items_given.value.toString()
            txtItemsCount.x = 283;
            txtItemsCount.y = 6;
        }

        private function _addGlory():void
        {
            var txtGlory:ShadowAppText = new ShadowAppText(0x5f3933);
            txtGlory.autoSize = TextFieldAutoSize.NONE;
            txtGlory.width = 60;
            txtGlory.height = 30;
            txtGlory.align = TextFormatAlign.RIGHT;
            txtGlory.size = 14;
            txtGlory.bold = true;
            addChild(txtGlory);
            txtGlory.text = _member.season_glory.value.toString();
            txtGlory.x = 350;
            txtGlory.y = 7;
        }

        private function _bindReactions():void
        {
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            _rect.y = this.h;
            _back.scrollRect = _rect;
        }

        private function _outHandler(event:MouseEvent):void
        {
            _rect.y = 0;
            _back.scrollRect = _rect;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
