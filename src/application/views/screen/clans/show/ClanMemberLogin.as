/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.01.14
 * Time: 19:26
 */
package application.views.screen.clans.show
{
    import application.views.TextFactory;
    import application.views.text.AppTextField;

    import flash.text.TextFieldAutoSize;

    import framework.core.struct.view.View;

    public class ClanMemberLogin extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanMemberLogin(login:String, lead:Boolean = false)
        {
            super();
            _login = login;
            _lead = lead;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            var html:String = "<font>" + _login + "</font>";
            if (_lead)
                html += "  <font color='#be1e2d'>" + (TextFactory.instance.getLabel('lead_label')) + "</font>";

            var txtUserName:AppTextField = new AppTextField();
            txtUserName.color = 0x3c2415;
            txtUserName.size = 14;
            txtUserName.autoSize = TextFieldAutoSize.LEFT;
            txtUserName.bold = true;
            addChild(txtUserName);
            txtUserName.htmlText = html;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _login:String;
        private var _lead:Boolean;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
