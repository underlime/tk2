/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.01.14
 * Time: 18:44
 */
package application.views.screen.clans.show
{
    import application.common.Colors;
    import application.views.AppView;
    import application.views.text.AppTextField;

    import flash.text.TextFormatAlign;

    public class ClanKeyValue extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanKeyValue(key:String, value:String)
        {
            super();
            _key = key;
            _value = value;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addKey();
            _addValue();
        }

        private function _addKey():void
        {
            var txtKey:AppTextField = new AppTextField();
            txtKey.color = Colors.LIGHT_BROWN;
            txtKey.width = 174;
            txtKey.size = 12;
            addChild(txtKey);
            txtKey.text = _key;
        }

        private function _addValue():void
        {
            var txtValue:AppTextField = new AppTextField();
            txtValue.color = Colors.DARK_BROWN;
            txtValue.size = 14;
            txtValue.bold = true
            txtValue.width = 174;
            txtValue.align = TextFormatAlign.RIGHT;
            addChild(txtValue);
            txtValue.text = _value;
            txtValue.y = -2;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _key:String;
        private var _value:String;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
