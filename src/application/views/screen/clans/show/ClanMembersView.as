/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.01.14
 * Time: 18:11
 */
package application.views.screen.clans.show
{
    import application.models.clans.ClanMember;
    import application.models.clans.ClanMembers;
    import application.views.AppView;
    import application.views.screen.common.scroll.ScrollObserver;
    import application.views.screen.common.scroll.Scrollbar;

    import flash.geom.Rectangle;

    public class ClanMembersView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanMembersView(clanMembers:ClanMembers)
        {
            super();
            _clanMembers = clanMembers;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addHeader();
            _addTable();
            if (_clanMembers.length > 5)
                _addScrollBar();
        }

        override public function destroy():void
        {
            if (_scrollObserver && !_scrollObserver.destroyed)
                _scrollObserver.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _scrollbar:Scrollbar;
        private var _clanCont:AppView;
        private var _scrollObserver:ScrollObserver;

        private var _clanMembers:ClanMembers;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHeader():void
        {
            addChild(new ClanMembersHeader());
        }

        private function _addTable():void
        {
            _clanCont = new AppView();
            var list:Array = _clanMembers.getMembersByGlory();
            var dy:int = 42;
            var cy:int = 0;

            for (var i:int = 0; i < list.length; i++) {
                var member:ClanMember = list[i];
                var position:ClanPosition = new ClanPosition(i + 1, member);
                position.y = cy;
                _clanCont.addChild(position);
                cy += dy;
            }
            addChild(_clanCont);
            _clanCont.y = 20;
        }

        private function _addScrollBar():void
        {
            _scrollbar = new Scrollbar(239);
            addChild(_scrollbar);
            _scrollbar.x = 420;

            _clanCont.$drawBackground(0);
            _scrollObserver = new ScrollObserver(_clanCont, _scrollbar, new Rectangle(0, 0, 420, 207));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
