/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.01.14
 * Time: 18:14
 */
package application.views.screen.clans.show
{
    import application.common.Colors;
    import application.views.TextFactory;
    import application.views.text.AppTextField;

    import framework.core.struct.view.View;

    public class ClanMembersHeader extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanMembersHeader()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            var l10n:TextFactory = TextFactory.instance;

            var placeField:AppTextField = _createField(l10n.getLabel('place_label'));
            placeField.x = 0;

            var memberField:AppTextField = _createField(l10n.getLabel('member_label'));
            memberField.x = 45;

            var passField:AppTextField = _createField(l10n.getLabel('send_items_label'));
            passField.x = 265;

            var gloryField:AppTextField = _createField(l10n.getLabel('glory_label'));
            gloryField.x = 377;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _createField(label:String):AppTextField
        {
            var txt:AppTextField = new AppTextField();
            txt.color = Colors.LIGHT_BROWN;
            txt.size = 12;
            addChild(txt);
            txt.text = label;

            return txt;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
