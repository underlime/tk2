/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.01.14
 * Time: 18:20
 */
package application.views.screen.clans.show
{
    import application.views.AppView;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.text.TextFormatAlign;

    public class ClanName extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "CLAN_NAME";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanName(title:String)
        {
            super();
            _title = title;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBack();
            _addLabel();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _title:String;
        private var _back:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            _back = super.source.getBitmap(BACK);
            addChild(_back);
        }

        private function _addLabel():void
        {
            var txtLabel:AppTextField = new AppTextField();
            txtLabel.size = 12;
            txtLabel.bold = true;
            txtLabel.color = 0x3c2415;
            txtLabel.width = _back.width;
            txtLabel.height = _back.height;
            txtLabel.align = TextFormatAlign.CENTER;
            addChild(txtLabel);
            txtLabel.text = _title;
            txtLabel.x = _back.x;
            txtLabel.y = _back.y + _back.height / 2 - txtLabel.textHeight / 2 - 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
