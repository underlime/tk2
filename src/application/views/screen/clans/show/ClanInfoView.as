/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.01.14
 * Time: 18:07
 */
package application.views.screen.clans.show
{
    import application.models.ModelData;
    import application.models.clans.ClanInfo;
    import application.models.user.User;
    import application.views.AppView;
    import application.views.screen.achievements.LockBlock;

    import flash.display.Bitmap;

    import framework.core.struct.data.ModelsRegistry;

    public class ClanInfoView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SEPARATOR:String = "CLAN_SEPARATOR";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanInfoView(clanInfo:ClanInfo)
        {
            super();
            _clanInfo = clanInfo;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addClanLogo();
            _addClanName();
            _addClanStats();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _clanInfo:ClanInfo;
        private var _user:User = ModelsRegistry.getModel(ModelData.USER) as User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var separator:Bitmap = super.source.getBitmap(SEPARATOR);
            addChild(separator);
            separator.x = 239;
            separator.y = 219;
        }

        private function _addClanLogo():void
        {
            var logo:Bitmap = ClanLogoHelper.getLogoById(_clanInfo.picture.value);
            var block:LockBlock = new LockBlock(logo);
            addChild(block);
            block.x = 105;
            block.y = 223;
        }

        private function _addClanName():void
        {
            var clanName:ClanName = new ClanName(_clanInfo.clan_name.value);
            addChild(clanName);
            clanName.x = 53;
            clanName.y = 300;
        }

        private function _addClanStats():void
        {
            var obj:Object = {
                "Участники": _clanInfo.clan_members.length.toString() + "/" + _user.gameConstants.clan_max_members.value.toString(),
                "Побед": _clanInfo.victories.value,
                "Слава": _clanInfo.glory.value
            };
            if (_clanInfo.previous_place_in_top.value > 0) {
                obj['Предыдущее место в топе'] = _clanInfo.previous_place_in_top.value;
            }
            var table:ClansKeyValueTable = new ClansKeyValueTable(obj);

            addChild(table);
            table.x = 60;
            table.y = 364;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
