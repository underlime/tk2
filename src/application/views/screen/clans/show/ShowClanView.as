/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.01.14
 * Time: 18:34
 */
package application.views.screen.clans.show
{
    import application.events.ClanEvent;
    import application.events.InfoEvent;
    import application.models.ModelData;
    import application.models.clans.ClanInfo;
    import application.models.inventory.Inventory;
    import application.models.market.Item;
    import application.models.user.User;
    import application.service.items.ItemsSelector;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.popup.ConfirmWindow;
    import application.views.screen.clans.ClanUserRole;
    import application.views.screen.common.buttons.RejectButton;
    import application.views.screen.inventory.context.ClanAdminContextMenu;
    import application.views.screen.inventory.context.ClanMemberContext;
    import application.views.screen.inventory.context.ContextMenu;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.utils.DisplayObjectUtils;

    public class ShowClanView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShowClanView(clanInfo:ClanInfo, canRequest:Boolean, localRole:ClanUserRole)
        {
            super();
            _clanInfo = clanInfo;
            _canRequest = canRequest;
            _localRole = localRole;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _layout = new ShowClanLayout(_clanInfo);
            addChild(_layout);

            if (_canRequest)
                _addRequestButton();
            _addContext();
        }

        override public function destroy():void
        {
            _layout.destroy();
            if (_itemSelector && !_itemSelector.destroyed)
                _itemSelector.destroy();
            if (_requestButton)
                _requestButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function rebuild():void
        {
            _layout.destroy();
            _layout = new ShowClanLayout(_clanInfo);
            addChild(_layout);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _layout:ShowClanLayout;
        private var _clanInfo:ClanInfo;
        private var _canRequest:Boolean;
        private var _requestButton:RejectButton;

        private var _context:ContextMenu;
        private var _user_id:int = 0;
        private var _soc_net_id:String = "";
        private var _localRole:ClanUserRole;

        private var _user:User = ModelsRegistry.getModel(ModelData.USER) as User;
        private var _inventory:Inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;
        private var _itemSelector:ItemsSelector;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addRequestButton():void
        {
            _requestButton = new RejectButton(TextFactory.instance.getLabel('send_clan_request'));
            _requestButton.w = 223;
            _requestButton.x = 490;
            _requestButton.y = 154;
            addChild(_requestButton);

            _requestButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _sendRequest():void
        {
            dispatchEvent(new ClanEvent(ClanEvent.REQUEST));
            _requestButton.destroy();
        }

        private function _addContext():void
        {
            _layout.addEventListener(ClanEvent.SELECT, _selectHandler);
            if (_localRole != ClanUserRole.GUEST) {
                if (_localRole == ClanUserRole.ADMIN) {
                    _context = new ClanAdminContextMenu(_getInfo, _sendItem, _deleteFromClan);
                } else {
                    _context = new ClanMemberContext(_getInfo, _sendItem);
                }

                addChild(_context);
                _context.hide();
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            var confirm:ConfirmWindow = new ConfirmWindow(TextFactory.instance.getLabel('send_req_confirm'), _sendRequest);
            addChild(confirm);
        }

        private function _selectHandler(event:ClanEvent):void
        {
            if (_localRole == ClanUserRole.GUEST) {
                _soc_net_id = event.soc_net_id;
                _getInfo();
            } else
                if (event.user_id != _user.userInfo.user_id.value) {
                    _user_id = event.user_id;
                    _soc_net_id = event.soc_net_id;
                    _context.x = mouseX;
                    _context.y = mouseY;
                    _context.show();
                    DisplayObjectUtils.moveIntoParent(_context, this);
                }
        }

        private function _getInfo():void
        {
            var event:InfoEvent = new InfoEvent(InfoEvent.GET_INFO);
            event.soc_net_id = _soc_net_id;
            super.sound.playUISound(UISound.CLICK);
            dispatchEvent(event);
        }

        private function _sendItem():void
        {
            if (_itemSelector && !_itemSelector.destroyed)
                _itemSelector.destroy();
            var items:Array = _inventory.getItems();
            if (items.length > 0) {
                _itemSelector = new ItemsSelector(items);
                _itemSelector.addEventListener(Event.SELECT, _itemSelectHandler);
                addChild(_itemSelector);
            } else {
                super.alert(
                        TextFactory.instance.getLabel('item_send_refuse'),
                        TextFactory.instance.getLabel('item_send_refuse_msg')
                );
            }
        }

        private function _itemSelectHandler(e:Event):void
        {
            var item:Item = _itemSelector.item;
            _itemSelector.destroy();

            var event:ClanEvent = new ClanEvent(ClanEvent.SEND_ITEM);
            event.user_id = _user_id;
            event.item_id = item.item_id.value;
            dispatchEvent(event);
        }

        private function _deleteFromClan():void
        {
            var event:ClanEvent = new ClanEvent(ClanEvent.DELETE);
            event.user_id = _user_id;
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
