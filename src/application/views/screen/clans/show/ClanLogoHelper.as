/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.01.14
 * Time: 18:14
 */
package application.views.screen.clans.show
{
    import flash.display.Bitmap;

    import framework.core.helpers.MathHelper;
    import framework.core.tools.SourceManager;

    public class ClanLogoHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "CLAN_SIGNS_";

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getLogo():Bitmap
        {
            return SourceManager.instance.getBitmap(BASIS + MathHelper.random(2, 41).toString());
        }

        public static function getLogoById(id:int):Bitmap
        {
            var logoID:int = MathHelper.clamp(id, 2, 41);
            return SourceManager.instance.getBitmap(BASIS + logoID.toString());
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
