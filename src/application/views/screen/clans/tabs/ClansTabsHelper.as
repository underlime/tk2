/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.01.14
 * Time: 12:54
 */
package application.views.screen.clans.tabs
{
    import application.views.screen.clans.*;
    import application.views.screen.common.tabs.TabFactory;
    import application.views.screen.common.tabs.TabType;

    import framework.core.display.tabs.ITab;

    public class ClansTabsHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getTabs(role:ClanUserRole):Vector.<ITab>
        {
            switch (role) {
                case ClanUserRole.ADMIN:
                    return _getAdminTabs();
                case ClanUserRole.MEMBER:
                    return _getMemberTabs();
                case ClanUserRole.GUEST:
                    return _getGuestTabs();
            }

            return _getGuestTabs();
        }

        private static function _getAdminTabs():Vector.<ITab>
        {
            var tabs:Vector.<ITab> = new Vector.<ITab>();
            tabs.push(
                    TabFactory.getTab(TabType.MY_CLAN),
                    TabFactory.getTab(TabType.EDIT_CLAN),
                    TabFactory.getTab(TabType.CLAN_LOG)
            );
            return tabs;
        }

        private static function _getMemberTabs():Vector.<ITab>
        {
            var tabs:Vector.<ITab> = new Vector.<ITab>();
            tabs.push(
                    TabFactory.getTab(TabType.MY_CLAN),
                    TabFactory.getTab(TabType.EDIT_CLAN)
            );
            return tabs;
        }

        private static function _getGuestTabs():Vector.<ITab>
        {
            var tabs:Vector.<ITab> = new Vector.<ITab>();
            tabs.push(
                    TabFactory.getTab(TabType.EDIT_CLAN)
            );
            return tabs;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
