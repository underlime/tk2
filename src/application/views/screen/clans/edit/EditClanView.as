/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.01.14
 * Time: 11:13
 */
package application.views.screen.clans.edit
{
    import application.events.ClanEvent;
    import application.models.clans.ClanInfo;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.screen.clans.ClanUserRole;
    import application.views.screen.clans.show.ClanInfoView;
    import application.views.screen.common.buttons.RejectButton;

    import flash.events.MouseEvent;

    public class EditClanView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EditClanView(clanInfo:ClanInfo, clanRole:ClanUserRole)
        {
            super();
            _clanInfo = clanInfo;
            _clanRole = clanRole;

        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addClanInfo();
            _addEditButton();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _clanInfoView:ClanInfoView;
        private var _editButton:RejectButton;

        private var _clanInfo:ClanInfo;
        private var _clanRole:ClanUserRole;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addClanInfo():void
        {
            _clanInfoView = new ClanInfoView(_clanInfo);
            addChild(_clanInfoView);
        }

        private function _addEditButton():void
        {
            var buttonLabel:String = _clanRole == ClanUserRole.ADMIN ? TextFactory.instance.getLabel("delete_clan_label") : TextFactory.instance.getLabel("leave_clan_label");
            _editButton = new RejectButton(buttonLabel);
            _editButton.w = 266;
            addChild(_editButton);
            _editButton.x = 344;
            _editButton.y = 316;

            _editButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            dispatchEvent(new ClanEvent(ClanEvent.DELETE));
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
