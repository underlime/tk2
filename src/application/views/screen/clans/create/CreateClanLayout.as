/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.01.14
 * Time: 20:45
 */
package application.views.screen.clans.create
{
    import application.views.TextFactory;
    import application.views.screen.office.ChangeLoginLayout;

    public class CreateClanLayout extends ChangeLoginLayout
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CreateClanLayout(price:int)
        {
            super(price, TextFactory.instance.getLabel("clan_label"));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _headerLabel = TextFactory.instance.getLabel("creation_clan_label");
            _descriptionLabel = TextFactory.instance.getLabel("clan_create_description");
            _payButtonLabel = TextFactory.instance.getLabel('create_label');
            super.render();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
