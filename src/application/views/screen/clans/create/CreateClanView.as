/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.01.14
 * Time: 16:12
 */
package application.views.screen.clans.create
{
    import application.views.AppView;

    public class CreateClanView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CreateClanView(price:int)
        {
            super();
            _price = price;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _layout = new CreateClanLayout(_price);
            _layout.y = 107;
            addChild(_layout);
        }

        override public function destroy():void
        {
            _layout.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _layout:CreateClanLayout;
        private var _price:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function loginDeny():void
        {
            _layout.loginDeny();
        }

        public function loginAllow():void
        {
            _layout.loginAllow();
        }
    }
}
