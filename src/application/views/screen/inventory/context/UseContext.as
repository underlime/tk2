/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.07.13
 * Time: 10:54
 */
package application.views.screen.inventory.context
{
    import application.views.screen.inventory.context.buttons.SellButton;
    import application.views.screen.inventory.context.buttons.UseButton;

    public class UseContext extends ContextMenu
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UseContext(useMethod:Function, sellMethod:Function)
        {
            super();
            register(new UseButton(), useMethod);
            register(new SellButton(), sellMethod);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
