/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.07.13
 * Time: 10:52
 */
package application.views.screen.inventory.context
{
    import application.views.screen.inventory.context.buttons.EquipButton;
    import application.views.screen.inventory.context.buttons.SellButton;
    import application.views.screen.inventory.context.buttons.SocialContextButton;

    public class EquipContext extends ContextMenu
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EquipContext(equipMethod:Function, sellMethod:Function, socialMethod:Function = null)
        {
            super();

            register(new EquipButton(), equipMethod);
            register(new SocialContextButton(), socialMethod);
            register(new SellButton(), sellMethod);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
