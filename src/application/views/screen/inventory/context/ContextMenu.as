/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.07.13
 * Time: 16:47
 */
package application.views.screen.inventory.context
{
    import application.views.screen.inventory.context.buttons.ContextButton;

    import com.greensock.TweenLite;

    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.utils.Dictionary;
    import flash.utils.clearTimeout;
    import flash.utils.setTimeout;

    import framework.core.struct.view.View;

    public class ContextMenu extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ContextMenu()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addShapes();
            _addButtons();
            _updateBackground();
            _bindReactions();
        }

        override public function destroy():void
        {
            _backShape.graphics.clear();
            _frontShape.graphics.clear();
            var count:int = _buttons.length;
            for (var i:int = 0; i < count; i++) {
                _buttons[i].removeEventListener(MouseEvent.CLICK, _clickHandler);
                _buttons[i].destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function register(button:ContextButton, callback:Function):void
        {
            _buttons.push(button);
            _callbacks[button] = callback;
        }

        public function show():void
        {
            this.visible = true;
            _hidden = false;

            _bindHoverReactions();
            _setTimeout();
        }

        public function hide():void
        {
            this.visible = false;
            this.alpha = 1;
            _unbindHoverReactions();
            _hidden = true;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _buttons:Vector.<ContextButton> = new Vector.<ContextButton>();

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _callbacks:Dictionary = new Dictionary(true);

        private var _backShape:Shape = new Shape();
        private var _frontShape:Shape = new Shape();

        private var _frontMargin:Number = 6;
        private var _backColor:uint = 0x754c29;
        private var _frontColor:uint = 0xddc078;

        private var _y0:int = 6;
        private var _dy:int = 4;

        private var _hidden:Boolean = false;
        private var _handler:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addShapes():void
        {
            addChild(_backShape);
            addChild(_frontShape);
        }

        private function _addButtons():void
        {
            var count:int = _buttons.length;
            var cy:int = _y0;

            for (var i:int = 0; i < count; i++) {
                addChild(_buttons[i]);
                _buttons[i].x = _frontMargin;
                _buttons[i].y = cy;
                cy += ContextButton.BUTTON_HEIGHT + _dy;

            }
        }

        private function _updateBackground():void
        {
            var totalWidth:Number = ContextButton.BUTTON_WIDTH + _frontMargin * 2;
            var totalHeight:Number = _buttons.length * ContextButton.BUTTON_HEIGHT + _frontMargin * 2 + (_buttons.length - 1) * _dy;

            _backShape.graphics.clear();
            _backShape.graphics.beginFill(_backColor);
            _backShape.graphics.drawRect(0, 0, totalWidth, totalHeight);
            _backShape.graphics.endFill();

            _frontShape.graphics.clear();
            _frontShape.graphics.beginFill(_frontColor);

            _frontShape.graphics.drawRect(2, 2, totalWidth - 4, totalHeight - 4);
            _frontShape.graphics.endFill();
        }

        private function _bindReactions():void
        {
            var count:int = _buttons.length;
            for (var i:int = 0; i < count; i++) {
                _buttons[i].addEventListener(MouseEvent.CLICK, _clickHandler);
            }
        }

        private function _bindHoverReactions():void
        {
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _unbindHoverReactions():void
        {
            clearTimeout(_handler);
            TweenLite.killTweensOf(this);
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _hideContext():void
        {
            clearTimeout(_handler);
            TweenLite.killTweensOf(this);
            var params:Object = {
                "alpha": 0,
                "onComplete": _tweenComplete
            };

            TweenLite.to(this, .3, params);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            var button:ContextButton = event.currentTarget as ContextButton;
            if (_callbacks[button] && _callbacks[button] is Function) {
                (_callbacks[button] as Function).call(this);
                this.hide();
            }
        }

        private function _overHandler(event:MouseEvent):void
        {
            clearTimeout(_handler);
            TweenLite.killTweensOf(this);
            this.alpha = 1;
        }

        private function _outHandler(event:MouseEvent):void
        {
            _setTimeout();
        }

        private function _setTimeout():void
        {
            clearTimeout(_handler);
            _handler = setTimeout(_hideContext, 1000);

        }

        private function _tweenComplete():void
        {
            this.hide();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get hidden():Boolean
        {
            return _hidden;
        }
    }
}
