/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.03.14
 * Time: 12:24
 */
package application.views.screen.inventory.context
{
    import application.views.screen.inventory.context.buttons.AddItemButton;
    import application.views.screen.inventory.context.buttons.ClanRemoveButton;

    public class ClanAdminContextMenu extends ContextMenu
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanAdminContextMenu(infoMethod:Function, addItem:Function, removeMethod:Function)
        {
            super();
            register(new ClanShowUserButton(), infoMethod);
            register(new AddItemButton(), addItem);
            register(new ClanRemoveButton(), removeMethod);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
