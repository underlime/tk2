/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.03.14
 * Time: 12:46
 */
package application.views.screen.inventory.context
{
    import application.views.TextFactory;
    import application.views.screen.inventory.context.buttons.ContextButton;

    public class ClanShowUserButton extends ContextButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanShowUserButton()
        {
            super(TextFactory.instance.getLabel('clan_user_info'), 360);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
