/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.07.13
 * Time: 20:57
 */
package application.views.screen.inventory.context.buttons
{
    import application.views.screen.common.text.ShadowAppText;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.display.Graphics;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.core.struct.view.View;

    public class ContextButton extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ICON_SOURCE:String = "CONTEXT_ICONS";

        public static const ICON_WIDTH:int = 40;
        public static const ICON_HEIGHT:int = 40;

        public static const BUTTON_WIDTH:int = 198;
        public static const BUTTON_HEIGHT:int = 40;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ContextButton(label:String, offset:int)
        {
            super();
            _offset = offset;
            _label = label;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _updateBackground();
            _addIcon();
            _addLabel();
            _bindReactions();

            this.scrollRect = new Rectangle(0, 0, _width, _height);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _width:Number = BUTTON_WIDTH;
        private var _height:Number = BUTTON_HEIGHT;

        private var _frontColor:uint = 0xefdf9a;
        private var _overColor:uint = 0xfffcae;
        private var _currentColor:uint = _frontColor;

        private var _icon:Bitmap;
        private var _label:String;

        private var _txtLabel:AppTextField;
        private var _offset:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateBackground():void
        {
            var gr:Graphics = this.graphics;
            gr.clear();
            gr.beginFill(_currentColor);
            gr.drawRect(0, 0, _width, _height);
            gr.endFill();
        }

        private function _addIcon():void
        {
            _icon = super.source.getBitmap(ICON_SOURCE);
            var delta:Number = _height / 2 - _icon.width / 2;
            _icon.scrollRect = new Rectangle(0, _offset, ICON_WIDTH, ICON_HEIGHT);
            _icon.x = delta;
            _icon.y = delta;
            addChild(_icon);
        }

        private function _addLabel():void
        {
            _txtLabel = new ShadowAppText(0x653b28);
            _txtLabel.color = 0xffffff;
            _txtLabel.size = 14;
            _txtLabel.bold = true;
            addChild(_txtLabel);
            _txtLabel.text = _label;

            _txtLabel.y = _height / 2 - _txtLabel.textHeight / 2 - 3;
            _txtLabel.x = _icon.x + _icon.width + 5;
        }

        private function _bindReactions():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _overHandler(event:MouseEvent):void
        {
            _currentColor = _overColor;
            _updateBackground();
        }

        private function _outHandler(event:MouseEvent):void
        {
            _currentColor = _frontColor;
            _updateBackground();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
