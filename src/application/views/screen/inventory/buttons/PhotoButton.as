/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.06.13
 * Time: 12:06
 */
package application.views.screen.inventory.buttons
{
    import application.events.ApplicationEvent;

    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    import framework.core.struct.view.View;
    import framework.core.utils.DestroyUtils;
    import framework.core.utils.DisplayObjectUtils;

    public class PhotoButton extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const PHOTO_BUTTON:String = "HOME_PhotoButton";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PhotoButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupButton();
            _bindReactions();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);

            DisplayObjectUtils.removeChild(_photoButton);
            DestroyUtils.destroy(_photoButton);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _photoButton:MovieClip;
        private var _hoverState:int = 2;
        private var _outState:int = 16;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupButton():void
        {
            _photoButton = super.source.getMovieClip(PHOTO_BUTTON);
            _photoButton.gotoAndStop(1);

            addChild(_photoButton);
        }

        private function _bindReactions():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;

            addEventListener(MouseEvent.CLICK, _clickHandler);
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.GENERATE_AVATAR, true));
        }

        private function _overHandler(event:MouseEvent):void
        {
            _photoButton.gotoAndPlay(_hoverState);
        }

        private function _outHandler(event:MouseEvent):void
        {
            _photoButton.gotoAndPlay(_outState);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
