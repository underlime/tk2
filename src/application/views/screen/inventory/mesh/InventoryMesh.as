/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.06.13
 * Time: 14:09
 */
package application.views.screen.inventory.mesh
{
    import application.models.inventory.InventoryItem;
    import application.views.screen.common.cell.Cell;
    import application.views.screen.common.cell.InventoryCell;
    import application.views.screen.common.mesh.Mesh;

    public class InventoryMesh extends Mesh
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InventoryMesh(cells:Vector.<Cell> = null)
        {
            super(cells);
            _x0 = 0;
            _y0 = 0;
            _dx = 85;
            _dy = 85;
            _lines = 2;
            _cols = 4;
            _limit = 8;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function get item():InventoryItem
        {
            if (_selectedCell is InventoryCell)
                return (_selectedCell as InventoryCell).item;
            else
                return null;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
