/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.06.13
 * Time: 12:34
 */
package application.views.screen.inventory
{
    import application.views.AppView;
    import application.views.screen.common.navigation.Navigation;
    import application.views.screen.inventory.buttons.ConfirmButton;
    import application.views.screen.inventory.mesh.InventoryMesh;

    public class InventoryLayout extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InventoryLayout()
        {
            super();
            this.y = 107;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setConfirmButton();
            _setupMesh();
            _addNavigation();
        }

        override public function destroy():void
        {
            _confirmButton.destroy();
            _navigation.destroy();
            _confirmButton.destroy();
            _mesh.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _confirmButton:ConfirmButton;
        private var _mesh:InventoryMesh;

        private var _navigation:Navigation;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setConfirmButton():void
        {
            _confirmButton = new ConfirmButton()
            _confirmButton.x = 275;
            _confirmButton.y = 310;
            addChild(_confirmButton);
        }

        private function _setupMesh():void
        {
            _mesh = new InventoryMesh();
            _mesh.x = 70;
            _mesh.y = 131;
            addChild(_mesh);
        }

        private function _addNavigation():void
        {
            _navigation = new Navigation();
            _navigation.arrowMargin = 3;
            addChild(_navigation);
            _navigation.y = 310;
            _navigation.x = 70;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get mesh():InventoryMesh
        {
            return _mesh;
        }

        public function get confirmButton():ConfirmButton
        {
            return _confirmButton;
        }

        public function get navigation():Navigation
        {
            return _navigation;
        }
    }
}
