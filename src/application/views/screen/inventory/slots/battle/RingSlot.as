/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.06.13
 * Time: 11:44
 */
package application.views.screen.inventory.slots.battle
{
    import application.views.screen.common.tabs.FilterIcons;
    import application.views.screen.common.tabs.FilterType;
    import application.views.screen.inventory.slots.base.EquippingSlot;

    public class RingSlot extends EquippingSlot
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RingSlot()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setupIcon():void
        {
            _icon = FilterIcons.getIcon(FilterType.RINGS);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
