/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.09.13
 * Time: 13:59
 */
package application.views.screen.inventory.slots.user
{
    import application.models.ModelData;
    import application.models.user.User;
    import application.views.screen.inventory.slots.*;

    import framework.core.struct.data.ModelsRegistry;

    public class UserInventorySlotsPanel extends InventorySlotsPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserInventorySlotsPanel()
        {
            super();
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setupRings():void
        {
            _ringsPanel = new UserRingsPanel(_user.userLevelParams.ring_slots_avaible.value);
            _ringsPanel.x = 9;
            _ringsPanel.y = 113;
            addChild(_ringsPanel);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
