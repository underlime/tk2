/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.09.13
 * Time: 14:21
 */
package application.views.screen.inventory.slots.user
{
    import application.views.screen.inventory.slots.*;

    public class UserRingsPanel extends RingSlotPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserRingsPanel(availableSlots:int = 1)
        {
            super();
            _availableSlots = availableSlots;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _setupRings():void
        {
            _ringSlot1 = new UserRingSlot();
            addChild(_ringSlot1);

            _ringSlot2 = new UserRingSlot();
            addChild(_ringSlot2);

            _ringSlot2.x = 208;

            super._slots.push(
                    _ringSlot1,
                    _ringSlot2
            );

            var lock:int = _slots.length - _availableSlots;
            this.lockSlots(lock);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _availableSlots:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
