/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.09.13
 * Time: 16:17
 */
package application.views.screen.inventory.slots.user
{
    import application.models.ModelData;
    import application.models.user.User;
    import application.views.screen.inventory.slots.ConsumeSlotPanel;

    import framework.core.struct.data.ModelsRegistry;

    public class UserConsumeSlots extends ConsumeSlotPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserConsumeSlots()
        {
            super();
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _checkAvailableSlots();
        }

        override protected function _renderSlots():void
        {
            var slot:UserConsumeSlot;
            var cx:int = 0;

            for (var i:int = 0; i < _count; i++) {
                slot = new UserConsumeSlot();
                slot.x = cx;
                _slots.push(slot);

                cx += _dx;
                addChild(slot);
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _checkAvailableSlots():void
        {
            var available:int = _user.userLevelParams.items_slots_avaible.value;
            var total:int = _slots.length;
            var lock:int = total - available;
            if (lock > 0) {
                lockSlots(lock);
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
