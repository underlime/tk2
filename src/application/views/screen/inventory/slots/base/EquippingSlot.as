/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.06.13
 * Time: 12:02
 */
package application.views.screen.inventory.slots.base
{
    import application.events.SlotEvent;
    import application.models.inventory.InventoryItem;
    import application.sound.lib.UISound;
    import application.views.PopupManager;
    import application.views.map.layers.buttons.LevelPlusButton;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.buttons.AppSpriteButton;
    import application.views.screen.common.cell.CellHintView;
    import application.views.screen.common.cell.ForgeIcon;
    import application.views.screen.common.tabs.FilterIcons;
    import application.views.screen.inventory.slots.SlotHint;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    import framework.core.utils.Singleton;

    public class EquippingSlot extends AppSpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "SMALL_SLOT";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EquippingSlot()
        {
            super(SOURCE);
            _w = 52;
            _h = 52;

            _outPosition = 52;
            _overPosition = 104;
            _downPosition = 104;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _setupLock();
            _setupIcon();
            _addIcon();
        }

        override public function destroy():void
        {
            clear();
            if (_unlockButton && !_unlockButton.destroyed)
                _unlockButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function clear():void
        {
            if (_itemIcon && this.contains(_itemIcon))
                removeChild(_itemIcon);

            if (_levelIcon && this.contains(_levelIcon))
                removeChild(_levelIcon);

            if (_item) {
                _unbindCell();
                _item.temp_put.value--;
            }

            if (_hintView) {
                _hintView.destroy();
                _hintView = null;
            }


            _item = null;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _icon:Bitmap;
        protected var _lockIcon:Bitmap;

        protected var _canUnlock:Boolean = false;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupIcon():void
        {
            _icon = super.source.getBitmap(SlotsData.AMULET_SLOT);
        }

        protected function _setupLock():void
        {
            _lockIcon = super.source.getBitmap(SOURCE);
            _lockIcon.scrollRect = new Rectangle(0, 0, 52, 52);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _locked:Boolean = false;
        private var _item:InventoryItem;
        private var _itemIcon:Bitmap;
        private var _readonly:Boolean = false;

        private var _unlockButton:LevelPlusButton;
        private var _popupManager:PopupManager = Singleton.getClass(PopupManager);

        private var _hintView:SlotHint;
        private var _levelIcon:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHint():void
        {
            _hintView = new SlotHint(_item);
            addChild(_hintView);
            var cords:Point = localToGlobal(new Point(CellHintView.WIDTH - 400, super.h / 2 - _hintView.h / 2));
            removeChild(_hintView);
            _hintView.x = int(cords.x);
            _hintView.y = int(cords.y);
        }

        private function _addIcon():void
        {
            addChild(_icon);
            _icon.x = _w / 2 - FilterIcons.ICON_WIDTH / 2;
            _icon.y = _h / 2 - FilterIcons.ICON_HEIGHT / 2;
        }

        private function _setupLockIcon():void
        {
            clear();
            addChild(_lockIcon);
        }

        private function _removeLockIcon():void
        {
            if (_lockIcon && this.contains(_lockIcon))
                removeChild(_lockIcon);
        }

        private function _updateItem():void
        {
            if (_item) {
                _setupItemIcon();
                if (!_readonly)
                    _bindCell();
            }
        }

        private function _setupItemIcon():void
        {
            _itemIcon = super.source.getBitmap(CommonData.ICON_PREFIX + _item.picture.value, true);
            _itemIcon.width = 42;
            _itemIcon.height = 42;
            _itemIcon.x = 5.5;
            _itemIcon.y = 5.5;
            addChild(_itemIcon);

            if (_item.sharpening_level.value > 0) {
                _levelIcon = ForgeIcon.getIcon(_item.sharpening_level.value);
                _levelIcon.x = -9;
                _levelIcon.y = 35;
                addChild(_levelIcon);
            }
        }

        private function _bindCell():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;

            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _unbindCell():void
        {
            this.buttonMode = false;
            this.useHandCursor = false;

            removeEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _addUnlockButton():void
        {
            if (_unlockButton)
                _unlockButton.destroy();
            _unlockButton = new LevelPlusButton();
            addChild(_unlockButton);
            _unlockButton.x = 26 - _unlockButton.w / 2;
            _unlockButton.y = 26 - _unlockButton.h / 2;

            _unlockButton.addEventListener(MouseEvent.CLICK, _unlockClickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(e:MouseEvent):void
        {
            var event:SlotEvent = new SlotEvent(SlotEvent.CHANGE, true);
            event.item = _item;

            _popupManager.remove();
            clear();
            super.dispatchEvent(event);
        }

        private function _unlockClickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            super.dispatchEvent(new SlotEvent(SlotEvent.UNLOCK, true));
        }

        override protected function _onOverHandler(e:MouseEvent):void
        {
            super._onOverHandler(e);
            if (_item && _hintView)
                _popupManager.add(_hintView);
        }

        override protected function _onOutHandler(e:MouseEvent):void
        {
            super._onOutHandler(e);
            _popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get locked():Boolean
        {
            return _locked;
        }

        public function set locked(value:Boolean):void
        {
            _locked = value;

            if (_locked) {
                _setupLockIcon();
                if (_canUnlock)
                    _addUnlockButton();
            } else {
                _removeLockIcon();
            }

            if (_locked && _item) {
                var event:SlotEvent = new SlotEvent(SlotEvent.CHANGE, true);
                dispatchEvent(event);
            }
        }

        public function get item():InventoryItem
        {
            return _item;
        }

        public function set item(value:InventoryItem):void
        {
            if (!_locked) {
                clear();
                _item = value;
                _item.temp_put.value++;
                _updateItem();

                _addHint();

                var event:SlotEvent = new SlotEvent(SlotEvent.CHANGE, true);
                dispatchEvent(event);
            }
        }

        public function get empty():Boolean
        {
            return _item ? false : true;
        }

        public function get readonly():Boolean
        {
            return _readonly;
        }

        public function set readonly(value:Boolean):void
        {
            _readonly = value;
            if (_readonly)
                _unbindCell();
            else
                _bindCell();
        }
    }
}
