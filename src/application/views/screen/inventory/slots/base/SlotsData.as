/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.06.13
 * Time: 11:01
 */
package application.views.screen.inventory.slots.base
{
    public class SlotsData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BATTLE_SLOT:String = "HOME_BattleSlot";
        public static const SIMPLE_SLOT_LOCK:String = "HOME_BattleSlotLock";
        public static const WEAPON_SLOT:String = "HOME_SlotWeapon";
        public static const SHIELD_SLOT:String = "HOME_SlotShield";
        public static const RING_SLOT:String = "HOME_SlotRing";
        public static const MASK_SLOT:String = "HOME_SlotMask";
        public static const HAT_SLOT:String = "HOME_SlotHat";
        public static const COAT_SLOT:String = "HOME_SlotCoat";
        public static const BOOTS_SLOT:String = "HOME_SlotBoots";
        public static const ARMOR_SLOT:String = "HOME_SlotArmor";
        public static const AMULET_SLOT:String = "HOME_SlotAmulet";
        public static const CONSUME_SLOT:String = "HOME_BattleSlot";
        public static const MONSTER_SLOT:String = "HOME_SlotMoster";

        public static const SOCIAL_WEAPON_SLOT:String = "HOME_SocialSlotWeapon";
        public static const SOCIAL_SHIELD_SLOT:String = "HOME_SocialSlotShield";
        public static const SOCIAL_HAT_SLOT:String = "HOME_SocialSlotHelm";
        public static const SOCIAL_MASK_SLOT:String = "HOME_SocailSlotMask";
        public static const SOCIAL_COAT_SLOT:String = "HOME_SocialSlotCoat";

        public static const SOCIAL_LOCK_ICON:String = "HOME_SocialSlotLock";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
