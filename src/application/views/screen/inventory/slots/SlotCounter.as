/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.07.13
 * Time: 11:13
 */
package application.views.screen.inventory.slots
{
    import application.views.text.AppTextField;

    import flash.display.Shape;
    import flash.filters.DropShadowFilter;
    import flash.text.TextFieldAutoSize;

    import framework.core.struct.view.View;

    public class SlotCounter extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SlotCounter(count:int = 2)
        {
            super();
            _count = count;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupTextCount();
            _setupBackground();
        }

        override public function destroy():void
        {
            if (_txtCount && !_txtCount.destroyed)
                _txtCount.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _count:int = 2;
        private var _txtCount:AppTextField;

        private var _backShape:Shape = new Shape();
        private var _frontShape:Shape = new Shape();
        private var _backgroundColor:uint = 0xad1d25;
        private var _frontBorderColor:uint = 0xffffff;
        private var _margin:Number = 2;
        private var _radius:Number = 20;
        private var _frontBorderSize:Number = 2;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTextCount():void
        {
            _txtCount = new AppTextField();

            _txtCount.autoSize = TextFieldAutoSize.RIGHT;
            _txtCount.bold = true;
            _txtCount.size = 14;
            _txtCount.color = 0xffffff;
            _txtCount.filters = [new DropShadowFilter(1, 45, 0x000000, 1, 4, 4, 1, 3)];
            addChild(_txtCount);
            _txtCount.text = _count.toString();
        }

        private function _setupBackground():void
        {
            _txtCount.text = _count.toString();
            _updateBackground();
            _addBackground();
        }

        private function _updateBackground():void
        {
            var _w:Number = _txtCount.width;
            var _h:Number = _txtCount.height;

            _backShape.graphics.clear();
            _backShape.graphics.beginFill(this._backgroundColor);
            _backShape.graphics.drawRoundRect(0, 0, _w + _margin * 4, _h - _margin, _radius);
            _backShape.graphics.endFill();

            _frontShape.graphics.clear();
            _frontShape.graphics.beginFill(0x000000, 0);
            _frontShape.graphics.lineStyle(_frontBorderSize, _frontBorderColor);
            _frontShape.graphics.drawRoundRect(0, 0, _w + _margin * 4, _h - _margin, _radius);
            _frontShape.graphics.endFill();
        }

        private function _addBackground():void
        {
            addChild(_backShape);
            addChild(_frontShape);
            _txtCount.x = _margin * 2;
            _txtCount.y = ( -1) * _margin / 2;
            addChild(_txtCount);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set count(value:int):void
        {
            _count = value;
            _setupBackground();
        }
    }
}
