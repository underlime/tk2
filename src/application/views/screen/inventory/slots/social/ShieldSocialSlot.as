/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.07.13
 * Time: 12:09
 */
package application.views.screen.inventory.slots.social
{
    import application.views.screen.inventory.slots.base.EquippingSlot;
    import application.views.screen.inventory.slots.base.SlotsData;

    public class ShieldSocialSlot extends EquippingSlot
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShieldSocialSlot()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _setupIcon():void
        {
            _icon = super.source.getBitmap(SlotsData.SOCIAL_SHIELD_SLOT);
        }

        override protected function _setupLock():void
        {
            _lockIcon = super.source.getBitmap(SlotsData.SOCIAL_LOCK_ICON);
        }


        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
