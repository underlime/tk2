/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.07.13
 * Time: 12:11
 */
package application.views.screen.inventory.slots.social
{
    import application.views.screen.inventory.slots.base.EquippingSlot;
    import application.views.screen.inventory.slots.base.SlotsData;

    public class CoatSocialSlot extends EquippingSlot
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CoatSocialSlot()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _setupIcon():void
        {
            _icon = super.source.getBitmap(SlotsData.SOCIAL_COAT_SLOT);
        }

        override protected function _setupLock():void
        {
            _lockIcon = super.source.getBitmap(SlotsData.SOCIAL_LOCK_ICON);
        }


        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
