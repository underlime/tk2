/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 25.08.13
 * Time: 10:52
 */
package application.views.screen.inventory.slots.tabs
{
    import application.events.ApplicationEvent;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.RejectButton;

    import flash.events.MouseEvent;

    public class EquipButton extends RejectButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EquipButton()
        {
            super(TextFactory.instance.getLabel('equipment_label'));
            _w = 106;
            _fontSize = 14;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        override public function select():void
        {
            super.select();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TAB_CHOOSE, true));
        }

        private function _clickHandler(event:MouseEvent):void
        {
            select();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
