/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 25.08.13
 * Time: 10:47
 */
package application.views.screen.inventory.slots
{
    import application.views.screen.inventory.slots.tabs.SlotTabs;
    import application.views.screen.inventory.slots.user.UserConsumeSlots;
    import application.views.screen.inventory.slots.user.UserInventorySlotsPanel;

    import flash.events.Event;

    import framework.core.struct.view.View;

    public class SlotsView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SlotsView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addConsumeSlots();
            _addSlots();
            _addSocialSlots();
            _addTabs();
        }

        override public function destroy():void
        {
            _inventorySlots.destroy();
            _socialSlots.destroy();
            _consumeSlots.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _inventorySlots:UserInventorySlotsPanel;
        private var _consumeSlots:ConsumeSlotPanel;

        private var _socialSlots:SocialSlots;

        private var _tabs:SlotTabs;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addConsumeSlots():void
        {
            _consumeSlots = new UserConsumeSlots();
            addChild(_consumeSlots);
        }

        private function _addSlots():void
        {
            _inventorySlots = new UserInventorySlotsPanel();
            addChild(_inventorySlots);
            _inventorySlots.visible = false;
        }

        private function _addSocialSlots():void
        {
            _socialSlots = new SocialSlots();
            addChild(_socialSlots);
            _socialSlots.visible = false;
        }

        private function _addTabs():void
        {
            _tabs = new SlotTabs();
            addChild(_tabs);

            _tabs.x = 9;
            _tabs.y = 284;

            _tabs.addEventListener(Event.CHANGE, _tabChangeHandler);
            _tabs.select(0);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _tabChangeHandler(event:Event):void
        {
            var selectedTab:int = _tabs.selectedTab;
            if (selectedTab > 0) {
                _socialSlots.visible = true;
                _inventorySlots.visible = false;
            } else {
                _socialSlots.visible = false;
                _inventorySlots.visible = true;
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get inventorySlots():InventorySlotsPanel
        {
            return _inventorySlots;
        }

        public function get consumeSlots():ConsumeSlotPanel
        {
            return _consumeSlots;
        }

        public function get socialSlots():SocialSlots
        {
            return _socialSlots;
        }
    }
}
