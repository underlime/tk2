/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.06.13
 * Time: 12:35
 */
package application.views.screen.inventory.slots
{
    import application.common.ArrangementVariant;
    import application.models.inventory.InventoryItem;
    import application.views.screen.inventory.slots.battle.AmuletSlot;
    import application.views.screen.inventory.slots.battle.ArmorSlot;
    import application.views.screen.inventory.slots.battle.CoatSlot;
    import application.views.screen.inventory.slots.battle.HatSlot;
    import application.views.screen.inventory.slots.battle.MaskSlot;
    import application.views.screen.inventory.slots.battle.ShieldSlot;
    import application.views.screen.inventory.slots.battle.WeaponSlot;

    public class InventorySlotsPanel extends SlotsPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InventorySlotsPanel()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            _setupMethodsRelations();

            _weaponSlot = new WeaponSlot();
            _shieldSlot = new ShieldSlot();
            _hatSlot = new HatSlot();
            _maskSlot = new MaskSlot();
            _coatSlot = new CoatSlot();
            _armorSlot = new ArmorSlot();
            _amuletSlot = new AmuletSlot();

            super._slots.push(
                    _weaponSlot,
                    _shieldSlot,
                    _hatSlot,
                    _maskSlot,
                    _coatSlot,
                    _armorSlot,
                    _amuletSlot
            );
        }

        override protected function render():void
        {
            _weaponSlot.x = 9;
            _weaponSlot.y = 221;

            _shieldSlot.x = 217;
            _shieldSlot.y = 221;

            _hatSlot.x = 109;
            _hatSlot.y = 60;

            _maskSlot.x = 163;
            _maskSlot.y = 60;

            _coatSlot.x = 55;
            _coatSlot.y = 60;

            _armorSlot.x = 9;
            _armorSlot.y = 167;

            _amuletSlot.x = 217;
            _amuletSlot.y = 167;

            addChild(_weaponSlot);
            addChild(_shieldSlot);
            addChild(_hatSlot);
            addChild(_maskSlot);
            addChild(_coatSlot);
            addChild(_armorSlot);
            addChild(_amuletSlot);

            _setupRings();
        }

        override public function destroy():void
        {
            _weaponSlot.destroy();
            _shieldSlot.destroy();
            _hatSlot.destroy();
            _maskSlot.destroy();
            _coatSlot.destroy();
            _armorSlot.destroy();
            _amuletSlot.destroy();

            _ringsPanel.destroy();

            super.destroy();
        }

        override public function add(item:InventoryItem):void
        {
            if (_methods[item.arrangement_variant.value]) {
                var method:Function = _methods[item.arrangement_variant.value];
                method.call(this, item);
            }
        }

        override public function getItems():Array
        {
            var result:Array = [];
            result = result.concat(super.getItems())
            result = result.concat(_ringsPanel.getItems());

            return result;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupRings():void
        {
            _ringsPanel = new RingSlotPanel();
            _ringsPanel.x = 9;
            _ringsPanel.y = 113;

            addChild(_ringsPanel);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _weaponSlot:WeaponSlot;
        protected var _shieldSlot:ShieldSlot;
        protected var _hatSlot:HatSlot;
        protected var _maskSlot:MaskSlot;
        protected var _coatSlot:CoatSlot;
        protected var _armorSlot:ArmorSlot;
        protected var _amuletSlot:AmuletSlot;

        protected var _ringsPanel:RingSlotPanel;

        private var _methods:Object = {};

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupMethodsRelations():void
        {
            _methods[ArrangementVariant.AMULET.toString()] = _addAmulet;
            _methods[ArrangementVariant.RINGS.toString()] = _addRing;

            _methods[ArrangementVariant.ROD_WEAPON.toString()] = _addWeapon;
            _methods[ArrangementVariant.GLOVES.toString()] = _addWeapon;
            _methods[ArrangementVariant.ONE_HANDED.toString()] = _addWeapon;
            _methods[ArrangementVariant.TWO_HANDED.toString()] = _addWeapon;

            _methods[ArrangementVariant.MASK.toString()] = _addMask;
            _methods[ArrangementVariant.GLASSES.toString()] = _addMask;

            _methods[ArrangementVariant.HAT.toString()] = _addHat;
            _methods[ArrangementVariant.ARMOR.toString()] = _addArmor;

            _methods[ArrangementVariant.COAT.toString()] = _addCoat;
            _methods[ArrangementVariant.SHIELD.toString()] = _addShield;

        }

        private function _addWeapon(item:InventoryItem):void
        {
            _weaponSlot.item = item;
        }

        private function _addShield(item:InventoryItem):void
        {
            _shieldSlot.item = item;
        }

        private function _addHat(item:InventoryItem):void
        {
            _hatSlot.item = item;
        }

        private function _addMask(item:InventoryItem):void
        {
            _maskSlot.item = item;
        }

        private function _addCoat(item:InventoryItem):void
        {
            _coatSlot.item = item;
        }

        private function _addArmor(item:InventoryItem):void
        {
            _armorSlot.item = item;
        }

        private function _addAmulet(item:InventoryItem):void
        {
            _amuletSlot.item = item;
        }

        private function _addRing(item:InventoryItem):void
        {
            _ringsPanel.add(item);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        override public function set readonly(value:Boolean):void
        {
            super.readonly = value;
            _ringsPanel.readonly = value;
        }

        public function get shieldSlot():ShieldSlot
        {
            return _shieldSlot;
        }

        public function get ringsPanel():RingSlotPanel
        {
            return _ringsPanel;
        }
    }
}
