/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.11.13
 * Time: 21:03
 */
package application.views.screen.inventory.slots
{
    import application.models.market.Item;
    import application.views.screen.common.cell.CellHintView;
    import application.views.screen.common.hint.HintCorner;

    import framework.core.enum.Side;

    public class SlotHint extends CellHintView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SlotHint(item:Item)
        {
            super(item);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _addCorner():void
        {
            var corner:HintCorner = new HintCorner(Side.RIGHT);
            addChild(corner);

            corner.x = int(super.w - 6);
            corner.y = int(super.h / 2 - corner.w / 2);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
