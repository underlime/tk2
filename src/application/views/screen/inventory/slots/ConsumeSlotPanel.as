/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.06.13
 * Time: 13:48
 */
package application.views.screen.inventory.slots
{
    import application.views.screen.inventory.slots.battle.ConsumeSlot;

    public class ConsumeSlotPanel extends RingSlotPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ConsumeSlotPanel()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _renderSlots();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _count:int = 5;
        protected var _dx:int = 54;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        protected function _renderSlots():void
        {
            var slot:ConsumeSlot;
            var cx:int = 0;

            for (var i:int = 0; i < _count; i++) {
                slot = new ConsumeSlot();
                slot.x = cx;
                _slots.push(slot);

                cx += _dx;
                addChild(slot);
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
