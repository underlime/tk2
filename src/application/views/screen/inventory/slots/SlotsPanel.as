/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 03.07.13
 * Time: 15:06
 */
package application.views.screen.inventory.slots
{
    import application.models.inventory.InventoryItem;
    import application.views.AppView;
    import application.views.screen.inventory.slots.base.EquippingSlot;

    public class SlotsPanel extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SlotsPanel()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function add(item:InventoryItem):void
        {

        }

        public function getItems():Array
        {
            var result:Array = [];
            var count:int = _slots.length;

            for (var i:int = 0; i < count; i++) {
                if (_slots[i].item)
                    result.push(_slots[i].item);
            }

            return result;
        }

        public function lockUp():void
        {
            var count:int = _slots.length;
            for (var i:int = 0; i < count; i++) {
                _slots[i].locked = true;
            }
        }

        public function unlock(count:int):void
        {
            var unlocked:int = 0;
            for (var i:int = 0; i < _slots.length; i++) {
                _slots[i].locked = false;
                unlocked++;

                if (unlocked >= count)
                    break;
            }
        }

        public function lockSlots(count:int):void
        {
            var total:int = _slots.length;
            if (count <= total) {
                var skip:int = total - count;
                for (skip; skip < total; skip++) {
                    _slots[skip].locked = true;
                }
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _slots:Vector.<EquippingSlot> = new Vector.<EquippingSlot>();

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get slots():Vector.<EquippingSlot>
        {
            return _slots;
        }

        public function set readonly(value:Boolean):void
        {
            if (value) {
                for (var i:int = 0; i < _slots.length; i++) {
                    _slots[i].readonly = true;
                }
            }
        }

    }
}
