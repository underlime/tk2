/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.06.13
 * Time: 12:52
 */
package application.views.screen.inventory.slots
{
    import application.models.inventory.InventoryItem;
    import application.views.screen.inventory.slots.battle.RingSlot;

    public class RingSlotPanel extends SlotsPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RingSlotPanel()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupRings();
        }

        override public function destroy():void
        {
            var count:int = _slots.length;
            for (var i:int = 0; i < count; i++) {
                _slots[i].destroy();
            }
            _slots = null;
            super.destroy();
        }

        override public function add(item:InventoryItem):void
        {
            var count:int = super._slots.length;
            for (var i:int = 0; i < count; i++) {
                if (_slots[i].empty && !_slots[i].locked) {
                    _slots[i].item = item;
                    break;
                }
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _ringSlot1:RingSlot;
        protected var _ringSlot2:RingSlot;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        protected function _setupRings():void
        {
            _ringSlot1 = new RingSlot();
            addChild(_ringSlot1);

            _ringSlot2 = new RingSlot();
            _ringSlot2.x = 208;

            addChild(_ringSlot2);

            super._slots.push(
                    _ringSlot1,
                    _ringSlot2
            );
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
