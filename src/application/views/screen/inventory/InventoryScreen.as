/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.06.13
 * Time: 10:17
 */
package application.views.screen.inventory
{
    import application.common.ArrangementVariant;
    import application.events.InventoryEvent;
    import application.events.SlotEvent;
    import application.models.ModelData;
    import application.models.inventory.Inventory;
    import application.models.inventory.InventoryHelper;
    import application.models.inventory.InventoryItem;
    import application.models.inventory.InventoryState;
    import application.models.market.Item;
    import application.models.user.User;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.constructor.builders.ClothesChanger;
    import application.views.constructor.common.WeaponTemplate;
    import application.views.constructor.helpers.DressHelper;
    import application.views.popup.BuyingItem;
    import application.views.popup.SellDialog;
    import application.views.screen.common.cell.Cell;
    import application.views.screen.common.cell.InventoryCell;
    import application.views.screen.inventory.context.ContextMenu;
    import application.views.screen.inventory.context.EquipContext;
    import application.views.screen.inventory.context.UseContext;
    import application.views.screen.inventory.slots.SlotsView;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.utils.DisplayObjectUtils;

    public class InventoryScreen extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InventoryScreen()
        {
            super();

            _inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;
            _user = ModelsRegistry.getModel(ModelData.USER) as User;

            _inventory.flush();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addSlots();
            _setupLayout();

            _addUnit();

            _fillInventorySlots();
            _fillSocialSlots();
            _fillConsumeSlots();

            _updateData();
            _addContext();
            _checkUnit();
            _bindReactions();
        }

        override public function destroy():void
        {
            _layout.destroy();
            _equipContext.destroy();
            _useContext.destroy();
            _slots.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            _updateData();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _state:InventoryState = new InventoryState();
        private var _unit:ClothesChanger;

        private var _inventory:Inventory;
        private var _layout:InventoryLayout;
        private var _equipContext:EquipContext = new EquipContext(_equip, _sell, _socialEquip);
        private var _useContext:UseContext = new UseContext(_use, _sell);

        private var _slots:SlotsView;
        private var _user:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupLayout():void
        {
            _layout = new InventoryLayout();
            addChild(_layout);
        }

        private function _addUnit():void
        {
            var items:Vector.<Item> = InventoryHelper.ItemsArray2Vector(_inventory.getBuildingItems());
            _unit = new ClothesChanger(_user.userInfo, items);
            addChild(_unit);

            _unit.scaleX = _unit.scaleY = .8;
            _unit.x = 530;
            _unit.y = 307;
            _unit.dress();
        }

        private function _addSlots():void
        {
            _slots = new SlotsView();
            _slots.x = 438;
            _slots.y = 144;
            addChild(_slots);
        }

        private function _fillInventorySlots():void
        {
            var items:Array = _inventory.getPutItems();
            var count:int = items.length;

            for (var i:int = 0; i < count; i++) {
                var item:InventoryItem = items[i] as InventoryItem;
                for (var j:int = 0; j < item.put.value; j++) {
                    _slots.inventorySlots.add(item);
                }
            }
        }

        private function _fillSocialSlots():void
        {
            var items:Array = _inventory.getSocialPutItems();
            var count:int = items.length;

            for (var i:int = 0; i < count; i++) {
                _slots.socialSlots.add(items[i] as InventoryItem);
            }
        }

        private function _fillConsumeSlots():void
        {
            var items:Array = _inventory.getConsumeItems();
            var count:int = items.length;

            for (var i:int = 0; i < count; i++) {
                _slots.consumeSlots.add(items[i] as InventoryItem);
            }
        }

        private function _updateItems():void
        {
            var cells:Vector.<Cell> = new Vector.<Cell>();
            var items:Array = _inventory.getAllItems(_state.start, _state.limit);
            var count:int = items.length;
            for (var i:int = 0; i < count; i++) {
                var item:InventoryItem = items[i];
                cells.push(new InventoryCell(item));
            }

            _layout.mesh.cells = cells;
            _layout.mesh.draw();
        }

        private function _updateNavigation():void
        {
            var totalItems:int = _inventory.getNotUseInTempItems().length;
            _state.totalPages = int(Math.ceil(totalItems / _state.limit));
            _layout.navigation.totalPages = _state.totalPages;
            _layout.navigation.currentPage = _state.currentPage;
        }

        private function _updatePage():void
        {
            _state.start = (_state.currentPage - 1) * _state.limit;
            _updateItems();
            _updateNavigation();
        }

        private function _bindReactions():void
        {
            _layout.mesh.addEventListener(Event.SELECT, _selectHandler);
            _layout.confirmButton.addEventListener(MouseEvent.CLICK, _confirmClickHandler);
            _layout.navigation.addEventListener(Event.CHANGE, _pageChangeHandler);

            _slots.addEventListener(SlotEvent.CHANGE, _slotsChangeHandler);
        }

        private function _updateData():void
        {
            _state.currentPage = 1;
            _updateItems();
            _updateNavigation();
        }

        private function _addContext():void
        {
            addChild(_equipContext);
            _equipContext.hide();

            addChild(_useContext);
            _useContext.hide();
        }

        private function _equip():void
        {
            var item:InventoryItem = _layout.mesh.item;
            if (item) {
                if (item.required_level.value <= _user.userLevelParams.max_level.value) {
                    if (item.isConsume)
                        _slots.consumeSlots.add(item);
                    else
                        _slots.inventorySlots.add(item);
                    _updateData();
                } else {
                    var message:String = TextFactory.instance.getLabel('equip_denied_message').replace("{{level}}", item.required_level.value.toString());
                    super.alert(TextFactory.instance.getLabel('equip_denied_label'), message);
                }
            }
            _checkUnit();
        }

        private function _socialEquip():void
        {
            var item:InventoryItem = _layout.mesh.item;
            if (item) {
                _slots.socialSlots.add(item);
                _updateData();
            }
            _checkUnit();
        }

        private function _sell():void
        {
            var item:InventoryItem = _layout.mesh.item;
            if (item) {
                var sellItem:BuyingItem = new BuyingItem();
                sellItem.importFromItem(_layout.mesh.item);
                var dialog:SellDialog = new SellDialog(sellItem, _sellItem);
                addChild(dialog);
            }
        }

        private function _sellItem():void
        {
            var item:InventoryItem = _layout.mesh.item;
            if (item) {
                var event:InventoryEvent = new InventoryEvent(InventoryEvent.SELL_ITEM);
                event.item = item;
                super.dispatchEvent(event);
            }
        }

        private function _use():void
        {
            var item:InventoryItem = _layout.mesh.item;
            if (item) {
                var event:InventoryEvent = new InventoryEvent(InventoryEvent.APPLY_ITEM);
                event.item = item;
                super.dispatchEvent(event);
            }
        }

        private function _checkUnit():void
        {
            if (_unit.weaponTemplate == WeaponTemplate.TWO_HANDED_WEAPON || _unit.weaponTemplate == WeaponTemplate.WITHOUT_WEAPON) {
                _slots.inventorySlots.shieldSlot.locked = true;
                _slots.socialSlots.shieldSlot.locked = true;
            } else {
                _slots.inventorySlots.shieldSlot.locked = false;
                _slots.socialSlots.shieldSlot.locked = false;
            }
            _update();
        }

        private function _update():void
        {
            var putItems:Array = _slots.inventorySlots.getItems();
            var socialItems:Array = _slots.socialSlots.getItems();
            var items:Array = DressHelper.getBuildItems(putItems, socialItems);

            _unit.items = InventoryHelper.ItemsArray2Vector(items);
            _unit.dress();

            _updateData();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _slotsChangeHandler(event:SlotEvent):void
        {
            _update();
            _checkUnit();
        }

        private function _pageChangeHandler(event:Event):void
        {
            _state.currentPage = _layout.navigation.currentPage;
            _updatePage();
        }

        private function _selectHandler(event:Event):void
        {
            _useContext.hide();
            _equipContext.hide();
            var context:ContextMenu;
            var item:Item = _layout.mesh.item;
            if (item.arrangement_variant.value == ArrangementVariant.APPLY.toString())
                context = _useContext;
            else
                context = _equipContext;

            context.show();
            context.x = mouseX;
            context.y = mouseY;

            DisplayObjectUtils.moveIntoParent(context, this);
        }

        private function _confirmClickHandler(e:MouseEvent):void
        {
            var putItems:Array = _slots.inventorySlots.getItems().concat(_slots.consumeSlots.getItems());
            var socialItems:Array = _slots.socialSlots.getItems();

            var put_list:Array = [];
            var social_put_list:Array = [];

            var count:int = putItems.length;
            for (var i:int = 0; i < count; i++) {
                put_list.push(putItems[i].item_id.value);
            }

            count = socialItems.length;
            for (i = 0; i < count; i++) {
                social_put_list.push(socialItems[i].item_id.value);
            }

            var event:InventoryEvent = new InventoryEvent(InventoryEvent.PUT_ITEMS);
            event.data['put_list'] = put_list;
            event.data['social_put_list'] = social_put_list;

            super.dispatchEvent(event);
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
