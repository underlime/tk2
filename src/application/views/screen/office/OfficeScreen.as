/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 03.09.13
 * Time: 15:54
 */
package application.views.screen.office
{
    import application.models.ModelData;
    import application.models.user.User;

    import flash.display.Bitmap;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;

    public class OfficeScreen extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "ACADEMY_STALL";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OfficeScreen()
        {
            super();
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            this.y = 107;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addLayout();
        }

        override public function destroy():void
        {
            _layout.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function loginDeny():void
        {
            _layout.loginDeny();
        }

        public function loginAllow():void
        {
            _layout.loginAllow();
        }

        public function update():void
        {
            _layout.update();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:User;
        private var _layout:ChangeLoginLayout;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var back:Bitmap = super.source.getBitmap(BACK);
            addChild(back);

            back.x = 38;
            back.y = 101;
        }

        private function _addLayout():void
        {
            var login:String = _user.userInfo.login.value;
            var price:int = _user.configPrices.login_ferros_price.value;

            _layout = new ChangeLoginLayout(price, login);
            addChild(_layout);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get login():String
        {
            return _layout.login;
        }
    }
}
