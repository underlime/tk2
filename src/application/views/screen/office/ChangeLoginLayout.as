/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.01.14
 * Time: 17:23
 */
package application.views.screen.office
{
    import application.events.OfficeEvent;
    import application.helpers.LoginHelper;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.text.AppTextField;

    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.text.TextFormatAlign;

    public class ChangeLoginLayout extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ChangeLoginLayout(price:int, defName:String)
        {
            super();
            _price = price;
            _currentLogin = defName;
            _typedLogin = _currentLogin;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addHeader();
            _addDescription();
            _addInputField();
            _addButton();
            _addPriceButton();
        }

        override public function destroy():void
        {
            _inputField.txtField.removeEventListener(Event.CHANGE, _loginChangeHandler);
            _inputField.destroy();

            _priceButton.destroy();
            _checkButton.destroy();

            _deniedLogin.destroy();
            _allowLogin.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function loginDeny():void
        {
            _checkButton.visible = false;
            _allowLogin.visible = false;
            _deniedLogin.visible = true;
        }

        public function loginAllow():void
        {
            _allowedLoginList.push(_typedLogin);
            _priceButton.enable();

            _checkButton.visible = true;
            _allowLogin.visible = true;
            _deniedLogin.visible = false;
        }

        public function update():void
        {
            _allowedLoginList = [];
            loginDeny();
            _priceButton.block();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _headerLabel:String = TextFactory.instance.getLabel('change_name_label');
        protected var _descriptionLabel:String = TextFactory.instance.getLabel('change_name_text');
        protected var _payButtonLabel:String = TextFactory.instance.getLabel('change_name_button_label');

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _header:AppTextField;
        private var _description:AppTextField;

        private var _inputField:InputField;

        private var _checkButton:CheckButton;

        private var _priceButton:ChangeLoginButton;
        private var _deniedLogin:DeniedResolution;
        private var _allowLogin:AllowButton;

        private var _price:int = 0;

        private var _currentLogin:String = "";
        private var _typedLogin:String = "";

        private var _allowedLoginList:Array = [];

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHeader():void
        {
            _header = new AppTextField();
            _header.size = 16;
            _header.bold = true;
            _header.color = 0x3c2415;
            _header.width = 408;
            _header.align = TextFormatAlign.CENTER;
            addChild(_header);
            _header.text = _headerLabel;

            _header.x = 165;
            _header.y = 131;
        }

        private function _addDescription():void
        {
            _description = new AppTextField();
            _description.size = 12;
            _description.bold = true;
            _description.color = 0x5e3113;
            _description.width = 408;
            _description.align = TextFormatAlign.CENTER;
            _description.multiline = true;
            _description.wordWrap = true;
            addChild(_description);
            _description.text = _descriptionLabel;

            _description.x = 165;
            _description.y = 160;
        }

        private function _addInputField():void
        {
            _inputField = new InputField();
            addChild(_inputField);
            _inputField.x = 188;
            _inputField.y = 226;

            _inputField.txtField.text = _currentLogin;
            _inputField.txtField.addEventListener(Event.CHANGE, _loginChangeHandler);
        }

        private function _addButton():void
        {
            _checkButton = new CheckButton();
            addChild(_checkButton);
            _checkButton.x = 446;
            _checkButton.y = 226;
            _checkButton.visible = false;

            _checkButton.addEventListener(MouseEvent.CLICK, _checkClickHandler);

            _allowLogin = new AllowButton();
            _allowLogin.x = 446;
            _allowLogin.y = 226;
            addChild(_allowLogin);
            _allowLogin.visible = false;

            _deniedLogin = new DeniedResolution();
            _deniedLogin.x = 446;
            _deniedLogin.y = 226;
            addChild(_deniedLogin);
            _deniedLogin.visible = true;
        }

        private function _addPriceButton():void
        {
            _priceButton = new ChangeLoginButton(_price);
            _priceButton.label = _payButtonLabel;
            addChild(_priceButton);
            _priceButton.x = 267;
            _priceButton.y = 289;
            _priceButton.block();

            _priceButton.addEventListener(MouseEvent.CLICK, _priceButtonClickHandler);
        }

        private function _validateLoginLength():void
        {
            if (_typedLogin.length > LoginHelper.MAX) {
                var count:int = _typedLogin.length;
                var newText:String = _typedLogin.slice(0, count - 1);
                _typedLogin = newText;
                _inputField.txtField.text = _typedLogin;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _loginChangeHandler(event:Event):void
        {
            _typedLogin = _inputField.txtField.text;
            _validateLoginLength();

            if (!_priceButton.blocked)
                _priceButton.block();

            if (_typedLogin != _currentLogin && LoginHelper.asseert(_typedLogin)) {
                _checkButton.visible = true;
                _allowLogin.visible = false;
                _deniedLogin.visible = false;
            } else {
                loginDeny();
            }
        }

        private function _checkClickHandler(e:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            if (_allowedLoginList.indexOf(_typedLogin) > -1) {
                loginAllow();
            } else {
                var event:OfficeEvent = new OfficeEvent(OfficeEvent.CHECK_LOGIN, true);
                event.login = _typedLogin;
                super.dispatchEvent(event);
            }
        }

        private function _priceButtonClickHandler(e:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            if (!_priceButton.blocked) {
                var event:OfficeEvent = new OfficeEvent(OfficeEvent.CHANGE_LOGIN, true);
                event.login = _typedLogin;
                super.dispatchEvent(event);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get login():String
        {
            return _typedLogin;
        }

    }
}
