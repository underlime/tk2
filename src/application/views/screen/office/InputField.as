/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 03.09.13
 * Time: 21:26
 */
package application.views.screen.office
{
    import application.views.screen.common.navigation.Counter;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;
    import flash.text.TextFieldType;

    import framework.core.display.rubber.RubberView;
    import framework.core.struct.view.View;
    import framework.core.tools.SourceManager;

    public class InputField extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InputField()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _defineWidth();
            _addBackground();
            _addTextField();
        }

        override public function destroy():void
        {
            _txtField.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtField:AppTextField;
        private var _width:Number;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _defineWidth():void
        {
            _width = super.w > 0 ? super.w : 250;
        }

        private function _addBackground():void
        {
            var left:Bitmap = SourceManager.instance.getBitmap(Counter.BOX_LEFT);
            var center:Bitmap = SourceManager.instance.getBitmap(Counter.BOX_CENTER);
            var right:Bitmap = SourceManager.instance.getBitmap(Counter.BOX_RIGHT);

            var rubber:RubberView = new RubberView(left, center, right);
            rubber.w = _width;
            rubber.scrollRect = new Rectangle(0, 80, _width, 40);
            addChild(rubber);
        }

        private function _addTextField():void
        {
            _txtField = new AppTextField();
            _txtField.width = _width - 32;
            _txtField.height = 30;
            _txtField.type = TextFieldType.INPUT;
            _txtField.color = 0x000000;
            _txtField.size = 16;
            _txtField.bold = true;
            addChild(_txtField);
            _txtField.x = 16;
            _txtField.y = 5;
            _txtField.mouseEnabled = true;
            _txtField.selectable = true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get txtField():AppTextField
        {
            return _txtField;
        }
    }
}
