/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 02.09.13
 * Time: 20:44
 */
package application.views.screen.market.filters
{
    import application.views.screen.common.filter.FilterTabsPanel;
    import application.views.screen.common.filter.tabs.AllTab;
    import application.views.screen.common.filter.tabs.GlovesTab;
    import application.views.screen.common.filter.tabs.OneHandedTab;
    import application.views.screen.common.filter.tabs.RodWeaponTab;
    import application.views.screen.common.filter.tabs.TwoHandedTab;

    import framework.core.display.tabs.ITab;

    public class WeaponFilters extends FilterTabsPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function WeaponFilters()
        {
            var tabs:Vector.<ITab> = new <ITab>[new AllTab(), new GlovesTab(), new OneHandedTab(), new RodWeaponTab(), new TwoHandedTab()];
            super(tabs);
            _dy = 1;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
