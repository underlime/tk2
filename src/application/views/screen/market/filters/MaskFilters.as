/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 02.09.13
 * Time: 20:51
 */
package application.views.screen.market.filters
{
    import application.views.screen.common.filter.FilterTabsPanel;
    import application.views.screen.common.filter.tabs.AllTab;
    import application.views.screen.common.filter.tabs.GlassesTab;
    import application.views.screen.common.filter.tabs.MaskTab;

    import framework.core.display.tabs.ITab;

    public class MaskFilters extends FilterTabsPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MaskFilters()
        {
            var tabs:Vector.<ITab> = new <ITab>[new AllTab(), new MaskTab(), new GlassesTab()];
            super(tabs);
            _dy = 1;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
