/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 02.09.13
 * Time: 20:50
 */
package application.views.screen.market.filters
{
    import application.views.screen.common.filter.FilterTabsPanel;
    import application.views.screen.common.filter.tabs.AllTab;
    import application.views.screen.common.filter.tabs.EnergyTab;
    import application.views.screen.common.filter.tabs.GrenadeTab;
    import application.views.screen.common.filter.tabs.MedicineTab;

    import framework.core.display.tabs.ITab;

    public class ItemsFilters extends FilterTabsPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ItemsFilters()
        {
            var tabs:Vector.<ITab> = new <ITab>[new AllTab(), new MedicineTab(), new EnergyTab(), new GrenadeTab()];
            super(tabs);
            _dy = 1;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
