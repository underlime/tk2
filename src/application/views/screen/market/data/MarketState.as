/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.06.13
 * Time: 15:27
 */
package application.views.screen.market.data
{
    import application.views.screen.common.filter.FilterTabsPanel;
    import application.views.screen.common.state.ScreenState;
    import application.views.screen.market.filters.AmuletFilters;
    import application.views.screen.market.filters.ArmorFilters;
    import application.views.screen.market.filters.ItemsFilters;
    import application.views.screen.market.filters.MaskFilters;
    import application.views.screen.market.filters.WeaponFilters;

    public class MarketState extends ScreenState
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketState()
        {
            super();
            this.limit = 10;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get rubric():String
        {
            var rubric:MarketRubric;

            switch (this.tabID) {
                case 0:
                    rubric = MarketRubric.WEAPON;
                    break;
                case 1:
                    rubric = MarketRubric.HAT;
                    break;
                case 2:
                    rubric = MarketRubric.ARMOR;
                    break;
                case 3:
                    rubric = MarketRubric.COAT;
                    break;
                case 4:
                    rubric = MarketRubric.MASK;
                    break;
                case 5:
                    rubric = MarketRubric.AMULET;
                    break;
                case 6:
                    rubric = MarketRubric.DRINK;
                    break;
                default:
                    rubric = MarketRubric.WEAPON;
                    break;
            }

            return rubric.toString();
        }

        public function get filtersPanel():FilterTabsPanel
        {
            switch (this.tabID) {
                case 0:
                    return new WeaponFilters();
                case 1:
                    return null;
                case 2:
                    return new ArmorFilters();
                case 3:
                    return null;
                case 4:
                    return new MaskFilters();
                case 5:
                    return new AmuletFilters();
                case 6:
                    return new ItemsFilters();
                default:
                    return null;
            }
        }

    }
}
