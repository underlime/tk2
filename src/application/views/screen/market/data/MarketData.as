/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.06.13
 * Time: 16:15
 */
package application.views.screen.market.data
{
    public class MarketData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "Market_Background";
        public static const ARROW_LEFT:String = "Market_ArrowLeft";
        public static const ARROW_RIGHT:String = "Market_ArrowRight";
        public static const BUY_BUTTON:String = "Market_BuyBtn";
        public static const CLOSE_BTN:String = "Market_CloseBtn";
        public static const FRAME:String = "Market_Frame";

        // tabs

        public static const WEAPON_TAB:String = "Market_WeaponTab";
        public static const HAT_TAB:String = "Market_HatTab";
        public static const ARMOR_TAB:String = "Market_ArmorTab";
        public static const COAT_TAB:String = "Market_CoatTab";
        public static const MASK_TAB:String = "Market_MaskTab";
        public static const AMULET_TAB:String = "Market_AmuletTab";
        public static const DRINK_TAB:String = "Market_DrinkTab";

        // filters

        public static const FILTER_ALL:String = "MAP_FilterAll";
        public static const FILTER_GLOVES:String = "MAP_FilterGloves";
        public static const FILTER_ONE_HANDED:String = "MAP_FilterOneHanded";
        public static const FILTER_ROD_WEAPON:String = "MAP_FilterRod";
        public static const FILTER_TWO_HANDED:String = "MAP_FilterTwoHanded";
        public static const FILTER_ARMOR:String = "MAP_FilterArmor";
        public static const FILTER_SHIELD:String = "MAP_FilterShield";
        public static const FILTER_RINGS:String = "MAP_FilterRings";
        public static const FILTER_RANGE:String = "MAP_FilterRange";
        public static const FILTER_QUEST:String = "MAP_FilterQuest";
        public static const FILTER_MED:String = "MAP_FilterMed";
        public static const FILTER_MASK:String = "MAP_FilterMasks";
        public static const FILTER_GRENADE:String = "MAP_FilterGranade";
        public static const FILTER_GLASSES:String = "MAP_FilterGlasses";
        public static const FILTER_ENERGY:String = "MAP_FilterEnergy";
        public static const FILTER_BOX:String = "MAP_FilterBox";
        public static const FILTER_BOOTS:String = "MAP_FilterBoots";
        public static const FILTER_AMULET:String = "MAP_FilterAmulets";
        public static const FILTER_HAT:String = "MAP_FilterHat";
        public static const FILTER_COAT:String = "MapFilterCoat";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
