/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.06.13
 * Time: 16:03
 */
package application.views.screen.market.data
{
    import framework.core.enum.BaseEnum;

    public class MarketRubric extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const WEAPON:MarketRubric = new MarketRubric("weapons");
        public static const HAT:MarketRubric = new MarketRubric("hats");
        public static const ARMOR:MarketRubric = new MarketRubric("armor");
        public static const COAT:MarketRubric = new MarketRubric("cloaks");
        public static const MASK:MarketRubric = new MarketRubric("masks");
        public static const AMULET:MarketRubric = new MarketRubric("rings");
        public static const DRINK:MarketRubric = new MarketRubric("elixirs");

        private static var _lockUp:Boolean = false;

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketRubric(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.views.screen.market.data.MarketRubric;

MarketRubric.lockUp();