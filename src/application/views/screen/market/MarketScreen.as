/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 31.05.13
 * Time: 14:07
 */
package application.views.screen.market
{
    import application.events.BuyEvent;
    import application.models.ModelData;
    import application.models.inventory.Inventory;
    import application.models.inventory.InventoryHelper;
    import application.models.market.Item;
    import application.models.market.MarketItems;
    import application.models.user.User;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.constructor.builders.ClothesChanger;
    import application.views.helpers.ItemDescriptionHelper;
    import application.views.popup.BuyDialog;
    import application.views.popup.BuyingItem;
    import application.views.popup.MultiBuyDialog;
    import application.views.screen.common.cell.Cell;
    import application.views.screen.common.cell.MarketCell;
    import application.views.screen.market.data.MarketState;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.struct.data.ModelsRegistry;

    public class MarketScreen extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketScreen()
        {
            super();
            _marketItems = ModelsRegistry.getModel(ModelData.MARKET_ITEMS) as MarketItems;
            _inventory = ModelsRegistry.getModel(ModelData.INVENTORY) as Inventory;
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _state = new MarketState();
            _setLayout();
            _updateParams();
            _bindNavigation();
            _createUnit();
        }

        override public function destroy():void
        {
            if (_marketLayout && !_marketLayout.destroyed)
                _marketLayout.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _marketItems:MarketItems;
        private var _marketLayout:MarketLayout;
        private var _state:MarketState;
        private var _items:Array = [];

        private var _unit:ClothesChanger;
        private var _inventory:Inventory;

        private var _user:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setLayout():void
        {
            _marketLayout = new MarketLayout(_state);
            addChild(_marketLayout);

            _marketLayout.tabs.addEventListener(Event.CHANGE, _tabChangeHandler);
            _marketLayout.tabs.select(0);

            _marketLayout.filterPanel.addEventListener(Event.CHANGE, _filterChangeHandler);
            _marketLayout.mesh.addEventListener(Event.SELECT, _selectHandler);
            _marketLayout.buyButton.addEventListener(MouseEvent.CLICK, _buyClickHandler);
        }

        private function _updateFilter():void
        {
            if (_marketLayout.filterPanel && _marketLayout.contains(_marketLayout.filterPanel)) {
                _marketLayout.filterPanel.destroy();
            }

            _marketLayout.filterPanel = _state.filtersPanel;

            if (_marketLayout.filterPanel) {
                _marketLayout.addChild(_marketLayout.filterPanel);
                _marketLayout.filterPanel.select(0);
                _marketLayout.filterPanel.addEventListener(Event.CHANGE, _filterChangeHandler);
            }

            _state.filter = null;
        }

        private function _updateItems():void
        {
            var filter:String = _state.filter ? _state.filter.toString() : null;
            _items = _marketItems.getSortedItems(_state.rubric, filter, _state.start, _state.limit);
        }

        private function _updateMesh():void
        {
            var cells:Vector.<Cell> = new Vector.<Cell>();
            var count:int = _items.length;

            for (var i:int = 0; i < count; i++) {
                var canEquip:Boolean = InventoryHelper.canEquip(_items[i], _user.userLevelParams.max_level.value);
                var cell:MarketCell = new MarketCell(_items[i], canEquip);
                cells.push(cell);
            }

            _marketLayout.mesh.cells = cells;
            _marketLayout.mesh.draw();
        }

        private function _bindNavigation():void
        {
            _marketLayout.navigation.addEventListener(Event.CHANGE, _pageChangeHandler);
        }

        private function _createUnit():void
        {
            var criteria:Object = {
                "put": 1
            };
            var items:Vector.<Item> = InventoryHelper.ItemsArray2Vector(_inventory.search(criteria));
            _unit = new ClothesChanger(_user.userInfo, items);
            _unit.scaleX = _unit.scaleY = .8;

            addChild(_unit);
            _unit.x = 570;
            _unit.y = 170;
            _unit.undress();
        }

        private function _getTotalItems():Array
        {
            var criteria:Object = {
                "rubric_code": _state.rubric,
                "show_in_market": true
            };

            if (_state.filter)
                criteria.arrangement_variant = _state.filter.toString();

            return _marketItems.search(criteria, _state.start);
        }

        private function _updateParams():void
        {
            _state.start = 0;

            _state.currentPage = 1;
            _state.totalPages = int(Math.ceil(_getTotalItems().length / _state.limit));

            _marketLayout.navigation.currentPage = 1;
            _marketLayout.navigation.totalPages = _state.totalPages;

            _updateItems();
            _updateMesh();
        }

        private function _updatePage():void
        {
            _state.start = (_state.currentPage - 1) * _state.limit;
            _updateItems();
            _updateMesh();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _tabChangeHandler(event:Event):void
        {
            _state.tabID = _marketLayout.tabs.selectedTab;
            _updateFilter();
            _state.filter = null;
            _updateParams();
            if (_unit)
                _unit.undress();
        }

        private function _filterChangeHandler(event:Event):void
        {
            _state.filter = _marketLayout.filterPanel.arrangement_variant;
            _updateParams();
        }

        private function _pageChangeHandler(event:Event):void
        {
            _state.currentPage = _marketLayout.navigation.currentPage;
            _updatePage();
        }

        private function _selectHandler(event:Event):void
        {
            var item:Item = _marketLayout.mesh.item;
            if (item) {
                _marketLayout.priceFerros.price = item.price_ferros.value;
                _marketLayout.priceTomatos.price = item.price_tomatos.value;

                var description:String = ItemDescriptionHelper.getDescription(item);
                _marketLayout.txtItemDescription.htmlText = description;

                _unit.addItem(item);
            }
        }

        private function _buyClickHandler(event:MouseEvent):void
        {
            var item:Item = _marketLayout.mesh.item;
            if (item) {
                var dialogItem:BuyingItem = new BuyingItem();
                dialogItem.importFromItem(item);
                var dialog:MultiBuyDialog = new MultiBuyDialog(dialogItem);
                dialog.addEventListener(BuyEvent.BUY, _buyHandler);
                addChild(dialog);
                super.sound.playUISound(UISound.CLICK);
            }
        }

        private function _buyHandler(event:BuyEvent):void
        {
            (event.currentTarget as BuyDialog).destroy();
            event.item = _marketLayout.mesh.item;
            super.dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
