/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.06.13
 * Time: 16:12
 */
package application.views.screen.market
{
    import application.views.TextFactory;
    import application.views.popup.common.FerrosPrice;
    import application.views.popup.common.TomatosPrice;
    import application.views.screen.base.BaseScreen;
    import application.views.screen.common.filter.FilterTabsPanel;
    import application.views.screen.common.navigation.Navigation;
    import application.views.screen.market.data.MarketState;
    import application.views.screen.market.mesh.MarketMesh;
    import application.views.screen.market.tabs.MarketTabs;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;

    public class MarketLayout extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const STALL:String = "MARKET_STALL";
        public static const INFO:String = "MARKET_INFO";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketLayout(state:MarketState)
        {
            _state = state;
            super(TextFactory.instance.getLabel('market_label'));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addBack();
            _setTabs();
            _setFilters();
            _setupMesh();
            _setItemLabels();
            _setBuyButton();
            _addPrices();
            _addNavigationBlock();
        }

        override public function destroy():void
        {
            _mesh.destroy();
            _tabs.destroy();
            _buyButton.destroy();
            _navigation.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _state:MarketState;

        private var _tabs:MarketTabs;
        private var _filterPanel:FilterTabsPanel;
        private var _mesh:MarketMesh;
        private var _txtItemDescription:AppTextField;

        private var _buyButton:BuyButton;
        private var _navigation:Navigation;

        private var _priceTomatos:TomatosPrice;
        private var _priceFerros:FerrosPrice;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            var marketStall:Bitmap = super.source.getBitmap(STALL);
            var marketInfo:Bitmap = super.source.getBitmap(INFO);

            addChild(marketStall);
            addChild(marketInfo);

            marketStall.x = 38;
            marketStall.y = 101;

            marketInfo.x = 529;
            marketInfo.y = 41;
        }

        private function _setTabs():void
        {
            _tabs = new MarketTabs();
            _tabs.y = 43;
            _tabs.x = 50;
            addChild(_tabs);
        }

        private function _setFilters():void
        {
            _filterPanel = _state.filtersPanel;
            addChild(_filterPanel);
        }

        private function _setupMesh():void
        {
            _mesh = new MarketMesh();
            addChild(_mesh);
        }

        private function _setItemLabels():void
        {
            _txtItemDescription = new AppTextField();
            _txtItemDescription.x = 537;
            _txtItemDescription.y = 155;
            _txtItemDescription.width = 166;
            _txtItemDescription.multiline = true;
            _txtItemDescription.wordWrap = true;
            _txtItemDescription.autoSize = TextFieldAutoSize.LEFT;

            addChild(_txtItemDescription);
        }

        private function _setBuyButton():void
        {
            _buyButton = new BuyButton();
            _buyButton.x = 300;
            _buyButton.y = 305;
            addChild(_buyButton);
        }

        private function _addNavigationBlock():void
        {
            _navigation = new Navigation();
            addChild(_navigation);
            _navigation.y = 305;
            _navigation.x = 70;
        }

        private function _addPrices():void
        {
            _priceTomatos = new TomatosPrice(0);
            addChild(_priceTomatos);
            _priceTomatos.price = 0;
            _priceTomatos.x = 540;
            _priceTomatos.y = 330;

            _priceFerros = new FerrosPrice(0);
            addChild(_priceFerros);
            _priceFerros.price = 0;
            _priceFerros.y = 330;
            _priceFerros.x = 637;

        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get tabs():MarketTabs
        {
            return _tabs;
        }

        public function get filterPanel():FilterTabsPanel
        {
            return _filterPanel;
        }

        public function set filterPanel(value:FilterTabsPanel):void
        {
            _filterPanel = value;
            if (_filterPanel) {
                _filterPanel.y = 107;
                _filterPanel.x = 6;
            }
        }

        public function get mesh():MarketMesh
        {
            return _mesh;
        }

        public function get txtItemDescription():AppTextField
        {
            return _txtItemDescription;
        }

        public function get buyButton():BuyButton
        {
            return _buyButton;
        }

        public function get navigation():Navigation
        {
            return _navigation;
        }

        public function get priceTomatos():TomatosPrice
        {
            return _priceTomatos;
        }

        public function get priceFerros():FerrosPrice
        {
            return _priceFerros;
        }
    }
}
