/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.06.13
 * Time: 11:06
 */
package application.views.screen.market.mesh
{
    import application.models.market.Item;
    import application.views.screen.common.cell.Cell;
    import application.views.screen.common.cell.MarketCell;
    import application.views.screen.common.mesh.Mesh;

    public class MarketMesh extends Mesh
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketMesh(cells:Vector.<Cell> = null)
        {
            super(cells);
            _y0 = 131;
            _x0 = 70;
            _dy = 87;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        public function get item():Item
        {
            if (_selectedCell is MarketCell)
                return (_selectedCell as MarketCell).item;
            else
                return null;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
