/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.10.13
 * Time: 11:32
 */
package application.views.screen.stats
{
    import application.common.Colors;
    import application.models.ModelData;
    import application.models.user.journal.Journal;
    import application.models.user.journal.JournalRecord;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.screen.common.scroll.ScrollObserver;
    import application.views.screen.common.scroll.Scrollbar;
    import application.views.text.AppTextField;

    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;

    public class StatsView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StatsView()
        {
            super();
            _model = ModelsRegistry.getModel(ModelData.USER_JOURNAL) as Journal;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addScrollView();
            _addScrollBar();
            _addPositions();

            _scrollObserver = new ScrollObserver(_scrollContainer, _scrollBar, new Rectangle(0, 0, 620, 245), height);
        }

        override public function destroy():void
        {
            _scrollContainer.removeAllChildrenAndDestroy(true);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _emptyMessage:String = TextFactory.instance.getLabel("stats_empty_label");

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _scrollContainer:View;
        private var _scrollBar:Scrollbar;
        private var _scrollObserver:ScrollObserver;

        private var _model:Journal;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addScrollView():void
        {
            _scrollContainer = new View();
            addChild(_scrollContainer);

            _scrollContainer.x = 52;
            _scrollContainer.y = 221;
        }

        private function _addScrollBar():void
        {
            _scrollBar = new Scrollbar(244);
            addChild(_scrollBar);
            _scrollBar.x = 672;
            _scrollBar.y = 219;
        }

        protected function _addPositions():void
        {
            var list:Object = _model.list.value;
            var dy:int = 125;
            var cy:int = 0;
            var height:int = 0;

            if (!_model.isEmpty) {
                for (var key:String in list) if (list.hasOwnProperty(key)) {
                    var record:JournalRecord = list[key];
                    var position:StatsPosition = new StatsPosition(record);
                    _scrollContainer.addChild(position);
                    position.y = cy;
                    cy += dy;
                    height += 125;
                }
                _scrollContainer.$drawBackground(0);
            }
            else {
                _addEmptyMessage();
            }
        }

        protected function _addEmptyMessage():void
        {
            var txt:AppTextField = new AppTextField();
            txt.size = 16;
            txt.color = Colors.DARK_BROWN;
            txt.width = 676;
            txt.height = 40;
            txt.align = TextFormatAlign.CENTER;
            addChild(txt);
            txt.text = _emptyMessage;
            txt.x = 38;
            txt.y = 300;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get scrollPosition():Number
        {
            return _scrollBar.percent;
        }

        public function set scrollPosition(val:Number):void
        {
            _scrollBar.percent = val;
        }
    }
}
