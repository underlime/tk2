/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.10.13
 * Time: 16:06
 */
package application.views.screen.stats
{
    import application.views.screen.common.text.ShadowAppText;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;

    import framework.core.struct.view.View;

    public class IconText extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function IconText(icon:Bitmap, value:int, margin:int = 5)
        {
            super();
            _icon = icon;
            _value = value;
            _margin = margin;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addIcon();
            _addText();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;
        private var _value:int;

        private var _txtLabel:ShadowAppText;
        private var _margin:int = 5;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addIcon():void
        {
            addChild(_icon);
        }

        private function _addText():void
        {
            _txtLabel = new ShadowAppText(AppTextField.LIGHT_BROWN);
            addChild(_txtLabel);
            _txtLabel.x = _icon.width;
            _txtLabel.y = _margin;
            var prefix:String = _value > 0 ? "+" : "";
            _txtLabel.text = prefix + _value.toString();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
