/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.10.13
 * Time: 11:37
 */
package application.views.screen.stats
{
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.events.InfoEvent;
    import application.models.common.JournalDescription;
    import application.models.user.journal.JournalEventType;
    import application.models.user.journal.JournalRecord;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.helpers.icons.IconType;
    import application.views.helpers.icons.IconsFactory;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.buttons.RejectButton;
    import application.views.screen.common.buttons.SpecialButton;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.events.TextEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;

    import framework.core.enum.Language;
    import framework.core.utils.Singleton;

    public class StatsPosition extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "NEWS_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StatsPosition(model:JournalRecord)
        {
            super();
            super.w = 620;
            super.h = 120;

            _model = model;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _data = {};
            try {
                _data = JSON.parse(_model.description.value);
            }
            catch (e:SyntaxError) {
                _data['login'] = _model.description.value;
            }

            _addBackground();
            _addUpMessage();
            _addMiddleMessage();
            _addProfit();
            if (_model.event_type.value != JournalEventType.WIN.toString())
                _addRevengeButton();
            else
                _addShareButton();
        }

        override public function destroy():void
        {
            if (_tellButton)
                _tellButton.destroy();
            if (_revengeButton)
                _revengeButton.destroy();

            removeAllChildren(true);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:JournalRecord;

        private var _back:Bitmap;
        private var _scrollRect:Rectangle;

        private var _journalDescriptions:JournalDescription;

        private var _tellButton:SpecialButton;
        private var _revengeButton:RejectButton;
        private var _data:Object;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            _back = super.source.getBitmap(BACK);
            addChild(_back);

            _scrollRect = new Rectangle(0, 0, super.w, super.h);
            _back.scrollRect = _scrollRect;

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _addUpMessage():void
        {
            var factory:TextFactory = TextFactory.instance;
            var label:String = _model.event_type.value == JournalEventType.WIN.toString() ? "win_label" : "lose_label";
            var result:String = "<font size='18' color='#be1e2d'><b>" + factory.getLabel(label).toUpperCase() + "</b></font>";
            var dateMsg:String = "  <font color='#3c2415'><b>" + _model.time.getFormattedDate(AppConfig.LANGUAGE == Language.RU) + "</b></font>";

            var upText:AppTextField = new AppTextField();
            upText.autoSize = TextFieldAutoSize.LEFT;
            addChild(upText);
            upText.htmlText = result + dateMsg;
            upText.x = 15;
            upText.y = 10;
        }

        private function _addMiddleMessage():void
        {
            _journalDescriptions = Singleton.getClass(JournalDescription);
            var msg:String = _model.event_type.value == JournalEventType.WIN.toString() ? _journalDescriptions.getWinMessage() : _journalDescriptions.getFailMessage();

            var link:String = '<font color="#be1e2d"><u><a href="event:callGuest">{0}</a></u></font>';
            msg = msg.replace('#player#', link.replace('{0}', _data['login']));

            var txtMiddle:AppTextField = new AppTextField();
            txtMiddle.autoSize = TextFieldAutoSize.LEFT;
            txtMiddle.addEventListener(TextEvent.LINK, _onMessageLink);
            addChild(txtMiddle);
            txtMiddle.mouseEnabled = true;
            txtMiddle.htmlText = "<font color='#3c2415'><b>" + msg + "</b></font>";
            txtMiddle.x = 15;
            txtMiddle.y = 45;
        }

        private function _onMessageLink(e:TextEvent):void
        {
            _dispatchGetInfo();
        }

        private function _dispatchGetInfo(e:Event = null):void
        {
            if (_data['soc_net_id']) {
                var event:InfoEvent = new InfoEvent(InfoEvent.GET_INFO, true);
                event.soc_net_id = _data['soc_net_id'];
                dispatchEvent(event);
            }
        }

        private function _addProfit():void
        {
            var expIcon:Bitmap = IconsFactory.getIcon(IconType.EXPERIENCE_ICO);
            var expPosition:IconText = new IconText(expIcon, _model.experience.value);
            addChild(expPosition);
            expPosition.x = 15;
            expPosition.y = 70;

            var gloryIcon:Bitmap = IconsFactory.getIcon(IconType.GLORY_ICO);
            var gloryPosition:IconText = new IconText(gloryIcon, _model.glory.value);
            addChild(gloryPosition);
            gloryPosition.x = 111;
            gloryPosition.y = 70;

            var tomatosIcon:Bitmap = super.source.getBitmap(CommonData.TOMATOS_RES_ICON);
            var tomatosPosition:IconText = new IconText(tomatosIcon, _model.tomatos.value, 2);
            addChild(tomatosPosition);
            tomatosPosition.x = 207;
            tomatosPosition.y = 72;
        }

        private function _addRevengeButton():void
        {
            _revengeButton = new RejectButton(TextFactory.instance.getLabel('revenge_label'));
            _revengeButton.w = 106;
            _revengeButton.fontSize = 13;
            _revengeButton.addEventListener(MouseEvent.CLICK, _dispatchGetInfo);
            addChild(_revengeButton);
            _revengeButton.x = 498;
            _revengeButton.y = 37;
        }

        private function _addShareButton():void
        {
            _tellButton = new SpecialButton(TextFactory.instance.getLabel("share_label"));
            _tellButton.w = 106;
            _tellButton.fontSize = 13;
            addChild(_tellButton);
            _tellButton.x = 498;
            _tellButton.y = 37;

            _tellButton.addEventListener(MouseEvent.CLICK, _tellClickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _tellClickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            var shareEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.SHARE, true);
            shareEvent.data['login'] = _data['login'];
            shareEvent.data['soc_net_id'] = _data['soc_net_id'];
            dispatchEvent(shareEvent);
        }

        private function _overHandler(event:MouseEvent):void
        {
            _scrollRect.y = super.h;
            _back.scrollRect = _scrollRect;
        }

        private function _outHandler(event:MouseEvent):void
        {
            _scrollRect.y = 0;
            _back.scrollRect = _scrollRect;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
