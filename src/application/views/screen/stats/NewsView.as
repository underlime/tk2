/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.10.13
 * Time: 11:20
 */
package application.views.screen.stats
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.user.journal.News;
    import application.models.user.journal.NewsRecord;

    import framework.core.struct.data.ModelsRegistry;

    public class NewsView extends StatsView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function NewsView(appController:ApplicationController)
        {
            super();
            _appController = appController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addPositions():void
        {
            var dy:int = 125;
            var cy:int = 0;
            var height:int = 0;

            var position:NewsPosition;

            if (!_newsModel.isEmpty) {
                for each (var record:NewsRecord in _newsModel.list.value) {
                    position = new NewsPosition(record, _appController);
                    _scrollContainer.addChild(position);
                    position.y = cy;
                    cy += dy;
                    height += 125;
                }
                _scrollContainer.$drawBackground(0);
            } else {
                _addEmptyMessage();
            }
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _newsModel:News = ModelsRegistry.getModel(ModelData.NEWS) as News;
        private var _appController:ApplicationController;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
