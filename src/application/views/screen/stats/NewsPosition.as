/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.10.13
 * Time: 12:12
 */
package application.views.screen.stats
{
    import application.config.AppConfig;
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.events.InfoEvent;
    import application.models.ModelData;
    import application.models.market.Item;
    import application.models.market.MarketItems;
    import application.models.user.journal.News;
    import application.models.user.journal.NewsRecord;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.popup.common.FerrosPrice;
    import application.views.popup.common.PriceView;
    import application.views.popup.common.TomatosPrice;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.checkbox.AppCheckBox;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.events.TextEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;

    import framework.core.enum.Language;
    import framework.core.helpers.DateHelper;
    import framework.core.struct.data.ModelsRegistry;

    public class NewsPosition extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function NewsPosition(record:NewsRecord, appController:ApplicationController)
        {
            super();
            _record = record;
            _appController = appController;

            _isLangRu = (AppConfig.LANGUAGE == Language.RU);

            super.w = 620;
            super.h = 120;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _parseNewsData();
            _addBackground();
            _addHeader();
            _addMessage();
            if (_newsModel.news_read_ids.value.indexOf(_record.id.value.toString()) == -1) {
                _addLikeButton();
                if (_record.like_reward_type.value && _record.like_reward_type.value.toLowerCase() != 'null')
                    _addPresent();
                _addCheckBox();
            }
        }

        override public function destroy():void
        {
            removeAllChildren(true);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _itemsModel:MarketItems = ModelsRegistry.getModel(ModelData.MARKET_ITEMS) as MarketItems;

        private var _header:String;
        private var _message:String;
        private var _record:NewsRecord;

        private var _back:Bitmap;
        private var _scrollRect:Rectangle;
        private var _likeButton:AcceptButton;

        private var _likeReward:PriceView;
        private var _data:Object;
        private var _isLangRu:Boolean;
        private var _checkBox:AppCheckBox;
        private var _newsModel:News = ModelsRegistry.getModel(ModelData.NEWS) as News;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _parseNewsData():void
        {
            try {
                _extractNewsAdditionalData();
            }
            catch (e:Error) {
                _header = '<font color="#ff0000">Error!</font>';
                _message = '<p>{0}<br>Report this error to underlime.net</p>'.replace('{0}', e.message);
            }
        }

        private function _extractNewsAdditionalData():void
        {
            _data = JSON.parse(_record.additional_data.value);
            switch (_record.news_type.value) {
                case News.TYPE_COMMON:
                    var headerKey:String, textKey:String;
                    if (AppConfig.LANGUAGE == Language.RU) {
                        headerKey = 'h_ru';
                        textKey = 'ru';
                    }
                    else {
                        headerKey = 'h_en';
                        textKey = 'en';
                    }
                    _header = _data[headerKey] || "Breaking news!";
                    _message = _data[textKey];
                    break;
                case News.TYPE_START_BONUS:
                    _header = TextFactory.instance.getLabel('news_start_bonus_label');
                    _message = _getStartBonusMessage();
                    break;
                case News.TYPE_FRIEND:
                    _header = TextFactory.instance.getLabel('new_friend_in_game_label');
                    _message = _getNewFriendMessage();
                    break;
            }
        }

        private function _getNewFriendMessage():String
        {
            return TextFactory.instance.getLabel('new_friend_message')
                    .replace('{login}', _data['login']);
        }

        private function _getStartBonusMessage():String
        {
            var name:String;
            switch (_data['bonus_type']) {
                case 'tomatos':
                    name = TextFactory.instance.getLabel('tomatos_label').toLowerCase();
                    break;
                case 'ferros':
                    name = TextFactory.instance.getLabel('ferros_label').toLowerCase();
                    break;
                default:
                    var item:Item = _itemsModel.getItemById(_data['item_id']);
                    if (item) {
                        var nameKey:String = _isLangRu ? 'name_ru' : 'name_en';
                        name = '«' + item[nameKey].value + '»';
                    }
            }

            return TextFactory.instance.getLabel('start_bonus_message')
                    .replace('{login}', _data['login'])
                    .replace('{gift}', name)
                    .replace('{count}', _data['count']);
        }

        private function _addBackground():void
        {
            _back = super.source.getBitmap(StatsPosition.BACK);
            addChild(_back);

            _scrollRect = new Rectangle(0, 0, super.w, super.h);
            _back.scrollRect = _scrollRect;

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _addHeader():void
        {
            var dateFormat:String = _isLangRu ? "%d.%m.%Y %H:%i" : "%m/%d/%Y %H:%i";

            var result:String = "<font size='18' color='#be1e2d'><b>{0}</b></font>".replace('{0}', _header);
            var dateMsg:String = "  <font color='#3c2415'><b>{0}</b></font>".replace('{0}', DateHelper.formatDate(_record.time.value, dateFormat));

            var upText:AppTextField = new AppTextField();
            upText.autoSize = TextFieldAutoSize.LEFT;
            addChild(upText);
            upText.htmlText = result + dateMsg;
            upText.x = 15;
            upText.y = 10;
        }

        private function _addMessage():void
        {
            var txtMiddle:AppTextField = new AppTextField();
            txtMiddle.width = 430;
            txtMiddle.multiline = true;
            txtMiddle.wordWrap = true;
            txtMiddle.addEventListener(TextEvent.LINK, _onMessageLink);
            addChild(txtMiddle);
            txtMiddle.mouseEnabled = true;
            txtMiddle.htmlText = "<font color='#3c2415'>{0}</font>".replace('{0}', _message);
            txtMiddle.x = 15;
            txtMiddle.y = 45;
        }

        private function _onMessageLink(e:TextEvent):void
        {
            var event:InfoEvent = new InfoEvent(InfoEvent.GET_INFO, true);
            event.soc_net_id = _data['soc_net_id'];
            dispatchEvent(event);
        }

        private function _addLikeButton():void
        {
            _likeButton = new AcceptButton(TextFactory.instance.getLabel('like_label'));
            _likeButton.w = 116;
            _likeButton.fontSize = 12;
            addChild(_likeButton);
            _likeButton.x = 488;
            _likeButton.y = 37;

            _likeButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _addCheckBox():void
        {
            _checkBox = new AppCheckBox(TextFactory.instance.getLabel("share_label"));
            addChild(_checkBox);
            _checkBox.checked = true;
            _checkBox.x = 15;
            _checkBox.y = 75;
        }

        private function _addPresent():void
        {
            switch (_record.like_reward_type.value) {
                case News.REWARD_TOMATOS:
                    _likeReward = new TomatosPrice(_record.like_reward_count.value);
                    break;
                case News.REWARD_FERROS:
                    _likeReward = new FerrosPrice(_record.like_reward_count.value);
                    break;
                default:
                    throw new Error("Wrong reward type");
            }

            _likeReward.prefix = "+";
            addChild(_likeReward);
            _likeReward.y = 84;
            _likeReward.x = 546 - _likeReward.width / 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            _scrollRect.y = super.h;
            _back.scrollRect = _scrollRect;
        }

        private function _outHandler(event:MouseEvent):void
        {
            _scrollRect.y = 0;
            _back.scrollRect = _scrollRect;
        }

        private function _clickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            var likeEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.LIKE, true);
            likeEvent.data['news_id'] = _record.id.value;
            likeEvent.data['share'] = _checkBox.checked;
            likeEvent.data['message'] = _message;
            dispatchEvent(likeEvent);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
