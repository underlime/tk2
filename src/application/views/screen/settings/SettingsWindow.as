/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.07.13
 * Time: 11:37
 */
package application.views.screen.settings
{
    import application.views.TextFactory;
    import application.views.screen.base.BaseScreen;
    import application.views.screen.common.checkbox.AppCheckBox;

    import flash.display.Bitmap;

    public class SettingsWindow extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BLOCK:String = "SETTINGS_BLOCKS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SettingsWindow()
        {
            super(TextFactory.instance.getLabel('settings_label'));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addBlocks();
            _addTextBlocks();
            _addRadioGroup();
            _addGraphicsCheckBox();
            _addCartoonCheckBox();
            _addSoundCheckBox();
            _addColorsGroup();
        }

        override public function destroy():void
        {
            _bloodCheckBox.destroy();
            _cartoonCheckBox.destroy();
            _soundCheckBox.destroy();
            _graphicsRadioGroup.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _blocks:Bitmap;
        private var _graphicsRadioGroup:SettingsRadioGroup;

        private var _bloodCheckBox:AppCheckBox;
        private var _cartoonCheckBox:AppCheckBox;
        private var _soundCheckBox:AppCheckBox;

        private var _colorsGroup:ColorsRadioGroup;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addRadioGroup():void
        {
            _graphicsRadioGroup = new SettingsRadioGroup();
            addChild(_graphicsRadioGroup);
            _graphicsRadioGroup.x = _blocks.x + 350;
            _graphicsRadioGroup.y = _blocks.y + 21;
        }

        private function _addGraphicsCheckBox():void
        {
            _bloodCheckBox = new BloodCheckBox();
            addChild(_bloodCheckBox);
            _bloodCheckBox.x = _blocks.x + 237;
            _bloodCheckBox.y = _blocks.y + 100;
        }

        private function _addCartoonCheckBox():void
        {
            _cartoonCheckBox = new CartoonCheckBox();
            addChild(_cartoonCheckBox);
            _cartoonCheckBox.x = _blocks.x + 615;
            _cartoonCheckBox.y = _blocks.y + 164;
        }

        private function _addSoundCheckBox():void
        {
            _soundCheckBox = new SoundCheckBox();
            addChild(_soundCheckBox);
            _soundCheckBox.x = _blocks.x + 615;
            _soundCheckBox.y = _blocks.y + 219;
        }

        private function _addColorsGroup():void
        {
            _colorsGroup = new ColorsRadioGroup();
            addChild(_colorsGroup);
            _colorsGroup.x = _blocks.x + 461;
            _colorsGroup.y = _blocks.y + 98;
        }

        private function _addBlocks():void
        {
            _blocks = super.source.getBitmap(BLOCK);
            addChild(_blocks);
            _blocks.x = 45;
            _blocks.y = 50;
        }

        private function _addTextBlocks():void
        {
            var factory:TextFactory = TextFactory.instance;
            var grHeader:String = factory.getLabel('graphics_quality_label');
            var grDesc:String = factory.getLabel('graphics_description');

            var graphicQuality:SettingsDescriptionBlock = new SettingsDescriptionBlock(grHeader, grDesc, 260);
            addChild(graphicQuality);
            graphicQuality.x = _blocks.x + 19;
            graphicQuality.y = _blocks.y + 3;

            var bloodHeader:String = factory.getLabel('blood_label');
            var bloodDesc:String = factory.getLabel('blood_description');
            var blood:SettingsDescriptionBlock = new SettingsDescriptionBlock(bloodHeader, bloodDesc, 205);
            addChild(blood);
            blood.x = _blocks.x + 19;
            blood.y = _blocks.y + 78;

            var bloodColorHeader:String = factory.getLabel('blood_color_label');
            var bloodColorDesc:String = factory.getLabel('blood_color_description');
            var bloodColor:SettingsDescriptionBlock = new SettingsDescriptionBlock(bloodColorHeader, bloodColorDesc, 130);
            addChild(bloodColor);
            bloodColor.x = _blocks.x + 309;
            bloodColor.y = _blocks.y + 78;

            var cartoonHeader:String = factory.getLabel('cartoon_border_label');
            var cartoonDesc:String = factory.getLabel('cartoon_description');
            var cartoon:SettingsDescriptionBlock = new SettingsDescriptionBlock(cartoonHeader, cartoonDesc, 270);
            addChild(cartoon);
            cartoon.x = _blocks.x + 19;
            cartoon.y = _blocks.y + 153;

            var soundHeader:String = factory.getLabel('sound_label');
            var soundDesc:String = factory.getLabel('sound_description');
            var sound:SettingsDescriptionBlock = new SettingsDescriptionBlock(soundHeader, soundDesc, 400);
            addChild(sound);
            sound.x = _blocks.x + 19;
            sound.y = _blocks.y + 208;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
