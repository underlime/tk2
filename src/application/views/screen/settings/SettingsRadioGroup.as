/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 11.09.13
 * Time: 11:13
 */
package application.views.screen.settings
{
    import application.config.AppConfig;
    import application.config.AppSettings;
    import application.views.TextFactory;
    import application.views.screen.common.radio.AppRadioButton;

    import flash.display.StageQuality;
    import flash.events.Event;

    import framework.core.display.components.RadioButton;
    import framework.core.display.components.RadioGroup;
    import framework.core.storage.LocalStorage;

    public class SettingsRadioGroup extends RadioGroup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SettingsRadioGroup()
        {
            super(new <RadioButton>[
                new AppRadioButton(TextFactory.instance.getLabel('graphics_low')),
                new AppRadioButton(TextFactory.instance.getLabel('graphics_medium')),
                new AppRadioButton(TextFactory.instance.getLabel('graphics_high'))
            ]);
            _dx = 75;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _setupDefault();
            addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            removeEventListener(Event.CHANGE, _changeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _storage:LocalStorage = LocalStorage.getInstance(AppConfig.APP_NAME);

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupDefault():void
        {
            var graphicsQuality:String = StageQuality.HIGH;
            if (_storage.hasItem(AppSettings.GRAPHICS_QUALITY))
                graphicsQuality = _storage.getItem(AppSettings.GRAPHICS_QUALITY);

            this.quality = graphicsQuality;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            event.stopImmediatePropagation();

            var graphicsQuality:String = this.quality;
            _storage.setItem(AppSettings.GRAPHICS_QUALITY, graphicsQuality);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set quality(value:String):void
        {
            switch (value) {
                case StageQuality.MEDIUM:
                    this.select(0);
                    break;
                case StageQuality.HIGH:
                    this.select(1);
                    break;
                case StageQuality.BEST:
                    this.select(2);
                    break;
            }
        }

        public function get quality():String
        {
            var quality:String = StageQuality.HIGH;
            switch (this.selectedRadio) {
                case 0:
                    return StageQuality.MEDIUM;
                case 1:
                    return StageQuality.HIGH;
                case 2:
                    return StageQuality.BEST;
            }
            return quality;
        }

    }
}
