/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.09.13
 * Time: 11:14
 */
package application.views.screen.settings
{
    import application.config.AppConfig;
    import application.config.AppSettings;
    import application.views.battle.blood.BloodColor;
    import application.views.screen.common.radio.ColoredRadioButton;
    import application.views.screen.common.radio.colors.*;

    import flash.events.Event;

    import framework.core.display.components.RadioButton;
    import framework.core.display.components.RadioGroup;
    import framework.core.storage.LocalStorage;

    public class ColorsRadioGroup extends RadioGroup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ColorsRadioGroup()
        {
            var group:Vector.<RadioButton> = new <RadioButton>[
                new RedRadio(),
                new GreenRadio(),
                new PurpleRadio(),
                new BlueRadio(),
                new YellowRadio()
            ];
            _dx = 10;
            super(group);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _setupDefault();
            addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            removeEventListener(Event.CHANGE, _changeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _storage:LocalStorage = LocalStorage.getInstance(AppConfig.APP_NAME);

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupDefault():void
        {
            var bloodColor:uint = BloodColor.RED;
            if (_storage.hasItem(AppSettings.BLOOD_COLOR))
                bloodColor = uint(_storage.getItem(AppSettings.BLOOD_COLOR));

            this.color = bloodColor;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            event.stopImmediatePropagation();
            var color:uint = (_radioButtons[this.selectedRadio] as ColoredRadioButton).color;
            _storage.setItem(AppSettings.BLOOD_COLOR, color.toString());
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set color(value:uint):void
        {
            var count:int = _radioButtons.length;
            for (var i:int = 0; i < count; i++) {
                var radioButton:ColoredRadioButton = _radioButtons[i] as ColoredRadioButton;
                if (radioButton.color == value)
                    this.select(i);
            }
        }

        public function get color():uint
        {
            var color:uint = BloodColor.RED;

            if (this.selectedRadio > -1) {
                var radio:ColoredRadioButton = _radioButtons[this.selectedRadio] as ColoredRadioButton;
                color = radio.color;
            }

            return color;
        }

    }
}
