/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.09.13
 * Time: 16:45
 */
package application.views.screen.settings
{
    import application.config.AppConfig;
    import application.config.AppSettings;
    import application.views.screen.common.checkbox.AppCheckBox;

    import flash.events.Event;

    import framework.core.storage.LocalStorage;

    public class SoundCheckBox extends AppCheckBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SoundCheckBox()
        {
            super("");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addDefault();
            addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            removeEventListener(Event.CHANGE, _changeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _storage:LocalStorage = LocalStorage.getInstance(AppConfig.APP_NAME);

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addDefault():void
        {
            if (!_storage.hasItem(AppSettings.SOUNDS_ENABLED))
                _storage.setItem(AppSettings.SOUNDS_ENABLED, "1");

            var flag:int = parseInt(_storage.getItem(AppSettings.SOUNDS_ENABLED));
            this.checked = flag > 0;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            event.stopImmediatePropagation();

            var flag:String = this.checked ? "1" : "0";
            _storage.setItem(AppSettings.SOUNDS_ENABLED, flag);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
