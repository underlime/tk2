/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.09.13
 * Time: 21:54
 */
package application.views.screen.settings
{
    import application.views.text.AppTextField;

    import framework.core.struct.view.View;

    public class SettingsDescriptionBlock extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SettingsDescriptionBlock(header:String, description:String, maxWidth:int)
        {
            super();
            _header = header;
            _description = description;
            _maxWidth = maxWidth;
        }

        override protected function render():void
        {
            _addHeader();
            _addDescription();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _maxWidth:int;
        private var _header:String;
        private var _description:String;

        private var _txtHeader:AppTextField;
        private var _txtDescription:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHeader():void
        {
            _txtHeader = new AppTextField();
            _txtHeader.size = 18;
            _txtHeader.color = AppTextField.DARK_BROWN;
            _txtHeader.width = _maxWidth;
            _txtHeader.multiline = true;
            _txtHeader.wordWrap = true;
            _txtHeader.bold = true;

            addChild(_txtHeader);
            _txtHeader.text = _header;
        }

        private function _addDescription():void
        {
            _txtDescription = new AppTextField();
            _txtDescription.size = 12;
            _txtDescription.color = AppTextField.LIGHT_BROWN;
            _txtDescription.width = _maxWidth;
            _txtDescription.multiline = true;
            _txtDescription.wordWrap = true;

            addChild(_txtDescription);
            _txtDescription.text = _description;

            _txtDescription.y = _txtHeader.textHeight;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
