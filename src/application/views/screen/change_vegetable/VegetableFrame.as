/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.09.13
 * Time: 11:11
 */
package application.views.screen.change_vegetable
{
    import application.events.BuyEvent;
    import application.models.ModelData;
    import application.models.chars.CharList;
    import application.models.inventory.InventoryHelper;
    import application.models.market.Item;
    import application.models.market.MarketItems;
    import application.models.user.User;
    import application.models.user.UserInfo;
    import application.models.user.Vegetable;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.constructor.builders.BattleUnit;
    import application.views.screen.home.HomeScreen;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.helpers.MathHelper;
    import framework.core.struct.data.ModelsRegistry;

    public class VegetableFrame extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const FRAME:String = "ACADEMY_CHANGE_TYPE_CHAR_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function VegetableFrame(vegetable:Vegetable, arrangements:Array)
        {
            super();
            _vegetable = vegetable;
            _arrangements = arrangements;

            _chars = ModelsRegistry.getModel(ModelData.CHAR_LIST) as CharList;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _updateUnit();
            _addChangeButton();
        }

        override public function destroy():void
        {
            if (_unit && !_unit.destroyed) {
                _unit.removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
                _unit.destroy();
            }

            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            _updateUnit();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _vegetable:Vegetable;
        private var _unit:BattleUnit;

        private var _chars:CharList;
        private var _items:MarketItems;

        private var _changeButton:ChangeButton;

        private var _arrangements:Array;
        private var _blocked:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var frame:Bitmap = super.source.getBitmap(FRAME);
            addChild(frame);

            var shade:Bitmap = super.source.getBitmap(HomeScreen.SHADOW);
            addChild(shade);
            shade.x = 22;
            shade.y = 152;
        }

        private function _updateUnit():void
        {
            if (_unit && !_unit.destroyed) {
                _unit.removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
                _unit.destroy();
            }

            var userInfo:UserInfo = _chars.getRandomBuildConfiguration(_vegetable);
            var items:Vector.<Item> = InventoryHelper.ItemsArray2Vector([]);

            _unit = new BattleUnit(userInfo, items);

            _unit.scaleX = .8;
            _unit.scaleY = .8;

            _unit.y = 80;
            _unit.x = 28;

            addChild(_unit);
            _unit.dress();

            _unit.mouseEnabled = true;
            _unit.addEventListener(MouseEvent.ROLL_OVER, _overHandler);
        }

        private function _addChangeButton():void
        {
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;
            var price:int = user.configPrices.changeVegetablePrice;

            _changeButton = new ChangeButton(price);
            addChild(_changeButton);
            _changeButton.x = -7;
            _changeButton.y = 197;

            _changeButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            if (!_blocked) {
                _blocked = true;
                _unit.kick(MathHelper.random(0, 3));
                _unit.addEventListener(Event.COMPLETE, _completeHandler);
            }
        }

        private function _completeHandler(event:Event):void
        {
            _unit.removeEventListener(Event.COMPLETE, _completeHandler);
            _blocked = false;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set vegetable(value:Vegetable):void
        {
            _vegetable = value;
            _updateUnit();
        }

        private function _clickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new BuyEvent(BuyEvent.BUY, true));
            super.sound.playUISound(UISound.CLICK);
        }
    }
}
