/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.09.13
 * Time: 19:56
 */
package application.views.screen.change_vegetable
{
    import application.common.ArrangementVariant;
    import application.config.AppConfig;
    import application.models.user.Vegetable;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.screen.office.OfficeScreen;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;

    public class ChangeVegetableView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BOX:String = "ACADEMY_CHANGE_TYPE_BOX_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ChangeVegetableView()
        {
            super();
            this.y = 107;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupHash();
            _addBackground();
            _addRadioGroup();
            _addFrames();
            _addRefreshButton();

            _vegetable = Vegetable.TOMATO;
        }

        override public function destroy():void
        {
            _radioGroup.removeEventListener(Event.CHANGE, _vegetableChangeHandler);
            _radioGroup.destroy();
            _glovesFrame.destroy();
            _twoHandedFrame.destroy();
            _rodFrame.destroy();
            _refreshButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _radioGroup:VegetablesRadioGroup;

        private var _glovesFrame:VegetableFrame;
        private var _rodFrame:VegetableFrame;
        private var _twoHandedFrame:VegetableFrame;

        private var _hash:Object = {};

        private var _refreshButton:RefreshButton;
        private var _vegetable:Vegetable;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupHash():void
        {
            _hash = {
                0: Vegetable.TOMATO,
                1: Vegetable.CUCUMBER,
                2: Vegetable.PEPPER,
                3: Vegetable.CARROT,
                4: Vegetable.BETA,
                5: Vegetable.ONION
            }
        }

        private function _addBackground():void
        {
            var back:Bitmap = super.source.getBitmap(OfficeScreen.BACK);
            addChild(back);
            back.x = 38;
            back.y = 101;

            var box:Bitmap = super.source.getBitmap(BOX);
            addChild(box);
            box.x = 50;
            box.y = 113;
        }

        private function _addRadioGroup():void
        {
            _radioGroup = new VegetablesRadioGroup();
            addChild(_radioGroup);

            _radioGroup.x = 63;
            _radioGroup.y = 126;
            _radioGroup.select(0);

            _radioGroup.addEventListener(Event.CHANGE, _vegetableChangeHandler);
        }

        private function _addFrames():void
        {
            _glovesFrame = new VegetableFrame(Vegetable.TOMATO, [ArrangementVariant.GLOVES, ArrangementVariant.ONE_HANDED]);
            _rodFrame = new VegetableFrame(Vegetable.TOMATO, [ArrangementVariant.ROD_WEAPON]);
            _twoHandedFrame = new VegetableFrame(Vegetable.TOMATO, [ArrangementVariant.TWO_HANDED]);

            addChild(_glovesFrame);
            addChild(_rodFrame);
            addChild(_twoHandedFrame);

            _glovesFrame.x = 214;
            _rodFrame.x = 380;
            _twoHandedFrame.x = 545;

            _glovesFrame.y = 117;
            _rodFrame.y = 117;
            _twoHandedFrame.y = 117;
        }

        private function _addRefreshButton():void
        {
            _refreshButton = new RefreshButton();
            addChild(_refreshButton);
            _refreshButton.x = AppConfig.APP_WIDTH - 36 - _refreshButton.w;
            _refreshButton.y = 45;
            _refreshButton.addEventListener(MouseEvent.CLICK, _refreshClickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _vegetableChangeHandler(event:Event):void
        {
            var selectedItem:int = _radioGroup.selectedRadio;
            _vegetable = _hash[selectedItem];
            _rodFrame.vegetable = _vegetable;
            _twoHandedFrame.vegetable = _vegetable;
            _glovesFrame.vegetable = _vegetable;
        }

        private function _refreshClickHandler(event:MouseEvent):void
        {
            _glovesFrame.update();
            _twoHandedFrame.update();
            _rodFrame.update();
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get vegetable():Vegetable
        {
            return _vegetable;
        }
    }
}
