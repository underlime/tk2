/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.09.13
 * Time: 21:17
 */
package application.views.screen.change_vegetable
{
    import application.views.TextFactory;
    import application.views.screen.common.radio.AppRadioButton;

    import framework.core.display.components.RadioButton;
    import framework.core.display.components.RadioGroup;

    public class VegetablesRadioGroup extends RadioGroup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function VegetablesRadioGroup()
        {
            var radios:Vector.<RadioButton> = new <RadioButton>[
                new AppRadioButton(TextFactory.instance.getLabel('tomato_label')),
                new AppRadioButton(TextFactory.instance.getLabel('cucumber_label')),
                new AppRadioButton(TextFactory.instance.getLabel('pepper_label')),
                new AppRadioButton(TextFactory.instance.getLabel('carrot_label')),
                new AppRadioButton(TextFactory.instance.getLabel('beta_label')),
                new AppRadioButton(TextFactory.instance.getLabel('onion_label'))
            ];
            super(radios);
            _dy = 9;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
