/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 05.09.13
 * Time: 19:27
 */
package application.views.screen.train
{
    import application.events.TrainEvent;
    import application.models.ModelData;
    import application.models.user.User;
    import application.sound.lib.UISound;
    import application.views.AppView;

    import flash.events.MouseEvent;

    import framework.core.display.buttons.SpriteButton;
    import framework.core.struct.data.ModelsRegistry;

    public class TrainView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const FOOTER:String = "ACADEMY_TRAIN_FOOTER";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrainView()
        {
            super();
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addLayout();
            _bindData();

            if (_user.userLevelParams.points_learning.value > 0)
                _bindReactions();

            _disableButtons();
        }

        override public function destroy():void
        {
            _strengthPlus.removeEventListener(MouseEvent.CLICK, _strengthPlusHandler);
            _strengthMinus.removeEventListener(MouseEvent.CLICK, _strengthMinusHandler);

            _agilityPlus.removeEventListener(MouseEvent.CLICK, _agilityPlusHandler);
            _agilityMinus.removeEventListener(MouseEvent.CLICK, _agilityMinusHandler);

            _intellectPlus.removeEventListener(MouseEvent.CLICK, _intellectPlusHandler);
            _intellectMinus.removeEventListener(MouseEvent.CLICK, _intellectMinusHandler);

            _cancelButton.removeEventListener(MouseEvent.CLICK, _cancelClickHandler);
            _confirmButton.removeEventListener(MouseEvent.CLICK, _confirmClickHandler);

            _layout.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            _disableButtons();
            _pointsView.flush();

            _layout.strengthPosition.flush();
            _layout.agilityPosition.flush();
            _layout.intellectPosition.flush();

            _layout.update();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _layout:TrainLayout;
        private var _user:User;

        private var _pointsView:LearningPointView;

        private var _strengthPlus:SpriteButton;
        private var _strengthMinus:SpriteButton;

        private var _agilityPlus:SpriteButton;
        private var _agilityMinus:SpriteButton;

        private var _intellectPlus:SpriteButton;
        private var _intellectMinus:SpriteButton;

        private var _cancelButton:SpriteButton;
        private var _confirmButton:SpriteButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addLayout():void
        {
            _layout = new TrainLayout();
            addChild(_layout);
        }

        private function _bindData():void
        {
            _pointsView = _layout.learningPoints;

            _strengthMinus = _layout.strengthPosition.minusButton;
            _strengthPlus = _layout.strengthPosition.plusButton;

            _agilityPlus = _layout.agilityPosition.plusButton;
            _agilityMinus = _layout.agilityPosition.minusButton;

            _intellectPlus = _layout.intellectPosition.plusButton;
            _intellectMinus = _layout.intellectPosition.minusButton;

            _cancelButton = _layout.cancelButton;
            _confirmButton = _layout.okButton;
        }

        private function _bindReactions():void
        {
            _strengthPlus.addEventListener(MouseEvent.CLICK, _strengthPlusHandler);
            _strengthMinus.addEventListener(MouseEvent.CLICK, _strengthMinusHandler);

            _agilityPlus.addEventListener(MouseEvent.CLICK, _agilityPlusHandler);
            _agilityMinus.addEventListener(MouseEvent.CLICK, _agilityMinusHandler);

            _intellectPlus.addEventListener(MouseEvent.CLICK, _intellectPlusHandler);
            _intellectMinus.addEventListener(MouseEvent.CLICK, _intellectMinusHandler);

            _cancelButton.addEventListener(MouseEvent.CLICK, _cancelClickHandler);
            _confirmButton.addEventListener(MouseEvent.CLICK, _confirmClickHandler);
        }

        private function _increasePosition(position:TrainPosition):void
        {
            if (_pointsView.points > 0) {
                var addendum:int = _layout.multiplier > _pointsView.points ? _pointsView.points : _layout.multiplier;
                position.currentValue += addendum;
                _pointsView.points -= addendum;

                position.showWin();
            } else {
                position.showFail();
            }

            if (_pointsView.points != _pointsView.defaultValue) {
                _enableButtons();
            } else {
                _disableButtons();
            }
        }

        private function _reducePosition(position:TrainPosition):void
        {
            var def:int = position.defaultValue;

            if (position.currentValue > def) {
                var diff:int = position.currentValue - def;
                var subtrahend:int = _layout.multiplier > diff ? diff : _layout.multiplier;
                _pointsView.points += subtrahend;
                position.currentValue -= subtrahend;

                position.showWin();
            } else {
                position.showFail();
            }

            if (_pointsView.points != _pointsView.defaultValue) {
                _enableButtons();
            } else {
                _disableButtons();
            }
        }

        private function _enableButtons():void
        {
            _cancelButton.enable();
            _confirmButton.enable();
        }

        private function _disableButtons():void
        {
            _cancelButton.block();
            _confirmButton.block();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _strengthPlusHandler(event:MouseEvent):void
        {
            _increasePosition(_layout.strengthPosition);
            super.sound.playUISound(UISound.CLICK);
        }

        private function _strengthMinusHandler(event:MouseEvent):void
        {
            _reducePosition(_layout.strengthPosition);
            super.sound.playUISound(UISound.CLICK);
        }

        private function _agilityPlusHandler(event:MouseEvent):void
        {
            _increasePosition(_layout.agilityPosition);
            super.sound.playUISound(UISound.CLICK);
        }

        private function _agilityMinusHandler(event:MouseEvent):void
        {
            _reducePosition(_layout.agilityPosition);
            super.sound.playUISound(UISound.CLICK);
        }

        private function _intellectPlusHandler(event:MouseEvent):void
        {
            _increasePosition(_layout.intellectPosition);
            super.sound.playUISound(UISound.CLICK);
        }

        private function _intellectMinusHandler(event:MouseEvent):void
        {
            _reducePosition(_layout.intellectPosition);
            super.sound.playUISound(UISound.CLICK);
        }

        private function _cancelClickHandler(event:MouseEvent):void
        {
            if (!_cancelButton.blocked) {
                _layout.strengthPosition.flush();
                _layout.agilityPosition.flush();
                _layout.intellectPosition.flush();

                _pointsView.flush();
                _disableButtons();
                super.sound.playUISound(UISound.CLICK);
            }
        }

        private function _confirmClickHandler(e:MouseEvent):void
        {
            if (!_confirmButton.blocked) {
                var event:TrainEvent = new TrainEvent(TrainEvent.TRAIN);
                event.strength = _layout.strengthPosition.currentValue - _layout.strengthPosition.defaultValue;
                event.agility = _layout.agilityPosition.currentValue - _layout.agilityPosition.defaultValue;
                event.intellect = _layout.intellectPosition.currentValue - _layout.intellectPosition.defaultValue;
                super.dispatchEvent(event);
                super.sound.playUISound(UISound.CLICK);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
