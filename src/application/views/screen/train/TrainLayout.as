/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 05.09.13
 * Time: 20:21
 */
package application.views.screen.train
{
    import application.common.SkillType;
    import application.config.AppConfig;
    import application.events.TrainEvent;
    import application.helpers.ResetPointsHelper;
    import application.models.ModelData;
    import application.models.user.User;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.buttons.RejectButton;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.screen.office.OfficeScreen;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.data.ModelsRegistry;

    public class TrainLayout extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrainLayout()
        {
            super();
            this.y = 107;

            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addPositions();
            _addFlushButton();
            _addRadioGroup();
            _addRadioLabel();
            _addLearningPoints();
            _addButtons();
        }

        override public function destroy():void
        {
            _flushButton.removeEventListener(MouseEvent.CLICK, _flushClickHandler);

            _radioGroup.destroy();
            _flushButton.destroy();
            _cancelButton.destroy();
            _learningPoints.destroy();
            _okButton.destroy();
            _cancelButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            if (_flushButton)
                _flushButton.price = _getFlushPrice();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:User;

        private var _flushButton:FlushButton;
        private var _radioGroup:DistributionRadioGroup;

        private var _learningPoints:LearningPointView;

        private var _okButton:AcceptButton;
        private var _cancelButton:RejectButton;

        private var _strengthPosition:TrainPosition;
        private var _agilityPosition:TrainPosition;
        private var _intellectPosition:TrainPosition;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var back:Bitmap = super.source.getBitmap(OfficeScreen.BACK);
            addChild(back);
            back.x = 38;
            back.y = 101;

            var footer:Bitmap = super.source.getBitmap(TrainView.FOOTER);
            addChild(footer);
            footer.x = 50;
            footer.y = 305;
        }

        private function _addFlushButton():void
        {
            _flushButton = new FlushButton(_getFlushPrice());
            addChild(_flushButton);
            _flushButton.x = AppConfig.APP_WIDTH - 38 - _flushButton.w;
            _flushButton.y = 49;

            if (_user.userLevelParams.max_level.value < 2)
                _flushButton.block();
            else
                _flushButton.addEventListener(MouseEvent.CLICK, _flushClickHandler);
        }

        private function _getFlushPrice():int
        {
            return ResetPointsHelper.getPrice(_user.configPrices.reset_skills_ferros_price.value, _user.userLevelParams.skill_resets_count.value);
        }

        private function _addRadioGroup():void
        {
            _radioGroup = new DistributionRadioGroup();
            addChild(_radioGroup);
            _radioGroup.x = 57;
            _radioGroup.y = 318;
            _radioGroup.select(0);
        }

        private function _addRadioLabel():void
        {
            var txtLabel:ShadowAppText = new ShadowAppText(AppTextField.DARK_BROWN);
            txtLabel.size = 14;
            txtLabel.autoSize = TextFieldAutoSize.NONE;
            txtLabel.width = 238;
            txtLabel.align = TextFormatAlign.CENTER;
            addChild(txtLabel);
            txtLabel.y = 114;
            txtLabel.text = TextFactory.instance.getLabel("distribution_points");
            txtLabel.x = 50;
            txtLabel.y = 293;
        }

        private function _addLearningPoints():void
        {
            _learningPoints = new LearningPointView();
            addChild(_learningPoints);
            _learningPoints.x = 382;
            _learningPoints.y = 302;

            var txt:AppTextField = new AppTextField();
            txt.size = 10;
            txt.bold = true;
            txt.color = AppTextField.DARK_BROWN;
            txt.align = TextFormatAlign.RIGHT;
            txt.width = 60;
            txt.multiline = true;
            txt.wordWrap = true;
            addChild(txt);
            txt.text = TextFactory.instance.getLabel('points_remain_label')
            txt.x = 314;
            txt.y = 310;
        }

        private function _addButtons():void
        {
            _okButton = new AcceptButton("OK");
            _okButton.w = 106;
            _okButton.fontSize = 14;
            addChild(_okButton);
            _okButton.x = 587;
            _okButton.y = 313;


            _cancelButton = new RejectButton(TextFactory.instance.getLabel('cancel_label'));
            _cancelButton.w = 106;
            _cancelButton.fontSize = 14;
            addChild(_cancelButton);
            _cancelButton.x = 472;
            _cancelButton.y = 313;
        }

        private function _addPositions():void
        {
            _strengthPosition = new TrainPosition(SkillType.STRENGTH);
            addChild(_strengthPosition);
            _strengthPosition.x = 65;
            _strengthPosition.y = 120;

            _agilityPosition = new TrainPosition(SkillType.AGILITY);
            addChild(_agilityPosition);
            _agilityPosition.x = 282;
            _agilityPosition.y = 120;

            _intellectPosition = new TrainPosition(SkillType.INTELLECT);
            addChild(_intellectPosition);
            _intellectPosition.x = 500;
            _intellectPosition.y = 120;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _flushClickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new TrainEvent(TrainEvent.RESET_SKILLS, true));
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get strengthPosition():TrainPosition
        {
            return _strengthPosition;
        }

        public function get agilityPosition():TrainPosition
        {
            return _agilityPosition;
        }

        public function get intellectPosition():TrainPosition
        {
            return _intellectPosition;
        }

        public function get learningPoints():LearningPointView
        {
            return _learningPoints;
        }

        public function get multiplier():int
        {
            if (_radioGroup.selectedRadio == 0)
                return 1;
            if (_radioGroup.selectedRadio == 1)
                return 10;
            if (_radioGroup.selectedRadio == 2)
                return 100;

            return 1;
        }

        public function get cancelButton():RejectButton
        {
            return _cancelButton;
        }

        public function get okButton():AcceptButton
        {
            return _okButton;
        }
    }
}
