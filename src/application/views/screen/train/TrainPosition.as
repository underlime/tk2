/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 07.09.13
 * Time: 20:34
 */
package application.views.screen.train
{
    import application.common.SkillType;
    import application.models.ModelData;
    import application.models.user.User;
    import application.views.TextFactory;
    import application.views.constructor.builders.TrainBuilder;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;

    public class TrainPosition extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "CHAR_BG_ACADEM";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrainPosition(skillType:SkillType)
        {
            super();
            _skillType = skillType;

            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addLabel();
            _addButtons();
            _setSkillValue();
            _addUnit();
            _addSkillValue();
        }

        override public function destroy():void
        {
            _minusButton.destroy();
            _plusButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function flush():void
        {
            this.currentValue = _getModelValue();
        }

        public function showWin():void
        {
            if (_unit)
                _unit.showSuccess();
        }

        public function showFail():void
        {
            if (_unit)
                _unit.showFail();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _skillType:SkillType;

        private var _minusButton:TrainMinusButton;
        private var _plusButton:TrainPlusButton;

        private var _user:User;
        private var _currentValue:int = 0;

        private var _txtSkill:ShadowAppText;
        private var _unit:TrainBuilder;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var back:Bitmap = super.source.getBitmap(BACK);
            addChild(back);
        }

        private function _addLabel():void
        {
            var txtLabel:ShadowAppText = new ShadowAppText(AppTextField.DARK_BROWN);
            txtLabel.size = 14;
            txtLabel.autoSize = TextFieldAutoSize.NONE;
            txtLabel.width = 184;
            txtLabel.align = TextFormatAlign.CENTER;
            addChild(txtLabel);
            txtLabel.y = 114;
            txtLabel.text = _getLabel();
        }

        private function _getLabel():String
        {
            if (_skillType == SkillType.STRENGTH)
                return TextFactory.instance.getLabel('strength_label');
            if (_skillType == SkillType.AGILITY)
                return TextFactory.instance.getLabel('agility_label');
            if (_skillType == SkillType.INTELLECT)
                return TextFactory.instance.getLabel('intellect_label');

            return TextFactory.instance.getLabel('strength_label');
        }

        private function _addButtons():void
        {
            _minusButton = new TrainMinusButton(_skillType);
            _minusButton.scrollRect = new Rectangle(0, 0, _minusButton.w, _minusButton.h);

            addChild(_minusButton);
            _minusButton.x = 28;
            _minusButton.y = 133;

            _plusButton = new TrainPlusButton(_skillType);
            _plusButton.scrollRect = new Rectangle(0, 0, _plusButton.w, _plusButton.h);

            addChild(_plusButton);
            _plusButton.x = 128;
            _plusButton.y = 133;
        }

        private function _setSkillValue():void
        {
            _currentValue = _getModelValue();
        }

        private function _addSkillValue():void
        {
            _txtSkill = new ShadowAppText(AppTextField.LIGHT_BROWN);
            _txtSkill.size = 16;
            _txtSkill.autoSize = TextFieldAutoSize.NONE;
            _txtSkill.width = 70;
            _txtSkill.height = 30;
            _txtSkill.align = TextFormatAlign.CENTER;
            addChild(_txtSkill);
            _txtSkill.text = _currentValue.toString();

            _txtSkill.x = 57;
            _txtSkill.y = 133;
        }

        private function _addUnit():void
        {
            _unit = new TrainBuilder(_user.userInfo, _skillType);
            addChild(_unit);
            _unit.scaleX = .6;
            _unit.scaleY = .6;

            _unit.x = 84;
            if (_skillType == SkillType.STRENGTH)
                _unit.x = 70;

            _unit.y = 45;

        }

        private function _getModelValue():int
        {
            if (_skillType == SkillType.STRENGTH)
                return _user.userLevelParams.strength.value;
            if (_skillType == SkillType.AGILITY)
                return _user.userLevelParams.agility.value;
            if (_skillType == SkillType.INTELLECT)
                return _user.userLevelParams.intellect.value;

            return _user.userLevelParams.strength.value;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get minusButton():TrainMinusButton
        {
            return _minusButton;
        }

        public function get plusButton():TrainPlusButton
        {
            return _plusButton;
        }

        public function set currentValue(value:int):void
        {
            _currentValue = value;
            if (_txtSkill)
                _txtSkill.text = _currentValue.toString();
        }

        public function get currentValue():int
        {
            return _currentValue;
        }

        public function get defaultValue():int
        {
            return _getModelValue();
        }
    }
}
