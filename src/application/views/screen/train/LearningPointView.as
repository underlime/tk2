/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 05.09.13
 * Time: 20:27
 */
package application.views.screen.train
{
    import application.models.ModelData;
    import application.models.user.User;
    import application.models.user.UserLevelParams;
    import application.views.screen.common.UserLearningPoints;

    import flash.events.Event;

    import framework.core.struct.data.ModelsRegistry;

    public class LearningPointView extends UserLearningPoints
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LearningPointView()
        {
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;
            _model = user.userLevelParams;
            super(_model.points_learning.value);

            _model.addEventListener(Event.CHANGE, _changeHandler);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            _model.removeEventListener(Event.CHANGE, _changeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function flush():void
        {
            super.points = _model.points_learning.value;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:UserLevelParams;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            super.points = _model.points_learning.value;
        }

        // ACCESSORS ---------------------------------------------------------------------------/


        public function get defaultValue():int
        {
            return _model.points_learning.value;
        }
    }
}
