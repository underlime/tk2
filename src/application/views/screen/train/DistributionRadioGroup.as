/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 05.09.13
 * Time: 20:12
 */
package application.views.screen.train
{

    import application.views.TextFactory;
    import application.views.screen.common.radio.AppRadioButton;

    import framework.core.display.components.RadioButton;
    import framework.core.display.components.RadioGroup;

    public class DistributionRadioGroup extends RadioGroup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DistributionRadioGroup()
        {
            super(new <RadioButton>[
                new AppRadioButton(TextFactory.instance.getLabel('for_1')),
                new AppRadioButton(TextFactory.instance.getLabel('for_10')),
                new AppRadioButton(TextFactory.instance.getLabel('for_100'))
            ]);
            _dx = 45;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
