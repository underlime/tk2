/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 08.09.13
 * Time: 8:56
 */
package application.views.screen.train
{
    import application.common.SkillType;
    import application.views.screen.common.buttons.AppSpriteButton;

    public class TrainPlusButton extends AppSpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const STRENGTH_BUTTON:String = "PLUS_BTN_FERROS";
        public static const AGILITY_BUTTON:String = "PLUS_BTN_LVL_UP";
        public static const INTELLECT_BUTTON:String = "PLUS_BTN_TOMATOS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrainPlusButton(skillType:SkillType)
        {
            _skillType = skillType;
            var source:String = _getSource();

            super(source);
            _w = 30;
            _h = 30;
            _outPosition = 0;
            _overPosition = 30;
            _downPosition = 60;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _skillType:SkillType;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getSource():String
        {
            if (_skillType == SkillType.STRENGTH)
                return STRENGTH_BUTTON;
            if (_skillType == SkillType.AGILITY)
                return AGILITY_BUTTON;
            if (_skillType == SkillType.INTELLECT)
                return INTELLECT_BUTTON;
            return STRENGTH_BUTTON;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
