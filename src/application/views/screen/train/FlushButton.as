/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 05.09.13
 * Time: 19:54
 */
package application.views.screen.train
{
    import application.views.TextFactory;
    import application.views.screen.common.buttons.FerrosPriceButton;

    public class FlushButton extends FerrosPriceButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FlushButton(price:int)
        {
            super(TextFactory.instance.getLabel('flush_points_label'), price);
            _w = 276;
            _iconMargin = 20;
            _fontSize = 12;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set price(value:int):void
        {
            if (_txtPrice) {
                _txtPrice.text = value.toString();
            }
        }
    }
}
