/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.11.13
 * Time: 16:09
 */
package application.views.screen.gmo
{
    import application.common.Colors;
    import application.views.TextFactory;
    import application.views.screen.base.BaseScreen;
    import application.views.screen.home.HomeScreen;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.text.TextFieldAutoSize;

    public class GMOScreen extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const CHAR:String = "MAD_CUCUMBA_COMBINE";
        public static const BUBBLE:String = "BUBBLE";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GMOScreen()
        {
            super(TextFactory.instance.getLabel("gmo_label"));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addBubble();
            _addUnit();
            _addTiles();
            _addRadioGroup();
        }

        override public function destroy():void
        {
            _radioGroup.destroy();
            _tiles.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _char:MovieClip;
        private var _radioGroup:GmoRadioGroup;
        private var _tiles:GmoTiles;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addUnit():void
        {
            var shade:Bitmap = super.source.getBitmap(HomeScreen.SHADOW);
            shade.x = 105;
            shade.y = 355;
            addChild(shade);

            _char = super.source.getMovieClip(CHAR);
            _char.x = 105;
            _char.y = 233;
            addChild(_char);
        }

        private function _addBubble():void
        {
            var bubble:Bitmap = super.source.getBitmap(BUBBLE);
            bubble.x = 42;
            bubble.y = 43;
            addChild(bubble);

            var txt:AppTextField = new AppTextField({
                "color": Colors.DARK_BROWN,
                "size": 12,
                "width": 177,
                "bold": true,
                "autoSize": TextFieldAutoSize.LEFT,
                "wordWrap": true,
                "multiline": true,
                "x": bubble.x + 18,
                "y": bubble.y + 13
            });
            addChild(txt);
            txt.text = TextFactory.instance.getLabel("gmo_desc");
        }

        private function _addRadioGroup():void
        {
            _radioGroup = new GmoRadioGroup();
            _radioGroup.x = 62;
            _radioGroup.y = 128;
            addChild(_radioGroup);
            _radioGroup.addEventListener(Event.CHANGE, _changeHandler);
            _radioGroup.select(0);
        }

        private function _addTiles():void
        {
            _tiles = new GmoTiles();
            _tiles.x = 265;
            _tiles.y = 43;
            addChild(_tiles);
            _tiles.update(0);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            var period:int = _radioGroup.selectedRadio;
            if (_tiles)
                _tiles.update(period);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get period():int
        {
            if (_radioGroup)
                return _radioGroup.selectedRadio;
            return 0;
        }
    }
}
