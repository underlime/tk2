/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.11.13
 * Time: 17:24
 */
package application.views.screen.gmo
{
    import application.views.screen.gmo.tiles.ArmorPosition;
    import application.views.screen.gmo.tiles.DamagePosition;
    import application.views.screen.gmo.tiles.EnergyPosition;
    import application.views.screen.gmo.tiles.ExpPosition;
    import application.views.screen.gmo.tiles.GloryPosition;
    import application.views.screen.gmo.tiles.GmoPosition;
    import application.views.screen.gmo.tiles.TomatosPosition;

    import flash.display.DisplayObject;

    import framework.core.display.components.TileList;

    public class GmoTiles extends TileList
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoTiles()
        {
            var positions:Vector.<DisplayObject> = new <DisplayObject>[
                _expPosition,
                _gloryPosition,
                _energyPosition,
                _damagePosition,
                _armorPosition,
                _tomatosPosition
            ];
            super(positions);

            _dx = 150;
            _dy = 165;

            _x0 = 0;
            _y0 = 0;

            _cols = 3;
            _lines = 3;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            super.setup();
            _positions.push(
                    _expPosition,
                    _gloryPosition,
                    _energyPosition,
                    _damagePosition,
                    _armorPosition,
                    _tomatosPosition
            );
        }

        override protected function render():void
        {
            super.render();
            super.draw();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        /**
         * Обновить цены
         */
        public function update(period:int):void
        {
            for (var i:int = 0; i < _positions.length; i++) {
                var gmoPosition:GmoPosition = _positions[i];
                gmoPosition.update(period);
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _expPosition:ExpPosition = new ExpPosition();
        private var _gloryPosition:GloryPosition = new GloryPosition();
        private var _energyPosition:EnergyPosition = new EnergyPosition();
        private var _damagePosition:DamagePosition = new DamagePosition();
        private var _armorPosition:ArmorPosition = new ArmorPosition();
        private var _tomatosPosition:TomatosPosition = new TomatosPosition();

        private var _positions:Array = [];

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/


    }
}
