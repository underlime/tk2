/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.11.13
 * Time: 18:14
 */
package application.views.screen.gmo.tiles
{
    import application.views.TextFactory;
    import application.views.screen.gmo.GmoType;

    public class ExpPosition extends GmoPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ExpPosition()
        {
            super(GmoType.EXPERIENCE);
            _label = TextFactory.instance.getLabel("exp_label").toUpperCase() + " х2";
            _offset = 0;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function update(period:int):void
        {
            super.price = _model.getExperiencePrice(period);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
