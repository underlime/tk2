/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.11.13
 * Time: 18:20
 */
package application.views.screen.gmo.tiles
{
    import application.views.TextFactory;
    import application.views.screen.gmo.GmoType;

    public class TomatosPosition extends GmoPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TomatosPosition()
        {
            super(GmoType.TOMATOS);
            _label = TextFactory.instance.getLabel("tomatos_label").toUpperCase() + " х2";
            _offset = 350;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function update(period:int):void
        {
            super.price = _model.getTomatosPrice(period);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
