/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.11.13
 * Time: 18:15
 */
package application.views.screen.gmo.tiles
{
    import application.views.TextFactory;
    import application.views.screen.gmo.GmoType;

    public class GloryPosition extends GmoPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GloryPosition()
        {
            super(GmoType.GLORY);
            _label = TextFactory.instance.getLabel("glory_label").toUpperCase() + " х2";
            _offset = 70;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function update(period:int):void
        {
            super.price = _model.getGloryPrice(period);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
