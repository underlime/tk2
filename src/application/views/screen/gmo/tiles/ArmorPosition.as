/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.11.13
 * Time: 18:19
 */
package application.views.screen.gmo.tiles
{
    import application.views.TextFactory;
    import application.views.screen.gmo.GmoType;

    public class ArmorPosition extends GmoPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArmorPosition()
        {
            super(GmoType.ARMOR);
            _label = TextFactory.instance.getLabel("armor_label").toUpperCase() + "++";
            _offset = 280;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function update(period:int):void
        {
            super.price = _model.getArmorPrice(period);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
