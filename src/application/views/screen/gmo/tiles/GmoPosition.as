/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.11.13
 * Time: 16:59
 */
package application.views.screen.gmo.tiles
{
    import application.common.Colors;
    import application.events.GmoEvent;
    import application.models.ModelData;
    import application.models.gmo.GmoHints;
    import application.models.gmo.GmoPrices;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.popup.common.FerrosPrice;
    import application.views.screen.common.buttons.SpecialButton;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.screen.gmo.*;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.utils.Singleton;

    public class GmoPosition extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "MONEY_BG";
        public static const POSITION_WIDTH:int = 140;
        public static const POSITION_HEIGHT:int = 160;

        public static const ICONS:String = "GMO_CENTER_ICONS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoPosition(gmoType:GmoType)
        {
            super();
            _gmoType = gmoType;
            _model = ModelsRegistry.getModel(ModelData.GMO_PRICES) as GmoPrices;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addIcon();
            _addNote();
            _addBuyButton();
            _addPrice();
            _addHint();
            _bindReactions();
        }

        public function update(period:int):void
        {
            throw new Error("Method mus be overriden!");
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            _buyButton.destroy();
            _ferrosPrice.destroy();
            _hintView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _offset:int = 0;
        protected var _label:String = "позиция";
        protected var _price:int = 0;

        protected var _model:GmoPrices;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _gmoType:GmoType;
        private var _icon:Bitmap;

        private var _background:Bitmap;
        private var _scrollRect:Rectangle = new Rectangle(0, 0, POSITION_WIDTH, POSITION_HEIGHT);

        private var _txtNote:AppTextField;
        private var _buyButton:SpecialButton;

        private var _ferrosPrice:FerrosPrice;
        private var _hintView:GmoHint;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            _background = super.source.getBitmap(BACK);
            _background.scrollRect = _scrollRect;

            addChild(_background);
        }

        private function _addIcon():void
        {
            _icon = super.source.getBitmap(ICONS);
            _icon.scrollRect = new Rectangle(0, _offset, 80, 80);
            addChild(_icon);
            _icon.x = POSITION_WIDTH / 2 - _icon.width / 2;
            _icon.y = 23;
        }

        private function _addNote():void
        {
            var header:ShadowAppText = new ShadowAppText(Colors.BROWN, {
                "align": TextFormatAlign.CENTER,
                "size": 14,
                "y": 7
            });
            header.autoSize = TextFieldAutoSize.NONE;
            header.width = _background.width;
            addChild(header);
            header.text = _label;
        }

        private function _addBuyButton():void
        {
            _buyButton = new SpecialButton(TextFactory.instance.getLabel("buy_label"));
            _buyButton.w = 126;
            _buyButton.fontSize = 12;
            addChild(_buyButton);
            _buyButton.x = 7;
            _buyButton.y = 113;

            _buyButton.addEventListener(MouseEvent.CLICK, _buyClickHandler);
        }

        private function _addPrice():void
        {
            _ferrosPrice = new FerrosPrice(_price);
            addChild(_ferrosPrice);
            _ferrosPrice.y = 85;
            _ferrosPrice.x = _background.width / 2 - _ferrosPrice.priceWidth / 2;
        }

        private function _addHint():void
        {
            var hints:GmoHints = Singleton.getClass(GmoHints);
            _hintView = new GmoHint(_label, hints.getHint(_gmoType));
            addChild(_hintView);
            var cords:Point = localToGlobal(new Point(-1 * _hintView.w - 10, POSITION_HEIGHT / 2 - _hintView.h / 2));
            removeChild(_hintView);
            _hintView.x = cords.x;
            _hintView.y = int(cords.y);
        }

        private function _bindReactions():void
        {
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _buyClickHandler(e:MouseEvent):void
        {
            var event:GmoEvent = new GmoEvent(GmoEvent.BUY, true);
            event.gmoType = _gmoType;
            dispatchEvent(event);
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set price(value:int):void
        {
            if (_ferrosPrice)
                _ferrosPrice.price = value;
        }

        private function _overHandler(event:MouseEvent):void
        {
            popupManager.add(_hintView)
        }

        private function _outHandler(event:MouseEvent):void
        {
            popupManager.remove();
        }

    }
}