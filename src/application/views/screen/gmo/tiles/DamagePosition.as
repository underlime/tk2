/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.11.13
 * Time: 18:18
 */
package application.views.screen.gmo.tiles
{
    import application.views.TextFactory;
    import application.views.screen.gmo.GmoType;

    public class DamagePosition extends GmoPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DamagePosition()
        {
            super(GmoType.DAMAGE);
            _label = TextFactory.instance.getLabel("damage_label").toUpperCase() + "++";
            _offset = 210;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function update(period:int):void
        {
            super.price = _model.getDamagePrice(period);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
