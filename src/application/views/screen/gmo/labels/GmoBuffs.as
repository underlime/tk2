/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.11.13
 * Time: 14:29
 */
package application.views.screen.gmo.labels
{
    import application.models.ModelData;
    import application.models.gmo.GmoGains;
    import application.views.screen.gmo.GmoType;

    import flash.events.Event;

    import framework.core.display.components.ArrTileList;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.data.ModelsRegistry;

    public class GmoBuffs extends ArrTileList
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoBuffs()
        {
            super();
            _cols = 3;
            _lines = 2;
            _limit = 6;
            _dx = 92;
            _dy = 37;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            _model = ModelsRegistry.getModel(ModelData.GMO_GAINS) as GmoGains;
            _updateTiles();
        }

        override protected function render():void
        {
            super.tiles = _buffs;
            draw();

            _model.addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            _model.removeEventListener(Event.CHANGE, _changeHandler);
            super.destroy();
        }


        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _buffs:Array = [];
        private var _model:GmoGains;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateTiles():void
        {
            _resetBuffs();
            var gains:Array = _model.getActiveGains();

            for (var i:int = 0; i < gains.length; i++) {
                var type:GmoType = gains[i] as GmoType;
                _buffs.push(
                        new GmoLabelCounter(type, _model.getBuffSeconds(type))
                );
            }

            if (_buffs.length > 0)
                _buffs.sortOn("timeRemain", Array.NUMERIC | Array.DESCENDING);
        }

        private function _resetBuffs():void
        {
            for (var i:int = 0; i < _buffs.length; i++) {
                if (_buffs[i] && !_buffs[i].destroyed)
                    _buffs[i].destroy();
            }
            _buffs = [];
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            clear();
            _updateTiles();
            super.tiles = _buffs;
            draw();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
