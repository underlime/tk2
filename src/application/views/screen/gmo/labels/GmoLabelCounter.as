/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.11.13
 * Time: 13:55
 */
package application.views.screen.gmo.labels
{
    import application.views.AppView;
    import application.views.screen.common.text.ShadowAppText;
    import application.views.screen.gmo.GmoType;

    import flash.display.Bitmap;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import org.casalib.util.ConversionUtil;

    public class GmoLabelCounter extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoLabelCounter(type:GmoType, secondsRemain:Number)
        {
            super();
            _type = type;
            _secondsRemain = secondsRemain;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addIcon();
            _addTextField();
            _startTimer();
        }

        override public function destroy():void
        {
            _destroyTimer();
            _txtLabel.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _type:GmoType;
        private var _secondsRemain:Number;
        private var _txtLabel:ShadowAppText;
        private var _timer:Timer;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addIcon():void
        {
            var icon:Bitmap = GmoLabelIcons.getIcon(_type);
            addChild(icon);
        }

        private function _addTextField():void
        {
            _txtLabel = new ShadowAppText(0x391a19);
            _txtLabel.size = 12;
            addChild(_txtLabel);

            _txtLabel.text = _secondsRemain.toString();
            _txtLabel.x = 30;
            _txtLabel.y = 13 - _txtLabel.textHeight / 2 - 3;
            _updateTime();
        }

        private function _updateTime():void
        {
            var timeString:String = "";
            var hours:int = int(ConversionUtil.secondsToHours(_secondsRemain));
            var minutes:int = 0;
            var remainSeconds:int = _secondsRemain - (hours * 3600);
            if (remainSeconds > 0)
                minutes = int(ConversionUtil.secondsToMinutes(remainSeconds));

            if (hours > 0)
                timeString += hours.toString() + "ч ";
            if (minutes > 0)
                timeString += minutes.toString() + "м";
            _txtLabel.text = timeString;
        }

        private function _startTimer():void
        {
            _timer = new Timer(1000 * 60, int.MAX_VALUE);
            _timer.addEventListener(TimerEvent.TIMER, _timerHandler);
            _timer.start();
        }

        private function _checkRemainTime():void
        {
            if (_secondsRemain <= 0)
                destroy();
        }

        private function _destroyTimer():void
        {
            _timer.stop();
            _timer.removeEventListener(TimerEvent.TIMER, _timerHandler);
            _timer = null;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _timerHandler(event:TimerEvent):void
        {
            _secondsRemain -= 60;
            _checkRemainTime();
            _updateTime();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get timeRemain():int
        {
            return _secondsRemain;
        }
    }
}
