/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.11.13
 * Time: 13:57
 */
package application.views.screen.gmo.labels
{
    import application.views.screen.gmo.GmoType;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.helpers.ImagesHelper;
    import framework.core.tools.SourceManager;

    public class GmoLabelIcons
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "GMO_CENTER_STATUS_ICONS";
        public static const WIDTH:int = 26;
        public static const HEIGHT:int = 26;

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getIcon(type:GmoType):Bitmap
        {
            var offset:int = _getOffset(type);
            var bitmap:Bitmap = SourceManager.instance.getBitmap(SOURCE);
            return ImagesHelper.getParticular(
                    bitmap,
                    new Rectangle(0, offset, WIDTH, HEIGHT)
            );
        }

        private static function _getOffset(type:GmoType):int
        {
            switch (type) {
                case GmoType.EXPERIENCE:
                    return 0;
                case GmoType.GLORY:
                    return HEIGHT;
                case GmoType.ENERGY:
                    return HEIGHT * 2;
                case GmoType.DAMAGE:
                    return HEIGHT * 3;
                case GmoType.ARMOR:
                    return HEIGHT * 4;
                case GmoType.TOMATOS:
                    return HEIGHT * 5;
            }
            return 0;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
