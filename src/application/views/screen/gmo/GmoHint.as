/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.12.13
 * Time: 20:24
 */
package application.views.screen.gmo
{
    import application.views.screen.achievements.LockHintView;
    import application.views.screen.common.hint.HintCorner;

    import framework.core.enum.Side;

    public class GmoHint extends LockHintView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoHint(header:String, message:String)
        {
            super(header, message);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _addCorner():void
        {
            var corner:HintCorner = new HintCorner(Side.RIGHT);
            addChild(corner);

            corner.x = super.w - HintCorner.CORNER_OFFSET + 4;
            corner.y = int(super.h / 2 - corner.h / 2);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
