/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.11.13
 * Time: 17:01
 */
package application.views.screen.gmo
{
    import framework.core.enum.BaseEnum;

    public class GmoType extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;
        public static var ENUM:Vector.<GmoType> = new Vector.<GmoType>();

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const EXPERIENCE:GmoType = new GmoType("experience");
        public static const GLORY:GmoType = new GmoType("glory");
        public static const ENERGY:GmoType = new GmoType("energy");
        public static const DAMAGE:GmoType = new GmoType("damage");
        public static const ARMOR:GmoType = new GmoType("armor");
        public static const TOMATOS:GmoType = new GmoType("fight_tomatos");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        public static function getEnumByKey(key:String):GmoType
        {
            var count:int = ENUM.length;
            for (var i:int = 0; i < count; i++) {
                if (ENUM[i].toString() == key)
                    return ENUM[i];
            }
            throw new Error("GmoType not found");
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoType(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
            ENUM.push(this);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.views.screen.gmo.GmoType;

GmoType.lockUp();