/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.11.13
 * Time: 12:06
 */
package application.views.screen.gmo
{
    import application.views.TextFactory;
    import application.views.TextFactory;
    import application.views.TextFactory;
    import application.views.screen.common.radio.AppRadioButton;

    import framework.core.display.components.RadioButton;
    import framework.core.display.components.RadioGroup;

    public class GmoRadioGroup extends RadioGroup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoRadioGroup()
        {
            oneHour = new AppRadioButton(TextFactory.instance.getLabel("1_hour"));
            sixHour = new AppRadioButton(TextFactory.instance.getLabel("6_hour"));
            oneDay = new AppRadioButton(TextFactory.instance.getLabel("1_day"));
            twoDays = new AppRadioButton(TextFactory.instance.getLabel("2_day"));

            super(new <RadioButton>[oneHour, sixHour, oneDay, twoDays]);
            _dy = 9;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            oneHour.x = 0;
            oneHour.y = 0;

            sixHour.x = 85;
            sixHour.y = 0;

            oneDay.x = 0;
            oneDay.y = 33;

            twoDays.x = 85;
            twoDays.y = 33;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var oneHour:AppRadioButton;
        private var sixHour:AppRadioButton;
        private var oneDay:AppRadioButton;
        private var twoDays:AppRadioButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
