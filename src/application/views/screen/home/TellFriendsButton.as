/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.08.13
 * Time: 15:25
 */
package application.views.screen.home
{
    import application.views.TextFactory;
    import application.views.screen.common.buttons.SpecialButton;

    public class TellFriendsButton extends SpecialButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TellFriendsButton()
        {
            super(TextFactory.instance.getLabel('tell_friends_button'));
            _w = 161;
            _fontSize = 14;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
