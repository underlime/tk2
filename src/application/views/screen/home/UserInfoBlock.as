/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.08.13
 * Time: 11:49
 */
package application.views.screen.home
{
    import application.events.ApplicationEvent;
    import application.models.inventory.Inventory;
    import application.models.inventory.InventoryHelper;
    import application.models.inventory.InventoryItem;
    import application.models.user.IUserData;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.constructor.builders.ClothesBuilder;
    import application.views.screen.common.UserLearningPoints;
    import application.views.screen.common.UserLevel;
    import application.views.screen.home.fields.UserLoginField;
    import application.views.screen.home.fields.UserRankField;
    import application.views.screen.inventory.slots.InventorySlotsPanel;

    import flash.events.MouseEvent;

    import framework.core.socnet.OdnoklassnikiRu;

    import framework.core.socnet.SocNet;

    public class UserInfoBlock extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserInfoBlock(user:IUserData, inventory:Inventory)
        {
            super();
            _user = user;
            _inventory = inventory;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addSlots();
            _addUnit();
            _addLevel();
            _addPoints();
            _addLogin();
            _addRankBlock();
            if (SocNet.instance().photosUploadAllowed) {
                _addBottomButton();
            }
        }

        override public function destroy():void
        {
            _inventorySlots.destroy();
            _unit.destroy();
            _userLevel.destroy();
            _learningPoints.destroy();
            _userLoginField.destroy();
            _userRankField.destroy();

            if (_tellButton && !_tellButton.destroyed) {
                _tellButton.destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _user:IUserData;
        protected var _inventory:Inventory;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _addBottomButton():void
        {
            _tellButton = new TellFriendsButton();
            addChild(_tellButton);
            _tellButton.x = 67;
            _tellButton.y = 285;

            _tellButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _inventorySlots:InventorySlotsPanel;

        private var _unit:ClothesBuilder;
        private var _userLevel:UserLevel;

        private var _learningPoints:UserLearningPoints;

        private var _userLoginField:UserLoginField;
        private var _userRankField:UserRankField;

        private var _tellButton:TellFriendsButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addSlots():void
        {
            _inventorySlots = new InventorySlotsPanel();

            var items:Array = _inventory.getPutItems();
            addChild(_inventorySlots);

            var availableRings:int = _user.userLevelParams.ring_slots_avaible.value;
            var total:int = _inventorySlots.ringsPanel.slots.length;
            var lock:int = total - availableRings;

            if (lock > 0) {
                _inventorySlots.ringsPanel.lockSlots(lock);
            }

            _inventorySlots.readonly = true;

            var count:int = items.length;
            for (var i:int = 0; i < count; i++) {
                var item:InventoryItem = items[i] as InventoryItem;
                for (var j:int = 0; j < item.put.value; j++) {
                    _inventorySlots.add(item);
                }
            }
        }

        private function _addUnit():void
        {
            var items:Array = _inventory.getBuildingItems();

            _unit = new ClothesBuilder(_user.userInfo, InventoryHelper.ItemsArray2Vector(items));
            addChild(_unit);

            _unit.scaleX = .8;
            _unit.scaleY = .8;

            _unit.x = 92;
            _unit.y = 163;

            _unit.dress();
        }

        private function _addLevel():void
        {
            _userLevel = new UserLevel(_user.userLevelParams.max_level.value);
            addChild(_userLevel);
            _userLevel.x = -12;
        }

        private function _addPoints():void
        {
            _learningPoints = new UserLearningPoints(_user.userLevelParams.points_learning.value);
            addChild(_learningPoints);
            _learningPoints.x = 220;
        }

        private function _addLogin():void
        {
            _userLoginField = new UserLoginField(_user.userInfo.login.value, _user.userFightData.glory.value);
            addChild(_userLoginField);
            _userLoginField.x = 54;
        }

        private function _addRankBlock():void
        {
            _userRankField = new UserRankField(_user.userFightData.glory.value);
            addChild(_userRankField);
            _userRankField.x = 54;
            _userRankField.y = 29;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            var shareEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.SHARE, true);
            dispatchEvent(shareEvent);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
