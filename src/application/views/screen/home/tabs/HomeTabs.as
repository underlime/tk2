/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.06.13
 * Time: 12:10
 */
package application.views.screen.home.tabs
{
    import application.views.screen.common.tabs.AppTabs;
    import application.views.screen.common.tabs.TabFactory;
    import application.views.screen.common.tabs.TabType;

    import framework.core.display.tabs.ITab;

    public class HomeTabs extends AppTabs
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HomeTabs()
        {
            var tabs:Vector.<ITab> = new Vector.<ITab>();
            tabs.push(
                    TabFactory.getTab(TabType.INVENTORY_TAB),
                    TabFactory.getTab(TabType.USER_INFO_TAB),
                    TabFactory.getTab(TabType.SPECIALS_TAB),
                    TabFactory.getTab(TabType.ACHIEVEMENTS_TAB)
            );

            super(tabs);
            _dx = 3;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
