/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.06.13
 * Time: 10:34
 */
package application.views.screen.home
{
    import application.views.TextFactory;
    import application.views.screen.base.BaseScreen;
    import application.views.screen.home.tabs.HomeTabs;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.core.display.tabs.TabPanel;

    public class HomeScreen extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const STALL:String = "INVENTORY_STALL";
        public static const CHAR_BG:String = "CHAR_BG";
        public static const SHADOW:String = "CHAR_SHADOW";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HomeScreen()
        {
            super(TextFactory.instance.getLabel('inventory_label'));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addBack();
            _setupTabs();
        }

        override public function destroy():void
        {
            graphics.clear();
            _tabs.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _tabs:TabPanel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            var marketStall:Bitmap = super.source.getBitmap(STALL);

            addChild(marketStall);

            marketStall.x = 38;
            marketStall.y = 101;


            var charBG:Bitmap = super.source.getBitmap(CHAR_BG);
            addChild(charBG);
            charBG.x = 480;
            charBG.y = 107;

            var shadow:Bitmap = super.source.getBitmap(SHADOW);
            addChild(shadow);
            shadow.x = 530;
            shadow.y = 272;
        }

        protected function _setupTabs():void
        {
            _tabs = new HomeTabs();
            _tabs.addEventListener(Event.CHANGE, _changeHandler);
            _tabs.y = 43;
            _tabs.x = 50;
            addChild(_tabs);
            _tabs.select(0);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _changeHandler(event:Event):void
        {
            super.dispatchEvent(new Event(Event.CHANGE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get tabs():TabPanel
        {
            return _tabs;
        }
    }
}
