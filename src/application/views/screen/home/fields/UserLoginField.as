/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.08.13
 * Time: 14:06
 */
package application.views.screen.home.fields
{
    import application.models.ModelData;
    import application.models.ranks.RankData;
    import application.models.ranks.RanksModel;
    import application.views.map.layers.common.NotificationCount;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.display.rubber.RubberView;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.tools.SourceManager;

    public class UserLoginField extends RubberView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserLoginField(login:String, glory:int)
        {
            var left:Bitmap = SourceManager.instance.getBitmap(NotificationCount.LEFT);
            var center:Bitmap = SourceManager.instance.getBitmap(NotificationCount.CENTER);
            var right:Bitmap = SourceManager.instance.getBitmap(NotificationCount.RIGHT);
            super(left, center, right);

            _login = login;
            _glory = glory;
            _w = 162;
            this.scrollRect = new Rectangle(0, 0, _w, NotificationCount.HEIGHT);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addLogin();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtLabel:ShadowAppText;
        private var _login:String;
        private var _glory:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addLogin():void
        {
            var rankModel:RanksModel = ModelsRegistry.getModel(ModelData.RANKS) as RanksModel;
            var rankData:RankData = rankModel.getRankData(_glory);

            _txtLabel = new ShadowAppText(0x4b1c0f);
            _txtLabel.size = 12;
            _txtLabel.color = 0xffffff;
            _txtLabel.bold = true;
            _txtLabel.autoSize = TextFieldAutoSize.NONE;
            _txtLabel.align = TextFormatAlign.CENTER;
            _txtLabel.width = _w;
            addChild(_txtLabel);
            _txtLabel.text = _login;
            _txtLabel.y = 2;

            var icon:Bitmap = super.source.getBitmap(CommonData.ICON_PREFIX + rankData.picture);
            addChild(icon);
            icon.x = 2;
            icon.y = 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
