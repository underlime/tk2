/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.10.13
 * Time: 13:00
 */
package application.views.screen.home
{
    import application.common.Colors;
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class HomeCounter extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "ACHIEVE_AND_SKILL_COUNT";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HomeCounter(header:String, current:int, total:int)
        {
            super();
            _header = header;
            _current = current;
            _total = total;

            this.w = 136;
            this.h = 44;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addHeader();
            _addCount();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _header:String;
        private var _current:int;
        private var _total:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var back:Bitmap = super.source.getBitmap(BACK);
            addChild(back);
        }

        private function _addHeader():void
        {
            var txtHeader:ShadowAppText = new ShadowAppText(Colors.DARK_BROWN);
            txtHeader.autoSize = TextFieldAutoSize.NONE;
            txtHeader.width = this.w;
            txtHeader.align = TextFormatAlign.CENTER;
            txtHeader.size = 12;
            addChild(txtHeader);
            txtHeader.text = _header;
            txtHeader.y = -10;
        }

        private function _addCount():void
        {
            var txtCount:ShadowAppText = new ShadowAppText(Colors.DARK_BROWN);
            txtCount.autoSize = TextFieldAutoSize.NONE;
            txtCount.width = this.w;
            txtCount.align = TextFormatAlign.CENTER;
            txtCount.size = 16;
            addChild(txtCount);
            txtCount.text = _current.toString() + "/" + _total.toString();
            txtCount.y = super.h / 2 - txtCount.textHeight / 2 - 3;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
