/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.06.13
 * Time: 10:35
 */
package application.views.screen.home.data
{
    public class HomeData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "HOME_Background";
        public static const CONFIRM_BUTTON:String = "HOME_ConfirmButton";
        public static const NAVIGATION_BACKGROUND:String = "HOME_NavigationBackground";
        public static const PHOTO_BUTTON:String = "HOME_PhotoButton";
        public static const SCROLL_HEADER:String = "HOME_ScrollHeaders";

        // dialogs

        public static const EQUIP_CONTEXT_BUTTON:String = "HOME_EquipContextBtn";
        public static const USE_CONTEXT_BUTTON:String = "HOME_UseContextBtn";
        public static const SELL_CONTEXT_BUTTON:String = "HOME_SellContextBtn";

        // tabs
        public static const INVENTORY_TAB:String = "HOME_ItemsTab";
        public static const STATS_TAB:String = "HOME_StatsTab";
        public static const SKILLS_TAB:String = "HOME_SkillsTab";
        public static const ACHIEVEMENTS_TAB:String = "HOME_AchievementTab";

        // inventory

        public static const PEDESTAL:String = "HOME_PIEDESTAL";
        public static const ITEMS_PANEL:String = "HOME_ItemsPanel";

        // slots

        public static const CONSUME:String = "HOME_BattleSlot";
        public static const CONSUME_LOCK:String = "HOME_SlotLock";
        public static const BATTLE_LOCK:String = "HOME_BattleSlotLock";
        public static const SOCIAL_LOCK:String = "HOME_SocialSlotLock";

        public static const MONSTER_SLOT:String = "HOME_SlotMoster";
        public static const BOOTS_SLOT:String = "HOME_SlotBoots";
        public static const WEAPON_SLOT:String = "HOME_SlotWeapon";
        public static const COAT_SLOT:String = "HOME_SlotCoat";
        public static const HAT_SLOT:String = "HOME_SlotHat";
        public static const MASK_SLOT:String = "HOME_SlotMask";
        public static const ARMOR_SLOT:String = "HOME_SlotArmor";
        public static const SHIELD_SLOT:String = "HOME_SlotShield";
        public static const AMULET_SLOT:String = "HOME_SlotAmulet";
        public static const RING_SLOT:String = "HOME_SlotRing";

        public static const SOCIAL_SLOT_HAT:String = "HOME_SocialSlotHelm";
        public static const SOCIAL_SLOT_COAT:String = "HOME_SocialSlotCoat";
        public static const SOCIAL_SLOT_MASK:String = "HOME_SocailSlotMask";
        public static const SOCIAL_SLOT_WEAPON:String = "HOME_SocialSlotWeapon";
        public static const SOCIAL_SLOT_SHIELD:String = "HOME_SocialSlotShield";

        // filters

        public static const FILTER_ALL:String = "HOME_FilterAll";
        public static const FILTER_ARMOR:String = "HOME_FilterArmor";
        public static const FILTER_COAT:String = "HOME_FilterCoats";
        public static const FILTER_CONSUME:String = "HOME_FilterConsumers";
        public static const FILTER_HAT:String = "HOME_FilterHat";
        public static const FILTER_MASK:String = "HOME_FilterMasks";
        public static const FILTER_MONSTER:String = "HOME_FilterMonsters";
        public static const FILTER_RINGS:String = "HOME_FilterRings";
        public static const FILTER_SHILED:String = "HOME_FilterShield";
        public static const FILTER_WEAPON:String = "HOME_FilterWeapon";

        // stats

        public static const HOME_STATS_BACKGROUND:String = "HOME_HomeStatsBackground";
        public static const HOME_SKILLPOINTS_BAR:String = "HOME_SkillPointsBar";
        public static const HOME_LEVEL_BAR:String = "HOME_LevelBarIcon";
        public static const STRENGTH_ICO:String = "HOME_StrengthIco";
        public static const AGILITY_ICO:String = "HOME_AgilityIco";
        public static const INTELLECT_ICO:String = "HOME_IntellectIco";

        // bars

        public static const HP_BAR:String = "HOME_HPBar";
        public static const ENERGY_BAR:String = "HOME_EnergyBar";
        public static const EXPERIENCE_BAR:String = "HOME_Experience";
        public static const GLORY_BAR:String = "HOME_GloryBar";

        public static const HP_ICO:String = "HOME_HPBarIco";
        public static const ENERGY_ICO:String = "HOME_EnergyBarIco";
        public static const EXPERIENCE_ICO:String = "HOME_ExperienceBarIco";
        public static const GLORY_ICO:String = "HOME_GloryIco";

        // locks

        public static const ACHIEVEMENT_LOCK:String = "HOME_AchievementLock";
        public static const SKILL_LOCK:String = "HOME_SkillLock";

        // achievementsScreen

        public static const ACHIEVEMENTS_BACKGROUND:String = "HOME_AchievementsBackground";
        public static const SKILLS_BACKGROUND:String = "HOME_SkillsBackground";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
