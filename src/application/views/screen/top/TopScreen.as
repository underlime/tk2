/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 12:42
 */
package application.views.screen.top
{
    import application.config.AppConfig;
    import application.models.top.TopInterval;
    import application.views.TextFactory;
    import application.views.screen.base.ScreenTitle;
    import application.views.screen.common.buttons.CloseButton;
    import application.views.screen.top.tabs.interval.IntervalTabsPanel;
    import application.views.screen.top.tabs.type.TypeTabsPanel;

    import flash.display.Bitmap;
    import flash.display.Shape;

    import framework.core.struct.data.ListData;
    import framework.core.struct.view.View;

    public class TopScreen extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "TOP_STALL";
        public static const SEPARATOR:String = "SEPARATOR";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopScreen()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _drawBackground();
            _addScreenTitle();
            _addCloseButton();

            _addTypeTabsPanel();
            _addIntervalTabsPanel();

            _addLayouts();

            _addSeparator();
        }

        override public function destroy():void
        {
            _topLayout.destroy();
            _intervalTabs.destroy();
            _typeTabs.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function showUsersTop():void
        {
            if (_topClanLayout && !_topClanLayout.destroyed)
                _topClanLayout.destroy();
            _topLayout.visible = true;
            _typeTabs.visible = true;
        }

        public function showClansTop():void
        {
            _topLayout.visible = false;
            _typeTabs.visible = false;
            _topClanLayout = new ClansTopLayout();
            addChild(_topClanLayout);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _screenTitle:ScreenTitle;

        private var _back:Bitmap;
        private var _closeButton:CloseButton;

        private var _topLayout:TopLayout;
        private var _topClanLayout:ClansTopLayout;

        private var _topList:ListData;

        private var _typeTabs:TypeTabsPanel;
        private var _intervalTabs:IntervalTabsPanel;

        private var _interval:TopInterval = TopInterval.GLOBAL;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _drawBackground():void
        {
            _drawBack();
            _addStall();
        }

        private function _addStall():void
        {
            _back = super.source.getBitmap(BACK);
            addChild(_back);

            _back.x = AppConfig.APP_WIDTH / 2 - _back.width / 2;
            _back.y = 30;
        }

        private function _drawBack():void
        {
            graphics.beginFill(0x000000, 0.8);
            graphics.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            graphics.endFill();
        }

        private function _addScreenTitle():void
        {
            _screenTitle = new ScreenTitle(TextFactory.instance.getLabel('top_label'));
            addChild(_screenTitle);
            _screenTitle.x = AppConfig.APP_WIDTH / 2 - _screenTitle.width / 2;
        }

        private function _addCloseButton():void
        {
            _closeButton = new CloseButton();
            addChild(_closeButton);

            _closeButton.x = _back.x + _back.width - _closeButton.w;
            _closeButton.y = 5;
        }

        private function _addTypeTabsPanel():void
        {
            _typeTabs = new TypeTabsPanel();
            _typeTabs.x = 465;
            _typeTabs.y = 73;
            addChild(_typeTabs);
        }

        private function _addIntervalTabsPanel():void
        {
            _intervalTabs = new IntervalTabsPanel();
            _intervalTabs.x = 24;
            _intervalTabs.y = 73;
            addChild(_intervalTabs);
        }

        private function _addLayouts():void
        {
            _topLayout = new TopLayout();
            addChild(_topLayout);
        }

        private function _addSeparator():void
        {
            var shape:Shape = new Shape();
            addChild(shape);
            shape.x = 20;
            shape.y = 435;

            shape.graphics.beginFill(0xddc078);
            shape.graphics.drawRect(0, 0, 710, 4);
            shape.graphics.endFill();
        }


        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set topList(value:ListData):void
        {
            _topList = value;
            _topLayout.updateTop(_topList, _intervalTabs.interval);
        }

        public function get typeTabs():TypeTabsPanel
        {
            return _typeTabs;
        }

        public function get intervalTabs():IntervalTabsPanel
        {
            return _intervalTabs;
        }

        public function set userPosition(value:int):void
        {
            _topLayout.userPosition = value;
        }

        public function get interval():TopInterval
        {
            return _interval;
        }

        public function set interval(value:TopInterval):void
        {
            _interval = value;
        }
    }
}
