/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.03.14
 * Time: 11:49
 */
package application.views.screen.top
{
    import application.models.ModelData;
    import application.models.clans.ClanInfo;
    import application.models.top.TopHelper;
    import application.models.user.User;

    import framework.core.struct.data.ModelsRegistry;

    public class UserClanPosition extends ClanPositionTop
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserClanPosition()
        {
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
            var clan:ClanInfo = _user.clanInfo;
            var position:int = TopHelper.getClanTopPosition(clan);
            super(position, clan);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
