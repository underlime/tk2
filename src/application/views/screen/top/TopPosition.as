/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 16:10
 */
package application.views.screen.top
{
    import application.events.InfoEvent;
    import application.models.top.TopInterval;
    import application.models.top.TopRecord;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.screen.common.RankView;
    import application.views.screen.common.frame.UserFrame;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.MouseEvent;
    import flash.events.SecurityErrorEvent;
    import flash.geom.Rectangle;
    import flash.net.URLRequest;
    import flash.system.LoaderContext;
    import flash.text.TextFieldAutoSize;

    import framework.core.helpers.ImagesHelper;
    import framework.core.struct.view.View;

    public class TopPosition extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "USER_POSITION";

        public static const POSITION_WIDTH:int = 676;
        public static const POSITION_HEIGHT:int = 40;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopPosition(position:int, model:TopRecord, interval:TopInterval)
        {
            super();
            _position = position;
            _model = model;
            _interval = interval;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _renderBackground();
            _addPosition();
            _addAvatar();
            _addLogin();
            _addSocialName();
            _addRank();
            _addClanName();
            _addGloryData();
            _bindReactions();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            _txtLogin.destroy();
            _rankView.destroy();

            if (_avatarLoader) {
                _avatarLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, _onAvatarLoaded);
                _avatarLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
                _avatarLoader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);
            }

            if (_avatarContainer && !_avatarContainer.destroyed)
                _avatarContainer.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _model:TopRecord;
        private var _position:int = 0;
        private var _txtLogin:AppTextField;
        private var _txtSocialName:AppTextField;
        private var _rankView:RankView;
        private var _avatar:Bitmap;

        private var _avatarContainer:View;
        private var _avatarLoader:Loader;

        private var _topNumber:TopNumber;
        private var _interval:TopInterval = TopInterval.GLOBAL;

        private var _gloryPosition:GloryPosition;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderBackground():void
        {
            _background = super.source.getBitmap(BACK);
            _background.scrollRect = new Rectangle(0, 0, POSITION_WIDTH, POSITION_HEIGHT);

            addChild(_background);
        }

        private function _addPosition():void
        {
            _topNumber = new TopNumber(_position);
            _topNumber.x = 5;
            _topNumber.y = 5;
            addChild(_topNumber)
        }

        private function _bindReactions():void
        {
            buttonMode = true;
            useHandCursor = true;

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);

            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _addAvatar():void
        {
            var icon:Bitmap = super.source.getBitmap(UserFrame.DEFAULT_ICON, true);
            var data:BitmapData = ImagesHelper.matrixResize(icon, 40, 40, true);
            _avatar = new Bitmap(data);
            addChild(_avatar);

            _avatar.x = 40;
            _avatar.y = 0;

            _avatarLoader = new Loader();
            _avatarLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, _onAvatarLoaded);
            _avatarLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
            _avatarLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);

            var request:URLRequest = new URLRequest(_model.picture.value);
            var context:LoaderContext = new LoaderContext(false);
            _avatarLoader.load(request, context);
        }

        private function _onAvatarLoaded(e:Event):void
        {
            _avatarContainer = new View();
            addChild(_avatarContainer);

            _avatarContainer.addChild(_avatarLoader);
            _avatarContainer.width = 34;
            _avatarContainer.height = 34;
            _avatarContainer.x = 45;
            _avatarContainer.y = 3;
        }

        private function _onAvatarError(e:Event):void
        {
        }

        private function _addLogin():void
        {
            _txtLogin = new AppTextField();
            _txtLogin.size = 14;
            _txtLogin.color = 0x3c2415;
            _txtLogin.bold = true;
            _txtLogin.autoSize = TextFieldAutoSize.LEFT;

            addChild(_txtLogin);
            _txtLogin.text = _model.login.value;
            _txtLogin.x = _avatar.x + _avatar.width + 5;
        }

        private function _addSocialName():void
        {
            _txtSocialName = new AppTextField();
            _txtSocialName.size = 12;
            _txtSocialName.color = 0x3c2415;
            _txtSocialName.autoSize = TextFieldAutoSize.LEFT;

            addChild(_txtSocialName);
            _txtSocialName.text = _model.first_name.value + ' ' + _model.last_name.value;

            _txtSocialName.x = _avatar.x + _avatar.width + 5;
            _txtSocialName.y = 15;
        }

        private function _addClanName():void
        {
            var txtLevel:AppTextField = new AppTextField();
            txtLevel.size = 14;
            txtLevel.color = 0xbe1e2d;
            txtLevel.bold = true;
            txtLevel.autoSize = TextFieldAutoSize.LEFT;

            addChild(txtLevel);
            txtLevel.text = _model.clan_name.value;
            txtLevel.x = 420;
            txtLevel.y = 7;
        }

        private function _addRank():void
        {
            _rankView = new RankView(_model.glory.value);
            _rankView.x = 261;
            _rankView.y = 9;
            addChild(_rankView);
        }

        private function _addGloryData():void
        {
            _gloryPosition = new GloryPosition(_getGloryValue());
            addChild(_gloryPosition);
            _gloryPosition.x = 628;
            _gloryPosition.y = 1;
        }

        private function _getGloryValue():int
        {
            switch (_interval) {
                case TopInterval.GLOBAL:
                    return _model.glory.value;
                case TopInterval.WEEK:
                    return _model.week_glory.value;
                case TopInterval.DAY:
                    return _model.day_glory.value;
            }

            return _model.glory.value;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            _background.scrollRect = new Rectangle(0, POSITION_HEIGHT, POSITION_WIDTH, POSITION_HEIGHT);
            super.sound.playUISound(UISound.MOUSE_OVER);
        }

        private function _outHandler(event:MouseEvent):void
        {
            _background.scrollRect = new Rectangle(0, 0, POSITION_WIDTH, POSITION_HEIGHT);
        }

        protected function _clickHandler(e:MouseEvent):void
        {
            var event:InfoEvent = new InfoEvent(InfoEvent.GET_INFO, true);
            event.soc_net_id = _model.soc_net_id.value;
            dispatchEvent(event);
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set position(value:int):void
        {
            _position = value;
            if (_topNumber)
                _topNumber.position = value;
        }

        public function set glory(value:int):void
        {
            if (_gloryPosition)
                _gloryPosition.glory = value;
        }
    }
}
