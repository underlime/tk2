/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 09.09.13
 * Time: 19:48
 */
package application.views.screen.top.scroll
{
    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.display.rubber.VerticalRubber;
    import framework.core.helpers.ImagesHelper;
    import framework.core.tools.SourceManager;

    public class ScrollTrack extends VerticalRubber
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SCROLL:String = "VERTICAL_SCROLL_BAR";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ScrollTrack(height:int)
        {
            var sourceAtlas:Bitmap = SourceManager.instance.getBitmap(SCROLL);

            var top:Bitmap = ImagesHelper.getParticular(sourceAtlas, new Rectangle(0, 0, 14, 7));
            var center:Bitmap = ImagesHelper.getParticular(sourceAtlas, new Rectangle(0, 8, 14, 1));
            var bottom:Bitmap = ImagesHelper.getParticular(sourceAtlas, new Rectangle(0, 10, 14, 7));

            super(top, center, bottom);

            _w = 14;
            _h = height;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
