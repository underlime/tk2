/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.09.13
 * Time: 13:46
 */
package application.views.screen.top
{
    import application.views.helpers.icons.IconType;
    import application.views.helpers.icons.IconsFactory;
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class GloryPosition extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GloryPosition(glory:int)
        {
            super();
            _glory = glory;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addIcon();
            _addValue();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _glory:int;
        private var _verticalMargin:int = 0;

        private var _icon:Bitmap;
        private var _txtValue:ShadowAppText;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addIcon():void
        {
            _icon = IconsFactory.getIcon(IconType.GLORY_ICO);
            addChild(_icon);
        }

        private function _addValue():void
        {
            _txtValue = new ShadowAppText(0x653b28);
            _txtValue.autoSize = TextFieldAutoSize.NONE;
            _txtValue.align = TextFormatAlign.RIGHT;
            _txtValue.width = 200;
            _txtValue.size = 14;
            _txtValue.color = 0xffffff;
            addChild(_txtValue);

            _txtValue.text = _glory.toString();
            _txtValue.x = -12 - _icon.width - 155;
            _txtValue.y = 20 - _txtValue.textHeight / 2 + _verticalMargin - 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set glory(value:int):void
        {
            if (_txtValue)
                _txtValue.text = value.toString();
        }
    }
}
