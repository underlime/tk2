/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 09.09.13
 * Time: 20:44
 */
package application.views.screen.top
{
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class TopNumber extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const STICKER:String = "PLACE_STICKER";

        public static const NUMBER_WIDTH:int = 30;
        public static const NUMBER_HEIGHT:int = 30;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopNumber(position:int)
        {
            super();
            _position = position;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addPosition();
            if (_position < 4 && _position != 0) {
                _addSticker();
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _position:int;

        private var _txtPosition:ShadowAppText;
        private var _icon:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addSticker():void
        {
            var offset:int = _getOffset();
            _icon = super.source.getBitmap(STICKER);
            _icon.scrollRect = new Rectangle(0, offset, NUMBER_WIDTH, NUMBER_HEIGHT);
            addChild(_icon);

            swapChildren(_icon, _txtPosition);
        }

        private function _getOffset():int
        {
            switch (_position) {
                case 1:
                    return 0;
                case 2:
                    return 30;
                case 3:
                    return 60;
            }

            return 0;
        }

        private function _addPosition():void
        {
            _txtPosition = new ShadowAppText(0x5f3933);
            _txtPosition.autoSize = TextFieldAutoSize.NONE;
            _txtPosition.width = 50;
            _txtPosition.height = 30;
            _txtPosition.align = TextFormatAlign.CENTER;
            _txtPosition.size = 14;
            _txtPosition.bold = true;
            addChild(_txtPosition);

            _updatePositionValue();
        }

        private function _updatePositionValue():void
        {
            var message:String = _position == 0 ? "..." : _position.toString();

            _txtPosition.text = message;
            _txtPosition.x = -10;
            _txtPosition.y = NUMBER_HEIGHT / 2 - _txtPosition.textHeight / 2;

            if (_position == 3 || _position == 2) {
                _txtPosition.x -= 1;
                _txtPosition.y -= 2;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set position(value:int):void
        {
            _position = value;
            if (_txtPosition) {
                _updatePositionValue();
                if (_icon && this.contains(_icon)) {
                    removeChild(_icon);
                }
                if (_position < 4 && _position != 0) {
                    _addSticker();
                }
            }
        }
    }
}
