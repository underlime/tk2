/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.09.13
 * Time: 16:56
 */
package application.views.screen.top
{
    import application.models.ModelData;
    import application.models.top.TopInterval;
    import application.models.top.TopRecord;
    import application.models.user.User;

    import flash.events.MouseEvent;

    import framework.core.struct.data.ModelsRegistry;

    public class UserTopPosition extends TopPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserTopPosition()
        {
            _user = ModelsRegistry.getModel(ModelData.USER) as User;

            var record:TopRecord = _getRecord();
            var position:int = _position;

            super(position, record, TopInterval.GLOBAL);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();

            buttonMode = false;
            useHandCursor = false;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:User;
        private var _position:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getRecord():TopRecord
        {
            var record:TopRecord = new TopRecord();
            record.soc_net_id.value = _user.userInfo.soc_net_id.value;
            record.user_id.value = _user.userInfo.user_id.value;
            record.glory.value = _user.userFightData.glory.value;
            record.level.value = _user.userFightData.glory.value;
            record.login.value = _user.userInfo.login.value;

            record.first_name.value = _user.socNetUserInfo.first_name.value;
            record.last_name.value = _user.socNetUserInfo.last_name.value;
            record.picture.value = _user.socNetUserInfo.picture.value;

            return record;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _clickHandler(e:MouseEvent):void
        {

        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
