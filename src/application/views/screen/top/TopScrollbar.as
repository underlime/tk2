/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.11.13
 * Time: 15:47
 */
package application.views.screen.top
{
    import application.views.screen.common.scroll.ScrollMarker;
    import application.views.screen.top.scroll.ScrollTrack;

    import flash.display.Sprite;
    import flash.geom.Rectangle;

    import framework.core.display.scroll.Scroll;

    public class TopScrollbar extends Scroll
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopScrollbar(height:int)
        {
            var marker:Sprite = new ScrollMarker();
            var track:ScrollTrack = new ScrollTrack(height);
            var rectangle:Rectangle = new Rectangle(0, 0, 0, 260);

            super(marker, track, rectangle);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _track.x = 9;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
