/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.03.14
 * Time: 19:03
 */
package application.views.screen.top
{
    import application.models.top.TopInterval;
    import application.models.top.TopRecord;
    import application.views.AppView;
    import application.views.screen.common.scroll.ScrollObserver;

    import flash.geom.Rectangle;

    import framework.core.struct.data.ListData;

    public class TopLayout extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopLayout()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addScrollBar();
            _addUserPosition();
        }

        override public function destroy():void
        {
            if (_scrollbar && !_scrollbar.destroyed)
                _scrollbar.destroy();
            _clearPositions();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function updateTop(positions:ListData, interval:TopInterval):void
        {
            _topList = positions;
            _interval = interval;
            _clearPositions();
            _addPositions();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _scrollbar:TopScrollbar;
        private var _positionsContainer:TopPositionsContainer;
        private var _scrollObserver:ScrollObserver;

        private var _interval:TopInterval = TopInterval.GLOBAL;
        private var _topList:ListData;

        private var _userPosition:UserTopPosition;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addScrollBar():void
        {
            _scrollbar = new TopScrollbar(293);
            addChild(_scrollbar);

            _scrollbar.x = 702;
            _scrollbar.y = 137;
        }

        private function _clearPositions():void
        {
            if (_scrollObserver && !_scrollObserver.destroyed)
                _scrollObserver.destroy();
            if (_scrollbar)
                _scrollbar.percent = 0;
        }

        private function _addPositions():void
        {
            var positions:Vector.<TopPosition> = new Vector.<TopPosition>();
            var list:Object = _topList.list.value;
            var position:int = 1;
            for (var key:String in list) {
                positions.push(new TopPosition(position, list[key] as TopRecord, _interval));
                position++;
            }
            _positionsContainer = new TopPositionsContainer(positions);
            addChild(_positionsContainer);
            _positionsContainer.x = 24;
            _positionsContainer.y = 137;

            _scrollObserver = new ScrollObserver(_positionsContainer, _scrollbar, new Rectangle(0, 0, TopPosition.POSITION_WIDTH, 293), _positionsContainer.renderHeight);
        }

        private function _addUserPosition():void
        {
            _userPosition = new UserTopPosition();
            addChild(_userPosition);

            _userPosition.x = 24;
            _userPosition.y = 445;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set userPosition(value:int):void
        {
            if (_userPosition)
                _userPosition.position = value;
        }

    }
}
