/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.03.14
 * Time: 19:39
 */
package application.views.screen.top
{
    import application.views.screen.common.navigation.Counter;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.display.rubber.RubberView;
    import framework.core.struct.view.View;
    import framework.core.tools.SourceManager;

    public class ClansTopInfoBack extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClansTopInfoBack()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            var left:Bitmap = SourceManager.instance.getBitmap(Counter.BOX_LEFT);
            var center:Bitmap = SourceManager.instance.getBitmap(Counter.BOX_CENTER);
            var right:Bitmap = SourceManager.instance.getBitmap(Counter.BOX_RIGHT);

            var rubber:RubberView = new RubberView(left, center, right);
            rubber.w = 254;
            rubber.scrollRect = new Rectangle(0, 40, 254, 40);
            addChild(rubber);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
