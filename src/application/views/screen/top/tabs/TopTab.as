/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.07.13
 * Time: 14:31
 */
package application.views.screen.top.tabs
{
    import application.views.text.AppTextField;

    import flash.text.TextFormatAlign;

    import framework.core.display.tabs.MovieClipTab;

    public class TopTab extends MovieClipTab
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOP_TAB_SOURCE:String = "TopScreenTab";


        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopTab(label:String)
        {
            super(TOP_TAB_SOURCE);

            _label = label;
            _hoverFrame = 2;
            _activeFrame = 2;
            _normalFrame = 1;

            _save = false;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addLabel();
        }

        override public function destroy():void
        {
            _txtLabel.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _label:String;
        private var _txtLabel:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addLabel():void
        {
            _txtLabel = new AppTextField();
            _txtLabel.width = 150;
            _txtLabel.size = 14;
            _txtLabel.bold = true;
            _txtLabel.color = 0x000000;
            _txtLabel.align = TextFormatAlign.CENTER;
            _txtLabel.y = 6;

            addChild(_txtLabel);
            _txtLabel.text = _label;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
