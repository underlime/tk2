/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.07.13
 * Time: 14:48
 */
package application.views.screen.top.tabs
{
    import framework.core.display.tabs.ITab;
    import framework.core.display.tabs.TabPanel;

    public class TopTabsPanel extends TabPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopTabsPanel()
        {

            var allTab:ITab = new AllTab();
            var weekTab:ITab = new WeekTab();
            var todayTab:ITab = new TodayTab();

            var arr:Vector.<ITab> = new Vector.<ITab>();
            arr.push(allTab, weekTab, todayTab);

            super(arr);

            _settings();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _settings():void
        {
            _x0 = 0;
            _y0 = 0;
            _dx = 151;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
