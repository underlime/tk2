/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.07.13
 * Time: 12:30
 */
package application.views.screen.top.tabs.interval
{
    import application.models.top.TopInterval;

    import framework.core.display.tabs.ITab;
    import framework.core.display.tabs.RelativeTabPanel;

    public class IntervalTabsPanel extends RelativeTabPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function IntervalTabsPanel()
        {
            var tabs:Vector.<ITab> = new Vector.<ITab>();
            tabs.push(new DayTab(), new WeekTab(), new AllTimeTab(), new ClansTab());
            super(tabs);

            _dx = 3;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get interval():TopInterval
        {
            if (tabs[selectedTab] is IntervalTopTab) {
                var tab:IntervalTopTab = tabs[selectedTab] as IntervalTopTab;
                return tab.interval;
            }
            return null;
        }
    }
}
