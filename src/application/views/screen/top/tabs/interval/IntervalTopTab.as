/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.07.13
 * Time: 12:22
 */
package application.views.screen.top.tabs.interval
{
    import application.events.ApplicationEvent;
    import application.models.top.TopInterval;
    import application.views.screen.common.buttons.SpecialButton;

    import flash.events.MouseEvent;

    public class IntervalTopTab extends SpecialButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const LEFT:String = "TOP_TAB_1";
        public static const CENTER:String = "TOP_TAB_2";
        public static const RIGHT:String = "TOP_TAB_3";
        private var _interval:TopInterval;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function IntervalTopTab(label:String, interval:TopInterval)
        {
            super(label);
            _interval = interval;
            _fontSize = 12;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        override public function select():void
        {
            super.select();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TAB_CHOOSE, true));
        }

        private function _clickHandler(event:MouseEvent):void
        {
            select();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get interval():TopInterval
        {
            return _interval;
        }
    }
}
