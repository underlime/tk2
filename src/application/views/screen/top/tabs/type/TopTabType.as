/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.07.13
 * Time: 11:24
 */
package application.views.screen.top.tabs.type
{
    import application.events.ApplicationEvent;
    import application.views.screen.common.buttons.AcceptButton;

    import flash.events.MouseEvent;

    public class TopTabType extends AcceptButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopTabType(label:String)
        {
            super(label);
            _fontSize = 12;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        override public function select():void
        {
            super.select();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TAB_CHOOSE, true));
        }

        private function _clickHandler(event:MouseEvent):void
        {
            select();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
