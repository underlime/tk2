/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.07.13
 * Time: 16:52
 */
package application.views.screen.top.buttons
{
    import application.views.screen.common.buttons.AppSpriteButton;
    import application.views.text.AppTextField;

    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    public class TopHeaderButton extends AppSpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopHeaderButton(source:String, label:String)
        {
            super(source);
            _label = label;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addLabel();
            this.scrollRect = new Rectangle(0, 0, _w, _h);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _label:String;
        protected var _txtLabel:AppTextField;
        protected var _margin:int = 20;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private function _addLabel():void
        {
            _txtLabel = new AppTextField();
            _txtLabel.width = _w - _margin;
            _txtLabel.size = 16;
            _txtLabel.color = 0xffffff;
            _txtLabel.bold = true;
            _txtLabel.align = TextFormatAlign.CENTER;
            _txtLabel.x = _margin;
            _txtLabel.y = 8;

            addChild(_txtLabel);
            _txtLabel.text = _label;
        }

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
