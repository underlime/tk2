/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 17:13
 */
package application.views.screen.top.buttons
{
    import application.config.AppConfig;
    import application.config.AppSettings;
    import application.sound.lib.UISound;
    import application.views.screen.common.buttons.AppSpriteButton;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.storage.LocalStorage;
    import framework.core.utils.Assert;

    public class SoundButton extends AppSpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "SOUND_BTN";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SoundButton()
        {
            super(SOURCE);

            _w = 36;
            _h = 36;

            _updateSoundProperty();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _checkSound();

            addEventListener(MouseEvent.CLICK, _clickHandler);
            _storage.addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            _storage.removeEventListener(Event.CHANGE, _changeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _storage:LocalStorage = LocalStorage.getInstance(AppConfig.APP_NAME);

        private var _enabled:Array = [0, 36, 72];
        private var _disabled:Array = [108, 144, 180];

        private var _default:Boolean = true;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _checkSound():void
        {
            if (_default)
                _update(_enabled);
            else
                _update(_disabled);

            super.update();
        }

        private function _update(arrPositions:Array):void
        {
            CONFIG::DEBUG {
                Assert.notEqual(arrPositions.length, 3);
            }
            _outPosition = arrPositions[0];
            _overPosition = arrPositions[1];
            _downPosition = arrPositions[2];
        }

        private function _updateSoundProperty():void
        {
            if (_storage.hasItem(AppSettings.SOUNDS_ENABLED))
                _default = Boolean(parseInt(_storage.getItem(AppSettings.SOUNDS_ENABLED)));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _updateSoundProperty();
            _checkSound();
        }

        private function _clickHandler(event:MouseEvent):void
        {
            _default = !_default;
            var val:String = _default ? "1" : "0";
            _storage.setItem(AppSettings.SOUNDS_ENABLED, val);
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
