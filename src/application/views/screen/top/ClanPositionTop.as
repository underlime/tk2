/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.03.14
 * Time: 9:41
 */
package application.views.screen.top
{
    import application.events.ClanEvent;
    import application.models.ModelData;
    import application.models.clans.ClanInfo;
    import application.models.user.User;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.screen.clans.show.ClanLogoHelper;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;

    import framework.core.helpers.ImagesHelper;
    import framework.core.struct.data.ModelsRegistry;

    public class ClanPositionTop extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const NO_CLAN_SRC:String = "NoClanIcon";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanPositionTop(position:int, clanInfo:ClanInfo)
        {
            super();
            _position = position;
            _clanInfo = clanInfo;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addPosition();
            _addLogo();
            if (_clanInfo.id.value > 0)
                _addClanName();
            else
                _addNoClanMessage();

            if (_clanInfo.members_count.value > 0)
                _addClanMembersCount();

            if (_clanInfo.glory.value > 0)
                _addGlory();

            if (_clanInfo.id.value > 0) {
                addEventListener(MouseEvent.CLICK, _clickHandler);
                buttonMode = true;
                useHandCursor = true;
            }
        }

        override public function destroy():void
        {
            _topNumber.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _clanInfo:ClanInfo;

        private var _background:Bitmap;
        private var _gloryPosition:GloryPosition;
        private var _position:int;
        private var _topNumber:TopNumber;
        private var _user:User = ModelsRegistry.getModel(ModelData.USER) as User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            _background = super.source.getBitmap(TopPosition.BACK);
            _background.scrollRect = new Rectangle(0, 0, TopPosition.POSITION_WIDTH, TopPosition.POSITION_HEIGHT);

            addChild(_background);

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _addPosition():void
        {
            _topNumber = new TopNumber(_position);
            _topNumber.x = 5;
            _topNumber.y = 5;
            addChild(_topNumber)
        }

        private function _addLogo():void
        {
            var logo:Bitmap;
            if (_clanInfo.id.value > 0) {
                logo = ClanLogoHelper.getLogoById(_clanInfo.picture.value);
            } else {
                logo = super.source.getBitmap(NO_CLAN_SRC);
            }

            var smallLogo:Bitmap = new Bitmap(ImagesHelper.matrixResize(logo, 30, 30, true));
            smallLogo.x = 46;
            smallLogo.y = 5;
            addChild(smallLogo);
        }

        private function _addClanName():void
        {
            var txtLevel:AppTextField = new AppTextField();
            txtLevel.size = 14;
            txtLevel.color = 0xbe1e2d;
            txtLevel.bold = true;
            txtLevel.autoSize = TextFieldAutoSize.LEFT;

            addChild(txtLevel);
            txtLevel.text = _clanInfo.clan_name.value;
            txtLevel.x = 88;
            txtLevel.y = 7;
        }

        private function _addNoClanMessage():void
        {
            var txtLevel:AppTextField = new AppTextField();
            txtLevel.size = 14;
            txtLevel.color = 0x3c2415;
            txtLevel.bold = true;
            txtLevel.autoSize = TextFieldAutoSize.LEFT;

            addChild(txtLevel);
            txtLevel.text = "Вы не состоите в клане";
            txtLevel.x = 88;
            txtLevel.y = 7;
        }

        private function _addClanMembersCount():void
        {
            var txt:AppTextField = new AppTextField();
            txt.color = 0x3c2415;
            txt.autoSize = TextFieldAutoSize.LEFT;
            txt.bold = true;
            txt.x = 355;
            txt.y = 8;
            addChild(txt);
            txt.text = _clanInfo.members_count.value.toString() + "/" + _user.gameConstants.clan_max_members.value.toString();
        }

        private function _addGlory():void
        {
            _gloryPosition = new GloryPosition(_clanInfo.glory.value);
            addChild(_gloryPosition);
            _gloryPosition.x = 628;
            _gloryPosition.y = 1;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            _background.scrollRect = new Rectangle(0, TopPosition.POSITION_HEIGHT, TopPosition.POSITION_WIDTH, TopPosition.POSITION_HEIGHT);
            super.sound.playUISound(UISound.MOUSE_OVER);
        }

        private function _outHandler(event:MouseEvent):void
        {
            _background.scrollRect = new Rectangle(0, 0, TopPosition.POSITION_WIDTH, TopPosition.POSITION_HEIGHT);
        }

        private function _clickHandler(event:MouseEvent):void
        {
            var ev:ClanEvent = new ClanEvent(ClanEvent.SHOW, true);
            ev.clan_id = _clanInfo.id.value;
            dispatchEvent(ev);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
