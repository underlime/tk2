/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.03.14
 * Time: 19:20
 */
package application.views.screen.top
{
import application.common.Colors;
import application.models.ModelData;
import application.models.clans.ClanInfo;
import application.models.clans.ClansTop;
import application.models.user.User;
import application.views.AppView;
import application.views.TextFactory;
import application.views.popup.common.FerrosPrice;
import application.views.screen.common.scroll.ScrollObserver;
import application.views.screen.common.scroll.Scrollbar;
import application.views.screen.common.text.ShadowAppText;
import application.views.text.AppTextField;

import flash.geom.Rectangle;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormatAlign;

import framework.core.helpers.DateHelper;
import framework.core.helpers.StringHelper;
import framework.core.struct.data.ModelsRegistry;

import org.casalib.util.NumberUtil;

public class ClansTopLayout extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClansTopLayout()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addText();

            if (_model.id.value > 0) {
                _addSeasonInfo();
                _addPrizeInfo();
            }

            _addPositions();
            _addUserPosition();
        }

        override public function destroy():void
        {
            if (_scrollObserver && !_scrollObserver.destroyed)
                _scrollObserver.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:ClansTop = ModelsRegistry.getModel(ModelData.CLANS_TOP) as ClansTop;
        private var _scrollbar:Scrollbar;
        private var _clanCont:AppView;
        private var _scrollObserver:ScrollObserver;
        private var _userPosition:UserClanPosition;
        private var _user:User = ModelsRegistry.getModel(ModelData.USER) as User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addText():void
        {
            var txt:AppTextField = new AppTextField();
            txt.width = 380;
            txt.wordWrap = true;
            txt.multiline = true;
            txt.color = Colors.DARK_BROWN;
            txt.height = 40;
            txt.x = 30;
            txt.y = 120;
            addChild(txt);
            var total:int = _user.gameConstants.clans_to_award.value;
            txt.text = TextFactory.instance.getLabel('clans_top_info').replace("{{count}}", total.toString() + " " + StringHelper.chooseRuPlural(total, ['клан', 'клана', 'кланов']));
        }

        private function _addSeasonInfo():void
        {
            var back:ClansTopInfoBack = new ClansTopInfoBack();
            back.x = 468;
            back.y = 76;
            addChild(back);

            var txt:ShadowAppText = new ShadowAppText(Colors.DARK_BROWN);
            txt.align = TextFormatAlign.RIGHT;
            txt.autoSize = TextFieldAutoSize.NONE;
            txt.width = 234;
            txt.x = back.x;
            txt.y = back.y + 7;
            addChild(txt);
            txt.text = DateHelper.getDifferense(_model.season_end);

            var txtHeader:AppTextField = new AppTextField();
            txtHeader.color = Colors.DARK_BROWN;
            txtHeader.size = 14;
            txtHeader.bold = true;
            txtHeader.x = back.x + 12;
            txtHeader.y = back.y + 9;
            txtHeader.width = 200;
            addChild(txtHeader);
            var month:String = NumberUtil.format(_model.season_end.value.getMonth(), "", 2, "0");
            txtHeader.text = TextFactory.instance.getLabel('clan_season_label') + " " + month + "." + _model.season_end.value.getFullYear().toString();
        }

        private function _addPrizeInfo():void
        {
            var back:ClansTopInfoBack = new ClansTopInfoBack();
            back.x = 468;
            back.y = 125;
            addChild(back);

            var txtHeader:AppTextField = new AppTextField();
            txtHeader.color = Colors.DARK_BROWN;
            txtHeader.size = 14;
            txtHeader.bold = true;
            txtHeader.x = back.x + 12;
            txtHeader.y = back.y + 7;
            txtHeader.width = 200;
            addChild(txtHeader);
            txtHeader.text = TextFactory.instance.getLabel('clan_season_prize') + ": ";

            var price:FerrosPrice = new FerrosPrice(_model.season_prize.value);
            addChild(price);
            price.x = back.x + 239 - price.priceWidth;
            price.y = back.y + 7;
        }

        private function _addPositions():void
        {
            _clanCont = new AppView();
            _clanCont.x = 24;
            _clanCont.y = 180;

            var list:Object = _model.clans_top_glory.list.value;

            var dy:int = 42;
            var cy:int = 0;
            var i:int = 1;
            for (var key:String in list) {
                var info:ClanInfo = list[key];
                var position:ClanPositionTop = new ClanPositionTop(i, info);
                position.y = cy;
                _clanCont.addChild(position);
                cy += dy;
                i++;
            }
            addChild(_clanCont);

            if (_model.clans_top_glory.length > 6) {
                _addScrollBar();
            }
        }

        private function _addScrollBar():void
        {
            _scrollbar = new Scrollbar(251);
            addChild(_scrollbar);
            _scrollbar.x = 701;
            _scrollbar.y = 178;

            _clanCont.$drawBackground(0);
            _scrollObserver = new ScrollObserver(_clanCont, _scrollbar, new Rectangle(0, 0, 676, 250));
        }

        private function _addUserPosition():void
        {
            _userPosition = new UserClanPosition();
            _userPosition.x = 24;
            _userPosition.y = 445;
            addChild(_userPosition);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
