/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.07.13
 * Time: 12:11
 */
package application.views.screen.top
{
    import framework.core.struct.view.View;

    public class TopPositionsContainer extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopPositionsContainer(positions:Vector.<TopPosition>)
        {
            super();
            _positions = positions;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addPositions();
            _addBackground(); // хак для прокрутки колесом
        }

        override public function destroy():void
        {
            var count:int = _positions.length;
            for (var i:int = 0; i < count; i++)
                if (_positions[i] && !_positions[i].destroyed)
                    _positions[i].destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _positions:Vector.<TopPosition>;
        private var _margin:int = 2;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            this.graphics.beginFill(0xffffff, 0);
            this.graphics.drawRect(0, 0, this.width, this.height);
            this.graphics.endFill();
        }

        private function _addPositions():void
        {
            var position:int = 1;
            var count:int = _positions.length;
            var cy:int = 0;

            for (var i:int = 0; i < count; i++) {

                var topPosition:TopPosition = _positions[i]
                addChild(topPosition);
                topPosition.y = cy;

                position++;
                cy += TopPosition.POSITION_HEIGHT + _margin;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get renderHeight():int
        {
            var count:int = _positions.length;
            return count * TopPosition.POSITION_HEIGHT + (count - 1) * _margin;
        }

    }
}
