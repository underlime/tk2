/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.08.13
 * Time: 11:00
 */
package application.views.screen.user_info
{
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;

    import framework.core.struct.view.View;

    public class IconPosition extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function IconPosition(icon:Bitmap, min:int, max:int = 0)
        {
            super();
            _icon = icon;
            _min = min;
            _max = max;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addIcon();
            _addValue();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var verticalMargin:int = 0

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;
        private var _min:int;
        private var _max:int;

        private var _txtValue:ShadowAppText;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addIcon():void
        {
            addChild(_icon);
        }

        private function _addValue():void
        {
            _txtValue = new ShadowAppText(0x653b28);
            _txtValue.size = 14;
            _txtValue.color = 0xffffff;
            addChild(_txtValue);

            _txtValue.text = _min.toString();
            _txtValue.x = _icon.width + 3;
            _txtValue.y = 20 - _txtValue.textHeight / 2 + verticalMargin;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
