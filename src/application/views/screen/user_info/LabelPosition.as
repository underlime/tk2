/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.08.13
 * Time: 11:40
 */
package application.views.screen.user_info
{
    import application.views.text.AppTextField;

    import flash.text.TextFieldAutoSize;

    import framework.core.struct.view.View;

    public class LabelPosition extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LabelPosition(label:String, labelValue:String)
        {
            super();
            _label = label;
            _labelValue = labelValue;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addText();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _label:String;
        private var _labelValue:String;

        private var _txtLabel:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addText():void
        {
            _txtLabel = new AppTextField();
            _txtLabel.color = 0x3c2415;
            _txtLabel.size = 12;
            _txtLabel.autoSize = TextFieldAutoSize.LEFT;
            _txtLabel.bold = true;
            addChild(_txtLabel);
            _txtLabel.text = _label + ": " + _labelValue;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
