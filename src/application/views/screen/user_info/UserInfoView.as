/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.08.13
 * Time: 10:53
 */
package application.views.screen.user_info
{
    import application.models.inventory.Inventory;
    import application.models.user.IUserData;
    import application.views.TextFactory;
    import application.views.helpers.icons.IconType;
    import application.views.helpers.icons.IconsFactory;
    import application.views.screen.common.frame.UserFrame;
    import application.views.screen.home.UserInfoBlock;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.SecurityErrorEvent;
    import flash.net.URLRequest;
    import flash.system.LoaderContext;
    import flash.text.TextFieldAutoSize;

    import framework.core.helpers.ImagesHelper;
    import framework.core.struct.view.View;

    import org.casalib.util.ConversionUtil;

    public class UserInfoView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "CHAR_INFO_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserInfoView(userData:IUserData, inventory:Inventory)
        {
            super();
            _userData = userData;
            _inventory = inventory;
            this.y = 107;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _addAvatar();
            _addName();
            _addIcons();
            _addUserInfoBlock();
            _addStats();
        }

        override public function destroy():void
        {
            if (_avatarLoader) {
                _avatarLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, _onAvatarLoaded);
                _avatarLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
                _avatarLoader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);
            }

            if (_avatarContainer && !_avatarContainer.destroyed)
                _avatarContainer.destroy();

            if (_userInfoBlock)
                _userInfoBlock.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _userData:IUserData;
        protected var _inventory:Inventory;

        private var _userInfoBlock:UserInfoBlock;
        private var _txtUserName:AppTextField;
        private var _avatarContainer:View;
        private var _avatarLoader:Loader;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            var back:Bitmap = super.source.getBitmap(BACKGROUND);
            addChild(back);

            back.x = 49;
            back.y = 112;
        }

        private function _addAvatar():void
        {
            var icon:Bitmap = super.source.getBitmap(UserFrame.DEFAULT_ICON, true);
            var data:BitmapData = ImagesHelper.matrixResize(icon, 40, 40, true);
            var resizeIcon:Bitmap = new Bitmap(data);
            addChild(resizeIcon);

            resizeIcon.x = 53;
            resizeIcon.y = 114;

            _avatarLoader = new Loader();
            _avatarLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, _onAvatarLoaded);
            _avatarLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, _onAvatarError);
            _avatarLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _onAvatarError);

            var request:URLRequest = new URLRequest(_userData.socNetUserInfo.picture.value);
            var context:LoaderContext = new LoaderContext(true);
            _avatarLoader.load(request, context);
        }

        private function _onAvatarLoaded(e:Event):void
        {
            _avatarContainer = new View();
            addChild(_avatarContainer);

            _avatarContainer.addChild(_avatarLoader);
            _avatarContainer.width = 35;
            _avatarContainer.height = 35;
            _avatarContainer.x = 54;
            _avatarContainer.y = 116.5;
        }

        private function _onAvatarError(e:Event):void
        {
        }

        private function _addName():void
        {
            _txtUserName = new AppTextField();
            _txtUserName.color = 0x3c2415;
            _txtUserName.size = 16;
            _txtUserName.autoSize = TextFieldAutoSize.LEFT;
            _txtUserName.bold = true;
            addChild(_txtUserName);

            var firstName:String = _userData.socNetUserInfo.first_name.value;
            var lastName:String = _userData.socNetUserInfo.last_name.value;
            _txtUserName.text = firstName + ' ' + lastName;
            _txtUserName.x = 94;
            _txtUserName.y = 120;
        }

        private function _addIcons():void
        {
            var hp:int = _userData.userLevelParams.max_hp.value + _userData.userBonus.scale.hp.value;
            var hpPosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.HP_ICO), hp);
            hpPosition.x = 61;
            hpPosition.y = 159;
            hpPosition.verticalMargin = -3;
            addChild(hpPosition);

            var expPosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.EXPERIENCE_ICO), _userData.userFightData.experience.value);
            expPosition.x = 61;
            expPosition.y = 199;
            addChild(expPosition);

            var gloryPosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.GLORY_ICO), _userData.userFightData.glory.value);
            gloryPosition.x = 61;
            gloryPosition.y = 239;
            addChild(gloryPosition);

            var strengthPosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.STRENGTH_ICO), _userData.userLevelParams.strength.value);
            strengthPosition.x = 223;
            strengthPosition.y = 159;
            addChild(strengthPosition);

            var agilityPosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.AGILITY_ICO), _userData.userLevelParams.agility.value);
            agilityPosition.x = 223;
            agilityPosition.y = 199;
            addChild(agilityPosition);

            var intellectPosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.INTELLECT_ICO), _userData.userLevelParams.intellect.value);
            intellectPosition.x = 223;
            intellectPosition.y = 239;
            addChild(intellectPosition);

            var damage:int = _userData.userBonus.fight.damage.value;
            var damagePosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.DAMAGE_ICO), damage);
            damagePosition.x = 323;
            damagePosition.y = 159;
            addChild(damagePosition);

            var armor:int = _userData.userBonus.fight.armor.value;
            var armorPosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.ARMOR_ICO), armor);
            armorPosition.x = 323;
            armorPosition.y = 199;
            addChild(armorPosition);

            var speed:int = _userData.userBonus.fight.speed.value;
            var speedPosition:IconPosition = new IconPosition(IconsFactory.getIcon(IconType.SPEED_ICO), speed);
            speedPosition.x = 323;
            speedPosition.y = 239;
            addChild(speedPosition);
        }

        protected function _addUserInfoBlock():void
        {
            _userInfoBlock = new UserInfoBlock(_userData, _inventory);
            _userInfoBlock.x = 438;
            _userInfoBlock.y = 37;
            addChild(_userInfoBlock);
        }

        private function _addStats():void
        {
            var factory:TextFactory = TextFactory.instance;
            var battleCount:int = _userData.userFightData.battles_count.value;
            var winsCount:int = _userData.userFightData.wins_count.value;

            var battles:LabelPosition = new LabelPosition(factory.getLabel('stats_battle_label'), battleCount.toString());
            addChild(battles);
            battles.x = 60;
            battles.y = 287;

            var winsPercent:Number = battleCount > 0 ? Math.round((winsCount / battleCount) * 100) : 0;
            var wins:LabelPosition = new LabelPosition(factory.getLabel('stats_wins_label'), winsCount.toString() + " (" + winsPercent.toString() + "%)");
            addChild(wins);
            wins.x = 60;
            wins.y = 310;

            var loseCount:int = battleCount - winsCount;
            var loses:LabelPosition = new LabelPosition(factory.getLabel('stats_loses_label'), loseCount.toString());
            addChild(loses);
            loses.x = 60;
            loses.y = 331;

            var totalSecs:int = ((new Date()).getTime() - _userData.userInfo.reg_time.value.getTime()) / 1000;
            var totalDays:int = Math.floor(ConversionUtil.secondsToDays(totalSecs));
            var days:LabelPosition = new LabelPosition(factory.getLabel('stats_days_in_game'), totalDays.toString());
            addChild(days);
            days.x = 203;
            days.y = 287;

            var ferros:LabelPosition = new LabelPosition(factory.getLabel('stats_ferros_payed_label'), _userData.userStat.ferros_payed.value.toString());
            addChild(ferros);
            ferros.x = 203;
            ferros.y = 310;

            var tomatos:LabelPosition = new LabelPosition(factory.getLabel('stats_tomatos_payed_label'), _userData.userStat.tomatos_payed.value.toString());
            addChild(tomatos);
            tomatos.x = 203;
            tomatos.y = 331;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
