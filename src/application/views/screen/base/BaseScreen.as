/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.08.13
 * Time: 14:32
 */
package application.views.screen.base
{
    import application.config.AppConfig;
    import application.views.screen.common.buttons.CloseButton;

    import flash.display.Bitmap;

    import framework.core.struct.view.View;

    public class BaseScreen extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "COMMON_WINDOW_BG_NEW";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseScreen(label:String)
        {
            _label = label;
            super();
            this.y = 107;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            _addBackground();
            _addCloseButton();
        }

        override protected function render():void
        {
            _addScreenTitle();
        }

        override public function destroy():void
        {
            _screenTitle.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _addBackground():void
        {
            _background = super.source.getBitmap(BACKGROUND);
            addChild(_background);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _screenTitle:ScreenTitle;

        private var _closeButton:CloseButton;
        private var _background:Bitmap;
        private var _label:String;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addCloseButton():void
        {
            _closeButton = new CloseButton();
            addChild(_closeButton);
            _closeButton.x = _background.width - _closeButton.w;
            _closeButton.y = -10;
        }

        private function _addScreenTitle():void
        {
            _screenTitle = new ScreenTitle(_label);
            addChild(_screenTitle);
            _screenTitle.x = AppConfig.APP_WIDTH / 2 - _screenTitle.width / 2;
            _screenTitle.y = -12;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get label():String
        {
            return _label;
        }

        public function set label(value:String):void
        {
            _label = value;
            if (_screenTitle)
                _screenTitle.title = _label.toUpperCase();
        }
    }
}
