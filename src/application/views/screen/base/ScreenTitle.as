/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 09.09.13
 * Time: 16:50
 */
package application.views.screen.base
{
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class ScreenTitle extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SOURCE:String = "TITLE_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ScreenTitle(title:String)
        {
            super();
            _title = title;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBack();
            _addLabel();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _title:String;
        private var _back:Bitmap;
        private var _txtLabel:AppTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            _back = super.source.getBitmap(SOURCE);
            addChild(_back);
        }

        private function _addLabel():void
        {
            _txtLabel = new AppTextField();
            _txtLabel.size = 16;
            _txtLabel.bold = true;
            _txtLabel.color = 0x3c2415;
            _txtLabel.width = _back.width;
            _txtLabel.height = _back.height;
            _txtLabel.align = TextFormatAlign.CENTER;
            addChild(_txtLabel);
            _txtLabel.text = _title.toUpperCase();
            _txtLabel.x = _back.x;
            _txtLabel.y = _back.y + _back.height / 2 - _txtLabel.textHeight / 2 - 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set title(value:String):void
        {
            if (_txtLabel)
                _txtLabel.text = value;
        }

    }
}
