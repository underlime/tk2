/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 13:44
 */
package application.views.screen.opponents
{
    import application.events.BattleEvent;
    import application.events.ClanEvent;
    import application.models.inventory.Inventory;
    import application.models.user.IUserData;
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.buttons.RejectButton;
    import application.views.screen.common.buttons.SpecialButton;
    import application.views.screen.home.UserInfoBlock;

    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;

    import framework.core.helpers.ProfileLinkHelper;
    import framework.core.socnet.SocNet;

    public class OpponentInfoBlock extends UserInfoBlock
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OpponentInfoBlock(user:IUserData, inventory:Inventory)
        {
            super(user, inventory);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            if (_user.clanInfo.id.value > 0) {
                _buttonFontSize = 12;
                _buttonWidth = 82;
            }

            super.render();

            _addProfileButton();
            if (_user.clanInfo.id.value > 0) {
                _addClanButton();
            }
        }

        override public function destroy():void
        {
            _battleButton.destroy();
            _profileButton.destroy();
            if (_clanButton)
                _clanButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _battleButton:RejectButton;
        private var _clanButton:AcceptButton;
        private var _profileButton:SpecialButton;

        private var _buttonWidth:int = 125;
        private var _buttonFontSize:int = 14;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addProfileButton():void
        {
            _profileButton = new SpecialButton(TextFactory.instance.getLabel('profile_label'));
            _profileButton.fontSize = _buttonFontSize;
            _profileButton.w = _buttonWidth;

            addChild(_profileButton);
            _profileButton.x = 9;
            _profileButton.y = 285;

            _profileButton.addEventListener(MouseEvent.CLICK, _profileClickHandler);
        }

        override protected function _addBottomButton():void
        {
            _battleButton = new RejectButton(TextFactory.instance.getLabel('fight_context_label'));
            _battleButton.w = _buttonWidth;
            _battleButton.fontSize = _buttonFontSize;
            addChild(_battleButton);
            _battleButton.x = 95;
            _battleButton.y = 285;


            _battleButton.addEventListener(MouseEvent.CLICK, _clickHandler);
            if (!_user.clanInfo.id.value) {
                _battleButton.x = 145;
            }
        }

        private function _addClanButton():void
        {
            _clanButton = new AcceptButton(TextFactory.instance.getLabel('clan_label'));
            _clanButton.w = _buttonWidth;
            _clanButton.fontSize = _buttonFontSize;
            addChild(_clanButton);
            _clanButton.x = _battleButton.x + _battleButton.w + 5;
            _clanButton.y = 285;

            _clanButton.addEventListener(MouseEvent.CLICK, _clanClickHandler);
        }

        private function _clanClickHandler(event:MouseEvent):void
        {
            var ev:ClanEvent = new ClanEvent(ClanEvent.SHOW, true);
            ev.clan_id = _user.clanInfo.id.value;
            dispatchEvent(ev);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new BattleEvent(BattleEvent.CALL_BATTLE, true));
            super.sound.playUISound(UISound.CLICK);
        }

        private function _profileClickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);

            var url:String;
            if (SocNet.instance().socNetName == SocNet.MY_MAIL_RU)
                url = _user.socNetUserInfo.link.value;
            else
                url = ProfileLinkHelper.getLink(_user.userInfo.soc_net_id.value.toString());
            navigateToURL(new URLRequest(url), "_blank");
            trace(_user.userInfo.soc_net_id.value);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
