/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 12:14
 */
package application.views.screen.opponents
{
    import application.views.screen.home.HomeScreen;

    import flash.events.Event;

    public class OpponentScreen extends HomeScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OpponentScreen()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _setupTabs():void
        {
            _tabs = new OpponentTabs();
            _tabs.addEventListener(Event.CHANGE, _changeHandler);
            addChild(_tabs);
            _tabs.select(0);

            _tabs.y = 43;
            _tabs.x = 50;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
