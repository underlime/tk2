/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 14:41
 */
package application.views.screen.opponents
{
    import application.models.ModelData;
    import application.models.inventory.Inventory;
    import application.models.user.IUserData;
    import application.models.user.User;
    import application.views.screen.specials.SpecialsScreen;

    import framework.core.struct.data.ModelsRegistry;

    public class OpponentSpecialsScreen extends SpecialsScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OpponentSpecialsScreen(user:IUserData, inventory:Inventory)
        {
            super(user, inventory);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            super.setup();
            var userModel:User = ModelsRegistry.getModel(ModelData.USER) as User;
            _totalCount = userModel.userOptInfo.specials_count.value;
        }

        override protected function _addUserInfoBlock():void
        {
            _opponentsInfoBlock = new OpponentInfoBlock(_user, _inventory);
            _opponentsInfoBlock.x = 438;
            _opponentsInfoBlock.y = 37;
            addChild(_opponentsInfoBlock);
        }

        override public function destroy():void
        {
            _opponentsInfoBlock.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _opponentsInfoBlock:OpponentInfoBlock;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
