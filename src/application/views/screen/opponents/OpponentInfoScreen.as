/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 14:29
 */
package application.views.screen.opponents
{
    import application.models.inventory.Inventory;
    import application.models.user.IUserData;
    import application.views.screen.user_info.UserInfoView;

    public class OpponentInfoScreen extends UserInfoView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OpponentInfoScreen(userData:IUserData, inventory:Inventory)
        {
            super(userData, inventory);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _addUserInfoBlock():void
        {
            _opponentsInfoBlock = new OpponentInfoBlock(_userData, _inventory);
            _opponentsInfoBlock.x = 438;
            _opponentsInfoBlock.y = 37;
            addChild(_opponentsInfoBlock);
        }

        override public function destroy():void
        {
            _opponentsInfoBlock.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _opponentsInfoBlock:OpponentInfoBlock;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
