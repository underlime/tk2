/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.06.13
 * Time: 10:17
 */
package application.views.constructor.builders
{
    import application.config.AppConfig;
    import application.config.AppSettings;
    import application.models.market.Item;
    import application.models.user.UserInfo;
    import application.views.constructor.common.Details;
    import application.views.constructor.helpers.CharHelper;

    import com.greensock.TweenMax;

    import flash.filters.DropShadowFilter;

    import framework.core.helpers.MathHelper;
    import framework.core.storage.LocalStorage;

    public class CharsBuilder extends TemplateBinder
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const DOPPELGANGER_ID:int = -1;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CharsBuilder(userInfo:UserInfo, items:Vector.<Item>)
        {
            super(userInfo, items);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            this.construct();
            _addStroke();
            _checkBot();
        }

        override public function construct():void
        {
            super.construct();
            this.build();
        }

        override public function update():void
        {
            super.update();
            this.build();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function build():void
        {
            _setBody(super.userInfo.body.value);
            _setHands(super.userInfo.hands.value);
            _setHair(super.userInfo.hair.value);
            _setEyes(super.userInfo.eyes.value);
            _setMouth(super.userInfo.mouth.value);
            _setWounds();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _woundLevel:int = 1;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setBody(body:String):void
        {
            if (!super.bodyContainer) {
                throw new Error("Body Container is not detected! State: " + super.state.toString());
            }

            if (super.body) {
                CharHelper.remove(super.body);
            }

            super.body = super.stash.getChar(body);
            super.body.name = Details.BODY;
            super.bodyContainer.addChild(super.body);
        }

        protected function _setHands(hands:String):void
        {
            if (!super.lHandContainer) {
                throw new Error("Left Hand Container is not detected! State: " + super.state.toString());
            }
            if (super.lHand) {
                CharHelper.remove(super.lHand);
            }

            super.lHand = super.stash.getChar(hands, false);
            super.lHand.name = Details.LEFT_HAND;
            super.lHandContainer.addChild(this.lHand);

            if (!this.rHandContainer) {
                throw new Error("Right Hand Container is not detected! State: " + super.state.toString());
            }
            if (super.rHand) {
                CharHelper.remove(super.rHand);
            }

            super.rHand = super.stash.getChar(hands, false);
            super.rHand.name = Details.RIGHT_HAND;
            super.rHandContainer.addChild(this.rHand);
        }

        protected function _setWounds():void
        {
            if (!super.woundContainer) {
                throw new Error("Wound Container is not detected! State: " + super.state.toString());
            }
            if (super.wound) {
                CharHelper.remove(super.wound);
            }

            _woundLevel = MathHelper.clamp(_woundLevel, 1, 10);

            super.wound = super.stash.getChar(super.userInfo.vegetable.value + Details.WOUND_POSTFIX);
            super.wound.gotoAndStop(_woundLevel);
            super.wound.name = Details.WOUND;
            super.woundContainer.addChild(super.wound);
            super.bodyContainer.swapChildren(super.body, super.woundContainer);
        }


        protected function _setHair(hair:String):void
        {
            _unsetHair();
            if (!super.hairContainer)
                throw new Error("Hair Container is not detected! State: " + super.state.toString());

            super.hair = super.stash.getChar(hair);
            super.hair.name = Details.HAIR;
            super.hairContainer.addChild(this.hair);
        }

        protected function _unsetHair():void
        {
            if (super.hair)
                CharHelper.remove(super.hair);
        }

        protected function _setEyes(eyes:String):void
        {
            var thumbX:Number;
            var thumbY:Number;
            var thumbZ:int;

            if (!super.faceContainer)
                throw new Error("face container is not detected! State: " + super.state.toString());

            if (super.eyes) {
                thumbX = this.eyes.x;
                thumbY = this.eyes.y;
                thumbZ = this.faceContainer.getChildIndex(super.eyes);

                CharHelper.remove(super.eyes);
            }

            super.eyes = super.stash.getChar(eyes);
            super.eyes.name = Details.EYES;
            super.faceContainer.addChildAt(this.eyes, thumbZ);

            super.eyes.x = thumbX;
            super.eyes.y = thumbY;
        }

        protected function _setMouth(mouth:String):void
        {
            if (!super.faceContainer)
                throw new Error("face is not detected! State: " + super.state.toString());

            if (super.mouth) {
                _mouthX = super.mouth.x;
                _mouthY = super.mouth.y;
                _mouthIndex = super.faceContainer.getChildIndex(super.mouth);

                CharHelper.remove(super.mouth);
            }

            super.mouth = super.stash.getChar(mouth);
            super.mouth.name = Details.MOUTH;
            super.faceContainer.addChildAt(super.mouth, _mouthIndex);
            super.mouth.x = _mouthX;
            super.mouth.y = _mouthY;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _mouthX:Number = 0;
        private var _mouthY:Number = 0;
        private var _mouthIndex:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addStroke():void
        {
            var filter:DropShadowFilter = new DropShadowFilter(0, 0, 0x000000, 1, 5, 5, 6);
            var storage:LocalStorage = LocalStorage.getInstance(AppConfig.APP_NAME);

            if (storage.hasItem(AppSettings.CARTOON_STROKE)) {
                var stroke:Boolean = Boolean(parseInt(storage.getItem(AppSettings.CARTOON_STROKE)));
                if (stroke)
                    this.filters = [filter];
            }
        }

        private function _checkBot():void
        {
            if (super.userInfo.user_id.value == DOPPELGANGER_ID) {
                TweenMax.to(this, 0, { colorMatrixFilter: { saturation: 0.2 }});
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
