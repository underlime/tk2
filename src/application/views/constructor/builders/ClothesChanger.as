/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.06.13
 * Time: 14:55
 */
package application.views.constructor.builders
{
    import application.common.ArrangementVariant;
    import application.models.market.Item;
    import application.models.user.UserInfo;
    import application.views.constructor.common.WeaponTemplate;

    public class ClothesChanger extends ClothesBuilder
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClothesChanger(userInfo:UserInfo, items:Vector.<Item>)
        {
            _registerHash();
            super(userInfo, items);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function addItem(item:Item):void
        {
            var arrangement:String = item.arrangement_variant.value;
            if (_hash[arrangement]) {
                var method:Function = _hash[arrangement];
                method.call(this, item);
            }
        }

        public function addHat(item:Item):void
        {
            _tempHat = item;
            super._setHat(item.picture.value);
        }

        public function addGlasses(item:Item):void
        {
            _tempGlasses = item;
            super._setGlasses(item.picture.value);
        }

        public function addMask(item:Item):void
        {
            _tempMask = item;
            super._setMask(item.picture.value);
        }

        public function addCoat(item:Item):void
        {
            _tempCoat = item;
            super._setCoat(item.picture.value);
        }

        public function addShield(item:Item):void
        {
            if (super.weaponTemplate == WeaponTemplate.TWO_HANDED_WEAPON)
                _setWithoutWeaponTemplate();
            super._setShield(item.picture.value);
        }

        public function addGloves(item:Item):void
        {
            if (super.weaponTemplate != WeaponTemplate.WITHOUT_WEAPON)
                _setWithoutWeaponTemplate();
            super._setGloves(item.picture.value);
        }

        public function addWeapon(item:Item):void
        {
            switch (item.arrangement_variant.value) {
                case ArrangementVariant.ONE_HANDED.toString() :
                    _addOneHandedWeapon(item);
                    break;
                case ArrangementVariant.ROD_WEAPON.toString() :
                    _addRodWeapon(item);
                    break;
                case ArrangementVariant.TWO_HANDED.toString() :
                    _addTwoHandedWeapon(item);
                    break;
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _hash:Object = {};

        private var _tempHat:Item;
        private var _tempGlasses:Item;
        private var _tempMask:Item;
        private var _tempCoat:Item;
        private var _tempGloves:Item;
        private var _tempWeapon:Item;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _registerHash():void
        {
            _hash[ArrangementVariant.COAT.toString()] = addCoat;
            _hash[ArrangementVariant.GLASSES.toString()] = addGlasses;
            _hash[ArrangementVariant.GLOVES.toString()] = addGloves;
            _hash[ArrangementVariant.HAT.toString()] = addHat;
            _hash[ArrangementVariant.MASK.toString()] = addMask;
            _hash[ArrangementVariant.ONE_HANDED.toString()] = addWeapon;
            _hash[ArrangementVariant.ROD_WEAPON.toString()] = addWeapon;
            _hash[ArrangementVariant.TWO_HANDED.toString()] = addWeapon;
            _hash[ArrangementVariant.SHIELD.toString()] = addShield;
        }

        private function _addOneHandedWeapon(item:Item):void
        {
            super._weaponTemplate = WeaponTemplate.ONE_HANDED_WEAPON;
            super.update();
            super._setWeapon(item.picture.value);
        }

        private function _addRodWeapon(item:Item):void
        {
            super._weaponTemplate = WeaponTemplate.ROD_WEAPON;
            super.update();
            super.undress();
            super._setWeapon(item.picture.value);
        }

        private function _addTwoHandedWeapon(item:Item):void
        {
            super._weaponTemplate = WeaponTemplate.TWO_HANDED_WEAPON;
            super.update();
            super.undress();
            super._setWeapon(item.picture.value);
        }

        private function _setWithoutWeaponTemplate():void
        {
            super._weaponTemplate = WeaponTemplate.WITHOUT_WEAPON;
            super.update();
            super.undress();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
