/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.07.13
 * Time: 10:52
 */
package application.views.constructor.builders
{
    import application.models.market.Item;
    import application.models.user.UserInfo;
    import application.views.constructor.common.TemplateState;
    import application.views.constructor.common.TemplateType;
    import application.views.constructor.helpers.MultiStateHelper;

    import flash.display.MovieClip;
    import flash.events.Event;

    public class MovingUnit extends ClothesBuilder
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SHADOW:String = "shade";

        public static const ATACK_PREFIX:String = "a";
        public static const DEFENSE_PREFIX:String = "d";
        public static const KO_PREFIX:String = "ko";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MovingUnit(userInfo:UserInfo, items:Vector.<Item>)
        {
            super(userInfo, items);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addShadow();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function stay():void
        {
            super._state = TemplateState.STAY;
            update();
            setUsualFace();
        }

        public function move():void
        {
            super._state = TemplateState.DASH_FORWARD;
            update();
            setAttackFace();
        }

        public function backMove():void
        {
            super._state = TemplateState.DASH_BACKWARD;
            update();
        }

        public function jump():void
        {
            var jumpHelper:MultiStateHelper = new MultiStateHelper(this, [TemplateState.JUMP_INIT, TemplateState.JUMP_AIR, TemplateState.JUMP_LANDING]);
            jumpHelper.addEventListener(Event.COMPLETE, _multiAnimationComplete);
            jumpHelper.execute();
            setAttackFace();
        }

        public function evade():void
        {
            var evadeHelper:MultiStateHelper = new MultiStateHelper(this, [TemplateState.EVADE_INIT, TemplateState.EVADE, TemplateState.EVADE_END]);
            evadeHelper.addEventListener(Event.COMPLETE, _multiAnimationComplete);
            evadeHelper.execute();
        }

        public function setUsualFace():void
        {
            super._setEyes(super.userInfo.eyes.value);
            super._setMouth(super.userInfo.mouth.value);
        }

        public function setAttackFace():void
        {
            var eyes:String = ATACK_PREFIX + super.userInfo.eyes.value;
            var mouth:String = ATACK_PREFIX + super.userInfo.mouth.value;
            super._setEyes(eyes);
            super._setMouth(mouth);
        }

        public function setDefenseFace():void
        {
            var eyes:String = DEFENSE_PREFIX + super.userInfo.eyes.value;
            var mouth:String = DEFENSE_PREFIX + super.userInfo.mouth.value;
            super._setEyes(eyes);
            super._setMouth(mouth);
        }

        public function setKOFace():void
        {
            var eyes:String = KO_PREFIX + super.userInfo.eyes.value;
            var mouth:String = KO_PREFIX + super.userInfo.mouth.value;
            super._setEyes(eyes);
            super._setMouth(mouth);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addShadow():void
        {
            var shadow:MovieClip = super.stash.getChar(SHADOW);
            addChild(shadow);
            swapChildren(super.template, shadow);
            shadow.y = 90;
            if (TemplateType.SHORT)
                shadow.x = 5;
        }

        private function _replaceEyes(eyes:String):void
        {

        }

        private function _replaceMouth(mouth:String):void
        {

        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _multiAnimationComplete(event:Event):void
        {
            var animationHelper:MultiStateHelper = event.currentTarget as MultiStateHelper;
            animationHelper.removeEventListener(Event.COMPLETE, _multiAnimationComplete);
            animationHelper.destroy();
            stay();
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
