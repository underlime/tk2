/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.07.13
 * Time: 12:04
 */
package application.views.constructor.builders
{
    import application.models.market.Item;
    import application.models.user.UserInfo;
    import application.service.battle.common.GrenadeType;
    import application.views.constructor.common.Details;
    import application.views.constructor.common.TemplateState;
    import application.views.constructor.helpers.AnimationHelper;
    import application.views.constructor.helpers.CharHelper;

    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.helpers.MathHelper;

    public class BattleUnit extends MovingUnit
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleUnit(userInfo:UserInfo, items:Vector.<Item>)
        {
            super(userInfo, items);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            super.dress();
        }

        override public function update():void
        {
            super.update();
            super.dress();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var isEnemy:Boolean = false;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function toDefaultState():void
        {
            super.bruise = false;
            if (super.wound)
                super.wound.gotoAndStop(1);
            _woundLevel = 1;
            _defaultHealth = 0;
            _health = 0;
        }

        public function kick(index:int):void
        {
            super._state = TemplateState.getKick(index);
            update();
            super.setAttackFace();
            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function damage(index:int):void
        {
            super._state = TemplateState.getDamage(index);
            update();
            super.setDefenseFace();
            _setBruise();

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function block():void
        {
            super._state = TemplateState.BLOCK;
            update();
            super.setDefenseFace();
            _setBruise();

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function ko():void
        {
            super._state = TemplateState.KO;
            update();
            super.setKOFace();
        }

        public function win():void
        {
            super._state = TemplateState.WIN;
            update();
        }

        public function cast():void
        {
            super._state = TemplateState.CAST;
            update();

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function doubleKick():void
        {
            super._state = TemplateState.DOUBLE_KICK;
            update();
            super.setAttackFace();

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function multiDamage():void
        {
            super._state = TemplateState.MULTI_DAMAGE;
            update();
            super.setDefenseFace();
            _setBruise();
        }

        public function rageStrikes():void
        {
            super._state = TemplateState.RAGE_STRIKES;
            update();
            super.setAttackFace();

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function forceBlow():void
        {
            super._state = TemplateState.FORCE_BLOW;
            update();
            super.setAttackFace();

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function telekinesis():void
        {
            super._state = TemplateState.TELEKINESIS;
            update();

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function useItem(itemSource:String):void
        {
            var item:MovieClip = super.stash.getChar(itemSource);
            super._state = TemplateState.USE_ITEM;
            update();

            if (_getItemCont()) {
                var cont:DisplayObjectContainer = _getItemCont() as DisplayObjectContainer;
                CharHelper.clear(cont);
                cont.addChild(item);
            }

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function throwGrenade(grenadeSource:String):void
        {
            var item:MovieClip = super.stash.getChar(grenadeSource);

            super._state = TemplateState.THROW_ITEM;
            update();

            if (_getThrowItemCont()) {
                var cont:DisplayObjectContainer = _getThrowItemCont() as DisplayObjectContainer;
                CharHelper.clear(cont);
                cont.addChild(item);
            }

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        public function grenadeDamage(grenade:GrenadeType):void
        {
            switch (grenade) {
                case GrenadeType.FIRE:
                    super._state = TemplateState.FIRE_DAMAGE;
                    break;
                case GrenadeType.ICE:
                    super._state = TemplateState.ICE_DAMAGE;
                    break;
                case GrenadeType.FLASH:
                    super._state = TemplateState.FLASH_DAMAGE;
                    break;
                default:
                    super._state = TemplateState.ICE_DAMAGE;
                    break;
            }
            update();

            var helper:AnimationHelper = new AnimationHelper(super.template);
            helper.addEventListener(Event.COMPLETE, _completeHandler);
            helper.execute();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _sector:int = 1;
        private var _health:int;

        private var _defaultHealth:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setBruise():void
        {
            if (!super.bruise) {
                if (MathHelper.chance(10)) {
                    trace("установить синяк");
                    super.bruise = true;
                }
            }
        }

        private function _updateWounds():void
        {
            _woundLevel = int(10 - (_health / _defaultHealth) * 10) + 1;
            _woundLevel = MathHelper.clamp(_woundLevel, 1, 10);

            if (super.wound)
                super.wound.gotoAndStop(_woundLevel);
        }

        private function _getItemCont():DisplayObject
        {
            return super.template.getChildByName(Details.ITEM);
        }

        private function _getThrowItemCont():DisplayObject
        {
            return super.template.getChildByName(Details.THROW_ITEM);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeHandler(event:Event):void
        {
            var helper:AnimationHelper = event.currentTarget as AnimationHelper;
            helper.removeEventListener(Event.COMPLETE, _completeHandler);
            stay();
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get sector():int
        {
            return _sector;
        }

        public function set sector(value:int):void
        {
            _sector = value;
        }

        public function get health():int
        {
            return _health;
        }

        public function set health(value:int):void
        {
            _health = value;
            _updateWounds();
        }

        public function get defaultHealth():int
        {
            return _defaultHealth;
        }

        public function set defaultHealth(value:int):void
        {
            _defaultHealth = value;
            _health = _defaultHealth;
        }

        public function get isDarkBot():Boolean
        {
            return userInfo.user_id.value == -2;
        }

    }
}
