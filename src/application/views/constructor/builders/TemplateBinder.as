/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.06.13
 * Time: 15:27
 */
package application.views.constructor.builders
{
    import application.models.market.Item;
    import application.models.user.UserInfo;
    import application.models.user.Vegetable;
    import application.views.constructor.common.Details;
    import application.views.constructor.common.TemplateState;
    import application.views.constructor.common.TemplateType;
    import application.views.constructor.common.WeaponTemplate;
    import application.views.constructor.helpers.CharHelper;
    import application.views.constructor.helpers.TemplateHelper;
    import application.views.constructor.helpers.WeaponTemplateHelper;
    import application.views.constructor.stash.TemplateStash;

    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;

    import framework.core.struct.view.View;

    public class TemplateBinder extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const VERTICAL_MARGIN:int = -70;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TemplateBinder(userInfo:UserInfo, items:Vector.<Item>)
        {
            _userInfo = userInfo;
            _items = items;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            super.$disableInteractive();
        }

        override public function destroy():void
        {
            _stash.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function clear():void
        {
            _clearContainers();
            _clearTemplate();
        }

        public function update():void
        {
            _updateTemplate();
            _template.gotoAndStop(1);
            _bindCharContainers();
            _bindItemContainers();
            _template.play();
            _clearContainers();
        }

        public function construct():void
        {
            _updateData();
            _updateTemplate();
            _template.gotoAndStop(1);
            _bindCharContainers();
            _bindItemContainers();
            _template.play();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _templateType:TemplateType = TemplateType.SHORT;
        protected var _weaponTemplate:WeaponTemplate = WeaponTemplate.WITHOUT_WEAPON;
        protected var _state:TemplateState = TemplateState.STAY;

        protected var _template:MovieClip;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _updateTemplateMovie():void
        {
            _template = _stash.getChar(_templateType.toString() + _weaponTemplate.toString() + _state.toString());
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _stash:TemplateStash = new TemplateStash();

        private var _bodyContainer:DisplayObjectContainer;
        private var _body:DisplayObject;

        private var _faceContainer:DisplayObjectContainer;
        private var _eyes:DisplayObject;
        private var _mouth:DisplayObject;

        private var _lHand:MovieClip;
        private var _lHandContainer:DisplayObjectContainer;

        private var _rHand:MovieClip;
        private var _rHandContainer:DisplayObjectContainer;

        private var _hair:MovieClip;
        private var _hairContainer:DisplayObjectContainer;

        private var _hematoma:MovieClip;
        private var _hematomaContainer:DisplayObjectContainer;

        private var _wound:MovieClip;
        private var _woundContainer:DisplayObjectContainer;

        private var _hatContainer:DisplayObjectContainer;
        private var _glassesContainer:DisplayObjectContainer;
        private var _maskContainer:DisplayObjectContainer;
        private var _lGloveContainer:DisplayObjectContainer;
        private var _rGloveContainer:DisplayObjectContainer;
        private var _coatContainer:DisplayObjectContainer;
        private var _weaponContainer:DisplayObjectContainer;
        private var _shieldContainer:DisplayObjectContainer;

        private var _hat:MovieClip;
        private var _glasses:MovieClip;
        private var _faceMask:MovieClip;
        private var _lGlove:MovieClip;
        private var _rGlove:MovieClip;
        private var _coat:MovieClip;
        private var _weapon:MovieClip;
        private var _shield:MovieClip;

        private var _items:Vector.<Item>;
        private var _userInfo:UserInfo;

        private var _vegetable:Vegetable;

        private var _bruise:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateData():void
        {
            _vegetable = Vegetable.getEnumByKey(_userInfo.vegetable.value);
            _weaponTemplate = WeaponTemplateHelper.getTemplate(_items);
            _templateType = TemplateHelper.getTemplateByVegetable(_vegetable);
        }

        private function _updateTemplate():void
        {
            this.clear();
            _updateTemplateMovie();
            addChild(_template);

            _template.y = _templateType == TemplateType.LONG ? VERTICAL_MARGIN : 0;
        }

        private function _bindCharContainers():void
        {
            _bodyContainer = _template.getChildByName(Details.BODY_CONTAINER) as DisplayObjectContainer;
            _body = _bodyContainer.getChildByName(Details.BODY) as MovieClip;

            _lHandContainer = _template.getChildByName(Details.LEFT_HAND_CONTAINER) as DisplayObjectContainer;
            _lHand = _lHandContainer.getChildByName(Details.LEFT_HAND) as MovieClip;

            _rHandContainer = _template.getChildByName(Details.RIGHT_HAND_CONTAINER) as DisplayObjectContainer;
            _rHand = _rHandContainer.getChildByName(Details.RIGHT_HAND) as MovieClip;

            _hairContainer = _template.getChildByName(Details.HAIR_CONTAINER) as DisplayObjectContainer;
            _hair = _hairContainer.getChildByName(Details.HAIR) as MovieClip;

            _faceContainer = _template.getChildByName(Details.FACE) as DisplayObjectContainer;

            _eyes = _faceContainer.getChildByName(Details.EYES) as MovieClip;
            _mouth = _faceContainer.getChildByName(Details.MOUTH) as MovieClip;

            _hematomaContainer = _faceContainer.getChildByName(Details.HEMATOMA_CONTAINER) as DisplayObjectContainer;
            _hematoma = _hematomaContainer.getChildByName(Details.HEMATOMA) as MovieClip;

            _woundContainer = _bodyContainer.getChildByName(Details.WOUND_CONTAINER) as DisplayObjectContainer;
            _wound = _woundContainer.getChildByName(Details.WOUND) as MovieClip;
        }

        private function _setBruise():void
        {
            if (_hematomaContainer) {
                CharHelper.clear(_hematomaContainer);
                _hematoma = stash.getChar(Details.HEMATOMA);
                _hematomaContainer.addChild(_hematoma);
            }
        }

        private function _removeBruise():void
        {
            if (_hematomaContainer)
                CharHelper.clear(_hematomaContainer);
        }

        protected function _bindItemContainers():void
        {
            _hatContainer = _template.getChildByName(Details.HAT_CONTAINER) as DisplayObjectContainer;
            _hat = _hatContainer.getChildByName(Details.HAT) as MovieClip;

            _glassesContainer = _faceContainer.getChildByName(Details.GLASSES_CONTAINER) as DisplayObjectContainer;
            _glasses = _glassesContainer.getChildByName(Details.GLASSES) as MovieClip;

            _maskContainer = _faceContainer.getChildByName(Details.MASK_CONTAINER) as DisplayObjectContainer;
            _faceMask = _maskContainer.getChildByName(Details.MASK) as MovieClip;

            _coatContainer = _template.getChildByName(Details.COAT_CONTAINER) as DisplayObjectContainer;
            _coat = _coatContainer.getChildByName(Details.COAT) as MovieClip;

            if (_weaponTemplate == WeaponTemplate.TWO_HANDED_WEAPON) {
                _weaponContainer = _template.getChildByName(Details.HEAVY_WEAPON_CONTAINER) as DisplayObjectContainer;
                _weapon = _weaponContainer.getChildByName(Details.HEAVY_WEAPON) as MovieClip;
            }
            else
                if (_weaponTemplate == WeaponTemplate.WITHOUT_WEAPON) {
                    _lGloveContainer = _lHandContainer;
                    _lGlove = _lGloveContainer.getChildByName(Details.LEFT_GLOVE) as MovieClip;

                    _rGloveContainer = _rHandContainer;
                    _rGlove = _rGloveContainer.getChildByName(Details.RIGHT_GLOVE) as MovieClip;
                }
                else {
                    _weaponContainer = this._template.getChildByName(Details.WEAPON_CONTAINER) as DisplayObjectContainer;
                    _weapon = _weaponContainer.getChildByName(Details.WEAPON) as MovieClip;
                }

            if (_weaponTemplate != WeaponTemplate.TWO_HANDED_WEAPON) {
                _shieldContainer = this._template.getChildByName(Details.SHIELD_CONTAINER) as DisplayObjectContainer;
                if (!_shieldContainer)
                    trace(_template);
                _shield = _shieldContainer.getChildByName(Details.SHIELD) as MovieClip;
            }

            if (_bruise)
                _setBruise();
        }

        private function _clearContainers():void
        {
            CharHelper.clear(_hatContainer);
            CharHelper.clear(_glassesContainer);
            CharHelper.clear(_maskContainer);
            CharHelper.clear(_coatContainer);
            CharHelper.clear(_lGloveContainer);
            CharHelper.clear(_rGloveContainer);
            CharHelper.clear(_shieldContainer);
            CharHelper.clear(_weaponContainer);
        }

        private function _clearTemplate():void
        {
            if (_template && this.contains(_template))
                removeChild(_template);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get stash():TemplateStash
        {
            return _stash;
        }

        public function get bodyContainer():DisplayObjectContainer
        {
            return _bodyContainer;
        }

        public function get faceContainer():DisplayObjectContainer
        {
            return _faceContainer;
        }

        public function get lHandContainer():DisplayObjectContainer
        {
            return _lHandContainer;
        }

        public function get rHandContainer():DisplayObjectContainer
        {
            return _rHandContainer;
        }

        public function get hematomaContainer():DisplayObjectContainer
        {
            return _hematomaContainer;
        }

        public function get woundContainer():DisplayObjectContainer
        {
            return _woundContainer;
        }

        public function get state():TemplateState
        {
            return _state;
        }

        public function get body():DisplayObject
        {
            return _body;
        }

        public function get eyes():DisplayObject
        {
            return _eyes;
        }

        public function get mouth():DisplayObject
        {
            return _mouth;
        }

        public function get lHand():MovieClip
        {
            return _lHand;
        }

        public function get rHand():MovieClip
        {
            return _rHand;
        }

        public function get hair():MovieClip
        {
            return _hair;
        }

        public function get hematoma():MovieClip
        {
            return _hematoma;
        }

        public function set body(value:DisplayObject):void
        {
            _body = value;
        }

        public function set eyes(value:DisplayObject):void
        {
            _eyes = value;
        }

        public function set mouth(value:DisplayObject):void
        {
            _mouth = value;
        }

        public function set lHand(value:MovieClip):void
        {
            _lHand = value;
        }

        public function set rHand(value:MovieClip):void
        {
            _rHand = value;
        }

        public function set hair(value:MovieClip):void
        {
            _hair = value;
        }

        public function set hematoma(value:MovieClip):void
        {
            _hematoma = value;
        }

        public function get userInfo():UserInfo
        {
            return _userInfo;
        }

        public function get items():Vector.<Item>
        {
            return _items;
        }

        public function get wound():MovieClip
        {
            return _wound;
        }

        public function set wound(value:MovieClip):void
        {
            _wound = value;
        }

        public function get hairContainer():DisplayObjectContainer
        {
            return _hairContainer;
        }

        public function get hatContainer():DisplayObjectContainer
        {
            return _hatContainer;
        }

        public function get glassesContainer():DisplayObjectContainer
        {
            return _glassesContainer;
        }

        public function get maskContainer():DisplayObjectContainer
        {
            return _maskContainer;
        }

        public function get lGloveContainer():DisplayObjectContainer
        {
            return _lGloveContainer;
        }

        public function get rGloveContainer():DisplayObjectContainer
        {
            return _rGloveContainer;
        }

        public function get coatContainer():DisplayObjectContainer
        {
            return _coatContainer;
        }

        public function get weaponContainer():DisplayObjectContainer
        {
            return _weaponContainer;
        }

        public function get shieldContainer():DisplayObjectContainer
        {
            return _shieldContainer;
        }

        public function get hat():MovieClip
        {
            return _hat;
        }

        public function set hat(value:MovieClip):void
        {
            _hat = value;
        }

        public function get glasses():MovieClip
        {
            return _glasses;
        }

        public function set glasses(value:MovieClip):void
        {
            _glasses = value;
        }


        public function get lGlove():MovieClip
        {
            return _lGlove;
        }

        public function set lGlove(value:MovieClip):void
        {
            _lGlove = value;
        }

        public function get rGlove():MovieClip
        {
            return _rGlove;
        }

        public function set rGlove(value:MovieClip):void
        {
            _rGlove = value;
        }

        public function get coat():MovieClip
        {
            return _coat;
        }

        public function set coat(value:MovieClip):void
        {
            _coat = value;
        }

        public function get weapon():MovieClip
        {
            return _weapon;
        }

        public function set weapon(value:MovieClip):void
        {
            _weapon = value;
        }

        public function get shield():MovieClip
        {
            return _shield;
        }

        public function set shield(value:MovieClip):void
        {
            _shield = value;
        }

        public function get vegetable():Vegetable
        {
            return _vegetable;
        }

        public function get templateType():TemplateType
        {
            return _templateType;
        }

        public function get weaponTemplate():WeaponTemplate
        {
            return _weaponTemplate;
        }

        public function get faceMask():MovieClip
        {
            return _faceMask;
        }

        public function set faceMask(value:MovieClip):void
        {
            _faceMask = value;
        }

        public function set items(value:Vector.<Item>):void
        {
            _items = value;
            this.clear();
            this.construct();
        }

        public function get template():MovieClip
        {
            return _template;
        }

        public function set state(value:TemplateState):void
        {
            _state = value;
        }

        public function get bruise():Boolean
        {
            return _bruise;
        }

        public function set bruise(value:Boolean):void
        {
            _bruise = value;
            if (_bruise) {
                _setBruise();
            } else {
                _removeBruise();
            }
        }
    }
}
