/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.06.13
 * Time: 15:15
 */
package application.views.constructor.builders
{
    import application.models.chars.CharDetail;
    import application.models.chars.CharGroup;
    import application.models.market.Item;
    import application.models.user.UserInfo;

    public class CharsChanger extends CharsBuilder
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CharsChanger(userInfo:UserInfo, items:Vector.<Item>)
        {
            super(userInfo, items);
            _setupMethodsHash();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            super.update();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function change(char:CharDetail):void
        {
            if (char.group.value == CharGroup.BODIES.toString()) {
                replaceBody(char.mc_1_name.value, char.mc_2_name.value);
            } else {
                var method:Function = _methodsHash[char.group.value];
                method.call(this, char.mc_1_name.value);
            }
        }

        public function replaceBody(body:String, hands:String):void
        {
            super._setBody(body);
            super._setHands(hands);
        }

        public function replaceHair(hair:String):void
        {
            super._setHair(hair);
        }

        public function replaceEyes(eyes:String):void
        {
            super._setEyes(eyes);
        }

        public function replaceMouth(mouth:String):void
        {
            super._setMouth(mouth);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _methodsHash:Object = {};

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupMethodsHash():void
        {
            _methodsHash[CharGroup.EYES.toString()] = replaceEyes;
            _methodsHash[CharGroup.HAIRS.toString()] = replaceHair;
            _methodsHash[CharGroup.MOUTHS.toString()] = replaceMouth;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
