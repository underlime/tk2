/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.06.13
 * Time: 10:23
 */
package application.views.constructor.builders
{
    import application.common.ArrangementVariant;
    import application.models.market.Item;
    import application.models.user.UserInfo;
    import application.views.constructor.common.WeaponTemplate;
    import application.views.constructor.helpers.CharHelper;

    public class ClothesBuilder extends CharsBuilder
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const LEFT_SIDE:String = "L";
        public static const RIGHT_SIDE:String = "R";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClothesBuilder(userInfo:UserInfo, items:Vector.<Item>)
        {
            _setupMethodsHash();
            super(userInfo, items);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function dress():void
        {
            var count:int = super.items.length;
            var item:Item;
            var arrangement:String;

            for (var i:int = 0; i < count; i++) {
                item = super.items[i];
                arrangement = item.arrangement_variant.value;

                if (_methodsHash[arrangement]) {
                    var method:Function = _methodsHash[arrangement];
                    method.call(this, item.picture.value);
                }

            }
        }

        public function undress():void
        {
            _unsetCoat();
            _unsetGlasses();
            _unsetGloves();
            _unsetHat();
            _unsetMask();
            _unsetShield();
            _unsetWeapon();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setHat(hat:String):void
        {
            _unsetHat();
            _unsetHair();

            super.hat = super.stash.getChar(hat);
            super.hatContainer.addChild(super.hat);
        }

        protected function _unsetHat():void
        {
            if (!super.hatContainer) {
                throw new Error("HatContainer is not detected! State: " + super.state.toString());
            }

            if (super.hat) {
                CharHelper.remove(super.hat);
            }

            super._setHair(super.userInfo.hair.value);
        }

        protected function _setGlasses(glasses:String):void
        {
            _unsetGlasses();
            _unsetMask();

            super.glasses = super.stash.getChar(glasses);
            super.glassesContainer.addChild(super.glasses);
        }

        protected function _unsetGlasses():void
        {
            if (!super.glassesContainer)
                throw new Error("GlassesContainer is not detected! State: " + super.state.toString());

            if (super.glasses)
                CharHelper.remove(super.glasses);
        }

        protected function _setMask(mask:String):void
        {
            _unsetMask();
            _unsetGlasses();

            super.faceMask = super.stash.getChar(mask);
            super.maskContainer.addChild(super.faceMask);
        }

        protected function _unsetMask():void
        {
            if (!super.maskContainer)
                throw new Error("MaskContainer is not detected! State: " + super.state.toString());

            if (super.faceMask)
                CharHelper.remove(super.faceMask);
        }

        protected function _setGloves(gloves:String):void
        {
            if (super.weaponTemplate != WeaponTemplate.WITHOUT_WEAPON)
                return;

            _unsetGloves();

            super.lGlove = super.stash.getChar(LEFT_SIDE + gloves);
            super.lGloveContainer.addChild(super.lGlove);

            super.rGlove = super.stash.getChar(RIGHT_SIDE + gloves);
            super.rGloveContainer.addChild(super.rGlove);
        }

        protected function _unsetGloves():void
        {
            if (super.weaponTemplate != WeaponTemplate.WITHOUT_WEAPON)
                return;

            if (!super.lGloveContainer)
                throw new Error("lGloveContainer is not detected! State: " + super.state.toString());
            if (super.lGlove)
                CharHelper.remove(super.lGlove);

            if (!super.rGloveContainer)
                throw new Error("rGloveContainer is not detected! State: " + super.state.toString());
            if (super.rGlove)
                CharHelper.remove(super.rGlove);
        }

        protected function _setCoat(coat:String):void
        {
            _unsetCoat();
            super.coat = super.stash.getChar(coat);
            super.coatContainer.addChild(super.coat);
        }

        protected function _unsetCoat():void
        {
            if (!super.coatContainer)
                throw new Error("CoatContainer is not detected! State: " + super.state.toString());
            if (super.coat)
                CharHelper.remove(super.coat);
        }

        protected function _setShield(shield:String):void
        {
            _unsetShield();
            super.shield = super.stash.getChar(shield);
            super.shieldContainer.addChild(super.shield);
        }

        protected function _unsetShield():void
        {
            if (!super.shieldContainer && super._weaponTemplate != WeaponTemplate.TWO_HANDED_WEAPON)
                throw new Error("Shield Container is not detected! State: " + super.state.toString());

            if (super.shield)
                CharHelper.remove(super.shield);

        }

        protected function _setWeapon(weapon:String):void
        {
            if (super.weaponTemplate == WeaponTemplate.WITHOUT_WEAPON)
                return;

            _unsetWeapon();
            super.weapon = super.stash.getChar(weapon);
            super.weaponContainer.addChild(super.weapon);
        }

        protected function _unsetWeapon():void
        {
            if (super.weaponTemplate == WeaponTemplate.WITHOUT_WEAPON)
                return;

            if (!super.weaponContainer)
                throw new Error("Weapon Container is not detected! State: " + super.state.toString());
            if (super.weapon)
                CharHelper.remove(super.weapon);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _methodsHash:Object;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupMethodsHash():void
        {
            _methodsHash = {};
            _methodsHash[ArrangementVariant.COAT.toString()] = _setCoat;
            _methodsHash[ArrangementVariant.GLASSES.toString()] = _setGlasses;
            _methodsHash[ArrangementVariant.GLOVES.toString()] = _setGloves;
            _methodsHash[ArrangementVariant.HAT.toString()] = _setHat;
            _methodsHash[ArrangementVariant.MASK.toString()] = _setMask;
            _methodsHash[ArrangementVariant.ONE_HANDED.toString()] = _setWeapon;
            _methodsHash[ArrangementVariant.ROD_WEAPON.toString()] = _setWeapon;
            _methodsHash[ArrangementVariant.TWO_HANDED.toString()] = _setWeapon;
            _methodsHash[ArrangementVariant.SHIELD.toString()] = _setShield;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
