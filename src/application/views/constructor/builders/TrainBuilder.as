/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 08.09.13
 * Time: 9:35
 */
package application.views.constructor.builders
{
    import application.common.SkillType;
    import application.models.inventory.InventoryHelper;
    import application.models.user.UserInfo;
    import application.views.constructor.common.TemplateState;
    import application.views.constructor.helpers.AnimationHelper;

    import flash.events.Event;

    public class TrainBuilder extends CharsBuilder
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrainBuilder(userInfo:UserInfo, skillType:SkillType)
        {
            super(userInfo, InventoryHelper.ItemsArray2Vector([]));
            _skillType = skillType;
            _state = TemplateState.TRAIN_STAY;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            stay();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function stay():void
        {
            super._state = TemplateState.TRAIN_STAY;
            update();
        }

        public function showSuccess():void
        {
            if (!_blocked) {
                _blocked = true;
                super._state = TemplateState.TRAIN_WIN;
                update();

                var helper:AnimationHelper = new AnimationHelper(super.template);
                helper.addEventListener(Event.COMPLETE, _completeHandler);
                helper.execute();
            }
        }

        public function showFail():void
        {
            if (!_blocked) {
                _blocked = true;
                super._state = TemplateState.TRAIN_FAIL;
                update();

                var helper:AnimationHelper = new AnimationHelper(super.template);
                helper.addEventListener(Event.COMPLETE, _completeHandler);
                helper.execute();
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _updateTemplateMovie():void
        {
            _template = super.stash.getChar(_templateType.toString() + _skillType.toString() + _state.toString());
        }

        override protected function _bindItemContainers():void
        {

        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _skillType:SkillType;
        private var _blocked:Boolean = false;


        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeHandler(event:Event):void
        {
            var helper:AnimationHelper = event.currentTarget as AnimationHelper;
            helper.removeEventListener(Event.COMPLETE, _completeHandler);
            stay();
            _blocked = false;
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
