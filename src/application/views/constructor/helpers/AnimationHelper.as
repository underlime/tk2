/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.07.13
 * Time: 11:26
 */
package application.views.constructor.helpers
{
    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.struct.service.Service;

    public class AnimationHelper extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AnimationHelper(movie:MovieClip)
        {
            super();
            _movie = movie;
            _movie.gotoAndStop(1);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _movie.addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _movie.play();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _movie:MovieClip;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _enterFrameHandler(event:Event):void
        {
            if (_movie.currentFrame == _movie.totalFrames) {
                _movie.removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
                super.dispatchEvent(new Event(Event.COMPLETE));
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get movie():MovieClip
        {
            return _movie;
        }
    }
}
