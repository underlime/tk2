/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.06.13
 * Time: 10:31
 */
package application.views.constructor.helpers
{
    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;

    public class CharHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function remove(char:DisplayObject):void
        {
            var parent:DisplayObjectContainer = char.parent;
            if (parent && parent.contains(char))
                parent.removeChild(char);
        }

        public static function clear(container:DisplayObjectContainer):void
        {
            if (container) {
                for (var i:int = 0; i < container.numChildren; i++) {
                    var object:DisplayObject = container.getChildAt(i);
                    container.removeChild(object);
                }
            }
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
