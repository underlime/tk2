/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.09.13
 * Time: 16:17
 */
package application.views.constructor.helpers
{
    import application.common.ArrangementVariant;
    import application.models.inventory.InventoryItem;

    public class DressHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const WEAPON_ARRANGEMENT:Array = [
            ArrangementVariant.GLOVES.toString(),
            ArrangementVariant.ONE_HANDED.toString(),
            ArrangementVariant.ROD_WEAPON.toString(),
            ArrangementVariant.TWO_HANDED.toString()
        ];

        public static const FACE_ARRANGEMENT:Array = [
            ArrangementVariant.MASK.toString(),
            ArrangementVariant.GLASSES.toString()
        ];

        public static const COAT_ARRANGEMENT:Array = [
            ArrangementVariant.COAT.toString()
        ];

        public static const HAT_ARRANGEMENT:Array = [
            ArrangementVariant.HAT.toString()
        ];

        public static const SHIELD_ARRANGEMENT:Array = [
            ArrangementVariant.SHIELD.toString()
        ];

        private static var _putItems:Array = [];
        private static var _socialPutItems:Array = [];

        // CLASS METHODS -----------------------------------------------------------------------/

        /**
         * Возвращает список предметов для построения персонажа
         * @param putItems
         * @param socialPutItems
         * @return
         */
        public static function getBuildItems(putItems:Array, socialPutItems:Array):Array
        {
            _putItems = putItems;
            _socialPutItems = socialPutItems;

            var buildItems:Array = [];

            var weapon:InventoryItem = _getBuildItem(WEAPON_ARRANGEMENT);
            var face:InventoryItem = _getBuildItem(FACE_ARRANGEMENT);
            var coat:InventoryItem = _getBuildItem(COAT_ARRANGEMENT);
            var hat:InventoryItem = _getBuildItem(HAT_ARRANGEMENT);
            var shield:InventoryItem = _getBuildItem(SHIELD_ARRANGEMENT);

            if (weapon)
                buildItems.push(weapon);
            if (face)
                buildItems.push(face);
            if (coat)
                buildItems.push(coat);
            if (hat)
                buildItems.push(hat);
            if (shield)
                buildItems.push(shield);

            _putItems = [];
            _socialPutItems = [];

            return buildItems;
        }

        private static function _getItem(items:Array, arrangements:Array):InventoryItem
        {
            var count:int = items.length;

            for (var i:int = 0; i < count; i++) {
                var item:InventoryItem = items[i] as InventoryItem;
                var av:String = item.arrangement_variant.value;

                if (arrangements.indexOf(av) > -1)
                    return item;
            }

            return null;
        }

        private static function _getBuildItem(arrangements:Array):InventoryItem
        {
            var putItem:InventoryItem = _getItem(_putItems, arrangements);
            var socialItem:InventoryItem = _getItem(_socialPutItems, arrangements);

            if (socialItem)
                return socialItem;
            if (putItem)
                return putItem;

            return null;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
