/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.07.13
 * Time: 12:03
 */
package application.views.constructor.helpers
{
    import application.views.constructor.builders.MovingUnit;
    import application.views.constructor.common.TemplateState;

    import flash.events.Event;

    import framework.core.struct.service.Service;

    public class MultiStateHelper extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MultiStateHelper(unit:MovingUnit, states:Array)
        {
            _unit = unit;
            _states = states;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _doNextState();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _states:Array;
        private var _stateIndex:int = 0;
        private var _unit:MovingUnit;

        private var _animationHelper:AnimationHelper;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _doNextState():void
        {
            var state:TemplateState = _states[_stateIndex];

            _unit.state = state;
            _unit.update();
            _animationHelper = new AnimationHelper(_unit.template);
            _animationHelper.addEventListener(Event.COMPLETE, _completeHandler);
            _animationHelper.execute();
        }

        private function _completeAnimation():void
        {
            _stateIndex++;
            if (_stateIndex < _states.length)
                _doNextState();
            else
                _completeAnimations();
        }

        private function _completeAnimations():void
        {
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeHandler(event:Event):void
        {
            _animationHelper.removeEventListener(Event.COMPLETE, _completeHandler);
            _animationHelper.destroy();
            _completeAnimation();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
