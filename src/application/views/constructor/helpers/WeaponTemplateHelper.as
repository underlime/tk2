/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.06.13
 * Time: 15:57
 */
package application.views.constructor.helpers
{
    import application.common.ArrangementVariant;
    import application.models.market.Item;
    import application.views.constructor.common.WeaponTemplate;

    public class WeaponTemplateHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getTemplate(items:Vector.<Item>):WeaponTemplate
        {
            var count:int = items.length;
            var template:WeaponTemplate = WeaponTemplate.WITHOUT_WEAPON;
            for (var i:int = 0; i < count; i++) {
                if (items[i].arrangement_variant.value == ArrangementVariant.ONE_HANDED.toString())
                    template = WeaponTemplate.ONE_HANDED_WEAPON;
                if (items[i].arrangement_variant.value == ArrangementVariant.ROD_WEAPON.toString())
                    template = WeaponTemplate.ROD_WEAPON;
                if (items[i].arrangement_variant.value == ArrangementVariant.TWO_HANDED.toString())
                    template = WeaponTemplate.TWO_HANDED_WEAPON;
            }

            return template;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
