/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.06.13
 * Time: 15:59
 */
package application.views.constructor.common
{
    import framework.core.enum.BaseEnum;

    public class WeaponTemplate extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const WITHOUT_WEAPON:WeaponTemplate = new WeaponTemplate("");
        public static const ONE_HANDED_WEAPON:WeaponTemplate = new WeaponTemplate("OH");
        public static const TWO_HANDED_WEAPON:WeaponTemplate = new WeaponTemplate("TH");
        public static const ROD_WEAPON:WeaponTemplate = new WeaponTemplate("RW");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function WeaponTemplate(num:String)
        {
            super(num);
            if (_lockUp)
                throw new Error("Enum has already inited");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.views.constructor.common.WeaponTemplate;

WeaponTemplate.lockUp();