/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.06.13
 * Time: 16:13
 */
package application.views.constructor.common
{
    import framework.core.enum.BaseEnum;

    public class TemplateState extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const STAY:TemplateState = new TemplateState("Stay");
        public static const DASH_FORWARD:TemplateState = new TemplateState("DashForward");
        public static const DASH_BACKWARD:TemplateState = new TemplateState("DashBackward");

        public static const JUMP_INIT:TemplateState = new TemplateState("JumpInit");
        public static const JUMP_AIR:TemplateState = new TemplateState("JumpAir");
        public static const JUMP_LANDING:TemplateState = new TemplateState("JumpLanding");

        public static const KICK_1:TemplateState = new TemplateState("NormalAtack_1");
        public static const KICK_2:TemplateState = new TemplateState("NormalAtack_2");
        public static const KICK_3:TemplateState = new TemplateState("NormalAtack_3");
        public static const KICK_4:TemplateState = new TemplateState("NormalAtack_4");

        public static const DAMAGE_1:TemplateState = new TemplateState("Damage_1");
        public static const DAMAGE_2:TemplateState = new TemplateState("Damage_2");
        public static const DAMAGE_3:TemplateState = new TemplateState("Damage_3");
        public static const DAMAGE_4:TemplateState = new TemplateState("Damage_4");

        public static const MULTI_DAMAGE:TemplateState = new TemplateState("Damage_5_multi");

        public static const DAMAGE_BACKWARD:TemplateState = new TemplateState("Damage_6_flight");

        public static const BLOCK:TemplateState = new TemplateState("Block");

        public static const EVADE_INIT:TemplateState = new TemplateState("EvadeInit");
        public static const EVADE:TemplateState = new TemplateState("Evade");
        public static const EVADE_END:TemplateState = new TemplateState("EvadeEnd");

        public static const KICK_VARIANTS:Array = [KICK_1, KICK_2, KICK_3, KICK_4];
        public static const DAMAGE_VARIANTS:Array = [DAMAGE_1, DAMAGE_2, DAMAGE_3, DAMAGE_4];

        public static const KO:TemplateState = new TemplateState("KO");
        public static const WIN:TemplateState = new TemplateState("Win_1");
        public static const CAST:TemplateState = new TemplateState("Cast");

        public static const DOUBLE_KICK:TemplateState = new TemplateState("_DoubleStrike");
        public static const RAGE_STRIKES:TemplateState = new TemplateState("s01_RageStrikes");
        public static const FORCE_BLOW:TemplateState = new TemplateState("_ForceBlow");
        public static const TELEKINESIS:TemplateState = new TemplateState("_Telekinez");

        public static const USE_ITEM:TemplateState = new TemplateState("ItemUseDrink");
        public static const THROW_ITEM:TemplateState = new TemplateState("ItemThrow");
        public static const ICE_DAMAGE:TemplateState = new TemplateState("DamageIce");
        public static const FIRE_DAMAGE:TemplateState = new TemplateState("DamageFire");
        public static const FLASH_DAMAGE:TemplateState = new TemplateState("DamageLightning");

        public static const TRAIN_STAY:TemplateState = new TemplateState("TrainingStay");
        public static const TRAIN_WIN:TemplateState = new TemplateState("TrainingWin");
        public static const TRAIN_FAIL:TemplateState = new TemplateState("TrainingFail");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getKick(index:int):TemplateState
        {
            if (KICK_VARIANTS[index])
                return KICK_VARIANTS[index];
            throw new Error("Kick variant is not exists. kick index = " + index.toString());
        }

        public static function getDamage(index:int):TemplateState
        {
            if (DAMAGE_VARIANTS[index])
                return DAMAGE_VARIANTS[index];
            throw new Error("Damage variant is not exists. damage index = " + index.toString());
        }

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TemplateState(num:String)
        {
            super(num);
            if (_lockUp)
                throw new Error("Enum has already inited");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.views.constructor.common.TemplateState;

TemplateState.lockUp();