/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.06.13
 * Time: 14:14
 */
package application.views.constructor.common
{
    public class Details
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BODY_CONTAINER:String = "cBody";
        public static const BODY:String = "body";

        public static const LEFT_HAND_CONTAINER:String = "cLarm";
        public static const LEFT_HAND:String = "arm";
        public static const LEFT_GLOVE:String = "left_glove";

        public static const RIGHT_HAND_CONTAINER:String = "cRarm";
        public static const RIGHT_HAND:String = "arm";
        public static const RIGHT_GLOVE:String = "right_glove";

        public static const HAIR_CONTAINER:String = "cHair";
        public static const HAIR:String = "hair";

        public static const FACE:String = "face";
        public static const EYES:String = "eyes";
        public static const MOUTH:String = "mouth";

        public static const HEMATOMA_CONTAINER:String = "cFingal";
        public static const HEMATOMA:String = "fingal";

        public static const WOUND_CONTAINER:String = "cWound";
        public static const WOUND:String = "wound";
        public static const WOUND_POSTFIX:String = "Wounds";

        public static const COAT_CONTAINER:String = "cCoat";
        public static const COAT:String = "coat";

        public static const MASK_CONTAINER:String = "cMask";
        public static const MASK:String = "mask";

        public static const GLASSES_CONTAINER:String = "cGlasses";
        public static const GLASSES:String = "glasses";

        public static const HAT_CONTAINER:String = "cHat";
        public static const HAT:String = "hat";

        public static const SHIELD_CONTAINER:String = "cShield";
        public static const SHIELD:String = "shield";

        public static const WEAPON_CONTAINER:String = "cWeapon";
        public static const WEAPON:String = "weapon";

        public static const HEAVY_WEAPON_CONTAINER:String = "cHeavyWeapon";
        public static const HEAVY_WEAPON:String = "heavyWeapon";

        public static const ITEM:String = "cDrink";
        public static const THROW_ITEM:String = "cThrow";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
