/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.06.13
 * Time: 16:04
 */
package application.views.constructor.stash
{
    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.system.ApplicationDomain;

    import framework.core.tools.SourceManager;
    import framework.core.utils.DestroyUtils;
    import framework.core.utils.DisplayObjectUtils;

    import org.casalib.core.IDestroyable;

    public class TemplateStash implements IDestroyable
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TemplateStash()
        {
            super();
            _appDomain = _source.appDomain;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getChar(data:String, save:Boolean = true):MovieClip
        {
            if (_pool[data])
                return _pool[data];

            if (_source.has(data)) {
                var clip:MovieClip = _source.getMovieClip(data, false);

                if (save)
                    _pool[data] = clip;

                return clip;
            }
            throw new Error("Item <" + data + "> not found")
        }

        public function destroy():void
        {
            for (var index:String in _pool) {
                if (_pool[index] is DisplayObject) {
                    DisplayObjectUtils.removeChild(_pool[index]);
                }
                DestroyUtils.destroy(_pool[index]);
            }
            DestroyUtils.destroy(_pool);
            _destroyed = true;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appDomain:ApplicationDomain;
        private var _pool:Object = {};
        private var _source:SourceManager = SourceManager.instance;

        private var _destroyed:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get destroyed():Boolean
        {
            return _destroyed;
        }
    }
}
