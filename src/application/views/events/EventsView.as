/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.09.13
 * Time: 19:22
 */
package application.views.events
{
    import application.config.AppConfig;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.popup.events.BaseEventBlock;

    import com.eclecticdesignstudio.motion.Equations;
    import com.eclecticdesignstudio.motion.GTweener;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.struct.view.View;

    public class EventsView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public const MAX:int = 4;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EventsView(events:Array)
        {
            super();
            _events = events;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBackground();
            _showEvents();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            for (var i:int = 0; i < _events.length; i++) {
                (_events[i] as BaseEventBlock).destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _events:Array;
        private var _container:View = new View();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBackground():void
        {
            graphics.beginFill(0x000000, .5);
            graphics.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            graphics.endFill();
        }

        private function _showEvents():void
        {
            addChild(_container);

            var count:int = _events.length;

            var cx:int = 16;
            var cy:int = 16;
            var dy:int = 116;

            var limit:int = count > MAX ? MAX : count;

            for (var i:int = 0; i < limit; i++) {
                var event:BaseEventBlock = _events[i] as BaseEventBlock;
                _container.addChild(event);
                event.x = cx;
                event.y = cy;

                cy += dy;
            }

            _container.y = AppConfig.APP_HEIGHT;
            GTweener.addTween(_container, 0.3, {y: 0}, { ease: Equations.easeNone, completeListener: _onTweenComplete });
        }

        private function _onTweenComplete(event:Event):void
        {
            _tweenComplete();
        }

        private function _tweenComplete():void
        {
            super.sound.playUISound(UISound.SUCCESS);
            _bindReactions();
        }

        private function _bindReactions():void
        {
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            if (event.target == event.currentTarget)
                this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
