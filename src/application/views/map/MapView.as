/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:14
 */
package application.views.map
{
    import application.views.AppView;
    import application.views.map.layers.BuildingsLayer;
    import application.views.map.layers.IconsLayer;

    public class MapView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MapView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupBuildings();
            _setupIconsLayer();
        }

        override public function destroy():void
        {
            _buildingsLayer.destroy();
            _iconsLayer.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _buildingsLayer:BuildingsLayer;
        private var _iconsLayer:IconsLayer;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBuildings():void
        {
            _buildingsLayer = new BuildingsLayer();
            addChild(_buildingsLayer);
        }

        private function _setupIconsLayer():void
        {
            _iconsLayer = new IconsLayer();
            addChild(_iconsLayer);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
