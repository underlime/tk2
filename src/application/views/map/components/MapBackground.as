/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.10.13
 * Time: 11:09
 */
package application.views.map.components
{
    import application.config.AppConfig;

    import flash.display.Bitmap;

    import framework.core.struct.view.View;

    public class MapBackground extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SUMMER_TOP:String = "SUMMER_BG_DAY_OK";
        public static const SUMMER_BOTTOM:String = "SUMMER_GROUND_DAY_OK";

        public static const WINTER_TOP:String = "BG_WINTER_DAY0001";
        public static const WINTER_BOTTOM:String = "MAP_WINTER_DAY0001";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MapBackground()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addTop();
            _addRiver();
            _addBottom();
        }

        override public function destroy():void
        {
            _river.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _river:MapRiver;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addTop():void
        {
            var top:Bitmap = super.source.getBitmap(SUMMER_TOP);
            addChild(top);
        }

        private function _addRiver():void
        {
            _river = new MapRiver();
            _river.y = 93;
            addChild(_river);
        }

        private function _addBottom():void
        {
            var bottom:Bitmap = super.source.getBitmap(SUMMER_BOTTOM);
            addChild(bottom);
            bottom.y = AppConfig.APP_HEIGHT - bottom.height;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
