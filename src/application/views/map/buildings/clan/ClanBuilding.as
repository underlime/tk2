/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.03.14
 * Time: 20:37
 */
package application.views.map.buildings.clan
{
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.map.buildings.BaseBuilding;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    import framework.core.enum.Side;

    public class ClanBuilding extends BaseBuilding
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "VOENKOM_BASE_D_L00";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanBuilding()
        {
            super();
            this.y = 132;
            this.x = 427;

            _callEventType = ApplicationEvent.CALL_CLANS;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHintData();
            _addBasis();
            _addRadar();
            _addHead();
        }

        override public function destroy():void
        {
            _radar.destroy();
            _clanHead.destroy();
            super.destroy();
        }

        private function _addHintData():void
        {
            var model:HintModel = hints.getHint('clan');
            model.side = Side.BOTTOM;
            _hintView = new HeaderHint(model);
            _hintView.x = 482;
            _hintView.y = 69;
        }

        private function _addBasis():void
        {
            var basis:Bitmap = super.source.getBitmap(BASIS);
            addChild(basis);
        }

        private function _addRadar():void
        {
            _radar = new ClanRadar();
            addChild(_radar);
            _radar.x = 53;
        }

        private function _addHead():void
        {
            _clanHead = new ClanBuildHead();
            addChild(_clanHead);
            _clanHead.x = 91;
            _clanHead.y = -12;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _radar:ClanRadar;
        private var _clanHead:ClanBuildHead;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _overHandler(event:MouseEvent):void
        {
            super._overHandler(event);
            _clanHead.reverse = false;
            _clanHead.playTo(ClanBuildHead.OPEN_FRAME);
        }

        override protected function _outHandler(event:MouseEvent):void
        {
            super._outHandler(event);
            _clanHead.reverse = true;
            _clanHead.playTo(1);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
