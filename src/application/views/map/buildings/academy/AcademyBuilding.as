/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:41
 */
package application.views.map.buildings.academy
{
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.map.buildings.*;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    import framework.core.enum.Language;
    import framework.core.enum.Side;

    public class AcademyBuilding extends BaseBuilding
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "ACADEMY_BASE_D_L01";
        public static const SIGN_RU:String = "ACADEMY_LABEL_RU";
        public static const SIGN_EN:String = "ACADEMY_LABEL_EN";
        public static const GLASS:String = "ACADEMY_GLASS_D_L04";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AcademyBuilding()
        {
            super();
            this.x = 1;
            this.y = 306;
            super._callEventType = ApplicationEvent.CALL_ACADEMY;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHintData();
            _addBasis();
            _addSign();
            _addGlassAnimation();
            _addGlass();
            _addFlag();
            _addClock();
            _addDoor();
        }

        override public function destroy():void
        {
            _glassAnimation.destroy();
            _flagAnimation.destroy();
            _clockAnimation.destroy();
            _doorAnimation.destroy();
            _hintView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _glassAnimation:GlassAnimation;
        private var _flagAnimation:FlagAnimation;
        private var _clockAnimation:ClockAnimation;
        private var _doorAnimation:DoorAnimation;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHintData():void
        {
            var model:HintModel = hints.getHint('academy');
            model.side = Side.BOTTOM;

            _hintView = new HeaderHint(model);
            _hintView.x = 100;
            _hintView.y = 250;
        }

        private function _addBasis():void
        {
            var basis:Bitmap = super.source.getBitmap(BASIS);
            addChild(basis);
        }

        private function _addSign():void
        {
            var source:String = AppConfig.LANGUAGE == Language.RU ? SIGN_RU : SIGN_EN;
            var sign:Bitmap = super.source.getBitmap(source);
            addChild(sign);
            sign.x = 132;
            sign.y = 92;
        }

        private function _addGlassAnimation():void
        {
            _glassAnimation = new GlassAnimation();
            addChild(_glassAnimation);
            _glassAnimation.x = 171;
            _glassAnimation.y = 24;
        }

        private function _addGlass():void
        {
            var glass:Bitmap = super.source.getBitmap(GLASS);
            glass.x = 171;
            glass.y = 24;
            addChild(glass);
        }

        private function _addFlag():void
        {
            _flagAnimation = new FlagAnimation();
            addChild(_flagAnimation);
            _flagAnimation.x = 131;
            _flagAnimation.y = -11;
        }

        private function _addClock():void
        {
            _clockAnimation = new ClockAnimation();
            addChild(_clockAnimation);
            _clockAnimation.x = 136;
            _clockAnimation.y = 45;
        }

        private function _addDoor():void
        {
            _doorAnimation = new DoorAnimation();
            addChild(_doorAnimation);
            _doorAnimation.x = 165;
            _doorAnimation.y = 128;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _overHandler(event:MouseEvent):void
        {
            super._overHandler(event);
            _doorAnimation.reverse = false;
            _doorAnimation.playTo(DoorAnimation.OPEN_FRAME);
        }

        override protected function _outHandler(event:MouseEvent):void
        {
            super._outHandler(event);
            _doorAnimation.reverse = true;
            _doorAnimation.playTo(1);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
