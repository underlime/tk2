/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.10.13
 * Time: 15:11
 */
package application.views.map.buildings.academy
{
    import flash.display.Bitmap;

    import framework.core.display.animation.Animation;
    import framework.core.helpers.FramesGen;

    public class DoorAnimation extends Animation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "ACADEMY_DOOR_D_L02_";
        public static const FRAMES:int = 15;
        public static const OPEN_FRAME:int = 14;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DoorAnimation()
        {
            var frames:Vector.<Bitmap> = FramesGen.getLoopFrames(BASIS, FRAMES, 1);
            super(frames);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            stop();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
