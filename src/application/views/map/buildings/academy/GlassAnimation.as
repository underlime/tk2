/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.10.13
 * Time: 14:52
 */
package application.views.map.buildings.academy
{
    import flash.display.Bitmap;

    import framework.core.display.animation.TimeAnimation;
    import framework.core.helpers.FramesGen;

    public class GlassAnimation extends TimeAnimation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "ACADEMY_JAM_D_L03_";
        public static const FRAMES:int = 10;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GlassAnimation()
        {
            var frames:Vector.<Bitmap> = FramesGen.getFrames(BASIS, FRAMES, 1);
            super(frames, 24);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
