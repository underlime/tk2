/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:31
 */
package application.views.map.buildings
{
    import application.events.ApplicationEvent;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.screen.common.hint.HeaderHint;

    import com.greensock.TweenMax;

    import flash.events.MouseEvent;
    import flash.filters.GlowFilter;

    public class BaseBuilding extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseBuilding()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;

            _setFilters();
            _setReactions();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _callEventType:String = ApplicationEvent.UNDEFINED_CALL;
        protected var _hintView:HeaderHint;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _arrFilters:Array;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setFilters():void
        {
            var filter1:GlowFilter = new GlowFilter(0xffff99, 0, 0, 0, 2, 1);
            var filter2:GlowFilter = new GlowFilter(0xff9900, 0, 0, 0, 2);
            _arrFilters = [filter1, filter2];
        }

        private function _setReactions():void
        {
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _overHandler(event:MouseEvent):void
        {
            this.filters = _arrFilters;

            TweenMax.to(this, .3, {glowFilter: {color: 0xffff99, alpha: 1, blurX: 10, blurY: 10, index: 0}});
            TweenMax.to(this, .3, {glowFilter: {color: 0xff9900, alpha: 1, blurX: 20, blurY: 20, index: 1}, overwrite: false});

            if (_hintView)
                super.popupManager.add(_hintView);
        }

        protected function _outHandler(event:MouseEvent):void
        {
            TweenMax.killAll();
            this.filters = [];
            super.popupManager.remove();
        }

        private function _clickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(_callEventType, true));
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
