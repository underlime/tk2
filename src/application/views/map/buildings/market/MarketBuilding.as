/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:40
 */
package application.views.map.buildings.market
{
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.map.buildings.*;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;

    import framework.core.enum.Language;
    import framework.core.enum.Side;

    public class MarketBuilding extends BaseBuilding
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "MARKET_BASE_D_L01";
        public static const SIGN_RU:String = "SIGN_RU";
        public static const SIGN_EN:String = "SIGN_EN";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketBuilding()
        {
            super();
            this.x = 489;
            this.y = 289;
            super._callEventType = ApplicationEvent.CALL_MARKET;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHintData();
            _addBasis();
            _addTable();
            _addSign();
            _addFlag();
            _addDisplay();
        }

        override public function destroy():void
        {
            _flag.destroy();
            _display.destroy();
            _table.destroy();
            _hintView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _flag:MarketFlag;

        private var _display:MarketDisplay;
        private var _table:MarketTable;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHintData():void
        {
            var model:HintModel = hints.getHint('market');
            model.side = Side.BOTTOM;
            _hintView = new HeaderHint(model);
            _hintView.x = 610;
            _hintView.y = 240;
        }

        private function _addBasis():void
        {
            var basis:Bitmap = super.source.getBitmap(BASIS);
            addChild(basis);
        }

        private function _addTable():void
        {
            _table = new MarketTable();
            addChild(_table);
            _table.x = 156;
            _table.y = -3;
        }

        private function _addSign():void
        {
            var source:String = AppConfig.LANGUAGE == Language.RU ? SIGN_RU : SIGN_EN;
            var sign:Bitmap = super.source.getBitmap(source);
            addChild(sign);
            sign.x = 156;
            sign.y = -3;
        }

        private function _addFlag():void
        {
            _flag = new MarketFlag();
            _flag.x = 124;
            _flag.y = 16;
            addChild(_flag);
        }

        private function _addDisplay():void
        {
            _display = new MarketDisplay();
            _display.x = 49;
            _display.y = 20;
            addChild(_display);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
