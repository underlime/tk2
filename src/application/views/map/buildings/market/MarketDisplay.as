/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.10.13
 * Time: 22:57
 */
package application.views.map.buildings.market
{
    import flash.display.Bitmap;

    import framework.core.display.animation.Animation;
    import framework.core.helpers.FramesGen;

    public class MarketDisplay extends Animation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "MARKET_DISPLAY_D_L04_0";
        public static const FRAMES:int = 15;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketDisplay()
        {
            var frames:Vector.<Bitmap> = FramesGen.getFrames(BASIS, FRAMES, 1);
            super(frames);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
