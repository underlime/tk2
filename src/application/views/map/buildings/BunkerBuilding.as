/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:43
 */
package application.views.map.buildings
{
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;

    import framework.core.enum.Side;

    public class BunkerBuilding extends BaseBuilding
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "VOENKOM";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BunkerBuilding()
        {
            super();
            this.y = 115;
            this.x = 452;

            _callEventType = ApplicationEvent.CALL_CLANS;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHintData();
            _addBasis();
        }

        private function _addHintData():void
        {
            var model:HintModel = hints.getHint('recruit');
            model.side = Side.BOTTOM;
            _hintView = new HeaderHint(model);
            _hintView.x = 482;
            _hintView.y = 69;
        }

        private function _addBasis():void
        {
            var basis:Bitmap = super.source.getBitmap(BASIS);
            addChild(basis);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
