/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:42
 */
package application.views.map.buildings.bank
{
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.map.buildings.*;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    import framework.core.enum.Language;
    import framework.core.enum.Side;

    public class BankBuilding extends BaseBuilding
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "BANK_BASE_D_L1";
        public static const DOME:String = "BANK_GLASS_D_L5";
        public static const SIGN_RU:String = "BANK_LABEL_RU_D_L3";
        public static const SIGN_EN:String = "BANK_LABEL_ENG_D_L3";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankBuilding()
        {
            super();
            this.x = 107;
            this.y = 69;
            super._callEventType = ApplicationEvent.CALL_BANK;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHintData();
            _addBack();
            _addSign();
            _addMoney();
            _addDome();
            _addDoor();
        }

        override public function destroy():void
        {
            _door.destroy();
            _money.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _money:MoneyAnimation;
        private var _door:BankDoor;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHintData():void
        {
            var model:HintModel = hints.getHint('bank');
            model.side = Side.BOTTOM;
            _hintView = new HeaderHint(model);
            _hintView.x = 200;
            _hintView.y = 49;
        }

        private function _addBack():void
        {
            var back:Bitmap = super.source.getBitmap(BACK);
            addChild(back);
        }

        private function _addSign():void
        {
            var signSource:String = AppConfig.LANGUAGE == Language.RU ? SIGN_RU : SIGN_EN;
            var sign:Bitmap = super.source.getBitmap(signSource);
            addChild(sign);
            sign.x = 103;
            sign.y = 60;
        }

        private function _addMoney():void
        {
            _money = new MoneyAnimation();
            addChild(_money);
            _money.x = 83;
        }

        private function _addDome():void
        {
            var dome:Bitmap = super.source.getBitmap(DOME);
            addChild(dome);
            dome.x = 83;
        }

        private function _addDoor():void
        {
            _door = new BankDoor();
            addChild(_door);
            _door.x = 110;
            _door.y = 94;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _overHandler(event:MouseEvent):void
        {
            super._overHandler(event);
            _door.reverse = false;
            _door.playTo(BankDoor.OPEN_FRAME);
        }

        override protected function _outHandler(event:MouseEvent):void
        {
            super._outHandler(event);
            _door.reverse = true;
            _door.playTo(1);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
