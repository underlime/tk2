/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.11.13
 * Time: 15:25
 */
package application.views.map.buildings.gmo
{
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.map.buildings.BaseBuilding;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;

    import framework.core.enum.Side;

    public class GmoTower extends BaseBuilding
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "GMO_CITADEL_BASE_DAY_L00";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoTower()
        {
            super();
            this.x = 278;
            this.y = 359;

            _callEventType = ApplicationEvent.CALL_GMO;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHintData();
            _addBasis();
            _addSoup();
            _addStream();
        }

        override public function destroy():void
        {
            _soup.destroy();
            _gmoStream.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _soup:GmoSoup;
        private var _gmoStream:GmoStream;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBasis():void
        {
            var basis:Bitmap = super.source.getBitmap(BASIS);
            addChild(basis);
        }

        private function _addHintData():void
        {
            var model:HintModel = hints.getHint('gmo');
            model.side = Side.BOTTOM;
            _hintView = new HeaderHint(model);
            _hintView.x = 278;
            _hintView.y = 309;
        }

        private function _addSoup():void
        {
            _soup = new GmoSoup();
            _soup.x = 41;
            _soup.y = 6;
            addChild(_soup);
        }

        private function _addStream():void
        {
            _gmoStream = new GmoStream();
            _gmoStream.x = 67;
            _gmoStream.y = 43;
            addChild(_gmoStream);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
