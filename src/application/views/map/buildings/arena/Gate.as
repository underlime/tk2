/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.10.13
 * Time: 15:27
 */
package application.views.map.buildings.arena
{
    import flash.display.Bitmap;

    import framework.core.display.animation.Animation;
    import framework.core.helpers.FramesGen;

    public class Gate extends Animation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASIS:String = "ARENA_GATE_D_L2_";
        public static const COUNT:int = 19;

        public static const OPEN_FRAME:int = 18;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Gate()
        {
            var frames:Vector.<Bitmap> = FramesGen.getLoopFrames(BASIS, COUNT, 1);
            super(frames);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            stop();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
