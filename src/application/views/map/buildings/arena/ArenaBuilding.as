/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:38
 */
package application.views.map.buildings.arena
{
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.map.buildings.*;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    import framework.core.enum.Language;
    import framework.core.enum.Side;

    public class ArenaBuilding extends BaseBuilding
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BASE:String = "ARENA_1FLOOR_D_L1";
        public static const SIGN_RU:String = "ARENA_LABEL_L5_RU";
        public static const SIGN_EN:String = "ARENA_LABEL_L5_ENG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArenaBuilding()
        {
            super();
            this.x = 218;
            this.y = 233;
            super._callEventType = ApplicationEvent.CALL_ARENA;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHintData();
            _addBase();
            _addObservers();
            _addSignBoard();
            _addChains();
            _addGate();
        }

        override public function destroy():void
        {
            _observers.destroy();
            _chains.destroy();
            _gate.destroy();
            _hintView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _observers:Observers;
        private var _chains:Chains;
        private var _gate:Gate;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHintData():void
        {
            var model:HintModel = hints.getHint('arena');
            model.side = Side.TOP;
            _hintView = new HeaderHint(model);
            _hintView.x = 360;
            _hintView.y = 390;
        }

        private function _addBase():void
        {
            var base:Bitmap = super.source.getBitmap(BASE);
            addChild(base);
        }

        private function _addObservers():void
        {
            _observers = new Observers();
            addChild(_observers);
            _observers.x = 53;
        }

        private function _addSignBoard():void
        {
            var source:String = AppConfig.LANGUAGE == Language.RU ? SIGN_RU : SIGN_EN;
            var sign:Bitmap = super.source.getBitmap(source);
            addChild(sign);
            sign.x = _observers.x + 158;
            sign.y = _observers.y + 79;
        }

        private function _addChains():void
        {
            _chains = new Chains();
            addChild(_chains);
            _chains.x = _observers.x - 19;
            _chains.y = _observers.y - 187;
        }

        private function _addGate():void
        {
            _gate = new Gate();
            addChild(_gate);
            _gate.x = _observers.x + 158;
            _gate.y = _observers.y + 98;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _overHandler(event:MouseEvent):void
        {
            super._overHandler(event);
            _gate.reverse = false;
            _gate.playTo(Gate.OPEN_FRAME);
        }

        override protected function _outHandler(event:MouseEvent):void
        {
            super._outHandler(event);
            _gate.reverse = true;
            _gate.playTo(1);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
