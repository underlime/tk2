/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 11:56
 */
package application.views.map.layers.bars
{
    import application.models.ModelData;
    import application.models.user.User;
    import application.models.user.UserFightData;
    import application.views.helpers.icons.IconType;
    import application.views.helpers.icons.IconsFactory;

    import com.greensock.TweenLite;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;

    import framework.core.struct.data.ModelsRegistry;

    public class EnergyBar extends IconBar
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EnergyBar()
        {
            var icon:Bitmap = IconsFactory.getIcon(IconType.ENERGY_ICO);
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;

            _model = user.userFightData;

            super(icon, _model.energy.value, _model.max_energy.value);

            _barFill = super.source.getBitmap(BarData.BAR_FILL_ENERGY);
            _backFill = super.source.getBitmap(BarData.BAR_FILL_ENERGY_BACK);

            super.barWidth = 76;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addEnergyRemainView();
            _model.addEventListener(Event.CHANGE, _changeHandler);

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        override public function destroy():void
        {
            _model.removeEventListener(Event.CHANGE, _changeHandler);
            TweenLite.killTweensOf(_energyView);
            _energyView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:UserFightData;
        private var _energyView:EnergyRemainView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addEnergyRemainView():void
        {
            _energyView = new EnergyRemainView();

            var cords:Point = localToGlobal(new Point(-10, 50));
            _energyView.x = cords.x;
            _energyView.y = cords.y;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            this.current = _model.energy.value;
            this.total = _model.max_energy.value;
        }

        override protected function _overHandler(event:MouseEvent):void
        {
            super._overHandler(event);
            if (_energyView)
                super.popupManager.add(_energyView);
        }

        override protected function _outHandler(event:MouseEvent):void
        {
            super._outHandler(event);
            super.popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
