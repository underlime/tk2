/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 15:07
 */
package application.views.map.layers.bars
{
    import application.models.ModelData;
    import application.models.common.hint.HintModel;
    import application.models.ranks.RanksModel;
    import application.models.user.User;
    import application.models.user.UserFightData;
    import application.views.helpers.icons.IconType;
    import application.views.helpers.icons.IconsFactory;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;

    import framework.core.struct.data.ModelsRegistry;

    public class GloryBar extends IconBar
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GloryBar()
        {
            var icon:Bitmap = IconsFactory.getIcon(IconType.GLORY_ICO);
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;

            _model = user.userFightData;
            _rankModel = ModelsRegistry.getModel(ModelData.RANKS) as RanksModel;
            _maxGlory = _rankModel.getMaxGlory(_model.glory.value);

            super(icon, _model.glory.value, _maxGlory);

            _barFill = super.source.getBitmap(BarData.BAR_FILL_GLORY);
            _backFill = super.source.getBitmap(BarData.BAR_FILL_GLORY_BACK);

            super.barWidth = 76
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHint();
            _model.addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            _model.removeEventListener(Event.CHANGE, _changeHandler);
            _hintView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:UserFightData;
        private var _hintView:BarHintView;
        private var _maxGlory:int = 0;
        private var _rankModel:RanksModel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHint():void
        {
            var data:HintModel = super.hints.getHint("glory");
            _hintView = new BarHintView(data.header);

            var cords:Point = localToGlobal(new Point(-10, 50));
            _hintView.x = cords.x;
            _hintView.y = cords.y;

            _hintView.message = _model.glory.value.toString() + "/" + _maxGlory.toString();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            this.current = _model.glory.value;
            this.total = _rankModel.getMaxGlory(this.current);

            _hintView.message = _model.glory.value.toString() + "/" + _maxGlory.toString();
        }

        override protected function _overHandler(event:MouseEvent):void
        {
            super._overHandler(event);
            super.popupManager.add(_hintView);
        }

        override protected function _outHandler(event:MouseEvent):void
        {
            super._outHandler(event);
            super.popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
