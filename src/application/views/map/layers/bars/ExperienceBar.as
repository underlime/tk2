/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 15:18
 */
package application.views.map.layers.bars
{
    import application.models.ModelData;
    import application.models.common.hint.HintModel;
    import application.models.user.User;
    import application.models.user.UserFightData;
    import application.models.user.UserOptInfo;
    import application.views.helpers.icons.IconType;
    import application.views.helpers.icons.IconsFactory;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;

    import framework.core.struct.data.ModelsRegistry;

    public class ExperienceBar extends IconBar
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ExperienceBar()
        {
            var icon:Bitmap = IconsFactory.getIcon(IconType.EXPERIENCE_ICO);
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;

            _userOptInfo = user.getChildByName(ModelData.USER_OPT_INFO) as UserOptInfo;
            _fightData = user.userFightData;

            _updateValues();

            super(icon, _currentValue, _maxValue);

            _barFill = super.source.getBitmap(BarData.BAR_FILL_EXP);
            _backFill = super.source.getBitmap(BarData.BAR_FILL_EXP_BACK);

            super.barWidth = 76;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHint();
            super.barLabel = _fightData.experience.value.toString();

            _userOptInfo.addEventListener(Event.CHANGE, _changeHandler);
            _fightData.addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            _userOptInfo.removeEventListener(Event.CHANGE, _changeHandler);
            _fightData.removeEventListener(Event.CHANGE, _changeHandler);
            _hintView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _userOptInfo:UserOptInfo;
        private var _fightData:UserFightData;

        private var _currentValue:int = 0;
        private var _maxValue:int = 0;

        private var _hintView:BarHintView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHint():void
        {
            var data:HintModel = super.hints.getHint("experience");
            _hintView = new BarHintView(data.header);

            var cords:Point = localToGlobal(new Point(-10, 50));
            _hintView.x = cords.x;
            _hintView.y = cords.y;

            _hintView.message = _fightData.experience.value.toString() + "/" + _userOptInfo.level_exp_current.value.toString();
        }

        private function _updateValues():void
        {
            // за ноль принимает значение _userOptInfo.level_exp_previous.value
            _currentValue = _fightData.experience.value - _userOptInfo.level_exp_previous.value;
            _maxValue = _userOptInfo.level_exp_current.value - _userOptInfo.level_exp_previous.value;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _updateValues();
            this.current = _currentValue;
            this.total = _maxValue;
            super.barLabel = _fightData.experience.value.toString();

            _hintView.message = _fightData.experience.value.toString() + "/" + _userOptInfo.level_exp_current.value.toString();
        }

        override protected function _overHandler(event:MouseEvent):void
        {
            super._overHandler(event);
            super.popupManager.add(_hintView);
        }

        override protected function _outHandler(event:MouseEvent):void
        {
            super._outHandler(event);
            super.popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
