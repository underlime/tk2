/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.10.13
 * Time: 19:38
 */
package application.views.map.layers.bars
{
    import application.common.Colors;
    import application.config.AppConfig;
    import application.models.ModelData;
    import application.models.user.User;
    import application.views.TextFactory;
    import application.views.screen.common.hint.BaseHeaderHint;
    import application.views.screen.common.hint.HintCorner;
    import application.views.text.AppTextField;

    import flash.events.Event;
    import flash.text.TextFormatAlign;

    import framework.core.enum.Language;
    import framework.core.enum.Side;
    import framework.core.helpers.DateHelper;
    import framework.core.struct.data.ModelsRegistry;

    public class EnergyRemainView extends BaseHeaderHint
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EnergyRemainView()
        {
            super(TextFactory.instance.getLabel("energy_label"));
            _model = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _model.userFightData.addEventListener(Event.CHANGE, _changeHandler);
            _updateTimeRemainValue();
            _addCorner();
            _updateDimensions();
            _addText();
        }

        override protected function _updateDimensions():void
        {
            super.w = 150;
            super.h = 60;

            _txtHeader.x = super.w / 2 - _txtHeader.textWidth / 2;
            _txtHeader.y = 6;
        }

        override public function destroy():void
        {
            _model.userFightData.removeEventListener(Event.CHANGE, _changeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:User;
        private var _txtData:AppTextField;

        private var _timeRemain:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateTimeRemainValue():void
        {
            _timeRemain = (_model.userFightData.max_energy.value - _model.userFightData.energy.value) * _model.userOptInfo.energy_period.value;
        }

        private function _addCorner():void
        {
            var corner:HintCorner = new HintCorner(Side.TOP);
            addChild(corner);

            corner.x = int(super.w / 2 - corner.w / 2);
            corner.y = -1 * HintCorner.CORNER_OFFSET;
        }

        private function _addText():void
        {
            _txtData = new AppTextField();
            _txtData.color = Colors.BROWN;
            _txtData.size = 10;
            _txtData.bold = true;
            _txtData.width = super.w;
            _txtData.height = 15;
            _txtData.align = TextFormatAlign.CENTER;
            addChild(_txtData);
            _txtData.y = 30;

            _updateTimeRemain();
        }

        private function _updateTimeRemain():void
        {
            if (_timeRemain > 0) {
                var lang:String = AppConfig.LANGUAGE == Language.RU ? DateHelper.LANG_RU : DateHelper.LANG_EN;
                _txtData.text = TextFactory.instance.getLabel('remain_label') + DateHelper.formatSecondsStamp(_timeRemain, lang).toString();
            } else {
                _txtData.text = TextFactory.instance.getLabel("energy_full_label");
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _updateTimeRemainValue();
            _updateTimeRemain();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
