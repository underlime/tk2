/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 11:14
 */
package application.views.map.layers.bars
{
    public class BarData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BAR_LEFT:String = "BAR_FRAME_01";
        public static const BAR_CENTER:String = "BAR_FRAME_02";
        public static const BAR_RIGHT:String = "BAR_FRAME_03";

        public static const BAR_FILL_HP:String = "BAR_FILL_HP";
        public static const BAR_FILL_HP_BACK:String = "BAR_FILL_HP_BG";

        public static const BAR_FILL_GLORY:String = "BAR_FILL_GLORY";
        public static const BAR_FILL_GLORY_BACK:String = "BAR_FILL_GLORY_BG";

        public static const BAR_FILL_EXP_BACK:String = "BAR_FILL_EXP_BG";
        public static const BAR_FILL_EXP:String = "BAR_FILL_EXP";

        public static const BAR_FILL_ENERGY:String = "BAR_FILL_ENERGY";
        public static const BAR_FILL_ENERGY_BACK:String = "BAR_FILL_ENERGY_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
