/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 11:20
 */
package application.views.map.layers.bars
{
    import application.views.AppView;
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.text.TextFormatAlign;

    import framework.core.helpers.MathHelper;

    public class MapBar extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BAR_HEIGHT:int = 26;
        public const FILL_MARGIN:int = 4;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MapBar(current:int, total:int)
        {
            super();
            _current = current;
            _total = total;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _checkBitmaps();
            _drawBackground();
            _addBar();
            _drawFrames();
            _addLabel();
            _bindReactions();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _backFill:Bitmap;
        protected var _barFill:Bitmap;

        protected var _x0:int = 0;
        protected var _y0:int = 0;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _barWidth:int = 200;
        private var _current:int;
        private var _total:int;

        private var _leftCorner:Bitmap;
        private var _rightCorner:Bitmap;
        private var _centerFill:Bitmap;

        private var _centerShape:Shape;
        private var _backgroundShape:Shape;
        private var _barShape:Shape;

        private var _txtLabel:ShadowAppText;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _checkBitmaps():void
        {
            if (!_backFill || !_barFill)
                throw new Error("you should define [_backFill] and [_barFill] props");
        }

        private function _drawBackground():void
        {
            _backgroundShape = new Shape();
            _backgroundShape.x = _x0;
            _backgroundShape.y = _y0;

            addChild(_backgroundShape);
            _backgroundShape.graphics.beginBitmapFill(_backFill.bitmapData);
            _backgroundShape.graphics.drawRect(FILL_MARGIN, 0, _barWidth - FILL_MARGIN * 2, _backFill.height);
            _backgroundShape.graphics.endFill();
        }

        private function _addBar():void
        {
            _barShape = new Shape();

            _barShape.x = _x0;
            _barShape.y = _y0;

            addChild(_barShape);

            _updateBar();
        }

        private function _drawFrames():void
        {
            _leftCorner = super.source.getBitmap(BarData.BAR_LEFT);
            _rightCorner = super.source.getBitmap(BarData.BAR_RIGHT);
            _centerFill = super.source.getBitmap(BarData.BAR_CENTER);

            _centerShape = new Shape();

            addChild(_leftCorner);
            addChild(_rightCorner);
            addChild(_centerShape);

            _leftCorner.x = _x0;
            _leftCorner.y = _y0;

            var fillWidth:Number = _barWidth - _leftCorner.width - _rightCorner.width;

            _centerShape.x = _leftCorner.width + _leftCorner.x;
            _centerShape.y = _y0;

            _centerShape.graphics.beginBitmapFill(_centerFill.bitmapData);
            _centerShape.graphics.drawRect(0, 0, fillWidth, _centerFill.height);
            _centerShape.graphics.endFill();

            _rightCorner.x = _leftCorner.x + _leftCorner.width + fillWidth;
            _rightCorner.y = _y0;
        }

        private function _updateBar():void
        {
            var percent:Number = _current / _total;
            if (percent > 1)
                percent = 1;

            var currentWidth:int = int(percent * (_barWidth - FILL_MARGIN * 2));
            if (currentWidth == 0)
                currentWidth = 1;

            _barShape.graphics.clear();

            _barShape.graphics.beginBitmapFill(_barFill.bitmapData);
            _barShape.graphics.drawRect(FILL_MARGIN, 0, currentWidth, BAR_HEIGHT);
            _barShape.graphics.endFill();
        }

        private function _addLabel():void
        {
            _txtLabel = new ShadowAppText(0x391a19);
            _txtLabel.size = 12;
            _txtLabel.multiline = false;
            _txtLabel.align = TextFormatAlign.CENTER;
            addChild(_txtLabel);
            _txtLabel.text = _current.toString();
            _txtLabel.y = _y0 + BAR_HEIGHT - _txtLabel.textHeight / 2 - 5;

            _updateLabelPosition();
        }

        private function _updateLabelPosition():void
        {
            _txtLabel.x = _barWidth / 2 - _txtLabel.textWidth / 2 + _x0;
        }

        private function _updateBarData():void
        {
            if (_txtLabel) {
                _updateBar();
                _txtLabel.text = _current.toString();
                _updateLabelPosition();
            }
        }

        private function _bindReactions():void
        {
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _overHandler(event:MouseEvent):void
        {
            var percent:Number = MathHelper.clamp(int((_current / _total) * 100), 0, 100);

            _txtLabel.text = percent.toString() + "%";
            _updateLabelPosition();
        }

        protected function _outHandler(event:MouseEvent):void
        {
            _updateBarData();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get barWidth():int
        {
            return _barWidth;
        }

        public function set barWidth(value:int):void
        {
            _barWidth = value;
        }

        public function get current():int
        {
            return _current;
        }

        public function set current(value:int):void
        {
            _current = value;
            _updateBarData();
        }

        public function get total():int
        {
            return _total;
        }

        public function set total(value:int):void
        {
            _total = value;
            _updateBarData();
        }

        public function set barLabel(value:String):void
        {
            if (_txtLabel) {
                _txtLabel.text = value;
                _updateLabelPosition();
            }
        }
    }
}
