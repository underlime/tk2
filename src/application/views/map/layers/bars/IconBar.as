/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 14:46
 */
package application.views.map.layers.bars
{
    import flash.display.Bitmap;

    public class IconBar extends MapBar
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function IconBar(icon:Bitmap, current:int, total:int)
        {
            super(current, total);
            _icon = icon;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addIcon();
            super.render();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addIcon():void
        {
            addChild(_icon);
            _x0 = _icon.width - 15;
            _y0 = _icon.width / 2 - MapBar.BAR_HEIGHT / 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
