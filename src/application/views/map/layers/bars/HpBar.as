/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.08.13
 * Time: 15:01
 */
package application.views.map.layers.bars
{
    import application.models.common.hint.HintModel;
    import application.models.user.bonus.UserHp;
    import application.views.helpers.icons.IconType;
    import application.views.helpers.icons.IconsFactory;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;

    import framework.core.utils.Singleton;

    public class HpBar extends IconBar
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HpBar()
        {
            var icon:Bitmap = IconsFactory.getIcon(IconType.HP_ICO);
            super(icon, _userHp.hp, _userHp.max_hp);

            _barFill = super.source.getBitmap(BarData.BAR_FILL_HP);
            _backFill = super.source.getBitmap(BarData.BAR_FILL_HP_BACK);
            super.barWidth = 76;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addHint();
            _userHp.addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            _userHp.removeEventListener(Event.CHANGE, _changeHandler);
            _hintView.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _userHp:UserHp = Singleton.getClass(UserHp);
        private var _hintView:BarHintView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHint():void
        {
            var data:HintModel = super.hints.getHint("health");
            _hintView = new BarHintView(data.header);

            var cords:Point = localToGlobal(new Point(-10, 50));
            _hintView.x = cords.x;
            _hintView.y = cords.y;

            _hintView.message = _userHp.hp.toString() + "/" + _userHp.max_hp.toString();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            super.current = _userHp.hp;
            super.total = _userHp.max_hp;
            _hintView.message = _userHp.hp.toString() + "/" + _userHp.max_hp.toString();
        }

        override protected function _overHandler(event:MouseEvent):void
        {
            super._overHandler(event);
            super.popupManager.add(_hintView);
        }

        override protected function _outHandler(event:MouseEvent):void
        {
            super._outHandler(event);
            super.popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
