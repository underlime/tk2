/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 14.11.13
 * Time: 12:28
 */
package application.views.map.layers.bars
{
    import application.common.Colors;
    import application.views.screen.common.hint.BaseHeaderHint;
    import application.views.screen.common.hint.HintCorner;
    import application.views.text.AppTextField;

    import flash.text.TextFormatAlign;

    import framework.core.enum.Side;

    public class BarHintView extends BaseHeaderHint
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BarHintView(header:String)
        {
            super(header);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addCorner();
            _updateDimensions();
            _addText();
        }

        override protected function _updateDimensions():void
        {
            super.w = 150;
            super.h = 60;

            _txtHeader.x = super.w / 2 - _txtHeader.textWidth / 2;
            _txtHeader.y = 6;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtData:AppTextField;
        private var _message:String = "";

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addCorner():void
        {
            var corner:HintCorner = new HintCorner(Side.TOP);
            addChild(corner);

            corner.x = int(super.w / 2 - corner.w / 2);
            corner.y = -1 * HintCorner.CORNER_OFFSET;
        }

        private function _addText():void
        {
            _txtData = new AppTextField();
            _txtData.color = Colors.BROWN;
            _txtData.size = 10;
            _txtData.bold = true;
            _txtData.width = super.w;
            _txtData.height = 15;
            _txtData.align = TextFormatAlign.CENTER;
            addChild(_txtData);
            _txtData.y = 30;
            _txtData.text = _message;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set message(value:String):void
        {
            _message = value;
            if (_txtData)
                _txtData.text = _message;
        }
    }
}
