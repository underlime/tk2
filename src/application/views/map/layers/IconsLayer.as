/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 12:47
 */
package application.views.map.layers
{
    import application.config.AppConfig;
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.user.User;
    import application.models.user.UserOptInfo;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.map.layers.bars.EnergyBar;
    import application.views.map.layers.bars.ExperienceBar;
    import application.views.map.layers.bars.GloryBar;
    import application.views.map.layers.bars.HpBar;
    import application.views.map.layers.buttons.TutorialButton;
    import application.views.map.layers.common.MainScreenUserFrame;
    import application.views.map.layers.icons.HomeIcon;
    import application.views.map.layers.icons.JournalIcon;
    import application.views.map.layers.icons.SettingsIcon;
    import application.views.map.layers.icons.TopIcon;
    import application.views.map.layers.money.FerrosBar;
    import application.views.map.layers.money.TomatosBar;
    import application.views.screen.gmo.labels.GmoBuffs;
    import application.views.screen.top.buttons.SoundButton;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.struct.data.ModelsRegistry;

    public class IconsLayer extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function IconsLayer()
        {
            super();
            _userModel = ModelsRegistry.getModel(ModelData.USER) as User;
            _optInfo = _userModel.userOptInfo;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupTopIcon();
            _setupSettingsIcon();
            _addHomeIcon();
            _addJournalIcon();
            _addBars();
            _addButtons();
            _addUserFrame();
            _addMoneyBars();
            _addGmoBuffs();

            if (_userModel.showTutorial) {
                _addTutorial();
                _userModel.showTutorial = false;
            }
        }

        private function _addGmoBuffs():void
        {
            var gmoBuffs:GmoBuffs = new GmoBuffs();
            gmoBuffs.x = 314;
            gmoBuffs.y = 12;
            addChild(gmoBuffs);
        }

        override public function destroy():void
        {
            _optInfo.removeEventListener(Event.CHANGE, _onOptInfo);

            _topIco.destroy();
            _settingsIco.destroy();
            _homeIcon.destroy();
            _energyBar.destroy();
            _hpBar.destroy();
            _gloryBar.destroy();
            _expBar.destroy();
            _userFrame.destroy();
            _ferrosBar.destroy();
            _tomatosBar.destroy();
            _journalIcon.destroy();
            _tutorialButton.destroy();
            if (_tutorial && !_tutorial.destroyed)
                _tutorial.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _topIco:TopIcon;
        private var _settingsIco:SettingsIcon;
        private var _homeIcon:HomeIcon;

        private var _energyBar:EnergyBar;
        private var _hpBar:HpBar;
        private var _gloryBar:GloryBar;

        private var _expBar:ExperienceBar;
        private var _tutorialButton:TutorialButton;
        private var _soundButton:SoundButton;

        private var _userFrame:MainScreenUserFrame;

        private var _userModel:User;

        private var _ferrosBar:FerrosBar;
        private var _tomatosBar:TomatosBar;

        private var _journalIcon:JournalIcon;
        private var _optInfo:UserOptInfo;

        private var _tutorial:AppTutorial;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTopIcon():void
        {
            _topIco = new TopIcon();
            addChild(_topIco);
            _topIco.x = AppConfig.APP_WIDTH - _topIco.width - 9;
            _topIco.y = 185;
        }

        private function _setupSettingsIcon():void
        {
            _settingsIco = new SettingsIcon();
            addChild(_settingsIco);
            _settingsIco.x = 11;
            _settingsIco.y = 185;
        }

        private function _addHomeIcon():void
        {
            _homeIcon = new HomeIcon();
            addChild(_homeIcon);
            _homeIcon.x = AppConfig.APP_WIDTH - _homeIcon.width - 9;
            _homeIcon.y = 109;
        }

        private function _addJournalIcon():void
        {
            _journalIcon = new JournalIcon(_optInfo.unread_news_count.value);
            addChild(_journalIcon);
            _journalIcon.x = 9;
            _journalIcon.y = 109;
            _optInfo.addEventListener(Event.CHANGE, _onOptInfo);
        }

        private function _onOptInfo(event:Event):void
        {
            _journalIcon.count = _optInfo.unread_news_count.value;
        }

        private function _addBars():void
        {
            _energyBar = new EnergyBar();
            _energyBar.x = 91;
            _energyBar.y = 43;
            addChild(_energyBar);

            _hpBar = new HpBar();
            _hpBar.x = 91;
            _hpBar.y = 8;
            addChild(_hpBar);

            _gloryBar = new GloryBar();
            _gloryBar.x = 202;
            _gloryBar.y = 43;
            addChild(_gloryBar);

            _expBar = new ExperienceBar();
            _expBar.x = 202;
            _expBar.y = 8;
            addChild(_expBar);
        }

        private function _addButtons():void
        {
            _soundButton = new SoundButton();
            addChild(_soundButton);

            _soundButton.x = AppConfig.APP_WIDTH - _soundButton.w - 9;
            _soundButton.y = AppConfig.APP_HEIGHT - _soundButton.h - 9;

            _tutorialButton = new TutorialButton();
            addChild(_tutorialButton);

            _tutorialButton.x = AppConfig.APP_WIDTH - _tutorialButton.w - 9;
            _tutorialButton.y = _soundButton.y - _tutorialButton.h - 9;

            _tutorialButton.addEventListener(MouseEvent.CLICK, _tutorialClickHandler);
        }

        private function _addUserFrame():void
        {
            _userFrame = new MainScreenUserFrame();
            addChild(_userFrame);
            _userFrame.x = 11;
            _userFrame.y = 11;

            _userFrame.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _addMoneyBars():void
        {
            _ferrosBar = new FerrosBar();
            addChild(_ferrosBar);
            _ferrosBar.x = AppConfig.APP_WIDTH - _ferrosBar.width - 9;
            _ferrosBar.y = 14;

            _tomatosBar = new TomatosBar();
            addChild(_tomatosBar);
            _tomatosBar.x = AppConfig.APP_WIDTH - _tomatosBar.width - 44;
            _tomatosBar.y = 50;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_HOME, true);
            dispatchEvent(event);
        }

        private function _tutorialClickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            _addTutorial();
        }

        private function _addTutorial():void
        {
            _tutorial = new AppTutorial();
            addChild(_tutorial);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
