/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.03.14
 * Time: 11:47
 */
package application.views.map.layers
{
    import application.events.ApplicationEvent;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.screen.common.buttons.RejectButton;
    import application.views.screen.common.navigation.TutorialCounter;

    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.MouseEvent;

    public class AppTutorial extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static const DATA:String = "TutorialMc";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AppTutorial()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            _tutorial = super.source.getMovieClip(DATA);
            _tutorial.gotoAndStop(1);
        }

        override protected function render():void
        {
            addChild(_tutorial);
            _addNavigation();
            _addExitButton();
        }

        override public function destroy():void
        {
            _navigation.destroy();
            removeChild(_tutorial);
            _exitButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _tutorial:MovieClip;
        private var _navigation:TutorialCounter;
        private var _exitButton:RejectButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addNavigation():void
        {
            _navigation = new TutorialCounter();

            addChild(_navigation);
            _navigation.x = 202;
            _navigation.y = 457;
            _navigation.totalPages = _tutorial.totalFrames;

            _navigation.addEventListener(Event.CHANGE, _changeHandler);
        }

        private function _addExitButton():void
        {
            _exitButton = new RejectButton(TextFactory.instance.getLabel('exit_tutorial'));
            _exitButton.w = 186;
            addChild(_exitButton);
            _exitButton.x = 11;
            _exitButton.y = 457;

            _exitButton.addEventListener(MouseEvent.CLICK, _exitHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            var page:int = _navigation.currentPage;
            if (_tutorial.totalFrames >= page && page > 0) {
                _tutorial.gotoAndStop(page);
            }
            super.sound.playUISound(UISound.CLICK);
        }

        private function _exitHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLOSE);
            if (_navigation.currentPage >= _tutorial.totalFrames) {
                this.dispatchEvent(new ApplicationEvent(ApplicationEvent.TUTORIAL_COMPLETE, true));
            }
            destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
