/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:15
 */
package application.views.map.layers
{
    import application.views.map.buildings.academy.AcademyBuilding;
    import application.views.map.buildings.arena.ArenaBuilding;
    import application.views.map.buildings.bank.BankBuilding;
    import application.views.map.buildings.clan.ClanBuilding;
    import application.views.map.buildings.gmo.GmoTower;
    import application.views.map.buildings.market.MarketBuilding;
    import application.views.map.components.MapBackground;

    import framework.core.struct.view.View;

    public class BuildingsLayer extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BuildingsLayer()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupBackground();
            _setBuildings();
        }

        override public function destroy():void
        {
            _arena.destroy();
            _market.destroy();
            _academy.destroy();
            _bank.destroy();
            _mapBackground.destroy();
            _gmoTower.destroy();
            _clanHall.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _arena:ArenaBuilding;
        private var _market:MarketBuilding;
        private var _academy:AcademyBuilding;
        private var _bank:BankBuilding;
        private var _clanHall:ClanBuilding;

        private var _mapBackground:MapBackground;
        private var _gmoTower:GmoTower;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _mapBackground = new MapBackground();
            addChild(_mapBackground);
        }

        private function _setBuildings():void
        {
            _bank = new BankBuilding();
            addChild(_bank);

            _clanHall = new ClanBuilding();
            addChild(_clanHall);

            _arena = new ArenaBuilding();
            addChild(_arena);

            _market = new MarketBuilding();
            addChild(_market);

            _academy = new AcademyBuilding();
            addChild(_academy);

            _gmoTower = new GmoTower();
            addChild(_gmoTower);

        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
