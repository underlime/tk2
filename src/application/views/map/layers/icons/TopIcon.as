/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 14:29
 */
package application.views.map.layers.icons
{
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;

    import framework.core.enum.Side;
    import framework.core.tools.SourceManager;

    public class TopIcon extends MainScreenIcon
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopIcon()
        {
            var icon:Bitmap = SourceManager.instance.getBitmap(IconsData.TOP_ICON);
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_TOP, true);
            super(icon, event);

            var model:HintModel = hints.getHint('top');
            model.side = Side.RIGHT;
            _hintView = new HeaderHint(model);
            _hintView.x = 555;
            _hintView.y = 205;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
