/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 14:29
 */
package application.views.map.layers.icons
{
    public class IconsData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOP_ICON:String = "ICO_TOP";
        public static const SETTINGS_ICON:String = "ICO_SETTINGS";
        public static const JOURNAL_ICON:String = "ICO_LOG";
        public static const HOME_ICON:String = "ICO_HOME";
        public static const GIFT_ICON:String = "ICO_GIFTS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
