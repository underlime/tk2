/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 20.08.13
 * Time: 20:19
 */
package application.views.map.layers.icons
{
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;

    import framework.core.enum.Side;
    import framework.core.tools.SourceManager;

    public class HomeIcon extends MainScreenIcon
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HomeIcon()
        {
            var icon:Bitmap = SourceManager.instance.getBitmap(IconsData.HOME_ICON);
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_HOME, true);
            super(icon, event);

            var model:HintModel = hints.getHint('home');
            model.side = Side.RIGHT;
            _hintView = new HeaderHint(model);
            _hintView.x = 520;
            _hintView.y = 125;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
