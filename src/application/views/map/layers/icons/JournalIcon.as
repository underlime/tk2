/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 03.10.13
 * Time: 21:08
 */
package application.views.map.layers.icons
{
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.map.layers.common.NotificationCount;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;

    import framework.core.enum.Side;
    import framework.core.helpers.MathHelper;
    import framework.core.tools.SourceManager;

    public class JournalIcon extends MainScreenIcon
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function JournalIcon(count:int = 0)
        {
            var icon:Bitmap = SourceManager.instance.getBitmap(IconsData.JOURNAL_ICON);
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_JOURNAL, true);
            super(icon, event);

            _count = count;

            var model:HintModel = hints.getHint('journal');
            model.side = Side.LEFT;
            _hintView = new HeaderHint(model);
            _hintView.x = 95;
            _hintView.y = 127;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addCounter();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _count:int;
        private var _counter:NotificationCount;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addCounter():void
        {
            _counter = new NotificationCount((_count).toString());
            _counter.notificationWidth = 40;
            _counter.x = int(_icon.width / 2 - _counter.notificationWidth / 2);
            _counter.y = 49;

            if (_count > 0) {
                addChild(_counter);
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get count():int
        {
            return _count;
        }

        public function set count(value:int):void
        {
            _count = MathHelper.clamp(value, 0, 99);
            if (_count < 1 && this.contains(_counter)) {
                removeChild(_counter);
            }
            else {
                _counter.label = _count.toString();
                addChild(_counter)
            }

        }
    }
}
