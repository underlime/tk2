/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.07.13
 * Time: 12:50
 */
package application.views.map.layers.icons
{
    import application.events.ApplicationEvent;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.screen.common.hint.HeaderHint;

    import com.greensock.TweenMax;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.filters.GlowFilter;

    public class MainScreenIcon extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MainScreenIcon(icon:Bitmap, event:ApplicationEvent)
        {
            super();
            _icon = icon;
            _event = event;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupIcon();
            _bindReactions();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            this.filters = [];

            if (_hintView)
                _hintView.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _icon:Bitmap;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _event:ApplicationEvent;
        private var _filter1:GlowFilter = new GlowFilter(0xffff99, 0, 0, 0, 2, 1);
        private var _filter2:GlowFilter = new GlowFilter(0xff9900, 0, 0, 0, 2);
        protected var _hintView:HeaderHint;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupIcon():void
        {
            buttonMode = true;
            useHandCursor = true;

            addChild(_icon);
        }

        private function _bindReactions():void
        {
            addEventListener(MouseEvent.CLICK, _clickHandler);
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/


        private function _clickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(_event);
            super.sound.playUISound(UISound.CLICK);
        }

        private function _overHandler(event:MouseEvent):void
        {
            this.filters = [_filter1, _filter2];

            TweenMax.to(this, .3, {glowFilter: {color: 0xffff99, alpha: 1, blurX: 10, blurY: 10, index: 0}});
            TweenMax.to(this, .3, {glowFilter: {color: 0xff9900, alpha: 1, blurX: 20, blurY: 20, index: 1}, overwrite: false});

            if (_hintView)
                super.popupManager.add(_hintView);
        }

        private function _outHandler(event:MouseEvent):void
        {
            TweenMax.killAll();
            this.filters = [];
            super.popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
