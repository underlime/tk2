/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.07.13
 * Time: 11:24
 */
package application.views.map.layers.icons
{
    import application.events.ApplicationEvent;
    import application.models.common.hint.HintModel;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;

    import framework.core.enum.Side;
    import framework.core.tools.SourceManager;

    public class SettingsIcon extends MainScreenIcon
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SettingsIcon()
        {
            var icon:Bitmap = SourceManager.instance.getBitmap(IconsData.SETTINGS_ICON);
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_SETTINGS, true);
            super(icon, event);

            var model:HintModel = hints.getHint('settings');
            model.side = Side.LEFT;

            _hintView = new HeaderHint(model);
            _hintView.x = 84;
            _hintView.y = 200;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
