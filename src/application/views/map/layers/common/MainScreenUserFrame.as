/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 20.08.13
 * Time: 20:15
 */
package application.views.map.layers.common
{
    import application.models.ModelData;
    import application.models.user.User;
    import application.views.map.layers.buttons.LevelPlusButton;
    import application.views.screen.common.frame.UserFrame;

    import flash.events.Event;

    import framework.core.struct.data.ModelsRegistry;

    public class MainScreenUserFrame extends UserFrame
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MainScreenUserFrame()
        {
            _userModel = ModelsRegistry.getModel(ModelData.USER) as User;
            super(
                    _userModel.userInfo.soc_net_id.value,
                    _userModel.socNetUserInfo.picture.value,
                    _userModel.userLevelParams.max_level.value,
                    _userModel.userFightData.glory.value
            );
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _userModel.userLevelParams.addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            _userModel.userLevelParams.removeEventListener(Event.CHANGE, _changeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _userModel:User;
        private var _plusButton:LevelPlusButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addPlusButton():void
        {
            _plusButton = new LevelPlusButton();
            addChild(_plusButton);
            _plusButton.x = (-1) * _plusButton.w / 4;
            _plusButton.y = (-1) * _plusButton.h / 4;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            super.level = _userModel.userLevelParams.level.value;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
