/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 20.08.13
 * Time: 21:17
 */
package application.views.map.layers.common
{
    import application.views.screen.common.text.ShadowAppText;

    import flash.display.Bitmap;
    import flash.display.Shape;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class NotificationCount extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const LEFT:String = "NOTIFICATION_COUNT_01";
        public static const CENTER:String = "NOTIFICATION_COUNT_02";
        public static const RIGHT:String = "NOTIFICATION_COUNT_03";

        public static const HEIGHT:int = 26;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        /**
         * @param label - заголовок
         * @param back - вариант подложки (0, 1, 2)
         */
        public function NotificationCount(label:String)
        {
            super();
            _label = label;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _rectangle = new Rectangle(0, 0, _notificationWidth, HEIGHT);
            _drawBackground();
            _addLabel();
            this.scrollRect = new Rectangle(0, 0, _notificationWidth, HEIGHT);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function enable():void
        {
            if (_container) {
                _rectangle.y = HEIGHT;
                _container.scrollRect = _rectangle;
            }

        }

        public function disable():void
        {
            if (_container) {
                _rectangle.y = 0;
                _container.scrollRect = _rectangle;
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _label:String;
        private var _notificationWidth:int = 100;

        private var _txtLabel:ShadowAppText;

        private var _leftCorner:Bitmap;
        private var _centerShape:Shape;
        private var _rightCorner:Bitmap;

        private var _centerFill:Bitmap;

        private var _container:View;
        private var _rectangle:Rectangle;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _drawBackground():void
        {
            _container = new View();
            addChild(_container);

            _leftCorner = super.source.getBitmap(LEFT);
            _container.addChild(_leftCorner);

            _rightCorner = super.source.getBitmap(RIGHT);
            _container.addChild(_rightCorner);

            _centerFill = super.source.getBitmap(CENTER);

            _centerShape = new Shape();
            _container.addChild(_centerShape);

            var currentWidth:Number = _notificationWidth - _leftCorner.width - _rightCorner.width;
            _centerShape.x = _leftCorner.width;

            _centerShape.graphics.beginBitmapFill(_centerFill.bitmapData);
            _centerShape.graphics.drawRect(0, 0, currentWidth, _centerFill.height);
            _centerShape.graphics.endFill();

            _rightCorner.x = _leftCorner.width + currentWidth;

            this.disable();
        }

        private function _addLabel():void
        {
            _txtLabel = new ShadowAppText(0x4b1c0f);
            _txtLabel.autoSize = TextFieldAutoSize.NONE;
            _txtLabel.width = _notificationWidth;
            _txtLabel.align = TextFormatAlign.CENTER;
            _txtLabel.size = 12;
            _txtLabel.color = 0xffffff;
            _txtLabel.bold = true;
            addChild(_txtLabel);
            _txtLabel.text = _label;
            _txtLabel.y = 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get notificationWidth():int
        {
            return _notificationWidth;
        }

        public function set notificationWidth(value:int):void
        {
            _notificationWidth = value;
        }

        public function get label():String
        {
            return _label;
        }

        public function set label(value:String):void
        {
            _label = value;
            if (_txtLabel) {
                _txtLabel.text = _label;
            }
        }
    }
}
