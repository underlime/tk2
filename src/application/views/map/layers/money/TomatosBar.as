/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.08.13
 * Time: 12:50
 */
package application.views.map.layers.money
{
    import application.models.ModelData;
    import application.models.common.hint.HintModel;
    import application.models.user.User;
    import application.models.user.UserFightData;
    import application.views.AppView;
    import application.views.map.layers.buttons.TomatosPlusButton;
    import application.views.map.layers.common.NotificationCount;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.enum.Side;
    import framework.core.struct.data.ModelsRegistry;

    import org.casalib.util.NumberUtil;

    public class TomatosBar extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TomatosBar()
        {
            super();
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;
            _model = user.userFightData;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addNote();
            _addHint();
            _model.addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            if (_model) {
                _model.removeEventListener(Event.CHANGE, _changeHandler);
            }
            if (_note && !_note.destroyed) {
                _note.destroy();
            }
            if (_button && !_button.destroyed) {
                _button.destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _note:NotificationCount;
        private var _model:UserFightData;
        private var _button:TomatosPlusButton;
        private var _hintView:HeaderHint;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addNote():void
        {
            var value:String = NumberUtil.format(_model.tomatos.value, " ");
            _note = new NotificationCount(value);
            _note.notificationWidth = 110;
            addChild(_note);

            var icon:Bitmap = super.source.getBitmap(CommonData.TOMATOS_SMALL_ICON_BAR);
            icon.x = -5;
            icon.y = NotificationCount.HEIGHT / 2 - icon.height / 2;
            addChild(icon);
        }

        private function _addHint():void
        {
            var model:HintModel = hints.getHint('tomatos');
            model.side = Side.RIGHT;
            _hintView = new HeaderHint(model);
            _hintView.x = 484;
            _hintView.y = 45;

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            if (_note) {
                var value:String = NumberUtil.format(_model.tomatos.value, " ");
                _note.label = value;
            }
        }

        private function _overHandler(event:MouseEvent):void
        {
            if (_hintView)
                super.popupManager.add(_hintView);
        }

        private function _outHandler(event:MouseEvent):void
        {
            super.popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
