/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 20.08.13
 * Time: 21:50
 */
package application.views.map.layers.money
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.common.hint.HintModel;
    import application.models.user.User;
    import application.models.user.UserFightData;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.map.layers.buttons.FerrosPlusButton;
    import application.views.map.layers.common.NotificationCount;
    import application.views.screen.common.CommonData;
    import application.views.screen.common.hint.HeaderHint;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.enum.Side;
    import framework.core.struct.data.ModelsRegistry;

    import org.casalib.util.NumberUtil;

    public class FerrosBar extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FerrosBar()
        {
            super();
            var user:User = ModelsRegistry.getModel(ModelData.USER) as User;
            _model = user.userFightData;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addHint();
            _addNote();
            _addButton();
            _model.addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            _model.removeEventListener(Event.CHANGE, _changeHandler);
            _note.destroy();
            _button.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _note:NotificationCount;
        private var _model:UserFightData;
        private var _hintView:HeaderHint;
        private var _button:FerrosPlusButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addHint():void
        {
            var model:HintModel = hints.getHint('ferros');
            model.side = Side.RIGHT;
            _hintView = new HeaderHint(model);
            _hintView.x = 495;
            _hintView.y = 10;

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _addNote():void
        {
            var value:String = NumberUtil.format(_model.ferros.value, " ");
            _note = new NotificationCount(value);
            _note.notificationWidth = 110;
            addChild(_note);

            var icon:Bitmap = super.source.getBitmap(CommonData.FERROS_SMALL_ICON_BAR);
            icon.x = -5;
            icon.y = NotificationCount.HEIGHT / 2 - icon.height / 2;
            addChild(icon);
        }

        private function _addButton():void
        {
            _button = new FerrosPlusButton();
            addChild(_button);
            _button.x = _note.x + _note.notificationWidth + 5;
            _button.y = NotificationCount.HEIGHT / 2 - _button.h / 2;

            _button.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            if (_note) {
                var value:String = NumberUtil.format(_model.ferros.value, " ");
                _note.label = value;
            }

        }

        private function _clickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_BANK, true));
            super.sound.playUISound(UISound.CLICK);
        }

        private function _overHandler(event:MouseEvent):void
        {
            if (_hintView)
                super.popupManager.add(_hintView);
        }

        private function _outHandler(event:MouseEvent):void
        {
            super.popupManager.remove();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
