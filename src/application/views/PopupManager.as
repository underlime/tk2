/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.11.13
 * Time: 19:28
 */
package application.views
{
    import com.greensock.TweenLite;

    import flash.display.DisplayObjectContainer;

    import framework.core.struct.service.Service;
    import framework.core.struct.view.View;
    import framework.core.utils.Assert;

    public class PopupManager extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const DELAY:Number = .5;
        public static const SPEED:Number = .2;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PopupManager()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function registerRoot(root:DisplayObjectContainer):void
        {
            _root = root;
        }

        public function add(popup:View):void
        {
            CONFIG::DEBUG {
                Assert.objectIsNull(_root, "PopupManager: root is not defined!");
            }

            if (_blocked)
                remove();

            _popup = popup;
            _addPopup();
        }

        public function remove():void
        {
            if (_popup) {
                if (_root.contains(_popup))
                    _root.removeChild(_popup);

                TweenLite.killTweensOf(_popup);
                _popup.alpha = 1;
                _popup.visible = true;
            }

            _blocked = false;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _root:DisplayObjectContainer;
        private var _popup:View;
        private var _blocked:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addPopup():void
        {
            _blocked = true;
            _popup.visible = false;
            _root.addChild(_popup);
            _animateShow();
        }

        private function _animateShow():void
        {
            _popup.alpha = 0;
            TweenLite.killTweensOf(_popup);
            TweenLite.to(_popup, SPEED, {
                "onStart": _tweenStart,
                "alpha": 1,
                "delay": DELAY
            });
        }

        private function _tweenStart():void
        {
            _popup.visible = true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
