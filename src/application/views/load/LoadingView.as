/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 31.05.13
 * Time: 16:55
 */
package application.views.load
{
    import application.config.AppConfig;
    import application.controllers.application.ApiExporter;

    import flash.display.Bitmap;

    import framework.core.display.animation.Animation;
    import framework.core.struct.view.View;
    import framework.core.tools.SpriteSheetTool;

    public class LoadingView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LoadingView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public function showLoader():void
        {
            if (!ApiExporter.lockCalls) {
                ApiExporter.lockCalls = true;
                _apiCallsLock = true;
            }
            else {
                _apiCallsLock = false;
            }
            _drawBackground();
            _addLoader();
        }

        public function hideLoader():void
        {
            this.graphics.clear();
            _loader.destroy();
            if (_apiCallsLock) {
                ApiExporter.lockCalls = false;
                _apiCallsLock = false;
            }
        }

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../lib/assets/LOADER_OK.png")]
        private var LoaderClass:Class;

        private var _loader:Animation;
        private var _apiCallsLock:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _drawBackground():void
        {
            graphics.beginFill(0x000000, .8);
            graphics.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            graphics.endFill();
        }

        private function _addLoader():void
        {
            var sprites:Bitmap = new LoaderClass();
            _loader = SpriteSheetTool.getAnimation(sprites, 12, 1, 12);
            addChild(_loader);
            _loader.x = AppConfig.APP_WIDTH / 2 - _loader.maxWidth / 2;
            _loader.y = AppConfig.APP_HEIGHT / 2 - _loader.maxHeight / 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
