/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 18:03
 */
package application.views.load
{
    import application.views.text.AppTextField;

    import com.greensock.TweenLite;

    import flash.filters.GlowFilter;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.core.struct.view.View;

    public class LoadView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LoadView()
        {
            super();
            super.$disableInteractive();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setLoader();
            _setStatusField();
            _setPercentField();
        }

        override public function destroy():void
        {
            TweenLite.killTweensOf(_txtStatus);
            _txtPercent.destroy();
            _txtStatus.destroy();
            _loader.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _status:String = "";
        private var _txtStatus:AppTextField = new AppTextField();

        private var _percent:int = 0;
        private var _txtPercent:AppTextField = new AppTextField();

        private var _loader:LoaderAnimation;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setLoader():void
        {
            _loader = new LoaderAnimation();
            _loader.mouseEnabled = false;
            addChild(_loader);
        }

        private function _setStatusField():void
        {
            _txtStatus = new AppTextField();
            _txtStatus.align = TextFormatAlign.CENTER;
            _txtStatus.autoSize = TextFieldAutoSize.LEFT;
            _txtStatus.wordWrap = true;
            _txtStatus.multiline = true;
            _txtStatus.width = 450;
            _txtStatus.size = 14;
            _txtStatus.x = 150;
            _txtStatus.y = 415;
            addChild(_txtStatus);

            _txtStatus.filters = [new GlowFilter(0x000000, 1, 0, 0, 1)];
        }

        private function _setPercentField():void
        {
            _txtPercent = new AppTextField();
            _txtPercent.align = TextFormatAlign.CENTER;
            _txtPercent.width = 34;
            _txtPercent.height = 23;
            _txtPercent.size = 16;
            _txtPercent.x = 359;
            _txtPercent.y = 347;
            addChild(_txtPercent);

            _txtPercent.filters = [new GlowFilter(0xffffff, 1, 5, 5, 1)];
        }

        private function _updateStatus():void
        {
            var params:Object = {
                "alpha": 0,
                "onComplete": _showStatus
            };
            TweenLite.to(_txtStatus, .5, params)
        }

        private function _showStatus():void
        {
            TweenLite.killTweensOf(_txtStatus);
            _txtStatus.text = _status;

            var params:Object = {
                "alpha": 1
            };
            TweenLite.to(_txtStatus, .5, params);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set status(value:String):void
        {
            _status = value;
            if (_txtStatus)
                _updateStatus();
        }

        public function set percent(value:int):void
        {
            _percent = value;
            if (_txtPercent)
                _txtPercent.text = Math.round(_percent / 2).toString();

            if (_loader)
                _loader.percent = _percent;
        }
    }
}
