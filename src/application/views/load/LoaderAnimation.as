/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 11.03.14
 * Time: 12:29
 */
package application.views.load
{
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.MovieClip;

    import framework.core.helpers.MathHelper;
    import framework.core.struct.view.View;
    import framework.core.utils.DestroyUtils;

    public class LoaderAnimation extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LoaderAnimation()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addBack();
            _addArmy();
            _addIndicator();
            _addFrontPic();
        }

        override public function destroy():void
        {
            DestroyUtils.destroy(_backData);
            DestroyUtils.destroy(_army);
            DestroyUtils.destroy(_indicator);
            DestroyUtils.destroy(_frontData);
            DestroyUtils.destroy(_indicatorData);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _backData:BitmapData;
        private var _army:MovieClip;
        private var _indicator:MovieClip;
        private var _frontData:BitmapData;
        private var _indicatorData:BitmapData;

        private var _percent:int = 1;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addBack():void
        {
            _backData = new Preloader_PreloaderBgPic();
            var bitmap:Bitmap = new Bitmap(_backData);
            addChild(bitmap);
        }

        private function _addArmy():void
        {
            _indicatorData = new Preloader_IndicatorBgPic();
            var bitmap:Bitmap = new Bitmap(_indicatorData);
            addChild(bitmap);

            _army = new Preloader_BattleAnimation();
            addChild(_army);
            _army.x = 0;
            _army.y = -3;
            _army.gotoAndPlay(1);
        }

        private function _addIndicator():void
        {
            _indicator = new Preloader_LoaderIndicator();
            _indicator.x = 266;
            _indicator.y = 146;
            addChild(_indicator);
            _indicator.gotoAndStop(1);
        }

        private function _addFrontPic():void
        {
            _frontData = new Preloader_PreloaderBgPic();
            var pic:Bitmap = new Bitmap(_frontData);
            addChild(pic);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/


        public function set percent(value:int):void
        {
            _percent = value;
            var frame:int = MathHelper.clamp(_percent, 1, 200);
            if (_indicator) {
                _indicator.gotoAndStop(frame);
            }
        }
    }
}
