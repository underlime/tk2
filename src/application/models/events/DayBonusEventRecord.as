package application.models.events
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.IntegerField;


    public class DayBonusEventRecord extends Data
    {
        private var _day:IntegerField = new IntegerField();
        private var _item_id:IntegerField = new IntegerField();
        private var _count:IntegerField = new IntegerField();
        private var _tomatos:IntegerField = new IntegerField();

        public function DayBonusEventRecord()
        {
            super("");
        }

        public function get day():IntegerField
        {
            return _day;
        }

        public function get item_id():IntegerField
        {
            return _item_id;
        }

        public function get count():IntegerField
        {
            return _count;
        }

        public function get tomatos():IntegerField
        {
            return _tomatos;
        }
    }
}
