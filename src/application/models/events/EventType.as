/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.09.13
 * Time: 19:39
 */
package application.models.events
{

    import framework.core.enum.BaseEnum;

    public class EventType extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ACHIEVEMENT:EventType = new EventType("achievment");
        public static const DAY_BONUS:EventType = new EventType("everyday_bonus");
        public static const NEW_SPECIAL:EventType = new EventType("new_special");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EventType(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.models.events.EventType;

EventType.lockUp();