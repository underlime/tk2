/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 11.10.13
 * Time: 20:47
 */
package application.models.events
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;

    public class SpecialEventRecord extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialEventRecord()
        {
            super("");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return [];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _active:BooleanField = new BooleanField();
        private var _code:CharField = new CharField();
        private var _name_ru:CharField = new CharField();
        private var _picture:CharField = new CharField();
        private var _text_ru:CharField = new CharField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get active():BooleanField
        {
            return _active;
        }

        public function get code():CharField
        {
            return _code;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get picture():CharField
        {
            return _picture;
        }

        public function get text_ru():CharField
        {
            return _text_ru;
        }
    }
}
