/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.09.13
 * Time: 19:48
 */
package application.models.events
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class AchievementEventRecord extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AchievementEventRecord()
        {
            super("");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return [];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _achievement_id:IntegerField = new IntegerField();
        private var _glory:IntegerField = new IntegerField();
        private var _name_ru:CharField = new CharField();
        private var _pic:CharField = new CharField();
        private var _text_ru:CharField = new CharField();
        private var _type:CharField = new CharField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get achievement_id():IntegerField
        {
            return _achievement_id;
        }

        public function get glory():IntegerField
        {
            return _glory;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get pic():CharField
        {
            return _pic;
        }

        public function get text_ru():CharField
        {
            return _text_ru;
        }

        public function get type():CharField
        {
            return _type;
        }
    }
}
