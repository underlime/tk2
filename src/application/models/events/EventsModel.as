/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.09.13
 * Time: 19:34
 */
package application.models.events
{
    import application.models.ModelData;

    import flash.events.Event;

    import framework.core.struct.data.Data;

    public class EventsModel extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EventsModel()
        {
            super(ModelData.EVENTS);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["events"];
        }

        override public function importData(data:Object):void
        {
            for (var i:int = 0; i < data.length; ++i) {
                var event:Object = data[i];
                switch (event['type']) {
                    case EventType.ACHIEVEMENT.toString():
                        _addAchievement(event);
                        break;
                    case EventType.DAY_BONUS.toString():
                        _addDayBonus(event);
                        break;
                    case EventType.NEW_SPECIAL.toString():
                        _addNewSpecial(event);
                        break;
                }
            }

            dispatchEvent(new Event(Event.CHANGE));
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function clearAchievements():void
        {
            while (_achievements.length > 0)
                _achievements.pop();
        }

        public function clearSpecials():void
        {
            while (_new_specials.length > 0)
                _new_specials.pop();
        }

        public function clearDayBonus():void
        {
            _dayBonus.destroy();
            _dayBonus = null;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _achievements:Vector.<AchievementEventRecord> = new Vector.<AchievementEventRecord>();
        private var _new_specials:Vector.<SpecialEventRecord> = new Vector.<SpecialEventRecord>();

        private var _dayBonus:DayBonusEventRecord;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addAchievement(event:Object):void
        {
            var data:AchievementEventRecord = new AchievementEventRecord();
            data.importData(event);
            _achievements.push(data);
        }

        private function _addDayBonus(event:Object):void
        {
            _dayBonus = new DayBonusEventRecord();
            _dayBonus.importData(event);
        }

        private function _addNewSpecial(event:Object):void
        {
            var data:SpecialEventRecord = new SpecialEventRecord();
            data.importData(event);
            _new_specials.push(data);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get achievements():Vector.<AchievementEventRecord>
        {
            return _achievements;
        }

        public function get new_specials():Vector.<SpecialEventRecord>
        {
            return _new_specials;
        }

        public function get dayBonus():DayBonusEventRecord
        {
            return _dayBonus;
        }
    }
}
