/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 15:14
 */
package application.models
{
    import application.models.arena.EnemiesList;
    import application.models.bank.Bank;
    import application.models.battle.Battle;
    import application.models.chars.CharList;
    import application.models.clans.ClanRequests;
    import application.models.clans.ClansTop;
    import application.models.eventBroker.EventBrokerParams;
    import application.models.events.EventsModel;
    import application.models.gmo.GmoGains;
    import application.models.gmo.GmoPrices;
    import application.models.inventory.Inventory;
    import application.models.market.MarketItems;
    import application.models.opponents.OpponentsList;
    import application.models.ranks.RanksModel;
    import application.models.top.TopModel;
    import application.models.user.User;
    import application.models.user.journal.Journal;
    import application.models.user.journal.News;

    import framework.core.struct.data.DataBase;

    public class GameDataBase extends DataBase
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GameDataBase()
        {
            super();
            addChild(new User());
            addChild(new Inventory());
            addChild(new MarketItems());
            addChild(new CharList());
            addChild(new TopModel());
            addChild(new RanksModel());
            addChild(new EnemiesList());
            addChild(new Battle());
            addChild(new OpponentsList());
            addChild(new EventsModel());
            addChild(new Journal());
            addChild(new News());
            addChild(new Bank());
            addChild(new GmoPrices());
            addChild(new GmoGains());
            addChild(new ClanRequests());
            addChild(new ClansTop());
            addChild(new EventBrokerParams());
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
