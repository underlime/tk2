/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 21.07.13
 * Time: 20:16
 */
package application.models.arena
{
    import application.models.ModelData;
    import application.models.inventory.Inventory;
    import application.models.user.SocNetUserInfo;
    import application.models.user.UserFightData;
    import application.models.user.UserInfo;
    import application.models.user.UserLevelParams;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.IntegerField;

    public class EnemyRecord extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EnemyRecord()
        {
            super(ModelData.ENEMY_RECORD);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        private var _user_info:UserInfo = new UserInfo();
        private var _soc_net_user_info:SocNetUserInfo = new SocNetUserInfo();
        private var _user_level_params:UserLevelParams = new UserLevelParams();
        private var _user_fight_data:UserFightData = new UserFightData();
        private var _user_inventory:Inventory = new Inventory();

        private var _bonus_hp:IntegerField = new IntegerField();

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get user_info():UserInfo
        {
            return _user_info;
        }

        public function get user_level_params():UserLevelParams
        {
            return _user_level_params;
        }

        public function get user_fight_data():UserFightData
        {
            return _user_fight_data;
        }

        public function get user_inventory():Inventory
        {
            return _user_inventory;
        }

        public function set user_info(value:UserInfo):void
        {
            _user_info = value;
        }

        public function set user_level_params(value:UserLevelParams):void
        {
            _user_level_params = value;
        }

        public function set user_fight_data(value:UserFightData):void
        {
            _user_fight_data = value;
        }

        public function set user_inventory(value:Inventory):void
        {
            _user_inventory = value;
        }

        public function get soc_net_user_info():SocNetUserInfo
        {
            return _soc_net_user_info;
        }

        public function get bonus_hp():IntegerField
        {
            return _bonus_hp;
        }
    }
}
