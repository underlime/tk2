/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.09.13
 * Time: 21:00
 */
package application.models.top
{
    import application.models.ModelData;
    import application.models.clans.ClanInfo;
    import application.models.clans.ClansTop;
    import application.models.user.UserFightData;

    import framework.core.struct.data.ListData;
    import framework.core.struct.data.ModelsRegistry;

    public class TopHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const INTERVALS:Array = [TopInterval.DAY, TopInterval.WEEK, TopInterval.GLOBAL];
        public static const TYPES:Array = [TopType.GLOBAL, TopType.FRIENDS, TopType.CITY];

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getUserPosition(topList:ListData, user_id:int):int
        {
            var list:Object = topList.list.value;
            var position:int = 1;

            for (var key:String in list) {
                var record:TopRecord = list[key] as TopRecord;
                if (record.user_id.value == user_id) {
                    return position;
                }
                position++;
            }

            return 0;
        }

        public static function getUserGlory(userFightData:UserFightData, interval:TopInterval):int
        {
            switch (interval) {
                case TopInterval.WEEK:
                    return userFightData.week_glory.value;
                case TopInterval.DAY:
                    return userFightData.day_glory.value;
            }
            return userFightData.glory.value;
        }

        public static function getInterval(tabID:int):TopInterval
        {
            if (INTERVALS[tabID])
                return INTERVALS[tabID];
            return INTERVALS[0];
        }

        public static function getType(tabID:int):TopType
        {
            if (TYPES[tabID])
                return TYPES[tabID];
            return TYPES[0];
        }

        public static function isClanTab(selectedTab:int):Boolean
        {
            return selectedTab == 3;
        }

        public static function getClanTopPosition(clanInfo:ClanInfo):int
        {
            var clans:ClansTop = ModelsRegistry.getModel(ModelData.CLANS_TOP) as ClansTop;

            if (clanInfo.id.value == 0) {
                return 0;
            }

            var list:Object = clans.clans_top_glory.list.value;
            var i:int = 0;
            for (var key:String in list) {
                i++;
                var position:ClanInfo = list[key];
                if (position.id.value == clanInfo.id.value) {
                    return i;
                }
            }

            return 0;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
