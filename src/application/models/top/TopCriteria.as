/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 11.07.13
 * Time: 11:25
 */
package application.models.top
{

    import framework.core.enum.BaseEnum;

    public class TopCriteria extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const STRENGTH:TopCriteria = new TopCriteria("strength");
        public static const AGILITY:TopCriteria = new TopCriteria("agility");
        public static const INTELLECT:TopCriteria = new TopCriteria("intellect");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopCriteria(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.models.top.TopCriteria;

TopCriteria.lockUp();