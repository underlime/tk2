/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.07.13
 * Time: 15:03
 */
package application.models.top
{
    public class TopData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOP_GLORY_BY_CITY_GLOBAL:String = "top_glory_by_city_global";
        public static const TOP_GLORY_BY_CITY_DAY:String = "top_glory_by_city_day";
        public static const TOP_GLORY_BY_CITY_WEEK:String = "top_glory_by_city_week";

        public static const TOP_GLORY_COMMON_GLOBAL:String = "top_glory_common_global";
        public static const TOP_GLORY_COMMON_WEEK:String = "top_glory_common_week";
        public static const TOP_GLORY_COMMON_DAY:String = "top_glory_common_day";

        public static const TOP_GLORY_FRIENDS_GLOBAL:String = "top_glory_friends_global";
        public static const TOP_GLORY_FRIENDS_WEEK:String = "top_glory_friends_week";
        public static const TOP_GLORY_FRIENDS_DAY:String = "top_glory_friends_day";

        public static const TOP_STRENGTH_COMMON_GLOBAL:String = "top_strength_common_global";
        public static const TOP_AGILITY_COMMON_GLOBAL:String = "top_agility_common_global";
        public static const TOP_INTELLECT_COMMON_GLOBAL:String = "top_intellect_common_global";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
