/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.07.13
 * Time: 10:36
 */
package application.models.top
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.FloatField;
    import framework.core.struct.data.fields.IntegerField;

    public class TopRecord extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopRecord()
        {
            super("TopRecord");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _soc_net_id:CharField = new CharField();
        private var _user_id:FloatField = new FloatField();
        private var _login:CharField = new CharField();
        private var _level:IntegerField = new IntegerField();
        private var _max_level:IntegerField = new IntegerField();
        private var _strength:IntegerField = new IntegerField();
        private var _agility:IntegerField = new IntegerField();
        private var _intellect:IntegerField = new IntegerField();
        private var _glory:IntegerField = new IntegerField();
        private var _day_glory:IntegerField = new IntegerField();
        private var _week_glory:IntegerField = new IntegerField();

        private var _picture:CharField = new CharField();
        private var _first_name:CharField = new CharField();
        private var _last_name:CharField = new CharField();

        private var _clan_name:CharField = new CharField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get soc_net_id():CharField
        {
            return _soc_net_id;
        }

        public function get user_id():FloatField
        {
            return _user_id;
        }

        public function get login():CharField
        {
            return _login;
        }

        public function get level():IntegerField
        {
            return _level;
        }

        public function get strength():IntegerField
        {
            return _strength;
        }

        public function get agility():IntegerField
        {
            return _agility;
        }

        public function get intellect():IntegerField
        {
            return _intellect;
        }

        public function get glory():IntegerField
        {
            return _glory;
        }

        public function get day_glory():IntegerField
        {
            return _day_glory;
        }

        public function get week_glory():IntegerField
        {
            return _week_glory;
        }

        public function get picture():CharField
        {
            return _picture;
        }

        public function get first_name():CharField
        {
            return _first_name;
        }

        public function get last_name():CharField
        {
            return _last_name;
        }

        public function get max_level():IntegerField
        {
            return _max_level;
        }

        public function get clan_name():CharField
        {
            return _clan_name;
        }
    }
}
