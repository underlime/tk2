/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.07.13
 * Time: 16:18
 */
package application.models.top.variants
{
    import application.models.top.TopData;
    import application.models.top.TopRecord;

    import framework.core.struct.data.ListData;

    public class TopGloryFriendsWeek extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopGloryFriendsWeek()
        {
            _RecordClass = TopRecord;
            super(TopData.TOP_GLORY_FRIENDS_WEEK);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return [TopData.TOP_GLORY_FRIENDS_WEEK];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
