/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.07.13
 * Time: 15:03
 */
package application.models.top.variants
{
    import application.models.top.TopData;
    import application.models.top.TopRecord;

    import framework.core.struct.data.ListData;

    public class TopGloryByCityGlobal extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopGloryByCityGlobal()
        {
            _RecordClass = TopRecord;
            super(TopData.TOP_GLORY_BY_CITY_GLOBAL);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return [TopData.TOP_GLORY_BY_CITY_GLOBAL];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
