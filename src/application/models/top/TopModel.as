/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 09.07.13
 * Time: 10:31
 */
package application.models.top
{
    import application.models.ModelData;
    import application.models.top.variants.TopAgilityCommonGlobal;
    import application.models.top.variants.TopGloryByCityDay;
    import application.models.top.variants.TopGloryByCityGlobal;
    import application.models.top.variants.TopGloryByCityWeek;
    import application.models.top.variants.TopGloryCommonDay;
    import application.models.top.variants.TopGloryCommonGlobal;
    import application.models.top.variants.TopGloryCommonWeek;
    import application.models.top.variants.TopGloryFriendsDay;
    import application.models.top.variants.TopGloryFriendsGlobal;
    import application.models.top.variants.TopGloryFriendsWeek;
    import application.models.top.variants.TopIntellectCommonGlobal;
    import application.models.top.variants.TopStrengthCommonGlobal;

    import framework.core.struct.data.DataContainer;
    import framework.core.struct.data.ListData;

    public class TopModel extends DataContainer
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopModel()
        {
            super(ModelData.TOP_MODEL);
            _addModels();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["top"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getTop(type:TopType, interval:TopInterval):ListData
        {
            switch (type) {
                case TopType.GLOBAL:
                    return getTopCommon(interval);
                case TopType.FRIENDS:
                    return getTopFriends(interval);
                case TopType.CITY:
                    return getTopByCity(interval);
            }
            return getTopCommon(interval);
        }

        public function getTopByCity(topInterval:TopInterval):ListData
        {
            var result:ListData = getChildByName(TopData.TOP_GLORY_BY_CITY_GLOBAL) as ListData;

            if (topInterval == TopInterval.WEEK)
                result = getChildByName(TopData.TOP_GLORY_BY_CITY_WEEK) as ListData;
            if (topInterval == TopInterval.DAY)
                result = getChildByName(TopData.TOP_GLORY_BY_CITY_DAY) as ListData;

            return result;
        }

        public function getTopCommon(topInterval:TopInterval):ListData
        {
            var result:ListData = getChildByName(TopData.TOP_GLORY_COMMON_GLOBAL) as ListData;

            if (topInterval == TopInterval.WEEK)
                result = getChildByName(TopData.TOP_GLORY_COMMON_WEEK) as ListData;
            if (topInterval == TopInterval.DAY)
                result = getChildByName(TopData.TOP_GLORY_COMMON_DAY) as ListData;

            return result;
        }

        public function getTopFriends(topInterval:TopInterval):ListData
        {
            var result:ListData = getChildByName(TopData.TOP_GLORY_FRIENDS_GLOBAL) as ListData;

            if (topInterval == TopInterval.WEEK)
                result = getChildByName(TopData.TOP_GLORY_FRIENDS_WEEK) as ListData;
            if (topInterval == TopInterval.DAY)
                result = getChildByName(TopData.TOP_GLORY_FRIENDS_DAY) as ListData;

            return result;
        }

        public function getTopByCriteria(topCriteria:TopCriteria):ListData
        {
            var result:ListData = getChildByName(TopData.TOP_STRENGTH_COMMON_GLOBAL) as ListData;

            if (topCriteria == TopCriteria.AGILITY)
                result = getChildByName(TopData.TOP_AGILITY_COMMON_GLOBAL) as ListData;
            if (topCriteria == TopCriteria.INTELLECT)
                result = getChildByName(TopData.TOP_INTELLECT_COMMON_GLOBAL) as ListData;

            return result;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addModels():void
        {
            addChild(new TopAgilityCommonGlobal());
            addChild(new TopGloryByCityDay());
            addChild(new TopGloryByCityGlobal());
            addChild(new TopGloryByCityWeek());
            addChild(new TopGloryCommonDay());
            addChild(new TopGloryCommonGlobal());
            addChild(new TopGloryCommonWeek());
            addChild(new TopGloryFriendsDay());
            addChild(new TopGloryFriendsGlobal());
            addChild(new TopGloryFriendsWeek());
            addChild(new TopIntellectCommonGlobal());
            addChild(new TopStrengthCommonGlobal());
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
