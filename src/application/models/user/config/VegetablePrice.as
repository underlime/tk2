/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.07.13
 * Time: 11:27
 */
package application.models.user.config
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.IntegerField;

    public class VegetablePrice extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function VegetablePrice()
        {
            super(ModelData.VEGETABLE_PRICE);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["vegetable_price"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _t:IntegerField = new IntegerField();
        private var _f:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get t():IntegerField
        {
            return _t;
        }

        public function get f():IntegerField
        {
            return _f;
        }
    }
}
