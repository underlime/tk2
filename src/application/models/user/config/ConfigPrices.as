/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.07.13
 * Time: 10:54
 */
package application.models.user.config
{
    import application.models.ModelData;

    import framework.core.struct.data.DataContainer;
    import framework.core.struct.data.fields.IntegerField;

    public class ConfigPrices extends DataContainer
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ConfigPrices()
        {
            super(ModelData.CONFIG_PRICES);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["config_prices"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _login_ferros_price:IntegerField = new IntegerField();
        private var _vegetable_price:VegetablePrice = new VegetablePrice();

        private var _reset_skills_ferros_price:IntegerField = new IntegerField();
        private var _ease_enemy_ferros_price:IntegerField = new IntegerField();
        private var _social_slots_ferros_price:IntegerField = new IntegerField();
        private var _item_slot_ferros_price:IntegerField = new IntegerField();
        private var _rival_price:IntegerField = new IntegerField();
        private var _ring_slot_ferros_price:IntegerField = new IntegerField();

        private var _clan_creation_price:IntegerField = new IntegerField();
        private var _clan_rename_price:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get login_ferros_price():IntegerField
        {
            return _login_ferros_price;
        }

        public function get changeVegetablePrice():int
        {
            return _vegetable_price.f.value;
        }

        public function get vegetable_price():VegetablePrice
        {
            return _vegetable_price;
        }

        public function get reset_skills_ferros_price():IntegerField
        {
            return _reset_skills_ferros_price;
        }

        public function get ease_enemy_ferros_price():IntegerField
        {
            return _ease_enemy_ferros_price;
        }

        public function get social_slots_ferros_price():IntegerField
        {
            return _social_slots_ferros_price;
        }

        public function get item_slot_ferros_price():IntegerField
        {
            return _item_slot_ferros_price;
        }

        public function get rival_price():IntegerField
        {
            return _rival_price;
        }

        public function get ring_slot_ferros_price():IntegerField
        {
            return _ring_slot_ferros_price;
        }

        public function get clan_creation_price():IntegerField
        {
            return _clan_creation_price;
        }

        public function get clan_rename_price():IntegerField
        {
            return _clan_rename_price;
        }
    }
}
