/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.08.13
 * Time: 12:37
 */
package application.models.user.achievements
{
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class AchievementRecord
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AchievementRecord()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _achievement_id:IntegerField = new IntegerField();
        private var _code:CharField = new CharField();
        private var _name_ru:CharField = new CharField();
        private var _text_ru:CharField = new CharField();
        private var _pic:CharField = new CharField();
        private var _glory:IntegerField = new IntegerField();
        private var _wins_level:IntegerField = new IntegerField();
        private var _fails_level:IntegerField = new IntegerField();
        private var _ferros_payed:IntegerField = new IntegerField();
        private var _tomatos_payed:IntegerField = new IntegerField();
        private var _visits_level:IntegerField = new IntegerField();
        private var _level_reached:IntegerField = new IntegerField();
        private var _strength_level:IntegerField = new IntegerField();
        private var _agility_level:IntegerField = new IntegerField();
        private var _intellect_level:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get achievement_id():IntegerField
        {
            return _achievement_id;
        }

        public function get code():CharField
        {
            return _code;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get text_ru():CharField
        {
            return _text_ru;
        }

        public function get pic():CharField
        {
            return _pic;
        }

        public function get glory():IntegerField
        {
            return _glory;
        }

        public function get wins_level():IntegerField
        {
            return _wins_level;
        }

        public function get fails_level():IntegerField
        {
            return _fails_level;
        }

        public function get ferros_payed():IntegerField
        {
            return _ferros_payed;
        }

        public function get tomatos_payed():IntegerField
        {
            return _tomatos_payed;
        }

        public function get visits_level():IntegerField
        {
            return _visits_level;
        }

        public function get level_reached():IntegerField
        {
            return _level_reached;
        }

        public function get strength_level():IntegerField
        {
            return _strength_level;
        }

        public function get agility_level():IntegerField
        {
            return _agility_level;
        }

        public function get intellect_level():IntegerField
        {
            return _intellect_level;
        }
    }
}
