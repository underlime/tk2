/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 21.07.13
 * Time: 16:53
 */
package application.models.user.specials
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.FloatField;
    import framework.core.struct.data.fields.IntegerField;

    public class SpecialRecord extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialRecord()
        {
            super(ModelData.SPECIAL_RECORD);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _special_id:IntegerField = new IntegerField();
        private var _code:CharField = new CharField();
        private var _name_ru:CharField = new CharField();
        private var _description_ru:CharField = new CharField();
        private var _picture:CharField = new CharField();
        private var _animation:CharField = new CharField();
        private var _active:BooleanField = new BooleanField();
        private var _bonuses:CharField = new CharField();
        private var _k_damage:FloatField = new FloatField();
        private var _steps:IntegerField = new IntegerField();
        private var _additional_data:CharField = new CharField();
        private var _intellect_level:IntegerField = new IntegerField();
        private var _level_intellect_step:IntegerField = new IntegerField();
        private var _level_k_bonuse:FloatField = new FloatField();
        private var _level:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get special_id():IntegerField
        {
            return _special_id;
        }

        public function get code():CharField
        {
            return _code;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get description_ru():CharField
        {
            return _description_ru;
        }

        public function get picture():CharField
        {
            return _picture;
        }

        public function get animation():CharField
        {
            return _animation;
        }

        public function get active():BooleanField
        {
            return _active;
        }

        public function get bonuses():CharField
        {
            return _bonuses;
        }

        public function get k_damage():FloatField
        {
            return _k_damage;
        }

        public function get steps():IntegerField
        {
            return _steps;
        }

        public function get additional_data():CharField
        {
            return _additional_data;
        }

        public function get intellect_level():IntegerField
        {
            return _intellect_level;
        }

        public function get level_intellect_step():IntegerField
        {
            return _level_intellect_step;
        }

        public function get level_k_bonuse():FloatField
        {
            return _level_k_bonuse;
        }

        public function get level():IntegerField
        {
            return _level;
        }
    }
}
