/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.11.13
 * Time: 12:41
 */
package application.models.user.bonus
{
    import application.models.ModelData;
    import application.models.user.User;

    import flash.events.Event;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.service.Service;

    public class UserHp extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserHp()
        {
            super();
            _setupData();
            _updateData();
            _bindData();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:User;
        private var _hp:int = 0;
        private var _max_hp:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupData():void
        {
            _user = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        private function _updateData():void
        {
            _hp = _user.userFightData.hp.value + _user.userBonus.scale.hp.value;
            _max_hp = _user.userLevelParams.max_hp.value + _user.userBonus.scale.hp.value;
        }

        private function _bindData():void
        {
            _user.userFightData.addEventListener(Event.CHANGE, _changeHandler);
            _user.userBonus.addEventListener(Event.CHANGE, _changeHandler);
            _user.userLevelParams.addEventListener(Event.CHANGE, _changeHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _updateData();
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get hp():int
        {
            return _hp;
        }

        public function get max_hp():int
        {
            return _max_hp;
        }
    }
}
