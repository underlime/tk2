/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 14:54
 */
package application.models.user.bonus
{
    import application.models.ModelData;
    import application.models.bonus.ApplyBonus;
    import application.models.bonus.FightBonus;
    import application.models.bonus.ScaleBonus;

    import flash.events.Event;

    import framework.core.struct.data.DataContainer;

    public class UserBonus extends DataContainer
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserBonus()
        {
            super(ModelData.USER_BONUS);
            _fight.addEventListener(Event.CHANGE, _changeHandler);
            _scale.addEventListener(Event.CHANGE, _changeHandler);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['user_bonuses'];
        }

        override public function importData(data:Object):void
        {
            super.importData(data);
            trace("import bonuses");
        }

        override public function invokeDataImport(data:Object):void
        {
            super.invokeDataImport(data);
            trace("invoke bonuses");
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _fight:FightBonus = new FightBonus();
        private var _scale:ScaleBonus = new ScaleBonus();
        private var _apply:ApplyBonus = new ApplyBonus();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get fight():FightBonus
        {
            return _fight;
        }

        public function get scale():ScaleBonus
        {
            return _scale;
        }

        public function get apply():ApplyBonus
        {
            return _apply;
        }
    }
}
