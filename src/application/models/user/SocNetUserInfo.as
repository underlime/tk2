package application.models.user
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.ArrayField;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.data.fields.IntegerField;

    public class SocNetUserInfo extends Data
    {
        private var _first_name:CharField = new CharField();
        private var _last_name:CharField = new CharField();
        private var _sex:CharField = new CharField();
        private var _birth_date:DateTimeField = new DateTimeField();
        private var _city:IntegerField = new IntegerField();
        private var _city_name:CharField = new CharField();
        private var _country:IntegerField = new IntegerField();
        private var _country_name:CharField = new CharField();
        private var _timezone:CharField = new CharField();
        private var _picture:CharField = new CharField();
        private var _photo_medium:CharField = new CharField();
        private var _photo_big:CharField = new CharField();
        private var _link:CharField = new CharField();
        private var _friends:ArrayField = new ArrayField();

        public function SocNetUserInfo()
        {
            super(ModelData.SOC_NET_USER_INFO);
        }

        override public function getModelDataKeys():Array
        {
            return ["soc_net_user_info"];
        }

        public function get first_name():CharField
        {
            return _first_name;
        }

        public function get last_name():CharField
        {
            return _last_name;
        }

        public function get sex():CharField
        {
            return _sex;
        }

        public function get birth_date():DateTimeField
        {
            return _birth_date;
        }

        public function get city_name():CharField
        {
            return _city_name;
        }

        public function get country():IntegerField
        {
            return _country;
        }

        public function get country_name():CharField
        {
            return _country_name;
        }

        public function get timezone():CharField
        {
            return _timezone;
        }

        public function get picture():CharField
        {
            return _picture;
        }

        public function get photo_medium():CharField
        {
            return _photo_medium;
        }

        public function get photo_big():CharField
        {
            return _photo_big;
        }

        public function get link():CharField
        {
            return _link;
        }

        public function get friends():ArrayField
        {
            return _friends;
        }

        public function get city():IntegerField
        {
            return _city;
        }
    }

}
