/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 14:15
 */
package application.models.user
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.data.fields.IntegerField;

    public class UserStat extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserStat()
        {
            super(ModelData.USER_STAT);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['user_stat'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _visits:IntegerField = new IntegerField();
        private var _last_visit:DateTimeField = new DateTimeField();
        private var _ferros_payed:IntegerField = new IntegerField();
        private var _tomatos_payed:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get visits():IntegerField
        {
            return _visits;
        }

        public function get last_visit():DateTimeField
        {
            return _last_visit;
        }

        public function get ferros_payed():IntegerField
        {
            return _ferros_payed;
        }

        public function get tomatos_payed():IntegerField
        {
            return _tomatos_payed;
        }
    }
}
