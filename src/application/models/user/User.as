/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 15:12
 */
package application.models.user
{
    import application.models.ModelData;
    import application.models.clans.ClanInfo;
    import application.models.clans.UserClan;
    import application.models.user.achievements.Achievements;
    import application.models.user.bonus.UserBonus;
    import application.models.user.config.ConfigPrices;
    import application.models.user.config.GameConstants;
    import application.models.user.specials.Specials;
    import application.views.screen.clans.ClanUserRole;

    import framework.core.struct.data.DataContainer;

    public class User extends DataContainer implements IUserData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function User()
        {
            super(ModelData.USER);
            addChild(new UserInfo());
            addChild(new SocNetUserInfo());
            addChild(new UserLevelParams());
            addChild(new UserFightData());
            addChild(new UserOptInfo());
            addChild(new UserStat());
            addChild(new UserSettings());
            addChild(new UserBonus());
            addChild(new Achievements());
            addChild(new Specials());
            addChild(new ConfigPrices());
            addChild(new UserClan());
            addChild(new GameConstants());
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return [];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var showTutorial:Boolean = false;
        public var firstTime:Boolean = false;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function get userInfo():UserInfo
        {
            return getChildByName(ModelData.USER_INFO) as UserInfo;
        }

        public function get socNetUserInfo():SocNetUserInfo
        {
            return getChildByName(ModelData.SOC_NET_USER_INFO) as SocNetUserInfo;
        }

        public function get userFightData():UserFightData
        {
            return getChildByName(ModelData.USER_FIGHT_DATA) as UserFightData;
        }

        public function get userLevelParams():UserLevelParams
        {
            return getChildByName(ModelData.USER_LEVEL_PARAMS) as UserLevelParams;
        }

        public function get userOptInfo():UserOptInfo
        {
            return getChildByName(ModelData.USER_OPT_INFO) as UserOptInfo;
        }

        public function get userStat():UserStat
        {
            return getChildByName(ModelData.USER_STAT) as UserStat;
        }

        public function get userSettings():UserSettings
        {
            return getChildByName(ModelData.USER_SETTINGS) as UserSettings;
        }

        public function get userBonus():UserBonus
        {
            return getChildByName(ModelData.USER_BONUS) as UserBonus;
        }

        public function get userAchievements():Achievements
        {
            return getChildByName(ModelData.ACHIEVEMENTS) as Achievements;
        }

        public function get userSpecials():Specials
        {
            return getChildByName(ModelData.SPECIALS) as Specials;
        }

        public function get clanInfo():ClanInfo
        {
            return getChildByName(ModelData.CLAN_INFO) as ClanInfo;
        }

        public function get configPrices():ConfigPrices
        {
            return getChildByName(ModelData.CONFIG_PRICES) as ConfigPrices;
        }

        public function get gameConstants():GameConstants
        {
            return getChildByName(ModelData.GAME_CONSTANTS) as GameConstants;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get clanRole():ClanUserRole
        {
            var clanInfo:ClanInfo = clanInfo;
            var userInfo:UserInfo = userInfo;
            if (clanInfo.id.value > 0) {
                if (clanInfo.leader_id.value == userInfo.user_id.value)
                    return ClanUserRole.ADMIN;
                else
                    return ClanUserRole.MEMBER;
            }
            return ClanUserRole.GUEST;
        }

        public function get isClanLeader():Boolean
        {
            return userInfo.user_id.value == clanInfo.leader_id.value;
        }
    }
}
