/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 14:10
 */
package application.models.user
{
    import application.models.ModelData;

    import flash.events.Event;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.data.fields.IntegerField;

    public class UserFightData extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserFightData()
        {
            super(ModelData.USER_FIGHT_DATA);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['user_fight_data'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        /**
         * Обновить значение энергии, пересчитать время до восстановления
         * @param energy - новое значение
         * @param period - количество секунд за 1 единицу энергии
         */
        public function setEnergy(energy:int):void
        {
            _energy.value = energy;
            dispatchEvent(new Event(Event.CHANGE));
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _hp:IntegerField = new IntegerField();
        private var _glory:IntegerField = new IntegerField();
        private var _day_glory:IntegerField = new IntegerField();
        private var _week_glory:IntegerField = new IntegerField();
        private var _glory_date:DateTimeField = new DateTimeField();
        private var _energy:IntegerField = new IntegerField();
        private var _max_energy:IntegerField = new IntegerField();
        private var _energy_time:DateTimeField = new DateTimeField();
        private var _experience:IntegerField = new IntegerField();
        private var _ferros:IntegerField = new IntegerField();
        private var _tomatos:IntegerField = new IntegerField();
        private var _battles_count:IntegerField = new IntegerField();
        private var _wins_count:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get hp():IntegerField
        {
            return _hp;
        }

        public function get glory():IntegerField
        {
            return _glory;
        }

        public function get day_glory():IntegerField
        {
            return _day_glory;
        }

        public function get week_glory():IntegerField
        {
            return _week_glory;
        }

        public function get glory_date():DateTimeField
        {
            return _glory_date;
        }

        public function get energy():IntegerField
        {
            return _energy;
        }

        public function get max_energy():IntegerField
        {
            return _max_energy;
        }

        public function get energy_time():DateTimeField
        {
            return _energy_time;
        }

        public function get experience():IntegerField
        {
            return _experience;
        }

        public function get ferros():IntegerField
        {
            return _ferros;
        }

        public function get tomatos():IntegerField
        {
            return _tomatos;
        }

        public function get battles_count():IntegerField
        {
            return _battles_count;
        }

        public function get wins_count():IntegerField
        {
            return _wins_count;
        }
    }
}
