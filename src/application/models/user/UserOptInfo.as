/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 14:07
 */
package application.models.user
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.IntegerField;
    import framework.core.struct.data.fields.StructField;

    public class UserOptInfo extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserOptInfo()
        {
            super(ModelData.USER_OPT_INFO);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['user_opt_info'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _max_glory:IntegerField = new IntegerField();
        private var _level_exp_current:IntegerField = new IntegerField();
        private var _level_exp_previous:IntegerField = new IntegerField();
        private var _level_exp_next:IntegerField = new IntegerField();
        private var _hp_on_level:IntegerField = new IntegerField();
        private var _energy_on_level:IntegerField = new IntegerField();
        private var _points_learning_on_level:IntegerField = new IntegerField();
        private var _energy_period:IntegerField = new IntegerField();
        private var _energy_recover_ferros_price:IntegerField = new IntegerField();
        private var _energy_for_fight:IntegerField = new IntegerField();
        private var _unread_news_count:IntegerField = new IntegerField();
        private var _day_bonuses_config:StructField = new StructField();
        private var _start_bonuses_config:StructField = new StructField();
        private var _achievements_count:IntegerField = new IntegerField();
        private var _specials_count:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get max_glory():IntegerField
        {
            return _max_glory;
        }

        public function get level_exp_current():IntegerField
        {
            return _level_exp_current;
        }

        public function get level_exp_previous():IntegerField
        {
            return _level_exp_previous;
        }

        public function get level_exp_next():IntegerField
        {
            return _level_exp_next;
        }

        public function get hp_on_level():IntegerField
        {
            return _hp_on_level;
        }

        public function get energy_on_level():IntegerField
        {
            return _energy_on_level;
        }

        public function get points_learning_on_level():IntegerField
        {
            return _points_learning_on_level;
        }

        public function get energy_period():IntegerField
        {
            return _energy_period;
        }

        public function get energy_recover_ferros_price():IntegerField
        {
            return _energy_recover_ferros_price;
        }

        public function get energy_for_fight():IntegerField
        {
            return _energy_for_fight;
        }

        public function get day_bonuses_config():StructField
        {
            return _day_bonuses_config;
        }

        public function get start_bonuses_config():StructField
        {
            return _start_bonuses_config;
        }

        public function get unread_news_count():IntegerField
        {
            return _unread_news_count;
        }

        public function get achievements_count():IntegerField
        {
            return _achievements_count;
        }

        public function get specials_count():IntegerField
        {
            return _specials_count;
        }
    }
}
