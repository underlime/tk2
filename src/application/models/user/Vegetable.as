/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.06.13
 * Time: 14:33
 */
package application.models.user
{
    import framework.core.enum.BaseEnum;

    public class Vegetable extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        public static var ENUM:Vector.<Vegetable> = new Vector.<Vegetable>();

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOMATO:Vegetable = new Vegetable("tomato")
        public static const CUCUMBER:Vegetable = new Vegetable("cucumber");
        public static const BETA:Vegetable = new Vegetable("beta");
        public static const CARROT:Vegetable = new Vegetable("carrot");
        public static const ONION:Vegetable = new Vegetable("onion");
        public static const PEPPER:Vegetable = new Vegetable("pepper");
        public static const ALL:Vegetable = new Vegetable("all");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        public static function getEnumByKey(key:String):Vegetable
        {
            var count:int = ENUM.length;
            for (var i:int = 0; i < count; i++) {
                if (ENUM[i].toString() == key)
                    return ENUM[i];
            }
            throw new Error("Vegetable not found");
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Vegetable(num:String)
        {
            super(num);
            if (_lockUp)
                throw new Error("Enum has already inited")
            ENUM.push(this);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.models.user.Vegetable;

Vegetable.lockUp();