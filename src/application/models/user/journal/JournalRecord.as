/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.10.13
 * Time: 11:43
 */
package application.models.user.journal
{
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.data.fields.IntegerField;

    public class JournalRecord
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function JournalRecord()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _event_id:IntegerField = new IntegerField();
        private var _event_type:CharField = new CharField();
        private var _time:DateTimeField = new DateTimeField();
        private var _glory:IntegerField = new IntegerField();
        private var _experience:IntegerField = new IntegerField();
        private var _tomatos:IntegerField = new IntegerField();
        private var _ferros:IntegerField = new IntegerField();
        private var _level:IntegerField = new IntegerField();
        private var _description:CharField = new CharField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get event_id():IntegerField
        {
            return _event_id;
        }

        public function get event_type():CharField
        {
            return _event_type;
        }

        public function get time():DateTimeField
        {
            return _time;
        }

        public function get glory():IntegerField
        {
            return _glory;
        }

        public function get tomatos():IntegerField
        {
            return _tomatos;
        }

        public function get level():IntegerField
        {
            return _level;
        }

        public function get ferros():IntegerField
        {
            return _ferros;
        }

        public function get experience():IntegerField
        {
            return _experience;
        }

        public function get description():CharField
        {
            return _description;
        }
    }
}
