package application.models.user.journal
{
    import application.models.ModelData;

    import framework.core.struct.data.ListData;
    import framework.core.struct.data.fields.ArrayField;

    public class News extends ListData
    {
        public static const TYPE_COMMON:int = 0;
        public static const TYPE_START_BONUS:int = 1;
        public static const TYPE_FRIEND:int = 2;
        public static const REWARD_TOMATOS:String = "tomatos";
        public static const REWARD_FERROS:String = "ferros";

        private var _news_read_ids:ArrayField = new ArrayField();

        public function News()
        {
            _RecordClass = NewsRecord;
            super(ModelData.NEWS);
        }

        override public function getModelDataKeys():Array
        {
            return ["user_news"];
        }

        public function get news_read_ids():ArrayField
        {
            return _news_read_ids;
        }
    }
}
