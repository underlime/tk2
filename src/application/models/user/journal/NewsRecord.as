package application.models.user.journal
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.data.fields.IntegerField;

    public class NewsRecord extends Data
    {
        private var _id:IntegerField = new IntegerField();
        private var _time:DateTimeField = new DateTimeField();
        private var _news_type:IntegerField = new IntegerField();
        private var _additional_data:CharField = new CharField();
        private var _like_reward_type:CharField = new CharField();
        private var _like_reward_count:IntegerField = new IntegerField();
        private var _like_reward_data:CharField = new CharField();

        public function NewsRecord()
        {
            super("NewsRecord");
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get news_type():IntegerField
        {
            return _news_type;
        }

        public function get time():DateTimeField
        {
            return _time;
        }

        public function get additional_data():CharField
        {
            return _additional_data;
        }

        public function get like_reward_type():CharField
        {
            return _like_reward_type;
        }

        public function get like_reward_count():IntegerField
        {
            return _like_reward_count;
        }

        public function get like_reward_data():CharField
        {
            return _like_reward_data;
        }
    }
}
