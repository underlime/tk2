/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 13:15
 */
package application.models.user
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.data.fields.IntegerField;

    public class UserInfo extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserInfo()
        {
            super(ModelData.USER_INFO);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['user_info'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _soc_net_id:CharField = new CharField();
        private var _user_id:IntegerField = new IntegerField(0);
        private var _referrer:IntegerField = new IntegerField(0);
        private var _city:IntegerField = new IntegerField(0);
        private var _login:CharField = new CharField();
        private var _ban_level:IntegerField = new IntegerField(0);
        private var _ban_reason:CharField = new CharField();
        private var _vegetable:CharField = new CharField();
        private var _body:CharField = new CharField();
        private var _hands:CharField = new CharField();
        private var _eyes:CharField = new CharField();
        private var _mouth:CharField = new CharField();
        private var _hair:CharField = new CharField();
        private var _reg_time:DateTimeField = new DateTimeField();
        private var _was_friends_prizes_given:BooleanField = new BooleanField();
        private var _album_id:CharField = new CharField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get soc_net_id():CharField
        {
            return _soc_net_id;
        }

        public function get user_id():IntegerField
        {
            return _user_id;
        }

        public function get referrer():IntegerField
        {
            return _referrer;
        }

        public function get city():IntegerField
        {
            return _city;
        }

        public function get login():CharField
        {
            return _login;
        }

        public function get ban_level():IntegerField
        {
            return _ban_level;
        }

        public function get ban_reason():CharField
        {
            return _ban_reason;
        }

        public function get vegetable():CharField
        {
            return _vegetable;
        }

        public function get body():CharField
        {
            return _body;
        }

        public function get hands():CharField
        {
            return _hands;
        }

        public function get eyes():CharField
        {
            return _eyes;
        }

        public function get mouth():CharField
        {
            return _mouth;
        }

        public function get hair():CharField
        {
            return _hair;
        }

        public function get reg_time():DateTimeField
        {
            return _reg_time;
        }

        public function get was_friends_prizes_given():BooleanField
        {
            return _was_friends_prizes_given;
        }

        public function get album_id():CharField
        {
            return _album_id;
        }
    }
}
