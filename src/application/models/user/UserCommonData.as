/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.08.13
 * Time: 13:27
 */
package application.models.user
{
    public class UserCommonData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var soc_net_id:String = "";
        public var user_id:int = 0;
        public var level:int = 0;
        public var hp:int = 0;
        public var maxHp:int = 0;
        public var maxLevel:int = 0;
        public var login:String = "";

        public var glory:int = 0;
        public var strength:int = 0;
        public var agility:int = 0;
        public var intellect:int = 0;

        public var picture:String = "";

        public var bonus_hp:int = 0;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function importData(userInfo:UserInfo, socNetUserInfo:SocNetUserInfo, userFightData:UserFightData, userLevelParams:UserLevelParams):void
        {
            soc_net_id = userInfo.soc_net_id.value;
            user_id = userInfo.user_id.value;
            level = userLevelParams.level.value;
            hp = userFightData.hp.value;
            login = userInfo.login.value;
            glory = userFightData.glory.value;
            strength = userLevelParams.strength.value;
            agility = userLevelParams.agility.value;
            intellect = userLevelParams.intellect.value;
            picture = socNetUserInfo.picture.value;
            maxHp = userLevelParams.max_hp.value;
            maxLevel = userLevelParams.max_level.value;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
