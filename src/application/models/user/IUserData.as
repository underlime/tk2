/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.08.13
 * Time: 12:05
 */
package application.models.user
{
    import application.models.clans.ClanInfo;
    import application.models.user.achievements.Achievements;
    import application.models.user.bonus.UserBonus;
    import application.models.user.specials.Specials;

    public interface IUserData
    {
        function get userInfo():UserInfo;

        function get socNetUserInfo():SocNetUserInfo;

        function get userFightData():UserFightData;

        function get userLevelParams():UserLevelParams;

        function get userOptInfo():UserOptInfo;

        function get userStat():UserStat;

        function get userSettings():UserSettings;

        function get userBonus():UserBonus;

        function get userAchievements():Achievements;

        function get userSpecials():Specials;

        function get clanInfo():ClanInfo;
    }
}
