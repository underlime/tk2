/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 14:18
 */
package application.models.user
{
    import application.models.ModelData;

    import flash.display.StageQuality;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;

    public class UserSettings extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserSettings()
        {
            super(ModelData.USER_SETTINGS);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['user_settings'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _blood_enabled:BooleanField = new BooleanField();
        private var _blood_color:CharField = new CharField();
        private var _cartoon_stroke:BooleanField = new BooleanField();
        private var _sounds_enabled:BooleanField = new BooleanField();
        private var _graphics_quality:CharField = new CharField(StageQuality.HIGH);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get blood_enabled():BooleanField
        {
            return _blood_enabled;
        }

        public function get blood_color():CharField
        {
            return _blood_color;
        }

        public function get cartoon_stroke():BooleanField
        {
            return _cartoon_stroke;
        }

        public function get sounds_enabled():BooleanField
        {
            return _sounds_enabled;
        }

        public function get graphics_quality():CharField
        {
            return _graphics_quality;
        }
    }
}
