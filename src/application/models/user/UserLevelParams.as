/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 14:01
 */
package application.models.user
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.IntegerField;

    public class UserLevelParams extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserLevelParams()
        {
            super(ModelData.USER_LEVEL_PARAMS);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['user_level_params'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _strength:IntegerField = new IntegerField();
        private var _agility:IntegerField = new IntegerField();
        private var _intellect:IntegerField = new IntegerField();
        private var _max_hp:IntegerField = new IntegerField();
        private var _level:IntegerField = new IntegerField();
        private var _max_level:IntegerField = new IntegerField();
        private var _points_learning:IntegerField = new IntegerField();
        private var _items_slots_avaible:IntegerField = new IntegerField();
        private var _social_slots_avaible:IntegerField = new IntegerField();
        private var _ring_slots_avaible:IntegerField = new IntegerField();
        private var _skill_resets_count:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get strength():IntegerField
        {
            return _strength;
        }

        public function get agility():IntegerField
        {
            return _agility;
        }

        public function get intellect():IntegerField
        {
            return _intellect;
        }

        public function get max_hp():IntegerField
        {
            return _max_hp;
        }

        public function get level():IntegerField
        {
            return _level;
        }

        public function get max_level():IntegerField
        {
            return _max_level;
        }

        public function get points_learning():IntegerField
        {
            return _points_learning;
        }

        public function get items_slots_avaible():IntegerField
        {
            return _items_slots_avaible;
        }

        public function get social_slots_avaible():IntegerField
        {
            return _social_slots_avaible;
        }

        public function get skill_resets_count():IntegerField
        {
            return _skill_resets_count;
        }

        public function get ring_slots_avaible():IntegerField
        {
            return _ring_slots_avaible;
        }
    }
}
