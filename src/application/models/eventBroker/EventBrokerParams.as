package application.models.eventBroker
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class EventBrokerParams extends Data
    {
        private var _host:CharField = new CharField();
        private var _port:IntegerField = new IntegerField();
        private var _channel:CharField = new CharField();

        public function EventBrokerParams()
        {
            super(ModelData.EVENT_BROKER_PARAMS);
        }

        public function get port():IntegerField
        {
            return _port;
        }

        public function get host():CharField
        {
            return _host;
        }

        public function get channel():CharField
        {
            return _channel;
        }

        override public function getModelDataKeys():Array
        {
            return ["tk_event_broker"];
        }
    }
}
