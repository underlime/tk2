package application.models.bank
{
    import application.models.ModelData;

    import framework.core.struct.data.ListData;

    public class Bank extends ListData
    {
        public function Bank()
        {
            _RecordClass = BankRecord;
            super(ModelData.BANK_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['currency_rates'];
        }
    }
}
