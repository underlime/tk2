package application.models.bank
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class BankRecord extends Data
    {
        private var _id:IntegerField = new IntegerField();
        private var _currency:CharField = new CharField();
        private var _count:IntegerField = new IntegerField();
        private var _price:IntegerField = new IntegerField();
        private var _best_price:BooleanField = new BooleanField();

        public function BankRecord()
        {
            super("BankRecord");
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get currency():CharField
        {
            return _currency;
        }

        public function get count():IntegerField
        {
            return _count;
        }

        public function get price():IntegerField
        {
            return _price;
        }

        public function get best_price():BooleanField
        {
            return _best_price;
        }
    }
}
