/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 13:14
 */
package application.models
{
    public class ModelData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const USER:String = "UserModel";
        public static const USER_INFO:String = "UserInfoModel";
        public static const USER_LEVEL_PARAMS:String = "UserLevelParamsModel";
        public static const USER_OPT_INFO:String = "UserOptInfo";
        public static const USER_FIGHT_DATA:String = "UserFightData";
        public static const USER_STAT:String = "UserStat";
        public static const USER_SETTINGS:String = "UserSettings";
        public static const USER_BONUS:String = "UserBonus";

        public static const FIGHT_BONUS:String = "FightBonus";
        public static const SCALE_BONUS:String = "ScaleBonus";
        public static const APPLY_BONUS:String = "ApplyBonus";

        public static const INVENTORY:String = "Inventory";

        public static const MARKET_ITEMS:String = "MarketItems";
        public static const CHAR_LIST:String = "CharListModel";
        public static const TOP_MODEL:String = "TopModelData";
        public static const CONFIG_PRICES:String = "ConfigPricesModel";
        public static const VEGETABLE_PRICE:String = "VegetablePrice";
        public static const RANKS:String = "RanksModel";

        public static const BATTLE_MODEL:String = "BattleModel";
        public static const BATTLE_PROFIT:String = "BattleProfit";
        public static const BATTLE_ARENA:String = "BattleArena";
        public static const BATTLE_STEPS:String = "BattleStepsModel";
        public static const BATTLE_STEP_RECORD:String = "BattleStepRecord";
        public static const BATTLE_SPECIAL_DATA:String = "BattleSpecialData";
        public static const BATTLE_ITEM_DATA:String = "BattleItemData";
        public static const SPECIAL_RECORD:String = "SpecialRecordModel";
        public static const SPECIALS:String = "SpecialsModel";
        public static const BATTLE_SPECIAL_STEPS:String = "SpecialStepsModel";
        public static const ENEMY_RECORD:String = "EnemyRecordModel";
        public static const ENEMIES_LIST:String = "EnemiesListModel";
        public static const ACHIEVEMENTS:String = "AchievementsModel";
        public static const OPPONENTS_LIST:String = "OpponentsList";
        public static const OPPONENT_DATA:String = "OpponentData";
        public static const EVENTS:String = "EventsModel";
        public static const USER_JOURNAL:String = "UserJournal";
        public static const NEWS:String = "News";
        public static const SOC_NET_USER_INFO:String = "SocNetUserInfo";
        public static const BANK_MODEL:String = "Bank";
        public static const GMO_PRICES:String = "GmoPrices";
        public static const GMO_GAINS:String = "GmoGains";
        public static const FORGE_PARAMS:String = "ForgeParams";
        public static const CLAN_INFO:String = "ClanInfoModel";
        public static const GAME_CONSTANTS:String = "GameConstants";
        public static const CLAN_MEMBER:String = "CLanMemberModel";
        public static const CLAN_REQUESTS:String = "ClanRequests";
        public static const CLANS_TOP:String = "ClansTopModel";
        public static const CLAN_LIST:String = "ClanListModel";

        public static const EVENT_BROKER_PARAMS:String = "EventBrokerParams";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
