/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 14:47
 */
package application.models.bonus
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.FloatField;
    import framework.core.struct.data.fields.IntegerField;

    public class FightBonus extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FightBonus()
        {
            super(ModelData.FIGHT_BONUS);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['fight'];
        }

        override public function importData(data:Object):void
        {
            super.importData(data);
            trace("import fight bonus");
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _damage:IntegerField = new IntegerField();
        private var _armor:IntegerField = new IntegerField();
        private var _speed:IntegerField = new IntegerField();
        private var _act_again:FloatField = new FloatField();
        private var _dodge:FloatField = new FloatField();
        private var _crit:FloatField = new FloatField();
        private var _block:FloatField = new FloatField();
        private var _special:FloatField = new FloatField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get damage():IntegerField
        {
            return _damage;
        }

        public function get armor():IntegerField
        {
            return _armor;
        }

        public function get speed():IntegerField
        {
            return _speed;
        }

        public function get act_again():FloatField
        {
            return _act_again;
        }

        public function get dodge():FloatField
        {
            return _dodge;
        }

        public function get crit():FloatField
        {
            return _crit;
        }

        public function get block():FloatField
        {
            return _block;
        }

        public function get special():FloatField
        {
            return _special;
        }
    }
}
