/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.06.13
 * Time: 11:02
 */
package application.models.bonus
{
    public class Bonus
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Bonus(data:XML)
        {
            _data = data;
            _setupData();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _data:XML;
        private var _damage:int = 0;
        private var _armor:int = 0;
        private var _speed:int = 0;
        private var _hp:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupData():void
        {
            _damage = int(_data.fight.damage.toString());
            _armor = int(_data.fight.armor.toString());
            _speed = int(_data.fight.speed.toString());
            _hp = int(_data.scale.hp.toString());
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get damage():int
        {
            return _damage;
        }

        public function get armor():int
        {
            return _armor;
        }

        public function get speed():int
        {
            return _speed;
        }

        public function get hp():int
        {
            return _hp;
        }
    }
}
