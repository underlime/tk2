package application.models.gmo
{
    import application.models.ModelData;
    import application.views.screen.gmo.GmoType;

    import framework.core.helpers.MathHelper;
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.StructField;

    public class GmoPrices extends Data
    {
        public static const PERIODS:Array = [1, 6, 24, 48];

        private var _experience:StructField = new StructField();
        private var _glory:StructField = new StructField();
        private var _energy:StructField = new StructField();
        private var _damage:StructField = new StructField();
        private var _armor:StructField = new StructField();
        private var _fight_tomatos:StructField = new StructField();

        private var _isEmpty:Boolean = true;

        public function GmoPrices()
        {
            super(ModelData.GMO_PRICES);
        }

        override public function getModelDataKeys():Array
        {
            return ["citadel_prices"];
        }

        override public function importData(data:Object):void
        {
            super.importData(data);
            _isEmpty = false;
        }

        public function getExperiencePrice(period:int):int
        {
            var index:int = MathHelper.clamp(period, 0, 3);
            var key:String = PERIODS[index].toString();
            if (_experience.value[key])
                return _experience.value[key];
            throw new Error("Price not found! Period " + period);
        }

        public function getGloryPrice(period:int):int
        {
            var index:int = MathHelper.clamp(period, 0, 3);
            var key:String = PERIODS[index].toString();
            if (_glory.value[key])
                return _glory.value[key];
            throw new Error("Price not found! Period " + period);
        }

        public function getEnergyPrice(period:int):int
        {
            var index:int = MathHelper.clamp(period, 0, 3);
            var key:String = PERIODS[index].toString();
            if (_energy.value[key])
                return _energy.value[key];
            throw new Error("Price not found! Period " + period);
        }

        public function getDamagePrice(period:int):int
        {
            var index:int = MathHelper.clamp(period, 0, 3);
            var key:String = PERIODS[index].toString();
            if (_damage.value[key])
                return _damage.value[key];
            throw new Error("Price not found! Period " + period);
        }

        public function getArmorPrice(period:int):int
        {
            var index:int = MathHelper.clamp(period, 0, 3);
            var key:String = PERIODS[index].toString();
            if (_armor.value[key])
                return _armor.value[key];
            throw new Error("Price not found! Period " + period);
        }

        public function getTomatosPrice(period:int):int
        {
            var index:int = MathHelper.clamp(period, 0, 3);
            var key:String = PERIODS[index].toString();
            if (_fight_tomatos.value[key])
                return _fight_tomatos.value[key];
            throw new Error("Price not found! Period " + period);
        }

        public function getGmoPrice(type:GmoType, period:int):int
        {
            switch (type) {
                case GmoType.ARMOR:
                    return getArmorPrice(period);
                case GmoType.DAMAGE:
                    return getDamagePrice(period);
                case GmoType.TOMATOS:
                    return getTomatosPrice(period);
                case GmoType.EXPERIENCE:
                    return getExperiencePrice(period);
                case GmoType.GLORY:
                    return getGloryPrice(period);
                case GmoType.ENERGY:
                    return getEnergyPrice(period);
            }
            throw new Error("Undefined gmo type!");
        }

        public function get experience():StructField
        {
            return _experience;
        }

        public function get glory():StructField
        {
            return _glory;
        }

        public function get energy():StructField
        {
            return _energy;
        }

        public function get damage():StructField
        {
            return _damage;
        }

        public function get armor():StructField
        {
            return _armor;
        }

        public function get fight_tomatos():StructField
        {
            return _fight_tomatos;
        }

        public function get isEmpty():Boolean
        {
            return _isEmpty;
        }
    }
}
