/**
 * Created by acvetkov on 06.12.13.
 */
package application.models.gmo
{
    public class GmoTimeHelper
    {
        public static function getTimeDiff(dateFrom:Date, dateTo:Date):Number
        {
            return dateTo.getTime() - dateFrom.getTime();
        }

        public static function validateDate(dateTo:Date):Boolean
        {
            var diff:Number = getTimeDiff(new Date(), dateTo);
            var hours:Number = (diff / 1000) / 3600;
            return hours <= _getMaxHours();
        }

        public static function getRemainTime(dateTo:Date):BuffTime
        {
            var time:BuffTime = new BuffTime();
            var diff:Number = getTimeDiff(new Date(), dateTo);

            time.hours = _getHours(diff);
            var remain:Number = time.hours * 3600 * 100;
            time.minutes = _getMinutes(diff - remain);

            return time;
        }

        public static function getRemainSeconds(dateTo:Date):Number
        {
            var diff:Number = getTimeDiff(new Date(), dateTo);
            return Math.ceil(diff / 1000);
        }

        private static function _getHours(miliseconds:Number):Number
        {
            return Math.floor((miliseconds / 1000) / 3600);
        }

        private static function _getMinutes(millisecond:Number):Number
        {
            var seconds:Number = millisecond / 1000;
            if (seconds < 60 && seconds > 0)
                return 1;

            return Math.floor(seconds / 60);
        }

        private static function _getMaxHours():Number
        {
            var size:int = GmoPrices.PERIODS.length;
            return GmoPrices.PERIODS[size - 1];
        }
    }
}
