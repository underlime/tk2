/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.12.13
 * Time: 20:06
 */
package application.models.gmo
{
    import application.config.AppConfig;
    import application.views.screen.gmo.GmoType;

    import flash.utils.ByteArray;

    import framework.core.enum.Language;

    public class GmoHints
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoHints()
        {
            super();
            _parseJson();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getHint(gmoType:GmoType):String
        {
            var lanKey:String = AppConfig.LANGUAGE == Language.RU ? "ru" : "en";
            if (_data[lanKey] && _data[lanKey][gmoType.toString()])
                return _data[lanKey][gmoType.toString()];
            return "";
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../lib/config/gmo.json", mimeType="application/octet-stream")]
        private var GmoClass:Class;
        private var _data:Object;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _parseJson():void
        {
            var bytes:ByteArray = new GmoClass();
            var json:String = bytes.readUTFBytes(bytes.length);
            _data = JSON.parse(json);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
