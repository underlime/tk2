/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.12.13
 * Time: 21:07
 */
package application.models.gmo
{
    import application.models.ModelData;
    import application.views.screen.gmo.GmoType;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.DateTimeField;

    public class GmoGains extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoGains()
        {
            super(ModelData.GMO_GAINS);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["gains"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getActiveGains():Array
        {
            var gains:Array = [];
            if (isEnabled(GmoType.ARMOR))
                gains.push(GmoType.ARMOR);

            if (isEnabled(GmoType.DAMAGE))
                gains.push(GmoType.DAMAGE);

            if (isEnabled(GmoType.TOMATOS))
                gains.push(GmoType.TOMATOS);

            if (isEnabled(GmoType.EXPERIENCE))
                gains.push(GmoType.EXPERIENCE);

            if (isEnabled(GmoType.ENERGY))
                gains.push(GmoType.ENERGY);

            if (isEnabled(GmoType.GLORY))
                gains.push(GmoType.GLORY);
            return gains;
        }

        public function getBuffTIme(gmoType:GmoType):BuffTime
        {
            var dateField:DateTimeField = _getDateField(gmoType);
            return GmoTimeHelper.getRemainTime(dateField.value);
        }

        public function getBuffSeconds(gmoType:GmoType):Number
        {
            var dateField:DateTimeField = _getDateField(gmoType);
            return GmoTimeHelper.getRemainSeconds(dateField.value);
        }

        public function isEnabled(type:GmoType):Boolean
        {
            var dateField:DateTimeField = _getDateField(type);

            if (dateField.value) {
                var timeDiff:Number = GmoTimeHelper.getTimeDiff(new Date(), dateField.value);
                return timeDiff > 0;
            }

            return false;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _experience:DateTimeField = new DateTimeField();
        private var _glory:DateTimeField = new DateTimeField();
        private var _energy:DateTimeField = new DateTimeField();
        private var _damage:DateTimeField = new DateTimeField();
        private var _armor:DateTimeField = new DateTimeField();
        private var _fight_tomatos:DateTimeField = new DateTimeField();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getDateField(gmoType:GmoType):DateTimeField
        {
            switch (gmoType) {
                case GmoType.ARMOR:
                    return _armor;
                    break;
                case GmoType.DAMAGE:
                    return _damage;
                    break;
                case GmoType.TOMATOS:
                    return _fight_tomatos;
                    break;
                case GmoType.EXPERIENCE:
                    return _experience;
                    break;
                case GmoType.GLORY:
                    return _glory;
                    break;
                case GmoType.ENERGY:
                    return _energy;
                    break;
            }
            throw new Error("Field not found!");
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get experience():DateTimeField
        {
            return _experience;
        }

        public function get glory():DateTimeField
        {
            return _glory;
        }

        public function get energy():DateTimeField
        {
            return _energy;
        }

        public function get damage():DateTimeField
        {
            return _damage;
        }

        public function get armor():DateTimeField
        {
            return _armor;
        }

        public function get fight_tomatos():DateTimeField
        {
            return _fight_tomatos;
        }
    }
}
