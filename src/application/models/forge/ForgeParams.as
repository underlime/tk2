/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.12.13
 * Time: 11:55
 */
package application.models.forge
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.FloatField;
    import framework.core.struct.data.fields.IntegerField;

    public class ForgeParams extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ForgeParams()
        {
            super(ModelData.FORGE_PARAMS);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["sharpening_params"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _damage:IntegerField = new IntegerField();
        private var _armor:IntegerField = new IntegerField();
        private var _price_ferros:FloatField = new FloatField();
        private var _price_tomatos:FloatField = new FloatField();
        private var _success_probability:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get damage():IntegerField
        {
            return _damage;
        }

        public function get price_ferros():FloatField
        {
            return _price_ferros;
        }

        public function get price_tomatos():FloatField
        {
            return _price_tomatos;
        }

        public function get success_probability():IntegerField
        {
            return _success_probability;
        }

        public function get armor():IntegerField
        {
            return _armor;
        }
    }
}
