/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.06.13
 * Time: 11:28
 */
package application.models.requirements
{
    public class Requirements
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Requirements(data:XML)
        {
            _data = data;
            _setupData();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _data:XML;
        private var _strength:int = 0;
        private var _agility:int = 0;
        private var _intellect:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupData():void
        {
            _strength = int(_data.user_data.strength.toString());
            _agility = int(_data.user_data.agility.toString());
            _intellect = int(_data.user_data.intellect.toString());
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get strength():int
        {
            return _strength;
        }

        public function get agility():int
        {
            return _agility;
        }

        public function get intellect():int
        {
            return _intellect;
        }
    }
}
