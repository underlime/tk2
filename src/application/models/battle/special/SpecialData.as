/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 21.07.13
 * Time: 16:25
 */
package application.models.battle.special
{
    import application.models.ModelData;
    import application.models.user.specials.SpecialRecord;

    import framework.core.struct.data.Data;

    public class SpecialData extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialData()
        {
            super(ModelData.BATTLE_SPECIAL_DATA);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _spec_info:SpecialRecord = new SpecialRecord();
        private var _spec_steps:SpecialSteps = new SpecialSteps();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get spec_steps():SpecialSteps
        {
            return _spec_steps;
        }

        public function get spec_info():SpecialRecord
        {
            return _spec_info;
        }
    }
}
