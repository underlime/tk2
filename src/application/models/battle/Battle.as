/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 21.07.13
 * Time: 15:14
 */
package application.models.battle
{
    import application.models.ModelData;
    import application.models.battle.enemy.EnemyFightData;
    import application.models.battle.enemy.EnemyInfo;
    import application.models.battle.enemy.EnemyInventory;
    import application.models.battle.enemy.EnemyLevelParams;
    import application.models.battle.enemy.SocNetEnemyInfo;

    import framework.core.struct.data.DataContainer;
    import framework.core.struct.data.fields.FloatField;

    public class Battle extends DataContainer
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Battle()
        {
            super(ModelData.BATTLE_MODEL);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["battle_data"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _enemy_info:EnemyInfo = new EnemyInfo();
        private var _soc_net_enemy_info:SocNetEnemyInfo = new SocNetEnemyInfo();
        private var _enemy_inventory:EnemyInventory = new EnemyInventory();
        private var _enemy_fight_data:EnemyFightData = new EnemyFightData();
        private var _enemy_level_params:EnemyLevelParams = new EnemyLevelParams();
        private var _profit:BattleProfit = new BattleProfit();
        private var _arena_data:BattleArena = new BattleArena();
        private var _battle_steps:BattleSteps = new BattleSteps();
        private var _wins:FloatField = new FloatField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get enemy_info():EnemyInfo
        {
            return _enemy_info;
        }

        public function get enemy_inventory():EnemyInventory
        {
            return _enemy_inventory;
        }

        public function get profit():BattleProfit
        {
            return _profit;
        }

        public function get arena_data():BattleArena
        {
            return _arena_data;
        }

        public function get battle_steps():BattleSteps
        {
            return _battle_steps;
        }

        public function get wins():FloatField
        {
            return _wins;
        }

        public function get enemy_level_params():EnemyLevelParams
        {
            return _enemy_level_params;
        }

        public function get enemy_fight_data():EnemyFightData
        {
            return _enemy_fight_data;
        }

        public function get soc_net_enemy_info():SocNetEnemyInfo
        {
            return _soc_net_enemy_info;
        }
    }
}
