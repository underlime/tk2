package application.models.battle.enemy
{
    import application.models.user.SocNetUserInfo;

    public class SocNetEnemyInfo extends SocNetUserInfo
    {
        public function SocNetEnemyInfo()
        {
            super();
        }

        override public function getModelDataKeys():Array
        {
            return ["soc_net_enemy_info"];
        }
    }
}
