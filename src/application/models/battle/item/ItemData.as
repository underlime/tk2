/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 21.07.13
 * Time: 16:42
 */
package application.models.battle.item
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.FloatField;

    public class ItemData extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ItemData()
        {
            super(ModelData.BATTLE_ITEM_DATA);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function importData(data:Object):void
        {
            super.importData(data);
            _empty = false;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _picture:CharField = new CharField();
        private var _heal:FloatField = new FloatField();

        private var _empty:Boolean = true;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get picture():CharField
        {
            return _picture;
        }

        public function get heal():FloatField
        {
            return _heal;
        }

        public function get empty():Boolean
        {
            return _empty;
        }
    }
}
