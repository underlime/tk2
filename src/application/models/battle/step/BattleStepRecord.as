/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 21.07.13
 * Time: 15:38
 */
package application.models.battle.step
{
    import application.models.battle.item.ItemData;
    import application.models.battle.special.SpecialData;

    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.FloatField;
    import framework.core.struct.data.fields.IntegerField;

    public class BattleStepRecord
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleStepRecord()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _who:IntegerField = new IntegerField();
        private var _who_role:CharField = new CharField();
        private var _type:CharField = new CharField();
        private var _damage:FloatField = new FloatField();
        private var _special:BooleanField = new BooleanField();
        private var _user_hp:FloatField = new FloatField();
        private var _enemy_hp:FloatField = new FloatField();
        private var _crit:BooleanField = new BooleanField();
        private var _block:BooleanField = new BooleanField();
        private var _dodge:BooleanField = new BooleanField();
        private var _act_again:BooleanField = new BooleanField();

        private var _spec_data:SpecialData = new SpecialData();
        private var _item_data:ItemData = new ItemData();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get who():IntegerField
        {
            return _who;
        }

        public function get who_role():CharField
        {
            return _who_role;
        }

        public function get type():CharField
        {
            return _type;
        }

        public function get damage():FloatField
        {
            return _damage;
        }

        public function get special():BooleanField
        {
            return _special;
        }

        public function get user_hp():FloatField
        {
            return _user_hp;
        }

        public function get enemy_hp():FloatField
        {
            return _enemy_hp;
        }

        public function get crit():BooleanField
        {
            return _crit;
        }

        public function get block():BooleanField
        {
            return _block;
        }

        public function get dodge():BooleanField
        {
            return _dodge;
        }

        public function get act_again():BooleanField
        {
            return _act_again;
        }

        public function get spec_data():SpecialData
        {
            return _spec_data;
        }

        public function get item_data():ItemData
        {
            return _item_data;
        }
    }
}
