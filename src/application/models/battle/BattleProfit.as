/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 21.07.13
 * Time: 15:21
 */
package application.models.battle
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.IntegerField;

    public class BattleProfit extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleProfit()
        {
            super(ModelData.BATTLE_PROFIT);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["profit"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _energy:IntegerField = new IntegerField();
        private var _glory:IntegerField = new IntegerField();
        private var _experience:IntegerField = new IntegerField();
        private var _tomatos:IntegerField = new IntegerField();
        private var _level:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get energy():IntegerField
        {
            return _energy;
        }

        public function get glory():IntegerField
        {
            return _glory;
        }

        public function get experience():IntegerField
        {
            return _experience;
        }

        public function get tomatos():IntegerField
        {
            return _tomatos;
        }

        public function get level():IntegerField
        {
            return _level;
        }
    }
}
