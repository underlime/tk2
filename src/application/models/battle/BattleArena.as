/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 21.07.13
 * Time: 15:24
 */
package application.models.battle
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;

    public class BattleArena extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleArena()
        {
            super(ModelData.BATTLE_ARENA);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _image:CharField = new CharField();
        private var _name_ru:CharField = new CharField();
        private var _name_en:CharField = new CharField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get image():CharField
        {
            return _image;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get name_en():CharField
        {
            return _name_en;
        }
    }
}
