/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 15:09
 */
package application.models.inventory
{
    import application.common.ArrangementVariant;
    import application.models.ModelData;
    import application.views.constructor.helpers.DressHelper;

    import framework.core.struct.data.ListData;

    public class Inventory extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Inventory()
        {
            _RecordClass = InventoryItem;
            super(ModelData.INVENTORY);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['user_inventory'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        /**
         * Получить список всех элементов инвентаря
         * @return
         */
        public function getItems():Array
        {
            return this.search({});
        }

        /**
         * Получить список неиспользуемых врменных предметов предметов (item.count > item.temp_put)
         * @return
         */
        public function getNotUseInTempItems():Array
        {
            var items:Array = getItems();
            var result:Array = [];

            var count:int = items.length;
            for (var i:int = 0; i < count; i++) {
                var item:InventoryItem = items[i] as InventoryItem;
                if (item.count.value - item.temp_put.value > 0)
                    result.push(item);
            }

            return result;
        }

        /**
         * Сбросить данные о временной экипировке
         */
        public function flush():void
        {
            var list:Object = super.list.value;
            for (var key:String in list) {
                var item:InventoryItem = list[key] as InventoryItem;
                if (item.temp_put.value > 0)
                    item.temp_put.value = 0;
            }
        }

        /**
         * Получить список расходников
         * @return
         */
        public function getConsumeItems():Array
        {
            var result:Array = [];
            var items:Object = list.value;

            for (var key:String in items) {
                var item:InventoryItem = items[key];
                if (item.put.value > 0 && item.isConsume) {
                    var count:int = item.put.value;
                    for (var j:int = 0; j < count; j++) {
                        result.push(item);
                    }
                }
            }

            return result;
        }

        /**
         * Список предметов в боевых слотах
         * @return
         */
        public function getPutItems():Array
        {
            var result:Array = [];
            var items:Object = list.value;
            for (var key:String in items) {
                var item:InventoryItem = items[key];
                if (item.put.value > 0 && !item.isConsume)
                    result.push(item);
            }

            return result;
        }

        /**
         * Список предметов в социальных слотах
         * @return
         */
        public function getSocialPutItems():Array
        {
            var result:Array = [];
            var items:Object = list.value;
            for (var key:String in items) {
                var item:InventoryItem = items[key];
                if (item.social_put.value > 0 && !item.isConsume)
                    result.push(item);
            }

            return result;
        }

        /**
         * Список предметов для экипировки персонажа
         * (смесь боевых и социальных слотов)
         * @return
         */
        public function getBuildingItems():Array
        {
            var putItems:Array = getPutItems();
            var socialItems:Array = getSocialPutItems();

            return DressHelper.getBuildItems(putItems, socialItems);
        }

        /**
         * Получить список всего оружия и защиты
         * @return
         */
        public function getArmorAndWeapon(start:int):Array
        {
            var items:Array = getItems();
            var needItems:Array = [];
            var av:Array = [ArrangementVariant.ROD_WEAPON, ArrangementVariant.ONE_HANDED, ArrangementVariant.GLOVES, ArrangementVariant.TWO_HANDED, ArrangementVariant.SHIELD, ArrangementVariant.ARMOR];
            var count:int = items.length;
            var skip:int = 0;
            for (var i:int = 0; i < count; i++) {
                var item:InventoryItem = items[i];
                var arrVariant:ArrangementVariant = ArrangementVariant.getEnumByKey(item.arrangement_variant.value);
                if (av.indexOf(arrVariant) > -1) {
                    if (skip >= start)
                        needItems.push(item);
                    skip++;
                }
            }

            return needItems;
        }


        public function getAllItems(start:int, limit:int):Array
        {
            var items:Array = [];
            var list:Object = list.value;
            for (var key:String in list) {
                var item:InventoryItem = list[key];
                if (item.count.value - item.temp_put.value > 0) {
                    items.push(item);
                }
            }

            return items.slice(start, start + limit);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
