/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.07.13
 * Time: 10:39
 */
package application.models.inventory
{
    import application.models.market.Item;

    public class InventoryHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const MAX_LEVEL:int = 17;

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function ItemsArray2Vector(items:Array):Vector.<Item>
        {
            var result:Vector.<Item> = new Vector.<Item>();
            var count:int = items.length;

            for (var i:int = 0; i < count; i++) {
                if (items[i] is Item)
                    result.push(items[i]);
            }

            return result;
        }

        public static function canEquip(item:Item, userLevel:int):Boolean
        {
            return (userLevel >= item.required_level.value);
        }

        /**
         * Преобразует массив индентфикаторов предметов в ассоциативный масиив
         * @param items Массив id-идентификаторов предметов
         * @return Массив объектов ["item_id" = "count", ..]
         */
        public static function getItemIdCount(items:Array):Array
        {
            var result:Array = [];
            var count:int = items.length;

            for (var i:int = 0; i < count; i++) {
                var item_id:int = items[i];

                if (result[item_id])
                    result[item_id]++;
                else
                    result[item_id] = 1;
            }

            return result;
        }

        public static function canUpgrade(item:InventoryItem):Boolean
        {
            return item.sharpening_level.value < MAX_LEVEL;
        }

        public static function getNextLevel(item:InventoryItem):int
        {
            if (canUpgrade(item))
                return item.sharpening_level.value + 1;
            throw new Error("This item reached max level");
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
