/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 14:58
 */
package application.models.inventory
{
    import application.models.market.Item;

    import framework.core.struct.data.fields.IntegerField;

    public class InventoryItem extends Item
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InventoryItem()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        /**
         * Количество временно экипированных
         */
        public var temp_put:IntegerField = new IntegerField(0);

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _count:IntegerField = new IntegerField();
        private var _broken_count:IntegerField = new IntegerField();
        private var _put:IntegerField = new IntegerField();
        private var _social_put:IntegerField = new IntegerField();
        private var _sharpening_level:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get count():IntegerField
        {
            return _count;
        }

        public function get broken_count():IntegerField
        {
            return _broken_count;
        }

        public function get put():IntegerField
        {
            return _put;
        }

        public function get social_put():IntegerField
        {
            return _social_put;
        }

        public function get sharpening_level():IntegerField
        {
            return _sharpening_level;
        }
    }
}
