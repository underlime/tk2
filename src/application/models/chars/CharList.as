/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 11:19
 */
package application.models.chars
{
    import application.models.ModelData;
    import application.models.user.UserInfo;
    import application.models.user.Vegetable;

    import framework.core.helpers.ArrayHelper;
    import framework.core.struct.data.ListData;

    public class CharList extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CharList()
        {
            _RecordClass = CharDetail;
            super(ModelData.CHAR_LIST);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['char_details'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getChars(vegetable:Vegetable, charGroup:CharGroup, skip:int = 0):Array
        {
            var result:Array = [];
            var list:Object = this.list.value;
            var count:int = 0;

            for (var key:String in list) {
                var char:CharDetail = list[key] as CharDetail;
                if (_validateCharGroup(char, charGroup) && _validateVegetable(char, vegetable)) {
                    if (count >= skip)
                        result.push(char);

                    count++;
                }
            }

            return result;
        }

        public function getAllAvailableChars(vegetable:Vegetable, skip:int = 0):Array
        {
            var result:Array = [];
            var list:Object = this.list.value;
            var count:int = 0;

            for (var key:String in list) {
                var char:CharDetail = list[key] as CharDetail;
                if (char.vegetable.value == vegetable.toString() || char.vegetable.value == Vegetable.ALL.toString()) {
                    if (count >= skip)
                        result.push(char);
                    count++;
                }
            }

            return result;
        }

        public function getRandomBuildConfiguration(vegetable:Vegetable):UserInfo
        {
            var userInfo:UserInfo = new UserInfo();

            var bodies:Array = getChars(vegetable, CharGroup.BODIES);
            var hairs:Array = getChars(vegetable, CharGroup.HAIRS);

            var eyes:Array = getChars(Vegetable.ALL, CharGroup.EYES);
            var mouths:Array = getChars(Vegetable.ALL, CharGroup.MOUTHS);

            var body:CharDetail = ArrayHelper.getRandomElement(bodies);
            var eye:CharDetail = ArrayHelper.getRandomElement(eyes);
            var mouth:CharDetail = ArrayHelper.getRandomElement(mouths);
            var hair:CharDetail = ArrayHelper.getRandomElement(hairs);

            var data:Object = {
                "vegetable": vegetable.toString(),
                "body": body.mc_1_name.value,
                "hands": body.mc_2_name.value,
                "eyes": eye.mc_1_name.value,
                "mouth": mouth.mc_1_name.value,
                "hair": hair.mc_1_name.value
            };

            userInfo.importData(data);

            return userInfo;
        }

        public function getDefaultBuildConfiguration(vegetable:Vegetable):UserInfo
        {
            var userInfo:UserInfo = new UserInfo();

            var bodies:Array = getChars(vegetable, CharGroup.BODIES);
            var hairs:Array = getChars(vegetable, CharGroup.HAIRS);

            var eyes:Array = getChars(Vegetable.ALL, CharGroup.EYES);
            var mouths:Array = getChars(Vegetable.ALL, CharGroup.MOUTHS);

            var body:CharDetail = bodies[0];
            var eye:CharDetail = eyes[0];
            var mouth:CharDetail = mouths[0];
            var hair:CharDetail = hairs[0];

            var data:Object = {
                "vegetable": vegetable.toString(),
                "body": body.mc_1_name.value,
                "hands": body.mc_2_name.value,
                "eyes": eye.mc_1_name.value,
                "mouth": mouth.mc_1_name.value,
                "hair": hair.mc_1_name.value
            };

            userInfo.importData(data);

            return userInfo;
        }

        public function getCharIndex(vegetable:Vegetable, char:CharDetail):int
        {
            var group:CharGroup = CharGroup.getEnumByKey(char.group.value);
            var veg:Vegetable = vegetable;
            if (group == CharGroup.EYES || group == CharGroup.MOUTHS)
                veg = Vegetable.ALL;
            var chars:Array = getChars(veg, group);
            return chars.indexOf(char);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _validateVegetable(char:CharDetail, vegetable:Vegetable):Boolean
        {
            if (vegetable == Vegetable.ALL) {
                if ((char.vegetable.value == Vegetable.ALL.toString()) || (char.vegetable.value == vegetable.toString()))
                    return true;
            }

            return char.vegetable.value == vegetable.toString();
        }

        private function _validateCharGroup(char:CharDetail, charGroup:CharGroup):Boolean
        {
            if (charGroup.toString() == CharGroup.ALL.toString())
                return true;
            return char.group.value == charGroup.toString();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
