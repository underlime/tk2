/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 11:10
 */
package application.models.chars
{
    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class CharDetail
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _char_detail_id:IntegerField = new IntegerField();
        private var _mc_1_name:CharField = new CharField();
        private var _mc_2_name:CharField = new CharField();
        private var _vegetable:CharField = new CharField();
        private var _group:CharField = new CharField();

        // TODO сделать геттер
        private var _default:BooleanField = new BooleanField();

        private var _price_ferros:IntegerField = new IntegerField();
        private var _price_tomatos:IntegerField = new IntegerField();
        private var _requirements:CharField = new CharField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get char_detail_id():IntegerField
        {
            return _char_detail_id;
        }

        public function get mc_1_name():CharField
        {
            return _mc_1_name;
        }

        public function get mc_2_name():CharField
        {
            return _mc_2_name;
        }

        public function get vegetable():CharField
        {
            return _vegetable;
        }

        public function get group():CharField
        {
            return _group;
        }

        public function get price_ferros():IntegerField
        {
            return _price_ferros;
        }

        public function get price_tomatos():IntegerField
        {
            return _price_tomatos;
        }

        public function get requirements():CharField
        {
            return _requirements;
        }

    }
}
