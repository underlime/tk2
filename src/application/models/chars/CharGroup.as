/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.06.13
 * Time: 15:44
 */
package application.models.chars
{
    import framework.core.enum.BaseEnum;

    public class CharGroup extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;
        public static var ENUM:Vector.<CharGroup> = new Vector.<CharGroup>();

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ALL:CharGroup = new CharGroup("all")
        public static const BODIES:CharGroup = new CharGroup("bodies");
        public static const HAIRS:CharGroup = new CharGroup("hairs");
        public static const EYES:CharGroup = new CharGroup("eyes");
        public static const MOUTHS:CharGroup = new CharGroup("mouths");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        public static function getEnumByKey(key:String):CharGroup
        {
            var count:int = ENUM.length;
            for (var i:int = 0; i < count; i++) {
                if (ENUM[i].toString() == key)
                    return ENUM[i];
            }
            throw new Error("Vegetable not found");
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CharGroup(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
            ENUM.push(this);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.models.chars.CharGroup;

CharGroup.lockUp();