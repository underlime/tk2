/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 11:44
 */
package application.models.opponents
{
    import application.models.ModelData;
    import application.models.user.User;

    import framework.core.struct.data.Data;

    public class OpponentsList extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function OpponentsList()
        {
            super(ModelData.OPPONENTS_LIST);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return [];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function addOpponent(user:Opponent):void
        {
            if (!hasOpponent(user.userInfo.soc_net_id.value))
                _opponents.push(user);
        }

        public function hasOpponent(soc_net_id:String):Boolean
        {
            var count:int = _opponents.length;
            for (var i:int = 0; i < count; i++) {
                var user:User = _opponents[i] as Opponent;
                if (user.userInfo.soc_net_id.value == soc_net_id)
                    return true;
            }

            return false;
        }

        public function getOpponent(soc_net_id:String):Opponent
        {
            if (hasOpponent(soc_net_id)) {
                var count:int = _opponents.length;
                for (var i:int = 0; i < count; i++) {
                    var user:Opponent = _opponents[i] as Opponent;
                    if (user.userInfo.soc_net_id.value == soc_net_id)
                        return user;
                }
            }
            return null;
        }

        public function deleteOpponent(user_id:int):void
        {
            var count:int = _opponents.length;
            for (var i:int = 0; i < count; i++) {
                var user:Opponent = _opponents[i];
                if (user.userInfo.user_id.value == user_id) {
                    _opponents.splice(i, 1);
                }
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _opponents:Array = [];

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
