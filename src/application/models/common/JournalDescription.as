/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.10.13
 * Time: 14:53
 */
package application.models.common
{
    import application.config.AppConfig;

    import flash.utils.ByteArray;

    import framework.core.enum.Language;

    import org.casalib.util.ArrayUtil;

    public class JournalDescription
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function JournalDescription()
        {
            super();
            _parseData();
            shuffle();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getWinMessage():String
        {
            var result:String;
            var arr:Array = AppConfig.LANGUAGE == Language.RU ? _wins_ru : _wins_en;
            var count:int = arr.length;
            if (count > 0) {
                if (!arr[_winIndex])
                    _winIndex = 0;

                result = arr[_winIndex];
                _winIndex++;
            }
            return result;
        }

        public function getFailMessage():String
        {
            var result:String;
            var arr:Array = AppConfig.LANGUAGE == Language.RU ? _lose_ru : _lose_en;
            var count:int = arr.length;
            if (count > 0) {
                if (!arr[_failIndex])
                    _failIndex = 0;

                result = arr[_failIndex];
                _failIndex++;
            }
            return result;
        }

        public function shuffle():void
        {
            ArrayUtil.randomize(_wins_ru);
            ArrayUtil.randomize(_lose_ru);
            ArrayUtil.randomize(_wins_en);
            ArrayUtil.randomize(_lose_en);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../lib/config/journal.json", mimeType="application/octet-stream")]
        private var JsonClass:Class;

        private var _wins_ru:Array = [];
        private var _lose_ru:Array = [];

        private var _wins_en:Array = [];
        private var _lose_en:Array = [];

        private var _winIndex:int = 0;
        private var _failIndex:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _parseData():void
        {
            var bytes:ByteArray = new JsonClass();
            var json:String = bytes.readUTFBytes(bytes.length);
            var data:Object = JSON.parse(json);
            var key:String;

            if (data['win']) {
                var winData:Object = data['win'];
                if (winData['russian']) {
                    for (key in winData['russian']) {
                        _wins_ru.push(winData['russian'][key]);
                    }
                }
                if (winData['english']) {
                    for (key in winData['english']) {
                        _wins_en.push(winData['english'][key]);
                    }
                }
            }

            if (data['fail']) {
                var loseData:Object = data['fail'];
                if (loseData['russian']) {
                    for (key in loseData['russian']) {
                        _lose_ru.push(loseData['russian'][key]);
                    }
                }
                if (loseData['english']) {
                    for (key in loseData['english']) {
                        _lose_en.push(loseData['english'][key]);
                    }
                }
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
