/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.09.13
 * Time: 18:26
 */
package application.models.common
{
    import application.config.AppConfig;

    import flash.utils.ByteArray;

    import framework.core.enum.Language;

    import org.casalib.util.ArrayUtil;

    public class Advices
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Advices()
        {
            super();
            _setupAdvices();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        public function getRandomAdvice():String
        {
            var advice:String = "";
            var arr:Array = AppConfig.LANGUAGE == Language.RU ? _advices_ru : _advices_en;
            var count:int = arr.length;
            if (count > 0) {

                if (!arr[_lastIndex])
                    _lastIndex = 0;

                advice = arr[_lastIndex];

                _lastIndex++;

                if (_lastIndex >= arr.length)
                    _lastIndex = 0;
            }

            return advice;
        }

        public function shuffleAdvices():void
        {
            if (_advices_ru && _advices_en) {
                _advices_en = ArrayUtil.randomize(_advices_ru);
                _advices_ru = ArrayUtil.randomize(_advices_ru);
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../lib/config/advices.json", mimeType="application/octet-stream")]
        private var AdvicesClass:Class;

        private var _advices_ru:Array = [];
        private var _advices_en:Array = [];

        private var _lastIndex:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupAdvices():void
        {
            var bytes:ByteArray = new AdvicesClass();
            var json:String = bytes.readUTFBytes(bytes.length);
            var data:Object = JSON.parse(json);

            if (data['russian']) {
                var russian_advices:Object = data['russian'];
                for (var key:String in russian_advices) {
                    _advices_ru.push(russian_advices[key]);
                }
            }

            if (data['english']) {
                var english_advices:Object = data['english'];
                for (var en_key:String in english_advices) {
                    _advices_en.push(english_advices[en_key]);
                }
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
