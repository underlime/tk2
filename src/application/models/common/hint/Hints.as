/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.10.13
 * Time: 19:47
 */
package application.models.common.hint
{
    import application.config.AppConfig;

    import flash.utils.ByteArray;

    import framework.core.enum.Language;

    public class Hints
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Hints()
        {
            super();
            _setup();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getHint(key:String):HintModel
        {
            var model:HintModel = new HintModel();
            var hash:Object = AppConfig.LANGUAGE == Language.RU ? _russian : _english;

            if (hash[key]) {
                var data:Object = hash[key];
                model.header = data['header'] ? data['header'] : "";
                model.description = data['description'] ? data['description'] : "";
            }

            return model;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../../lib/config/hints.json", mimeType="application/octet-stream")]
        private var HintClass:Class;

        private var _russian:Object;
        private var _english:Object;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void
        {
            var bytes:ByteArray = new HintClass();
            var json:String = bytes.readUTFBytes(bytes.length);
            var data:Object = JSON.parse(json);

            if (data['russian']) {
                _russian = data['russian'];

            }

            if (data['english']) {
                _english = data['english'];
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
