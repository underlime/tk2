/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.04.14
 * Time: 15:56
 */
package application.models.common
{
    import application.config.AppConfig;

    import flash.utils.ByteArray;

    import framework.core.enum.Language;

    public class TabHintsModel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TabHintsModel()
        {
            super();
            _setupTabHints();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getHint(key:String):Object
        {
            var data:Object = AppConfig.LANGUAGE == Language.RU ? _ruData : _enData;
            if (data[key]) {
                return data[key];
            }
            return {
                "title": "",
                "description": ""
            };
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../lib/config/tab_hints.json", mimeType="application/octet-stream")]
        private var HintsClass:Class;

        private var _data:Object;
        private var _enData:Object;
        private var _ruData:Object;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTabHints():void
        {
            var bytes:ByteArray = new HintsClass();
            var json:String = bytes.readUTFBytes(bytes.length);
            _data = JSON.parse(json);

            if (_data['ru']) {
                _ruData = _data['ru'];
            }

            if (_data['en']) {
                _enData = _data['en'];
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
