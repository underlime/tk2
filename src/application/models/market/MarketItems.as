/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 31.05.13
 * Time: 14:47
 */
package application.models.market
{
    import application.common.ArrangementVariant;
    import application.models.ModelData;

    import framework.core.helpers.ArrayHelper;
    import framework.core.struct.data.ListData;

    public class MarketItems extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MarketItems()
        {
            _RecordClass = Item;
            super(ModelData.MARKET_ITEMS);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['market_items_list'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getRandomBuildItems(weaponVariant:ArrangementVariant = null):Array
        {
            var items:Array = [];
            var weaponArrangements:Array = [ArrangementVariant.GLOVES, ArrangementVariant.ONE_HANDED, ArrangementVariant.ROD_WEAPON, ArrangementVariant.TWO_HANDED];

            if (!weaponVariant)
                weaponVariant = ArrayHelper.getRandomElement(weaponArrangements);

            if (weaponVariant != ArrangementVariant.GLOVES && weaponVariant != ArrangementVariant.TWO_HANDED)
                items.push(getRandomItem([ArrangementVariant.SHIELD]));

            items.push(
                    getRandomItem([weaponVariant]),
                    getRandomItem([ArrangementVariant.MASK, ArrangementVariant.GLASSES]),
                    getRandomItem([ArrangementVariant.COAT]),
                    getRandomItem([ArrangementVariant.HAT])
            );

            return items;
        }

        public function getRandomItem(arrangementVariants:Array):Item
        {
            var items:Array = [];
            var list:Object = this.list.value;

            for (var key:String in list) if (list.hasOwnProperty(key)) {
                var item:Item = list[key] as Item;
                var arrangement:ArrangementVariant = ArrangementVariant.getEnumByKey(item.arrangement_variant.value);
                if (arrangementVariants.indexOf(arrangement) > -1)
                    items.push(item);
            }

            var randomItem:Item = ArrayHelper.getRandomElement(items);
            return randomItem;
        }

        public function getItemById(itemId:int):Item
        {
            for each (var record:Item in this.list.value) {
                if (record.item_id.value == itemId)
                    return record;
            }
            return null;
        }

        public function getSortedItems(rubric:String, filter:String = null, start:int = 0, limit:int = 100):Array
        {
            var list:Object = list.value;
            var items:Array = [];

            for (var key:String in list) {
                var item:Item = list[key];
                if (item.rubric_code.value == rubric && item.show_in_market.value) {
                    if (!filter) {
                        items.push(item);
                    } else {
                        if (item.arrangement_variant.value == filter) {
                            items.push(item);
                        }
                    }
                }
            }

            items.sortOn("requiredLevel", Array.NUMERIC);
            return items.slice(start, start + limit);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
