/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 31.05.13
 * Time: 14:29
 */
package application.models.market
{
    import application.common.ArrangementVariant;

    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class Item
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Item()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item_id:IntegerField = new IntegerField();
        private var _arrangement_variant:CharField = new CharField();
        private var _rubric_code:CharField = new CharField();
        private var _name_ru:CharField = new CharField();
        private var _picture:CharField = new CharField();
        private var _price_ferros:IntegerField = new IntegerField();
        private var _price_tomatos:IntegerField = new IntegerField();
        private var _sell_price_tomatos:IntegerField = new IntegerField();
        private var _buy_another:IntegerField = new IntegerField();
        private var _buy_another_at_once:IntegerField = new IntegerField();
        private var _requirements:CharField = new CharField();
        private var _bonuses:CharField = new CharField();
        private var _apply_in_fight:CharField = new CharField();
        private var _fight_damage:IntegerField = new IntegerField();
        private var _heal_percents:IntegerField = new IntegerField();
        private var _show_in_market:BooleanField = new BooleanField();
        private var _sort:IntegerField = new IntegerField();
        private var _required_level:IntegerField = new IntegerField();

        private var _description_ru:CharField = new CharField();

        private var _sharpen_damage:IntegerField = new IntegerField();
        private var _sharpen_armor:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get item_id():IntegerField
        {
            return _item_id;
        }

        public function get arrangement_variant():CharField
        {
            return _arrangement_variant;
        }

        public function get rubric_code():CharField
        {
            return _rubric_code;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get picture():CharField
        {
            return _picture;
        }

        public function get price_ferros():IntegerField
        {
            return _price_ferros;
        }

        public function get price_tomatos():IntegerField
        {
            return _price_tomatos;
        }

        public function get sell_price_tomatos():IntegerField
        {
            return _sell_price_tomatos;
        }

        public function get buy_another():IntegerField
        {
            return _buy_another;
        }

        public function get buy_another_at_once():IntegerField
        {
            return _buy_another_at_once;
        }

        public function get requirements():CharField
        {
            return _requirements;
        }

        public function get bonuses():CharField
        {
            return _bonuses;
        }

        public function get apply_in_fight():CharField
        {
            return _apply_in_fight;
        }

        public function get fight_damage():IntegerField
        {
            return _fight_damage;
        }

        public function get heal_percents():IntegerField
        {
            return _heal_percents;
        }

        public function get show_in_market():BooleanField
        {
            return _show_in_market;
        }

        public function get sort():IntegerField
        {
            return _sort;
        }

        public function get required_level():IntegerField
        {
            return _required_level;
        }

        public function get isConsume():Boolean
        {
            return this.arrangement_variant.value == ArrangementVariant.DRINK.toString() || this.arrangement_variant.value == ArrangementVariant.GRENADE.toString();
        }

        public function get description_ru():CharField
        {
            return _description_ru;
        }

        public function get sharpen_damage():IntegerField
        {
            return _sharpen_damage;
        }

        public function get sharpen_armor():IntegerField
        {
            return _sharpen_armor;
        }

        public function get requiredLevel():int
        {
            return _required_level.value;
        }
    }
}
