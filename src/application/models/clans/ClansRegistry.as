/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.02.14
 * Time: 14:51
 */
package application.models.clans
{
    public class ClansRegistry
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static var _clans:Array = [];

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function add(clan:ClanInfo):void
        {
            if (!has(clan.id.value)) {
                _clans.push(clan);
            }
        }

        public static function has(clan_id:int):Boolean
        {
            var count:int = _clans.length;
            for (var i:int = 0; i < count; i++) {
                var currentClan:ClanInfo = _clans[i];
                if (clan_id == currentClan.id.value)
                    return true;
            }
            return false;
        }

        public static function getClan(clan_id:int):ClanInfo
        {
            var count:int = _clans.length;
            for (var i:int = 0; i < count; i++) {
                var currentClan:ClanInfo = _clans[i];
                if (clan_id == currentClan.id.value)
                    return currentClan;
            }
            return null;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
