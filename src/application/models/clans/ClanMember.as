/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.02.14
 * Time: 14:37
 */
package application.models.clans
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class ClanMember extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanMember()
        {
            super("ClanMember");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _soc_net_id:CharField = new CharField();
        private var _user_id:IntegerField = new IntegerField();
        private var _season_glory:IntegerField = new IntegerField();
        private var _login:CharField = new CharField();
        private var _is_leader:BooleanField = new BooleanField();
        private var _items_given:IntegerField = new IntegerField();
        private var _picture:CharField = new CharField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get user_id():IntegerField
        {
            return _user_id;
        }

        public function get season_glory():IntegerField
        {
            return _season_glory;
        }

        public function get login():CharField
        {
            return _login;
        }

        public function get is_leader():BooleanField
        {
            return _is_leader;
        }

        public function get items_given():IntegerField
        {
            return _items_given;
        }

        public function get soc_net_id():CharField
        {
            return _soc_net_id;
        }

        public function get picture():CharField
        {
            return _picture;
        }

        public function get gloryValue():int
        {
            return this.season_glory.value;
        }
    }
}
