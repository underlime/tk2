/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.03.14
 * Time: 20:18
 */
package application.models.clans
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.data.fields.IntegerField;

    public class ClanRequestRecord extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanRequestRecord()
        {
            super("ClanRequestRecord");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user_id:IntegerField = new IntegerField();
        private var _soc_net_id:CharField = new CharField();
        private var _login:CharField = new CharField();
        private var _glory:IntegerField = new IntegerField();
        private var _level:IntegerField = new IntegerField();
        private var _max_level:IntegerField = new IntegerField();
        private var _request_time:DateTimeField = new DateTimeField();
        private var _picture:CharField = new CharField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get user_id():IntegerField
        {
            return _user_id;
        }

        public function get soc_net_id():CharField
        {
            return _soc_net_id;
        }

        public function get login():CharField
        {
            return _login;
        }

        public function get glory():IntegerField
        {
            return _glory;
        }

        public function get level():IntegerField
        {
            return _level;
        }

        public function get max_level():IntegerField
        {
            return _max_level;
        }

        public function get request_time():DateTimeField
        {
            return _request_time;
        }

        public function get picture():CharField
        {
            return _picture;
        }
    }
}
