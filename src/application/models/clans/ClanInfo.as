/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.02.14
 * Time: 14:43
 */
package application.models.clans
{
    import application.models.ModelData;

    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class ClanInfo extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanInfo()
        {
            super(ModelData.CLAN_INFO);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["clan_info"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function deleteClan():void
        {
            _id.value = 0;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _id:IntegerField = new IntegerField();
        private var _clan_name:CharField = new CharField();
        private var _leader_id:IntegerField = new IntegerField();
        private var _glory:IntegerField = new IntegerField();
        private var _previous_place_in_top:IntegerField = new IntegerField();
        private var _victories:IntegerField = new IntegerField();
        private var _picture:IntegerField = new IntegerField();
        private var _members_count:IntegerField = new IntegerField();

        private var _clan_members:ClanMembers = new ClanMembers();

        private var _requestSent:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get id():IntegerField
        {
            return _id;
        }

        public function get leader_id():IntegerField
        {
            return _leader_id;
        }

        public function get glory():IntegerField
        {
            return _glory;
        }

        public function get previous_place_in_top():IntegerField
        {
            return _previous_place_in_top;
        }

        public function get victories():IntegerField
        {
            return _victories;
        }

        public function get picture():IntegerField
        {
            return _picture;
        }

        public function get clan_name():CharField
        {
            return _clan_name;
        }

        public function get clan_members():ClanMembers
        {
            return _clan_members;
        }

        public function get requestSent():Boolean
        {
            return _requestSent;
        }

        public function set requestSent(value:Boolean):void
        {
            _requestSent = value;
        }

        public function get members_count():IntegerField
        {
            return _members_count;
        }
    }
}
