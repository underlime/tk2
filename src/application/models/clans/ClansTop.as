/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.03.14
 * Time: 18:10
 */
package application.models.clans
{
import application.models.ModelData;

import framework.core.struct.data.Data;
import framework.core.struct.data.fields.DateTimeField;
import framework.core.struct.data.fields.IntegerField;

public class ClansTop extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClansTop()
        {
            super(ModelData.CLANS_TOP);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["clans_clash_season", "clans_top_glory"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _season_end:DateTimeField = new DateTimeField();
        private var _season_prize:IntegerField = new IntegerField();
        private var _id:IntegerField = new IntegerField();

        private var _clans_top_glory:ClanList = new ClanList();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get season_end():DateTimeField
        {
            return _season_end;
        }

        public function get season_prize():IntegerField
        {
            return _season_prize;
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get clans_top_glory():ClanList
        {
            return _clans_top_glory;
        }
    }
}
