/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.02.14
 * Time: 14:35
 */
package application.models.clans
{
    import application.models.ModelData;

    import framework.core.struct.data.ListData;

    public class ClanMembers extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanMembers()
        {
            _RecordClass = ClanMember;
            super(ModelData.CLAN_MEMBER);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["clan_members"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getMembersByGlory():Array
        {
            var members:Array = [];
            var list:Object = list.value;
            for (var key:String in list) {
                members.push(list[key]);
            }
            members.sortOn("gloryValue", Array.NUMERIC | Array.DESCENDING);
            return members;
        }

        public function deleteMember(user_id:int):void
        {
            var list:Object = this.list.value;
            for (var key:String in list) {
                var member:ClanMember = list[key];
                if (member.user_id.value == user_id)
                    delete list[key];
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
