/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.07.13
 * Time: 14:29
 */
package application.models.ranks
{
    import application.config.AppConfig;
    import application.models.ModelData;

    import framework.core.enum.Language;
    import framework.core.struct.data.ListData;

    public class RanksModel extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RanksModel()
        {
            _RecordClass = RankRecord;
            super(ModelData.RANKS);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["ranks_collection"]
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getRankData(glory:int):RankData
        {
            var data:RankData = new RankData();
            var list:Object = list.value;
            var ru:Boolean = AppConfig.LANGUAGE == Language.RU;

            for (var key:String in list) {
                var record:RankRecord = list[key] as RankRecord;
                if (glory >= record.glory.value) {
                    data.name = ru ? record.name_ru.value : record.name_en.value;
                    data.picture = record.picture.value;
                }
            }

            return data;
        }

        public function getMaxGlory(glory:int):int
        {
            var max:int = 0;
            var list:Object = list.value;

            for (var key:String in list) {
                var record:RankRecord = list[key] as RankRecord;
                if (glory <= record.glory.value) {
                    max = record.glory.value;
                    break;
                }
            }

            return max;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
