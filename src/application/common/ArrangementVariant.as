/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.06.13
 * Time: 20:50
 */
package application.common
{
    import framework.core.enum.BaseEnum;

    public class ArrangementVariant extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static var ENUM:Vector.<ArrangementVariant> = new Vector.<ArrangementVariant>();

        public static const APPLY:ArrangementVariant = new ArrangementVariant("apply");
        public static const ARMOR:ArrangementVariant = new ArrangementVariant("armor");
        public static const GLOVES:ArrangementVariant = new ArrangementVariant("both_hands");
        public static const COAT:ArrangementVariant = new ArrangementVariant("cloak");
        public static const DRINK:ArrangementVariant = new ArrangementVariant("drink");
        public static const GLASSES:ArrangementVariant = new ArrangementVariant("glasses");
        public static const GRENADE:ArrangementVariant = new ArrangementVariant("grenade");
        public static const HAT:ArrangementVariant = new ArrangementVariant("hat");
        public static const TWO_HANDED:ArrangementVariant = new ArrangementVariant("heavy_weapon");
        public static const SHIELD:ArrangementVariant = new ArrangementVariant("left_hand");
        public static const MASK:ArrangementVariant = new ArrangementVariant("mask");
        public static const MINION:ArrangementVariant = new ArrangementVariant("minion");
        public static const ONE_HANDED:ArrangementVariant = new ArrangementVariant("right_hand");
        public static const AMULET:ArrangementVariant = new ArrangementVariant("amulet");
        public static const RINGS:ArrangementVariant = new ArrangementVariant("ring");
        public static const ROD_WEAPON:ArrangementVariant = new ArrangementVariant("rod");
        public static const SLOT:ArrangementVariant = new ArrangementVariant("slot");
        public static const BOOTS:ArrangementVariant = new ArrangementVariant("boots");
        public static const ENERGY:ArrangementVariant = new ArrangementVariant("energy");
        public static const RANGE:ArrangementVariant = new ArrangementVariant("range");
        public static const BOX:ArrangementVariant = new ArrangementVariant("box");
        public static const QUEST:ArrangementVariant = new ArrangementVariant("quest");

        private static var _lockUp:Boolean = false;

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        public static function getEnumByKey(key:String):ArrangementVariant
        {
            var count:int = ENUM.length;
            for (var i:int = 0; i < count; i++) {
                if (ENUM[i].toString() == key)
                    return ENUM[i];
            }
            throw new Error("Variant not found");
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArrangementVariant(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
            ENUM.push(this);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.common.ArrangementVariant;

ArrangementVariant.lockUp();
