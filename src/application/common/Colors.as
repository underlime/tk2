/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.10.13
 * Time: 15:09
 */
package application.common
{
    public class Colors
    {
        public static const WHITE:uint = 0xffffff;
        public static const BLACK:uint = 0x000000;
        public static const DARK_BROWN:uint = 0x3c2415;
        public static const BROWN:uint = 0x754c29;
        public static const LIGHT_BROWN:uint = 0x5e3113;
        public static const RED:uint = 0xbe1e2d;
        public static const GREEN:uint = 0x006938;
        public static const BLUE:uint = 0x1c75bc;

        public static const DARK_MILK:uint = 0xddc078;
        public static const MILK:uint = 0xefdf9a;
    }
}
