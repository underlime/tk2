/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 15:58
 */
package application.config
{
    import framework.core.enum.Language;
    import framework.core.socnet.SocNet;

    public class AppConfig
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const DEBUG:Boolean = true;
        public static const APP_WIDTH:int = 750;
        public static const APP_HEIGHT:int = 510;

        public static const APP_NAME:String = "TomatoCombat";
        public static const APP_LABEL:String = "Tomato Combat";
        public static const EN:String = "english";
        public static const RU:String = "russian";

        public static const RESULT_SALT:Array = [14, 10, 7, 22, 12, 16, 24, 81];
        public static const TOKEN_SALT:Array = [27, 26, 27, 32, 18, 30, 20, 12, 22, 18];
        public static const Z:int = 127;

        public static var LANGUAGE:Language = Language.RU;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public static function get applicationLink():String
        {
            var link:String = '#';
            switch (SocNet.instance().socNetName) {
                case SocNet.VK_COM:
                    link = 'https://vk.com/tomato_kombat';
                CONFIG::DEBUG {
                    link = 'https://vk.com/app3875155';
                }
                    break;
            }
            return link;
        }
    }
}
