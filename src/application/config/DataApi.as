/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 31.05.13
 * Time: 16:46
 */
package application.config
{
    import framework.core.socnet.SocNet;
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class DataApi extends Data
    {
        private static const CONTROL_SETS:Object = {
            'local_debug': {
                'domain': 'tomatoapi4.loc'
            },
            'server_debug': {
                'domain': 'tomato-kombat.hub.webkraken.ru'
            },
            'rc': {
                'domain': 'tomato-kombat-rc.hub.webkraken.ru'
            },
            'release': {
                'domain': 'tomato-kombat.webkraken.ru'
            }
        };

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DataApi()
        {
            super(Config.DATA_API_CONFIG);

            var controlSet:Object = CONTROL_SETS[_getControlSetName()];
            var socNetType:String = SocNet.socNetType;
            if (socNetType == SocNet.MOCK)
                socNetType = SocNet.VK_COM;

            _protocol.value = controlSet['protocol'] || 'http';
            _domain.value = controlSet['domain'] || 'localhost';
            _port.value = controlSet['port'] || 80;
            _url.value = '/api/' + socNetType + '/';
            _mock.value = Boolean(controlSet['mock']);
        }

        private function _getControlSetName():String
        {
            var controlSetName:String;
            CONFIG::DEBUG {
                controlSetName = 'server_debug';
                CONFIG::LOCAL {
                    controlSetName = 'local_debug';
                }
            }

            CONFIG::RC {
                controlSetName = 'rc';
            }

            CONFIG::RELEASE {
                controlSetName = 'release';
            }

            if (!controlSetName)
                throw new Error('Build flag is not set');

            return controlSetName;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _protocol:CharField = new CharField();
        private var _domain:CharField = new CharField();
        private var _port:IntegerField = new IntegerField();
        private var _url:CharField = new CharField();
        private var _mock:BooleanField = new BooleanField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get protocol():CharField
        {
            return _protocol;
        }

        public function get domain():CharField
        {
            return _domain;
        }

        public function get port():IntegerField
        {
            return _port;
        }

        public function get url():CharField
        {
            return _url;
        }

        public function get mock():BooleanField
        {
            return _mock;
        }

    }
}
