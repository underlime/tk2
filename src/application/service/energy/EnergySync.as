/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.10.13
 * Time: 16:10
 */
package application.service.energy
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.user.User;
    import application.service.model.RenewEnergy;

    import flash.events.Event;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.service.Service;

    import org.casalib.time.Interval;

    public class EnergySync extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SERVER_RENEW_PERIOD:int = 300000; // 5 min

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EnergySync(appController:ApplicationController)
        {
            super();
            _appController = appController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupModel();
            _setupService();
            _checkEnergy();
            _model.userFightData.addEventListener(Event.CHANGE, _changeHandler);
        }

        override public function destroy():void
        {
            _model.userFightData.removeEventListener(Event.CHANGE, _changeHandler);
            _stopSynchronise();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _model:User;
        private var _service:RenewEnergy;

        private var _serverInterval:Interval;
        private var _clientInterval:Interval;

        private var _isSync:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupModel():void
        {
            _model = ModelsRegistry.getModel(ModelData.USER) as User;
        }

        private function _setupService():void
        {
            _service = new RenewEnergy(_appController);
        }

        private function _setServerInterval():void
        {
            _destroyServerInterval();
            _serverInterval = Interval.setInterval(_syncEnergy, SERVER_RENEW_PERIOD);
            _serverInterval.repeatCount = int.MAX_VALUE;
            _serverInterval.start();
        }

        private function _syncEnergy():void
        {
            _service.execute();
        }

        private function _startSynchronise():void
        {
            _setClientInterval();
            _setServerInterval();
            _isSync = true;
        }

        private function _stopSynchronise():void
        {
            _destroyServerInterval();
            _destroyClientInterval();
            _isSync = false;
        }

        private function _setClientInterval():void
        {
            _destroyClientInterval();
            _clientInterval = Interval.setInterval(_increaseEnergy, _model.userOptInfo.energy_period.value * 1000);
            _clientInterval.repeatCount = int.MAX_VALUE;
            _clientInterval.start();
        }

        private function _increaseEnergy():void
        {
            var energy:int = _model.userFightData.energy.value + 1;
            _model.userFightData.setEnergy(energy);

            if (_model.userFightData.energy.value >= _model.userFightData.max_energy.value) {
                _stopSynchronise();
            }
        }

        private function _destroyClientInterval():void
        {
            if (_clientInterval && !_clientInterval.destroyed)
                _clientInterval.destroy();
        }

        private function _destroyServerInterval():void
        {
            if (_serverInterval && !_serverInterval.destroyed)
                _serverInterval.destroy();
        }

        private function _checkEnergy():void
        {
            if (_model.userFightData.energy.value < _model.userFightData.max_energy.value && !_isSync) {
                _startSynchronise();
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _checkEnergy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
