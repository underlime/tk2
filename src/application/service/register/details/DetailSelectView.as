/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.10.13
 * Time: 20:07
 */
package application.service.register.details
{
    import application.models.ModelData;
    import application.models.chars.CharDetail;
    import application.models.chars.CharGroup;
    import application.models.chars.CharList;
    import application.models.market.Item;
    import application.models.user.UserInfo;
    import application.models.user.Vegetable;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.constructor.builders.CharsChanger;
    import application.views.constructor.common.TemplateType;

    import flash.events.Event;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.struct.view.View;

    public class DetailSelectView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DetailSelectView()
        {
            super();

            _charList = ModelsRegistry.getModel(ModelData.CHAR_LIST) as CharList;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addEyesSelect();
            _addMouthSelect();
            _addHairSelect();
            _addBodySelect();

            _inited = true;
            _update();
        }

        override public function destroy():void
        {
            _eyesSelect.destroy();
            _mouthSelect.destroy();
            _hairSelect.destroy();
            _bodySelect.destroy();
            _unit.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function showUnit():void
        {

        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _eyesSelect:DetailSelect;
        private var _mouthSelect:DetailSelect;
        private var _hairSelect:DetailSelect;
        private var _bodySelect:DetailSelect;

        private var _charList:CharList;
        private var _vegetable:Vegetable = Vegetable.TOMATO;

        private var _inited:Boolean = false;
        private var _unit:CharsChanger;

        private var _unitCont:View = new View();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addEyesSelect():void
        {
            _eyesSelect = new DetailSelect();
            addChild(_eyesSelect);
            _eyesSelect.x = 349;
            _eyesSelect.y = 192;
            _eyesSelect.label = TextFactory.instance.getLabel('eyes_label');
            _eyesSelect.addEventListener(Event.CHANGE, _eyesChangeHandler);
        }

        private function _addMouthSelect():void
        {
            _mouthSelect = new DetailSelect();
            addChild(_mouthSelect);
            _mouthSelect.x = 349;
            _mouthSelect.y = 262;
            _mouthSelect.label = TextFactory.instance.getLabel('mouth_label');
            _mouthSelect.addEventListener(Event.CHANGE, _mouthSelectHandler);
        }

        private function _addHairSelect():void
        {
            _hairSelect = new DetailSelect();
            addChild(_hairSelect);
            _hairSelect.x = 539;
            _hairSelect.y = 192;
            _hairSelect.label = TextFactory.instance.getLabel('hair_label');
            _hairSelect.addEventListener(Event.CHANGE, _hairSelectHandler);
        }

        private function _addBodySelect():void
        {
            _bodySelect = new DetailSelect();
            addChild(_bodySelect);
            _bodySelect.x = 539;
            _bodySelect.y = 262;
            _bodySelect.label = TextFactory.instance.getLabel('color_label');
            _bodySelect.addEventListener(Event.CHANGE, _bodyChangeHandler);
        }

        private function _update():void
        {
            _eyesSelect.list = _charList.getChars(Vegetable.ALL, CharGroup.EYES);
            _mouthSelect.list = _charList.getChars(Vegetable.ALL, CharGroup.MOUTHS);
            _hairSelect.list = _charList.getChars(_vegetable, CharGroup.HAIRS);
            _bodySelect.list = _charList.getChars(_vegetable, CharGroup.BODIES);

            _updateUnit();
        }

        private function _updateUnit():void
        {
            if (_unit && !_unit.destroyed)
                _unit.destroy();

            _addUnit();
        }

        private function _addUnit():void
        {
            _unitCont = new View();
            addChild(_unitCont);

            var userInfo:UserInfo = _charList.getDefaultBuildConfiguration(_vegetable);
            _unit = new CharsChanger(userInfo, new Vector.<Item>());
            _unitCont.addChild(_unit);
            _unitCont.x = 200;
            _unitCont.y = 218;

            if (_unit.templateType == TemplateType.LONG) {
                _unitCont.x = 180;
                _unitCont.y = 238;
            }

            _unit.scaleX = -.8;
            _unit.scaleY = .8;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _eyesChangeHandler(event:Event):void
        {
            _unit.change(_eyesSelect.currentChar);
        }

        private function _mouthSelectHandler(event:Event):void
        {
            _unit.change(_mouthSelect.currentChar);
        }

        private function _hairSelectHandler(event:Event):void
        {
            _unit.change(_hairSelect.currentChar);
        }

        private function _bodyChangeHandler(event:Event):void
        {
            _unit.change(_bodySelect.currentChar);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set vegetable(vegetable:Vegetable):void
        {
            _vegetable = vegetable;
            if (_inited)
                _update();
        }

        public function get body():CharDetail
        {
            return _bodySelect.currentChar;
        }

        public function get hair():CharDetail
        {
            return _hairSelect.currentChar;
        }

        public function get mouth():CharDetail
        {
            return _mouthSelect.currentChar;
        }

        public function get eyes():CharDetail
        {
            return _eyesSelect.currentChar;
        }

        public function get details():Array
        {
            return [
                _eyesSelect.currentChar,
                _hairSelect.currentChar,
                _mouthSelect.currentChar,
                _bodySelect.currentChar
            ];
        }
    }
}
