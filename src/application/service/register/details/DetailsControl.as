/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.10.13
 * Time: 15:38
 */
package application.service.register.details
{
    import application.service.register.RegisterControl;
    import application.service.register.RegisterData;
    import application.views.TextFactory;
    import application.views.register.RegisterView;

    public class DetailsControl extends RegisterControl
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DetailsControl(root:RegisterView)
        {
            super(root);
            caption = TextFactory.instance.getLabel('choose_detail_label');

            _setupView();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        override public function open():void
        {
            _root.addChild(_view);
            _view.vegetable = _root.vegetable;
            _view.showUnit();
        }

        override public function done():void
        {
            _root.removeChild(_view);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function importDetails(data:RegisterData):void
        {
            data.body = _view.body.mc_1_name.value;
            data.hands = _view.body.mc_2_name.value;
            data.hair = _view.hair.mc_1_name.value;
            data.mouth = _view.mouth.mc_1_name.value;
            data.eyes = _view.eyes.mc_1_name.value;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:DetailSelectView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupView():void
        {
            _view = new DetailSelectView();
            _view.vegetable = _root.vegetable;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get details():Array
        {
            return _view.details;
        }
    }
}
