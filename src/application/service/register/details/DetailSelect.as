/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.10.13
 * Time: 19:59
 */
package application.service.register.details
{
    import application.models.chars.CharDetail;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.screen.common.NotificationLabel;
    import application.views.screen.common.navigation.LeftArrow;
    import application.views.screen.common.navigation.RightArrow;

    import flash.events.Event;
    import flash.events.MouseEvent;

    public class DetailSelect extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DetailSelect()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addElements();
            _update();
        }

        override public function destroy():void
        {
            _prevButton.destroy();
            _nextButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _rubber:NotificationLabel;
        private var _nextButton:RightArrow;
        private var _prevButton:LeftArrow;

        private var _list:Array = [];

        private var _index:int = 0;
        private var _count:int = 0;

        private var _label:String = "";

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addElements():void
        {
            _rubber = new NotificationLabel(" ");
            _rubber.notificationWidth = 96;
            addChild(_rubber);
            _rubber.x = 32;

            _prevButton = new LeftArrow();
            _prevButton.y = 7;
            addChild(_prevButton);

            _nextButton = new RightArrow();
            _nextButton.x = 129;
            _nextButton.y = 7;
            addChild(_nextButton);
        }

        private function _enableButtons():void
        {
            _prevButton.enable();
            _nextButton.enable();

            _prevButton.addEventListener(MouseEvent.CLICK, _prevClickHandler);
            _nextButton.addEventListener(MouseEvent.CLICK, _nextClickHandler);
        }

        private function _disableButtons():void
        {
            _prevButton.block();
            _nextButton.block();
        }

        private function _update():void
        {
            _index = 0;
            _count = _list.length;

            if (_count > 0) {
                _enableButtons();
                _showChar();
            } else {
                _disableButtons();
            }
        }

        private function _showChar():void
        {
            _rubber.label = _label + " " + (_index + 1).toString();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _nextClickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            if (_index < _count - 1)
                _index++;
            else
                _index = 0;
            _showChar();
            dispatchEvent(new Event(Event.CHANGE));
        }

        private function _prevClickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            if (_index > 0)
                _index--;
            else
                _index = _count - 1;
            _showChar();
            dispatchEvent(new Event(Event.CHANGE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get list():Array
        {
            return _list;
        }

        public function set list(value:Array):void
        {
            _list = value;
            _update();
        }

        public function get currentChar():CharDetail
        {
            return _list[_index];
        }

        public function set label(value:String):void
        {
            _label = value;
        }

    }
}
