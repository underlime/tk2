/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 03.10.13
 * Time: 16:09
 */
package application.service.register.vegetable
{
    import flash.display.DisplayObject;

    import framework.core.struct.view.View;
    import framework.core.utils.Assert;

    public class Embryo extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOMATO:String = "INCUBATOR_TomatoEmbrion";
        public static const CUCUMBER:String = "INCUBATOR_CucumberEmbrion";
        public static const CARROT:String = "INCUBATOR_CarrotEmbrion";
        public static const ONION:String = "INCUBATOR_OnionEmbrion";
        public static const BETA:String = "INCUBATOR_BetaEmbrion";
        public static const PEPPER:String = "INCUBATOR_PepperEmbrion";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Embryo()
        {
            super();
            _setupVegetables();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addEmbryo();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            if (_embryo && this.contains(_embryo))
                this.removeChild(_embryo);

            Assert.indexInRange(_index, 0, 5);

            _embryo = _vegetables[_index];
            _setEmbryo();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _vegetables:Array = [];
        private var _embryo:DisplayObject;

        private var _index:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addEmbryo():void
        {
            _embryo = _vegetables[0];
            _setEmbryo();
        }

        private function _setEmbryo():void
        {
            addChild(_embryo);
            _embryo.x = 90;
            _embryo.y = 188;
        }

        private function _setupVegetables():void
        {
            _vegetables.push(
                    super.source.getMovieClip(TOMATO),
                    super.source.getMovieClip(CUCUMBER),
                    super.source.getMovieClip(PEPPER),
                    super.source.getMovieClip(CARROT),
                    super.source.getMovieClip(BETA),
                    super.source.getMovieClip(ONION)
            );
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set vegetable(value:int):void
        {
            _index = value;
            update();
        }

        public function get embryo():DisplayObject
        {
            return _embryo;
        }

        public function get index():int
        {
            return _index;
        }
    }
}
