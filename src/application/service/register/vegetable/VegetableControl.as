/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.10.13
 * Time: 15:37
 */
package application.service.register.vegetable
{
    import application.models.user.Vegetable;
    import application.service.register.RegisterControl;
    import application.views.TextFactory;
    import application.views.register.RegisterView;

    import flash.events.Event;

    import framework.core.tools.ZoomHelper;

    public class VegetableControl extends RegisterControl
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function VegetableControl(root:RegisterView)
        {
            super(root);
            super.caption = TextFactory.instance.getLabel("choose_veg_label");
            _setupView();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            _embryo.destroy();
            _view.destroy();
            super.destroy();
        }

        override public function open():void
        {
            _addView();
        }

        override public function done():void
        {
            _root.removeChild(_view);
            ZoomHelper.hide(_embryo.embryo, _remove);
        }

        private function _remove():void
        {
            _embryo.update();
            _root.removeChild(_embryo);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:VegetableChoose;
        private var _embryo:Embryo;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupView():void
        {
            _view = new VegetableChoose();
            _embryo = new Embryo();

            _view.addEventListener(Event.CHANGE, _changeHandler);
        }

        private function _addView():void
        {
            _root.addChild(_view);
            _root.addChild(_embryo);

            ZoomHelper.show(_embryo.embryo);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _embryo.vegetable = _view.vegetable;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get vegetable():Vegetable
        {
            var vegetable:Vegetable = Vegetable.TOMATO;
            switch (_embryo.index) {
                case 0:
                    vegetable = Vegetable.TOMATO;
                    break;
                case 1:
                    vegetable = Vegetable.CUCUMBER;
                    break;
                case 2:
                    vegetable = Vegetable.PEPPER;
                    break;
                case 3:
                    vegetable = Vegetable.CARROT;
                    break;
                case 4:
                    vegetable = Vegetable.BETA;
                    break;
                case 5:
                    vegetable = Vegetable.ONION;
                    break;
            }

            return vegetable;
        }
    }
}
