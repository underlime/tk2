/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.10.13
 * Time: 20:04
 */
package application.service.register.vegetable
{
    import application.views.AppView;

    import flash.events.Event;

    public class VegetableChoose extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function VegetableChoose()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addRadio();
        }

        override public function destroy():void
        {
            _vegetableRadio.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _vegetableRadio:RegisterVegetableGroup;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addRadio():void
        {
            _vegetableRadio = new RegisterVegetableGroup();
            addChild(_vegetableRadio);
            _vegetableRadio.x = 382;
            _vegetableRadio.y = 200;
            _vegetableRadio.select(0);

            _vegetableRadio.addEventListener(Event.CHANGE, _changeHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            dispatchEvent(new Event(Event.CHANGE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/


        public function get vegetable():int
        {
            return _vegetableRadio.selectedRadio;
        }
    }
}
