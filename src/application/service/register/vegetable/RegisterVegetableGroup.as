/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.10.13
 * Time: 20:05
 */
package application.service.register.vegetable
{
    import application.views.TextFactory;
    import application.views.screen.common.radio.AppRadioButton;

    import framework.core.display.components.RadioButton;
    import framework.core.display.components.RadioGroup;

    public class RegisterVegetableGroup extends RadioGroup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RegisterVegetableGroup()
        {
            var radios:Vector.<RadioButton> = new <RadioButton>[
                new AppRadioButton(TextFactory.instance.getLabel('tomato_label')),
                new AppRadioButton(TextFactory.instance.getLabel('cucumber_label')),
                new AppRadioButton(TextFactory.instance.getLabel('pepper_label')),
                new AppRadioButton(TextFactory.instance.getLabel('carrot_label')),
                new AppRadioButton(TextFactory.instance.getLabel('beta_label')),
                new AppRadioButton(TextFactory.instance.getLabel('onion_label'))
            ];
            super(radios);
            _dy = 9;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();

            _radioButtons[3].y = _radioButtons[0].y;
            _radioButtons[4].y = _radioButtons[1].y;
            _radioButtons[5].y = _radioButtons[2].y;

            _radioButtons[3].x = 150;
            _radioButtons[4].x = 150;
            _radioButtons[5].x = 150;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
