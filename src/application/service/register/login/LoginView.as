/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.10.13
 * Time: 15:34
 */
package application.service.register.login
{
    import application.events.OfficeEvent;
    import application.helpers.LoginHelper;
    import application.models.ModelData;
    import application.models.chars.CharDetail;
    import application.models.chars.CharList;
    import application.models.market.Item;
    import application.models.user.UserInfo;
    import application.models.user.Vegetable;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.constructor.builders.CharsChanger;
    import application.views.constructor.common.TemplateType;
    import application.views.screen.office.AllowButton;
    import application.views.screen.office.CheckButton;
    import application.views.screen.office.DeniedResolution;
    import application.views.screen.office.InputField;
    import application.views.text.AppTextField;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.struct.data.ModelsRegistry;

    public class LoginView extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LoginView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addInput();
            _addNote();
            _addButtons();
        }

        override public function destroy():void
        {
            _unit.destroy();
            _input.destroy();
            _deniedLogin.destroy();
            _allowLogin.destroy();
            _checkButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            if (_unit && !_unit.destroyed)
                _unit.destroy();
            _addUnit();
        }

        public function loginDeny():void
        {
            _declineLogin.push(_typedLogin);
            _loginDecline();
        }

        public function loginAllow():void
        {
            _acceptLogin.push(_typedLogin);
            _loginAccept();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _input:InputField;
        private var _checkButton:CheckButton;
        private var _allowLogin:AllowButton;
        private var _deniedLogin:DeniedResolution;

        private var _unit:CharsChanger;

        private var _vegetable:Vegetable = Vegetable.TOMATO;
        private var _details:Array = [];

        private var _acceptLogin:Array = ["123"];
        private var _declineLogin:Array = [];

        private var _typedLogin:String = "";
        private var _ready:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addInput():void
        {
            _input = new InputField();
            _input.w = 226;
            addChild(_input);
            _input.x = 344;
            _input.y = 218;
            _input.txtField.text = TextFactory.instance.getLabel("default_login");

            _declineLogin.push(TextFactory.instance.getLabel("default_login"));
            _input.txtField.addEventListener(Event.CHANGE, _textChangeHandler);
        }

        private function _addNote():void
        {
            var note:AppTextField = new AppTextField();
            note.x = 355;
            note.y = 260;
            note.height = 40;
            note.size = 10;
            note.width = 230;
            note.multiline = true;
            note.wordWrap = true;
            note.color = AppTextField.RED;
            addChild(note);
            note.text = TextFactory.instance.getLabel("register_login_note");
        }

        private function _addButtons():void
        {
            _checkButton = new CheckButton();
            addChild(_checkButton);
            _checkButton.x = 576;
            _checkButton.y = 218;
            _checkButton.visible = false;
            _checkButton.addEventListener(MouseEvent.CLICK, _clickHandler);

            _allowLogin = new AllowButton();
            _allowLogin.x = 576;
            _allowLogin.y = 218;
            addChild(_allowLogin);
            _allowLogin.visible = false;

            _deniedLogin = new DeniedResolution();
            _deniedLogin.x = 576;
            _deniedLogin.y = 218;
            addChild(_deniedLogin);
            _deniedLogin.visible = true;
        }

        private function _addUnit():void
        {
            var chars:CharList = ModelsRegistry.getModel(ModelData.CHAR_LIST) as CharList;
            var userInfo:UserInfo = chars.getDefaultBuildConfiguration(_vegetable);

            _unit = new CharsChanger(userInfo, new Vector.<Item>());
            addChild(_unit);

            _unit.x = 200;
            _unit.y = 218;

            if (_unit.templateType == TemplateType.LONG) {
                _unit.x = 180;
                _unit.y = 238;
            }

            _unit.scaleX = -.8;
            _unit.scaleY = .8;

            for (var i:int = 0; i < _details.length; i++) {
                var char:CharDetail = _details[i] as CharDetail;
                _unit.change(char);
            }
        }

        private function _checkLogin():void
        {
            _deniedLogin.visible = false;
            _allowLogin.visible = false;
            _checkButton.visible = true;
            _ready = false;
        }

        private function _loginDecline():void
        {
            _deniedLogin.visible = true;
            _allowLogin.visible = false;
            _checkButton.visible = false;
            _ready = false;
        }

        private function _loginAccept():void
        {
            _deniedLogin.visible = false;
            _allowLogin.visible = true;
            _checkButton.visible = false;
            _ready = true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _textChangeHandler(event:Event):void
        {
            _typedLogin = _input.txtField.text;

            if (_declineLogin.indexOf(_typedLogin) > -1) {
                _loginDecline();
            } else {
                if (_acceptLogin.indexOf(_typedLogin) > -1) {
                    _loginAccept();
                } else {
                    if (LoginHelper.asseert(_typedLogin))
                        _checkLogin();
                    else
                        _loginDecline();
                }
            }
        }

        private function _clickHandler(e:MouseEvent):void
        {
            var event:OfficeEvent = new OfficeEvent(OfficeEvent.CHECK_LOGIN, true);
            event.login = _typedLogin;
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set vegetable(value:Vegetable):void
        {
            _vegetable = value;
        }

        public function set details(value:Array):void
        {
            _details = value;
        }

        public function get login():String
        {
            return _typedLogin;
        }

        public function get ready():Boolean
        {
            return _ready;
        }
    }
}
