/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.10.13
 * Time: 15:38
 */
package application.service.register.login
{
    import application.service.register.RegisterControl;
    import application.views.TextFactory;
    import application.views.register.RegisterView;

    public class LoginControl extends RegisterControl
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LoginControl(root:RegisterView)
        {
            super(root);
            caption = TextFactory.instance.getLabel("choose_login_label");
            _addView();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        override public function done():void
        {
            _root.removeChild(_view);
        }

        override public function open():void
        {
            _root.addChild(_view);
            _view.vegetable = _root.vegetable;
            _view.details = _root.details;
            _view.update();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function loginDeny():void
        {
            _view.loginDeny();
        }

        public function loginAllow():void
        {
            _view.loginAllow();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:LoginView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addView():void
        {
            _view = new LoginView();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get ready():Boolean
        {
            return _view.ready;
        }

        public function get login():String
        {
            return _view.login;
        }
    }
}
