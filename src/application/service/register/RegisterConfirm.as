/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.10.13
 * Time: 11:28
 */
package application.service.register
{
    import application.sound.lib.UISound;
    import application.views.TextFactory;
    import application.views.popup.BasePopup;
    import application.views.screen.common.buttons.RejectButton;
    import application.views.screen.common.buttons.SpecialButton;
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    public class RegisterConfirm extends BasePopup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const EVENT_PIC:String = "EVENT_PICS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RegisterConfirm(callback:Function)
        {
            super();
            _callback = callback;
            _popupWidth = 310;
            _popupHeight = 302;
            _header = TextFactory.instance.getLabel("register_complete_header");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            _addEventPic();
            _addMessage();
            _addButtons();
        }

        override public function destroy():void
        {
            _txtMessage.destroy();
            _rejectButton.destroy();
            _okButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtMessage:AppTextField;
        private var _okButton:SpecialButton;
        private var _rejectButton:RejectButton;

        private var _callback:Function;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addMessage():void
        {
            _txtMessage = new AppTextField();
            _txtMessage.color = 0x5e3113;
            _txtMessage.size = 12;
            _txtMessage.width = 280;
            _txtMessage.multiline = true;
            _txtMessage.wordWrap = true;
            _txtMessage.align = TextFormatAlign.CENTER;
            _txtMessage.autoSize = TextFieldAutoSize.LEFT;
            addChild(_txtMessage);
            _txtMessage.text = TextFactory.instance.getLabel('register_complete_text');
            _txtMessage.x = _backShape.x + _popupWidth / 2 - _txtMessage.width / 2;
            _txtMessage.y = _backShape.y + 178;
        }

        private function _addEventPic():void
        {
            var eventPic:Bitmap = super.source.getBitmap(EVENT_PIC);
            eventPic.scrollRect = new Rectangle(0, 0, 280, 100);
            addChild(eventPic);
            eventPic.x = _backShape.x + _popupWidth / 2 - eventPic.width / 2;
            eventPic.y = _backShape.y + 56;
        }

        private function _addButtons():void
        {
            _okButton = new SpecialButton(TextFactory.instance.getLabel("register_ok_label"));
            _okButton.w = 126;
            _okButton.fontSize = 12;

            _rejectButton = new RejectButton(TextFactory.instance.getLabel("register_think"));
            _rejectButton.w = 126;
            _rejectButton.fontSize = 12;

            addChild(_okButton);
            addChild(_rejectButton);

            _okButton.x = _backShape.x + 18;
            _okButton.y = _backShape.y + 241;

            _rejectButton.x = _backShape.x + 154;
            _rejectButton.y = _backShape.y + 241;

            _rejectButton.addEventListener(MouseEvent.CLICK, _rejectHandler);
            _okButton.addEventListener(MouseEvent.CLICK, _okClickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _rejectHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            this.destroy();
        }

        private function _okClickHandler(event:MouseEvent):void
        {
            if (_callback is Function)
                _callback.call();
            this.destroy();
            super.sound.playUISound(UISound.CLICK);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
