package application.service.items
{
    import application.config.AppConfig;
    import application.views.AppView;
    import application.views.TextFactory;
    import application.views.screen.base.ScreenTitle;
    import application.views.screen.common.buttons.AcceptButton;
    import application.views.screen.common.buttons.CloseButton;
    import application.views.screen.common.navigation.Navigation;
    import application.views.screen.home.HomeScreen;
    import application.views.screen.inventory.mesh.InventoryMesh;

    import flash.display.Bitmap;

    public class ItemsSelectorLayout extends AppView
    {
        public static const BACK:String = HomeScreen.STALL;


        private var _screenTitle:ScreenTitle;
        private var _closeButton:CloseButton;
        private var _confirmButton:AcceptButton;
        private var _navigation:Navigation;
        private var _mesh:InventoryMesh;

        public function ItemsSelectorLayout()
        {
            super();
        }

        override protected function render():void
        {
            _drawBackground();
            _addTitle();
            _addStall();
            _addCloseButton();
            _addConfirmButton();
            _addPageCounter();
            _addMesh();
        }

        private function _drawBackground():void
        {
            graphics.beginFill(0x000, .8);
            graphics.drawRect(0, 0, AppConfig.APP_WIDTH, AppConfig.APP_HEIGHT);
            graphics.endFill();
        }

        private function _addTitle():void
        {
            _screenTitle = new ScreenTitle(TextFactory.instance.getLabel("choose_item_label"));
            addChild(_screenTitle);
            _screenTitle.x = 225;
            _screenTitle.y = 90;
        }

        private function _addStall():void
        {
            var stall:Bitmap = super.source.getBitmap(BACK);
            addChild(stall);
            stall.x = 177;
            stall.y = 153;
        }

        private function _addCloseButton():void
        {
            _closeButton = new CloseButton();
            addChild(_closeButton);
            _closeButton.x = 548;
            _closeButton.y = 135;
        }

        private function _addConfirmButton():void
        {
            _confirmButton = new AcceptButton(TextFactory.instance.getLabel("accept_label"));
            _confirmButton.w = 126;
            _confirmButton.x = 421;
            _confirmButton.y = 360;
            addChild(_confirmButton);
        }

        private function _addPageCounter():void
        {
            _navigation = new Navigation();
            _navigation.x = 202;
            _navigation.y = 360;
            _navigation.arrowMargin = 3;
            addChild(_navigation);
            _navigation.totalPages = 10;
        }

        private function _addMesh():void
        {
            _mesh = new InventoryMesh();
            _mesh.x = 215;
            _mesh.y = 182;
            addChild(_mesh);
        }

        override public function destroy():void
        {
            _screenTitle.destroy();
            _closeButton.destroy();
            _mesh.destroy();
            super.destroy();
        }

        public function get mesh():InventoryMesh
        {
            return _mesh;
        }

        public function get navigation():Navigation
        {
            return _navigation;
        }

        public function get confirmButton():AcceptButton
        {
            return _confirmButton;
        }
    }
}
