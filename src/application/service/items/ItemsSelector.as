/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.12.13
 * Time: 19:53
 */
package application.service.items
{
    import application.events.ApplicationEvent;
    import application.models.inventory.InventoryItem;
    import application.sound.lib.UISound;
    import application.views.AppView;
    import application.views.screen.common.cell.Cell;
    import application.views.screen.common.cell.InventoryCell;
    import application.views.screen.home.HomeScreen;

    import flash.events.Event;
    import flash.events.MouseEvent;

    public class ItemsSelector extends AppView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = HomeScreen.STALL;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ItemsSelector(items:Array)
        {
            super();
            _items = items;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _renderLayout();
            _fillMesh();
            _setNavigation();
            _bindReactions();
        }

        override public function destroy():void
        {
            _layout.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _layout:ItemsSelectorLayout;
        private var _start:int = 0;
        private var _limit:int = 8;
        private var _items:Array;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _renderLayout():void
        {
            _layout = new ItemsSelectorLayout();
            addChild(_layout);
        }

        private function _fillMesh():void
        {
            var cells:Vector.<Cell> = new Vector.<Cell>();
            var items:Array = _items.slice(_start);

            for (var key:String in items) {
                var item:InventoryItem = items[key];
                if (item.count.value - item.temp_put.value > 0)
                    cells.push(new InventoryCell(item));
            }

            _layout.mesh.clear();
            _layout.mesh.cells = cells;
            _layout.mesh.draw();
        }

        private function _bindReactions():void
        {
            addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _layout.confirmButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _setNavigation():void
        {
            _layout.navigation.totalPages = Math.ceil(_getTotalItems() / _limit);
            _layout.navigation.currentPage = 1;

            _layout.navigation.addEventListener(Event.CHANGE, _changeHandler);
        }

        private function _getTotalItems():int
        {
            return _items.length;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(event:ApplicationEvent):void
        {
            destroy();
        }

        private function _changeHandler(event:Event):void
        {
            _start = (_layout.navigation.currentPage - 1) * _limit;
            _fillMesh();
        }

        private function _clickHandler(event:MouseEvent):void
        {
            super.sound.playUISound(UISound.CLICK);
            if (item)
                dispatchEvent(new Event(Event.SELECT, true));

        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get item():InventoryItem
        {
            return _layout.mesh.item;
        }

    }
}
