/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.09.13
 * Time: 20:14
 */
package application.service.model
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.chars.CharList;
    import application.models.user.Vegetable;
    import application.server_api.ApiMethods;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class GetAllChars extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GetAllChars(appController:ApplicationController)
        {
            super();
            _appController = appController;
            _model = ModelsRegistry.getModel(ModelData.CHAR_LIST) as CharList;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_model.isEmpty)
                _loadData();
            else
                super.complete();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _model:CharList;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _loadData():void
        {
            var params:Object = {
                "vegetable": Vegetable.ALL.toString()
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.GET_CHARS, params, _onServerSuccess);
        }

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);

            super.complete();
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
