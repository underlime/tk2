/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 09.10.13
 * Time: 14:39
 */
package application.service.model
{
    import application.controllers.ApplicationController;
    import application.models.user.LoginModel;
    import application.server_api.ApiMethods;

    import framework.core.task.Task;
    import framework.core.task.TaskEvent;
    import framework.core.underquery.ApiRequestEvent;

    public class CheckLogin extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CheckLogin(appController:ApplicationController, login:String)
        {
            super();
            _appController = appController;
            _login = login;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _sendQuery();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _login:String;
        private var _loginModel:LoginModel = new LoginModel();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            var params:Object = {
                "login": _login
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.CHECK_LOGIN, params, _onLoginSuccess);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onLoginSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();
            var data:Object = e.data;
            if (data['login_allowable'])
                _loginModel.importData(data['login_allowable']);

            dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));

            return false;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get loginModel():LoginModel
        {
            return _loginModel;
        }
    }
}
