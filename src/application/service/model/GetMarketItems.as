/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.10.13
 * Time: 15:14
 */
package application.service.model
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.market.MarketItems;
    import application.server_api.ApiMethods;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class GetMarketItems extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GetMarketItems(appController:ApplicationController)
        {
            super();
            _appController = appController;
            _model = ModelsRegistry.getModel(ModelData.MARKET_ITEMS) as MarketItems;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_model.isEmpty)
                _loadData();
            else
                super.complete();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _model:MarketItems;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _loadData():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.ITEMS_GET_MARKET_LIST, {}, _onServerSuccess);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);

            trace("all items loaded");
            super.complete();
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
