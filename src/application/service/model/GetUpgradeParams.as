/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.12.13
 * Time: 11:46
 */
package application.service.model
{
    import application.controllers.ApplicationController;
    import application.models.forge.ForgeParams;
    import application.models.inventory.InventoryHelper;
    import application.models.inventory.InventoryItem;
    import application.server_api.ApiMethods;

    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class GetUpgradeParams extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GetUpgradeParams(appController:ApplicationController, item:InventoryItem)
        {
            super();
            _appController = appController;
            _item = item;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _loadData();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _item:InventoryItem;

        private var _forgeParams:ForgeParams = new ForgeParams();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _loadData():void
        {
            var params:Object = {
                "item_id": _item.item_id.value,
                "level": InventoryHelper.getNextLevel(_item)
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.GET_SHARPENING_PARAMS, params, _onServerSuccess);
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            var obData:Object = e.data;
            if (obData['sharpening_params'])
                _forgeParams.importData(obData['sharpening_params']);
            _complete();
            return false;
        }

        private function _complete():void
        {
            _appController.loadingView.hideLoader();
            super.complete();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get forgeParams():ForgeParams
        {
            return _forgeParams;
        }
    }
}
