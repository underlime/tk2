/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.02.14
 * Time: 16:36
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.models.clans.ClanInfo;
    import application.models.clans.ClansRegistry;
    import application.server_api.ApiMethods;

    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class GetClanMembers extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GetClanMembers(appController:ApplicationController, clan_id:int)
        {
            super();
            _appController = appController;
            _clan_id = clan_id;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _clanInfo = ClansRegistry.getClan(_clan_id);
            if (_clanInfo) {
                if (_clanInfo.clan_members.isEmpty)
                    _sendQuery();
                else
                    complete();
            } else {
                throw new Error("Clan [" + _clan_id.toString() + "] not found");
            }
        }

        private function _sendQuery():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.CLAN_MEMBERS, {'clan_id': _clan_id}, _onSuccess);
        }

        private function _onSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();

            if (e.data['clan_members']) {
                _clanInfo.clan_members.importData(e.data['clan_members']);
                ClansRegistry.add(_clanInfo);
            }

            super.complete();
            return false;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _clan_id:int;

        private var _clanInfo:ClanInfo;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
