/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.03.14
 * Time: 15:53
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.server_api.ApiMethods;

    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class LeaveClanAction extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LeaveClanAction(appController:ApplicationController, user_id:int)
        {
            super();
            _appController = appController;
            _user_id = user_id;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _sendQuery();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _user_id:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.LEAVE_CLAN, {'user_id': _user_id}, _onSuccess);
        }

        private function _onSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();
            super.complete();
            return true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
