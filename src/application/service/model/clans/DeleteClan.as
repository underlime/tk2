/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.02.14
 * Time: 12:05
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.models.clans.ClanInfo;
    import application.server_api.ApiMethods;

    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class DeleteClan extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DeleteClan(appController:ApplicationController, clanInfo:ClanInfo)
        {
            super();
            _appController = appController;
            _clanInfo = clanInfo;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _sendQuery();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _action:String = ApiMethods.DELETE_CLAN;

        private var _appController:ApplicationController;
        private var _clanInfo:ClanInfo;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(_action, {'clan_id': _clanInfo.id.value}, _onSuccess);
        }

        private function _onSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();
            _clanInfo.deleteClan();
            super.complete();
            return false;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
