/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.03.14
 * Time: 20:24
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.clans.ClanRequests;
    import application.server_api.ApiMethods;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class GetClanRequests extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GetClanRequests(appController:ApplicationController)
        {
            super();
            _appController = appController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_requests.isEmpty) {
                _sendQuery();
            } else {
                complete();
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _requests:ClanRequests = ModelsRegistry.getModel(ModelData.CLAN_REQUESTS) as ClanRequests;
        private var _appController:ApplicationController;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onReady);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.CLAN_GET_REQUESTS, {}, _onServerSuccess);
        }

        private function _onReady(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onReady);

            complete()
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
