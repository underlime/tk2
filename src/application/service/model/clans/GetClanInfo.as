/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.02.14
 * Time: 15:20
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.models.clans.ClanInfo;
    import application.models.clans.ClansRegistry;
    import application.server_api.ApiMethods;

    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class GetClanInfo extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GetClanInfo(appController:ApplicationController, clan_id:int)
        {
            super();
            _appController = appController;
            _clan_id = clan_id;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (!ClansRegistry.has(_clan_id))
                _sendQuery();
            else
                complete();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _clan_id:int;

        private var _clanInfo:ClanInfo;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.CLAN_INFO, {'clan_id': _clan_id}, _onSuccess);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();
            _clanInfo = new ClanInfo();

            if (e.data['clan_info']) {
                _clanInfo.importData(e.data['clan_info']);
                ClansRegistry.add(_clanInfo);
            }

            super.complete();
            return false;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
