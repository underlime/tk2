/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.02.14
 * Time: 20:49
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.server_api.ApiMethods;

    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class SendInviteRequest extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SendInviteRequest(appController:ApplicationController, clan_id:int)
        {
            super();
            _appController = appController;
            _clan_id = clan_id;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _sendQuery();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _clan_id:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.CLAN_SEND_REQUEST, {'clan_id': _clan_id}, _onSuccess);
        }

        private function _onSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();
            super.complete();
            return false;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
