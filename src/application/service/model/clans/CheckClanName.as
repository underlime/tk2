/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.02.14
 * Time: 16:16
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.models.clans.ClanNameModel;
    import application.server_api.ApiMethods;

    import framework.core.task.TaskEvent;
    import framework.core.underquery.ApiRequestEvent;

    public class CheckClanName extends framework.core.task.Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CheckClanName(appController:ApplicationController, name:String)
        {
            super();
            _appController = appController;
            _name = name;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _sendQuery();
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _name:String;
        private var _clanNameModel:ClanNameModel = new ClanNameModel();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            var params:Object = {
                "name": _name
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.VALIDATE_CLAN_NAME, params, _onLoginSuccess);
        }

        private function _onLoginSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();
            var data:Object = e.data;
            if (data['clan_name_validation_results'])
                _clanNameModel.importData(data['clan_name_validation_results']);

            dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));

            return false;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get clanNameModel():ClanNameModel
        {
            return _clanNameModel;
        }

    }
}
