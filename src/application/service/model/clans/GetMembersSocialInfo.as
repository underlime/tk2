package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.models.clans.ClanInfo;
    import application.models.clans.ClanMember;
    import application.models.clans.ClansRegistry;

    import framework.core.task.Task;

    public class GetMembersSocialInfo extends Task
    {
        private var _appController:ApplicationController;
        private var _clanID:int;
        private var _clanInfo:ClanInfo;
        private var _members:Object;

        public function GetMembersSocialInfo(appController:ApplicationController, clanID:int)
        {
            super();
            _appController = appController;
            _clanID = clanID;
        }

        override public function execute():void
        {
            _clanInfo = ClansRegistry.getClan(_clanID);
            if (_clanInfo && _clanInfo.clan_members.length) {
                _members = _clanInfo.clan_members.list.value;
                _requestData();
            }
            else {
                complete();
            }
        }

        private function _requestData():void
        {
            var socNetIds:Array = [];
            for each (var rec:ClanMember in _members) {
                socNetIds.push(rec.soc_net_id.value);
            }
            _appController.socNet.getUsersListInfo(socNetIds, _onSocNetData);
        }

        private function _onSocNetData(data:Object):void
        {
            var usersInfo:Object = {};
            if (data["users_list"]) {
                usersInfo = data["users_list"];
                if (usersInfo["_init"]) {
                    usersInfo = usersInfo["_init"];
                }
                else
                    if (usersInfo["_add"]) {
                        usersInfo = usersInfo["_add"];
                    }
            }
            var usersTable:Object = {};
            for each (var socNetRecord:Object in usersInfo) {
                usersTable[socNetRecord.social_id] = socNetRecord;
            }

            for each (var rec:ClanMember in _clanInfo.clan_members.list.value) {
                var socNetId:String = rec.soc_net_id.value;
                if (usersTable.hasOwnProperty(socNetId)) {
                    rec.importData(usersTable[socNetId]);
                }
            }

            complete();
        }

        override public function destroy():void
        {
            _appController = null;
            _clanInfo = null;
            _members = null;
            super.destroy();
        }
    }
}
