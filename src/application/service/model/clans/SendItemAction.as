/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.03.14
 * Time: 19:54
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.server_api.ApiMethods;

    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class SendItemAction extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SendItemAction(appController:ApplicationController, item_id:int, to_user_id:int)
        {
            super();
            _appController = appController;
            _item_id = item_id;
            _to_user_id = to_user_id;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _sendQuery();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _item_id:int;
        private var _to_user_id:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            var params:Object = {
                "item_id": _item_id,
                "count": 1,
                "to_user_id": _to_user_id
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.ITEM_TRANSFER, params, _onServerSuccess);
        }

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            super.complete();
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
