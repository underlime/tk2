package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.clans.ClanRequestRecord;
    import application.models.clans.ClanRequests;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.Task;

    public class GetRequestsSocialInfo extends Task
    {
        private var _appController:ApplicationController;
        private var _requests:Vector.<ClanRequestRecord> = new <ClanRequestRecord>[];

        public function GetRequestsSocialInfo(appController:ApplicationController)
        {
            super();
            _appController = appController;
        }

        override public function execute():void
        {
            var model:ClanRequests = ModelsRegistry.getModel(ModelData.CLAN_REQUESTS) as ClanRequests;
            var socNetIds:Array = [];
            for each (var data:ClanRequestRecord in model.list.value) {
                _requests.push(data);
                socNetIds.push(data.soc_net_id.value);
            }
            _appController.socNet.getUsersListInfo(socNetIds, _onSocNetData);
        }

        private function _onSocNetData(data:Object):void
        {
            var usersInfo:Object = {};
            if (data["users_list"]) {
                usersInfo = data["users_list"];
                if (usersInfo["_init"]) {
                    usersInfo = usersInfo["_init"];
                }
                else if (usersInfo["_add"]) {
                    usersInfo = usersInfo["_add"];
                }
            }
            var usersTable:Object = {};
            for each (var socNetRecord:Object in usersInfo) {
                usersTable[socNetRecord.social_id] = socNetRecord;
            }

            for each (var rec:ClanRequestRecord in _requests) {
                var socNetId:String = rec.soc_net_id.value;
                if (usersTable.hasOwnProperty(socNetId)) {
                    rec.importData(usersTable[socNetId]);
                }
            }
            complete();
        }

        override public function destroy():void
        {
            _appController = null;
            _requests = null;
            super.destroy();
        }
    }
}
