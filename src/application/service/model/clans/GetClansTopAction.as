/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.03.14
 * Time: 18:19
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.clans.ClansTop;
    import application.server_api.ApiMethods;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class GetClansTopAction extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GetClansTopAction(appController:ApplicationController)
        {
            super();
            _appController = appController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_model.clans_top_glory.isEmpty) {
                _sendQuery();
            } else {
                super.complete();
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _model:ClansTop = ModelsRegistry.getModel(ModelData.CLANS_TOP) as ClansTop;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.GET_CLANS_TOP, {}, _onServerSuccess);
        }

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);

            super.complete();
        }

        private function _onServerSuccess(event:ApiRequestEvent):Boolean
        {
            if (event.data['clans_top_glory']) {
                _model.clans_top_glory.importData(event.data['clans_top_glory']);
            }
            return true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
