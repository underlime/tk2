/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 01.03.14
 * Time: 21:03
 */
package application.service.model.clans
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.user.User;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class ClanRequestResolution extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanRequestResolution(appController:ApplicationController, action:String, user_id:int)
        {
            super();
            _appController = appController;
            _action = action;
            _user_id = user_id;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .makeRequest(_action, {"user_id": _user_id}, _onSuccess);
        }

        private function _onSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.loadingView.hideLoader();
            if (e.data['clan_members']) {
                _user.clanInfo.clan_members.importData(e.data['clan_members']);
            }
            super.complete();
            return true;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _action:String;
        private var _user_id:int;
        private var _user:User = ModelsRegistry.getModel(ModelData.USER) as User;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
