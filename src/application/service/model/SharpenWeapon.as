/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.12.13
 * Time: 13:20
 */
package application.service.model
{
    import application.common.Currency;
    import application.controllers.ApplicationController;
    import application.server_api.ApiMethods;

    import framework.core.struct.data.fields.BooleanField;
    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class SharpenWeapon extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SharpenWeapon(appController:ApplicationController, item_id:int, currency:Currency)
        {
            super();
            _appController = appController;
            _item_id = item_id;
            _currency = currency;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _sendData();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _item_id:int;
        private var _currency:Currency;

        private var _success:BooleanField = new BooleanField();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendData():void
        {
            var params:Object = {
                "item_id": _item_id,
                "currency": _currency.toString()
            };

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.SHARPEN_WEAPON, params, _onServerSuccess);
        }

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);

            super.complete();
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            var data:Object = e.data;
            if (data['sharpen_weapon_result']) {
                var result:Object = data['sharpen_weapon_result'];
                if (result['success']) {
                    _success.importValue(result['success']);
                }
            }
            return true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get success():Boolean
        {
            return _success.value;
        }
    }
}
