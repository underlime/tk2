/**
 * Created by acvetkov on 04.12.13.
 */
package application.service.model
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.gmo.GmoPrices;
    import application.server_api.ApiMethods;

    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class GetGmoPrices extends Task
    {
        private var _appController:ApplicationController;
        private var _model:GmoPrices;

        public function GetGmoPrices(appController:ApplicationController)
        {
            _appController = appController;
            _model = ModelsRegistry.getModel(ModelData.GMO_PRICES) as GmoPrices;
        }

        override public function execute():void
        {
            if (_model.isEmpty)
                _loadData();
            else
                super.complete();
        }

        private function _loadData():void
        {
            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.GET_GMO_PRICES, {}, _onServerSuccess);
        }

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);

            super.complete();
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }
    }
}
