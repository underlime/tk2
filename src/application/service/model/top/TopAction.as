/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.03.14
 * Time: 19:33
 */
package application.service.model.top
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.top.TopInterval;
    import application.models.top.TopModel;
    import application.models.top.TopRecord;
    import application.models.top.TopType;
    import application.models.user.User;
    import application.server_api.ApiMethods;

    import framework.core.struct.data.ListData;
    import framework.core.struct.data.ModelsRegistry;
    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class TopAction extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TopAction(appController:ApplicationController, type:TopType, interval:TopInterval)
        {
            super();
            _appController = appController;
            _type = type;
            _interval = interval;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _data = _model.getTop(_type, _interval);
            if (_data.isEmpty) {
                _sendQuery();
            } else {
                super.complete();
            }
        }

        private function _sendQuery():void
        {
            var params:Object = {
                "interval": _interval.toString(),
                "city_code": _user.userInfo.city.value
            };

            _appController.loadingView.showLoader();
            _appController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onComplete);
            _appController.serverApi.makeRequest(_getApiMethod(), params, _onServerSuccess);
        }


        override public function destroy():void
        {
            _appController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onComplete);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _type:TopType;
        private var _interval:TopInterval;

        private var _user:User = ModelsRegistry.getModel(ModelData.USER) as User;
        private var _model:TopModel = ModelsRegistry.getModel(ModelData.TOP_MODEL) as TopModel;
        private var _data:ListData;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getApiMethod():String
        {
            switch (_type) {
                case TopType.GLOBAL:
                    return ApiMethods.TOP_GLORY_GLOBAL;
                case TopType.FRIENDS:
                    return ApiMethods.TOP_GLORY_FRIENDS;
                case TopType.CITY:
                    return ApiMethods.TOP_GLORY_CITY;
            }
            return ApiMethods.TOP_GLORY_GLOBAL;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            return true;
        }

        private function _onComplete(event:ApiRequestEvent):void
        {
            _appController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onComplete);
            _getSocialInfo();
        }

        private function _getSocialInfo():void
        {
            var socNetIdsList:Array = [];
            for each (var record:TopRecord in _data.list.value)
                socNetIdsList.push(record.soc_net_id.value);
            _appController.socNet.getUsersListInfo(socNetIdsList, _onSocNetData);
        }

        private function _onSocNetData(data:Object):void
        {
            var usersInfo:Object = {};
            if (data["users_list"]) {
                usersInfo = data["users_list"];
                if (usersInfo["_init"])
                    usersInfo = usersInfo["_init"];
                else
                    if (usersInfo["_add"])
                        usersInfo = usersInfo["_add"];
            }

            var usersTable:Object = {};
            for each (var socNetRecord:Object in usersInfo)
                usersTable[socNetRecord.social_id] = socNetRecord;

            for each (var record:TopRecord in _data.list.value)
                record.importData(usersTable[record.soc_net_id.value]);

            _appController.loadingView.hideLoader();
            super.complete();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
