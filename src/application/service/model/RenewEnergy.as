/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.10.13
 * Time: 16:21
 */
package application.service.model
{
    import application.controllers.ApplicationController;
    import application.server_api.ApiMethods;

    import framework.core.task.Task;
    import framework.core.underquery.ApiRequestEvent;

    public class RenewEnergy extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RenewEnergy(appController:ApplicationController)
        {
            super();
            _appController = appController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.RENEW_ENERGY, {}, _onServerSuccess);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            trace("энергия синхронизирована");
            return true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
