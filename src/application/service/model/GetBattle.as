/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.10.13
 * Time: 16:36
 */
package application.service.model
{
    import application.controllers.ApplicationController;
    import application.server_api.ApiMethods;

    import framework.core.task.TaskEvent;
    import framework.core.underquery.ApiRequestEvent;

    public class GetBattle extends framework.core.task.Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GetBattle(appController:ApplicationController, enemy_id:int)
        {
            super();
            _appController = appController;
            _enemy_id = enemy_id;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _sendQuery();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appController:ApplicationController;
        private var _ease:Boolean = false;
        private var _enemy_id:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _sendQuery():void
        {
            var params:Object = {
                "enemy_id": _enemy_id
            };

            if (ease)
                params['ease_enemy'] = 1;

            _appController.loadingView.showLoader();
            _appController
                    .serverApi
                    .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onBattleReady);
            _appController
                    .serverApi
                    .makeRequest(ApiMethods.FIGHT, params, _onServerSuccess);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onBattleReady(event:ApiRequestEvent):void
        {
            _appController.loadingView.hideLoader();
            _appController
                    .serverApi
                    .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onBattleReady);

            dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            trace("success");
            return true;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get ease():Boolean
        {
            return _ease;
        }

        public function set ease(value:Boolean):void
        {
            _ease = value;
        }

        public function get enemy_id():int
        {
            return _enemy_id;
        }

        public function set enemy_id(value:int):void
        {
            _enemy_id = value;
        }
    }
}
