/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 23.07.13
 * Time: 21:48
 */
package application.service.battle.special
{
    import framework.core.enum.BaseEnum;

    public class SpecialType extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;
        public static var ENUM:Vector.<SpecialType> = new Vector.<SpecialType>();

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const DOUBLE:SpecialType = new SpecialType("double_kick");
        public static const RAGE:SpecialType = new SpecialType("RAGE_STRIKES");
        public static const BONE_BREAKER:SpecialType = new SpecialType("xray");
        public static const STALAGMITE:SpecialType = new SpecialType("stalagmit");
        public static const FORCE_BLOW:SpecialType = new SpecialType("force_blow");
        public static const TELEKINESIS:SpecialType = new SpecialType("Telekinez");
        public static const HEAVY_HELPER:SpecialType = new SpecialType("valera");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        public static function getEnumByKey(key:String):SpecialType
        {
            var count:int = ENUM.length;
            for (var i:int = 0; i < count; i++) {
                if (ENUM[i].toString() == key)
                    return ENUM[i];
            }
            throw new Error("Special not found");
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialType(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
            ENUM.push(this);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.service.battle.special.SpecialType;

SpecialType.lockUp();