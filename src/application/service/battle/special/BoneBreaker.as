/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.07.13
 * Time: 14:04
 */
package application.service.battle.special
{
    import application.models.battle.step.BattleStepRecord;
    import application.models.user.Vegetable;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;
    import application.views.constructor.helpers.AnimationHelper;

    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.task.TaskEvent;
    import framework.core.tools.SourceManager;

    public class BoneBreaker extends SpecialAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOMATO:String = "tomatoXrayBoneBreaker";
        public static const CUCUMBER:String = "cucumberXrayBoneBreaker";
        public static const BETA:String = "betaXrayBoneBreaker";
        public static const CARROT:String = "carrotXrayBoneBreaker";
        public static const ONION:String = "onionXrayBoneBreaker";
        public static const PEPPER:String = "pepperXrayBoneBreaker";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BoneBreaker(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user, enemy, stepModel);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _specialType = SpecialType.BONE_BREAKER;
            super._addSpecialHint();

            super.observer.swapUnits(_user, _enemy);

            _setupHash();
            _user.addEventListener(Event.COMPLETE, _completeHandler);
            _user.cast();
            super.sound.playBattleSound(BattleSound.CAST);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _hash:Object = {};

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupHash():void
        {
            _hash[Vegetable.TOMATO.toString()] = TOMATO;
            _hash[Vegetable.CUCUMBER.toString()] = CUCUMBER;
            _hash[Vegetable.BETA.toString()] = BETA;
            _hash[Vegetable.CARROT.toString()] = CARROT;
            _hash[Vegetable.ONION.toString()] = ONION;
            _hash[Vegetable.PEPPER.toString()] = PEPPER;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeHandler(event:Event):void
        {
            _user.removeEventListener(Event.COMPLETE, _completeHandler);
            if (_hash[_enemy.vegetable.toString()]) {
                var src:String = _hash[_enemy.vegetable.toString()];
                var animation:MovieClip = SourceManager.instance.getMovieClip(src);
                animation.x = 0;
                animation.scaleX = 1;

                if (!_enemy.isEnemy) {
                    animation.x = 750;
                    animation.scaleX = -1;
                }

                super.observer.container.addChild(animation);
                var helper:AnimationHelper = new AnimationHelper(animation);
                helper.addEventListener(Event.COMPLETE, _animationCompleteHandler)
                helper.execute();
                super.sound.playBattleSound(BattleSound.BONE_BREAKER);
            } else {
                throw new Error("Undefined Vegetable!");
            }

            super._showBubbleHint();
            super.observer.reduceHealth(_stepModel.damage.value, _enemy.isEnemy);
        }

        private function _animationCompleteHandler(event:Event):void
        {
            var helper:AnimationHelper = event.currentTarget as AnimationHelper;
            if (super.observer.container.contains(helper.movie))
                super.observer.container.removeChild(helper.movie)

            helper.removeEventListener(Event.COMPLETE, _animationCompleteHandler)
            helper.destroy();

            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
