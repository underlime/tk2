/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.07.13
 * Time: 12:35
 */
package application.service.battle.special
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.SlideAct;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;
    import application.views.constructor.common.WeaponTemplate;

    import flash.events.Event;

    import framework.core.task.TaskEvent;

    public class RageStrikes extends SpecialAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const PERIOD:int = 2;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RageStrikes(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user, enemy, stepModel);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            super.observer.swapUnits(_user, _enemy);

            if (super.observer.isNeighborhood(super.unit, _enemy))
                _kick();
            else
                _move();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _frameCounter:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _kick():void
        {
            _specialType = SpecialType.RAGE;
            super._addSpecialHint();

            _user.addEventListener(Event.COMPLETE, _completeHandler);
            _user.addEventListener(Event.ENTER_FRAME, _enterFrameHandler);

            _user.rageStrikes();

            _enemy.multiDamage();
            super._showBubbleHint();
            super.observer.swapUnits(_user, _enemy);
            super.observer.addBlood(_enemy, true);
            super.observer.reduceHealth(_stepModel.damage.value, _enemy.isEnemy);
        }

        private function _move():void
        {
            var target:int = _enemy.isEnemy ? _enemy.sector - 1 : _enemy.sector + 1;
            var slideAct:SlideAct = new SlideAct(super.unit, target);
            slideAct.addEventListener(TaskEvent.COMPLETE, _moveCompleteHandler);
            slideAct.execute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeHandler(event:Event):void
        {
            _user.removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _user.removeEventListener(Event.COMPLETE, _completeHandler);
            _enemy.stay();

            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        private function _moveCompleteHandler(event:TaskEvent):void
        {
            var slideAct:SlideAct = event.currentTarget as SlideAct;
            slideAct.removeEventListener(TaskEvent.COMPLETE, _moveCompleteHandler);
            slideAct.destroy();
            _kick();
        }

        private function _enterFrameHandler(event:Event):void
        {
            if (_frameCounter % PERIOD == 0) {
                if (super.unit.weaponTemplate == WeaponTemplate.WITHOUT_WEAPON)
                    super.sound.playBattleSound(BattleSound.getPunch());
                else
                    super.sound.playBattleSound(BattleSound.getSlash());
            }

            _frameCounter++;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
