/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.07.13
 * Time: 16:28
 */
package application.service.battle.special
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.CastPositionAct;
    import application.service.battle.common.BattleSectors;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;
    import application.views.constructor.helpers.AnimationHelper;

    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.task.TaskEvent;
    import framework.core.tools.SourceManager;

    public class StalagmiteSpecial extends SpecialAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const STALAGMITE:String = "s_Stalagmit";
        public static const TOUCH_FRAME:int = 2;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StalagmiteSpecial(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user, enemy, stepModel);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _specialType = SpecialType.STALAGMITE;

            super.observer.swapUnits(_user, _enemy);

            var act:CastPositionAct = new CastPositionAct(_user, _enemy);
            act.addEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.execute();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _cast():void
        {
            super._addSpecialHint();
            _setStalagmite();
            _user.cast();
            super.sound.playBattleSound(BattleSound.CAST);
        }

        private function _setStalagmite():void
        {
            var stalagmite:MovieClip = SourceManager.instance.getMovieClip(STALAGMITE);
            super.observer.container.addChild(stalagmite);
            stalagmite.y = _enemy.y;
            stalagmite.x = 0;
            if (_enemy.isEnemy)
                stalagmite.x = _enemy.x - 30;
            else
                stalagmite.x = _enemy.x - BattleSectors.SECTOR_WIDTH + 30;

            var helper:AnimationHelper = new AnimationHelper(stalagmite);
            helper.movie.addEventListener(Event.ENTER_FRAME, _frameHandler);
            helper.addEventListener(Event.COMPLETE, _stalagmiteCompleteHandler);
            helper.execute();

            super.sound.playBattleSound(BattleSound.STALAGMITE);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _positionHandler(event:TaskEvent):void
        {
            var act:CastPositionAct = event.currentTarget as CastPositionAct;
            act.removeEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.destroy();

            _cast();
        }

        private function _stalagmiteCompleteHandler(event:Event):void
        {
            var helper:AnimationHelper = event.currentTarget as AnimationHelper;
            if (super.observer.container.contains(helper.movie))
                super.observer.container.removeChild(helper.movie);

            helper.removeEventListener(Event.COMPLETE, _stalagmiteCompleteHandler);
            helper.destroy();

            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        private function _frameHandler(event:Event):void
        {
            var movie:MovieClip = event.currentTarget as MovieClip;
            if (movie.currentFrame >= TOUCH_FRAME) {
                movie.removeEventListener(Event.ENTER_FRAME, _frameHandler);
                _enemy.damage(3);
                super.observer.addBlood(_enemy, true);
                super._showBubbleHint();
                super.observer.reduceHealth(_stepModel.damage.value, _enemy.isEnemy);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
