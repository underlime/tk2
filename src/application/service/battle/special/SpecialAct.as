/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.08.13
 * Time: 15:22
 */
package application.service.battle.special
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.BattleAct;
    import application.service.battle.common.WhoRole;
    import application.views.battle.common.hint.HintBubble;
    import application.views.battle.common.hint.SpecialHint;
    import application.views.constructor.builders.BattleUnit;

    public class SpecialAct extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialAct(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user);
            _user = user;
            _enemy = enemy;
            _stepModel = stepModel;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _user:BattleUnit;
        protected var _enemy:BattleUnit;
        protected var _stepModel:BattleStepRecord;

        protected var _specialType:SpecialType = SpecialType.BONE_BREAKER;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _showBubbleHint():void
        {
            var hint:HintBubble = new HintBubble(_stepModel.damage.value);
            hint.x = _enemy.x - 75;
            hint.y = _enemy.y;

            if (_stepModel.who_role.value == WhoRole.ENEMY.toString())
                hint.damage = true;

            if (_stepModel.block.value)
                hint.block = true;

            if (_stepModel.dodge.value)
                hint.miss = true;

            super.observer.container.addChild(hint);
            _enemy.health -= _stepModel.damage.value;
        }

        protected function _addSpecialHint():void
        {
            var specialHint:SpecialHint = new SpecialHint(_specialType);
            super.observer.container.addChild(specialHint);
            specialHint.y = 185;
            specialHint.x = 175;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
