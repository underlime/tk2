/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.07.13
 * Time: 12:27
 */
package application.service.battle.special
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.SlideAct;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;

    import flash.events.Event;

    import framework.core.task.TaskEvent;

    public class SpecialDouble extends SpecialAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialDouble(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user, enemy, stepModel);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            super.observer.swapUnits(_user, _enemy);

            if (super.observer.isNeighborhood(super.unit, _enemy))
                _kick();
            else
                _move();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _kick():void
        {
            _specialType = SpecialType.DOUBLE;
            super._addSpecialHint();

            _user.addEventListener(Event.COMPLETE, _completeHandler);
            _user.doubleKick();
            _enemy.multiDamage();
            super._showBubbleHint();
            super.observer.swapUnits(_user, _enemy);
            super.observer.addBlood(_enemy, true);
            super.observer.reduceHealth(_stepModel.damage.value, _enemy.isEnemy);
            super.sound.playBattleSound(BattleSound.CRITICAL);
        }

        private function _move():void
        {
            var target:int = _enemy.isEnemy ? _enemy.sector - 1 : _enemy.sector + 1;
            var slideAct:SlideAct = new SlideAct(super.unit, target);
            slideAct.addEventListener(TaskEvent.COMPLETE, _moveCompleteHandler);
            slideAct.execute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _moveCompleteHandler(event:TaskEvent):void
        {
            var slideAct:SlideAct = event.currentTarget as SlideAct;
            slideAct.removeEventListener(TaskEvent.COMPLETE, _moveCompleteHandler);
            slideAct.destroy();
            _kick();
        }

        private function _completeHandler(event:Event):void
        {
            _user.removeEventListener(Event.COMPLETE, _completeHandler);
            _enemy.stay();

            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
