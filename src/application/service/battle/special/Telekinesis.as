/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.07.13
 * Time: 16:58
 */
package application.service.battle.special
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.CastPositionAct;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;

    import flash.events.Event;

    import framework.core.helpers.MathHelper;
    import framework.core.task.TaskEvent;

    public class Telekinesis extends SpecialAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOUCH_FRAME:int = 41;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Telekinesis(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user, enemy, stepModel);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _specialType = SpecialType.TELEKINESIS;
            super._addSpecialHint();

            super.observer.swapUnits(_user, _enemy);

            var act:CastPositionAct = new CastPositionAct(_user, _enemy);
            act.addEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.execute();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _cast():void
        {
            _user.addEventListener(Event.COMPLETE, _completeHandler);
            _user.telekinesis();
            _user.template.addEventListener(Event.ENTER_FRAME, _frameHandler);
            super.sound.playBattleSound(BattleSound.TELEKINESIS);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _positionHandler(event:TaskEvent):void
        {
            var act:CastPositionAct = event.currentTarget as CastPositionAct;
            act.removeEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.destroy();

            _cast();
        }

        private function _frameHandler(event:Event):void
        {
            if (_user.template.currentFrame >= TOUCH_FRAME) {
                _user.template.removeEventListener(Event.ENTER_FRAME, _frameHandler);
                _enemy.damage(MathHelper.random(0, 2));
                super.observer.addBlood(_enemy, true);
                super._showBubbleHint();
                super.observer.reduceHealth(_stepModel.damage.value, _enemy.isEnemy);
            }
        }

        private function _completeHandler(event:Event):void
        {
            _user.removeEventListener(Event.COMPLETE, _completeHandler);
            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
