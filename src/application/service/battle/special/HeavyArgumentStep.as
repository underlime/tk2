/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.07.13
 * Time: 14:54
 */
package application.service.battle.special
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.CastPositionAct;
    import application.service.battle.common.BattleSectors;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;
    import application.views.constructor.helpers.AnimationHelper;

    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.task.TaskEvent;
    import framework.core.tools.SourceManager;

    public class HeavyArgumentStep extends SpecialAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const PUMPKIN:String = "sa_heavyArgument";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HeavyArgumentStep(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user, enemy, stepModel);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            super.observer.swapUnits(_user, _enemy);

            _specialType = SpecialType.HEAVY_HELPER;
            _setupPositions();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupPositions():void
        {
            var act:CastPositionAct = new CastPositionAct(_user, _enemy);
            act.addEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.execute();
        }

        private function _cast():void
        {
            super._addSpecialHint();
            _user.addEventListener(Event.COMPLETE, _castCompleteHandler);
            _user.cast();
        }

        private function _startSpecData():void
        {
            var pumpkin:MovieClip = SourceManager.instance.getMovieClip(PUMPKIN);
            super.observer.container.addChild(pumpkin);
            pumpkin.y = _enemy.y;
            pumpkin.x = 0;
            if (_enemy.isEnemy)
                pumpkin.x = _enemy.x;
            else
                pumpkin.x = _enemy.x - BattleSectors.SECTOR_WIDTH;

            var helper:AnimationHelper = new AnimationHelper(pumpkin);
            helper.addEventListener(Event.COMPLETE, _pumpkinCompleteHandler);
            helper.execute();

            super._showBubbleHint();
            super.observer.addBlood(_enemy, true);
            super.sound.playBattleSound(BattleSound.HEAVY_ARGUMENT);
            super.observer.reduceHealth(_stepModel.damage.value, _enemy.isEnemy);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _positionHandler(event:TaskEvent):void
        {
            var act:CastPositionAct = event.currentTarget as CastPositionAct;
            act.removeEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.destroy();

            _cast();
            super.sound.playBattleSound(BattleSound.CAST);
        }

        private function _castCompleteHandler(event:Event):void
        {
            _user.removeEventListener(Event.COMPLETE, _castCompleteHandler);
            _startSpecData();
        }

        private function _pumpkinCompleteHandler(event:Event):void
        {
            var helper:AnimationHelper = event.currentTarget as AnimationHelper;
            if (super.observer.container.contains(helper.movie))
                super.observer.container.removeChild(helper.movie);

            helper.removeEventListener(Event.COMPLETE, _pumpkinCompleteHandler);
            helper.destroy();

            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
