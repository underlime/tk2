/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.07.13
 * Time: 16:12
 */
package application.service.battle.special
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.CastPositionAct;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;

    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.task.TaskEvent;

    public class ForceBlow extends SpecialAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TOUCH_FRAME:int = 9;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ForceBlow(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user, enemy, stepModel);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _specialType = SpecialType.FORCE_BLOW;
            super._addSpecialHint();

            super.observer.swapUnits(_user, _enemy);

            var act:CastPositionAct = new CastPositionAct(_user, _enemy);
            act.addEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.execute();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _cast():void
        {
            _user.addEventListener(Event.COMPLETE, _castCompleteHandler);
            _user.forceBlow();
            _user.template.addEventListener(Event.ENTER_FRAME, _frameHandler);

            super.sound.playBattleSound(BattleSound.FORCE_BLOW);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _positionHandler(event:TaskEvent):void
        {
            var act:CastPositionAct = event.currentTarget as CastPositionAct;
            act.removeEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.destroy();

            _cast();
        }

        private function _castCompleteHandler(event:Event):void
        {
            _user.removeEventListener(Event.COMPLETE, _castCompleteHandler);
            _enemy.stay();

            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        private function _frameHandler(event:Event):void
        {
            var movie:MovieClip = event.currentTarget as MovieClip;
            if (movie.currentFrame >= TOUCH_FRAME) {
                movie.removeEventListener(Event.ENTER_FRAME, _frameHandler);
                _enemy.multiDamage();
                super._showBubbleHint();
                super.observer.addBlood(_enemy, true);
                super.observer.reduceHealth(_stepModel.damage.value, _enemy.isEnemy);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
