/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.12.13
 * Time: 15:33
 */
package application.service.battle.act
{
    import application.service.battle.common.BattleSectors;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;
    import application.views.constructor.helpers.AnimationHelper;

    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.tools.SourceManager;

    public class DarkBotInit extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ANIMATION:String = "NoobSaibotEnter";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DarkBotInit(unit:BattleUnit)
        {
            super(unit);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            unit.x = BattleSectors.getSectorPosition(5, true);
            unit.visible = false;
            _addAnimation();
        }

        private function _addAnimation():void
        {
            var mc:MovieClip = SourceManager.instance.getMovieClip(ANIMATION);
            mc.x = unit.x;
            mc.y = unit.y;
            super.observer.container.addChild(mc);
            var helper:AnimationHelper = new AnimationHelper(mc);
            helper.addEventListener(Event.COMPLETE, _animationCompleteHandler);
            super.sound.playBattleSound(BattleSound.DARK_BOT_INIT);
            helper.execute();
        }

        private function _animationCompleteHandler(event:Event):void
        {
            var helper:AnimationHelper = event.currentTarget as AnimationHelper;
            if (super.observer.container.contains(helper.movie))
                super.observer.container.removeChild(helper.movie);

            helper.removeEventListener(Event.COMPLETE, _animationCompleteHandler);
            helper.destroy();

            unit.visible = true;
            super.complete();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
