/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 31.07.13
 * Time: 13:35
 */
package application.service.battle.act
{
    import application.models.battle.item.ItemData;
    import application.sound.lib.BattleSound;
    import application.views.battle.common.hint.HintBubble;
    import application.views.constructor.builders.BattleUnit;

    import flash.events.Event;

    import framework.core.task.TaskEvent;

    public class MedicineAct extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MedicineAct(user:BattleUnit, itemData:ItemData)
        {
            super(user);
            _itemData = itemData;
            _item = itemData.picture.value;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            super.unit.addEventListener(Event.COMPLETE, _completeHandler);
            super.unit.useItem(_item);

            super.observer.addHealth(_itemData.heal.value, super.unit.isEnemy);
            var hint:HintBubble = new HintBubble(_itemData.heal.value);
            hint.medical = true;
            hint.x = super.unit.x - 75;
            hint.y = super.unit.y;

            super.sound.playBattleSound(BattleSound.DRINK);
            super.observer.container.addChild(hint);

            var health:int = unit.health;
            unit.health = health + _itemData.heal.value;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item:String;
        private var _itemData:ItemData;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeHandler(event:Event):void
        {
            super.unit.removeEventListener(Event.COMPLETE, _completeHandler);
            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
