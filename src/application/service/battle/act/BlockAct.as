/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.07.13
 * Time: 10:56
 */
package application.service.battle.act
{
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;
    import application.views.constructor.common.TemplateState;
    import application.views.constructor.common.WeaponTemplate;

    import flash.events.Event;

    import framework.core.helpers.MathHelper;
    import framework.core.task.TaskEvent;

    public class BlockAct extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BlockAct(attack:BattleUnit, defense:BattleUnit)
        {
            super(attack);
            _enemy = defense;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _kickVariant = MathHelper.random(0, TemplateState.KICK_VARIANTS.length - 1);
            _updateUser();
            _updateEnemy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _enemy:BattleUnit;
        private var _kickVariant:int = 0;

        private var _userComplete:Boolean = false;
        private var _enemyComplete:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateUser():void
        {
            super.unit.addEventListener(Event.COMPLETE, _unitComplete);
            super.unit.kick(_kickVariant);

            super.observer.swapUnits(super.unit, _enemy);
        }

        private function _updateEnemy():void
        {
            _enemy.addEventListener(Event.COMPLETE, _enemyCompleteHandler);
            _enemy.block();
            if (super.unit.weaponTemplate == WeaponTemplate.WITHOUT_WEAPON)
                super.sound.playBattleSound(BattleSound.BLOCK_PUNCH);
            else
                super.sound.playBattleSound(BattleSound.getBlockSlash());
        }

        private function _checkComplete():void
        {
            if (_userComplete && _enemyComplete)
                super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _unitComplete(event:Event):void
        {
            super.unit.removeEventListener(Event.COMPLETE, _unitComplete);
            _userComplete = true;
            _checkComplete();
        }

        private function _enemyCompleteHandler(event:Event):void
        {
            _enemy.removeEventListener(Event.COMPLETE, _enemyCompleteHandler);
            _enemyComplete = true;
            _checkComplete();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
