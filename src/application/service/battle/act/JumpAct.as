/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.07.13
 * Time: 16:25
 */
package application.service.battle.act
{
    import application.service.battle.common.BattleSectors;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;

    import com.greensock.TweenLite;

    import flash.events.Event;

    import framework.core.helpers.TimeHelper;
    import framework.core.task.TaskEvent;

    public class JumpAct extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static const JUMP_INIT_FRAMES:int = 4;
        private static const JUMPING_FRAMES:int = 7;
        private static const JUMP_LANDING_FRAMES:int = 8;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function JumpAct(unit:BattleUnit, sector:int)
        {
            super(unit);
            _targetSector = sector;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _targetX = BattleSectors.getSectorPosition(_targetSector, super.unit.isEnemy);

            if (_targetSector == super.unit.sector)
                _complete();
            else
                _moveUnit();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _targetX:Number = 0;
        private var _targetSector:int = 1;

        private var _jumpCompleted:Boolean = false;
        private var _tweenCompleted:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _moveUnit():void
        {
            var params:Object = {
                "x": _targetX,
                "onComplete": _tweenComplete,
                "delay": TimeHelper.getFramesTime(JUMP_INIT_FRAMES)
            };

            var time:Number = TimeHelper.getFramesTime(JUMPING_FRAMES);

            TweenLite.to(unit, time, params);

            super.unit.addEventListener(Event.COMPLETE, _jumpCompleteHandler);
            super.unit.jump();
            super.sound.playBattleSound(BattleSound.getJump());
        }

        private function _complete():void
        {
            super.unit.sector = _targetSector;
            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        private function _tweenComplete():void
        {
            _tweenCompleted = true;
            _checkComplete();
        }

        private function _checkComplete():void
        {
            if (_tweenCompleted && _jumpCompleted)
                _complete();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _jumpCompleteHandler(event:Event):void
        {
            super.unit.removeEventListener(Event.COMPLETE, _jumpCompleteHandler);
            _jumpCompleted = true;
            _checkComplete();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
