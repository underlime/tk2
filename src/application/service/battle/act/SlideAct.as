/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.07.13
 * Time: 17:08
 */
package application.service.battle.act
{
    import application.service.battle.common.BattleConfig;
    import application.service.battle.common.BattleSectors;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;

    import com.greensock.TweenLite;

    import framework.core.task.TaskEvent;

    public class SlideAct extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SlideAct(unit:BattleUnit, sector:int)
        {
            super(unit);
            _targetSector = sector;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _targetX = BattleSectors.getSectorPosition(_targetSector, super.unit.isEnemy);
            _distance = Math.abs(_targetSector - super.unit.sector);

            if (_targetSector == super.unit.sector)
                _complete();
            else
                _moveUnit();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _targetX:Number = 0;
        private var _targetSector:int = 1;
        private var _distance:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _moveUnit():void
        {
            _setAnimationType();

            var duration:Number = _distance * BattleConfig.MOVING_DURATION;
            var params:Object = {
                "x": _targetX,
                "onComplete": _completeTween
            };
            TweenLite.to(super.unit, duration, params);
            super.sound.playBattleSound(BattleSound.getDash());
        }

        private function _setAnimationType():void
        {
            if (_targetSector > super.unit.sector) {
                if (super.unit.isEnemy)
                    super.unit.backMove();
                else
                    super.unit.move();
            } else {
                if (super.unit.isEnemy)
                    super.unit.move();
                else
                    super.unit.backMove();
            }
        }

        private function _completeTween():void
        {
            TweenLite.killTweensOf(super.unit);
            super.unit.stay();
            _complete();
        }

        private function _complete():void
        {
            super.unit.sector = _targetSector;
            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
