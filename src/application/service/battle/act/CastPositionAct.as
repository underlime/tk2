/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.07.13
 * Time: 14:57
 */
package application.service.battle.act
{
    import application.views.constructor.builders.BattleUnit;

    import framework.core.task.TaskEvent;

    public class CastPositionAct extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CastPositionAct(user:BattleUnit, enemy:BattleUnit)
        {
            super(user);
            _user = user;
            _enemy = enemy;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            var left:BattleUnit = _user.isEnemy ? _enemy : _user;
            var right:BattleUnit = _user.isEnemy ? _user : _enemy;

            var userAct:SlideAct = new SlideAct(left, 2);
            userAct.addEventListener(TaskEvent.COMPLETE, _userCompleteHandler);
            userAct.execute();

            var enemyAct:SlideAct = new SlideAct(right, 5);
            enemyAct.addEventListener(TaskEvent.COMPLETE, _enemyCompleteHandler);
            enemyAct.execute();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        private function _checkComplete():void
        {
            if (_userCompleted && _enemyCompleted)
                super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:BattleUnit;
        private var _enemy:BattleUnit;

        private var _userCompleted:Boolean = false;
        private var _enemyCompleted:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _userCompleteHandler(event:TaskEvent):void
        {
            var act:SlideAct = event.currentTarget as SlideAct;
            act.removeEventListener(TaskEvent.COMPLETE, _userCompleteHandler);
            act.destroy();

            _userCompleted = true;
            _checkComplete();
        }

        private function _enemyCompleteHandler(event:TaskEvent):void
        {
            var act:SlideAct = event.currentTarget as SlideAct;
            act.removeEventListener(TaskEvent.COMPLETE, _enemyCompleteHandler);
            act.destroy();

            _enemyCompleted = true;
            _checkComplete();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
