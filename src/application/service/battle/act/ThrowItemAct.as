/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.08.13
 * Time: 12:30
 */
package application.service.battle.act
{
    import application.events.BattleEvent;
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.common.GrenadeType;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;

    import com.greensock.TweenMax;

    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.task.TaskEvent;
    import framework.core.tools.SourceManager;

    public class ThrowItemAct extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public const GRENADE_THROW_FRAME:int = 29;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ThrowItemAct(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user);
            _user = user;
            _enemy = enemy;
            _stepModel = stepModel;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_stepModel.item_data.empty) {
                throw new Error("No grenade!");
            }

            _startUser();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:BattleUnit;
        private var _enemy:BattleUnit;
        private var _stepModel:BattleStepRecord;
        private var _grenade:MovieClip;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _startUser():void
        {
            var grenadeSource:String = _stepModel.item_data.picture.value;
            _grenade = SourceManager.instance.getMovieClip(grenadeSource);

            _user.addEventListener(Event.COMPLETE, _throwCompleteHandler);
            _user.throwGrenade(grenadeSource);
            _user.template.addEventListener(Event.ENTER_FRAME, _frameHandler);
        }

        private function _throwGrenade():void
        {
            _grenade.x = _user.x + 30;
            _grenade.y = _user.y - 40;
            super.observer.container.addChild(_grenade);

            var params:Object = {
                "bezierThrough": [
                    {
                        x: _enemy.x - 125,
                        y: _grenade.y - 30
                    },
                    {
                        x: _enemy.x + 30,
                        y: _enemy.y + 50
                    }
                ],
                "orientToBezier": true,
                "onComplete": _tweenComplete
            };

            TweenMax.to(_grenade, .3, params);
        }

        private function _tweenComplete():void
        {
            TweenMax.killTweensOf(_grenade);

            if (super.observer.container.contains(_grenade))
                super.observer.container.removeChild(_grenade);

            var grenadeType:GrenadeType = GrenadeType.getEnumByKey(_stepModel.item_data.picture.value);
            _enemy.addEventListener(Event.COMPLETE, _enemyCompleteHandler);

            _enemy.grenadeDamage(grenadeType);
            super.dispatchEvent(new BattleEvent(BattleEvent.GET_DAMAGE));
            super.sound.playBattleSound(BattleSound.GRENADE_BLOW);
            super.sound.playBattleSound(BattleSound.getGrenade(grenadeType));
        }

        private function _enemyCompleteHandler(event:Event):void
        {
            _enemy.removeEventListener(Event.COMPLETE, _enemyCompleteHandler);
            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }


        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _throwCompleteHandler(event:Event):void
        {
            _user.removeEventListener(Event.COMPLETE, _throwCompleteHandler);
        }

        private function _frameHandler(event:Event):void
        {
            var movie:MovieClip = event.currentTarget as MovieClip;
            if (movie.currentFrame >= GRENADE_THROW_FRAME) {
                movie.removeEventListener(Event.ENTER_FRAME, _frameHandler);
                _throwGrenade();
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
