/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.07.13
 * Time: 11:28
 */
package application.service.battle.act
{
    import application.service.battle.common.BattleObserver;
    import application.sound.SoundManager;
    import application.views.constructor.builders.BattleUnit;

    import framework.core.task.Task;
    import framework.core.utils.Singleton;

    public class BattleAct extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleAct(unit:BattleUnit)
        {
            super();
            _unit = unit;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _unit:BattleUnit;
        private var _observer:BattleObserver = BattleObserver.instance;
        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get unit():BattleUnit
        {
            return _unit;
        }

        public function get observer():BattleObserver
        {
            return _observer;
        }

        public function get sound():SoundManager
        {
            return _sound;
        }
    }
}
