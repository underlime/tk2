/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.07.13
 * Time: 10:15
 */
package application.service.battle.act
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.common.WhoRole;
    import application.sound.lib.BattleSound;
    import application.views.battle.common.hint.HintBubble;
    import application.views.constructor.builders.BattleUnit;
    import application.views.constructor.common.TemplateState;
    import application.views.constructor.common.WeaponTemplate;

    import flash.events.Event;

    import framework.core.helpers.MathHelper;
    import framework.core.task.TaskEvent;

    public class SimpleKickAct extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const KICKS_COUNT:int = TemplateState.KICK_VARIANTS.length;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SimpleKickAct(attack:BattleUnit, defense:BattleUnit, model:BattleStepRecord)
        {
            super(attack);
            _enemy = defense;
            _model = model;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (super.observer.isNeighborhood(super.unit, _enemy))
                _kick();
            else
                _move();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _enemy:BattleUnit;
        private var _kickVariant:int = 0;

        private var _userComplete:Boolean = false;
        private var _enemyComplete:Boolean = false;

        private var _model:BattleStepRecord;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateUser():void
        {
            super.unit.addEventListener(Event.COMPLETE, _unitComplete);
            super.unit.kick(_kickVariant);
            var hint:HintBubble = new HintBubble(_model.damage.value);
            hint.x = _enemy.x - 75;
            hint.y = _enemy.y;

            if (_model.who_role.value == WhoRole.ENEMY.toString())
                hint.damage = true;

            if (_model.block.value)
                hint.block = true;

            if (_model.dodge.value)
                hint.miss = true;
            if (_model.crit.value)
                hint.crit = true;


            super.observer.container.addChild(hint);
            super.observer.swapUnits(super.unit, _enemy);
            _enemy.health -= _model.damage.value;
        }

        private function _updateEnemy():void
        {
            _enemy.addEventListener(Event.COMPLETE, _enemyCompleteHandler);
            if (_model.block.value) {
                _enemy.block();
                super.observer.addSparkle(_enemy);
                if (super.unit.weaponTemplate == WeaponTemplate.WITHOUT_WEAPON)
                    super.sound.playBattleSound(BattleSound.BLOCK_PUNCH);
                else
                    super.sound.playBattleSound(BattleSound.getBlockSlash());
            } else {
                _enemy.damage(_kickVariant);
                super.observer.addBlood(_enemy);

                if (_model.crit.value)
                    super.sound.playBattleSound(BattleSound.CRITICAL);
                else
                    if (super.unit.weaponTemplate == WeaponTemplate.WITHOUT_WEAPON)
                        super.sound.playBattleSound(BattleSound.getPunch());
                    else
                        super.sound.playBattleSound(BattleSound.getSlash());
            }
        }

        private function _checkComplete():void
        {
            if (_userComplete && _enemyComplete)
                super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        private function _kick():void
        {
            _kickVariant = MathHelper.random(0, KICKS_COUNT - 1);
            _updateUser();
            _updateEnemy();
            super.observer.reduceHealth(_model.damage.value, _enemy.isEnemy);
        }

        private function _move():void
        {
            var target:int = _enemy.isEnemy ? _enemy.sector - 1 : _enemy.sector + 1;
            var slideAct:SlideAct = new SlideAct(super.unit, target);
            slideAct.addEventListener(TaskEvent.COMPLETE, _moveCompleteHandler);
            slideAct.execute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _moveCompleteHandler(event:TaskEvent):void
        {
            var slideAct:SlideAct = event.currentTarget as SlideAct;
            slideAct.removeEventListener(TaskEvent.COMPLETE, _moveCompleteHandler);
            slideAct.destroy();
            _kick();
        }

        private function _unitComplete(event:Event):void
        {
            super.unit.removeEventListener(Event.COMPLETE, _unitComplete);
            _userComplete = true;
            _checkComplete();
        }

        private function _enemyCompleteHandler(event:Event):void
        {
            _enemy.removeEventListener(Event.COMPLETE, _enemyCompleteHandler);
            _enemyComplete = true;
            _checkComplete();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
