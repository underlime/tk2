/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.07.13
 * Time: 15:18
 */
package application.service.battle.act
{
    import application.views.constructor.builders.BattleUnit;

    public class MovingAct extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MovingAct(unit:BattleUnit, enemy:BattleUnit)
        {
            super(unit);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            super.execute();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
