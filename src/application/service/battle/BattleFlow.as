/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.07.13
 * Time: 16:30
 */
package application.service.battle
{
    import application.models.battle.BattleSteps;
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.common.WhoRole;
    import application.service.battle.step.GrenadeStep;
    import application.service.battle.step.SimpleStep;
    import application.service.battle.step.SpecialStep;
    import application.service.battle.step.StartStep;
    import application.service.battle.step.StepType;
    import application.service.battle.step.WinStep;
    import application.views.constructor.builders.BattleUnit;

    import framework.core.task.TaskFlow;

    public class BattleFlow extends TaskFlow
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleFlow(user:BattleUnit, enemy:BattleUnit, battleSteps:BattleSteps, userWin:Boolean)
        {
            super();
            _user = user;
            _enemy = enemy;
            _battleSteps = battleSteps;
            _userWin = userWin;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            var list:Object = _battleSteps.list.value;

            super.add(new StartStep(_user, _enemy));

            for (var key:String in list) {
                var stepModel:BattleStepRecord = list[key] as BattleStepRecord;

                if (stepModel.special.value)
                    _addSpecialStep(stepModel);
                else
                    if (stepModel.type.value == StepType.GRENADE.toString())
                        _addGrenade(stepModel);
                    else
                        super.add(new SimpleStep(stepModel, _user, _enemy));
            }

            if (_userWin)
                super.add(new WinStep(_user, _enemy));
            else
                super.add(new WinStep(_enemy, _user));

            super.execute();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:BattleUnit;
        private var _enemy:BattleUnit;
        private var _battleSteps:BattleSteps;
        private var _userWin:Boolean;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addSpecialStep(stepModel:BattleStepRecord):void
        {
            var attack:BattleUnit = stepModel.who_role.value == WhoRole.USER.toString() ? _user : _enemy;
            var defense:BattleUnit = stepModel.who_role.value == WhoRole.USER.toString() ? _enemy : _user;

            super.add(new SpecialStep(attack, defense, stepModel));
        }

        private function _addGrenade(stepModel:BattleStepRecord):void
        {
            super.add(new GrenadeStep(stepModel, _user, _enemy));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
