/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.08.13
 * Time: 15:02
 */
package application.service.battle.step
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.*;
    import application.service.battle.special.BoneBreaker;
    import application.service.battle.special.ForceBlow;
    import application.service.battle.special.HeavyArgumentStep;
    import application.service.battle.special.RageStrikes;
    import application.service.battle.special.SpecialAct;
    import application.service.battle.special.SpecialDouble;
    import application.service.battle.special.SpecialType;
    import application.service.battle.special.StalagmiteSpecial;
    import application.service.battle.special.Telekinesis;
    import application.views.constructor.builders.BattleUnit;

    import framework.core.task.TaskEvent;

    public class SpecialStep extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpecialStep(user:BattleUnit, enemy:BattleUnit, stepModel:BattleStepRecord)
        {
            super(user);

            _user = user;
            _enemy = enemy;
            _stepModel = stepModel;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _fillSpecialsHash();
            _defineSpecialAct();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:BattleUnit;
        private var _enemy:BattleUnit;
        private var _stepModel:BattleStepRecord;

        private var _specHash:Object = {};

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _fillSpecialsHash():void
        {
            _specHash[SpecialType.DOUBLE.toString()] = SpecialDouble;
            _specHash[SpecialType.RAGE.toString()] = RageStrikes;
            _specHash[SpecialType.STALAGMITE.toString()] = StalagmiteSpecial;
            _specHash[SpecialType.FORCE_BLOW.toString()] = ForceBlow;
            _specHash[SpecialType.TELEKINESIS.toString()] = Telekinesis;
            _specHash[SpecialType.HEAVY_HELPER.toString()] = HeavyArgumentStep;
            _specHash[SpecialType.BONE_BREAKER.toString()] = BoneBreaker;
        }

        private function _defineSpecialAct():void
        {
            var special:String = _stepModel.spec_data.spec_info.code.value;
            var specialType:SpecialType = SpecialType.getEnumByKey(special);

            if (_specHash[specialType.toString()]) {
                var SpecialActClass:Class = _specHash[specialType.toString()];
                var specialAct:SpecialAct = new SpecialActClass(_user, _enemy, _stepModel);
                specialAct.addEventListener(TaskEvent.COMPLETE, _completeHandler);
                specialAct.execute();
            }
        }

        private function _completeHandler(event:TaskEvent):void
        {
            var specialAct:SpecialAct = event.currentTarget as SpecialAct;
            specialAct.removeEventListener(TaskEvent.COMPLETE, _completeHandler);
            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
