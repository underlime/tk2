/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 23.07.13
 * Time: 21:00
 */
package application.service.battle.step
{
    import framework.core.enum.BaseEnum;

    public class StepType extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SIMPLE:StepType = new StepType("simple");
        public static const SPECIAL:StepType = new StepType("special");
        public static const GRENADE:StepType = new StepType("act_item");
        public static const MEDICAL:StepType = new StepType("med_item");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StepType(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.service.battle.step.StepType;

StepType.lockUp();