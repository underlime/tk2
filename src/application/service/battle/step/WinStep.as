/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.07.13
 * Time: 16:17
 */
package application.service.battle.step
{
    import application.service.battle.act.BattleAct;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;

    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import framework.core.task.TaskEvent;

    public class WinStep extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function WinStep(winner:BattleUnit, loser:BattleUnit)
        {
            super(winner);
            _winner = winner;
            _loser = loser;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            super.observer.swapUnits(_winner, _loser);
            _winner.win();
            _loser.ko();
            super.sound.playBattleSound(BattleSound.KO);

            _addDelayCall();
        }

        private function _addDelayCall():void
        {
            _timer = new Timer(1500, 1);
            _timer.addEventListener(TimerEvent.TIMER_COMPLETE, _completeHandler);
            _timer.start();
        }

        private function _completeHandler(event:TimerEvent):void
        {
            _timer.stop();
            _timer.removeEventListener(TimerEvent.TIMER_COMPLETE, _completeHandler);
            _timer = null;
            _showWinStep();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _winner:BattleUnit;
        private var _loser:BattleUnit;

        private var _timer:Timer;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _showWinStep():void
        {
            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
