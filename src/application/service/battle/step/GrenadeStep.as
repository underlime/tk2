/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.08.13
 * Time: 10:44
 */
package application.service.battle.step
{
    import application.events.BattleEvent;
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.BattleAct;
    import application.service.battle.act.CastPositionAct;
    import application.service.battle.act.ThrowItemAct;
    import application.service.battle.common.WhoRole;
    import application.views.battle.common.hint.HintBubble;
    import application.views.constructor.builders.BattleUnit;

    import framework.core.task.TaskEvent;

    public class GrenadeStep extends BattleAct
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GrenadeStep(stepModel:BattleStepRecord, user:BattleUnit, enemy:BattleUnit)
        {
            super(user);
            _stepModel = stepModel;
            _user = user;
            _enemy = enemy;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            var act:CastPositionAct = new CastPositionAct(_user, _enemy);
            act.addEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.execute();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _user:BattleUnit;
        private var _enemy:BattleUnit;
        private var _stepModel:BattleStepRecord;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _throwGrenade():void
        {
            var throwAct:ThrowItemAct = new ThrowItemAct(_user, _enemy, _stepModel);
            throwAct.addEventListener(TaskEvent.COMPLETE, _completeThrowHandler);
            throwAct.addEventListener(BattleEvent.GET_DAMAGE, _damageHandler);
            throwAct.execute();
        }

        private function _damageHandler(event:BattleEvent):void
        {
            var throwAct:ThrowItemAct = event.currentTarget as ThrowItemAct;
            throwAct.removeEventListener(BattleEvent.GET_DAMAGE, _damageHandler);

            var hint:HintBubble = new HintBubble(_stepModel.damage.value);
            hint.x = _enemy.x - 75;
            hint.y = _enemy.y;

            if (_stepModel.who_role.value == WhoRole.ENEMY.toString())
                hint.damage = true;

            if (_stepModel.block.value)
                hint.block = true;

            if (_stepModel.dodge.value)
                hint.miss = true;

            super.observer.container.addChild(hint);
            super.observer.reduceHealth(_stepModel.damage.value, _enemy.isEnemy);
            _enemy.health -= _stepModel.damage.value;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _positionHandler(event:TaskEvent):void
        {
            var act:CastPositionAct = event.currentTarget as CastPositionAct;
            act.removeEventListener(TaskEvent.COMPLETE, _positionHandler);
            act.destroy();

            _throwGrenade();
        }

        private function _completeThrowHandler(event:TaskEvent):void
        {
            var throwAct:ThrowItemAct = event.currentTarget as ThrowItemAct;
            throwAct.removeEventListener(TaskEvent.COMPLETE, _completeThrowHandler);
            throwAct.destroy();

            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
