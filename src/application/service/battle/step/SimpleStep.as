/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 23.07.13
 * Time: 21:57
 */
package application.service.battle.step
{
    import application.models.battle.step.BattleStepRecord;
    import application.service.battle.act.EvadeAct;
    import application.service.battle.act.JumpAct;
    import application.service.battle.act.MedicineAct;
    import application.service.battle.act.SimpleKickAct;
    import application.service.battle.act.SlideAct;
    import application.service.battle.common.BattleObserver;
    import application.service.battle.common.WhoRole;
    import application.views.constructor.builders.BattleUnit;

    import framework.core.helpers.MathHelper;
    import framework.core.task.TaskFlow;

    public class SimpleStep extends TaskFlow
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SimpleStep(model:BattleStepRecord, unit:BattleUnit, enemy:BattleUnit)
        {
            super();
            _model = model;
            _user = unit;
            _enemy = enemy;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _addSteps();
            super.execute();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:BattleStepRecord;

        private var _user:BattleUnit;
        private var _enemy:BattleUnit;

        private var _targetUserSector:int = 1;
        private var _targetEnemySector:int = 1;

        private var _observer:BattleObserver = BattleObserver.instance;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addSteps():void
        {
            if (_model.type.value == StepType.MEDICAL.toString())
                super.add(new MedicineAct(_user, _model.item_data));

            if (_model.who_role.value == WhoRole.USER.toString()) {
                _addEnemyMove();
                if (_model.dodge.value)
                    super.add(new EvadeAct(_user, _enemy));
                else
                    super.add(new SimpleKickAct(_user, _enemy, _model));
            } else {
                _addUserMove();
                if (_model.dodge.value)
                    super.add(new EvadeAct(_enemy, _user));
                else
                    super.add(new SimpleKickAct(_enemy, _user, _model));
            }
        }

        private function _addUserMove():void
        {
            _targetUserSector = _observer.getUserRandomSector();
            _targetEnemySector = _targetUserSector + 1;

            _observer.userSector = _targetUserSector;
            _observer.enemySector = _targetEnemySector;

            _moveUnits(_user, _targetUserSector, _enemy, _targetEnemySector);
        }

        private function _addEnemyMove():void
        {
            _targetEnemySector = _observer.getEnemyRandomSector();
            _targetUserSector = _targetEnemySector - 1;

            _observer.userSector = _targetUserSector;
            _observer.enemySector = _targetEnemySector;

            _moveUnits(_enemy, _targetEnemySector, _user, _targetUserSector);
        }

        private function _moveUnits(first:BattleUnit, targetFirst:int, second:BattleUnit, targetSecond:int):void
        {
            if (MathHelper.chance(80))
                super.add(new SlideAct(first, targetFirst));
            else
                super.add(new JumpAct(first, targetFirst));

            if (MathHelper.chance(80))
                super.add(new SlideAct(second, targetSecond));
            else
                super.add(new JumpAct(second, targetSecond));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
