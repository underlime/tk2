/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.07.13
 * Time: 11:27
 */
package application.service.battle.step
{
    import application.service.battle.act.BattleAct;
    import application.service.battle.act.DarkBotInit;
    import application.service.battle.act.SlideAct;
    import application.service.battle.common.BattleObserver;
    import application.sound.SoundManager;
    import application.sound.lib.BattleSound;
    import application.views.constructor.builders.BattleUnit;
    import application.views.constructor.helpers.AnimationHelper;

    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.core.task.Task;
    import framework.core.task.TaskEvent;
    import framework.core.tools.SourceManager;
    import framework.core.utils.Singleton;

    public class StartStep extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const FIGHT_LABEL:String = "FightLabel";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StartStep(user:BattleUnit, enemy:BattleUnit)
        {
            super();
            _user = user;
            _enemy = enemy;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _startLabel();
            _startUnits();
        }

        private function _startUnits():void
        {
            var startUserAct:BattleAct = new SlideAct(_user, 2);
            var startEnemyAct:BattleAct;
            if (!_enemy.isDarkBot)
                startEnemyAct = new SlideAct(_enemy, 5);
            else
                startEnemyAct = new DarkBotInit(_enemy);

            startEnemyAct.addEventListener(TaskEvent.COMPLETE, _completeEnemyHandler);
            startUserAct.addEventListener(TaskEvent.COMPLETE, _completeUserHandler);

            startEnemyAct.execute();
            startUserAct.execute();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _fightLabel:MovieClip;
        private var _helper:AnimationHelper;
        private var _observer:BattleObserver = BattleObserver.instance;

        private var _enemy:BattleUnit;
        private var _user:BattleUnit;

        private var _labelCompleted:Boolean = false;
        private var _userCompleted:Boolean = false;
        private var _enemyCompleted:Boolean = false;
        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/


        private function _checkComplete():void
        {
            if (_userCompleted && _enemyCompleted && _labelCompleted)
                super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _startLabel():void
        {
            _fightLabel = SourceManager.instance.getMovieClip(FIGHT_LABEL);
            _observer.container.addChild(_fightLabel);
            _fightLabel.x = 239;
            _fightLabel.y = 194;

            _helper = new AnimationHelper(_fightLabel);
            _helper.addEventListener(Event.COMPLETE, _completeHandler);
            _helper.execute();
            _sound.playBattleSound(BattleSound.GONG);
        }

        private function _completeEnemyHandler(event:TaskEvent):void
        {
            var act:Task = event.currentTarget as Task;
            act.removeEventListener(TaskEvent.COMPLETE, _completeEnemyHandler);
            act.destroy();

            _enemyCompleted = true;
            _checkComplete();
        }

        private function _completeUserHandler(event:TaskEvent):void
        {
            var act:Task = event.currentTarget as Task;
            act.removeEventListener(TaskEvent.COMPLETE, _completeEnemyHandler);
            act.destroy();

            _userCompleted = true;
            _checkComplete();
        }

        private function _completeHandler(event:Event):void
        {
            _helper.removeEventListener(Event.COMPLETE, _completeHandler);
            _helper.destroy();

            _observer.container.removeChild(_fightLabel);
            _fightLabel = null;

            _labelCompleted = true;
            _checkComplete();

        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
