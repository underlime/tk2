/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.07.13
 * Time: 12:39
 */
package application.service.battle.common
{
    import application.config.AppConfig;

    public class BattleSectors
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const COUNT:int = 6;
        public static const ENEMY_MARGIN:int = 120;
        public static const SECTOR_WIDTH:int = 125;

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getSectorPosition(sector:int, isEnemy:Boolean = false):Number
        {
            var x:Number = 120;
            switch (sector) {
                case 0:
                    x = 0;
                    break;
                case 1:
                    x = 120;
                    break;
                case 2:
                    x = 245;
                    break;
                case 3:
                    x = 370;
                    break;
                case 4:
                    x = 495;
                    break;
                case 5:
                    x = 620;
                    break;
                case 6:
                    x = 745;
                    break;
                case 7:
                    x = AppConfig.APP_WIDTH;
                    break;
                default :
                    x = 120;
                    break;
            }

            if (isEnemy)
                x -= ENEMY_MARGIN;

            return x;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
