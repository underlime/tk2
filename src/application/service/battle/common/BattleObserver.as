/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.07.13
 * Time: 12:34
 */
package application.service.battle.common
{
    import application.views.battle.bar.LifeBar;
    import application.views.battle.blood.Blood;
    import application.views.battle.blood.Sparkle;
    import application.views.battle.common.UserHealth;
    import application.views.constructor.builders.BattleUnit;

    import flash.display.DisplayObjectContainer;

    import framework.core.helpers.MathHelper;
    import framework.core.struct.service.Service;

    public class BattleObserver extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _instance:BattleObserver = null;

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleObserver()
        {
            super();
            if (BattleObserver._instance) {
                throw new Error("Use instance property");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var container:DisplayObjectContainer;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function isNeighborhood(user:BattleUnit, enemy:BattleUnit):Boolean
        {
            if (Math.abs(user.sector - enemy.sector) > 1)
                return false;

            return true;
        }

        public function getUserRandomSector():int
        {
            var arr:Array = _getVariantsForUser();
            var randomIndex:int = MathHelper.random(0, arr.length - 1);

            return arr[randomIndex];
        }

        public function getEnemyRandomSector():int
        {
            var arr:Array = _getVariantsForEnemy();
            var randomIndex:int = MathHelper.random(0, arr.length - 1);

            return arr[randomIndex];
        }

        public function swapUnits(attack:BattleUnit, defense:BattleUnit):void
        {
            if (container && defense.parent && attack.parent && container == defense.parent && container == attack.parent) {
                if (container.getChildIndex(attack) < container.getChildIndex(defense)) {
                    container.swapChildren(attack, defense);
                }
            }
        }

        public function addBlood(defense:BattleUnit, bleeding:Boolean = false):void
        {
            var blood:Blood = new Blood();
            blood.x = defense.x + 20;
            blood.y = defense.y;

            if (!defense.isEnemy) {
                blood.scaleX = -1;
                blood.x = defense.x - 20;
            }

            if (bleeding)
                blood.execution = true;

            container.addChild(blood);
        }

        public function addSparkle(defense:BattleUnit):void
        {
            var blood:Sparkle = new Sparkle();
            blood.x = defense.x + 20;
            blood.y = defense.y;

            if (!defense.isEnemy) {
                blood.scaleX = -1;
                blood.x = defense.x - 20;
            }

            container.addChild(blood);
        }

        public function reduceHealth(damage:int, isEnemy:Boolean = false):void
        {
            var bar:LifeBar = isEnemy ? _enemyBar : _userBar;
            if (bar) {
                bar.damage = damage;
            }
            var health:UserHealth = isEnemy ? _enemyHealth : _userHealth;
            health.damage = damage;
        }

        public function addHealth(heal:int, isEnemy:Boolean = false):void
        {
            var bar:LifeBar = isEnemy ? _enemyBar : _userBar;
            if (bar) {
                bar.heal = heal;
            }

            var health:UserHealth = isEnemy ? _enemyHealth : _userHealth;
            health.heal = heal;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _userSector:int = 3;
        private var _enemySector:int = 4;

        private var _enemyBar:LifeBar;
        private var _userBar:LifeBar;

        private var _userHealth:UserHealth;
        private var _enemyHealth:UserHealth;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getVariantsForUser():Array
        {
            var arr:Array = [];
            var borderSector:int = _enemySector;
            for (var i:int = 1; i < borderSector; i++) {
                arr.push(i);
            }
            return arr;
        }

        private function _getVariantsForEnemy():Array
        {
            var arr:Array = [];
            var borderSector:int = _userSector;
            for (var i:int = BattleSectors.COUNT; i > borderSector; i--) {
                arr.push(i);
            }
            return arr;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public static function get instance():BattleObserver
        {
            if (BattleObserver._instance == null) {
                BattleObserver._instance = new BattleObserver();
            }
            return BattleObserver._instance;
        }

        public function get enemySector():int
        {
            return _enemySector;
        }

        public function get userSector():int
        {
            return _userSector;
        }

        public function set userSector(value:int):void
        {
            _userSector = value;
        }

        public function set enemySector(value:int):void
        {
            _enemySector = value;
        }

        public function set enemyBar(value:LifeBar):void
        {
            _enemyBar = value;
        }

        public function set userBar(value:LifeBar):void
        {
            _userBar = value;
        }

        public function set userHealth(value:UserHealth):void
        {
            _userHealth = value;
        }

        public function set enemyHealth(value:UserHealth):void
        {
            _enemyHealth = value;
        }
    }
}
