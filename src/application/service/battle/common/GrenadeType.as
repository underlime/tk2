/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.08.13
 * Time: 10:49
 */
package application.service.battle.common
{

    import framework.core.enum.BaseEnum;

    public class GrenadeType extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;
        public static var ENUM:Vector.<GrenadeType> = new Vector.<GrenadeType>();

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ICE:GrenadeType = new GrenadeType("trow001");
        public static const FIRE:GrenadeType = new GrenadeType("trow002");
        public static const FLASH:GrenadeType = new GrenadeType("trow003");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        public static function getEnumByKey(key:String):GrenadeType
        {
            var count:int = ENUM.length;
            for (var i:int = 0; i < count; i++) {
                if (ENUM[i].toString() == key)
                    return ENUM[i];
            }
            throw new Error("Grenade not found");
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GrenadeType(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
            ENUM.push(this);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.service.battle.common.GrenadeType;

GrenadeType.lockUp();