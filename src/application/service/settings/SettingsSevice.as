/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.09.13
 * Time: 21:03
 */
package application.service.settings
{
    import application.config.AppConfig;
    import application.config.AppSettings;
    import application.sound.SoundManager;

    import flash.display.Stage;
    import flash.events.Event;

    import framework.core.storage.LocalStorage;
    import framework.core.struct.service.Service;
    import framework.core.utils.Singleton;

    public class SettingsSevice extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SettingsSevice(stage:Stage)
        {
            super();
            _stage = stage;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _updateSettings();
            _storage.addEventListener(Event.CHANGE, _changeHandler);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _storage:LocalStorage = LocalStorage.getInstance(AppConfig.APP_NAME);
        private var _stage:Stage;

        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateGraphics():void
        {
            if (_storage.hasItem(AppSettings.GRAPHICS_QUALITY))
                _stage.quality = _storage.getItem(AppSettings.GRAPHICS_QUALITY);
        }

        private function _updateSound():void
        {
            if (_storage.hasItem(AppSettings.SOUNDS_ENABLED)) {
                var flag:int = parseInt(_storage.getItem(AppSettings.SOUNDS_ENABLED));
                _sound.mute = !Boolean(flag);
            }
        }

        private function _updateSettings():void
        {
            _updateGraphics();
            _updateSound();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _updateSettings();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
