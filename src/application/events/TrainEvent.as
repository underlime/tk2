/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 09.09.13
 * Time: 12:21
 */
package application.events
{
    import flash.events.Event;

    public class TrainEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TRAIN:String = "TrainEvent";
        public static const RESET_SKILLS:String = "TrainEventResetSkills";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrainEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:TrainEvent = new TrainEvent(type, bubbles);
            event.data = super.data;
            event.strength = _strength;
            event.agility = _agility;
            event.intellect = _intellect;

            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _strength:int = 0;
        private var _agility:int = 0;
        private var _intellect:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get strength():int
        {
            return _strength;
        }

        public function set strength(value:int):void
        {
            _strength = value;
        }

        public function get agility():int
        {
            return _agility;
        }

        public function set agility(value:int):void
        {
            _agility = value;
        }

        public function get intellect():int
        {
            return _intellect;
        }

        public function set intellect(value:int):void
        {
            _intellect = value;
        }
    }
}
