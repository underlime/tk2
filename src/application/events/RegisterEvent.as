/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 10.10.13
 * Time: 12:10
 */
package application.events
{
    import application.service.register.RegisterData;

    import flash.events.Event;

    public class RegisterEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const REGISTER:String = "RegisterEventRegister";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RegisterEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:RegisterEvent = new RegisterEvent(type, bubbles);
            event.registerData = _registerData;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _registerData:RegisterData;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get registerData():RegisterData
        {
            return _registerData;
        }

        public function set registerData(value:RegisterData):void
        {
            _registerData = value;
        }
    }
}
