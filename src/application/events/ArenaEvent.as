/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.09.13
 * Time: 9:46
 */
package application.events
{
    import flash.events.Event;

    public class ArenaEvent extends Event
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const GET_BY_LEVEL:String = "GetByLevelEvent";
        public static const GET_BY_IDENTITY:String = "GetByIdentityEvent";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArenaEvent(type:String)
        {
            super(type);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _level:int = 0;
        private var _identity:String;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get level():int
        {
            return _level;
        }

        public function set level(value:int):void
        {
            _level = value;
        }

        public function get identity():String
        {
            return _identity;
        }

        public function set identity(value:String):void
        {
            _identity = value;
        }
    }
}
