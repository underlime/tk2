/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.07.13
 * Time: 12:12
 */
package application.events
{
    import application.models.inventory.InventoryItem;

    import flash.events.Event;

    public class SlotEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const CHANGE:String = "SlotEventChange";
        public static const UNLOCK:String = "SlotEventUnlock";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SlotEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:SlotEvent = new SlotEvent(super.type, super.bubbles);
            event.item = _item;

            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item:InventoryItem;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get item():InventoryItem
        {
            return _item;
        }

        public function set item(value:InventoryItem):void
        {
            _item = value;
        }
    }
}
