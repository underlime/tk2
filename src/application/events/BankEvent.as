/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.09.13
 * Time: 20:53
 */
package application.events
{
    import flash.events.Event;

    public class BankEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BUY:String = "BankBuyEvent";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:BankEvent = new BankEvent(type, bubbles);
            event.data = this.data;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
