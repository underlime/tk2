/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.05.13
 * Time: 21:56
 */
package application.events
{
    import flash.events.Event;

    import framework.core.events.GameEvent;

    public class ApplicationEvent extends GameEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const UNDEFINED_CALL:String = "UndefinedCallEvent";
        public static const CALL_MARKET:String = "CallMarketEvent";
        public static const CHOOSE:String = "ChooseEvent";
        public static const CLOSE:String = "CloseEvent";
        public static const TAB_CHOOSE:String = "TabChooseEvent";
        public static const CALL_ACADEMY:String = "CallLaboratory";
        public static const CALL_HOME:String = "CallHomeEvent";
        public static const GENERATE_AVATAR:String = "GenerateAvatarHandler";
        public static const CALL_TOP:String = "CallTopEvent";
        public static const CALL_SETTINGS:String = "CallSettingsEvent";
        public static const CALL_ARENA:String = "CallArena";
        public static const COMPLETE:String = "ApplicationEventComplete";
        public static const CALL_BANK:String = "CallBankAppEvent";
        public static const CALL_EXPRESS_BANK:String = "CallExpressBankEvent";
        public static const CALL_GUEST_SCREEN:String = "CallGuestScreen";
        public static const LOW_ENERGY:String = "LowEnergyApplicationEvent";
        public static const CALL_JOURNAL:String = "CallJournalEvent";
        public static const SHARE:String = "Share";
        public static const LIKE:String = "Like";
        public static const SERVER_INFO_LOADED:String = "ServerInfoLoaded";
        public static const CALL_GMO:String = "CallGMOEvent";
        public static const CALL_CLANS:String = "CallClansEvent";
        public static const UPDATE:String = "UpdateAppEvent";
        public static const TUTORIAL_COMPLETE:String = "TutorialComplete";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ApplicationEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:ApplicationEvent = new ApplicationEvent(type, bubbles, cancelable);
            event.data = super.data;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
