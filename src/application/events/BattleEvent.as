/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.07.13
 * Time: 10:19
 */
package application.events
{
    import application.models.arena.EnemyRecord;

    import flash.events.Event;

    public class BattleEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const CALL_BATTLE:String = "BattleEvent";
        public static const CALL_MAP:String = "CallMapBattleEvent";
        public static const GET_DAMAGE:String = "BattleEventGetDamage";
        public static const BATTLE_AGAIN:String = "BattleEventAgain";
        public static const SHARE:String = "BattleEventShare";
        public static const QUICK_BATTLE:String = "BattleEventQuickBattle";
        public static const EASE:String = "EaseBattleEvent";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:BattleEvent = new BattleEvent(type, bubbles);
            event.enemy_id = _enemy_id;
            event.enemyRecord = _enemyRecord;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _enemy_id:int = 0;
        private var _enemyRecord:EnemyRecord;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get enemy_id():int
        {
            return _enemy_id;
        }

        public function set enemy_id(value:int):void
        {
            _enemy_id = value;
        }

        public function get enemyRecord():EnemyRecord
        {
            return _enemyRecord;
        }

        public function set enemyRecord(value:EnemyRecord):void
        {
            _enemyRecord = value;
        }
    }
}
