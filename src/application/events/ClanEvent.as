/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.02.14
 * Time: 11:41
 */
package application.events
{
    import flash.events.Event;

    public class ClanEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const DELETE:String = "DeleteClanEvent";
        public static const SHOW:String = "ShowClanEvent";
        public static const REQUEST:String = "RequestClanEvent";
        public static const RESOLUTION:String = "ClanEventResolution";
        public static const SELECT:String = "SelectClanUser";
        public static const SEND_ITEM:String = "SendItemAction";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ClanEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:ClanEvent = new ClanEvent(type, bubbles);
            event.data = this.data;
            event.clan_id = this.clan_id;
            event.resolution = this.resolution;
            event.user_id = this.user_id;
            event.soc_net_id = this.soc_net_id;
            event.item_id = this.item_id;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _clan_id:int = 0;
        private var _user_id:int = 0;
        private var _soc_net_id:String = "";
        private var _resolution:Boolean = false;
        private var _item_id:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get clan_id():int
        {
            return _clan_id;
        }

        public function set clan_id(value:int):void
        {
            _clan_id = value;
        }

        public function get resolution():Boolean
        {
            return _resolution;
        }

        public function set resolution(value:Boolean):void
        {
            _resolution = value;
        }

        public function get user_id():int
        {
            return _user_id;
        }

        public function set user_id(value:int):void
        {
            _user_id = value;
        }

        public function get soc_net_id():String
        {
            return _soc_net_id;
        }

        public function set soc_net_id(value:String):void
        {
            _soc_net_id = value;
        }

        public function get item_id():int
        {
            return _item_id;
        }

        public function set item_id(value:int):void
        {
            _item_id = value;
        }
    }
}
