/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 02.07.13
 * Time: 15:16
 */
package application.events
{
    import application.models.market.Item;

    import flash.events.Event;

    public class InventoryEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SELL_ITEM:String = "SellItemEvent";
        public static const PUT_ITEMS:String = "PutItemsEvent";
        public static const APPLY_ITEM:String = "ApplyItemEvent";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InventoryEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        override public function clone():Event
        {
            var event:InventoryEvent = new InventoryEvent(super.type, super.bubbles);
            event.item = _item;
            return event;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _item:Item;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get item():Item
        {
            return _item;
        }

        public function set item(value:Item):void
        {
            _item = value;
        }
    }
}
