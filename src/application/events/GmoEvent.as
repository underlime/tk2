/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.12.13
 * Time: 20:42
 */
package application.events
{
    import application.views.screen.gmo.GmoType;

    import flash.events.Event;

    public class GmoEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BUY:String = "GmoBuyEvent";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GmoEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:GmoEvent = new GmoEvent(type, bubbles);
            event.gmoType = _gmoType;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _gmoType:GmoType;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get gmoType():GmoType
        {
            return _gmoType;
        }

        public function set gmoType(value:GmoType):void
        {
            _gmoType = value;
        }
    }
}
