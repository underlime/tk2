package application.events
{
    import framework.core.events.GameEvent;

    public class ServerEvent extends GameEvent
    {
        public static const RIVAL_ADDED:String = "rival_added";
        public static const LOGIN_SUCCESS:String = "login_success";

        public function ServerEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }
    }
}
