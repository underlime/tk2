/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 14.06.13
 * Time: 11:10
 */
package application.events
{
    import application.common.Currency;
    import application.models.chars.CharDetail;
    import application.models.market.Item;

    import flash.events.Event;

    public class BuyEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BUY:String = "BuyEvent";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BuyEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:BuyEvent = new BuyEvent(super.type, super.bubbles);
            event.currency = _currency;
            event.item = _item;
            event.char = _char;
            event.count = _count;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _currency:Currency = Currency.FERROS;
        private var _item:Item;
        private var _char:CharDetail;
        private var _count:int = 1;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get currency():Currency
        {
            return _currency;
        }

        public function set currency(value:Currency):void
        {
            _currency = value;
        }

        public function get item():Item
        {
            return _item;
        }

        public function set item(value:Item):void
        {
            _item = value;
        }

        public function get char():CharDetail
        {
            return _char;
        }

        public function set char(value:CharDetail):void
        {
            _char = value;
        }

        public function get count():int
        {
            return _count;
        }

        public function set count(value:int):void
        {
            _count = value;
        }
    }
}
