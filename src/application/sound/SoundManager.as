/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.09.13
 * Time: 19:38
 */
package application.sound
{
    import application.sound.lib.BattleSound;
    import application.sound.lib.UISound;

    import flash.media.Sound;
    import flash.system.ApplicationDomain;

    import framework.core.struct.service.Service;

    public class SoundManager extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const MAX_UI_CHANNELS:int = 4;
        public static const MAX_BATTLE_CHANNELS:int = 15;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SoundManager()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        /**
         * Проиграть звук UI
         * @param sound
         */
        public function playUISound(sound:UISound):void
        {
            var soundSource:Sound = _storage.getSound(sound.toString());
            if (soundSource)
                _uiSoundFlow.play(soundSource);
        }

        /**
         * Проиграть звук битвы
         * @param sound
         */
        public function playBattleSound(sound:BattleSound):void
        {
            var soundSource:Sound = _storage.getSound(sound.toString());
            if (soundSource)
                _battleSoundFLow.play(soundSource);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appDomain:ApplicationDomain = new ApplicationDomain();
        private var _storage:SoundStorage = new SoundStorage(_appDomain);
        private var _mute:Boolean = false;

        private var _uiSoundFlow:SoundFlow = new SoundFlow(MAX_UI_CHANNELS);
        private var _battleSoundFLow:SoundFlow = new SoundFlow(MAX_BATTLE_CHANNELS);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get appDomain():ApplicationDomain
        {
            return _appDomain;
        }

        public function get mute():Boolean
        {
            return _mute;
        }

        public function set mute(value:Boolean):void
        {
            _mute = value;
            _battleSoundFLow.mute = _mute;
            _uiSoundFlow.mute = _mute;
        }
    }
}
