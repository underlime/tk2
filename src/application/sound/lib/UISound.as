/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.09.13
 * Time: 19:54
 */
package application.sound.lib
{

    import framework.core.enum.BaseEnum;

    public class UISound extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const MOUSE_OVER:UISound = new UISound("Social_btnOver");
        public static const CLICK:UISound = new UISound("Social_CheckBox_click");
        public static const CLOSE:UISound = new UISound("Social_closeBtn");
        public static const ITEM_SALE:UISound = new UISound("Social_ItemSale");
        public static const OK:UISound = new UISound("Social_ApproveBtn");
        public static const CANCEL:UISound = new UISound("Social_CancelBtn");
        public static const CASH:UISound = new UISound("Social_Cash");
        public static const EQUIP_ITEMS:UISound = new UISound("Social_EquipItem");
        public static const ITEM_DRINK:UISound = new UISound("Social_ItemUse");
        public static const TAB_CHANGE:UISound = new UISound("Social_TabFlip");
        public static const UNEQUIP_ITEMS:UISound = new UISound("Social_UnEquip");
        public static const SUCCESS:UISound = new UISound("fanfare");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UISound(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.sound.lib.UISound;

UISound.lockUp();