/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.09.13
 * Time: 19:54
 */
package application.sound.lib
{

    import application.service.battle.common.GrenadeType;

    import framework.core.enum.BaseEnum;

    import org.casalib.util.ArrayUtil;

    public class BattleSound extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const PUNCH_1:BattleSound = new BattleSound("Battle_Punch1");
        public static const PUNCH_2:BattleSound = new BattleSound("Battle_Punch2");
        public static const PUNCH_3:BattleSound = new BattleSound("Battle_Punch3");
        public static const PUNCH_4:BattleSound = new BattleSound("Battle_Punch4");
        public static const PUNCH_5:BattleSound = new BattleSound("Battle_Punch5");
        public static const PUNCH_6:BattleSound = new BattleSound("Battle_Punch6");
        public static const PUNCH_7:BattleSound = new BattleSound("Battle_Punch7");
        public static const PUNCHES:Array = [PUNCH_1, PUNCH_2, PUNCH_3, PUNCH_4, PUNCH_5, PUNCH_6, PUNCH_7];

        public static const SLASH_1:BattleSound = new BattleSound("Battle_Slash1");
        public static const SLASH_2:BattleSound = new BattleSound("Battle_Slash2");
        public static const SLASH_3:BattleSound = new BattleSound("Battle_Slash3");
        public static const SLASH_4:BattleSound = new BattleSound("Battle_Slash4");
        public static const SLASH_5:BattleSound = new BattleSound("Battle_Slash5");
        public static const SLASH_6:BattleSound = new BattleSound("Battle_Slash6");
        public static const SLASH_7:BattleSound = new BattleSound("Battle_Slash7");
        public static const SLASHES:Array = [SLASH_1, SLASH_2, SLASH_3, SLASH_4, SLASH_5, SLASH_6, SLASH_7];

        public static const BLOCK_SLASH_1:BattleSound = new BattleSound("Battle_Block1");
        public static const BLOCK_SLASH_2:BattleSound = new BattleSound("Battle_Block2");
        public static const SLASH_BLOCKS:Array = [BLOCK_SLASH_1, BLOCK_SLASH_2];

        public static const DASH_1:BattleSound = new BattleSound("Battle_Dash1");
        public static const DASH_2:BattleSound = new BattleSound("Battle_Dash2");
        public static const DASHES:Array = [DASH_1, DASH_2];

        public static const EVADE_1:BattleSound = new BattleSound("Battle_Evade1");
        public static const EVADE_2:BattleSound = new BattleSound("Battle_Evade2");
        public static const EVADE_3:BattleSound = new BattleSound("Battle_Evade3");
        public static const EVADE_4:BattleSound = new BattleSound("Battle_Evade4");
        public static const EVADES:Array = [EVADE_1, EVADE_2, EVADE_3, EVADE_4];

        public static const JUMP_1:BattleSound = new BattleSound("Battle_Jump1");
        public static const JUMP_2:BattleSound = new BattleSound("Battle_Jump2");
        public static const JUMP_3:BattleSound = new BattleSound("Battle_Jump3");
        public static const JUMPS:Array = [JUMP_1, JUMP_2, JUMP_3];

        public static const BLOCK_PUNCH:BattleSound = new BattleSound("Battle_Block_Punch");
        public static const BONE_BREAKER:BattleSound = new BattleSound("Battle_BonebreakerSFX");
        public static const CRITICAL:BattleSound = new BattleSound("Battle_CriticalStrike");
        public static const DRINK:BattleSound = new BattleSound("Battle_drink");
        public static const FIRE_GRENADE:BattleSound = new BattleSound("Battle_flame_granade");
        public static const FORCE_BLOW:BattleSound = new BattleSound("Battle_ForceBlowSFX");
        public static const FROZEN_GRENADE:BattleSound = new BattleSound("Battle_frozen_grenade");
        public static const GONG:BattleSound = new BattleSound("Battle_Gong");
        public static const GRENADE_BLOW:BattleSound = new BattleSound("Battle_granade_blow");
        public static const GUN:BattleSound = new BattleSound("Battle_Gun1");
        public static const HEAVY_ARGUMENT:BattleSound = new BattleSound("Battle_Punch1");
        public static const DARK_BOT_INIT:BattleSound = new BattleSound("Battle_NS_Intro");
        public static const STALAGMITE:BattleSound = new BattleSound("Battle_Stalagmit");
        public static const TELEKINESIS:BattleSound = new BattleSound("Battle_TelekinezSFX");
        public static const TESLA_GRENADE:BattleSound = new BattleSound("Battle_tesla_grenade");
        public static const KO:BattleSound = new BattleSound("Battle_KO");
        public static const CAST:BattleSound = new BattleSound("Battle_Cast");


        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getPunch():BattleSound
        {
            return ArrayUtil.random(PUNCHES) as BattleSound;
        }

        public static function getSlash():BattleSound
        {
            return ArrayUtil.random(SLASHES) as BattleSound;
        }

        public static function getBlockSlash():BattleSound
        {
            return ArrayUtil.random(SLASH_BLOCKS) as BattleSound;
        }

        public static function getDash():BattleSound
        {
            return ArrayUtil.random(DASHES) as BattleSound;
        }

        public static function getEvade():BattleSound
        {
            return ArrayUtil.random(EVADES) as BattleSound;
        }

        public static function getJump():BattleSound
        {
            return ArrayUtil.random(JUMPS) as BattleSound;
        }

        public static function getGrenade(grenade:GrenadeType):BattleSound
        {
            switch (grenade) {
                case GrenadeType.FIRE:
                    return FIRE_GRENADE;
                case GrenadeType.ICE:
                    return FROZEN_GRENADE;
                case GrenadeType.FLASH:
                    return TESLA_GRENADE;
            }

            return FIRE_GRENADE;
        }


        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BattleSound(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}

import application.sound.lib.BattleSound;

BattleSound.lockUp();