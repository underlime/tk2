/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.09.13
 * Time: 19:50
 */
package application.sound
{
    import flash.events.Event;
    import flash.media.Sound;
    import flash.media.SoundChannel;
    import flash.media.SoundTransform;

    import framework.core.struct.service.Service;

    public class SoundFlow extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SoundFlow(maxChannels:int)
        {
            super();
            _maxChannels = maxChannels;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function play(sound:Sound):void
        {
            if (_mute)
                return;

            if (_channels.length >= _maxChannels) {
                _destroySoundChannel(_channels[0]);
            }

            var channel:SoundChannel = sound.play(0, 0, _soundTransform);
            channel.addEventListener(Event.SOUND_COMPLETE, _soundCompleteHandler);

            _channels.push(channel);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _channels:Array = [];
        private var _maxChannels:int = 5;

        private var _soundTransform:SoundTransform = new SoundTransform(.4);
        private var _mute:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _destroySoundChannel(index:int):void
        {
            if (_channels[index]) {
                var channel:SoundChannel = _channels[index] as SoundChannel;
                channel.stop();
                channel.removeEventListener(Event.SOUND_COMPLETE, _soundCompleteHandler);
                channel = null;
                _channels.splice(index, 1);
            }
        }

        private function _destroySounds():void
        {
            var count:int = _channels.length;
            for (var i:int = 0; i < count; i++) {
                _destroySoundChannel(i);
            }

        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _soundCompleteHandler(event:Event):void
        {
            var channel:SoundChannel = event.currentTarget as SoundChannel;
            var index:int = _channels.indexOf(channel);
            if (index > -1) {
                _destroySoundChannel(index);
            } else {
                channel.removeEventListener(Event.SOUND_COMPLETE, _soundCompleteHandler);
                channel = null;
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get maxChannels():int
        {
            return _maxChannels;
        }

        public function set maxChannels(value:int):void
        {
            _maxChannels = value;
        }

        public function get soundTransform():SoundTransform
        {
            return _soundTransform;
        }

        public function set mute(value:Boolean):void
        {
            _mute = value;
            if (_mute) {
                _destroySounds();
            }
        }

        public function get mute():Boolean
        {
            return _mute;
        }
    }
}
