/**
 * Сигнатуры методов находятся в пакете signatures.
 * Если для данного метода нет структуры с сигнатурой,
 * значит, он не принимает параметров
 */
package application.server_api
{
    public class ApiMethods
    {
        public static const USER_GET_INFO:String = 'user/get.info';
        public static const ITEMS_GET_MARKET_LIST:String = 'items/get.market.list';
        public static const ITEMS_BUY:String = 'items/buy.item';
        public static const GET_CHARS:String = "academy/get.char.details";
        public static const CHANGE_DETAILS:String = "academy/change.details";
        public static const SELL_ITEM:String = "items/sell.item";
        public static const PUT_ITEMS:String = "items/put.items";
        public static const APPLY_ITEM:String = "items/apply.item";

        public static const TOP_GLORY_GLOBAL:String = "top/glory/get.global";
        public static const TOP_GLORY_CITY:String = "top/glory/get.by.city";
        public static const TOP_GLORY_FRIENDS:String = "top/glory/get.friends.top";

        public static const OPPONENTS_BY_LEVEL:String = "arena/enemies/get.by.level";
        public static const OPPONENTS_BY_IDENTITY:String = "arena/enemies/get.by.identity";

        public static const FIGHT:String = "arena/fight";
        public static const GET_ACHIEVEMENTS:String = "user/get.achievements";
        public static const GET_SPECIALS:String = "user/get.specials";

        public static const CHECK_LOGIN:String = "user/check.login";
        public static const CHANGE_LOGIN:String = "academy/change.login";
        public static const CHANGE_VEGETABLE:String = "academy/change.vegetable";
        public static const SAVE_POINTS:String = "academy/train.skills";

        public static const RESET_SKILLS:String = "academy/reset.skills";
        public static const BUY_RING_SLOT:String = "user/buy.ring.slot";
        public static const BUY_CONSUME_SLOT:String = "user/buy.item.slot";
        public static const USER_RECOVER_ENERGY:String = "user/recover.energy";
        public static const USER_GIVE_FRIENDS_PRIZES:String = "user/give.friends.prizes";
        public static const USER_SET_ALBUM_ID:String = "user/set.album.id";
        public static const USER_GET_JOURNAL:String = "user/get.journal";
        public static const REGISTER:String = "user/register";
        public static const NEWS_GET_NEWS:String = "news/get.news";
        public static const NEWS_SET_NEWS_READ:String = "news/set.news.read";

        public static const RENEW_ENERGY:String = "user/renew.energy";
        public static const BANK_GET_CURRENCY_RATES:String = "bank/get.currency.rates";
        public static const USER_GET_MONEY:String = "user/get.money";

        public static const GET_GMO_PRICES:String = "citadel/get.prices";
        public static const SET_GAIN:String = "citadel/set.gain";
        public static const GET_SHARPENING_PARAMS:String = "sharpening/get.sharpening.params";
        public static const SHARPEN_WEAPON:String = "sharpening/sharpen.weapon";
        public static const VALIDATE_CLAN_NAME:String = "clans/validate.name";
        public static const REGISTER_CLAN:String = "clans/create";
        public static const DELETE_CLAN:String = "clans/delete";
        public static const LEAVE_CLAN:String = "clans/del.member";
        public static const CLAN_INFO:String = "clans/get.info";
        public static const CLAN_MEMBERS:String = "clans/get.members";
        public static const CLAN_SEND_REQUEST:String = "clans/requests/create";
        public static const CLAN_GET_REQUESTS:String = "clans/requests/get";
        public static const CLAN_ACCEPT_REQUEST:String = "clans/requests/accept";
        public static const CLAN_REJECT_REQUEST:String = "clans/requests/reject";
        public static const ITEM_TRANSFER:String = "items/transfer";
        public static const GET_CLANS_TOP:String = "clans/get.top";
        public static const SYSTEM_LOG:String = "system/log";
        public static const TRIGGERS_ON_TUTORIAL_COMPLETE:String = "triggers/on.tutorial.complete";
    }
}
