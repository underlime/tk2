package application.server_api
{
    import application.config.AppConfig;
    import application.config.Config;
    import application.config.DataApi;
    import application.controllers.ApplicationController;

    import framework.core.struct.controller.BaseController;
    import framework.core.tools.SaltTool;
    import framework.core.underquery.ApiRequest;

    public class ServerApi extends framework.core.underquery.ServerApi
    {
        private var _dataApiConfig:DataApi;

        public function ServerApi(baseController:BaseController, mock:Boolean)
        {
            super(baseController);
            var applicationController:ApplicationController = _baseController as ApplicationController;
            _dataApiConfig = applicationController.config.getChildByName(Config.DATA_API_CONFIG) as DataApi;

            if (mock)
                _ApiRequestClass = MockApiRequest;
        }

        override protected function _createRequestUri(method:String, queryString:String = ''):String
        {
            var requestUri:String = _dataApiConfig.url.value;
            if (requestUri.charAt(0) != '/')
                requestUri = '/' + requestUri;

            if (requestUri.charAt(requestUri.length - 1) != '/')
                requestUri += '/';

            requestUri += method;
            if (requestUri.charAt(requestUri.length - 1) != '/')
                requestUri += '/';

            if (queryString)
                requestUri += '?' + queryString;

            requestUri = requestUri.replace('//', '/');
            return requestUri;
        }

        override protected function _createFullUrl(requestUri:String):String
        {
            var fullUrl:String = _dataApiConfig.protocol.value + '://' + _dataApiConfig.domain.value;
            if (_dataApiConfig.port.value != 80)
                fullUrl += ':' + String(_dataApiConfig.port.value);
            fullUrl += requestUri;
            return fullUrl;
        }

        override protected function _getTokenSalt():String
        {
            return SaltTool.decode(AppConfig.TOKEN_SALT, AppConfig.Z);
        }
    }

}
