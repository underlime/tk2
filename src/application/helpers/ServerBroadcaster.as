package application.helpers
{
    import org.casalib.events.RemovableEventDispatcher;

    public class ServerBroadcaster extends RemovableEventDispatcher
    {
        private static var _instance:ServerBroadcaster = null;
        private static var _instanceLock:Boolean = true;

        public function ServerBroadcaster()
        {
            if (_instanceLock) {
                throw new Error("Use instance method!");
            }
        }

        public static function instance():ServerBroadcaster
        {
            if (_instance === null) {
                _instanceLock = false;
                _instance = new ServerBroadcaster();
                _instanceLock = true;
            }
            return _instance;
        }
    }
}
