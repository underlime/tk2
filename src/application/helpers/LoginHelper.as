/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 09.10.13
 * Time: 15:01
 */
package application.helpers
{
    public class LoginHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const MAX:int = 30;
        public static const MIN:int = 3;
        public static const LOGIN_PATTERN:RegExp = /^[А-Яa-zа-я0-9_\.-]+$/i;

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function asseert(login:String):Boolean
        {
            if (login.length <= MAX && login.length >= MIN) {
                return LOGIN_PATTERN.test(login);
            }
            return false;
            return false;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
