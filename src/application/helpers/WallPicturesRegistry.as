package application.helpers
{
    import framework.core.socnet.SocNet;

    public class WallPicturesRegistry
    {
        public static function get achievement():String
        {
            var variants:Object = {
                'vk.com': 'photo-34929264_312809186',
                'odnoklassniki.ru': 'http://tomato-kombat.webkraken.ru/assets/ok/achieve.png',
                'default': 'http://tomato-kombat.webkraken.ru/assets/ACHIEVEMENT.png'
            };
            return _chooseSrc(variants);
        }

        public static function get gift():String
        {
            var variants:Object = {
                'vk.com': 'photo-34929264_312809192',
                'odnoklassniki.ru': 'http://tomato-kombat.webkraken.ru/assets/ok/item.png',
                'default': 'http://tomato-kombat.webkraken.ru/assets/GIFT_ITEM.png'
            };
            return _chooseSrc(variants);
        }

        public static function get level():String
        {
            var variants:Object = {
                'vk.com': 'photo-34929264_312809193',
                'odnoklassniki.ru': 'http://tomato-kombat.webkraken.ru/assets/ok/level.png',
                'default': 'http://tomato-kombat.webkraken.ru/assets/LEVEL.png'
            };
            return _chooseSrc(variants);
        }

        public static function get special():String
        {
            var variants:Object = {
                'vk.com': 'photo-34929264_312809195',
                'odnoklassniki.ru': 'http://tomato-kombat.webkraken.ru/assets/ok/new_skill.png',
                'default': 'http://tomato-kombat.webkraken.ru/assets/NEW_SKILL.png'

            };
            return _chooseSrc(variants);
        }

        public static function get news():String
        {
            var variants:Object = {
                'vk.com': 'photo-34929264_312809196',
                'odnoklassniki.ru': 'http://tomato-kombat.webkraken.ru/assets/ok/news.png',
                'default': 'http://tomato-kombat.webkraken.ru/assets/NEWS.png'
            };
            return _chooseSrc(variants);
        }

        public static function get victory():String
        {
            var variants:Object = {
                'vk.com': 'photo-34929264_312809197',
                'odnoklassniki.ru': 'http://tomato-kombat.webkraken.ru/assets/ok/victory.png',
                'default': 'http://tomato-kombat.webkraken.ru/assets/VICTORY.png'
            };
            return _chooseSrc(variants);
        }

        private static function _chooseSrc(variantsTable:Object):String
        {
            var src:String;
            switch (SocNet.instance().socNetName) {
                case SocNet.VK_COM:
                case SocNet.MOCK:
                    src = variantsTable['vk.com'];
                    break;
                case SocNet.ODNOKLASSNIKI_RU:
                    src = variantsTable['odnoklassniki.ru'];
                    break;
                default:
                    src = variantsTable['default'];
                    break;
            }
            return src;
        }
    }
}
