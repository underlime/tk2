/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.06.13
 * Time: 12:52
 */
package application.helpers
{
    import application.models.ModelData;
    import application.models.chars.CharDetail;
    import application.models.chars.CharGroup;
    import application.models.user.User;
    import application.models.user.UserInfo;

    import framework.core.struct.data.ModelsRegistry;

    public class CharsRequestHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public static function getCharsData(char:CharDetail):Object
        {
            var model:User = ModelsRegistry.getModel(ModelData.USER) as User;
            var userInfo:UserInfo = model.getChildByName(ModelData.USER_INFO) as UserInfo;

            var chars:Object = {
                "body": userInfo.body.value,
                "eyes": userInfo.eyes.value,
                "hair": userInfo.hair.value,
                "mouth": userInfo.mouth.value
            };

            var key:String = _getKeyByGroup(char);
            if (chars[key])
                chars[key] = char.mc_1_name.value;


            return chars;
        }

        private static function _getKeyByGroup(char:CharDetail):String
        {
            switch (char.group.value) {
                case CharGroup.BODIES.toString():
                    return "body"
                    break;
                case CharGroup.EYES.toString():
                    return "eyes"
                    break;
                case CharGroup.HAIRS.toString():
                    return "hair"
                    break;
                case CharGroup.MOUTHS.toString():
                    return "mouth"
                    break;
                default :
                    return "";
                    break;
            }

            return "";
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
