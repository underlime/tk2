package application.helpers
{
    import flash.display.Bitmap;
    import flash.display.DisplayObject;

    import framework.core.struct.view.View;
    import framework.core.tools.SourceManager;

    public class PostBgHelper
    {
        public static function buildPicture(basePic:DisplayObject):Bitmap
        {
            var small:Boolean = (basePic.width < 200 && basePic.height < 200);

            var bgName:String = (small) ? 'WALL_POST_BG_S' : 'WALL_POST_BG';
            var marginTop:int = (small) ? 0 : 15;
            var bg:Bitmap = SourceManager.instance.getBitmap(bgName);

            basePic.x = (bg.width - basePic.width)/2;
            basePic.y = (bg.height - basePic.height)/2 + marginTop;

            var view:View = new View();
            view.addChild(bg);
            view.addChild(basePic);
            return view.getAsBitmap();
        }
    }

}
