package framework.core.utils
{
    public function getTimer():Number
    {
        return flash.utils.getTimer() + deltaTime;
    }

}

import flash.utils.getTimer;
import flash.utils.setInterval;

/**
 * эта величина характеризует на сколько флэшовый таймер сбился
 * относительно системного времени
 */
internal var deltaTime:Number = 0;

/**
 * время старта таймера
 */
internal const startTime:Number = ( new Date() ).getTime() - getTimer();

/**
 * взводим таймер для периодической синхронизации
 */
setInterval(function ():void
{
    deltaTime = ( new Date() ).getTime() - startTime - getTimer();
}, 1e3);