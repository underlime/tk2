/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.09.13
 * Time: 18:21
 */
package framework.core.utils
{
    import flash.utils.Dictionary;

    public class Singleton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static var _map:Dictionary;

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getClass(NeedleClass:Class):*
        {
            if (Singleton._map == null)
                Singleton._map = new Dictionary();

            return NeedleClass in Singleton._map ? Singleton._map[NeedleClass] : Singleton._map[NeedleClass] = new NeedleClass();
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
