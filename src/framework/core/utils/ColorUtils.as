package framework.core.utils
{
    import flash.utils.getQualifiedClassName;

    public class ColorUtils
    {
        // NAMESPACES --------------------------------------------------------------------------/
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        //---------------------------------------------------/
        // extract from 24bit color
        //---------------------------------------------------/

        public static function extractRFromRGB(color:uint):uint
        {
            return color >> 16;
        }

        public static function extractGFromRGB(color:uint):uint
        {
            return color >> 8 & 0xFF;
        }

        public static function extractBFromRGB(color:uint):uint
        {
            return color & 0xFF;
        }

        //---------------------------------------------------/
        // extract from 32bit color
        //---------------------------------------------------/

        public static function extractAFromARGB(color:uint):uint
        {
            return color >>> 24;
        }

        public static function extractRFromARGB(color:uint):uint
        {
            return color >>> 16 & 0xFF;
        }

        public static function extractGFromARGB(color:uint):uint
        {
            return color >>> 8 & 0xFF;
        }

        public static function extractBFromARGB(color:uint):uint
        {
            return color & 0xFF;
        }

        //---------------------------------------------------/
        // merge
        //---------------------------------------------------/

        public static function mergeRBG(r:uint, g:uint, b:uint):uint
        {
            return r << 16 | g << 8 | b;
        }

        public static function mergeARBG(a:uint, r:uint, g:uint, b:uint):uint
        {
            return a << 24 | r << 16 | g << 8 | b
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ColorUtils()
        {
            super();
            if ((this as Object).constructor === ColorUtils) {
                throw new ArgumentError('ArgumentError: ' + getQualifiedClassName(this) + ' class cannot be instantiated.');
            }
        }

        // OVERRIDEN PROPERTIES ----------------------------------------------------------------/
        // OVERRIDEN METHODS -------------------------------------------------------------------/
        // IMPLEMENTED METHODS -----------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
        // DEPRECATED PROPERTIES ---------------------------------------------------------------/

    }
}