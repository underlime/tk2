package framework.core.display.tabs
{

    import application.events.ApplicationEvent;

    import flash.display.DisplayObject;
    import flash.events.Event;

    import framework.core.struct.view.View;

    import org.casalib.core.IDestroyable;

    /**
     * Базовый класс для переключателей
     * @author adenikin
     */
    public class TabPanel extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TabPanel(tabs:Vector.<ITab>)
        {
            _tabs = tabs;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setTabs();
        }

        override public function destroy():void
        {
            var count:int = _tabs.length;
            for (var i:int = 0; i < count; i++) {
                var tab:ITab = arguments[i];
                if (tab is IDestroyable)
                    (tab as IDestroyable).destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function select(index:int = 0):void
        {
            if (index >= _tabs.length) {
                throw new Error("There are any tab with same index");
            }

            _tabs[index].select();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _tabs:Vector.<ITab>;

        protected var _x0:Number = 0;
        protected var _y0:Number = 0;

        protected var _dx:Number = 10;
        protected var _dy:Number = 0;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setTabs():void
        {
            if (_y0 != 0 && _dy == 0) {
                this.y = _y0;
            }

            if (_x0 != 0 && _dx == 0) {
                this.x = _x0;
            }

            for (var i:int = 0; i < _tabs.length; i++) {
                if (_dx != 0) {
                    (_tabs[i] as DisplayObject).x = _x0 + _dx * i;
                }

                if (_dy != 0) {
                    (_tabs[i] as DisplayObject).y = _y0 + _dy * i;
                }

                _tabs[i].addEventListener(ApplicationEvent.TAB_CHOOSE, _tabChooseHandler);
                addChild(_tabs[i] as DisplayObject);
            }
        }

        protected function _changeTab():void
        {
            super.dispatchEvent(new Event(Event.CHANGE));
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _selectedTab:int = 0;
        private var _chosenTab:ITab;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _detectSelectedTab(chosenTab:ITab):void
        {
            for (var i:int = 0; i < _tabs.length; i++) {
                if (_tabs[i] == chosenTab) {
                    _selectedTab = i;
                }
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _tabChooseHandler(e:ApplicationEvent):void
        {
            e.stopImmediatePropagation();
            var chosenTab:ITab = e.currentTarget as ITab;
            if (!_chosenTab || _chosenTab != chosenTab) {
                for (var i:int = 0; i < _tabs.length; i++) {
                    if (_tabs[i] != chosenTab) {
                        _tabs[i].unselect();
                    }
                }
                _detectSelectedTab(chosenTab);
                _chosenTab = chosenTab;
                _changeTab();
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get selectedTab():int
        {
            return _selectedTab;
        }

        public function get tabs():Vector.<ITab>
        {
            return _tabs;
        }
    }

}