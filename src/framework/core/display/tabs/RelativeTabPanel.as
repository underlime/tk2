/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.07.13
 * Time: 11:53
 */
package framework.core.display.tabs
{
    import application.events.ApplicationEvent;

    import flash.display.DisplayObject;

    public class RelativeTabPanel extends TabPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RelativeTabPanel(tabs:Vector.<ITab>)
        {
            super(tabs);
            _dx = 0;
            _dy = 0;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function _setTabs():void
        {
            var count:int = _tabs.length;
            var vMargin:int = 0;
            var hMargin:int = 0;

            for (var i:int = 0; i < count; i++) {
                var tab:DisplayObject = _tabs[i] as DisplayObject;


                tab.x = hMargin;
                tab.y = vMargin;
                addChild(tab);

                if (_dx > 0)
                    hMargin += tab.width + _dx;

                if (_dy > 0)
                    vMargin += tab.height + _dy;

                _tabs[i].addEventListener(ApplicationEvent.TAB_CHOOSE, _tabChooseHandler);
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
