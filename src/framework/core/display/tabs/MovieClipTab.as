package framework.core.display.tabs
{

    import application.events.ApplicationEvent;

    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    import framework.core.struct.view.View;

    public class MovieClipTab extends View implements ITab
    {
        private var _checked:Boolean = false;
        private var _tabName:String;
        protected var _tab:MovieClip;

        protected var _hoverFrame:int = 2;
        protected var _activeFrame:int = 3;
        protected var _normalFrame:int = 1;

        protected var _save:Boolean = true;

        public function MovieClipTab(tab:String)
        {
            _tabName = tab;
            this.buttonMode = true;
            this.useHandCursor = true;
        }

        override protected function render():void
        {
            _tab = super.source.getMovieClip(_tabName, _save);
            _tab.gotoAndStop(1);
            _setTab();
            _activateTab();

            addChild(_tab);
        }

        override public function destroy():void
        {
            _disactivateTab();
            super.destroy();
        }

        /**
         * Выделить
         */
        public function select():void
        {
            if (!_tab) {
                throw new Error("Сначала необходимо добавить переключатель на сцену");
                return;
            }

            if (!_checked) {
                _tab.gotoAndStop(_activeFrame);
                _checked = true;
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TAB_CHOOSE));
                _disactivateTab();
            }
        }

        /**
         * Снять выделение с кнопки
         */
        public function unselect():void
        {
            if (!_tab) {
                throw new Error("Сначала необходимо добавить переключатель на сцену");
                return;
            }

            if (_checked) {
                _tab.gotoAndStop(_normalFrame);
                _checked = false;
                _activateTab();
            }
        }

        private function _activateTab():void
        {
            addEventListener(MouseEvent.ROLL_OVER, _onOver_handler);
            addEventListener(MouseEvent.ROLL_OUT, _onOut_handler);
            addEventListener(MouseEvent.CLICK, _onClick_handler);
        }

        private function _disactivateTab():void
        {
            this.removeEventListener(MouseEvent.ROLL_OVER, _onOver_handler);
            this.removeEventListener(MouseEvent.ROLL_OUT, _onOut_handler);
            this.removeEventListener(MouseEvent.CLICK, _onClick_handler);
        }

        protected function _setTab():void
        {

        }

        protected function _onOver_handler(e:MouseEvent):void
        {
            _tab.gotoAndStop(_hoverFrame);
        }

        protected function _onOut_handler(e:MouseEvent):void
        {
            _tab.gotoAndStop(_normalFrame);
        }

        protected function _onClick_handler(e:MouseEvent):void
        {
            this.select();
        }

        public function get checked():Boolean
        {
            return _checked;
        }

    }

}