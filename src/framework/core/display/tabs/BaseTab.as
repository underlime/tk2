/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 10:05
 */
package framework.core.display.tabs
{
    import framework.core.struct.view.View;

    public class BaseTab extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseTab()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var tabID:int = 0;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function enable():void
        {
            if (!_enabled) {
                _enableTab();
                _enabled = true;
            }
        }

        public function disable():void
        {
            if (_enabled) {
                _disableTab();
                _enabled = false;
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _enableTab():void
        {
            throw new Error("You must override method");
        }

        protected function _disableTab():void
        {
            throw new Error("You must override method");
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _enabled:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get enabled():Boolean
        {
            return _enabled;
        }
    }
}
