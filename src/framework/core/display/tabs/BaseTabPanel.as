/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 10:16
 */
package framework.core.display.tabs
{
    import flash.events.MouseEvent;

    import framework.core.events.GameEvent;
    import framework.core.struct.view.View;

    public class BaseTabPanel extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseTabPanel(tabs:Vector.<BaseTab>)
        {
            _tabs = tabs;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addTabs();
            _setupTabs();
            _bindTabs();
        }

        override public function destroy():void
        {
            for (var i:int = 0; i < _tabs.length; i++) {
                _tabs[i].removeEventListener(MouseEvent.CLICK, _tabClickHandler);
                _tabs[i].destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _currentTab:BaseTab;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _addTabs():void
        {
            throw new Error("You must override method")
        }

        protected function _setupTabs():void
        {
            _currentTab = _tabs[0];
            _currentTab.enable();
            _sendEvent();
        }

        protected function _bindTabs():void
        {
            for (var i:int = 0; i < _tabs.length; i++) {
                _tabs[i].addEventListener(MouseEvent.CLICK, _tabClickHandler);
            }
        }

        protected function _sendEvent():void
        {
            var event:GameEvent = new GameEvent(GameEvent.CHANGE);
            event.data.tabID = _currentTab.tabID;
            super.dispatchEvent(event);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _tabs:Vector.<BaseTab>;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _tabClickHandler(event:MouseEvent):void
        {
            var chosenTab:BaseTab = (event.currentTarget as BaseTab);
            if (chosenTab != _currentTab) {
                _currentTab.disable();
                _currentTab = chosenTab;
                _currentTab.enable();
                _sendEvent();
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get tabs():Vector.<BaseTab>
        {
            return _tabs;
        }
    }
}
