package framework.core.display.tabs
{
    import flash.events.IEventDispatcher;

    /**
     * Интерфейс для кнопки-переключателя
     * @author Aleksey Tsvetkov
     */
    public interface ITab extends IEventDispatcher
    {
        /**
         * Включен ли переключатель
         */
        function get checked():Boolean;

        /**
         * Выделить
         */
        function select():void;

        /**
         * Снять выделение
         */
        function unselect():void;
    }

}