/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.07.13
 * Time: 15:57
 */
package framework.core.display.scroll
{
    import application.events.JsScrollEvent;
    import application.events.JsScrollEvent;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.core.struct.view.View;

    import org.casalib.core.Destroyable;

    public class ScrollContentObserver extends Destroyable
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        /**
         * @param scrollContent  Контент для прокрутки
         * @param scroll         Скроллбар
         * @param scrollRect     Область прокрутки
         * @param customHeight   своя высота
         */
        public function ScrollContentObserver(scrollContent:View, scroll:Scroll, scrollRect:Rectangle, customHeight:int = 0)
        {
            super();
            _content = scrollContent;
            _contentHeight = customHeight > 0 ? customHeight : scrollContent.height;
            scrollContent.scrollRect = scrollRect;
            scrollContent.addEventListener(MouseEvent.MOUSE_WHEEL, _changeByWheel);
            _scrollbar = scroll;
            _scrollbar.addEventListener(ScrollEvent.CHANGE, _updateContent);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            if (_content && !_content.destroyed)
                _content.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _content:View;
        protected var _scrollbar:Scroll;
        protected var _contentHeight:Number;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _updateContent(event:ScrollEvent):void
        {
            var scrollable:Number = _contentHeight - _content.scrollRect.height;
            var sr:Rectangle = _content.scrollRect.clone();
            sr.y = scrollable * event.percent;

            _content.scrollRect = sr;
        }

        protected function _changeByWheel(event:Event):void
        {
            var percent:Number = 0;
            if (event is MouseEvent) {
                percent = _scrollbar.percent - MouseEvent(event).delta / 100
            }
            else if (event is JsScrollEvent) {
                percent = _scrollbar.percent - JsScrollEvent(event).dir * 3 / 100;
            }

            if (percent < 0) {
                percent = 0;
            }
            else if (percent > 1) {
                percent = 1;
            }

            _scrollbar.percent = percent;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
