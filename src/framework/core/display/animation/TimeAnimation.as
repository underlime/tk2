/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.10.13
 * Time: 15:40
 */
package framework.core.display.animation
{
    import flash.display.Bitmap;

    import org.casalib.time.Interval;

    /**
     * Класс для анимирования растровых кадров.
     * FPS = FPS флеш плеера
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class TimeAnimation extends Animation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TimeAnimation(frames:Vector.<Bitmap>, fps:int = 30)
        {
            super(frames);
            _fps = fps;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            super.setup();
            _setupInterval();
        }

        override public function destroy():void
        {
            if (_animationInterval && !_animationInterval.destroyed)
                _animationInterval.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _enableFrameChange():void
        {
            _setupInterval();
            _animationInterval.start();
            _animationInterval.start();
        }

        override protected function _disableFrameChange():void
        {
            _animationInterval.stop();
            _animationInterval.destroy();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animationInterval:Interval;
        private var _fps:int = 30;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupInterval():void
        {
            var delay:Number = 1000 / _fps;
            _animationInterval = Interval.setInterval(super._changeFrame, delay);
            _animationInterval.repeatCount = Number.MAX_VALUE;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get fps():int
        {
            return _fps;
        }

        public function set fps(value:int):void
        {
            _fps = value;
        }
    }
}
