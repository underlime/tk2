package framework.core.display.animation
{
    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.core.helpers.MathHelper;
    import framework.core.struct.view.View;

    /**
     * Класс для анимирования растровых кадров.
     * FPS = FPS флеш плеера
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class Animation extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Animation(frames:Vector.<Bitmap>)
        {
            _frames = frames;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            for (var i:int = 0; i < _frames.length; i++) {
                if (_frames[i].width > maxWidth) {
                    maxWidth = _frames[i].width;
                    super.w = maxWidth;
                }

                if (_frames[i].height > maxHeight) {
                    maxHeight = _frames[i].height;
                    super.h = maxHeight;
                }
            }

            _totalFrames = _frames.length;
        }

        override protected function render():void
        {
            _addFrame();
            play();
        }

        override public function destroy():void
        {
            _removeFrame();
            _disableFrameChange();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var maxWidth:Number = 0;
        public var maxHeight:Number = 0;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function play():void
        {
            _enableFrameChange();
        }

        public function stop():void
        {
            _frameTarget = 0;
            _disableFrameChange();
        }

        public function gotoAndPlay(frame:int):void
        {
            _currentFrame = MathHelper.clamp(frame, 1, _totalFrames);
            _addFrame();
            play();
        }

        public function gotoAndStop(frame:int):void
        {
            _currentFrame = MathHelper.clamp(frame, 1, _totalFrames);
            _frameTarget = 0;
            _addFrame();
            stop();
        }

        public function playTo(frameTarget:int):void
        {
            _frameTarget = MathHelper.clamp(frameTarget, 1, _totalFrames);
            play();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _enableFrameChange():void
        {
            if (!hasEventListener(Event.ENTER_FRAME)) {
                addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            }
        }

        protected function _disableFrameChange():void
        {
            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        protected function _changeFrame():void
        {
            if (_currentFrame != _frameTarget) {
                if (!_reverse)
                    _currentFrame = _currentFrame + 1 > _totalFrames ? 1 : _currentFrame + 1;
                else
                    _currentFrame = _currentFrame - 1 < 1 ? _totalFrames : _currentFrame - 1;
                _addFrame();
            } else {
                stop();
            }
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _frames:Vector.<Bitmap> = new Vector.<Bitmap>();
        private var _currentBitmap:Bitmap;

        private var _frameTarget:int = 0;
        private var _currentFrame:int = 1;
        private var _totalFrames:int;

        private var _reverse:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addFrame():void
        {
            _removeFrame();
            _currentBitmap = _frames[_currentFrame - 1];
            addChild(_currentBitmap);
        }

        private function _removeFrame():void
        {
            if (_currentBitmap && this.contains(_currentBitmap)) {
                removeChild(_currentBitmap);
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _enterFrameHandler(e:Event):void
        {
            _changeFrame();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get totalFrames():int
        {
            return _totalFrames;
        }

        public function get currentFrame():int
        {
            return _currentFrame;
        }

        public function get frames():Vector.<Bitmap>
        {
            return _frames;
        }

        public function get reverse():Boolean
        {
            return _reverse;
        }

        public function set reverse(value:Boolean):void
        {
            _reverse = value;
        }
    }

}