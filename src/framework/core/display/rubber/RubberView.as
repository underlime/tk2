/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.08.13
 * Time: 12:03
 */
package framework.core.display.rubber
{
    import flash.display.Bitmap;
    import flash.display.Shape;

    import framework.core.struct.view.View;

    public class RubberView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RubberView(left:Bitmap, center:Bitmap, right:Bitmap)
        {
            _left = left;
            _center = center;
            _right = right;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function setup():void
        {
            addChild(_left);
            addChild(_right);
            addChild(_centerShape);
        }

        override protected function render():void
        {
            _updatePosition();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _left:Bitmap;
        private var _center:Bitmap;
        private var _right:Bitmap;

        private var _centerShape:Shape = new Shape();

        private var _x0:int = 0;
        private var _y0:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updatePosition():void
        {
            _left.x = _x0;
            _left.y = _y0;

            var currentWidth:Number = _w - _left.width - _right.width;

            _centerShape.graphics.clear();
            _centerShape.x = _left.x + _left.width;
            _centerShape.y = _y0;

            _centerShape.graphics.beginBitmapFill(_center.bitmapData);
            _centerShape.graphics.drawRect(0, 0, currentWidth, _left.height);
            _centerShape.graphics.endFill();

            _right.x = _centerShape.x + currentWidth;

            _h = _left.height;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        override public function set w(value:Number):void
        {
            _w = value;
            _updatePosition();
        }

        public function get x0():int
        {
            return _x0;
        }

        public function set x0(value:int):void
        {
            _x0 = value;
        }

        public function get y0():int
        {
            return _y0;
        }

        public function set y0(value:int):void
        {
            _y0 = value;
        }
    }
}
