/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 09.09.13
 * Time: 15:39
 */
package framework.core.display.rubber
{
    import flash.display.Bitmap;
    import flash.display.Shape;

    import framework.core.struct.view.View;
    import framework.core.utils.Assert;

    public class VerticalRubber extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function VerticalRubber(top:Bitmap, center:Bitmap, bottom:Bitmap)
        {
            super();

            _w = 5;
            _h = 20;

            CONFIG::DEBUG {
                Assert.objectIsNull(top, "top is null!");
                Assert.objectIsNull(center, "center is null!");
                Assert.objectIsNull(bottom, "bottom is null!");
            }

            _top = top;
            _center = center;
            _bottom = bottom;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            addChild(_top);
            addChild(_bottom);
            addChild(_centerShape);

            _updatePosition();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _top:Bitmap;
        private var _center:Bitmap;
        private var _bottom:Bitmap;

        private var _x0:int = 0;
        private var _y0:int = 0;

        private var _centerShape:Shape = new Shape();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updatePosition():void
        {
            _top.x = _x0;
            _top.y = _y0;

            var currentHeight:Number = _h - _top.height - _bottom.height;

            _centerShape.graphics.clear();
            _centerShape.x = _x0;
            _centerShape.y = _top.y + _top.height;

            _centerShape.graphics.beginBitmapFill(_center.bitmapData);
            _centerShape.graphics.drawRect(0, 0, _w, currentHeight);
            _centerShape.graphics.endFill();

            _bottom.x = _x0;
            _bottom.y = _centerShape.y + currentHeight;

            _w = _top.width;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get x0():int
        {
            return _x0;
        }

        public function set x0(value:int):void
        {
            _x0 = value;
        }

        public function get y0():int
        {
            return _y0;
        }

        public function set y0(value:int):void
        {
            _y0 = value;
        }

        override public function set w(value:Number):void
        {
            _w = value;
            _updatePosition();
        }

        override public function set h(value:Number):void
        {
            _h = value;
            _updatePosition();
        }
    }
}
