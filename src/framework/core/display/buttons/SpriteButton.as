package framework.core.display.buttons
{
    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.core.struct.view.View;
    import framework.core.tools.SourceManager;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class SpriteButton extends View implements IButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpriteButton(source:String)
        {
            _source = source;
            super();

            _w = 100;
            _h = 100;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _createButton();
            _setupBorders();
            addChild(_button);
            enable();
        }

        override public function destroy():void
        {
            _unbindReactions();
            if (_button && contains(_button)) {
                removeChild(_button);
                _button = null;
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function block():void
        {
            _unbindReactions();
            _rectangle.y = _blockPosition;
            _button.scrollRect = _rectangle;
            _blocked = true;
        }

        public function select():void
        {
            _unbindReactions();
            _rectangle.y = _selectedPosition;
            _button.scrollRect = _rectangle;
            _checked = true;
        }

        public function enable():void
        {
            _rectangle = new Rectangle(0, 0, _w, _h);
            _rectangle.y = _outPosition;
            _button.scrollRect = _rectangle;
            _bindReactions();
            _blocked = false;
        }

        public function unselect():void
        {
            _checked = false;
            enable();
        }

        public function update():void
        {
            _rectangle = new Rectangle(0, 0, _w, _h);
            _rectangle.y = _outPosition;
            _button.scrollRect = _rectangle;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _source:String;
        protected var _button:Bitmap;
        protected var _rectangle:Rectangle;

        protected var _overPosition:int = 0;
        protected var _outPosition:int = 0;
        protected var _downPosition:int = 0;
        protected var _blockPosition:int = 0;
        protected var _selectedPosition:int = 0;

        protected var _checked:Boolean = false;

        protected var _blocked:Boolean = false;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _createButton():void
        {
            _button = SourceManager.instance.getBitmap(_source);
        }

        protected function _setupBorders():void
        {
            _button.scrollRect = new Rectangle(0, 0, _w, _h);
        }

        protected function _bindReactions():void
        {
            buttonMode = true;
            useHandCursor = true;

            addEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            addEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
            addEventListener(MouseEvent.MOUSE_DOWN, _onDownHandler);
        }

        protected function _unbindReactions():void
        {
            buttonMode = false;
            useHandCursor = false;

            removeEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
            removeEventListener(MouseEvent.MOUSE_DOWN, _onDownHandler);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _onOutHandler(e:MouseEvent):void
        {
            _rectangle.y = _outPosition;
            _button.scrollRect = _rectangle;
        }

        protected function _onOverHandler(e:MouseEvent):void
        {
            _rectangle.y = _overPosition;
            _button.scrollRect = _rectangle;
        }

        protected function _onDownHandler(event:MouseEvent):void
        {
            _rectangle.y = _downPosition;
            _button.scrollRect = _rectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get checked():Boolean
        {
            return _checked;
        }

        public function get blocked():Boolean
        {
            return _blocked;
        }
    }

}