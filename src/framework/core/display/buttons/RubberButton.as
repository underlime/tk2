/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.07.13
 * Time: 10:03
 */
package framework.core.display.buttons
{
    import application.views.text.AppTextField;

    import flash.display.Bitmap;
    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;

    public class RubberButton extends SpriteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RubberButton(label:String, leftCorner:Bitmap, rightCorner:Bitmap, centerFill:Bitmap)
        {
            _label = label;
            _leftCorner = leftCorner;
            _rightCorner = rightCorner;
            _centerFill = centerFill;

            _parts = [_leftCorner, _rightCorner];

            super("");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _createButton();
            _createLabel();
            _updateLabelPosition();

            _createRectangle();
            enable();
        }

        override public function block():void
        {
            _unbindReactions();
            _moveSprites(_blockPosition);
            _blocked = true;
            _txtLabel.filters = [];
        }

        override public function select():void
        {
            _unbindReactions();
            _moveSprites(_selectedPosition);
            _checked = true;
        }

        override public function enable():void
        {
            _moveSprites(_outPosition);
            _bindReactions();
            _blocked = false;
            _txtLabel.filters = _textFilters;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _txtLabel:AppTextField;
        protected var _borderMargin:int = 3;
        protected var _centerShape:Shape;

        protected var _centerWidth:int = 0;
        protected var _leftCorner:Bitmap;
        protected var _rightCorner:Bitmap;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _createLabel():void
        {
            _txtLabel = new AppTextField();
            _addStyle();
            _txtLabel.autoSize = TextFieldAutoSize.LEFT
            addChild(_txtLabel);
            _txtLabel.text = _label;
        }

        protected function _addStyle():void
        {
            _txtLabel.color = 0x000000;
            _txtLabel.size = 14;
            _txtLabel.bold = true;
        }

        override protected function _createButton():void
        {
            _calcCenterWidth();

            addChild(_leftCorner);
            _centerShape = new Shape();
            addChild(_centerShape);

            _centerShape.graphics.beginBitmapFill(_centerFill.bitmapData);
            _centerShape.graphics.drawRect(_leftCorner.width, 0, _centerWidth + 1, _leftCorner.height);
            _centerShape.graphics.endFill();
            _parts.push(_centerShape);

            addChild(_rightCorner);
            _rightCorner.x = _leftCorner.width + _centerWidth;
        }

        protected function _calcCenterWidth():void
        {
            _centerWidth = _w - _leftCorner.width - _rightCorner.width;
        }

        protected function _updateLabelPosition():void
        {
            _txtLabel.x = _w / 2 - _txtLabel.textWidth / 2;
            _txtLabel.y = _h / 2 - _txtLabel.height / 2;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _label:String;
        private var _centerFill:Bitmap;
        private var _parts:Array;

        private var _textFilters:Array = [];

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _createRectangle():void
        {
            _rectangle = new Rectangle(0, 0, _w, _h);
        }

        private function _moveSprites(y:Number):void
        {
            var count:int = _parts.length;
            for (var i:int = 0; i < count; i++) {
                _rectangle.y = y;
                _parts[i].scrollRect = _rectangle;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _onOutHandler(e:MouseEvent):void
        {
            _moveSprites(_outPosition);
        }

        override protected function _onOverHandler(e:MouseEvent):void
        {
            _moveSprites(_overPosition);
        }

        override protected function _onDownHandler(event:MouseEvent):void
        {
            _moveSprites(_downPosition);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set borderMargin(value:int):void
        {
            _borderMargin = value;
        }

        public function get label():String
        {
            return _label;
        }

        public function set label(value:String):void
        {
            _label = value;
            if (_txtLabel)
                _txtLabel.text = label;
        }

        public function set textFilters(value:Array):void
        {
            _textFilters = value;
            if (_txtLabel) {
                _txtLabel.filters = _textFilters;
            }
        }
    }
}
