/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 01.06.13
 * Time: 18:00
 */
package framework.core.display.buttons
{
    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    import framework.core.struct.view.View;

    public class MovieClipButton extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MovieClipButton(source:String, save:Boolean = true)
        {
            super();
            _source = source;
            _save = save;
            this.buttonMode = true;
            this.useHandCursor = true;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setButton();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _onOver_handler);
            removeEventListener(MouseEvent.ROLL_OUT, _onOut_handler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var hoverFrame:int = 2;
        public var linkFrame:int = 1;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function disable():void
        {
            _disableHandlers();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _source:String;
        private var _save:Boolean;

        private var _button:MovieClip;

        private var _buttonWidth:Number;
        private var _buttonHeight:Number;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setButton():void
        {
            _button = super.source.getMovieClip(_source, _save);
            _button.gotoAndStop(linkFrame);

            _setHandlers();

            if (_buttonWidth) {
                _button.width = _buttonWidth;
            }
            if (_buttonHeight) {
                _button.height = _buttonHeight;
            }
            addChild(_button);
        }

        protected function _setHandlers():void
        {
            addEventListener(MouseEvent.ROLL_OVER, _onOver_handler);
            addEventListener(MouseEvent.ROLL_OUT, _onOut_handler);

            this.buttonMode = true;
            this.useHandCursor = true;
        }

        protected function _disableHandlers():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _onOver_handler);
            removeEventListener(MouseEvent.ROLL_OUT, _onOut_handler);
            this.buttonMode = false;
            this.useHandCursor = false;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _onOver_handler(e:MouseEvent):void
        {
            if (_button.totalFrames > 1) {
                _button.gotoAndStop(this.hoverFrame);
            }
        }

        protected function _onOut_handler(e:MouseEvent):void
        {
            if (_button.totalFrames > 1) {
                _button.gotoAndStop(this.linkFrame);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set buttonWidth(value:Number):void
        {
            _buttonWidth = value;
        }

        public function set buttonHeight(value:Number):void
        {
            _buttonHeight = value;
        }

        public function get button():MovieClip
        {
            return _button;
        }
    }
}
