/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.07.13
 * Time: 10:30
 */
package framework.core.display.buttons
{
    import framework.core.display.tabs.ITab;

    public interface IButton extends ITab
    {
        function block():void;

        function enable():void;
    }
}
