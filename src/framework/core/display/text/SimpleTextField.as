package framework.core.display.text
{
    import flash.events.Event;
    import flash.text.AntiAliasType;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;

    import framework.core.helpers.ObjectHelper;

    import org.casalib.display.CasaTextField;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class SimpleTextField extends CasaTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SimpleTextField(settings:Object = null)
        {
            super();

            _settings = settings;
            addEventListener(Event.ADDED_TO_STAGE, _init);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var size:int = 12;
        public var color:uint = 0xffffff;
        public var align:String = TextFormatAlign.LEFT;
        public var bold:Boolean = false;
        public var italic:Boolean = false;
        public var systemFamily:String;
        public var fontFamily:String = "";
        public var data:String = "";

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _settings:Object = {};

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addDefaultSettings():void
        {
            super.mouseEnabled = false;
            super.embedFonts = true;
            super.selectable = false;
            super.antiAliasType = AntiAliasType.ADVANCED;
        }

        private function _addUserSettings():void
        {
            if (_settings)
                _checkPassSettings();
            _addFormatSettings();
        }

        private function _checkPassSettings():void
        {
            var formatKeys:Array = ["size", "align", "color", "bold", "italic", "text", "htmlText", "x", "y", "autosize", "width", "height", "wordWrap", "multiline"];
            var formatData:Object = ObjectHelper.filterObject(_settings, formatKeys);

            for (var key:String in formatData) {
                if (this.hasOwnProperty(key))
                    this[key] = formatData[key];
            }
        }

        private function _addFormatSettings():void
        {
            var format:TextFormat = new TextFormat();

            if (systemFamily) {
                super.embedFonts = false;
                format.font = systemFamily;
            } else {
                format.font = fontFamily;
            }

            format.size = size;
            format.align = align;
            format.color = color;
            format.bold = bold;
            format.italic = italic;

            super.defaultTextFormat = format;

        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _init(e:Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, _init);

            _addDefaultSettings();
            _addUserSettings();
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }

}