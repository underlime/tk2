/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.09.13
 * Time: 20:49
 */
package framework.core.display.components
{
    import application.sound.SoundManager;
    import application.sound.lib.UISound;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.core.utils.Singleton;

    public class RadioButton extends CheckBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RadioButton(source:String, label:String)
        {
            super(source, label);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            super.render();
            addEventListener(Event.CHANGE, _radioChangeHandler);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _sound:SoundManager = Singleton.getClass(SoundManager);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _radioChangeHandler(event:Event):void
        {
            event.stopImmediatePropagation();
            super.dispatchEvent(new Event(Event.SELECT));
        }

        override protected function _clickHandler(event:MouseEvent):void
        {
            select();
            _sound.playUISound(UISound.CLICK);
        }

        override protected function _onOverHandler(event:MouseEvent):void
        {
            super._onOverHandler(event);
            _sound.playUISound(UISound.MOUSE_OVER);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
