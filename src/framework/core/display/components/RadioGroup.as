/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.09.13
 * Time: 20:49
 */
package framework.core.display.components
{
    import flash.events.Event;

    import framework.core.struct.view.View;
    import framework.core.utils.Assert;

    public class RadioGroup extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RadioGroup(radioButtons:Vector.<RadioButton>)
        {
            super();
            _radioButtons = radioButtons;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _addButtons();
        }

        override public function destroy():void
        {
            var count:int = _radioButtons.length;
            for (var i:int = 0; i < count; i++) {
                if (_radioButtons[i] && !_radioButtons[i].destroyed) {
                    _radioButtons[i].removeEventListener(Event.SELECT, _buttonChangeHandler);
                    _radioButtons[i].destroy();
                }
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function select(radioID:int):void
        {
            CONFIG::DEBUG {
                Assert.indexInRange(radioID, 0, _radioButtons.length - 1);
            }

            var radioButton:RadioButton = _radioButtons[radioID];

            if (_currentButton && _currentButton != radioButton)
                _currentButton.unselect();

            radioButton.select();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _dx:int = 0;
        protected var _dy:int = 0;
        protected var _radioButtons:Vector.<RadioButton>;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _addButtons():void
        {
            var count:int = _radioButtons.length;
            var vMargin:int = 0;
            var hMargin:int = 0;

            for (var i:int = 0; i < count; i++) {
                var tab:RadioButton = _radioButtons[i];
                addChild(tab);

                tab.x = hMargin;
                tab.y = vMargin;

                if (_dx > 0)
                    hMargin += tab.w + _dx;

                if (_dy > 0)
                    vMargin += tab.h + _dy;

                _radioButtons[i].addEventListener(Event.SELECT, _buttonChangeHandler);
            }
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _currentButton:RadioButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _buttonChangeHandler(e:Event):void
        {
            e.stopImmediatePropagation();
            var button:RadioButton = e.currentTarget as RadioButton;

            if (!_currentButton) {
                _currentButton = button;
            } else {
                if (_currentButton != button) {
                    _currentButton.unselect();
                    _currentButton = button;
                    dispatchEvent(new Event(Event.CHANGE));
                }
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get selectedRadio():int
        {
            if (!_currentButton)
                return -1;
            var count:int = _radioButtons.length;
            for (var i:int = 0; i < count; i++) {
                if (_radioButtons[i] == _currentButton) {
                    return i;
                }
            }
            return -1;
        }

    }
}
