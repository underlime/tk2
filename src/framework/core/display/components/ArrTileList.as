/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.08.13
 * Time: 13:37
 */
package framework.core.display.components
{
    import flash.display.DisplayObject;

    import framework.core.struct.view.View;
    import framework.core.utils.DestroyUtils;

    public class ArrTileList extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArrTileList(tiles:Array = null)
        {
            super();
            _assertTiles(tiles);
            _tiles = tiles;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            var count:int = _tiles.length;
            for (var i:int = 0; i < count; i++)
                if (_tiles[i])
                    DestroyUtils.destroy(_tiles[i]);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function draw():void
        {
            if (_tiles)
                _drawMesh();
        }

        public function clear():void
        {
            if (!_tiles)
                return;
            var count:int = _tiles.length;
            for (var i:int = 0; i < count; i++)
                if (_tiles[i] && this.contains(_tiles[i]))
                    removeChild(_tiles[i]);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _x0:Number = 0;
        protected var _y0:Number = 0;

        protected var _dx:Number = 86;
        protected var _dy:Number = 76;

        protected var _cols:int = 5;
        protected var _lines:int = 3;

        protected var _limit:int = 15;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _drawMesh():void
        {
            var cx:Number = _x0;
            var cy:Number = _y0;
            var colsCount:int = 0;
            var linesCount:int = 1;
            var rendered:int = 0;

            var count:int = _tiles.length;
            var target:int = _limit <= count ? _limit : count;

            for (var i:int = 0; i < target; i++) {

                _tiles[i].x = cx;
                _tiles[i].y = cy;

                addChild(_tiles[i]);

                colsCount++;
                rendered++;
                cx += _dx;

                if (colsCount >= _cols) {
                    colsCount = 0;
                    cx = _x0;
                    cy += _dy;
                    linesCount++;
                }
            }
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _tiles:Array;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _assertTiles(arr:Array):void
        {
            if (arr && arr.length > 0) {
                var count:int = arr.length;
                for (var i:int = 0; i < count; i++) {
                    if (!(arr[i] is DisplayObject))
                        throw new ArgumentError("Item is not display object!");
                }
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set tiles(value:Array):void
        {
            clear();
            _tiles = value;
            _assertTiles(value);
        }

        public function get tiles():Array
        {
            return _tiles;
        }
    }
}
