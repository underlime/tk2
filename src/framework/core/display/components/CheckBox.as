/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.09.13
 * Time: 20:27
 */
package framework.core.display.components
{
    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;

    import framework.core.display.buttons.IButton;
    import framework.core.display.text.SimpleTextField;
    import framework.core.struct.view.View;

    public class CheckBox extends View implements IButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CheckBox(source:String, label:String)
        {
            super();
            _source = source;
            _label = label;

            _w = 10;
            _h = 10;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override protected function render():void
        {
            _setupBitmap();
            _addLabelField();
        }

        override public function destroy():void
        {
            _unbindReactions();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function select():void
        {
            _scrollRectangle.y = _selectedPosition;
            _box.scrollRect = _scrollRectangle;

            _checked = true;

            super.dispatchEvent(new Event(Event.CHANGE));
        }

        public function unselect():void
        {
            _scrollRectangle.y = _outPosition;
            _box.scrollRect = _scrollRectangle;

            _checked = false;

            super.dispatchEvent(new Event(Event.CHANGE));
        }

        public function block():void
        {
            _scrollRectangle.y = _blockPosition;
            _box.scrollRect = _scrollRectangle;
            _unbindReactions();
        }

        public function enable():void
        {
            _scrollRectangle.y = _outPosition;
            _box.scrollRect = _scrollRectangle;
            _bindReactions();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _outPosition:int = 0;
        protected var _overPosition:int = 0;
        protected var _blockPosition:int = 0;
        protected var _selectedPosition:int = 0;
        protected var _selectedOverPosition:int = 0;

        protected var _box:Bitmap;
        protected var _scrollRectangle:Rectangle;

        protected var _label:String;
        protected var _txtLabel:SimpleTextField;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _addLabelField():void
        {
            _txtLabel = new SimpleTextField();
            _addStyle();
            addChild(_txtLabel);
            _txtLabel.text = _label;
        }

        protected function _addStyle():void
        {
            _txtLabel.autoSize = TextFieldAutoSize.LEFT;
            _txtLabel.size = 14;
            _txtLabel.color = 0x000000;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _source:String;
        private var _checked:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBitmap():void
        {
            _box = super.source.getBitmap(_source);
            addChild(_box);

            _scrollRectangle = new Rectangle(0, _outPosition, _w, _h);
            enable();
        }

        private function _bindReactions():void
        {
            buttonMode = true;
            useHandCursor = true;

            addEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            addEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _unbindReactions():void
        {
            buttonMode = false;
            useHandCursor = false;

            removeEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
            removeEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _clickHandler(event:MouseEvent):void
        {
            if (!_checked)
                select();
            else
                unselect();
        }

        protected function _onOutHandler(event:MouseEvent):void
        {
            if (!_checked)
                _scrollRectangle.y = _outPosition;
            else
                _scrollRectangle.y = _selectedPosition;

            _box.scrollRect = _scrollRectangle;
        }

        protected function _onOverHandler(event:MouseEvent):void
        {
            if (_checked)
                _scrollRectangle.y = _selectedOverPosition;
            else
                _scrollRectangle.y = _overPosition;

            _box.scrollRect = _scrollRectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get checked():Boolean
        {
            return _checked;
        }

        public function set checked(value:Boolean):void
        {
            _checked = value;
            if (_checked)
                select();
            else
                unselect();
        }
    }
}
