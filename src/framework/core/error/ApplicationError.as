/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.12.12
 */

package framework.core.error
{
    public class ApplicationError
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const UNKNOWN_ERROR:String = "Unknown error";
        public static const UNAVAILABLE_EXTERNAL_INTERFACE:String = "Unavailable external interface";
        public static const UNAVAILABLE_REMOTE_SERVER:String = "Unavailabel remote old.server";
        public static const ACCSESS_DENIED:String = "Access denied";
        public static const APPLICATION_TEMPORARILY_UNAVAIBLE:String = "Temporarily unavaible";
        public static const LOAD_ERROR:String = "Load error";
        public static const SOCIAL_NETWORK_ERROR:String = "Social network error";
        public static const SEVER_ERROR:String = "Server error";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ApplicationError()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var code:int = 0;
        public var message:String = ApplicationError.UNKNOWN_ERROR;
        public var type:String = ApplicationError.UNKNOWN_ERROR;
        public var errorName:String = ApplicationError.UNKNOWN_ERROR;

        public var fatal:Boolean = false;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
