/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.10.13
 * Time: 12:48
 */
package framework.core.helpers
{
    import flash.display.Bitmap;

    import framework.core.tools.SourceManager;

    public class FramesGen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        /**
         * Получить кадры анимации
         * @param sourceBasis
         * @param framesCount
         * @param from
         * @return
         */
        public static function getFrames(sourceBasis:String, framesCount:int, from:int = 0):Vector.<Bitmap>
        {
            var frames:Vector.<Bitmap> = new Vector.<Bitmap>();
            var manager:SourceManager = SourceManager.instance;

            for (var i:int = from; i < framesCount; i++) {
                var source:String = sourceBasis + i.toString();
                frames.push(manager.getBitmap(source));
            }

            return frames;
        }


        /**
         * Получить зацикленную анимацию. Сначала проигрывается вперед, потом в обратном порядке
         * @param sourceBasis
         * @param framesCount
         * @param from
         * @return
         */
        public static function getLoopFrames(sourceBasis:String, framesCount:int, from:int = 0):Vector.<Bitmap>
        {
            var frames:Vector.<Bitmap> = getFrames(sourceBasis, framesCount, from);
            var count:int = frames.length;

            if (frames.length > 1) {
                for (var i:int = count - 2; i > 1; i--) {
                    var bitmap:Bitmap = frames[i];
                    frames.push(bitmap);
                }
            }

            return frames;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
