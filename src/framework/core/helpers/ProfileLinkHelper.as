package framework.core.helpers
{
    import framework.core.socnet.SocNet;

    public class ProfileLinkHelper
    {
        public static function getLink(socNetId:String):String
        {
            var linkTpl:String;
            switch (SocNet.instance().socNetName) {
                case SocNet.VK_COM:
                    linkTpl = '//vk.com/id{SOC_NET_ID}';
                    break;
                case SocNet.FACEBOOK_COM:
                    linkTpl = '//www.facebook.com/profile.php?id={SOC_NET_ID}';
                    break;
                case SocNet.ODNOKLASSNIKI_RU:
                    linkTpl = '//www.odnoklassniki.ru/profile/{SOC_NET_ID}';
                    break;
                default:
                    linkTpl = '#';
            }

            return linkTpl.replace('{SOC_NET_ID}', socNetId);
        }
    }

}
