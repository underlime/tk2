package framework.core.helpers
{
    public class StringHelper
    {
        private static var _trimRegExp:RegExp = new RegExp("(^[\\s\\r\\n]+)|([\\s\\r\\n]+)$");
        private static var _removeHtmlRegExp:RegExp = new RegExp("<[^<]+?>", "gi");
        private static var _multipleSpacesRegExp:RegExp = new RegExp("\\s+");

        public static function trim(str:String):String
        {
            return str.replace(_trimRegExp, "");
        }

        public static function stripTags(str:String):String
        {
            var res:String = str.replace(_removeHtmlRegExp, "");
            res = trim(res);
            res = res.replace(_multipleSpacesRegExp, " ");
            return res;
        }

        public static function chooseRuPlural(amount:int, variants:Array):String
        {
            if (variants.length < 3)
                throw new RangeError("Length of variants must be >= 3");

            amount = Math.abs(amount);
            var mod10:int = amount % 10;
            var mod100:int = amount % 100;

            var variant:int = 2;
            if (mod10 == 1 && mod100 != 11)
                variant = 0;
            else
                if (mod10 >= 2 && mod10 <= 4 && !(mod100 > 10 && mod100 < 20))
                    variant = 1;

            return variants[variant];
        }
    }

}
