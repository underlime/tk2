/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.02.13
 * Time: 11:25
 */
package framework.core.helpers
{
    public class ObjectHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        /**
         * преобразует XML в объект
         * @param xml
         * @return
         */
        public static function xmlToObject(xml:XML):Object
        {
            var data:Object = {};
            for each (var node:XML in xml.*) {
                if (node.children().length() > 1)
                    data[node.name()] = ObjectHelper.xmlToObject(node); else
                    data[node.name()] = node.toString();
            }

            return data;
        }

        /**
         * Получить значение по ключу
         * @param obj исходный объект
         * @param key ключ
         * @param instead значение по умолчанию, если в объекте нет указанного ключа
         * @return
         */
        public static function getData(obj:Object, key:String, instead:*):*
        {
            if (obj[key])
                return obj[key];
            else
                return instead;
        }

        /**
         * Фильтрует объект по указанным ключам. Возвращается новый объект
         * @param obj исходный объект
         * @param keys список ключей для нового объекта
         * @return
         */
        public static function filterObject(obj:Object, keys:Array):Object
        {
            var result:Object = {};
            for each(var key:String in keys) {
                if (obj[key])
                    result[key] = obj[key];
            }

            return result;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
