/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.10.12
 */
package framework.core.tools
{

    import by.blooddy.crypto.Base64;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.MovieClip;
    import flash.system.ApplicationDomain;
    import flash.utils.ByteArray;

    import mx.graphics.codec.PNGEncoder;

    public class SourceManager
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static var _instance:SourceManager = null;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SourceManager()
        {
            if (SourceManager._instance) {
                throw new Error("Use instance property");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function has(definition:String):Boolean
        {
            return _appDomain.hasDefinition(definition) || definition in _bitmapPool || definition in _bitmapDataPool;
        }

        /**
         * Создать MovieClip из Библиотеки
         * @param   definition - название класса
         * @param   save - сохранить ссылку на MC в пуле объектов
         * @return ссылка на MovieCLip
         */
        public function getMovieClip(definition:String, save:Boolean = true):MovieClip
        {
            if (!_appDomain.hasDefinition(definition)) {
                throw new Error("There is no same definition in this applicationDomain: " + definition);
            }

            if (save && _pool[definition]) {
                return _pool[definition] as MovieClip;
            }

            var tmpClass:Class = _appDomain.getDefinition(definition) as Class;
            var mc:MovieClip = new tmpClass();

            if (save) {
                _pool[definition] = mc;
            }
            return mc;
        }

        /**
         * Добавить битмапу в пул
         */
        public function addBitmap(bitmap:Bitmap, name:String):void
        {
            if (!_bitmapDataPool[name]) {
                _bitmapDataPool[name] = bitmap.bitmapData;
            }
        }

        /**
         * Получит битмапу по имени
         * @param name
         * @param allowSmoothing
         * @return
         */
        public function getBitmap(name:String, allowSmoothing:Boolean = false):Bitmap
        {
            var data:BitmapData = this.getBitmapData(name);
            var bitmap:Bitmap = new Bitmap(data, "auto", allowSmoothing);
            _bitmapPool[name] = bitmap;
            return bitmap;
        }

        /**
         * Получить битмапдату
         * @param   definition
         * @return
         */
        public function getBitmapData(definition:String):BitmapData
        {
            if (!_bitmapDataPool[definition]) {
                if (_appDomain.hasDefinition(definition)) {
                    var TempClass:Class = _appDomain.getDefinition(definition) as Class;
                    var data:BitmapData = new TempClass();
                    _bitmapDataPool[definition] = data;
                    return data;
                } else {
                    throw new Error("There is no same definition in this applicationDomain: " + definition);
                }
            }
            return _bitmapDataPool[definition];
        }

        /**
         * Получить из библиотеки изображение, закодированное как base64
         * @param definition
         * @return
         */
        public function getBase64String(definition:String):String
        {
            if (_base64StringsPool[definition])
                return _base64StringsPool[definition];

            var bitmapData:BitmapData = this.getBitmapData(definition);
            var pngData:ByteArray = (new PNGEncoder()).encode(bitmapData);
            var base64Data:String = Base64.encode(pngData);
            var base64String:String = 'data:image/png; base64,' + base64Data;

            _base64StringsPool[definition] = base64String;
            return base64String;
        }

        /**
         * Определяем, сохранено ли в кэше искомое изображение
         * @param   name
         * @return
         */
        public function bitmapContains(name:String):Boolean
        {
            return Boolean(_bitmapPool[name]);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appDomain:ApplicationDomain = new ApplicationDomain(ApplicationDomain.currentDomain);
        private var _pool:Array = [];
        private var _bitmapPool:Array = [];
        private var _bitmapDataPool:Array = [];
        private var _base64StringsPool:Array = [];

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public static function get instance():SourceManager
        {
            if (SourceManager._instance == null) {
                SourceManager._instance = new SourceManager();
            }
            return SourceManager._instance;
        }

        public function get appDomain():ApplicationDomain
        {
            return _appDomain;
        }
    }
}
