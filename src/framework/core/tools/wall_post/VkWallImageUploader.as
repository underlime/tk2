package framework.core.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.system.Capabilities;

    import framework.core.events.GameEvent;
    import framework.core.socnet.VkCom;
    import framework.core.tools.AbstractImageUploader;

    public class VkWallImageUploader extends AbstractImageUploader
    {
        private var _vkCom:VkCom;

        public function VkWallImageUploader(image:Bitmap)
        {
            super(image);
            _vkCom = VkCom(_socNet);
        }

        override public function upload():void
        {
            if (Capabilities.playerType == "StandAlone") {
                _saveToDisk();
            }
            else {
                _checkAlbum();
            }
        }

        private function _checkAlbum():void
        {
            if (WallPostTool.albumId) {
                _albumId = WallPostTool.albumId;
                _vkCom.getAlbumsListInfo(_onAlbumsInfo);
            }
            else {
                _createAlbum();
            }
        }

        private function _onAlbumsInfo(data:Object):void
        {
            var albumsInfo:Array = data['albums_info'] as Array;
            var idsList:Array = [];
            for (var i:int = 0; i < albumsInfo.length; ++i) {
                idsList.push(albumsInfo[i]['aid']);
            }

            if (idsList.indexOf(parseFloat(_albumId)) != -1) {
                _vkCom.getUploadPhotoParams(_albumId, _onParamsSuccess, _onParamsError);
            }
            else {
                _createAlbum();
            }
        }

        private function _onParamsSuccess(data:Object):void
        {
            var serverInfo:Object = data['server_info'];
            _uploadPhotoToServer(serverInfo['server_url'], serverInfo['post_params'], serverInfo['file_name']);
        }

        private function _onParamsError(...args):void
        {
            var message:String = 'Unable to get upload old.server params';
            _dispatchErrorEvent(message);
        }

        private function _createAlbum():void
        {
            var privacy:int = 0;
            _vkCom.createAlbum(WallPostTool.gameName, privacy, _onAlbumCreate);
        }

        private function _onAlbumCreate(data:Object):void
        {
            _albumId = data['album_info']['aid'];
            WallPostTool.albumId = _albumId;

            var createEvent:GameEvent = new GameEvent(GameEvent.ALBUM_CREATED);
            createEvent.data['photo_album_id'] = _albumId;
            dispatchEvent(createEvent);

            _vkCom.getUploadPhotoParams(_albumId, _onParamsSuccess, _onParamsError);
        }
    }
}
