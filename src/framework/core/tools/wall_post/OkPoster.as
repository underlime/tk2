package framework.core.tools.wall_post
{
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.system.Capabilities;

    import framework.core.events.GameEvent;
    import framework.core.socnet.OdnoklassnikiRu;

    import org.casalib.util.ObjectUtil;

    public class OkPoster extends AbstractWallPoster
    {
        private var _uploader:OkWallImageUploader;
        private var _okRu:OdnoklassnikiRu;

        public function OkPoster()
        {
            super();
            _okRu = OdnoklassnikiRu(_socNet);
        }

        override public function destroy():void
        {
            if (_uploader) {
                _uploader.destroy();
                _uploader = null;
            }

            super.destroy();
        }

        override public function addPost():void
        {
            var photosPermission:Boolean = _okRu.getPhotosPermission();
            if (_bitmapImage && photosPermission) {
                _uploader = new OkWallImageUploader(_bitmapImage);
                _uploader.addEventListener(Event.COMPLETE, _onImageUploaded);
                _uploader.addEventListener(ErrorEvent.ERROR, _redispatchEvent);
                _uploader.addEventListener(GameEvent.ALBUM_CREATED, _redispatchEvent);
                _uploader.upload();
            }
            else if (_pictureSocNetId) {
                _addWallPost(_pictureSocNetId);
            }
            else {
                _addWallPost();
            }
        }

        private function _onImageUploaded(e:Event):void
        {
            if (Capabilities.playerType == 'StandAlone') {
                var event:Event = new Event(Event.COMPLETE);
                dispatchEvent(event);
            }
            else {
                try {
                    var responseData:Object = JSON.parse(_uploader.loader.data.toString());
                }
                catch (e:SyntaxError) {
                    var errorEvent:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
                    errorEvent.text = 'Invalid server data';
                    dispatchEvent(errorEvent);
                }

                var photoId:String = ObjectUtil.getKeys(responseData['photos'])[0];
                var token:String = responseData['photos'][photoId]['token'];

                _okRu.savePhoto(photoId, token, _postText, _onPhotoSaved, _onSaveError);
            }
        }

        private function _onPhotoSaved(data:Object):void
        {
            var photoSrc:String = data['photo_info']['pic640x480'];
            _addWallPost(photoSrc);
        }

        private function _onSaveError(...args):void
        {
            var message:String = 'Unable to save a photo';
            var event:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
            event.text = message;
            dispatchEvent(event);
        }

        private function _addWallPost(photoSrc:String = null):void
        {
            _okRu.postPhotoToWall(photoSrc, _postText, _postText);
            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }
    }
}
