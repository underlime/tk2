package framework.core.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.system.Capabilities;

    import framework.core.events.GameEvent;
    import framework.core.socnet.OdnoklassnikiRu;
    import framework.core.tools.AbstractImageUploader;

    public class OkWallImageUploader extends AbstractImageUploader
    {
        private var _okRu:OdnoklassnikiRu;

        public function OkWallImageUploader(image:Bitmap)
        {
            super(image);
            _okRu = OdnoklassnikiRu(_socNet);
        }

        override public function upload():void
        {
            _fillBackground(0xFFFFFFFF);
            if (Capabilities.playerType == 'StandAlone') {
                _saveToDisk();
            }
            else {
                _checkAlbum();
            }
        }

        private function _checkAlbum():void
        {
            if (WallPostTool.albumId) {
                _albumId = WallPostTool.albumId;
                _okRu.getAlbumsListInfo(_onAlbumsInfo);
            }
            else {
                _createAlbum();
            }
        }

        private function _onAlbumsInfo(data:Object):void
        {
            var albumsInfo:Array = data['albums_info'] as Array;
            var idsList:Array = [];
            for (var i:int = 0; i < albumsInfo.length; ++i) {
                idsList.push(parseFloat(albumsInfo[i]['aid']));
            }

            if (idsList.indexOf(_albumId) != -1) {
                _okRu.getUploadPhotoParams(_albumId, _onParamsSuccess, _onParamsError);
            }
            else {
                _createAlbum();
            }
        }

        private function _createAlbum():void
        {
            var albumType:String = 'public';
            _okRu.createAlbum(WallPostTool.gameName, albumType, _onAlbumCreate);
        }

        private function _onAlbumCreate(data:Object):void
        {
            _albumId = data['photo_album_id'];

            var createEvent:GameEvent = new GameEvent(GameEvent.ALBUM_CREATED);
            createEvent.data['photo_album_id'] = _albumId;
            dispatchEvent(createEvent);

            _okRu.getUploadPhotoParams(_albumId, _onParamsSuccess, _onParamsError);
        }

        private function _onParamsSuccess(data:Object):void
        {
            var serverInfo:Object = data['server_info'];
            _uploadPhotoToServer(serverInfo['server_url'], serverInfo['post_params'], serverInfo['file_name']);
        }

        //noinspection JSUnusedLocalSymbols
        private function _onParamsError(...args):void
        {
            var message:String = 'Unable to get upload server params';
            _dispatchErrorEvent(message);
        }
    }
}
