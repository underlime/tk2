package framework.core.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.core.events.GameEvent;
    import framework.core.socnet.SocNet;

    import org.casalib.events.RemovableEventDispatcher;

    public class WallPostTool extends RemovableEventDispatcher
    {
        private static var _gameName:String = "Game";
        private static var _applicationLink:String = "";
        private static var _albumId:String = "0";

        private var _poster:AbstractWallPoster;
        private var _postText:String = '';
        private var _bitmapImage:Bitmap;
        private var _pictureSocNetId:String;

        public function WallPostTool()
        {
            switch (SocNet.instance().socNetName) {
                case SocNet.VK_COM:
                case SocNet.MOCK:
                    _poster = new VkPoster();
                    break;
                case SocNet.ODNOKLASSNIKI_RU:
                    _poster = new OkPoster();
                    break;
                default:
                    throw new Error('Unsupported social network');
            }

            _poster.addEventListener(Event.COMPLETE, _redispatchEvent);
            _poster.addEventListener(ErrorEvent.ERROR, _redispatchEvent);
            _poster.addEventListener(GameEvent.ALBUM_CREATED, _redispatchEvent);
        }

        private function _redispatchEvent(e:Event):void
        {
            dispatchEvent(e);
        }

        override public function destroy():void
        {
            if (_poster) {
                _poster.destroy();
                _poster = null;
            }

            super.destroy();
        }

        public function addPost():void
        {
            _poster.postText = _postText;
            _poster.bitmapImage = _bitmapImage;
            _poster.pictureSocNetId = _pictureSocNetId;
            _poster.addPost();
        }

        public function set postText(value:String):void
        {
            _postText = value;
        }

        public function set bitmapImage(image:Bitmap):void
        {
            _bitmapImage = image;
        }

        public function set pictureSocNetId(value:String):void
        {
            _pictureSocNetId = value;
        }

        public static function get applicationLink():String
        {
            return _applicationLink;
        }

        public static function set applicationLink(value:String):void
        {
            _applicationLink = value;
        }

        public static function get albumId():String
        {
            return _albumId;
        }

        public static function set albumId(value:String):void
        {
            _albumId = value;
        }

        public static function get gameName():String
        {
            return _gameName;
        }

        public static function set gameName(value:String):void
        {
            _gameName = value;
        }
    }
}
