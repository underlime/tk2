package framework.core.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.core.socnet.SocNet;

    import org.casalib.events.RemovableEventDispatcher;

    public class AbstractWallPoster extends RemovableEventDispatcher
    {
        protected var _postText:String;
        protected var _bitmapImage:Bitmap;
        protected var _pictureSocNetId:String;
        protected var _socNet:SocNet = SocNet.instance();

        public function addPost():void
        {
            throw new Error('Function is not implemented');
        }

        public function set postText(value:String):void
        {
            _postText = value;
        }

        public function set bitmapImage(image:Bitmap):void
        {
            _bitmapImage = image;
        }

        public function set pictureSocNetId(value:String):void
        {
            _pictureSocNetId = value;
        }

        protected function _redispatchEvent(e:Event):void
        {
            dispatchEvent(e);
        }
    }
}
