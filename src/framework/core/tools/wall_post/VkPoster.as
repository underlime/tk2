package framework.core.tools.wall_post
{
    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.core.events.GameEvent;
    import framework.core.socnet.VkCom;

    public class VkPoster extends AbstractWallPoster
    {
        private var _uploader:VkWallImageUploader;

        public function VkPoster()
        {
        }

        override public function destroy():void
        {
            if (_uploader) {
                _uploader.destroy();
                _uploader = null;
            }

            super.destroy();
        }

        override public function addPost():void
        {
            if (_bitmapImage) {
                _uploader = new VkWallImageUploader(_bitmapImage);
                _uploader.addEventListener(Event.COMPLETE, _onImageUploaded);
                _uploader.addEventListener(ErrorEvent.ERROR, _redispatchEvent);
                _uploader.addEventListener(GameEvent.ALBUM_CREATED, _redispatchEvent);
                _uploader.upload();
            }
            else if (_pictureSocNetId) {
                _addWallPost(_pictureSocNetId);
            }
            else {
                _addWallPost();
            }
        }

        private function _onImageUploaded(e:Event):void
        {
            if (_uploader.loader) {
                var serverResponse:Object = _uploader.loader.data;
                VkCom(_socNet).savePhoto(WallPostTool.albumId, serverResponse, _onPhotoSaved, _onSaveError);
            }
            else {
                _addWallPost();
            }
        }

        private function _onPhotoSaved(data:Object):void
        {
            var photoId:String = data['photo_info']['photo_id'];
            _addWallPost(photoId);
        }

        private function _onSaveError(...args):void
        {
            var message:String = 'Unable to save a photo';
            var event:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
            event.text = message;
            dispatchEvent(event);
        }

        private function _addWallPost(photoId:String = null):void
        {
            VkCom(_socNet).postPhotoToWall(photoId, _postText, WallPostTool.applicationLink);

            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }
    }
}
