package framework.core.tools
{
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.events.SecurityErrorEvent;
    import flash.geom.Rectangle;
    import flash.net.FileReference;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.net.URLRequestHeader;
    import flash.net.URLRequestMethod;
    import flash.utils.ByteArray;

    import framework.core.helpers.UploadPostHelper;
    import framework.core.socnet.SocNet;

    import mx.graphics.codec.PNGEncoder;

    import org.casalib.events.RemovableEventDispatcher;

    public class AbstractImageUploader extends RemovableEventDispatcher
    {
        protected var _image:Bitmap;
        protected var _loader:URLLoader;
        protected var _albumId:String;
        protected var _socNet:SocNet = SocNet.instance();

        public function AbstractImageUploader(image:Bitmap)
        {
            _image = image;
        }

        override public function destroy():void
        {
            if (_loader) {
                _loader.removeEventListener(ProgressEvent.PROGRESS, _redispatchEvent);
                _loader.removeEventListener(IOErrorEvent.IO_ERROR, _onUploadIoError);
                _loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, _onUploadSecError);
                _loader.removeEventListener(Event.COMPLETE, _redispatchEvent);
                _loader = null;
            }

            super.destroy();
        }

        public function upload():void
        {
            throw new Error('Function is not implemented');
        }

        protected function _uploadPhotoToServer(serverUrl:String, postParams:Object, fileName:String):void
        {
            var pngEncoder:PNGEncoder = new PNGEncoder();
            var fileData:ByteArray = pngEncoder.encode(_image.bitmapData);

            var request:URLRequest = new URLRequest(serverUrl);
            request.requestHeaders.push(new URLRequestHeader('Cache-Control', 'no-cache'));
            request.requestHeaders.push(new URLRequestHeader('Content-Type', 'multipart/form-data; boundary=' + UploadPostHelper.getBoundary()));
            request.method = URLRequestMethod.POST;
            request.data = UploadPostHelper.getPostData('image.png', fileData, postParams, fileName);

            _loader = new URLLoader();
            _loader.addEventListener(ProgressEvent.PROGRESS, _redispatchEvent);
            _loader.addEventListener(IOErrorEvent.IO_ERROR, _onUploadIoError);
            _loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _onUploadSecError);
            _loader.addEventListener(Event.COMPLETE, _redispatchEvent);
            _loader.load(request);
        }

        protected function _redispatchEvent(e:Event):void
        {
            dispatchEvent(e);
        }

        protected function _onUploadIoError(e:IOErrorEvent):void
        {
            _dispatchErrorEvent(e.text);
        }

        protected function _dispatchErrorEvent(text:String):void
        {
            var event:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
            event.text = text;
            dispatchEvent(event);
        }

        protected function _onUploadSecError(e:SecurityErrorEvent):void
        {
            _dispatchErrorEvent(e.text);
        }

        protected function _fillBackground(aRgbColor:int):void
        {
            var workData:BitmapData = new BitmapData(_image.width, _image.height);
            workData.fillRect(new Rectangle(0, 0, _image.width, _image.height), aRgbColor);
            workData.draw(_image);
            _image = new Bitmap(workData);
        }

        public function get loader():URLLoader
        {
            return _loader;
        }

        protected function _saveToDisk():void
        {
            var pngEncoder:PNGEncoder = new PNGEncoder();
            var file:FileReference = new FileReference();
            file.save(pngEncoder.encode(_image.bitmapData), 'screenshot.png');

            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }
    }
}
