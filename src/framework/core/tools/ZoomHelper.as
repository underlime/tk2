/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.10.13
 * Time: 16:31
 */
package framework.core.tools
{
    import com.greensock.TweenLite;

    import flash.display.DisplayObject;

    public class ZoomHelper
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function hide(object:DisplayObject, onComplete:Function = null, seconds:Number = 0.1, delay:Number = 0):void
        {
            var w:Number = object.width;
            var h:Number = object.height;

            var x:Number = object.x;
            var y:Number = object.y;

            var params:Object = {
                "width": 0,
                "height": 0,
                "x": (x + w / 2),
                "y": (y + h / 2),
                "onComplete": _onComplete,
                "delay": delay
            };

            TweenLite.to(object, seconds, params);

            function _onComplete():void
            {
                if (onComplete is Function)
                    onComplete.call();

                _setupDefaults(object, x, y, w, h);
                TweenLite.killTweensOf(object);
            }
        }

        public static function show(object:DisplayObject, onComplete:Function = null, seconds:Number = 0.1, delay:Number = 0):void
        {
            var w:Number = object.width;
            var h:Number = object.height;

            var x:Number = object.x;
            var y:Number = object.y;

            object.x = (x + w / 2);
            object.y = (y + h / 2);

            object.width = 1;
            object.height = 1;

            var params:Object = {
                "width": w,
                "height": h,
                "x": x,
                "y": y,
                "onComplete": _onComplete,
                "delay": delay
            };

            TweenLite.to(object, seconds, params);

            function _onComplete():void
            {
                if (onComplete is Function)
                    onComplete.call();

                _setupDefaults(object, x, y, w, h);
                TweenLite.killTweensOf(object);
            }
        }

        private static function _setupDefaults(object:DisplayObject, x:Number, y:Number, width:Number, height:Number):void
        {
            object.x = x;
            object.y = y;
            object.width = width;
            object.height = height;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
