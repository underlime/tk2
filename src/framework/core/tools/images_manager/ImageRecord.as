package framework.core.tools.images_manager
{
    import flash.display.Bitmap;

    import framework.core.tools.ImagesManager;

    public class ImageRecord
    {
        public var url:String;
        public var status:int = ImagesManager.STATUS_NOT_LOADED;
        public var bitmap:Bitmap;

        private var _completeFunctionsList:Vector.<Function> = new Vector.<Function>();

        public function ImageRecord()
        {
        }

        public function addCompleteCallback(callback:Function):void
        {
            if (bitmap == null)
                _completeFunctionsList.push(callback);
            else
                callback(bitmap);
        }

        public function executeCompleteFunctions():void
        {
            for (var i:int = 0; i < _completeFunctionsList.length; ++i)
                _completeFunctionsList[i](bitmap);

            _completeFunctionsList = new Vector.<Function>();
        }
    }
}
