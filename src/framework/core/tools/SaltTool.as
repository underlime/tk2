package framework.core.tools
{
    public class SaltTool
    {
        public function SaltTool()
        {
        }

        public static function decode(encryptedSalt:Array, z:int):String
        {
            var salt:String = '';
            for each (var x:int in encryptedSalt)
                salt += String.fromCharCode(z ^ x);
            return salt;
        }
    }
}
