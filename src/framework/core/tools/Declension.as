/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 27.09.13
 * Time: 13:39
 */
package framework.core.tools
{
    public class Declension
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public static function getVariant(count:int, wordForms:Array):String
        {
            assertForms(wordForms);

            var currencyName:String = wordForms[0];
            var mod10:Number = count % 10;
            var mod100:Number = count % 100;
            var not11:Boolean = (mod100 <= 10 || mod100 >= 20);

            if (mod10 >= 2 && mod10 <= 4 && not11)
                currencyName = wordForms[1];
            else
                if (!not11 || mod10 == 0 || (not11 && mod10 > 4))
                    currencyName = wordForms[2];

            return currencyName;
        }

        private static function assertForms(wordForms:Array):void
        {
            if (!(wordForms is Array)) {
                throw new Error('Формы слов должны передаваться в виде массива');
            }
            if (wordForms.length != 3) {
                throw new Error('Нужно передавать 3 формы склонения');
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
