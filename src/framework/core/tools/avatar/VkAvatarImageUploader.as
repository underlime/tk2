package framework.core.tools.avatar
{
    import flash.display.Bitmap;

    import framework.core.socnet.SocNet;
    import framework.core.socnet.VkCom;
    import framework.core.tools.AbstractImageUploader;

    public class VkAvatarImageUploader extends AbstractImageUploader
    {
        public function VkAvatarImageUploader(image:Bitmap)
        {
            super(image);
        }

        override public function upload():void
        {
            VkCom(SocNet.instance()).getUploadProfilePhotoParams(_onParamsSuccess, _onParamsError);
        }

        private function _onParamsSuccess(data:Object):void
        {
            var serverInfo:Object = data['server_info'];
            _uploadPhotoToServer(serverInfo['server_url'], serverInfo['post_params'], serverInfo['file_name']);
        }

        private function _onParamsError(...args):void
        {
            var message:String = 'Unable to get upload old.server params';
            _dispatchErrorEvent(message);
        }
    }
}
