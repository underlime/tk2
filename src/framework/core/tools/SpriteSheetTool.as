/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.09.13
 * Time: 10:14
 */
package framework.core.tools
{
    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.core.display.animation.Animation;
    import framework.core.display.animation.TimeAnimation;
    import framework.core.helpers.ImagesHelper;

    public class SpriteSheetTool
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getFrames(source:Bitmap, framesCols:int, framesRows:int, framesCount:int):Vector.<Bitmap>
        {
            var frames:Vector.<Bitmap> = new Vector.<Bitmap>();
            var frameWidth:int = int(source.width / framesCols);
            var frameHeight:int = int(source.height / framesRows);

            var cx:int = 0;
            var cy:int = 0;
            var count:int = 0;

            for (var i:int = 0; i < framesCount; i++) {

                if (count >= framesCols) {
                    cy += frameHeight;
                    cx = 0;
                    count = 0;
                }
                frames.push(ImagesHelper.getParticular(source, new Rectangle(cx, cy, frameWidth, frameHeight)));
                cx += frameWidth;
                count++;
            }
            return frames;
        }

        public static function getAnimation(source:Bitmap, framesCols:int, framesRows:int, framesCount:int):Animation
        {
            var frames:Vector.<Bitmap> = getFrames(source, framesCols, framesRows, framesCount);
            return new Animation(frames);
        }

        public static function getTimeAnimation(source:Bitmap, framesCols:int, framesRows:int, framesCount:int, fps:int = 30):TimeAnimation
        {
            var frames:Vector.<Bitmap> = getFrames(source, framesCols, framesRows, framesCount);
            return new TimeAnimation(frames, fps);
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
