package framework.core.interfaces
{

    import flash.display.DisplayObjectContainer;

    import framework.core.error.ApplicationError;
    import framework.core.socnet.SocNet;
    import framework.core.underquery.ServerApi;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public interface IBaseController extends IController
    {
        /**
         * графический контейнер
         */
        function get container():DisplayObjectContainer;

        function get serverApi():ServerApi;

        function get socNet():SocNet;

        function appError(error:ApplicationError):void;
    }

}