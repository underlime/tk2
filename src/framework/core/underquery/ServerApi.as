/**
 * Метод makeRequest реализует отправку запросов к API данных.
 * Конкретные методы API реализуются для каждого приложения
 * в отдельном классе.
 *
 * При вызове метода возможны 2 события:
 * - успех,
 * - ошибка.
 *
 * По умолчанию успех инициирует импорт данных в модели,
 * ошибка вызывает метод BaseController.appError.
 *
 * Это поведение можно переопределить, если передавать методам колбэки
 * (необязательные параметры). Колбэки должны возвращать тип Boolean,
 * при возврате true после колбэка выполняется действие по умолчанию,
 * при false или если колбэк имеет тип void - действие по умолчанию
 * не выполняется.
 */
package framework.core.underquery
{
    import framework.core.error.ApplicationError;
    import framework.core.struct.controller.BaseController;
    import framework.core.struct.service.Service;

    public class ServerApi extends Service
    {
        protected var _baseController:BaseController;
        protected var _ApiRequestClass:Class = ApiRequest;

        protected var _queryID:int = 0;

        public function ServerApi(baseController:BaseController)
        {
            _baseController = baseController;
        }

        public function makeRequest(method:String, methodArguments:Object = null, successCallback:Function = null, errorCallback:Function = null):int
        {
            if (methodArguments == null)
                methodArguments = {};

            var requestUri:String = _createRequestUri(method);
            var token:String = _createToken(requestUri);
            var fullUrl:String = _createFullUrl(requestUri);
            var apiRequest:ApiRequest = new _ApiRequestClass(fullUrl);

            var serverCallParams:Object = _baseController.socNet.serverCallParams;
            for (var key:String in serverCallParams) if (serverCallParams.hasOwnProperty(key))
                methodArguments[key] = serverCallParams[key];

            _queryID++;

            apiRequest.methodArguments = methodArguments;
            apiRequest.token = token;
            apiRequest.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiRequestSuccess);
            apiRequest.addEventListener(ApiRequestEvent.ERROR_EVENT, _onApiRequestError);
            apiRequest.successCallback = successCallback;
            apiRequest.errorCallback = errorCallback;
            apiRequest.queryID = _queryID;
            apiRequest.send();

            return _queryID;
        }

        private function _onApiRequestSuccess(e:ApiRequestEvent):void
        {
            var needImport:Boolean = true;
            if (e.successCallback != null)
                needImport = e.successCallback(e);

            if (needImport)
                _baseController.dataBase.invokeDataImport(e.data);

            var event:ApiRequestEvent = new ApiRequestEvent(ApiRequestEvent.SUCCESS_EVENT);
            event.queryID = e.queryID;
            super.dispatchEvent(event);

            e.target.destroy();
        }

        private function _onApiRequestError(e:ApiRequestEvent):void
        {
            var needAppError:Boolean = true;
            if (e.errorCallback != null)
                needAppError = e.errorCallback(e);

            if (needAppError) {
                var error:ApplicationError = new ApplicationError();
                error.type = ApplicationError.SEVER_ERROR;
                if (e.data) {
                    error.code = e.data['code'];
                    error.message = e.data['message'];
                } else {
                    error.code = e.httpStatus;
                    error.message = "HTTP error (" + error.code + ")";
                }

                if (e.httpStatus == 503) {
                    error.type = ApplicationError.APPLICATION_TEMPORARILY_UNAVAIBLE;
                    error.message = 'Application temporarily unavailable. Please try later';
                }

                _baseController.appError(error);
            }

            e.target.destroy();
        }

        /**
         * Создать базовый URL метода. Зависит от конфига конкретного приложения,
         * который должен извлекаться в дочерних классах
         * @param method
         * @param queryString
         * @return
         */
        protected function _createRequestUri(method:String, queryString:String = ''):String
        {
            throw new Error('Function is not implemented');
        }

        /**
         * Создать полный URL метода. Зависит от конфига конкретного приложения,
         * который должен извлекаться в дочерних классах
         * @abstract
         * @return
         */
        protected function _createFullUrl(requestUri:String):String
        {
            throw new Error('Function is not implemented');
        }

        private function _createToken(requestUri:String):String
        {
            return _baseController.socNet.createToken(requestUri, _getTokenSalt());
        }

        /**
         * Извлечь соль для токена. Зависит от реализации конкретного приложения
         * @abstract
         * @return
         */
        protected function _getTokenSalt():String
        {
            throw new Error('Function is not implemented');
        }
    }
}
