package framework.core.underquery.data_example
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class Test2 extends Data
    {
        private var _id:IntegerField = new IntegerField();
        private var _soc_net_id:IntegerField = new IntegerField();
        private var _test1:CharField = new CharField();
        private var _test2:CharField = new CharField();

        public function Test2()
        {
            super('Test2');
        }

        override public function getModelDataKeys():Array
        {
            return ['user', 'test1', 'test2'];
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function set id(value:IntegerField):void
        {
            _id = value;
        }

        public function get soc_net_id():IntegerField
        {
            return _soc_net_id;
        }

        public function set soc_net_id(value:IntegerField):void
        {
            _soc_net_id = value;
        }

        public function get test1():CharField
        {
            return _test1;
        }

        public function set test1(value:CharField):void
        {
            _test1 = value;
        }

        public function get test2():CharField
        {
            return _test2;
        }

        public function set test2(value:CharField):void
        {
            _test2 = value;
        }
    }
}
