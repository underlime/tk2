package framework.core.underquery.data_example
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class Test1 extends Data
    {
        private var _id:IntegerField = new IntegerField();
        private var _test1:CharField = new CharField();

        public function Test1()
        {
            super('Test1');
        }

        override public function getModelDataKeys():Array
        {
            return ['user', 'test1'];
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function set id(value:IntegerField):void
        {
            _id = value;
        }

        public function get test1():CharField
        {
            return _test1;
        }

        public function set test1(value:CharField):void
        {
            _test1 = value;
        }
    }
}
