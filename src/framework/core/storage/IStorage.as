/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.09.13
 * Time: 21:37
 */
package framework.core.storage
{
    public interface IStorage
    {
        function addItem(key:String, value:String):void;

        function hasItem(key:String):Boolean;

        function getItem(key:String):String;

        function removeItem(key:String):void;

        function setItem(key:String, value:String):void;
    }
}
