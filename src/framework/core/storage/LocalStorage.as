/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.09.13
 * Time: 21:24
 */
package framework.core.storage
{
    import flash.events.Event;
    import flash.net.SharedObject;

    import framework.core.struct.service.Service;

    public class LocalStorage extends Service implements IStorage
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static var _instance:LocalStorage = null;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LocalStorage(storageNamespace:String)
        {
            super();
            if (LocalStorage._instance)
                throw new Error("User instance property");
            _namespace = storageNamespace;
            _setupStorage();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function setItem(key:String, value:String):void
        {
            if (!hasItem(key))
                addItem(key, value);
            else
                _storage.data[key] = value;

            super.dispatchEvent(new Event(Event.CHANGE));
        }

        public function addItem(key:String, value:String):void
        {
            if (!hasItem(key)) {
                _storage.data[key] = value;
            }
        }

        public function hasItem(key:String):Boolean
        {
            return getItem(key) != null;
        }

        public function getItem(key:String):String
        {
            if (_storage.data[key])
                return _storage.data[key];
            return null;
        }

        public function removeItem(key:String):void
        {
            if (hasItem(key)) {
                delete _storage.data[key];
                super.dispatchEvent(new Event(Event.CHANGE));
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _namespace:String;
        private var _storage:SharedObject;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupStorage():void
        {
            _storage = SharedObject.getLocal(_namespace, '/');
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public static function getInstance(storageNamespace:String):LocalStorage
        {
            if (_instance === null)
                _instance = new LocalStorage(storageNamespace);
            return _instance;
        }
    }
}
