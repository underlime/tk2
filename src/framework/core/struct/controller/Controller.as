package framework.core.struct.controller
{

    import flash.display.DisplayObjectContainer;
    import flash.events.Event;

    import framework.core.events.GameEvent;
    import framework.core.interfaces.IBaseController;
    import framework.core.interfaces.IController;
    import framework.core.struct.data.DataBase;

    import org.casalib.events.RemovableEventDispatcher;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class Controller extends RemovableEventDispatcher implements IController
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        public function Controller(baseController:IBaseController)
        {
            super();
            _baseController = baseController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            super.dispatchEvent(new GameEvent(GameEvent.DESTROY));
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function execute():void
        {

        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _redispatchEvent(e:Event):void
        {
            dispatchEvent(e);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _baseController:IBaseController;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get baseController():IBaseController
        {
            return _baseController;
        }

        public function get dataBase():DataBase
        {
            return _baseController.dataBase;
        }

        public function get container():DisplayObjectContainer
        {
            return _baseController.container;
        }
    }
}