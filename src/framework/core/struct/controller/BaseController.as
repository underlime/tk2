package framework.core.struct.controller
{

    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;

    import framework.core.error.ApplicationError;
    import framework.core.interfaces.IBaseController;
    import framework.core.socnet.SocNet;
    import framework.core.struct.data.DataBase;
    import framework.core.struct.view.View;
    import framework.core.underquery.ServerApi;

    import org.casalib.core.IDestroyable;
    import org.casalib.events.RemovableEventDispatcher;
    import org.casalib.util.ArrayUtil;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BaseController extends RemovableEventDispatcher implements IBaseController
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseController(root:DisplayObjectContainer)
        {
            super();
            _root = root;
            execute();
        }


        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function execute():void
        {
            _container = new View();
            _root.addChild(_container);
        }

        public function appError(error:ApplicationError):void
        {

        }

        public function clear():void
        {
            for (var i:int = 0; i < container.numChildren; i++) {
                var object:DisplayObject = container.getChildAt(i);
                root.removeChild(object);
            }
        }

        /**
         * Очистить контейнер от визуальных объектов
         * @param arrException массив исключений - объекты, которые будут игнорироваться
         */
        public function clearContents(arrException:Array = null):void
        {
            for (var i:int = 0; i < container.numChildren; i++) {
                var object:DisplayObject = container.getChildAt(i);
                if (arrException) {
                    if (ArrayUtil.contains(arrException, object) == 0) {
                        if (object is IDestroyable) {
                            (object as IDestroyable).destroy();
                        }
                        else {
                            container.removeChild(object);
                        }
                    }
                } else {
                    container.removeChild(object);
                }
            }
        }

        /**
         * Удалить в контейнере все визуальные объекты заданного типа
         * @param ClearedClass
         */
        public function clearContainerByClass(ClearedClass:Class):void
        {
            for (var i:int = 0; i < container.numChildren; i++) {
                var object:DisplayObject = container.getChildAt(i);
                if (object is ClearedClass) {
                    if (object is IDestroyable) {
                        (object as IDestroyable).destroy();
                    }
                    else {
                        container.removeChild(object);
                    }
                }
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _dataBase:DataBase;
        protected var _serverApi:ServerApi;
        protected var _socNet:SocNet;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _container:View;
        private var _root:DisplayObjectContainer;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get container():DisplayObjectContainer
        {
            return _container;
        }

        public function get baseController():IBaseController
        {
            return this;
        }

        public function get dataBase():DataBase
        {
            return _dataBase;
        }

        public function get serverApi():ServerApi
        {
            return _serverApi;
        }

        public function get socNet():SocNet
        {
            return _socNet;
        }

        public function get root():DisplayObjectContainer
        {
            return _root;
        }
    }
}