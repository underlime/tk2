package framework.core.struct.view
{

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.events.Event;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    import framework.core.tools.SourceManager;

    import org.casalib.display.CasaSprite;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class View extends CasaSprite
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function View()
        {
            super();
            addEventListener(Event.ADDED_TO_STAGE, _init);
            setup();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        /**
         * Функция отключает интерактивные элементы для вида.
         *
         */
        public function $disableInteractive():void
        {
            this.mouseChildren = false;
            this.mouseEnabled = false;
            this.tabChildren = false;
            this.tabEnabled = false;
        }

        /**
         * Отладочный метод для отрисовки реального размера объекта
         */
        public function $drawBackground(alpha:Number = .9):void
        {
            graphics.beginFill(0x000000, alpha);
            graphics.drawRect(0, 0, width, height);
            graphics.endFill();
        }

        /**
         * Извлечь изображение представления как Bitmap
         * @param startX
         * @param startY
         * @param width
         * @param height
         * @return
         */
        public function getAsBitmap(startX:Number = 0, startY:Number = 0, width:Number = 0, height:Number = 0):Bitmap
        {
            var srcData:BitmapData = new BitmapData(this.width, this.height, true, 0);
            srcData.draw(this);

            var bitmapWidth:Number = (width) ? width : this.width;
            var bitmapHeight:Number = (height) ? height : this.height;

            var rect:Rectangle = new Rectangle(startX, startY, bitmapWidth, bitmapHeight);
            var point:Point = new Point(0, 0);

            var dstData:BitmapData = new BitmapData(bitmapWidth, bitmapHeight, true, 0);
            dstData.copyPixels(srcData, rect, point);

            var bitmap:Bitmap = new Bitmap(dstData);
            return bitmap;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        /**
         * Метод вызывается при добавлении объекта на сцену
         */
        protected function render():void
        {

        }

        /**
         * Функция вызывается при инициализации объекта
         */
        protected function setup():void
        {

        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _w:Number = 0;
        protected var _h:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _init(event:Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, _init);
            render();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get source():SourceManager
        {
            return SourceManager.instance
        }

        public function get w():Number
        {
            return _w;
        }

        public function set w(value:Number):void
        {
            _w = value;
        }

        public function get h():Number
        {
            return _h;
        }

        public function set h(value:Number):void
        {
            _h = value;
        }
    }
}