package framework.core.struct.data
{
    public class ModelsRegistry
    {
        private static var _dataBase:DataBase;

        public function ModelsRegistry()
        {
        }

        public static function get dataBase():DataBase
        {
            return _dataBase;
        }

        public static function set dataBase(value:DataBase):void
        {
            _dataBase = value;
        }

        public static function getModel(modelName:String):Data
        {
            if (_dataBase)
                return _dataBase.getChildByName(modelName);

            throw new Error('Database is not initialized');
        }
    }
}
