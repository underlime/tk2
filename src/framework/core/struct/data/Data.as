package framework.core.struct.data
{
    import flash.events.Event;

    import framework.core.struct.data.fields.IDataField;

    import mx.utils.ObjectUtil;

    import org.casalib.events.RemovableEventDispatcher;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class Data extends RemovableEventDispatcher
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Data(name:String)
        {
            super();
            this._name = name;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function toString():String
        {
            return ObjectUtil.toString(this);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        private var _name:String;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        /**
         * Метод, возвращающий ключи в ответах сервера,
         * на которые подписывается модель.
         * Обязателен к реализации.
         * В моделях, наследуемых от DataContainer, getModelDataKeys()
         * должен возвращать так же и результаты функции getChildrenDataKeys(),
         * чтобы при опросе моделей было понятно, куда спускать данные
         * @abstract
         * @return Массив строк ключей
         */
        public function getModelDataKeys():Array
        {
            throw new Error('getModelDataKeys not implemented in ' + _name);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _parent:DataContainer;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get name():String
        {
            return _name;
        }

        public function get parent():DataContainer
        {
            return _parent;
        }

        public function set parent(value:DataContainer):void
        {
            _parent = value;
        }

        public function importData(data:Object):void
        {
            var wasChanged:Boolean = false;
            for (var key:String in data) {
                if (data.hasOwnProperty(key) && this.hasOwnProperty(key)) {
                    if (this[key] is IDataField)
                        this[key].importValue(data[key]);
                    else if (this[key] is Data)
                        this[key].importData(data[key]);
                    wasChanged = true;
                }
            }

            if (wasChanged)
                dispatchEvent(new Event(Event.CHANGE));
        }
    }
}