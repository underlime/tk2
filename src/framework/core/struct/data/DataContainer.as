package framework.core.struct.data
{
    import org.casalib.util.ArrayUtil;
    import org.casalib.util.ObjectUtil;

    /**
     * Контейнер
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class DataContainer extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DataContainer(name:String)
        {
            super(name);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        /**
         * Деструктор модели
         */
        override public function destroy():void
        {
            for (var i:int = 0; i < _children.length; i++) {
                _children[i].destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        /**
         * Добавить ребеннк в модель
         * @param child вложенная модель
         * @return
         */
        public function addChild(child:Data):Data
        {
            if (_checkAddition(child)) {
                _children.push(child);
                child.parent = this;
            }
            return child;
        }

        /**
         * Удалить модель
         * @param child модель
         */
        public function removeChild(child:Data):void
        {
            if (this.contains(child.name)) {
                child.destroy();
                var deleteIndex:int = this.getChildIndex(child);
                _children.splice(deleteIndex, 1);
            }
        }


        /**
         * Прооврить наличие модели
         * @param childName
         * @return
         */
        public function contains(childName:String):Boolean
        {
            for (var i:int = 0; i < _children.length; i++) {
                if (childName == _children[i].name) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Получить индекс модели
         * @param child
         * @return
         */
        public function getChildIndex(child:Data):int
        {
            if (this.contains(child.name)) {
                return _children.indexOf(child);
            }
            else {
                throw new Error("Type was not found");
            }
        }

        /**
         * Получить ссылку на модель
         * @param childName
         * @return
         */
        public function getChildByName(childName:String):Data
        {
            if (this.contains(childName)) {
                for (var i:int = 0; i < _children.length; i++) {
                    if (childName == _children[i].name) {
                        return _children[i];
                    }
                }
            }
            throw new ArgumentError("Element not found");
        }

        /**
         * Получить список ключей модели и ее детей
         * @return
         */
        public function getChildrenDataKeys():Array
        {
            var keysList:Array = [];
            for (var i:int = 0; i < _children.length; ++i) {
                keysList = keysList.concat(_children[i].getModelDataKeys());
                if (_children[i] is DataContainer)
                    keysList = keysList.concat((_children[i] as DataContainer).getChildrenDataKeys());
            }

            return ArrayUtil.removeDuplicates(keysList);
        }

        /**
         * Импортировать данные в модель и ее детей
         * @param data
         */
        public function invokeDataImport(data:Object):void
        {
            var dataKeys:Array = ObjectUtil.getKeys(data);
            for (var i:int = 0; i < _children.length; ++i) {
                if (_children[i] is DataContainer)
                    _sendDataToContainer(_children[i] as DataContainer, dataKeys, data);
                _sendDataToModel(_children[i], dataKeys, data);
            }

            _sendDataToModel(this, dataKeys, data);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _children:Vector.<Data> = new Vector.<Data>();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _checkAddition(child:Data):Boolean
        {
            if (child === this)
                Error.throwError(ArgumentError, 2024);
            if (this.parent == child)
                Error.throwError(ArgumentError, 2024);

            if (child === null)
                throw new ArgumentError("child is null");

            if (this.contains(child.name)) {
                throw new ArgumentError("DataContainer has already element with same name");
            }

            if (child.name == "")
                throw new ArgumentError("child name is not defined");

            return true;
        }

        private function _sendDataToContainer(container:DataContainer, dataKeys:Array, data:Object):void
        {
            var modelKeys:Array = container.getChildrenDataKeys();
            if (ArrayUtil.containsAny(modelKeys, dataKeys))
                container.invokeDataImport(data);
        }

        private function _sendDataToModel(model:Data, dataKeys:Array, data:Object):void
        {
            var modelKeys:Array = model.getModelDataKeys();
            if (ArrayUtil.containsAny(modelKeys, dataKeys))
                for (var i:int; i < modelKeys.length; ++i)
                    if (data.hasOwnProperty(modelKeys[i]))
                        model.importData(data[modelKeys[i]]);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get numChildren():int
        {
            return _children.length;
        }
    }

}