package framework.core.struct.data
{
    import flash.events.Event;

    public class ModelDataEvent extends Event
    {
        public static const INIT:String = 'init';
        public static const ADD:String = 'add';
        public static const UPDATE:String = 'update';
        public static const DELETE:String = 'delete';

        private var _data:Object = {};

        public function ModelDataEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }

        override public function clone():Event
        {
            var event:ModelDataEvent = new ModelDataEvent(this.type, this.bubbles, this.cancelable);
            event.data = this.data;
            return event;
        }

        public function get data():Object
        {
            return _data;
        }

        public function set data(value:Object):void
        {
            _data = value;
        }
    }
}
