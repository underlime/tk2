package framework.core.struct.data.fields
{
    public class CharField implements IDataField
    {
        private var _value:String = '';

        public function CharField(value:String = "")
        {
            _value = value;
            super();
        }

        public function get value():String
        {
            return _value;
        }

        public function set value(value:String):void
        {
            _value = value;
        }

        public function importValue(value:*):void
        {
            _value = String(value);
        }
    }
}
