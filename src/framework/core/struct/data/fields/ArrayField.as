package framework.core.struct.data.fields
{
    public class ArrayField implements IDataField
    {
        private var _value:Array = [];

        public function importValue(value:*):void
        {
            _value = value as Array;
        }

        public function get value():Array
        {
            return _value;
        }

        public function set value(value:Array):void
        {
            _value = value;
        }
    }
}
