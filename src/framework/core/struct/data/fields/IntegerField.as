package framework.core.struct.data.fields
{
    public class IntegerField implements IDataField
    {
        private var _value:int = 0;

        public function IntegerField(value:int = 0)
        {
            _value = value;
            super();
        }

        public function get value():int
        {
            return _value;
        }

        public function set value(value:int):void
        {
            _value = value;
        }

        public function importValue(value:*):void
        {
            _value = int(value);
        }
    }
}
