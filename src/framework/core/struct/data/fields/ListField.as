package framework.core.struct.data.fields
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.ListData;
    import framework.core.struct.data.ModelDataEvent;

    import org.casalib.events.RemovableEventDispatcher;

    public class ListField extends RemovableEventDispatcher implements IDataField
    {
        private static const LIST_KEYS:Array = ['_init', '_add', '_update', '_delete'];

        private var _RecordClass:Class;
        private var _value:Object = {};

        public function ListField(recordClass:Class)
        {
            _RecordClass = recordClass;
        }

        public function importValue(value:*):void
        {
            if (value is Object) {
                if (value.hasOwnProperty('_init'))
                    _init(value['_init']);
                else
                    _changeRecords(value);
            }
        }

        private function _changeRecords(value:Object):void
        {
            if (value.hasOwnProperty('_add'))
                _add(value['_add']);

            if (value.hasOwnProperty('_update'))
                _update(value['_update']);

            if (value.hasOwnProperty('_delete') && value['_delete'] is Array)
                _delete(value['_delete']);
        }

        private function _init(valuesList:Object):void
        {
            _value = {};
            var record:Object;
            var recordsList:Array = [];
            for (var key:String in valuesList) {
                record = _createRecord(valuesList[key]);
                recordsList.push(record);
                _value[key] = record;
            }

            var event:ModelDataEvent = new ModelDataEvent(ModelDataEvent.INIT);
            event.data = recordsList;
            dispatchEvent(event);
        }

        private function _createRecord(data:Object):Object
        {
            var record:Object = new _RecordClass();
            _fillRecord(record, data);
            return record;
        }

        private function _fillRecord(record:Object, data:Object):void
        {
            for (var key:String in data) {
                if (record.hasOwnProperty(key))
                    _setRecordSimpleValue(record, data, key);
                else
                    if (record is ListData && LIST_KEYS.indexOf(key) != -1)
                        _setRecordListValue(record, data, key);
            }
        }

        private function _setRecordSimpleValue(record:Object, data:Object, key:String):void
        {
            if (record[key] is IDataField)
                record[key].importValue(data[key]);
            else
                if (record[key] is Data)
                    record[key].importData(data[key]);
        }

        private function _setRecordListValue(record:Object, data:Object, key:String):void
        {
            var listValue:Object;
            listValue = {};
            listValue[key] = data[key];
            record.list.importValue(listValue);
        }

        private function _add(valuesList:Object):void
        {
            var record:Object;
            var recordsList:Array = [];
            for (var id:String in valuesList)
                if (valuesList.hasOwnProperty(id)) {
                    record = _createRecord(valuesList[id]);
                    recordsList.push(record);
                    _value[id] = record;
                }

            var event:ModelDataEvent = new ModelDataEvent(ModelDataEvent.ADD);
            event.data = recordsList;
            dispatchEvent(event);
        }

        private function _update(valuesList:Object):void
        {
            var record:Object;
            var recordsList:Array = [];
            var data:Object;
            for (var id:String in valuesList) {
                if (_value.hasOwnProperty(id)) {
                    record = _value[id];
                    data = valuesList[id];
                    _fillRecord(record, data);
                    recordsList.push(_value[id]);
                }
            }

            var event:ModelDataEvent = new ModelDataEvent(ModelDataEvent.UPDATE);
            event.data = recordsList;
            dispatchEvent(event);
        }

        private function _delete(valuesList:Array):void
        {
            for (var i:int = 0; i < valuesList.length; ++i)
                if (_value.hasOwnProperty(valuesList[i]))
                    delete _value[valuesList[i]];

            var event:ModelDataEvent = new ModelDataEvent(ModelDataEvent.DELETE);
            event.data = valuesList;
            dispatchEvent(event);
        }

        public function get value():Object
        {
            return _value;
        }
    }
}
