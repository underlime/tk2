package framework.core.struct.data.fields
{
    public interface IDataField
    {
        function importValue(value:*):void;
    }
}
