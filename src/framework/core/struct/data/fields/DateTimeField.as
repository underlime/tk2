package framework.core.struct.data.fields
{
    import framework.core.helpers.DateHelper;
    import framework.core.helpers.MathHelper;

    public class DateTimeField implements IDataField
    {
        private var _value:Date = null;
        private var _utc:Boolean = true;

        private var _month_ru:Array = [
            "января",
            "февраля",
            "марта",
            "апреля",
            "мая",
            "июня",
            "августа",
            "сентября",
            "октября",
            "ноября",
            "декабря"
        ];

        public function DateTimeField(value:Date = null, utc:Boolean = true)
        {
            _value = value;
            _utc = utc;
            super();
        }

        public function get value():Date
        {
            return _value;
        }

        public function set value(value:Date):void
        {
            _value = value;
        }

        public function get rus_mouth():String
        {
            if (_value) {
                var month:int = MathHelper.clamp(_value.getMonth() + 1, 1, 12);
                return _month_ru[month - 1];
            }

            return "";
        }

        public function getFormattedDate(rus:Boolean = true):String
        {
            if (_value) {
                var day:int = _value.getDate();
                var month:String = rus ? this.rus_mouth : (_value.getMonth() + 1).toString();
                var year:int = _value.getFullYear();
                var delimiter:String = rus ? " " : ".";

                var datePart:String = day.toString() + delimiter + month.toString() + delimiter + year.toString();

                var hours:int = _value.getHours();
                var strHours:String = hours.toString();

                if (hours < 10) {
                    strHours = "0" + strHours;
                }

                var minutes:int = _value.getMinutes();
                var strMinutes:String = minutes.toString();

                if (minutes < 10) {
                    strMinutes = "0" + minutes.toString();
                }

                delimiter = ":";
                var timePart:String = strHours + delimiter + strMinutes;

                return datePart + ", " + timePart;
            }

            return "";
        }

        public function importValue(value:*):void
        {
            if (value is Date) {
                _value = value;
            }
            else if (value is String) {
                _value = DateHelper.strToDate(value, _utc);
            }
            else {
                _value = null;
            }
        }
    }
}
