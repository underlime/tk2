package framework.core.struct.data.fields
{
    public class FloatField implements IDataField
    {
        private var _value:Number = 0;

        public function FloatField(value:Number = 0)
        {
            _value = value;
            super();
        }

        public function get value():Number
        {
            return _value;
        }

        public function set value(value:Number):void
        {
            _value = value;
        }

        public function importValue(value:*):void
        {
            _value = Number(value);
        }
    }
}
