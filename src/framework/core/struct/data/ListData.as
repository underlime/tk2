package framework.core.struct.data
{
    import framework.core.struct.data.fields.IDataField;
    import framework.core.struct.data.fields.ListField;
    import org.casalib.util.ObjectUtil;

    public class ListData extends Data
    {
        protected var _RecordClass:Class = null;
        private var _list:ListField = null;

        public function ListData(name:String)
        {
            if (_RecordClass === null)
                _RecordClass = Object;
            _list = new ListField(_RecordClass);

            _list.addEventListener(ModelDataEvent.INIT, _redispatchEvent);
            _list.addEventListener(ModelDataEvent.ADD, _redispatchEvent);
            _list.addEventListener(ModelDataEvent.UPDATE, _redispatchEvent);
            _list.addEventListener(ModelDataEvent.DELETE, _redispatchEvent);

            super(name);
        }

        private function _redispatchEvent(e:ModelDataEvent):void
        {
            dispatchEvent(e);
        }

        override public function destroy():void
        {
            _list.removeEventListener(ModelDataEvent.INIT, _redispatchEvent);
            _list.removeEventListener(ModelDataEvent.ADD, _redispatchEvent);
            _list.removeEventListener(ModelDataEvent.UPDATE, _redispatchEvent);
            _list.removeEventListener(ModelDataEvent.DELETE, _redispatchEvent);

            super.destroy();
        }

        override public function importData(data:Object):void
        {
            if (data['_init'] || data['_add'] || data['_update'] || data['_delete'])
                super.importData({ 'list': data });
            super.importData(data);
        }

        /**
         * Метод для поиска элементов
         * @param params
         * @param skip
         * @param limit
         * @return
         */
        public function search(params:Object, skip:int = 0, limit:int = int.MAX_VALUE):Array
        {
            var result:Array = [];
            var collection:Object = this.list.value;
            var total:int = 0;
            var start:int = 0;

            for (var index:String in collection) {
                var item:Object = collection[index];
                if (_validateSearchParams(item, params)) {
                    if (start >= skip) {
                        result.push(item);
                        total++;
                        if (total >= limit)
                            break;
                    }
                    start++;
                }
            }

            return result;
        }

        private function _validateSearchParams(record:Object, params:Object):Boolean
        {
            for (var index:String in params) {
                if (!record.hasOwnProperty(index))
                    return false;

                if (!(record[index] is IDataField))
                    return false;

                if (record[index].value != params[index])
                    return false;
            }
            return true;
        }

        public function getRecordById(id:String):Object
        {
            if (list.value[id])
                return list.value[id];
            return null;
        }

        /**
         * Проверяет, каких записей нет в списке
         * @param idsList Список id для проверки
         * @return id, которых нет в модели
         */
        public function checkNotExists(idsList:Array):Array
        {
            var notExistsList:Array = [];
            for (var i:int = 0; i < idsList.length; ++i) {
                var id:String = idsList[i];
                if (!list.value[id])
                    notExistsList.push(id.toString());
            }

            return notExistsList;
        }

        public function get isEmpty():Boolean
        {
            return (ObjectUtil.isEmpty(_list.value));
        }

        public function get length():int
        {
            var length:int = 0;
            for each (var record:* in _list.value)
                ++length;
            return length;
        }

        public function get list():ListField
        {
            return _list;
        }
    }
}
