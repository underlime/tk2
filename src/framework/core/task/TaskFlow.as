/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.07.13
 * Time: 16:34
 * Класс для выполнения очереди асинхронных задач
 */
package framework.core.task
{
    public class TaskFlow extends Task
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TaskFlow()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_taskList.length > 0)
                _start();
            else
                _complete();
        }

        override public function destroy():void
        {
            var count:int = _taskList.length;
            if (count > 0) {
                for (var i:int = 0; i < count; i++) {
                    if (_taskList[i] && !_taskList[i].destroyed) {
                        _taskList[i].destroy();
                    }
                }
            }
            _taskList = null;
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function add(task:Task):void
        {
            if (!_isRunning)
                _taskList.push(task);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _taskList:Vector.<Task> = new Vector.<Task>();
        private var _taskIndex:int = 0;

        private var _isRunning:Boolean = false;
        private var _taskCount:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _start():void
        {
            _isRunning = true;
            _taskCount = _taskList.length;
            _doNextTask();
        }

        private function _doNextTask():void
        {
            if (_taskIndex < _taskCount) {
                var currentTask:Task = _taskList[_taskIndex];
                currentTask.addEventListener(TaskEvent.COMPLETE, _completeTaskHandler);
                currentTask.addEventListener(TaskEvent.ERROR, _errorTaskHandler);
                currentTask.execute();
            } else {
                _complete();
            }
        }

        private function _complete():void
        {
            _isRunning = false;
            super.dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeTaskHandler(event:TaskEvent):void
        {
            var task:Task = event.currentTarget as Task;
            task.removeEventListener(TaskEvent.COMPLETE, _completeTaskHandler);
            task.removeEventListener(TaskEvent.ERROR, _errorTaskHandler);
            task.destroy();

            _taskIndex++;
            _doNextTask();
        }

        private function _errorTaskHandler(event:TaskEvent):void
        {
            trace("task error");

            var task:Task = event.currentTarget as Task;
            task.removeEventListener(TaskEvent.COMPLETE, _completeTaskHandler);
            task.removeEventListener(TaskEvent.ERROR, _errorTaskHandler);
            task.destroy();

            _taskIndex++;
            _doNextTask();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get taskList():Vector.<Task>
        {
            return _taskList;
        }

        public function get isRunning():Boolean
        {
            return _isRunning;
        }
    }
}
