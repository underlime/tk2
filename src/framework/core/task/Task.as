/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.07.13
 * Time: 16:40
 */
package framework.core.task
{
    import framework.core.struct.service.Service;

    public class Task extends Service
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Task()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function complete():void
        {
            _completed = true;
            dispatchEvent(new TaskEvent(TaskEvent.COMPLETE));
        }

        protected function error():void
        {
            dispatchEvent(new TaskEvent(TaskEvent.ERROR));
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _completed:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get completed():Boolean
        {
            return _completed;
        }
    }
}
