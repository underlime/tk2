/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.10.13
 * Time: 15:45
 */
package framework.core.enum
{

    public class Language extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const RU:Language = new Language("ru");
        public static const EN:Language = new Language("en_US");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Language(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import framework.core.enum.Language;

Language.lockUp();