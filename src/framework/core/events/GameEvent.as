/**
 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
 */
package framework.core.events
{

    import flash.events.Event;

    public class GameEvent extends Event
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const COMPLETE:String = "CompleteGameEvent";
        public static const CHANGE:String = "ChangeGameEvent";
        public static const DESTROY:String = "DestroyEvent";
        public static const ALBUM_CREATED:String = "AlbumCreated";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GameEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:GameEvent = new GameEvent(super.type, super.bubbles, super.cancelable);
            event.data = _data;
            event.raw = raw;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var raw:String = "";

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _data:Object = {};

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get data():Object
        {
            return _data;
        }

        public function set data(value:Object):void
        {
            _data = value;
        }
    }
}
