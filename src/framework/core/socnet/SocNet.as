/**
 * Публичные методы классов с интерфейсом SocNet
 * соответствуют методам API взаимодействия с соц. сетью.
 *
 * При вызове метода возможны 3 события:
 * - успех,
 * - ошибка,
 * - отмена операции пользователем.
 *
 * По умолчанию успех инициирует импорт данных в модели,
 * ошибка вызывает метод BaseController.appError, а отмена
 * ничего не делает.
 *
 * Это поведение можно переопределить, если передавать методам колбэки
 * (необязательные параметры). Колбэки должны возвращать тип Boolean,
 * при возврате true после колбэка выполняется действие по умолчанию,
 * при false или если колбэк имеет тип void - действие по умолчанию
 * не выполняется.
 */
package framework.core.socnet
{
    import application.config.AppConfig;

    import flash.events.TimerEvent;
    import flash.external.ExternalInterface;
    import flash.utils.Timer;

    import framework.core.error.ApplicationError;
    import framework.core.socnet.struct.BuyMoneyById;
    import framework.core.struct.controller.BaseController;

    public class SocNet
    {
        public static const VK_COM:String = 'vk.com';
        public static const FACEBOOK_COM:String = 'facebook.com';
        public static const ODNOKLASSNIKI_RU:String = 'odnoklassniki.ru';
        public static const MY_MAIL_RU:String = 'my.mail.ru';
        public static const MOCK:String = 'mock';

        public static const LANG_RU:String = 'ru';
        public static const LANG_EN:String = 'en_US';

        public static const REQUEST_TIMEOUT:int = 30000;

        private static var _instance:SocNet;
        private static var _socNetName:String;

        protected var _baseController:BaseController;
        protected var _requestId:uint = 0;
        protected var _callKeysRegistry:Object = {};

        /*
         * Фабрика
         * При инициализации нужен базовый контроллер, при последующих обращениях
         * он не нужен
         */
        public static function instance(baseController:BaseController = null):SocNet
        {
            if (_instance == null)
                _instance = _getInstance(baseController);
            return _instance;
        }

        private static function _getInstance(baseController:BaseController):SocNet
        {
            if (!baseController)
                throw new Error('Base controller is null');

            switch (SocNet.socNetType) {
                case VK_COM:
                    return new VkCom(baseController, new PrivateClass());
                case FACEBOOK_COM:
                    return new FacebookCom(baseController, new PrivateClass());
                case ODNOKLASSNIKI_RU:
                    return new OdnoklassnikiRu(baseController, new PrivateClass());
                case MY_MAIL_RU:
                    return new MyMailRu(baseController, new PrivateClass());
                case MOCK:
                    if (AppConfig.DEBUG)
                        return new VkComMock(baseController, new PrivateClass());
                    else
                        throw new Error('Wrong social network');
                default:
                    throw new Error('Wrong social network');
            }
        }

        public static function get socNetType():String
        {
            if (ExternalInterface.available)
                _socNetName = ExternalInterface.call('SocNetWrapper.getSocNetName') as String;
            else
                _socNetName = MOCK;
            return _socNetName;
        }

        /*
         * Конструктор
         */
        public function SocNet(baseController:BaseController, privateClass:PrivateClass)
        {
            _baseController = baseController;
            _registerCallbacks();
        }

        protected function _registerCallbacks():void
        {
            if (ExternalInterface.available) {
                ExternalInterface.addCallback('onSocNetSuccess', _onSuccessCallback);
                ExternalInterface.addCallback('onSocNetCancel', _onCancelCallback);
                ExternalInterface.addCallback('onSocNetError', _onErrorCallback);
                ExternalInterface.call('onSocNetCallbacks');
            }
        }

        protected function _onSuccessCallback(data:Object):void
        {
            if (data.hasOwnProperty('request_id')) {
                var requestId:String = data['request_id'];
                if (_callKeysRegistry.hasOwnProperty(requestId))
                    _handleSuccess(requestId, data);
                _cleanCallData(requestId);
            }
        }

        protected function _handleSuccess(requestId:String, data:Object):void
        {
            var callData:CallData = _callKeysRegistry[requestId], needImport:Boolean = true;

            if (callData.success_callback != null)
                needImport = callData.success_callback(data);

            if (needImport)
                _invokeDataImport(data, callData);
        }

        protected function _invokeDataImport(data:Object, callData:CallData):void
        {
            var dataToImport:Object = {}, key:String, c:String;

            for (c in data)
                if (data.hasOwnProperty(c) && c != 'request_id') {
                    key = callData.model_prefix + c;
                    dataToImport[key] = data[c];
                }
            _baseController.dataBase.invokeDataImport(dataToImport);
        }

        protected function _cleanCallData(requestId:String):void
        {
            if (_callKeysRegistry.hasOwnProperty(requestId)) {
                if (_callKeysRegistry[requestId].request_timer) {
                    _callKeysRegistry[requestId].request_timer.stop();
                    _callKeysRegistry[requestId].request_timer = null;
                }
                delete _callKeysRegistry[requestId];
            }
        }

        protected function _onCancelCallback(data:Object):void
        {
            if (data.hasOwnProperty('request_id')) {
                var requestId:String = data['request_id'], callData:CallData = _callKeysRegistry[requestId];

                if (callData.cancel_callback != null)
                    callData.cancel_callback(data);

                _cleanCallData(requestId);
            }
        }

        protected function _onErrorCallback(data:Object):void
        {
            if (data.hasOwnProperty('request_id')) {
                var requestId:String = data['request_id'];
                if (_callKeysRegistry.hasOwnProperty(requestId))
                    _handleError(requestId, data);
                _cleanCallData(requestId);
            }
        }

        protected function _handleError(requestId:String, data:Object):void
        {
            var callData:CallData = _callKeysRegistry[requestId], needAppError:Boolean = true;

            if (callData.error_callback != null)
                needAppError = callData.error_callback(data);

            if (needAppError) {
                var error:ApplicationError = new ApplicationError();
                error.type = ApplicationError.SOCIAL_NETWORK_ERROR;
                error.message = 'Response error';
                _baseController.appError(error);
            }
        }

        /*
         * Методы соц. сети
         */
        public function get capabilities():Object
        {
            var capabilities:Object = {};
            var capFunction:String = 'function() {return SocNetWrapper.CAPABILITIES}';
            if (ExternalInterface.available)
                capabilities = ExternalInterface.call(capFunction) as Object;
            else
                capabilities = {
                    'favorites': true,
                    'set_avatar': true,
                    'group_membership': true
                };

            return capabilities;
        }

        public function get socNetName():String
        {
            return _socNetName;
        }

        public function get serverCallParams():Object
        {
            var params:Object = {};
            if (ExternalInterface.available)
                params = ExternalInterface.call('SocNetWrapper.getServerCallParams') as Object;
            return params;
        }

        public function get currentLanguage():String
        {
            var lang:String = LANG_RU;
            if (ExternalInterface.available)
                lang = ExternalInterface.call('SocNetWrapper.getCurrentLanguage') as String;
            return lang;
        }

        public function get accessToken():String
        {
            var token:String = null;
            if (ExternalInterface.available)
                token = ExternalInterface.call('SocNetWrapper.getAccessToken') as String;
            return token;
        }

        public function getUserInfo(socNetId:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.getUserInfo', [socNetId], successCallback, errorCallback);
        }

        public function getReferrerInfo(successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.getReferrerInfo', [], successCallback, errorCallback);
        }

        public function getUsersListInfo(socNetIdsList:Array, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.getUsersListInfo', [socNetIdsList], successCallback, errorCallback);
        }

        public function getFriendsList(socNetId:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.getFriendsList', [socNetId], successCallback, errorCallback);
        }

        public function getFriendsInAppList(successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.getFriendsInAppList', [], successCallback, errorCallback);
        }

        public function getCurrentUserFullInfo(successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.getCurrentUserFullInfo', [], successCallback, errorCallback);
        }

        public function getCountryName(countryId:int, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.getCountryName', [countryId], successCallback, errorCallback);
        }

        public function getCityName(cityId:int, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.getCityName', [cityId], successCallback, errorCallback);
        }

        public function showInviteBox():void
        {
            ExternalInterface.call('SocNetWrapper.showInviteBox');
        }

        public function buyMoney(currency:String, count:int, price:int, successCallback:Function = null, errorCallback:Function = null, cancelCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.buyMoney', [currency, count, price],
                    successCallback, errorCallback, cancelCallback, false);
        }

        public function buyMoneyById(params:BuyMoneyById, successCallback:Function = null, errorCallback:Function = null, cancelCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.buyMoneyById', [Object(params)], successCallback,
                    errorCallback, cancelCallback, false);
        }

        /*
         * Обеспечение методов соц. сетей
         */
        protected function _callMethod(method:String, methodParams:Array, successCallback:Function, errorCallback:Function, cancelCallback:Function = null, needCheckTimeout:Boolean = true):void
        {
            var requestId:uint = _requestId++;
            var callData:CallData = new CallData();

            callData.method_name = method;
            callData.request_id = requestId;
            callData.success_callback = successCallback;
            callData.error_callback = errorCallback;
            callData.cancel_callback = cancelCallback;
            _callKeysRegistry[requestId] = callData;

            if (needCheckTimeout)
                _checkCallTimeout(callData);
            var callParams:Array = [method, requestId].concat(methodParams);
            ExternalInterface.call.apply(null, callParams);
        }

        protected function _checkCallTimeout(callData:CallData):void
        {
            callData.request_timer = new Timer(REQUEST_TIMEOUT, 1);
            callData.request_timer.addEventListener(TimerEvent.TIMER, onTimeout);
            callData.request_timer.start();

            function onTimeout(e:TimerEvent):void
            {
                trace('SocNet timeout!');
                trace('    Method: ' + callData.method_name);
                trace('    Request id: ' + callData.request_id);

                _onErrorCallback({
                    'request_id': callData.request_id.toString(),
                    'timeout': true
                });
            }
        }

        /*
         * Токен
         */
        public function createToken(queryString:String, salt:String):String
        {
            throw new Error('Function is not implemented');
        }

        public function get photosUploadAllowed():Boolean
        {
            var status:Boolean = true;
            if (this.socNetName == SocNet.ODNOKLASSNIKI_RU) {
                status = OdnoklassnikiRu(this).getPhotosPermission();
            }
            return status;
        }

        public function get queryParams():Object
        {
            var queryParams:Object = {};
            if (ExternalInterface.available)
                queryParams = ExternalInterface.call('SocNetWrapper.getQueryParams') as Object;
            return queryParams;
        }
    }
}
