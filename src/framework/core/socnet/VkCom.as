package framework.core.socnet
{
    import by.blooddy.crypto.MD5;

    import framework.core.struct.controller.BaseController;

    public class VkCom extends SocNet
    {
        public function VkCom(baseController:BaseController, privateClass:PrivateClass)
        {
            super(baseController, privateClass);
        }

        override public function createToken(requestUri:String, salt:String):String
        {
            var socNetParams:Object = this.serverCallParams;
            var baseString:String =
                socNetParams['auth_key']
                    + socNetParams['sid']
                    + socNetParams['viewer_id']
                    + requestUri
                    + salt;
            return MD5.hash(baseString);
        }

        public function getAlbumInfo(albumId:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('VkPostTool.getAlbumInfo', [albumId], successCallback, errorCallback);
        }

        public function getAlbumsListInfo(successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('VkPostTool.getAlbumsListInfo', [], successCallback, errorCallback);
        }

        public function createAlbum(name:String, privacy:int, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('VkPostTool.createAlbum', [name, privacy], successCallback, errorCallback);
        }

        public function getUploadPhotoParams(albumId:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('VkPostTool.getUploadPhotoParams', [albumId], successCallback, errorCallback);
        }

        public function savePhoto(albumId:String, serverResponse:Object, successCallback:Function = null, errorCallback:Function = null):void
        {
            var respString:String = serverResponse.toString().replace('/\(.)/g', '$1');
            var respObj:Object;
            try {
                respObj = JSON.parse(respString);
            }
            catch (e:SyntaxError) {
                respObj = {};
            }

            _callMethod('VkPostTool.savePhoto', [albumId, respObj], successCallback, errorCallback);
        }

        public function postPhotoToWall(photoId:String, message:String, link:String):void
        {
            _callMethod('VkPostTool.postPhotoToWall', [photoId , message, link], callback, null, null, false);
            function callback(data:Object):void
            {
            }
        }

        public function getUploadProfilePhotoParams(successCallback:Function, errorCallback:Function = null):void
        {
            _callMethod('VkPostTool.getUploadProfilePhotoParams', [], successCallback, errorCallback);
        }

        public function setUserAvatar(serverResponse:Object, successCallback:Function = null):void
        {
            if (serverResponse is String)
                serverResponse = JSON.parse(serverResponse as String);
            _callMethod('VkPostTool.setUserAvatar', [serverResponse], successCallback, null, null, false);
        }
    }
}
