package framework.core.socnet
{
    import by.blooddy.crypto.MD5;

    import flash.utils.Timer;

    import framework.core.socnet.struct.BuyMoneyById;
    import framework.core.struct.controller.BaseController;

    public class VkComMock extends VkCom
    {
        private static const API_ID:int = 1;
        private static const API_SECRET:String = 'Secret';

        public function VkComMock(baseController:BaseController, privateClass:PrivateClass)
        {
            super(baseController, privateClass);
        }

        override public function get serverCallParams():Object
        {
            var params:Object = {
                'viewer_id': 3081515,//14849282//3081515//160437082//164980663
                'sid': '123'
            };
            var authKeyBase:String = API_ID + '_' + params['viewer_id'] + '_' + API_SECRET;

            params['auth_key'] = MD5.hash(authKeyBase);
            return params;
        }


        override public function getUserInfo(socNetId:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback);
            _onSuccessCallback({
                'request_id': 0,
                'user_info': {
                    'social_id': socNetId,
                    'first_name': 'Андрей',
                    'last_name': 'Прокопюк',
                    'sex': 'M',
                    'birth_date': '1988-01-01 00:00:00',
                    'city': 722,
                    'country': 1,
                    'timezone': 3,
                    'picture': 'http://cs417729.vk.me/v417729515/b4eb/mSCdakZyh6w.jpg',
                    'photo_medium': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                    'photo_big': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                    'friends': []
                }
            });
        }

        override public function getReferrerInfo(successCallback:Function = null, errorCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback);
            var data:Object = {
                'request_id': 0,
                'referrer_data': {
                    'type': 'client_test',
                    'user_id': 3081515
                }
            };
            _onSuccessCallback(data);
        }

        override public function getUsersListInfo(socNetIdsList:Array, successCallback:Function = null, errorCallback:Function = null):void
        {
            var usersList:Object = {};
            for (var i:int = 0; i < socNetIdsList.length; ++i) {
                usersList[socNetIdsList[i]] = {
                    'social_id': socNetIdsList[i],
                    'first_name': 'User',
                    'last_name': socNetIdsList[i],
                    'sex': 'M',
                    'birth_date': '1988-01-01 00:00:00',
                    'city': 722,
                    'country': 1,
                    'timezone': 3,
                    'picture': 'http://cs417729.vk.me/v417729515/b4eb/mSCdakZyh6w.jpg',
                    'photo_medium': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                    'photo_big': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                    'friends': []
                };
            }

            _makeMockCall(successCallback, errorCallback);
            _onSuccessCallback({ 'request_id': 0, 'users_list': { '_add': usersList} });
        }

        private function _makeMockCall(successCallback:Function, errorCallback:Function, cancelCallback:Function = null):void
        {
            var callData:CallData = new CallData();
            callData.request_id = 0;
            callData.success_callback = successCallback;
            callData.error_callback = errorCallback;
            callData.cancel_callback = cancelCallback;
            callData.request_timer = new Timer(REQUEST_TIMEOUT, 1);

            _callKeysRegistry[0] = callData;
        }

        override public function getFriendsList(socNetId:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback);
            _onSuccessCallback({
                'request_id': 0,
                'friends_list': [
                    {
                        'social_id': 3081515,
                        'first_name': 'Андрей',
                        'last_name': 'Прокопюк',
                        'sex': 'M',
                        'birth_date': '1988-01-01 00:00:00',
                        'city': 722,
                        'country': 1,
                        'timezone': 3,
                        'picture': 'http://cs417729.vk.me/v417729515/b4eb/mSCdakZyh6w.jpg',
                        'photo_medium': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                        'photo_big': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                        'friends': []
                    }
                ]
            });
        }

        override public function getFriendsInAppList(successCallback:Function = null, errorCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback);
            _onSuccessCallback({
                'request_id': 0,
                'friends_list': [
                    {
                        'social_id': 3081515,
                        'first_name': 'Андрей',
                        'last_name': 'Прокопюк',
                        'sex': 'M',
                        'birth_date': '1988-01-01 00:00:00',
                        'city': 722,
                        'country': 1,
                        'timezone': 3,
                        'picture': 'http://cs417729.vk.me/v417729515/b4eb/mSCdakZyh6w.jpg',
                        'photo_medium': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                        'photo_big': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                        'friends': []
                    }
                ]
            });
        }

        override public function getCurrentUserFullInfo(successCallback:Function = null, errorCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback);
            _onSuccessCallback({
                'request_id': 0,
                'user_info': {
                    'social_id': 3081515,
                    'first_name': 'Андрей',
                    'last_name': 'Прокопюк',
                    'sex': 'M',
                    'birth_date': '1988-01-01 00:00:00',
                    'city': 722,
                    'country': 1,
                    'timezone': 3,
                    'picture': 'http://cs417729.vk.me/v417729515/b4eb/mSCdakZyh6w.jpg',
                    'photo_medium': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                    'photo_big': 'http://cs417729.vk.me/v417729515/b4e7/sbf_x8k5Dpk.jpg',
                    'friends': [
                        {
                            'social_id': 3081515,
                            'first_name': 'Андрей',
                            'last_name': 'Прокопюк',
                            'sex': 'M',
                            'birth_date': '1988-01-01 00:00:00',
                            'city': 722,
                            'country': 1,
                            'timezone': 3,
                            'picture': '../lib/in_extremo.jpg',
                            'photo_medium': '../lib/in_extremo.jpg',
                            'photo_big': '../lib/in_extremo.jpg',
                            'friends': []
                        }
                    ]
                }
            });
        }

        override public function getCountryName(countryId:int, successCallback:Function = null, errorCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback);
            _onSuccessCallback({
                'request_id': 0,
                'country_name': 'Россия'
            });
        }

        override public function getCityName(cityId:int, successCallback:Function = null, errorCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback);
            _onSuccessCallback({
                'request_id': 0,
                'city_name': 'Ногинск'
            });
        }

        override public function showInviteBox():void
        {
            trace('invite box');
        }

        override public function buyMoney(currency:String, count:int, price:int, successCallback:Function = null, errorCallback:Function = null, cancelCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback, cancelCallback);
            _onSuccessCallback({
                'request_id': 0,
                'data': {}
            });
        }

        override public function buyMoneyById(params:BuyMoneyById, successCallback:Function = null, errorCallback:Function = null, cancelCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback, cancelCallback);
            _onSuccessCallback({
                'request_id': 0,
                'data': {}
            });
        }

        override public function getUploadPhotoParams(albumId:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback);
            _onSuccessCallback({
                'request_id': 0,
                'server_info': null
            });
        }

        override public function savePhoto(albumId:String, serverResponse:Object, successCallback:Function = null, errorCallback:Function = null):void
        {
            _makeMockCall(successCallback, errorCallback);
            _onSuccessCallback({
                'request_id': 0,
                'photo_info': null
            });
        }

        override public function postPhotoToWall(photoId:String, message:String, link:String):void
        {
            trace("Post photo to wall");
            trace(" PhotoId: ", photoId);
            trace(" Message: ", message);
            trace(" Link: ", link);
        }
    }
}
