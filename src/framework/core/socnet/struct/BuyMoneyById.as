package framework.core.socnet.struct
{
    public class BuyMoneyById
    {
        public var item_id:int = 0;
        public var count:int = 0;
        public var currency:String = "";
        public var price:Number = 0;

        public function BuyMoneyById()
        {
        }
    }
}
