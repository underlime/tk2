package framework.core.socnet.models
{
    import framework.core.socnet.models.soc_net_registry.SocNetUserRecord;
    import framework.core.struct.data.ListData;

    public class SocNetInfoRegistry extends ListData
    {
        public function SocNetInfoRegistry()
        {
            _RecordClass = SocNetUserRecord;
            super('SocNetInfoRegistry');
        }

        override public function getModelDataKeys():Array
        {
            return ['soc_net_users_list'];
        }
    }
}
