package framework.core.socnet.models.soc_net_registry
{
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.data.fields.IntegerField;

    public class SocNetUserRecord
    {
        private var _social_id:IntegerField = new IntegerField();
        private var _first_name:CharField = new CharField();
        private var _last_name:CharField = new CharField();
        private var _sex:CharField = new CharField();
        private var _birth_date:DateTimeField = new DateTimeField();
        private var _picture:CharField = new CharField();
        private var _photo_medium:CharField = new CharField();
        private var _photo_big:CharField = new CharField();

        public function SocNetUserRecord()
        {
        }

        public function get social_id():IntegerField
        {
            return _social_id;
        }

        public function get first_name():CharField
        {
            return _first_name;
        }

        public function get last_name():CharField
        {
            return _last_name;
        }

        public function get sex():CharField
        {
            return _sex;
        }

        public function get birth_date():DateTimeField
        {
            return _birth_date;
        }

        public function get picture():CharField
        {
            return _picture;
        }

        public function get photo_medium():CharField
        {
            return _photo_medium;
        }

        public function get photo_big():CharField
        {
            return _photo_big;
        }
    }
}
