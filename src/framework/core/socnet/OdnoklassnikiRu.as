package framework.core.socnet
{
    import by.blooddy.crypto.MD5;

    import flash.external.ExternalInterface;

    import framework.core.struct.controller.BaseController;

    public class OdnoklassnikiRu extends SocNet
    {
        public function OdnoklassnikiRu(baseController:BaseController, privateClass:PrivateClass)
        {
            super(baseController, privateClass);
        }

        override public function createToken(requestUri:String, salt:String):String
        {
            if (requestUri.charAt(requestUri.length-1) != "/") {
                requestUri += "/";
            }
            var socNetParams:Object = this.serverCallParams;
            var baseString:String =
                    socNetParams['query_params']
                            + requestUri
                            + salt;
            return MD5.hash(baseString);
        }

        public function getPhotosPermission():Boolean
        {
            return Boolean(ExternalInterface.call('OkPostTool.getPhotosPermission'));
        }

        public function getAlbumInfo(albumId:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('OkPostTool.getAlbumInfo', [albumId], successCallback, errorCallback);
        }

        public function getAlbumsListInfo(successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('OkPostTool.getAlbumsListInfo', [], successCallback, errorCallback);
        }

        public function createAlbum(name:String, type:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('OkPostTool.createAlbum', [name, type], successCallback, errorCallback);
        }

        public function getUploadPhotoParams(albumId:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('OkPostTool.getUploadPhotoParams', [albumId], successCallback, errorCallback);
        }

        public function savePhoto(photoId:String, token:String, comment:String = null, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('OkPostTool.commitPhoto', [photoId, token, comment], successCallback, errorCallback);
        }

        public function postPhotoToWall(photoSrc:String, shortText:String, fullText:String, linkText:String=""):void
        {
            _callMethod('OkPostTool.postPhotoToWall', [photoSrc , shortText, fullText, linkText], callback, null, null, false);
            function callback(data:Object):void
            {
            }
        }
    }
}
