package framework.core.socnet
{
    import by.blooddy.crypto.MD5;

    import framework.core.struct.controller.BaseController;

    public class MyMailRu extends SocNet
    {
        public function MyMailRu(baseController:BaseController, privateClass:PrivateClass)
        {
            super(baseController, privateClass);
        }

        override public function createToken(requestUri:String, salt:String):String
        {
            if (requestUri.charAt(requestUri.length-1) != "/") {
                requestUri += "/";
            }
            var socNetParams:Object = this.serverCallParams;
            var baseString:String =
                    socNetParams['query_params']
                            + requestUri
                            + salt;
            return MD5.hash(baseString);
        }
    }
}
