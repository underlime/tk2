package framework.core.socnet
{
    import flash.utils.Timer;

    public class CallData
    {
        public var method_name:String = '';
        public var request_id:uint = undefined;
        public var model_prefix:String = 'soc_net_';
        public var success_callback:Function = null;
        public var cancel_callback:Function = null;
        public var error_callback:Function = null;
        public var request_timer:Timer = null;

        public function CallData()
        {
        }
    }
}
