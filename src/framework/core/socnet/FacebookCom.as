package framework.core.socnet
{
    import by.blooddy.crypto.MD5;

    import framework.core.struct.controller.BaseController;

    public class FacebookCom extends SocNet
    {
        public function FacebookCom(baseController:BaseController, privateClass:PrivateClass)
        {
            super(baseController, privateClass);
        }

        override public function createToken(requestUri:String, salt:String):String
        {
            var socNetParams:Object = this.serverCallParams;
            var baseString:String =
                    socNetParams['signed_request']
                            + requestUri
                            + salt;
            return MD5.hash(baseString);
        }
    }
}
