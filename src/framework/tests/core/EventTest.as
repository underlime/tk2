/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.10.13
 * Time: 12:04
 */
package framework.tests.core
{
    import flash.events.Event;

    import flexunit.framework.Assert;

    import framework.core.struct.view.View;

    public class EventTest
    {

        private var _view:View;


        [Before]
        public function before():void
        {
            _createView();
        }

        [After]
        public function after():void
        {
            _view = null;
        }

        [Test(order=1, description="Object free test")]
        public function viewFreeTest():void
        {
            _bindView();
            Assert.assertTrue(_view.hasEventListener(Event.CHANGE));
            Assert.assertTrue(_view.hasEventListener(Event.COMPLETE));
            Assert.assertTrue(_view.hasEventListener(Event.CLOSE));

            _view.destroy();

            Assert.assertFalse(_view.hasEventListener(Event.CHANGE));
            Assert.assertFalse(_view.hasEventListener(Event.COMPLETE));
            Assert.assertFalse(_view.hasEventListener(Event.CLOSE));
        }

        private function _createView():void
        {
            _view = new View();
        }

        private function _bindView():void
        {
            _view.addEventListener(Event.CHANGE, _changeHandler);
            _view.addEventListener(Event.COMPLETE, _completeHandler);
            _view.addEventListener(Event.CLOSE, _closeHandler);
        }

        private function _changeHandler(event:Event):void
        {

        }

        private function _completeHandler(event:Event):void
        {

        }

        private function _closeHandler(event:Event):void
        {

        }
    }
}