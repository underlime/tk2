package framework.tests.data.classes.data_container
{
    import framework.core.struct.data.DataContainer;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.FloatField;

    public class ChildDummyDataContainer extends DataContainer
    {
        private var _float_field:FloatField = new FloatField();
        private var _char_field:CharField = new CharField();

        public function ChildDummyDataContainer()
        {
            super('ChildDummyDataContainer');
        }

        override public function getModelDataKeys():Array
        {
            return ['key3', 'key4'];
        }

        public function get float_field():FloatField
        {
            return _float_field;
        }

        public function get char_field():CharField
        {
            return _char_field;
        }
    }
}
