package framework.tests.data.classes.list_data
{
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.IntegerField;

    public class DummyListRecord
    {
        private var _id:IntegerField = new IntegerField();
        private var _char_field:CharField = new CharField();

        public function DummyListRecord()
        {
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get char_field():CharField
        {
            return _char_field;
        }
    }
}
