package framework.tests.data.classes.list_data
{
    import framework.core.struct.data.ListData;

    public class DummyListOfRecords extends ListData
    {
        public function DummyListOfRecords()
        {
            _RecordClass = DummyListRecord;
            super('DummyListOfRecords');
        }
    }
}
