package framework.tests.data.classes.data
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.BooleanField;
    import framework.core.struct.data.fields.DateTimeField;
    import framework.core.struct.data.fields.StructField;

    public class NestedDummyData extends Data
    {
        private var _boolean_field:BooleanField = new BooleanField();
        private var _struct_field:StructField = new StructField();
        private var _date:DateTimeField = new DateTimeField();

        public function NestedDummyData()
        {
            super('NestedDummyData');
        }

        public function get boolean_field():BooleanField
        {
            return _boolean_field;
        }

        public function get struct_field():StructField
        {
            return _struct_field;
        }

        public function get date():DateTimeField
        {
            return _date;
        }
    }
}
