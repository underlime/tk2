package framework.tests.data.classes.data
{
    import framework.core.struct.data.Data;
    import framework.core.struct.data.fields.CharField;
    import framework.core.struct.data.fields.FloatField;
    import framework.core.struct.data.fields.IntegerField;

    public class DummyData extends Data
    {
        private var _int_field:IntegerField = new IntegerField();
        private var _float_field:FloatField = new FloatField();
        private var _char_field:CharField = new CharField();

        private var _nested_data:NestedDummyData = new NestedDummyData();

        public function DummyData()
        {
            super('DummyData');
        }

        override public function getModelDataKeys():Array
        {
            return ['key1', 'key2'];
        }

        public function get int_field():IntegerField
        {
            return _int_field;
        }

        public function get float_field():FloatField
        {
            return _float_field;
        }

        public function get char_field():CharField
        {
            return _char_field;
        }

        public function get nested_data():NestedDummyData
        {
            return _nested_data;
        }
    }
}
