package framework.tests.data
{
    import flash.events.Event;

    import flexunit.framework.Assert;

    import framework.tests.data.classes.data.DummyData;

    import org.flexunit.async.Async;

    public class DataTest
    {
        /*============================================================================*/
        /* Private Properties                                                         */
        /*============================================================================*/
        private var _data:DummyData;
        private var _flag:Boolean = false;

        /*============================================================================*/
        /* Test Setup and Teardown                                                    */
        /*============================================================================*/
        [Before]
        public function before():void
        {
            _data = new DummyData();
            _flag = false;
        }

        [After]
        public function after():void
        {
            _data.destroy();
            _data = null;
        }

        /*============================================================================*/
        /* Tests                                                                      */
        /*============================================================================*/
        [Test(order=1)]
        public function modelDataKeys():void
        {
            var keysList:Array = _data.getModelDataKeys();
            Assert.assertObjectEquals(keysList, ['key1', 'key2']);
        }

        [Test(order=2)]
        public function importSimpleData():void
        {
            var data:Object = {
                'int_field': '1',
                'float_field': '2.053',
                'char_field': 'Hello world!'
            };
            _data.importData(data);

            Assert.assertEquals(1, _data.int_field.value);
            Assert.assertEquals('Hello world!', _data.char_field.value);
            Assert.assertTrue(_data.float_field.value - 2.053 <= 1e-10000);
        }

        [Test(order=3)]
        public function importNestedModelData():void
        {
            var struct:Object = {
                'x': 1,
                'y': 2,
                'z': 3
            };
            var data:Object = {
                'int_field': '1',
                'nested_data': {
                    'boolean_field': 1,
                    'struct_field': struct
                }
            };
            _data.importData(data);

            Assert.assertEquals(1, _data.int_field.value);
            Assert.assertTrue(_data.nested_data.boolean_field.value === true);
            Assert.assertObjectEquals(struct, _data.nested_data.struct_field.value);
            Assert.assertNull(_data.nested_data.date.value);
        }

        [Test(order=4)]
        public function importDateTimeFromString():void
        {
            var data:Object = {
                'nested_data': {
                    'date': '1988-01-01 11:40'
                }
            };
            _data.importData(data);

            var dateValue:Date = _data.nested_data.date.value;
            Assert.assertEquals(1988, dateValue.getFullYear());
            Assert.assertEquals(0, dateValue.getMonth());
            Assert.assertEquals(1, dateValue.getDate());
            Assert.assertEquals(15, dateValue.getHours());
            Assert.assertEquals(40, dateValue.getMinutes());
        }

        [Test(order=5, async)]
        public function changeEvent():void
        {
            var asyncHandler:Function = Async.asyncHandler(this, _onModelChange, 500);

            var data:Object = {"int_field": "1"};
            _data.addEventListener(Event.CHANGE, asyncHandler);
            _data.importData(data);

            Assert.assertTrue(_flag);
        }

        private function _onModelChange(...args):void
        {
            _flag = true;
        }
    }
}
