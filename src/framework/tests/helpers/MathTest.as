/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.10.13
 * Time: 15:54
 */
package framework.tests.helpers
{
    import flexunit.framework.Assert;

    import framework.core.helpers.MathHelper;

    public class MathTest
    {
        [Test]
        public function randomTest():void
        {
            var min:int = 10;
            var max:int = 100;
            var random:Number = MathHelper.random(min, max);
            Assert.assertTrue(min <= random);
            Assert.assertTrue(max >= random);
        }

        [Test]
        public function chanceTest():void
        {
            var prob:Boolean = MathHelper.chance(100);
            Assert.assertTrue(prob);

            prob = MathHelper.chance(1230);
            Assert.assertTrue(prob);
        }

        [Test]
        public function clampTest():void
        {
            var param:int = MathHelper.clamp(100, 10, 1000);
            Assert.assertEquals(param, 100);

            param = MathHelper.clamp(100, 101, 105);
            Assert.assertEquals(param, 101);

            param = MathHelper.clamp(106, 101, 105);
            Assert.assertEquals(param, 105);
        }

        [Test]
        public function greaterTest():void
        {
            var param:int = MathHelper.equalOrGreater(100, 1000);
            Assert.assertEquals(param, 1000);

            param = MathHelper.equalOrGreater(100, 50);
            Assert.assertEquals(param, 100);
        }

        [Test]
        public function lowerTest():void
        {
            var param:int = MathHelper.equalOrLower(100, 1000);
            Assert.assertEquals(param, 100);

            param = MathHelper.equalOrLower(100, 50);
            Assert.assertEquals(param, 50);
        }

        [Test]
        public function increaseTest():void
        {
            var param:Number = MathHelper.increasePercent(100, 10);
            Assert.assertEquals(param, 110);

            param = MathHelper.increasePercent(100, 100);
            Assert.assertEquals(param, 200);
        }

        [Test]
        public function reduceTest():void
        {
            var param:Number = MathHelper.reducePercent(100, 10);
            Assert.assertEquals(param, 90);

            param = MathHelper.reducePercent(100, 100);
            Assert.assertEquals(param, 0);
        }
    }
}
