package framework.tests
{
    import framework.tests.core.EventTest;
    import framework.tests.data.DataContainerTest;
    import framework.tests.data.DataTest;
    import framework.tests.data.ListOfDataTest;
    import framework.tests.data.ListOfListsTest;
    import framework.tests.data.ListOfRecordsTest;
    import framework.tests.helpers.HelpersTest;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */

    [RunWith("org.flexunit.runners.Suite")]
    [Suite]
    public class FrameworkSuite
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var dataTest:DataTest;
        public var dataContainerTest:DataContainerTest;
        public var listOfRecordsTest:ListOfRecordsTest;
        public var listOfDataTest:ListOfDataTest;
        public var listOfListsTest:ListOfListsTest;

        public var viewTest:EventTest;
        public var helpersSuite:HelpersTest;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}